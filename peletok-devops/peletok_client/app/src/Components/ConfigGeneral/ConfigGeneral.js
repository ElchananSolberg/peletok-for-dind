import React from 'react';
import { Row, Col, Container, Button } from 'reactstrap';
import { LanguageManager as LM } from '../LanguageManager/Language';
import './ConfigGeneral.css'
import InputTypeText from '../InputUtils/InputTypeText';
import {Notifications} from '../Notifications/Notifications';
import {Loader} from '../Loader/Loader';
import {
    getVat, postVat
} from '../DataProvider/DataProvider'
//**Component that displays a list of suppliers and their maximum profit percentages* */

/*TODO  -- Must make a scroll in case there are many names*/
export class ConfigGeneral extends React.Component {
    constructor(props) {
        super(props);
        this.fields = {}

        this.state = {
            vat: null,
            percentages: [
                // { name: 'celcom', Percent: 8 },
                // { name: 'pelefon', Percent: 3 },
                // { name: 'egged', Percent: 2 },
                // { name: 'ravkav', Percent: 13 },
                // { name: 'peletok', Percent: 12 },
                // { name: 'partner', Percent: 24 },
                // { name: 'global', Percent: 23 },
                // { name: 'hot', Percent: 23 },
                // { name: 'celcom', Percent: 8 }
            ],
        };
    }

    componentDidMount(){
        getVat().then((res)=>{
            this.setState({
                vat: res.vat
            },()=>{
            this.fields.vat.setValue(res.vat.toString())
        })
        })

    }

    saveVat = () => {
        this.fields.vat.runValidation();
        let newVat = this.fields.vat.getValue()
        if(newVat.valid){
            Loader.show()
            postVat({vat: newVat.value}).then((res)=>{
                Notifications.show(LM.getString('updatedSuccessfully'), 'success')
                Loader.hide()
            }).catch((err)=>{
                Notifications.show(LM.getString('error_while_saving'), 'danger')
                Loader.hide()
            })
        }
    }


    render() {
        /** Screen Header text */
        const header = LM.getString('general');

        return (
            <Container className="configGeneralContainer" >
                <Row >
                    <Col className="copyPermissionsHeader">
                        {header}
                    </Col>
                </Row>
                <Container className="copyPermissionsSettings">
                    <Row >
                        <Col style={{ maxWidth: '36%' }} >
                            <InputTypeText required={true} ref={(obj)=>this.fields.vat = obj} id="headerVat" title={LM.getString("vat")} tooltip='' maxLength='30' />
                        </Col>
                       <Col sm="auto">
                            <Button
                                id='vatSave'
                                onClick={this.saveVat}
                                className="vatButton"
                                className="suppliersButton"
                            >
                                {LM.getString("save")}
                            </Button>
                        </Col>
                    </Row>
                    <hr />
                   { /*<Row style={{ margin: 'var(--marginConfigVat)' }}>
                       <Col sm='5' style={{ marginTop: '3%', maxWidth: '45%' }}>
                           <div className='tableVat'>
                               
                               <Row >
                                   <Col className='myCol1' sm='6'>{LM.getString("agentsNameTitle")}</Col>
                                   <Col className='myCol2' sm='6'>{LM.getString("configMaximumProfitPercentage")}</Col>
                               </Row>
                               {this.state.percentages.map((cur, index) =>

                                   <Row >
                                       <Col sm='6' className={index % 2 == 0 ? 'myColGray' : 'myCol'}>{cur.name}</Col>
                                       <Col sm='6' className={index % 2 == 0 ? 'myColGray' : 'myCol'}>{cur.Percent}
                                       </Col>
                                   </Row>
                               )}
                               <Row>
                                   <Col className='footer' sm='12'></Col>
                               </Row>
                           </div>
                       </Col>
                   </Row>*/}
                </Container>
            </Container>
        );
    }
}