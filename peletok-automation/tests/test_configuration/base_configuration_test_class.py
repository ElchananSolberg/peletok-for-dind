from tests.base_project_test_class import *


class BaseConfigurationTestClass(BaseProjectTestClass):

    def login_and_get_in_Configuration(self):
        self.login()
        self.driver.tools.wait_and_click(HomePage.configuration_Link)

    def select_config_link_options(self, config_link_name):
        self.driver.tools.wait_and_click((GeneralConfigurationPage.config_link_options[0],
                                          GeneralConfigurationPage.config_link_options[1].format(config_link_name)))


