from tests.reports_manager_hashavshevet.base_report_hashavshevet_test_class import *


class TestReceiptOfReceiptsHashavshevet(BaseReportsHashavshevetTestClass):

    def login_and_get_into_Receipt_of_receipts_from_hashavshevet_page(self):
        self.login_and_get_in_report_manager_area_hashavshevet("importReceiptsFromHashavshevet",
                                                               GeneralElementsInReportsAreaManagerHashavshevet.view_report_button)

    def test_receipt_of_receipts_hashavshevet_page_login_and_check_credit_checkbox(self):
        self.login_and_get_into_Receipt_of_receipts_from_hashavshevet_page()
        self.click_checkbox_hashavshevet(GeneralElementsInReportsAreaManagerHashavshevet.credit_id_check_box,
                                         GeneralElementsInReportsAreaManagerHashavshevet.credit_label_check_box)
        self.remove_logs_dir = True

    def test_receipt_of_receipts_hashavshevet_page_login_and_check_delek_user_checkbox(self):
        self.login_and_get_into_Receipt_of_receipts_from_hashavshevet_page()
        self.click_checkbox_hashavshevet(GeneralElementsInReportsAreaManagerHashavshevet.delek_id_check_box,
                                         GeneralElementsInReportsAreaManagerHashavshevet.delek_label_check_box)
        self.remove_logs_dir = True

    def test_receipt_of_receipts_hashavshevet_page_show_reports_btn(self):
        self.login_and_get_into_Receipt_of_receipts_from_hashavshevet_page()
        self.driver.wait.wait_for_element_to_be_present(
            GeneralElementsInReportsAreaManagerHashavshevet.view_report_button)
        self.remove_logs_dir = True
