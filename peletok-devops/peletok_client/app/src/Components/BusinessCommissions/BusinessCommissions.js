import React, { Component } from 'react';

import '../CommissionProfile/CommissionProfile.css';

import { Button, Container, Row, Col } from 'reactstrap';

import { LanguageManager as LM } from "../LanguageManager/Language";
import { Notifications } from '../Notifications/Notifications';
import { Loader } from '../Loader/Loader'
import { Confirm } from '../Confirm/Confirm';
import { SelectedSeller } from '../SelectedSeller/SelectedSeller';

import CommissionProfileTable from '../CommissionProfile/CommissionProfileTable';

import {
    getBusinessProductCommissionDueToUser, getBusinessProductCommissionAuthorizationDueToBusiness,
    getBusinessSupplierAuthorizationDueToUser, getBusinessSupplierAuthorizationDueToBusiness,
    putBusinessProducts,
    putBusinessCommission, putBusinessCommissionForFinalClient,
} from '../DataProvider/DataProvider'

import InputTypeSearchList from '../InputUtils/InputTypeSearchList';
import InputTypeNumber from '../InputUtils/InputTypeNumber';

import { TypesEnum } from '../../enums/TypesEnum';
/**
 * BusinessCommissions is a main component of BusinessCommissions Page.  BusinessCommissions works with .css file: CommissionProfile.css.
 */
class BusinessCommissions extends Component {

    constructor(props) {
        super(props);

        this.state = {
            currentUserProducts: [],
            /** working variable that will contain a selected seller object */
            selectedSeller: {},
            /** working variable that will contain an array of suppliers from the server*/
            suppliers: [],
            /** working variable that will contain an array of products from the server */
            products: [],
            /** tableRows - an array of objects for <CommissionProfileTable/> component. The values of the objects are the data of the rows of a table */
            tableRows: [],
        }
        /** working variable that will contain a FULL array of products from the server */
        this.products = [];
        /** working variable that will contain a FILTERED due to supplier array of products from the server */
        this.productsFilteredDueToSupplier = [];
    }

    /** "Select Product" field onChange handler */
    productChanged = (e) => {
        let value = this.selectProduct.getValue().value;
        let selectedProduct = this.productsFilteredDueToSupplier.find(product => (product.product_name === value));
        let productsFiltered = this.productsFilteredDueToSupplier.filter(element => element.product_id === selectedProduct.product_id);
        this.setState({ products: productsFiltered }, () => {
            this.loadProductAuthorizationDataIntoTable();
        });
    }
    /** "Select Supplier" field onChange handler */
    supplierChanged = (e) => {
        let value = this.selectSupplier.getValue().value;
        let selectedSupplier = this.state.suppliers.find(supplier => (supplier.supplier_name === value));
        let productsFiltered = this.products.filter(element => element.supplier_id === selectedSupplier.supplier_id);

        this.setState({ products: productsFiltered }, () => {
            this.selectProduct.setValue("");
            this.loadProductAuthorizationDataIntoTable();
        });
        this.productsFilteredDueToSupplier = productsFiltered;
    }

    /** "Spread Commission" button click handler */
    buttonSpreadCommissionClick = (e) => {
        if (SelectedSeller.currentSeller.getValue().id) {
            let value;
            let sellerId = this.state.selectedSeller.id;
            if (this.commission.getValue().valid) value = this.commission.getValue().value;
            if (value) {
                let productsIds = this.state.products.map((product) => { return product.product_id });
                let commissionData = {
                    "business_id": sellerId,
                    "business_commission": value,
                    "product_id": productsIds,
                };
                Confirm.confirm(LM.getString("thisActionwillLeadToChangesInAllProducts")).then(() => {
                    Loader.show();
                    putBusinessCommission(commissionData).then(
                        res => {
                            Loader.hide();
                            Notifications.show(LM.getString("commission") + ' ' + LM.getString('updatedSuccessfully'), 'success');
                            this.fieldsLoad();
                        }
                    ).catch(
                        err => {
                            Loader.hide();
                            Notifications.show(err, 'danger');
                        }
                    )
                });
            }
            else Notifications.show(LM.getString("commission") + ' ' + LM.getString('notDefined'), 'warning');
        }
        else Notifications.show(LM.getString("sellerNotSelectedError"), 'warning');
    }

    /** "Spread Commission For Final Client" button click handler */
    buttonSpreadCommissionForFinalClientClick = (e) => {
        if (SelectedSeller.currentSeller.getValue().id) {
            let value;
            let sellerId = this.state.selectedSeller.id;
            if (this.commissionForFinalClient.getValue().valid) value = this.commissionForFinalClient.getValue().value;
            if (value) {
                let productsIds = this.state.products.map((product) => { return product.product_id });
                let commissionForFinalClientData = {
                    "business_id": sellerId,
                    "final_commission": value,
                    "product_id": productsIds,
                };
                Confirm.confirm(LM.getString("thisActionwillLeadToChangesInAllProducts")).then(() => {
                    Loader.show();
                    putBusinessCommissionForFinalClient(commissionForFinalClientData).then(
                        res => {
                            Loader.hide();
                            Notifications.show(LM.getString("commissionForFinalClient") + ' ' + LM.getString('updatedSuccessfully'), 'success');
                            this.fieldsLoad();
                        }
                    ).catch(
                        err => {
                            Loader.hide();
                            Notifications.show(err, 'danger');
                        }
                    )
                });
            }
            else Notifications.show(LM.getString("commissionForFinalClient") + ' ' + LM.getString('notDefined'), 'warning');
        }
        else Notifications.show(LM.getString("sellerNotSelectedError"), 'warning');
    }
    /** "Save" button click handler */
    buttonSaveClick = (e) => {
        if (SelectedSeller.currentSeller.getValue().id) {
            let productsList = this.state.currentUserProducts.map((product, index = 0) => {
                let tableRowCheckBoxChecked = this.commissionProfileTable.tableRows['tableRow' + index].tableRowCheckBox.props.checked;
                let tableRowResellerCommission = this.commissionProfileTable.tableRows['tableRow' + index].resellerCommission.getValue().value;
                let tableRowCommissionForFinalClient = this.commissionProfileTable.tableRows['tableRow' + index].commissionForFinalClient.getValue().value;
                if (tableRowResellerCommission === '' && product.business_commission === null) tableRowResellerCommission = null;
                if (tableRowCommissionForFinalClient === '' && product.final_commission === null) tableRowCommissionForFinalClient = null;
                if (
                    tableRowCheckBoxChecked !== product.is_authorized
                    || tableRowResellerCommission !== product.business_commission
                    || tableRowCommissionForFinalClient !== product.final_commission
                ) {
                    return {
                        "product_id": product.product_id,
                        "is_authorized": tableRowCheckBoxChecked,
                        "business_commission": (tableRowResellerCommission === '') ? null : tableRowResellerCommission,
                        "final_commission": (tableRowCommissionForFinalClient === '') ? null : tableRowCommissionForFinalClient,
                    };
                }
                else return null;
            });
            let cleanedFromNullProductsList = productsList.filter(product => product !== null);
            let businessProductData = {
                "business_id": this.state.selectedSeller.id,
                "products_list": cleanedFromNullProductsList,
            };
            if (cleanedFromNullProductsList[0]) {
                Loader.show();
                putBusinessProducts(businessProductData).then(
                    res => {
                        Loader.hide();
                        Notifications.show(LM.getString("successSellerUpdating") + this.state.selectedSeller.name, 'success');
                    }
                ).catch(
                    err => {
                        Loader.hide();
                        Notifications.show(err, 'danger');
                    }
                );
            }
            else Notifications.show(LM.getString("noChanges"), 'warning');
        }
    }

    /** Function that enters a data from a response on getBusinessProductCommissionDueToUser() request into the table. */
    loadProductDataIntoTable = () => {
        let tableRows = this.state.products.map(product => { return { 'product': product.product_name } });
        this.setState({ tableRows: tableRows })
    }
    /** Function that enters a data from a response on getBusinessProductCommissionAuthorizationDueToBusiness() request into the table. */
    loadProductAuthorizationDataIntoTable = () => {

        let tableRows = this.state.currentUserProducts.map(currentProduct => {
            let product = this.state.products.find(p => p.product_id == currentProduct.product_id)
            return product ? {
                'product': product.product_name,
                'resellerCommission': product.business_commission,
                'resellerCommissionP20': product.article_20_business_commission,
                'commissionForFinalClient': product.final_commission,
                'checkbox': product.is_authorized,
                validationObjec: {
                    business_commission: currentProduct.business_commission,
                    max_percentage_profit: currentProduct.max_percentage_profit,
                    min_percentage_profit: currentProduct.min_percentage_profit,
                    distribution_fee: currentProduct.distribution_fee,
                }
            } : false;
        }).filter(p => p);
        this.setState({ tableRows: tableRows })
    }

    /** Function that loads from a server a new data for the table due to selected Seller. */
    fieldsLoad = () => {
        let selectedSellerId = this.state.selectedSeller.id;

        getBusinessSupplierAuthorizationDueToBusiness(selectedSellerId).then(
            (res) => {
                this.setState({ suppliers: res.filter(s => s.type == TypesEnum.SUPPLIER_BILL) });
            }
        ).catch(
            (err) => {
                Notifications.show(LM.getString('loadSuppliersListFailed') + ' ' + err, 'danger');
            }
        )

        getBusinessProductCommissionAuthorizationDueToBusiness(selectedSellerId).then(
            (res) => {
                this.products = res;
                this.setState({ products: res }, () => this.loadProductAuthorizationDataIntoTable());
            }
        ).catch(
            (err) => {
                Notifications.show(LM.getString('loadProductsListFailed') + ' ' + err, 'danger');
            }
        )
    }

    launchGetBusinessProductCommissionDueToUser = () => {
        getBusinessSupplierAuthorizationDueToUser().then(
            (res) => {
                this.setState({ suppliers: res });
            }
        ).catch(
            (err) => {
                Notifications.show(LM.getString('loadSuppliersListFailed') + ' ' + err, 'danger');
            }
        )

        getBusinessProductCommissionDueToUser().then(
            (res) => {
                this.products = res;
                this.setState({ currentUserProducts: res }, () => this.loadProductDataIntoTable());
            }
        ).catch(
            (err) => {
                Notifications.show(LM.getString('loadProductsListFailed') + ' ' + err, 'danger');
            }
        )
    }

    componentDidMount() {
        SelectedSeller.setHeader.next(LM.getString('commissions'));
        if (SelectedSeller.newSellerState) SelectedSeller.createSellerClicked.next();
        if (SelectedSeller.newDistributorState) SelectedSeller.createDistributorClicked.next();
        if (SelectedSeller.currentSeller.getValue().id) this.setState({ selectedSeller: SelectedSeller.currentSeller.getValue() }, () => this.fieldsLoad());
        this.launchGetBusinessProductCommissionDueToUser();
        SelectedSeller.currentSeller.subscribe(x => {
            if(x.id){
                this.setState({ selectedSeller: x }, () => this.fieldsLoad())
            }
        });
        SelectedSeller.currentDistributor.subscribe(x => {
            if(x.id){
                this.setState({ selectedDistributor: x }, () => {
                    this.selectSupplier && this.selectSupplier.setValue("");
                    this.selectProduct && this.selectProduct.setValue("");
                })
            }
        })
    }

    render() {
        /** selectSupplierProps - props for "Select Supplier" field (<InputTypeSearchList/> component) */
        const selectSupplierProps = {
            options: [''],
            title: LM.getString('selectSupplier'),
            id: "BusinessCommissionsSelectSupplier",
        };
        /** selectProductProps - props for "Select Product From List" field (<InputTypeSearchList/> component) */
        const selectProductProps = {
            options: [''],
            title: LM.getString('selectProductFromList'),
            id: "BusinessCommissionsSelectProduct",
        };

        /** commissionProps - props for "Commission" field (<InputTypeNumber> component) */
        const commissionProps = {
            title: LM.getString('commission'),
            id: "BusinessCommissionsCommission",
        };
        /** commissionForFinalClientProps - props for "Commission for Final Client" field (<InputTypeNumber> component) */
        const commissionForFinalClientProps = {
            title: LM.getString('commissionForFinalClient') + ":",
            id: "BusinessCommissionsCommissionForFinalClient",
        };

        /** data for button Spread Commission on all the Products  */
        const buttonSpreadCommission = LM.getString('spreadCommissionOnAll');
        /** data for button Spread Final Commission on all the Products  */
        const buttonSpreadCommissionForFinalClient = LM.getString('spreadCommissionForFinalClientOnAll');
        /** data for button Save  */
        const buttonSave = LM.getString('save');

        /** tableHeadersObj - an object for <CommissionProfileTable/> component. The values of the object are the HEADERS of a table */
        const tableHeadersObj = {
            product: LM.getString('product'),
            resellerCommission: LM.getString('resellerCommission'),
            resellerCommissionP20: LM.getString('resellerCommissionP20'),
            commissionForFinalClient: LM.getString('commissionForFinalClient'),
            authorizedUser: LM.getString('authorizedUser'),
        };

        return (
            <Container className="commissionProfileContainer">
                {SelectedSeller.currentSeller.getValue().id ?
                    <Container className="commissionProfileSettings">
                        <Row>
                            <Col lg="4" className='d-flex flex-column'>
                                <InputTypeSearchList
                                    {...selectSupplierProps}
                                    onChange={this.supplierChanged}
                                    options={this.state.suppliers.map(supplier => supplier.supplier_name) || ['']}
                                    ref={(componentObj) => {
                                        this.selectSupplier = componentObj
                                    }}
                                />
                            </Col>

                            <Col lg="4" className='d-flex flex-column'>
                                <InputTypeSearchList
                                    {...selectProductProps}
                                    onChange={this.productChanged}
                                    options={this.productsFilteredDueToSupplier.map(product => product.product_name) || ['']}
                                    ref={(componentObj) => {
                                        this.selectProduct = componentObj
                                    }}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col lg="4" className='d-flex flex-column'>
                                <InputTypeNumber
                                    {...commissionProps}
                                    ref={(componentObj) => {
                                        this.commission = componentObj
                                    }}
                                />
                            </Col>
                            <Col lg="auto" className='d-flex flex-column'>
                                <Row>
                                    <Col style={{ marginTop: '25px', }}>
                                        <Button
                                            style={{ width: '100%', overflow: 'hidden' }}
                                            className={'commissionProfileButton commissionProfileBigButton commissionProfileMinWidth'}
                                            onClick={this.buttonSpreadCommissionClick}
                                            id="BusinessCommissionsSpreadCommissionButton"
                                        >
                                            {buttonSpreadCommission}
                                        </Button>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            <Col lg="4" className='d-flex flex-column'>
                                <InputTypeNumber
                                    {...commissionForFinalClientProps}
                                    ref={(componentObj) => {
                                        this.commissionForFinalClient = componentObj
                                    }}
                                />
                            </Col>
                            <Col lg="auto" className='d-flex flex-column'>
                                <Row>
                                    <Col style={{ marginTop: '25px', }}>
                                        <Button
                                            style={{ width: '100%', overflow: 'hidden' }}
                                            className={'commissionProfileButton commissionProfileBigButton commissionProfileMinWidth'}
                                            onClick={this.buttonSpreadCommissionForFinalClientClick}
                                            id="BusinessCommissionsSpreadCommissionForFinalClientButton"
                                        >
                                            {buttonSpreadCommissionForFinalClient}
                                        </Button>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        <Row style={{ marginTop: '30px' }}>
                            <Col>
                                <CommissionProfileTable
                                    tableRows={this.state.tableRows}
                                    tableHeadersObj={tableHeadersObj}
                                    tableFootersObj={this.state.tableFootersObj}
                                    tableRowsEditable={[false, true, false, true, false]}
                                    ref={(componentObj) => {
                                        this.commissionProfileTable = componentObj
                                    }}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col lg="2" style={{ marginTop: '15px', }}>
                                <Button
                                    style={{ width: '100%', overflow: 'hidden' }}
                                    className="commissionProfileButton commissionProfileBigButton"
                                    onClick={this.buttonSaveClick}
                                    id="BusinessCommissionsSaveButton"
                                >
                                    {buttonSave}
                                </Button>
                            </Col>
                        </Row>
                    </Container>
                    : <></>
                }
            </Container>
        );
    }
}
export default BusinessCommissions;