from selenium.webdriver.common.by import By


class LoginPage:
    username = (By.ID, "usernameId")
    password = (By.ID, "passwordId")
    submit_btn = (By.ID, "btnLogin")
