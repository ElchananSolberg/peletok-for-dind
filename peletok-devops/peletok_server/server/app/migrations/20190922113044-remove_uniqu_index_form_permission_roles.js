'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return  Promise.resolve()
      .then(()=>queryInterface.removeConstraint('PRM_PermissionRole', 'PRM_PermissionRole_permission_id_role_id_key'))
  
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  }
};
