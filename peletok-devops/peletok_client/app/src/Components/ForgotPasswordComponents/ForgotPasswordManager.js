import React, { Component } from 'react';
import { Container, Col, Row } from 'reactstrap';
import { ForgotPasswordEnterPhone } from './ForgotPasswordEnterPhone';
import { ForgotPasswordSms } from './ForgotPasswordSms';
import { ForgotPasswordResetPass } from './ForgotPasswordResetPass';
import { SupportLayout } from "../SupportLayout/SupportLayout";
import { LanguageManager as LM } from "../LanguageManager/Language";
import { getSuffixPhoneNumber, getPhoneNumberForUser, postServerSmsChecker, postResetPassWithVrarificationCode } from "../DataProvider/DataProvider";
import { Notifications } from "../Notifications/Notifications";


export class ForgotPasswordManager extends Component {

    // this calss manages the page content of the forgot password prosess
    // TODO - change all mock calls ("server...Checker") to reall calls to the sever
    // TODO - redirect at the end of the forgot password to: (login/main?)


    constructor(props) {
        super(props);
        this.state = {
            screen: 'enterPhone',
            username: props.match.params.username,
            lastDigits: '',
            phoneNumber: '',
            phoneErrMsg: '',
            smsCode: '',
            smsCodeErrMsg: '',
            passOne: '',
            passTwo: '',
            passErrMsg: '',
        };
        this.phoneChange = this.phoneChange.bind(this);
        this.smsCodeChange = this.smsCodeChange.bind(this);
        this.passOneChange = this.passOneChange.bind(this);
        this.passTwoChange = this.passTwoChange.bind(this);
    }

    serverPhoneChecker(username, phone) {
        return getPhoneNumberForUser(username, phone)
    }

    serverSmsChecker(username, smsCode) {
        return postServerSmsChecker(username, smsCode)
    }

    handleEnterPhone(e) {
        // e.preventDefault();
        if (this.state.phoneNumber.length === 0) {
            this.setState({ phoneErrMsg: LM.getString('MustEnterPhoneNumber') })
        } else {
            if (this.state.phoneNumber.length !== 10) {
                this.setState({ phoneErrMsg: LM.getString('PhoneNumberMustInclude10Digits') })
            } else {
                if (this.state.lastDigits !== this.state.phoneNumber.slice(-3)) {
                    this.setState({ phoneErrMsg: LM.getString('LastDigitsDoNotMatch') })
                } else {
                    this.serverPhoneChecker(this.state.username, this.state.phoneNumber).then((res) => {
                        this.setState({ screen: "smsCode" })
                    }).catch(err => {
                        this.setState({ phoneErrMsg: err.response.data.message })
                    })
                }
            }
        }
    }

    handleEnterSmsCode(e) {
        if (this.state.smsCode.length === 0) {
            this.setState({ smsCodeErrMsg: LM.getString("MustEnterCodeNumber") })
        } else {
            this.serverSmsChecker(this.state.username, this.state.smsCode).then((res) => {
                switch (res) {
                    case 1:
                        this.setState({ screen: "resetPassword" })
                        break;
                    case -1:
                        this.setState({ smsCodeErrMsg: LM.getString('verificationCodeExpired') })
                        break;

                    case -2:
                        this.setState({ smsCodeErrMsg: LM.getString('wrongVerificationCode') })
                        break;

                    default:
                        break;
                }
            });
        }
    }

    async handleEnterPassCodes(e) {
        if (this.state.passOne.length === 0 | this.state.passTwo.length === 0) {
            this.setState({ passErrMsg: LM.getString("MustEnterBothPasswords") })
        } else {
            if (this.state.passOne == this.state.passTwo) {
                await postResetPassWithVrarificationCode(this.state.username, this.state.smsCode, this.state.passTwo).then(() => {
                    setTimeout(() => {
                        Notifications.show(LM.getString("selectUserPasswordChanged"), "success");
                        this.props.history.push('/main/charge')
                        Notifications.show(LM.getString("nowLoginWithNewPass"), "success");
                    }, 1500)
                }).catch((error) => {
                    Notifications.show(error, "danger")
                })
            } else {
                this.setState({ passErrMsg: LM.getString("PasswordsDidNotMatchPleaseEnterAgain") })
            };
        }
    }

    componentDidMount() {
        getSuffixPhoneNumber(this.state.username).then(res => {
            this.setState({ lastDigits: res + "" })
        })
    }

    phoneChange(e) {
        this.setState({ phoneNumber: e.target.value, phoneErrMsg: '' })
    }

    smsCodeChange(e) {
        this.setState({ smsCode: e.target.value, smsCodeErrMsg: '' })
    }

    passOneChange(e) {
        this.setState({ passOne: e.target.value, passErrMsg: '' })
    }

    passTwoChange(e) {
        this.setState({ passTwo: e.target.value, passErrMsg: '' })
    }

    render() {
        let currentScreen = false;
        switch (this.state.screen) {
            case 'enterPhone':
                currentScreen = <ForgotPasswordEnterPhone lastDigits={this.state.lastDigits} onChange={this.phoneChange} onSubmit={() => this.handleEnterPhone()} errMsg={this.state.phoneErrMsg} />
                break;
            case 'smsCode':
                currentScreen = <ForgotPasswordSms phoneNumber={this.state.phoneNumber} onChange={this.smsCodeChange} onSubmit={() => this.handleEnterSmsCode()} errMsg={this.state.smsCodeErrMsg} />
                break;
            case 'resetPassword':
                currentScreen = <ForgotPasswordResetPass lastDigits={this.state.lastDigits} onChangeOne={this.passOneChange} onChangeTwo={this.passTwoChange} onSubmit={() => this.handleEnterPassCodes()} errMsg={this.state.passErrMsg} />
                break;
            default:
                currentScreen = <div> No currentScreen </div>;
        }
        if (!currentScreen) {
            this.setState({ screen: 'enterPhone' });
        }
        return (
            <Container color={'light'} className={'mb-5'}>
                <Row>
                    <Col className={'d-sm-flex justify-content-between border-bottom'}>
                        <h5 id="back" link="true" className={'pointer font-weight-light fs-26 pb-md-3 pt-md-4 pt-2'} color={"link"} onClick={(e) => { e.preventDefault(); window.history.back(); }}
                        > {"<"} {LM.getString("back")}</h5>
                        <h5 className={'font-weight-light fs-22 pb-md-3 pt-md-4 py-sm-2'}>
                            {LM.getString("forgotPassEntry")}
                        </h5>
                    </Col>
                </Row>
                <Row className={'pt-md-4 justify-content-around'}>
                    <Col md="5" sm="12" className={'fs-17'}>
                        {currentScreen}
                    </Col>
                    <Col md="6" sm="12">
                        <SupportLayout />
                    </Col>
                </Row>
            </Container>
        );
    }
}
