export function arrayToObject(data = [], keyName){
    return data.reduce((object, item)=>{
      let key = item[keyName] || 'others';
      
        // If key exist push to array else create new array
        object[key] ?
          object[key].push(item) 
            : object[key] = [item];
    
      return object;
    },{})
  }

export function createArrayInObjectOrPushIfExist(object, key, value){
    if(object[key]){
        object[key].push(value) 
    } else {
        object[key] = [value]
    }
    return object;
}

function _getDayToString(date){
  return  (date.getDate() + 1).toString().length < 2 ? '0' + (date.getDate().toString() ) : (date.getDate().toString());
}

function _getMonthToString(date){
  return (date.getMonth() + 1).toString().length < 2 ? '0' + (date.getMonth() + 1).toString() : (date.getMonth() + 1).toString();
}
function _getYearToString(date){
  return date.getFullYear().toString().substr(-2);
}
export function dateToString(date){
    let day   = _getDayToString(date);
    let month = _getMonthToString(date);
    let year  = _getYearToString(date);
    return  day + '/' + month+ '/' + year;
}

export function objectToFormData (obj, form, namespace) {
  var fd = form || new FormData();
  var formKey;
  
  for(var property in obj) {
    if(obj.hasOwnProperty(property)) {
      
      if(namespace) {
        formKey = namespace + '[' + property + ']';
      } else {
        formKey = property;
      }
     
      // if the property is an object, but not a File,
      // use recursivity.
      if(typeof obj[property] === 'object' && !(obj[property] instanceof File)) {
        
        objectToFormData(obj[property], fd, property);
        
      } else {
        
        // if it's a string or a File object
        fd.append(formKey, obj[property]);
      }
      
    }
  }
  
  return fd;
    
};