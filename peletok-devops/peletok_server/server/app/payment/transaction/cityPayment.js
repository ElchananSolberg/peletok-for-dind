const cityPaymentTypeSiteModel = require("../../index").cityPaymentTypeSite.model;
const siteModel = require("../../index").site.model;
const ResInfo = require('../../resInfo')
const LM = require('../../config/LanguageManager').LM
const fs = require('fs');
const path = require('path');
const NoSupplier = "getXmlRow not found"
/***
  *choose site for  payment bill 
     */
class CityPayment {
    constructor(itemId, billNum, idClient, price,req) {
        this.req=req
        this.itemId = itemId
        this.billNum = billNum
        this.idClient = idClient
        this.price = price
        this.info =new ResInfo
    }
    /***
  * Get site by id   from DB
    */
    async  findSiteById(id) {
        return await cityPaymentTypeSiteModel.findByPk(id, {
            include: {
                model: siteModel,
                as: "Site"
            }
        });
    }
    /***
     * find and  get  name  of Class 
     *  go to his address
     *         */
    getClassesAsObject() {
        let obj = {};
        const pathCrawlers = path.join(__dirname, "./cityCrawlers");
        fs.readdirSync(pathCrawlers).forEach(f => {
            if (f.indexOf("Crawler") !== -1) {
                obj[f.charAt(0).toUpperCase() + f.slice(1, -3)] = require(path.join(pathCrawlers, f.slice(0, -3)))
            }
        });
        return obj;
    }
    /***
     * Get  site id  for  payment 
     *   * run class  by his  name 
        */
    async getPaymentSite() {
        const objClass = this.getClassesAsObject();
        const res = await this.findSiteById(this.itemId)
        try {
          
            
            return new objClass[res.Site.class_name](res.Site.site_url + res.params, this.billNum, this.idClient, this.price,this.req);
          }
          catch(error) {
          
              
            this.info.setErrMessage(LM.getString("errCityPayment"),error)
            return this.info
          }
        
     
      
            
        
    }
    /***
     *   check   price of  bill for  transaction
        */
    async  getPrice() {
    
        const paymentSite = await this.getPaymentSite()
        if (this.info.error.message) {
            return this.info
        }
        return await paymentSite.getPrice()
    }
    /***
     *   check  what price of  bill
        */
    async  getBillPrice() {

     

        
        const paymentSite = await this.getPaymentSite()
        if (this.info.error.message) {
            return this.info
        }
        return await paymentSite.getBillPrice()
    
    }
    /***
      * Pay a bill     
      @returns number  of   confirmation
                */
               async pay(transactionId, creditCardId){
        const paymentSite = await this.getPaymentSite()
        if (this.info.error.message) {
            return this.info
        }
    
       return await paymentSite.pay(transactionId, creditCardId)
    }
}
module.exports = CityPayment