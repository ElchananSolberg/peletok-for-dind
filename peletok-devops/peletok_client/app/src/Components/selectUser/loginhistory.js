import React, { Component } from 'react';
import { Container, Row, Col, Modal, ModalBody, ModalHeader } from 'reactstrap'
import './selectUser.css'
import { HistoryLine } from './HistoryLine';
import { LanguageManager as LM } from '../LanguageManager/Language';


/**
 * A pop-up screen with the user's specific login history
 * TODO --All strings will be replaced by languages
 */
export class LoginHistory extends Component {

    render() {
        return (
            <Modal
                isOpen={this.props.isOpen}
                toggle={this.props.toggle}
                className={"loginHistoryModal"}
            >
                <ModalHeader toggle={this.props.toggle} >
                    <Container >
                        <Row>
                            <div style={{ width: "100%", textAlign: "center", color: '#4864de' }}>{LM.getString("selectUsertitleLoginHistory")}</div>
                        </Row>
                        <div style={{ textAlign: "center", color: '#626A7C', fontSize: '18px' }}>
                            {this.props.user.first_name + ' ' + this.props.user.last_name}
                        </div>
                    </Container>
                </ModalHeader>

                <div className='my-3 mx-3'>
                    <Row className="loginHistoryModalLine loginHistoryModalLineHeader">
                        <Col sm="1" xs='1' className={'fs-17 font-weight-bold'}>{LM.getString("selectUserNum")}</Col>
                        <Col sm="3" xs='3' className={'fs-17 font-weight-bold'}>
                            {LM.getString("ip")}
                        </Col>
                        <Col sm="3" xs='5' className={'fs-17 font-weight-bold'}>{LM.getString("date")}</Col>
                        <Col sm="3" className={'fs-17 d-none d-sm-block font-weight-bold'}>{LM.getString("selectUserday")}</Col>
                        <Col sm="2" xs='2' className={'fs-17 font-weight-bold'}>{LM.getString("time")}</Col>
                    </Row>
                    {
                        Array.isArray(this.props.user.login_history) ? this.props.user.login_history.map((cur, index) =>
                            <HistoryLine key={index} index={index} user={this.props.user} login_history={cur} type="history" />
                        ) :
                            <HistoryLine user={this.props.user} login_history={this.props.login_history} type="history" />
                    }
                </div>

            </Modal>
        )
    }
}