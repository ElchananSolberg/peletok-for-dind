import React from 'react';
import { 
    Modal,
    ModalHeader,
    ModalFooter,
    ModalBody,
    Button ,
} from 'reactstrap';
import './Confirm.css'
import { LanguageManager as LM } from '../LanguageManager/Language';

/** <Confirm/> is Singleton  */
/** The component receives a message to display, and also receives the color of the message */
/** To display a message you must import the component and then use the desired location as this example :
 *               {Confirm.show("הגעת ליעד","info",5000)}
 * 
*/
export class Confirm extends React.Component {

    static singleton;
    static index = 0;
    resolve
    reject
    constructor(props) {
        super(props);

        
        /** here we make Confirm to be singleton (to have only 1 instance) */
        if (Confirm.singleton) {
            throw 'Attention, developer! You must not use Confirm as a component, you should use show()  interface.';
        }
        Confirm.singleton = this;

        this.state = {
            isOpen: false,
            meassege : '',
        }

    }

    show = (meassege) => {
       
        if (Confirm.index > 5) throw 'It seems that Confirm component does not exist in a render tree.';

        if (!Confirm.singleton) {
            Confirm.index++;
            setTimeout(() => {
                Confirm.show();
            }, 300)
        } else { 
            Confirm.singleton.setState({ meassege: meassege ,isOpen:true},()=>{
                Confirm.singleton.setState({ show: true });
            });
        }
        // Return treu enable to call notification in tinery condition and then call ui element in render
        return true;
    }

    clean(){
        Confirm.singleton.setState({ meassege: '' ,isOpen:false})
        Confirm.singleton.resolve = undefined;
        Confirm.singleton.reject = undefined;
    }

    static confirm(message, skip=false){
        if(!skip){
            return new Promise((resolve, reject)=>{
                Confirm.singleton.show(message)
                Confirm.singleton.resolve = resolve;
                Confirm.singleton.reject = reject;
            })
        }else{
            Confirm.singleton.clean()
            return Promise.resolve()
        }
    }




    render = () => {
        return (
            <>
                {this.state.show &&
                    <div>
                          <Modal isOpen={Confirm.singleton.state.isOpen}  >
                                <ModalBody>
                                    {Confirm.singleton.state.meassege}
                                </ModalBody>
                                <ModalFooter>
                                    <Button className ='m-2' color="primary" onClick={()=>{
                                        Confirm.singleton.resolve();
                                        Confirm.singleton.clean();}} >{LM.getString("confirm")
                                    }</Button>{' '}
                                    <Button color="secondary" onClick={()=>{
                                        Confirm.singleton.reject();
                                        Confirm.singleton.clean();}} >{LM.getString("cancel")}</Button>
                                </ModalFooter>
                            </Modal>
                    </div>
                }
            </>
        );
    }
}
