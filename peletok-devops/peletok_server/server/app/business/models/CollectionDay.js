const DataTypes = require('sequelize').DataTypes;
const BaseModel = require('../../BaseModel');

/**
 * CollectionDay model
 */
class CollectionDay extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            sunday: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false
            },
            monday: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false
            },
            tuesday: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false
            },
            wednesday: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false
            },
            thursday: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false
            },
            friday: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false
            },
            saturday: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false
            },
            monthly: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false
            },

        };
        this.author = true;
        this.statusField = true;
        this.statusTimestampField = true;
        this.prefix = 'BSN';
    }

    /**
     * creates associate for CollectionDay model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.business.model, {as: 'Business'});
    }
}

module.exports = new CollectionDay();