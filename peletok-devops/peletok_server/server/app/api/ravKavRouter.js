const express = require('express');
const axios = require('axios').default;
const ravKavRouter = express.Router();
const models = require('../index.js');
const Op = require("sequelize").Op;


const BASE_URL = "https://ravkavonline.co.il";
const URL_SESSION = "/api/transaction/session/";
// const URL_GET_PRODUCTS = "/api/ravkav/identify-extended-casponet/";
const URL_GET_PRODUCTS = "/api/identify/dump-extended/";
const URL_CREATE_TRANSACTION = "/api/transaction/transaction/";
const URL_PAY_WITH_BALANCE = "/api/transaction/{0}/payment-balance/";
const URL_CREATE_RELOADING = "/api/transaction/{0}/reloading/";
const URL_ABORT_RELOADING = "/api/transaction/{0}/abort-reloading/";
const URL_ABORT_TRANSACTION = "/api/transaction/{0}/abort/";
const URL_GET_PURCHASE_APPROVAL = "/api/transaction/{0}/purchase-approval/";
const URL_CANCEL = "/api/transaction/{0}/reloading-cancellation/";
const URL_REPORT = "/api/transaction/?{0}";
const URL_GET_STATUS_BY_TRANSACTION_ID = "/api/transaction/{0}/";

const TOKEN = "Token 553a590068869bf15997253063a13e0f7833c822";

//בטסט הפניה חייבת לצאת מפורט 8000
const BASE_URL_TEST = "https://test-ravkavonline.co";
// const TOKEN_TEST = "Token 57a1e3b8a527fa46bdebd835cdded30d6f1833ad";
const TOKEN_TEST = "Token 8897420b10fc5d4b6895166fe598fa6bd806386c";

const SessionUrl = BASE_URL + URL_SESSION;
const TestSessionUrl = BASE_URL_TEST + URL_SESSION;
const GetProductsUrl = BASE_URL + URL_GET_PRODUCTS;
const TestGetProductsUrl = BASE_URL_TEST + URL_GET_PRODUCTS;
const CreateTransactionUrl = BASE_URL + URL_CREATE_TRANSACTION;
const TestCreateTransactionUrl = BASE_URL_TEST + URL_CREATE_TRANSACTION;
const PayWithBalanceUrl = BASE_URL + URL_PAY_WITH_BALANCE;
const TestPayWithBalanceUrl = BASE_URL_TEST + URL_PAY_WITH_BALANCE;
const CreateReloadingUrl = BASE_URL + URL_CREATE_RELOADING;
const TestCreateReloadingUrl = BASE_URL_TEST + URL_CREATE_RELOADING;
const AbortTransactionUrl = BASE_URL + URL_ABORT_TRANSACTION;
const TestAbortTransactionUrl = BASE_URL_TEST + URL_ABORT_TRANSACTION;
const AbortReloadingUrl = BASE_URL + URL_ABORT_RELOADING;
const TestAbortReloadingUrl = BASE_URL_TEST + URL_ABORT_RELOADING;
const GetPurchaseAppvoralUrl = BASE_URL + URL_GET_PURCHASE_APPROVAL;
const TestGetPurchaseAppvoralUrl = BASE_URL_TEST + URL_GET_PURCHASE_APPROVAL;
const CancelUrl = BASE_URL + URL_CANCEL;
const TestCancelUrl = BASE_URL_TEST + URL_CANCEL;
const ReportUrl = BASE_URL + URL_REPORT;
const TestReportUrl = BASE_URL_TEST + URL_REPORT;
const GetStatusByTransactionIdUrl = BASE_URL + URL_GET_STATUS_BY_TRANSACTION_ID;
const TestGetStatusByTransactionIdUrl = BASE_URL_TEST + URL_GET_STATUS_BY_TRANSACTION_ID;


/*-----------------------------    REST section    ---------------------------------*/

ravKavRouter.post('/', async (req, res) => {
    let ravKavRes;
    await axios({
        method: 'post',
        url: TestSessionUrl,
        crossDomain: true,
        headers: { "Authorization": TOKEN_TEST, 'Access-Control-Allow-Origin': '*', }
    }).then(async res => {
        ravKavRes = res.data
    })

    res.send({ ravKavRes, msg: 'success' })
})

ravKavRouter.post('/getProducts', async (req, res) => {
    let ravKavRes;
    await axios({
        method: 'post',
        url: TestGetProductsUrl,
        crossDomain: true,
        headers: {
            'Authorization': TOKEN_TEST,
            'Access-Control-Allow-Origin': '*',
            'X-Ravkav-Location': req.params.location || '31.783559, 35.218986',
            'Content-Type': 'application/json',
            'dataType': 'json',
            'processData': 'false',
        },
        data: JSON.stringify({dump: req.body})
    }).then(async res => {
        ravKavRes = res.data
    }).catch(e => {
        throw new Error(e);
    });
    res.send({ ravKavRes, msg: 'success' })

})



module.exports.ravKavRouter = ravKavRouter;