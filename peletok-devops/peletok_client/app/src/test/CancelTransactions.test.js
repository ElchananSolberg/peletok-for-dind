import React from 'react';
import ReactDOM from 'react-dom';
import {CancelTransactions} from '../Components/CancelTransactions/CancelTransactions'
import { shallow } from 'enzyme';
import {config} from "../utils/Config";
import axios from 'axios';
import { LanguageManager as LM } from '../Components/LanguageManager/Language';
import { Notifications } from '../Components/Notifications/Notifications';
import MockAjaxResponse from './mockAjsx'

    jest.mock('axios');
   let transactions = [
            {
                id: 1,
                date: '22/12/79',
                time: '',
                businessName: '',
                customerNumber: '',
                agentName: '',
                agentNumber: '',
                customerPrice: '',
                businessPirce: '',
                supplier: '',
                product: '',
                action: '',
                phone: '',
                requestDate: '',
                requestTime: '',
                requesterName: '',
                parentTransactionId: 97

            },{
                id: 2,
                date: '22/12/79',
                time: '',
                businessName: '',
                customerNumber: '',
                agentName: '',
                agentNumber: '',
                customerPrice: '',
                businessPirce: '',
                supplier: '',
                product: '',
                action: '',
                phone: '',
                requestDate: '',
                requestTime: '',
                requesterName: '',
                parentTransactionId: 98

            },
            {
                id: 3,
                date: '22/12/79',
                time: '',
                businessName: '',
                customerNumber: '',
                agentName: '',
                agentNumber: '',
                customerPrice: '',
                businessPirce: '',
                supplier: '',
                product: '',
                action: '',
                phone: '',
                requestDate: '',
                requestTime: '',
                requesterName: '',
                parentTransactionId: 99

            },
          
        ]

        const suppliers = {"prepaid":[
                               {"name":"פלאפון","id":95}
                              ,{"name":"סלקום","id":94}
                              ,{"name":"פרטנר","id":127}
                              ,{"name":"שיחה חסויה","id":196}
                              ,{"name":"בינלאומי","id":124}
                              ,{"name":"גלובל סים","id":203}
                              ,{"name":"בזק בינלאומי 014","id":205}
                              ,{"name":"012 מובייל","id":201}
                              ,{"name":"פריפון","id":202}
                          ],
                          "bills":[
                               {"name":"תשלום חשבון חשמל","id":185}
                              ,{"name":"טעינת חשמל","id":186}
                              ,{"name":"כביש 6","id":187}
                              ,{"name":"בזק אונליין","id":194}
                              ,{"name":"רב קו","id":999}
                              ,{"name":"מנהרות הכרמל","id":192}
                              ,{"name":"תשלום לרשויות","id":193}
                          ]}


        const withOutThree = transactions.filter(t=>t.id!=3)
        const withOutTwo = transactions.filter(t=>t.id!=2)
        
         axios.get.mockImplementation(MockAjaxResponse.get)

         axios.post.mockImplementation(MockAjaxResponse.post)
  

describe('<CancelTransactions/>', () => {

  let wrapper;
  let notifications = shallow(<Notifications />);  

  beforeEach(() => {
     axios.get.mockClear() 
     wrapper = shallow(<CancelTransactions />);    
     Notifications.singleton.setState({show: false})
  });

  afterEach(() => {
    
    
  })

  describe('componentDidMount', () => {
      it('get transaction on #componentDidMount', async (done) => {    
         wrapper 
        .instance()
        .componentDidMount()
        .then(() => {
          expect(axios.get).toHaveBeenCalled();
          expect(wrapper.instance().state.transactions).toMatchObject(transactions)
          done();
        });
      });

      

      it('it should have 4 tr after getting transaction ', async (done) => {
         
         wrapper 
        .instance()
        .componentDidMount()
        .then(() => {
          wrapper.update()
          expect(wrapper.find('.trx-row')).toHaveLength(3);     
          done();
        });
      });      
  } )

  


    describe('filterRequests method' , ()=>{
      it('should have #supplier-list-select element', async (done) => {
          
          expect(wrapper.find('#supplier-list-select').children()).toHaveLength(1);
          
  
          await wrapper.find('#supplier-list-select').children().simulate('change', 'פרטנר');
          expect(axios.get).toHaveBeenCalled();
          done()
      } )

      it('should change transactions list on supllier list change', async (done) => {
         
         wrapper 
        .instance()
        .componentDidMount().then(async () => {
            expect(wrapper.instance().state.transactions).toMatchObject(transactions)
            
            await wrapper.find('#supplier-list-select').children().simulate('change', 'פרטנר');
            expect(axios.get).toHaveBeenCalled();
            await wrapper.update()
            expect(wrapper.instance().state.transactions).toMatchObject(withOutThree)
            done();
           
        } )
        
      });
    })

    describe('sendCancel', () => {
        it('should send post to the server', (done) => {
           wrapper 
            .instance()
            .componentDidMount().then(async () => {
                await wrapper.update()
                expect(wrapper.find('.cancel-btn')).toHaveLength(3);
                await wrapper.find('.cancel-btn').at(1).simulate('click')
                await wrapper.update()
                expect(axios.post).toHaveBeenCalled();
                done();
 
            })
        } )

        it('should filter delted transaction form trx table ', (done) => {
           wrapper 
            .instance()
            .componentDidMount().then(async () => {
                await wrapper.update()
                expect(wrapper.find('.cancel-btn')).toHaveLength(3);
                await wrapper.find('.cancel-btn').at(1).simulate('click')
                await wrapper.update()
                expect(axios.post).toHaveBeenCalled();
                expect(wrapper.find('.trx-row')).toHaveLength(2); 
                expect(wrapper.instance().state.transactions).toMatchObject(withOutTwo)

                done();
 
            })
        } )

        it('Should display notification if res not equal to "success"', async () => {
            await wrapper.update()
                expect(wrapper.find('.cancel-btn')).toHaveLength(3);
                await wrapper.find('.cancel-btn').at(2).simulate('click')
                await wrapper.update()
                expect(axios.post).toHaveBeenCalled();
                expect(Notifications.singleton.state.show).toBe(true)
           
        } )

    } )

    describe('No transctions found', () => {
        it('Should display message to the user', async () => {
            await wrapper.find('#supplier-list-select').children().simulate('change', 'סלקום');
            await wrapper.update()
            expect(wrapper.contains(<td colspan={wrapper.instance().tableHeaders.length} >{LM.getString('noTrxFoundToCancel')}</td>)).toBe(true)
           
        } )
    } )



});
