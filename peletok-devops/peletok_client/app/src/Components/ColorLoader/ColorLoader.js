var themes = {
    main:{
        headerNavLink:"#11425F",
        headerNavLinkSelected:"#1287CB",
        regButton:"#1287CB",
        sideBarBack:"#16577D",
        menuItemTextSelected:"#48DEDB",
        menuItemFocused:"#11425F",
        menuItemBack:"#16577D",
        balanceTitleBoxBack:"#154D6F",
        balanceTitleBoxBorder:"#406D88",
        balanceValueTextRemainder:"#9EA2AF",
        selectorBackground:"#48DEDB",
        selectorBorder:"#2BC6C3",
        tableHeaderText:"#1287CB",
        secondaryButtonBackground:"#48DEDB",
        secondaryButtonText:"#16577D",
        userAreaEditBackground:"#C3D9E1",
        grayRow:'#E5E7Ef',
        newSaveButton: "#1287CB",
    },
    neon:{
        headerNavLink:"#611E8C",
        headerNavLinkSelected:"#AD4FEA",
        regButton:"#AD4FEA",
        sideBarBack:"#8134B3",
        menuItemTextSelected:"#E555FF",
        menuItemFocused:"#611E8C",
        menuItemBack:"#712A9F",
        balanceTitleBoxBack:"#611E8C",
        balanceTitleBoxBorder:"#CA80FA",
        balanceValueTextRemainder:"#CA80FA",
        selectorBackground:"#E555FF",
        selectorBorder:"#8B1C9F",
        tableHeaderText:"#AD4FEA",
        secondaryButtonBackground:"#E555FF",
        secondaryButtonText:"#611E8C",
        userAreaEditBackground:"#E4BCFF",
        grayRow:'#E5E7Ef',
        newSaveButton: "#AD4FEA",
    },
    blue:{
        headerNavLink:"#103268",
        headerNavLinkSelected:"#3879E1",
        regButton:"#3879E1",
        sideBarBack:"#144594",
        menuItemTextSelected:"#1FC5EC",
        menuItemFocused:"#103268",
        menuItemBack:"#143B7A",
        balanceTitleBoxBack:"#103268",
        balanceTitleBoxBorder:"#72A6FA",
        balanceValueTextRemainder:"#A7FCF8",
        selectorBackground:"#1FC5EC",
        selectorBorder:"#158CA8",
        tableHeaderText:"#3879E1",
        secondaryButtonBackground:"#1FC5EC",
        secondaryButtonText:"#103268",
        userAreaEditBackground:"#B4D9E1",
        grayRow:'#E5E7Ef',
        newSaveButton: "#3879E1",
    },
    fresh:{
        headerNavLink:"#1A7E7A",
        headerNavLinkSelected:"#0DCBC8",
        regButton:"#0DCBC8",
        sideBarBack:"#1FA6A0",
        menuItemTextSelected:"#46E271",
        menuItemFocused:"#1D7471",
        menuItemBack:"#198A85",
        balanceTitleBoxBack:"#1A7E7A",
        balanceTitleBoxBorder:"#90F5F1",
        balanceValueTextRemainder:"#A7FCF8",
        selectorBackground:"#46E271",
        selectorBorder:"#308848",
        tableHeaderText:"#1A7E7A",
        secondaryButtonBackground:"#46E271",
        secondaryButtonText:"#1A7E7A",
        userAreaEditBackground:"#B8D5D4",
        grayRow:'#E5E7Ef',
        newSaveButton: "#0DCBC8",
    },
    earthly:{
        headerNavLink:"#621616",
        headerNavLinkSelected:"#CF2A2A",
        regButton:"#CF2A2A",
        sideBarBack:"#920000",
        menuItemTextSelected:"#FF7B27",
        menuItemFocused:"#6A0000",
        menuItemBack:"#7E0000",
        balanceTitleBoxBack:"#6A0000",
        balanceTitleBoxBorder:"#AF5050",
        balanceValueTextRemainder:"#E35C5C",
        selectorBackground:"#FF7B27",
        selectorBorder:"#D8702C",
        tableHeaderText:"#C44242",
        secondaryButtonBackground:"#FF7B27",
        secondaryButtonText:"#791E1E",
        userAreaEditBackground:"#EFCACA",
        grayRow:'#E5E7Ef',
        newSaveButton: "#CF2A2A",
    }
};


/**
 * loadsa color theme to root's color variables
 * @param themeName the theme to load
 */
export function ColorLoader (themeName){
    if (themeName in themes){
        let root = document.getElementsByTagName('html')[0];
        let theme = themes[themeName];
        for (let key in theme) {
            if (theme.hasOwnProperty(key)) {
                root.style.setProperty("--"+key,theme[key]);
            }
        }
    }
}