import React, { Component } from 'react';
import './Hashavshevet.css';

import { Container, Row, Col, Button, Collapse } from 'reactstrap';

import { LanguageManager as LM } from '../LanguageManager/Language';

import InputTypeFile from '../InputUtils/InputTypeFile';
import InputTypeCheckBox from '../InputUtils/InputTypeCheckBox';

import { ReportTableContent } from '../ReportComponent/ReportTableContent';
import {
    postHashavshevetImport,
    postHashavshevetShowReport
} from '../DataProvider/DataProvider'
import { objectToFormData } from '../../utilities/utilities'
import { Notifications } from '../Notifications/Notifications'
import { Loader } from '../Loader/Loader'
/** ImportReceiptsFromHashavshevet is a main component of ImportReceiptsFromHashavshevet Page.  
 * ImportReceiptsFromHashavshevet and its children are working with .css file: Hashavshevet.css - common for all Hashavshevet Reports Pages. */
class ImportReceiptsFromHashavshevet extends Component {
    constructor(props) {
        super(props);

        this.state = {
            /** working variable that indicates if a user is on the stage of viewing a report */
            viewingReport: false,
            payments: [],
            fileName: null,
            isHashavshevetId: false,
            isCredit: false,
            render: true,
        }
    }

    /** Show Report Button click handler
     * TODO: Add functionality */
    showReportClicked = () => {
        let isValid = this.selectFile.runValidation()
        if(isValid){
            let file = this.selectFile.getValue().value
            let data = objectToFormData({
                hashavshevet: file,
                isHashavshevetId: this.state.isHashavshevetId
            })
            Loader.show()
            postHashavshevetShowReport(data).then((res)=>{
                Loader.hide()
                this.setState({
                    viewingReport: true,
                    payments: res.payments,
                    fileName: res.file,
                })
            }).catch((err)=>{
                Loader.hide()
                Notifications.show(LM.getString('cantShowReport'), 'danger')
            })
        } 
        // this.setState({ viewingReport: true });
    }

    importReportClicked = () => {
        Loader.show()
        postHashavshevetImport({
            fileName: this.state.fileName,
            isHashavshevetId: this.state.isHashavshevetId,
            isCredit: this.state.isCredit,
        }).then((res)=>{
            Loader.hide()
            Notifications.show(LM.getString('reportIportedSuccessfully'), 'success')
            this.reseteAll()
        }).catch((err)=>{
            Loader.hide()
            Notifications.show(LM.getString('reportWasntImported'), 'danger')
        })
    }

    reseteAll = () => {
        this.setState({
            viewingReport: false,
            payments: [],
            fileName: null,
            isHashavshevetId: false,
            isCredit: false,
            render: false
        }, ()=>{
            this.setState({
                render: true
            })
        })
    }

    render() {
        /** Page header text */
        const header = LM.getString('importReceiptsFromHashavshevet');

        /** data for "Select a File to Receive" field (InputTypeFile InputUtils component) */
        const selectFileProps = {
            title: LM.getString("selectFileToReceive"),
            id: "ImportReceiptsFromHashavshevet_SelectFile",
            required: true,
        };

        /** data for "Credit" field (InputTypeCheckBox InputUtils component) */
        const creditProps = {
            title: LM.getString("credit"),
            id: "ImportReceiptsFromHashavshevet_Credit",
        };
        /** data for "Delek User" field (InputTypeCheckBox InputUtils component) */
        const delekUserProps = {
            title: LM.getString("delekUser"),
            id: "ImportReceiptsFromHashavshevet_DelekUser",
        };

        /** Table Header data */
        const tableHeadersObj = {
            businessName: LM.getString('sellerName'),
            receiptNumber: LM.getString('receiptNumber'),
            date: LM.getString('date'),
            hour: LM.getString('time'),
            amount: LM.getString('amount'),
        };

        /** data for Show Report button */
        const showReportButtonText = LM.getString("showReport");
        const importReportButtonText = LM.getString("importReport");
        const buttonCancel = LM.getString("cancel");
        return (
            <Container className="hashavshevetContainer">
                <Row >
                    <Col className="hashavshevetHeader">
                        {header}
                    </Col>
                </Row>
                {this.state.render && <Container className="hashavshevetSettings">
                    <Row >
                        <Col sm='4'>
                            <InputTypeFile {...selectFileProps} ref={(componentObj) => { this.selectFile = componentObj }} required={true}/>
                        </Col>
                    </Row>
                    <Row >
                        <Col sm='auto'>
                            <InputTypeCheckBox
                            {...creditProps}
                            onChange={()=>{
                                this.setState({
                                    isCredit: !this.state.isCredit
                                })
                            }}
                            />
                        </Col>
                        <Col sm='auto'>
                            <InputTypeCheckBox
                            {...delekUserProps}
                            onChange={()=>{
                                this.setState({
                                    isHashavshevetId: !this.state.isHashavshevetId
                                })
                            }}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="auto">
                            <Button className="hashavshevetButton hashavshevetButtonPosition" id="hashavshevetShowReportButton" onClick={!this.state.viewingReport ? this.showReportClicked : this.importReportClicked} > {!this.state.viewingReport ? showReportButtonText : importReportButtonText} </Button>
                        </Col>
                        {this.state.viewingReport && <Col sm='auto'>
                            <Button
                                className="hashavshevetCancelButton hashavshevetButtonPosition"
                                id="businessButtonCancel"
                                onClick={this.reseteAll}
                            >
                                {buttonCancel}
                            </Button>
                        </Col>}
                    </Row>
                </Container>}
                <Row>
                    <Col >
                        <Collapse isOpen={this.state.viewingReport} >
                            <ReportTableContent
                                tableHeadersObj={tableHeadersObj}
                                tableRows={this.state.payments}
                                reportName={"ImportReceiptsFromHashavshevet"}
                            />
                        </Collapse>
                    </Col>
                </Row>
            </Container>
        )
    }
}

export default ImportReceiptsFromHashavshevet;