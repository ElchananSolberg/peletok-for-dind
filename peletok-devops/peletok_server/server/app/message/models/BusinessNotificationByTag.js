const BaseModel = require('../../BaseModel');
const DataTypes = require("sequelize").DataTypes;

/**
 * Tag BusinessNotificationByTag
 */
class BusinessNotificationByTag extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {};
        this.prefix = "MSG";
        this.author = true;
    }
    /**
     * creates associate for BusinessNotificationByTag model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.tagOfMessage.model, { as: 'TagOfMessage' });
        this.model.belongsTo(models.business.model, { as: 'Business' });
    }
}

module.exports = new BusinessNotificationByTag();