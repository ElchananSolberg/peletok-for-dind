import React, { Component } from 'react';
import { SideMenu } from '../SideMenu/SideMenu'
import { Container, Row, Col } from 'reactstrap'
import { Route, Redirect, Switch } from 'react-router-dom';
import { ContentRouter } from "./ContentRouter";
import { LigthBox } from '../AdBanner/LigthBox'

import { HeaderBar } from '../HeaderBar/HeaderBar'
import { FooterBar } from '../FooterBar/FooterBar'
import Banner from '../AdBanner/Banner';
import './MainScreen.css'


export class MainScreen extends Component {

    render() {
        /**
         * renders a table view with a 75% space for the content and 25% for the menu
         * the switch in the first col (content) navigates through the pages and refirects the '/'
         * address to /Charge as it is the first page to encounter.
         */
        return (
            <div className={'fillScreen'}>  
                <LigthBox/>

                <div className={"main-header"}>
                    <Route path={`${this.props.match.path}:tab`} render={(routeProps =>
                        <HeaderBar authenticate={this.props.authenticate} {...routeProps} />)} />
                </div>
                <div className={'temp_background main-body pt-5'}>
                    <Container fluid className='container_restriction'>
                        <Row className={'page d-flex flex-row mx-0'}>
                            <Col className={' p-1 index col-sm-20 d-none d-md-block'}>
                                <Route path={`${this.props.match.path}:tab`} component={SideMenu} />
                            </Col>
                            <Col className={'content content-container col-sm-80 '}>
                                <Switch>
                                    <div style={{ minHeight: '580px' }} >
                                        <Redirect exact from={`${this.props.match.url}`} to={"/main"} />
                                        <Route path={`${this.props.match.path}:tab`} component={ContentRouter} />
                                    </div>
                                </Switch>
                                <div className={'mt-3'}>
                                    <Banner bannerType="footer" />
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </div>
                <div className={"main-footer"}>
                    <FooterBar />
                </div>
            </div>
        )
    }
}