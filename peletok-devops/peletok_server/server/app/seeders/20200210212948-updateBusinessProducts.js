'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(
      'UPDATE "BSN_BusinessProduct" SET is_authorized = true, final_commission = s.min_percentage_profit, business_commission = s.max_percentage_profit, percentage_profit = s.max_percentage_profit ' + 
      'from "BSN_BusinessProduct" as bp join "SUPL_Product" as p on bp.product_id = p.id join "SUPL_Supplier" as s on p.supplier_id = s.id ' +
      'WHERE "BSN_BusinessProduct"."business_id" = 1 and bp.id = "BSN_BusinessProduct"."id"',
      {type: queryInterface.sequelize.QueryTypes.UPDATE})
  },

  down: (queryInterface, Sequelize) => {
    return Promise.resolve()
  }
};
