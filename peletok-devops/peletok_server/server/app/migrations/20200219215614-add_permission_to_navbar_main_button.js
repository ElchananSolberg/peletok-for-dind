'use strict';
const {addPermission} = require('../dbUtils/addPermission');

module.exports = {
  up: (queryInterface, Sequelize) => {
   return addPermission(queryInterface, 'הצגת תפריט ראשי', 'ui', '/show_navbar_main_button', 10)
  },

  down: (queryInterface, Sequelize) => {
   return Promise.resolve()
  }
};
