import React, { Component } from 'react';
import './Hashavshevet.css';

import { Container, Row, Col, Button, Collapse, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

import { LanguageManager as LM } from '../LanguageManager/Language';

import { getSellers,
    getHashavshevetOrders,
    getHashavshevetOrdersByID,
    postHashavshevetExpoertOrder, 
    getAgentsResponsible,
    getHashavshevetByDate, 
} from '../DataProvider/DataProvider'

import InputTypeSearchList from '../InputUtils/InputTypeSearchList';
import InputTypeNumber from '../InputUtils/InputTypeNumber';
import InputTypeDate from '../InputUtils/InputTypeDate';
import InputTypeTime from '../InputUtils/InputTypeTime';
import InputTypeText from '../InputUtils/InputTypeText';
import InputTypeCheckBox from '../InputUtils/InputTypeCheckBox';
import InputTypeSimpleSelect from '../InputUtils/InputTypeSimpleSelect';
import { ReportTableContent } from '../ReportComponent/ReportTableContent';
import { Notifications } from '../Notifications/Notifications'
import { Loader } from '../Loader/Loader'
/** ExportOrdersToHashavshevetReport is a main component of ExportOrdersToHashavshevetReport Page.  
 * ExportOrdersToHashavshevetReport and its children are working with .css file: Hashavshevet.css - common for all Hashavshevet Reports Pages. */
class ExportOrdersToHashavshevetReport extends Component {
    constructor(props) {
        super(props);

        this.state = {
            render: true,

            ordersByDate: [],
            viewOrdersByDate: false,
            exportedOrderId: null,
            sellers: [],
            viewingReport: false,
            viewingOrderDetails: false,
            transactionsGroups: [],
            transactions: [],
            modal: false,
            agents: [],
        }
        this.selectedBusiness = []
    }


    /** Show Report Button click handler
     * TODO: Add functionality */
    showReportClicked = (e, exportedOrder = null) => {
        
        let isStartingDate = this.startingDate.runValidation()
        let isEndingDate = this.endingDate.runValidation()
        if (isStartingDate && isEndingDate){
            if(!this.isOrdersByDate() || exportedOrder){
                if(exportedOrder){
                    this.setState({
                        exportedOrderId: exportedOrder.id
                    })
                }
                let params = !exportedOrder ? {
                    ...(this.agent.getValue().value ? {agentId: this.agent.getValue().value} : {}),
                    ...(this.customerNumber.getValue().value ? {customerNumber: this.customerNumber.getValue().value} : {}),
                    ...(this.section20.getValue().value ? {section20: this.section20.getValue().value} : {}),
                    ...(this.isReportOnly() ? {isReportOnly: true} : {}),
                    ...(this.isCommissionExport() ? {isCommissionExport: true} : {}),
                    businessId: this.selectSeller.getValue().value || 0,
                    startDate: `${this.startingDate.getValue().value} ${this.startingTime.getValue().value}`,
                    endDate: `${this.endingDate.getValue().value} ${this.endingTime.getValue().value}`
                } : {
                    exportedOrdertId: exportedOrder.id
                }
                Loader.show()
                getHashavshevetOrders(params).then((res)=>{
                    this.setState({
                        viewingReport: true,
                        transactionsGroups: res
                    });
                    this.selectedBusiness = res.map(t=>t.business_id)
                    Loader.hide()
                })
            } else {
                let params = {
                    startDate: `${this.startingDate.getValue().value} ${this.startingTime.getValue().value}`,
                    endDate: `${this.endingDate.getValue().value} ${this.endingTime.getValue().value}`
                }
                Loader.show()
                getHashavshevetByDate(params).then((res)=>{
                    this.setState({
                        viewOrdersByDate: true,
                        ordersByDate: res
                    });
                    Loader.hide()
                })
            }
        }
    }

    isOrdersByDate = () => {
        return this.props.match.params.type == 'orderByDate';
    }

    isReportOnly = () => {
        return this.props.match.params.type == 'report' || this.props.match.params.type == 'CommissionReport';
    }
    

    isCommissionExport = () => {
        return this.props.match.params.type == 'CommissionExport' || this.props.match.params.type == 'CommissionReport';
    }

    /** For Export checkbox change handler
    * TODO: Add functionality */
    forExportChanged = (e,item) => {
        
        if(e.target.checked){
            this.selectedBusiness.push(item.business_id)
        }else{
            this.selectedBusiness = this.selectedBusiness.filter(id=>id!=item.business_id)
        }

    }

    /** Orders Table Row click handler
     * TODO: Add functionality: 
     *                          - due to e.target.parentElement.id send a GET request
     *                          - fill the Order Details Table with the data from the response (setState({transactions:res}) ).
     */
    trClicked = (e, item) => {
        if (['TD', 'TR'].includes(e.target.tagName)) {
            let isBusinessValid;
            let isStartingDate;
            let isEndingDate;
            if(!this.state.exportedOrderId){
                isBusinessValid = this.selectSeller.runValidation()
                isStartingDate = this.startingDate.runValidation()
                isEndingDate = this.endingDate.runValidation()
            }
            if (this.state.exportedOrderId || (isBusinessValid && isStartingDate && isEndingDate)){

                let businessId = item.business_id;
                let params = !this.state.exportedOrderId ? {
                    ...(this.agent.getValue().value ? {agentId: this.agent.getValue().value} : {}),
                    ...(this.customerNumber.getValue().value ? {customerNumber: this.customerNumber.getValue().value} : {}),
                    ...(this.section20.getValue().value ? {section20: this.section20.getValue().value} : {}),
                    ...(this.isReportOnly() ? {isReportOnly: true} : {}),
                    ...(this.isCommissionExport() ? {isCommissionExport: true} : {}),
                    startDate: `${this.startingDate.getValue().value} ${this.startingTime.getValue().value}`,
                    endDate: `${this.endingDate.getValue().value} ${this.endingTime.getValue().value}`
                } : {exportedOrdertId: this.state.exportedOrderId};
                Loader.show()
                getHashavshevetOrdersByID(businessId, params).then((res)=>{
                    this.setState({
                        viewingOrderDetails: true,
                        transactions: res
                    });
                    Loader.hide()
                })
            }
            e.persist();
        }
    }

    /** Export Orders Button click handler
     * TODO: Add functionality */
    exportOrdersClicked = () => {
         if(this.selectedBusiness && this.selectedBusiness.length > 0){
            this.setState({ modal: !this.state.modal });
        }else{
            Notifications.show(LM.getString('youDidntSelectItemToExport'), 'danger')
        }
        
    }
    toggleExportOrdersModal = () => {
        this.setState({ modal: !this.state.modal });
    }
    
    /** Export Orders Button inside Export Orders Modal window click handler
     * TODO: Add functionality */
    finalExportOrdersClicked = () => {
        let isStartingDate;
        let isEndingDate;
        let hasFileName;
        if(!this.state.exportedOrderId){
            isStartingDate = this.startingDate.runValidation()
            isEndingDate = this.endingDate.runValidation()
            hasFileName = this.exportOrdersFileName.runValidation()
        }
        if (this.state.exportedOrderId || (isStartingDate && isEndingDate && hasFileName)){

            
            let params = !this.state.exportedOrderId ? {
                ...(this.agent.getValue().value ? {agentId: this.agent.getValue().value} : {}),
                ...(this.customerNumber.getValue().value ? {customerNumber: this.customerNumber.getValue().value} : {}),
                ...(this.section20.getValue().value ? {section20: this.section20.getValue().value} : {}),
                ...(this.isCommissionExport() ? {isCommissionExport: true} : {}),
                businessIds: this.selectedBusiness,
                startDate: `${this.startingDate.getValue().value} ${this.startingTime.getValue().value}`,
                endDate: `${this.endingDate.getValue().value} ${this.endingTime.getValue().value}`,
                fileName: this.exportOrdersFileName.getValue().valule
            } : {
                businessIds: this.selectedBusiness,
                exportedOrdertId: this.state.exportedOrderId
            };
            Loader.show()
            postHashavshevetExpoertOrder(params).then((res)=>{
                // window.open('data:Application/octet-stream,' + encodeURIComponent(res), "_blank");
                var uri = 'data:Application/octet-stream,' + encodeURIComponent(res);
                var link = document.createElement('a');
                link.setAttribute("download", this.exportOrdersFileName.getValue().value + '.xml');
                link.setAttribute("href", uri);
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(document.body.lastChild);
                this.setState({
                    transactionsGroups: [],
                    transactions: [],
                    viewingOrderDetails: false,
                    exportedOrderId: null,
                })
                this.selectedBusiness = []
                this.toggleExportOrdersModal();
                Loader.hide()
            })
        }

    }

    componentDidMount() {
        getSellers(true)
            .then(
                res => {
                    this.setState({ sellers: res });
                }
            )
            .catch((err) => alert(err));
        getAgentsResponsible().then(res=>{
            this.setState({
                agents: res
            })
        })
    }

    componentDidUpdate(prevProps) {
        if (this.props.match.params.type !== prevProps.match.params.type) {
            this.onRouteChanged();
        }
    }

    onRouteChanged() {

        this.setState({
            render: false,
            viewingReport: false,
            viewingOrderDetails: false,
            transactionsGroups: [],
            transactions: [],
            ordersByDate: [],
            viewOrdersByDate: false,
            exportedOrderId: null,
            modal: false,
        }, () => {
            this.setState({
                render: true
            })
        })
    }


    render() {
        /** Payments Header  */
        const header = LM.getString(this.props.match.params.type || 'exportOrdersToHashavshevetReport');

        /** data for "Select Seller" field (InputTypeSearchList InputUtils component) */
        const selectSellerProps = {
            title: LM.getString("selectSeller"),
            id: "ExportOrdersToHashavshevetSelectSeller",
        };

        const section20Props = {
            title: LM.getString("section20") + ':',
            id: "ExportOrdersToHashavshevetsection20",
        };

        /** data for "Agent" field (InputTypeNumber InputUtils component) */
        const agentProps = {
            title: LM.getString("agent"),
            id: "OrdersExportedToHashavshevet_Agent",
        };
        /** data for "Terminal Number" field (InputTypeNumber InputUtils component) */
        const terminalNumberProps = {
            title: LM.getString("terminalNumber"),
            id: "ExportOrdersToHashavshevetTerminalNumber",
        };

        /** data for "Starting Date" field (InputTypeDate InputUtils component) */
        const startingDateProps = {
            title: LM.getString("startingDate"),
            id: "ExportOrdersToHashavshevetStartingDate",
        };
        /** data for "Ending Date" field (InputTypeDate InputUtils component) */
        const endingDateProps = {
            title: LM.getString("endingDate"),
            id: "ExportOrdersToHashavshevetEndingDate",
        };

        /** data for "Starting Time" field (InputTypeDate InputUtils component) */
        const startingTimeProps = {
            title: LM.getString("startingTime"),
            id: "ExportOrdersToHashavshevetStartingTime",
        };
        /** data for "Ending Time" field (InputTypeDate InputUtils component) */
        const endingTimeProps = {
            title: LM.getString("endingTime"),
            id: "ExportOrdersToHashavshevetEndingTime",
        };

        /** data for "File Name" field (InputTypeFile InputUtils component) */
        const fileNameProps = {
            required: true,
            title: LM.getString("fileName") + ":",
            id: "ExportOrdersToHashavshevetFileName",
        };

        /** Table Header data */
        const tableHeadersObj = {
            forExport: LM.getString('forExport'),
            customerName: LM.getString('businessName'),
            customerNumber: LM.getString('customerNumber'),
            hashavshevetCustomerCode: LM.getString('hashavshevetCustomerCode'),
            totalPayments: LM.getString('totalPayment2'),
            balance: LM.getString('balance2'),
            vat: LM.getString('vat'),
            earliestDateInOrder: LM.getString('earliestDateInOrder'),
            latestDateInOrder: LM.getString('latestDateInOrder'),
        };
        /** Order Details Table Header data */
        const orderDetailsTableHeadersObj = {
            itemName: LM.getString('itemName'),
            hashavshevetItemNumber: LM.getString('hashavshevetItemNumber'),
            price: !this.isCommissionExport() ? LM.getString('pricePerUnit') : LM.getString('price'),
            quantity: LM.getString('quantity'),
            total2: LM.getString('total2'),
        };

        const ordersByDateTableHeadersObj = {
            fileName: LM.getString('fileName'),
            date: LM.getString('exportDate'),
            
        };

        /** data for Show Report button */
        const showReportButtonText = LM.getString("showReport");
        /** data for Export Orders button */
        const exportOrdersButtonText = LM.getString("exportOrders");
        /** data for Cancel button */
        const cancelButtonText = LM.getString("cancel");
        /** data for Orders sub header */
        const ordersHeaderText = LM.getString('orders');

        const ordersHeaderTextByDate = LM.getString('ordersByDate');
        /** data for Order Details sub header */
        const orderDetailsHeaderText = LM.getString('orderDetails');

        return (
            this.state.render ? <Container className="hashavshevetContainer">
                <Row >
                    <Col className="hashavshevetHeader">
                        {header}
                    </Col>
                </Row>
                <Container className="hashavshevetSettings">
                    {!this.isOrdersByDate() && <Row>
                        <Col sm='4'>
                            <InputTypeSimpleSelect {...selectSellerProps}
                                options={[{label: LM.getString('all'), value: '0'},...this.state.sellers.map(seller =>{return {value: seller.id, label: seller.name}})]}
                                ref={(componentObj) => { this.selectSeller = componentObj }} />
                        </Col> <Col sm='4'>
                            <InputTypeCheckBox {...section20Props}
                                ref={(componentObj) => { this.section20 = componentObj }} />
                        </Col>
                    </Row>}
                    {!this.isOrdersByDate() && <Row >
                        <Col sm='4'>
                            <InputTypeSimpleSelect
                                {...agentProps}
                                options={this.state.agents.map(seller =>{return {value: seller.id, label: seller.name}})}
                                ref={(componentObj) => { this.agent = componentObj }}
                                />
                        </Col>
                        <Col sm='4'>
                            <InputTypeNumber {...terminalNumberProps} ref={(componentObj) => { this.customerNumber = componentObj }} />
                        </Col>
                    </Row>}
                    <Row >
                        <Col sm='4'>
                            <InputTypeDate {...startingDateProps} ref={(componentObj) => { this.startingDate = componentObj }} />
                        </Col>
                        <Col sm='4'>
                            <InputTypeTime {...startingTimeProps} ref={(componentObj) => { this.startingTime = componentObj }} />
                        </Col>
                    </Row>
                    <Row >
                        <Col sm='4'>
                            <InputTypeDate {...endingDateProps} ref={(componentObj) => { this.endingDate = componentObj }} />
                        </Col>
                        <Col sm='4'>
                            <InputTypeTime {...endingTimeProps} ref={(componentObj) => { this.endingTime = componentObj }} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="auto">
                            <Button className="hashavshevetButton" id="hashavshevetShowReportButton" onClick={this.showReportClicked} > {showReportButtonText} </Button>
                        </Col>
                    </Row>
                </Container>
                <Collapse isOpen={this.state.viewOrdersByDate} >
                    <Row >
                        <Col className="subHeader">
                            {ordersHeaderTextByDate}
                        </Col>
                    </Row>
                    <Row>
                        <Col >
                            <ReportTableContent
                                tableHeadersObj={ordersByDateTableHeadersObj}
                                tableRows={this.state.ordersByDate}
                                reportName={"order_by_date"}
                                trClickedWithObject={this.showReportClicked}
                            />
                        </Col>
                    </Row>
                </Collapse>
                <Collapse isOpen={this.state.viewingReport} >
                    <Row >
                        <Col className="subHeader">
                            {ordersHeaderText}
                        </Col>
                    </Row>
                    <Row>
                        <Col >
                            <ReportTableContent
                                tableHeadersObj={tableHeadersObj}
                                tableRows={this.state.transactionsGroups}
                                reportName={"hashavsevet_export"}
                                trClickedWithObject={this.trClicked}
                                cehckeBoxChangedWithObject={this.forExportChanged}
                            />
                        </Col>
                    </Row>
                    {!this.isReportOnly() && <Row>
                        <Col sm="auto">
                            <Button className="hashavshevetButton" id="hashavshevetExportOrdersButton" onClick={this.exportOrdersClicked} > {exportOrdersButtonText} </Button>
                        </Col>
                        <Modal isOpen={this.state.modal} toggle={this.toggleExportOrdersModal} >
                            <ModalHeader className="hashavshevetModalHeader" toggle={this.toggle}>{exportOrdersButtonText}</ModalHeader>
                            <ModalBody className="hashavshevetModalBody" >
                                {this.state.exportedOrderId && <Row>
                                    <Col >
                                       {LM.getString("reExportAttantion")}
                                    </Col>
                                </Row>}
                                <Row>
                                    <Col >
                                        <InputTypeText {...fileNameProps} ref={(componentObj) => { 
                                            this.exportOrdersFileName = componentObj; 
                                            if(componentObj){
                                                componentObj.setValue("ExportOrdersToHashavshevetReport_" + Date.now()) 
                                            }
                                        }} />
                                    </Col>
                                </Row>
                            </ModalBody>
                            <ModalFooter className="hashavshevetModalFooter">
                                <Row>
                                    <Col sm="auto">
                                        <Button className="hashavshevetButton" onClick={this.finalExportOrdersClicked}>{exportOrdersButtonText}</Button>{' '}
                                    </Col>
                                    <Col sm="auto">
                                        <Button className="hashavshevetButton hashavshevetButtonCancel" onClick={this.toggleExportOrdersModal}>{cancelButtonText}</Button>
                                    </Col>
                                </Row>
                            </ModalFooter>
                        </Modal>
                    </Row>}
                </Collapse>
                <Collapse isOpen={this.state.viewingReport && this.state.viewingOrderDetails} >
                    <Row >
                        <Col className="subHeader">
                            {orderDetailsHeaderText}
                        </Col>
                    </Row>
                    <Row>
                        <Col >
                            <ReportTableContent
                                tableHeadersObj={orderDetailsTableHeadersObj}
                                tableRows={this.state.transactions}
                                reportName={"Order_details"}
                            />
                        </Col>
                    </Row>
                </Collapse>
            </Container> : <></>
        )
    }
}

export default ExportOrdersToHashavshevetReport