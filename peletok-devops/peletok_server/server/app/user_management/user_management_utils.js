const models = require('../index');
const user = models.user;
const userModel = user.model;
const userPassword = models.userPassword;
const userPasswordModel = userPassword.model;
const userContactModel = models["userContact"].model;
const userContact = models.userContact;
const address = models.address
const userAddress = models.userAddress
const addressModel = address.model
const userAddressModel = userAddress.model
var { getNumStatus, compareHash, addHashWithSalt } = require('../utils');
const moment = require('moment')
const { Info } = require("../mapping/crudMiddleware");
const phone_type = 1
const mail_type = 2
const userBusinessModel = require("../index").userBusiness.model;
const { Op } = require('sequelize');
const { get, wrapperUpdate } = require("../mapping/crudMiddleware");
const LM = require('../config/LanguageManager').LM;
const sequelize = require('sequelize');
const reportView = require("../transaction/view/ReportView");
const reportViewModel = reportView.model
let compatibleCodeStatus = 1
let codeExpiredStatus = -1
let WrongCodeStatus = -2


/**
 * Gets business information (credit balance,name, identifier, etc..)
 * @param {*} req 
 * @param {*} res 
 */
async function getUserInformation(req, res) {
    let mappingPath = "business";
    let filter = { id: req.user.business_id };
    let info = await get(mappingPath, filter);
    let frame = info.result[0]['frame']
    let currentBalance = info.result[0]['balance']
    let points = info.result[0]['points']
    let business_identifier = info.result[0]['business_identifier']
    let status = info.result[0]['status']
    let address =  await models.address.model.findOne({where:{id:info.result[0]['Address.id']},attributes:[[sequelize.fn("concat",
    sequelize.col("country"), ' ', sequelize.col("city"),' ',sequelize.col("street"),' ',sequelize.col("number"),' ',sequelize.col("entrance"),' ',sequelize.col("floor")), "address"]]})
    let creditBalance = [
        { strRepr: LM.getString("frame"), value: frame },
        { strRepr: LM.getString("balance"), value: currentBalance },
        { strRepr: LM.getString("creditBalance"), value: frame + currentBalance },
        { strRepr: LM.getString("pointsBalance"), value: points }]
    let user = await userModel.findOne({ where: { id: req.user.id } })
    let userLoginName = user['login_name']
    let useDetails = { id: user['id'], loginName: userLoginName, businessName: info.result[0]['name'], business_identifier: business_identifier,address:address, use_point: info.result[0]['use_point'], status: status }
    info.result = { balance: creditBalance, details: useDetails }
    res.customSend(info);
}

/**
 * Creates user by username and password
 * @param userName: string for login_name field
 * @param password: string for password field
 * @param israeliId: string of israeli id
 */
async function createUser(userName, password, israeliId) {
    let newUser = await userModel.create({
        login_name: userName, israeli_id: israeliId,
    }).catch(
        function (err) {
            console.log(err.toString());
            return false
        });

    if (newUser) {
        userPasswordModel.create(
            {
                password: password,
                status: 1,
                user_id: newUser.id
            }).catch(
                function (err) {
                    console.log(err.toString())
                })
    }
}


/**
 * 
 * @param {*} req 
 * @param {*} res 
 * update user by req body
 */
async function updateUser(req, res) {
    await wrapperUpdate(req, res, null, req.body, "user");
}


/**
 * Creates user and userPassword by object as key: value
 * @param objectValues: object as key: value
 */

/**
 * 
 * @param {*} req 
 * @param {*} res 
 * Checks whether the password received from the
 *  request is the same as the password in the database
 *  and updates the new password in the database
 */
async function changePassword(req, res) {
    //TODO- add admin permission check
    let tempUser = await userModel.findOne({ where: { id: req.params.id } })
    if (tempUser) {
        let dbOldPasswordRow = await userPasswordModel.findOne({ where: { user_id: req.params.id, status: { [Op.or]: [getNumStatus('active', 'UserPassword'), null] } } })
        let password = dbOldPasswordRow['password']
        if (await compareHash(req.body.old_password, password)) {
            await dbOldPasswordRow.update({ status: getNumStatus('inactive', 'UserPassword') });
            await userPasswordModel.create({
                password: req.body.new_password,
                user_id: req.params.id,
                status_author_id: req.user.id,
                status: getNumStatus('active', 'UserPassword'),
                status_timestamp: moment().toDate(),
                ip: req.headers['x-forwarded-for'] || req.connection.remoteAddress
            });
        } else {
            res.status(403).send(LM.getString("wrongPassErrMsg"))
            return
        }
    } else {
        res.status(400).send(LM.getString("noUserErrMsg"))
    }

    res.status(200).send(LM.getString("success"))


}

/**
 * Change password of connection user
 * @param req request object
 * @param res response object
 * @returns {Promise<void>}
 */
async function changePasswordForConnectedUser(req, res) {
    let info = new Info();
    let currentPassword;
    await userPasswordModel.findAll({ where: { user_id: req.user.id }, attributes: ["id", "password"] }).then(async r => {
        currentPassword = r[0] ? r[0].password : null;
        if (!currentPassword) throw Error(LM.getString("authFailed"));
        let matchPassword = await compareHash(req.body.current_password, currentPassword);
        if (!matchPassword) throw Error(LM.getString("wrongPassword"));
        await r[0].destroy();
        await userPasswordModel.create({
            user_id: req.user.id, password: req.body.new_password, author: req.user.id,
            status_author: req.user.id,
            status: getNumStatus('active', 'UserPassword'),
            status_timestamp: moment().toDate(),
            ip: req.headers['x-forwarded-for'] || req.connection.remoteAddress
        }).catch(e => {
            info.errors.push(e)
        });
    }).catch(e => {
        info.errors.push(e)
    });
    res.customSend(info);
}

/**
 * Change password of different user
 * @param req request object
 * @param res response object
 * @returns {Promise<void>}
 */
async function changePasswordForDifferentUser(req, res) {
    let info = new Info();
    let currentPassword;
    await userPasswordModel.findAll({ where: { user_id: req.body.user_id }, attributes: ["id", "password"] }).then(async r => {
        currentPassword = r[0] ? r[0].password : null;
        if (!currentPassword) throw Error(LM.getString("authFailed"));
        let matchPassword = await compareHash(req.body.current_password, currentPassword);
        if (!matchPassword) throw Error(LM.getString("wrongPassword"));
        await r[0].destroy();
        await userPasswordModel.create({
            user_id: req.body.user_id, password: req.body.new_password, author: req.user.id,
            status_author: req.user.id,
            status: getNumStatus('active', 'UserPassword'),
            status_timestamp: moment().toDate(),
            ip: req.headers['x-forwarded-for'] || req.connection.remoteAddress
        }).catch(e => {
            info.errors.push(e)
        });
    }).catch(e => {
        info.errors.push(e)
    });
    res.customSend(info);
}


async function createUserFromBody(objectValues) {
    var errorMessage;
    let newUser = await userModel.create(objectValues, { fields: user.getFields() }).catch(
        function (err) {
            errorMessage = err.toString();
            return false
        });

    if (!newUser) {
        return errorMessage;
    }
    objectValues["user_id"] = newUser.id;

    await userPasswordModel.create(
        objectValues, { fields: userPassword.getFields() }).catch(
            function (err) {
                errorMessage = err.toString();
            });
    return errorMessage;
}


/***
 * Returns phone number of user from DB bu username
 * @param username string of username
 * @returns {Promise<phone number or empty string>}
 */
async function getPhoneNumber(username) {
    //TODO: create getIndex function
    // var indexPhoneType = getIndex(userContactModel.name, "phoneNumber");
    const phoneNumber = await userModel.findOne(
        {
            raw: true,
            where: { login_name: username }, attributes: [],
            include: {
                model: userContactModel,
                as: 'UserContact',
                attributes: ["value"],
                where: { type: phone_type }
            }
        });
    return phoneNumber ? phoneNumber["UserContact.value"] : ''
}

/***
 * Returns true if username is in the DB
 * @param username string of username
 * @returns {Boolean<if user exictes>}
 */
async function isUserExist(username) {
    const user = await userModel.findOne(
        {
            where: { login_name: username }
        });
    return user !== null
}


/***
 * Returns email of user from DB bu username
 * @param username string of username
 * @returns {Promise<email or empty string>}
 */
async function getUserEmail(username) {
    const getEmail = await userModel.findOne(
        {
            raw: true,
            where: { login_name: username }, attributes: [],
            include: {
                model: userContactModel,
                as: 'UserContact',
                attributes: ["value"],
                where: { type: mail_type }
            }
        });
    return getEmail ? getEmail["UserContact.value"] : ''
}

/**
 * get history of all password changes of user
 * @param {*} req 
 * @param {*} res 
 */
async function getPasswordChangHistory(req, res) {
    let passwords = await userModel.findAll({
        raw: true,
        as: "User",
        where: {
            "$UserBusiness.business_id$": req.user.business_id,
            id: req.user.id
        },
        attributes: ["id", 'login_name'],
        include: [
            {
                model: userPasswordModel,
                as: 'UserPassword',
                paranoid: false,
                where: { deleted_at: { [Op.ne]: null } },
                attributes: ['deleted_at', 'id', 'ip']
            },
            {
                model: userBusinessModel,
                as: 'UserBusiness',
                attributes: ["id", "business_id"]
            }]
    });

    if (passwords) {
        res.send(passwords)
    } else {
        res.status(401).send(LM.getString("failedErrMsg"))
    }
}

/***
 * Deletes user document
 * @param req req object
 * @param res res object
 * @return {Promise<void>}
 */
async function deleteUserDocument(req, res) {
    const userDocumentModel = models.userDocument.model;
    await userDocumentModel.destroy({ where: { id: req.params.id }, individualHooks: true }).then(() => {
        res.status(200).send(LM.getString("success"));
    }
    ).catch(e => {
        //TODO: Add info abuot error
        logger.error(e);
        res.status(500).send(LM.getString("failedErrMsg"));
    })
}
/**
 * compare between DB and client incoming verification code
 * @param {*} loginName 
 * @param {*} verificationCode 
 * @returns {string} code status
 */
async function checkVerificationCode(loginName, verificationCode) {
    let currentUser = await models.user.model.findOne({
        where: { login_name: loginName },
        include: [
            {
                model: models.userPassword.model,
                as: 'UserPassword',
                attributes: ['verification_code', 'verification_code_expiry']
            }
        ]
    })
    if (currentUser.UserPassword[0].verification_code_expiry <= moment()) {
        return codeExpiredStatus
    } else if (currentUser.UserPassword[0].verification_code_expiry >= moment() && verificationCode == currentUser.UserPassword[0].verification_code) {
        return compatibleCodeStatus
    } else {
        return WrongCodeStatus
    }
}
/**
 * get all user with business balance update permission under user business_id
 * @param {*} req 
 * @param {*} res 
 */
async function getBalanceUpdateApprovers(req, res) {
    let approvers = await models.user.model.findAll({
        include: [{
            model: models.role.model,
            attributes: [],
            include: [{
                model: models.permission.model,
                where: {
                    path: 'GET /balance_approval',
                },
                attributes: [],
                required: true,
                through: {
                    where: { action: 'edit' },
                    attributes: [],

                }
            }],
            required: true
        }, {
            model: models.userBusiness.model,
            as: 'UserBusiness',
            where: { business_id: req.user.business_id },
            attributes: [],
            required: true
        }],

        attributes: ['id', [sequelize.fn("concat", sequelize.col("first_name")," ", sequelize.col("last_name")), 'name']]
    })
    res.send(approvers)
}

/**
 * 
 * @param {*} req 
 * @param {*} res 
 * 
 */
async function changePasswordWithRycoveryCode(req, res) {
    let tempAuth = await checkVerificationCode(req.query.username, req.query.verificationCode);
    if (tempAuth == compatibleCodeStatus) {
        let user = await userModel.findOne({ where: { "login_name": req.query.username } }).catch(err => { console.log(err); res.status(400).send(LM.getString("failedErrMsg")) })
        let dbOldPasswordRow = await userPasswordModel.findOne({ where: { user_id: user.id, status: { [Op.or]: [getNumStatus('active', 'UserPassword'), null] } } }).catch(err => { console.log(err); res.status(400).send(LM.getString("failedErrMsg")) })
        await dbOldPasswordRow.update({ status: getNumStatus('inactive', 'UserPassword') }).catch(err => { console.log(err); res.status(400).send(LM.getString("failedErrMsg")) })
        await userPasswordModel.create({
            password: req.body.new_password,
            user_id: user.id,
            status_author_id: user.id,
            status: getNumStatus('active', 'UserPassword'),
            status_timestamp: moment().toDate(),
            ip: req.headers['x-forwarded-for'] || req.connection.remoteAddress
        }).catch(err => { console.log(err); res.status(400).send(LM.getString("failedErrMsg")) })
        res.status(200).send(LM.getString("success"))
    } else {
        res.status(400).send(LM.getString("failedErrMsg"))
    }
}


/**
 * 
 * @param {*} req 
 * @param {*} res 
 * 
 */
async function changePasswordWithRycoveryCode(req, res) {
    let tempAuth = await checkVerificationCode(req.query.username, req.query.verificationCode);
    if (tempAuth == compatibleCodeStatus) {
        let user = await userModel.findOne({ where: { "login_name": req.query.username } }).catch(err => { console.log(err); res.status(400).send(LM.getString("failedErrMsg")) })
        let dbOldPasswordRow = await userPasswordModel.findOne({ where: { user_id: user.id, status: { [Op.or]: [getNumStatus('active', 'UserPassword'), null] } } }).catch(err => { console.log(err); res.status(400).send(LM.getString("failedErrMsg")) })
        await dbOldPasswordRow.update({ status: getNumStatus('inactive', 'UserPassword') }).catch(err => { console.log(err); res.status(400).send(LM.getString("failedErrMsg")) })
        await userPasswordModel.create({
            password: req.body.new_password,
            user_id: user.id,
            status_author_id: user.id,
            status: getNumStatus('active', 'UserPassword'),
            status_timestamp: moment().toDate(),
            ip: req.headers['x-forwarded-for'] || req.connection.remoteAddress
        }).catch(err => { console.log(err); res.status(400).send(LM.getString("failedErrMsg")) })
        res.status(200).send(LM.getString("success"))
    } else {
        res.status(400).send(LM.getString("failedErrMsg"))
    }
}

async function getUser30LastActions(userID){
    //TODO - find out which value to calculate (sum,payment,etc..)
    return await models.transaction.model.sum('price', { where: { created_at: { [Op.between]: [moment().subtract(30, 'days').toDate(),moment().toDate()] },user_id:userID, status:3 } })
}

async function getUsersRole(userID) {
    let user_role = await models.role.model.scope({ method: ['userRoles', userID]}).findOne()    
    return user_role ? {id: user_role["id"], name: user_role["name"]} : null
}

async function checkIfModelFiledExists(model, field, value){
    return (await models[model].model.count({where: {[field]: value}})) > 0   
}

module.exports.createUsers = createUser;
module.exports.createUserFromBody = createUserFromBody;
module.exports.getPhoneNumber = getPhoneNumber;
module.exports.isUserExist = isUserExist;
module.exports.getUserEmail = getUserEmail;
module.exports.updateUser = updateUser;
module.exports.changePassword = changePassword;
module.exports.getPasswordChangHistory = getPasswordChangHistory;
module.exports.getUserInformation = getUserInformation;
module.exports.deleteUserDocument = deleteUserDocument;
module.exports.changePasswordForConnectedUser = changePasswordForConnectedUser;
module.exports.changePasswordForDifferentUser = changePasswordForDifferentUser;
module.exports.checkVerificationCode = checkVerificationCode;
module.exports.changePasswordWithRycoveryCode = changePasswordWithRycoveryCode;
module.exports.getBalanceUpdateApprovers = getBalanceUpdateApprovers;
module.exports.getUser30LastActions = getUser30LastActions;
module.exports.getUsersRole = getUsersRole;
module.exports.checkIfModelFiledExists = checkIfModelFiledExists;
