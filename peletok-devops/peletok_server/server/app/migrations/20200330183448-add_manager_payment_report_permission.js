'use strict';
const {addPermission, renamePermission} = require('../dbUtils/addPermission');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      renamePermission(queryInterface, '/reports/reports/emailReports', '/reports/reports/19')
    ])
  },

  down: (queryInterface, Sequelize) => {
   return Promise.resolve()
  }
};
