const models = require('../../index')
const sequelize = require('../../setupSequelize')
const request = require('supertest');
// const routing = require('../../routing')
const {creditCardRouter} = require('../../api/creditCardRouter')
const Product = models.product.model;
const bodyParser = require('body-parser');
var express = require('express');
const {getTypeNum, getNumStatus} = require('../../utils')
const {fileNameFormat} = require('./testUtil')
const moment = require('moment')
const path = require("path");


let truncateModel = (model) => {
       return sequelize.getQueryInterface().bulkDelete(model.getTableName(),{},{
           truncate: true,
           cascade: true,
       }, model)
}






    

    describe(' /creditCardRouter', () => {

            let params;
            let current_user;
            let business;
            let app;

            

            beforeAll(async  (done) => {
                
                
                 await sequelize.sync({
                         force: true,
                         logging: false
                     })
                
                 current_user = await models.user.model.create({
                     login_name: 'admin',
                     first_name: 'user',
                     last_name: 'family',   
                 })

                 business = await models.business.model.create({
                     name: 'test',
                     business_identifier: 32,
                 })

                 business2 = await models.business.model.create({
                     name: 'test2',
                     business_identifier: 31,
                 })
                 
                 await models.creditCard.model.bulkCreate([
                    {
                        id:1,
                        cc_company_name:'ויזה',
                        cc_number:'4000123456783070',
                        cc_holder_name:'test',
                        cc_holder_id:'1234',
                        cc_cvv_number:'123',
                        cc_validity: moment([2003, 11 - 1 ]),
                        description:''
                    },
                    {
                        id:2,
                        cc_company_name:'ישראכרט',
                        cc_number:'5000129456783090',
                        cc_holder_name:'test',
                        cc_holder_id:'1234',
                        cc_cvv_number:'123',
                        cc_validity: moment([2004, 12 - 1 ]),
                        description:''
                    },
                    {
                        id:3,
                        cc_company_name:'מאסטרקארד',
                        cc_number:'5000129456783010',
                        cc_holder_name:'test',
                        cc_holder_id:'1234',
                        cc_cvv_number:'123',
                        cc_validity: moment([2006, 8 - 1 ]),
                        description:''
                    },
                ])


                app = express();
                app.use(express.json());
                app.use(express.urlencoded({extended: false}));
                app.use(bodyParser.urlencoded({ extended: false })); 
                // mock req.user
                app.use((req, res, next)=>{      
                    req.user = {id: current_user.id, business_id: business.id};
                    next();
                })
                // app.use('/', routing)
                app.use('/credit_card/', creditCardRouter );                


                
                
                 done()
            } )

            beforeEach(async  (done) => {
               
                done()
            })

            describe('GET /credit_card', ()=>{

                it('Should retun status 200', () => {
                    return request(app).get('/credit_card/').then((response) => {
                        expect(response.statusCode).toBe(200);
                    } )
                } )

                it('Should match', () => {
                    return request(app).get('/credit_card/').then((response) => {
                        expect(response.body).toMatchObject([{
                        id:1,
                        cc_company_name:'ויזה',
                        last_4_numbers:'xxxxxxxxxxxx3070',
                        cc_holder_name:'test',
                        cc_holder_id:'1234',
                        cc_cvv_number:'123',
                        cc_validity_year: 2003,
                        cc_validity_month: 11,
                        cc_validity: '11/2003',
                        description:''
                    },
                    {
                        id:2,
                        cc_company_name:'ישראכרט',
                        last_4_numbers:'xxxxxxxxxxxx3090',
                        cc_holder_name:'test',
                        cc_holder_id:'1234',
                        cc_cvv_number:'123',
                        cc_validity_year: 2004,
                        cc_validity_month: 12,
                        cc_validity: '12/2004',
                        description:''
                    },
                    {
                        id:3,
                        cc_company_name:'מאסטרקארד',
                        last_4_numbers:'xxxxxxxxxxxx3010',
                        cc_holder_name:'test',
                        cc_holder_id:'1234',
                        cc_cvv_number:'123',
                        cc_validity_year: 2006,
                        cc_validity_month: 8,
                        cc_validity: '8/2006',
                        description:''
                    }]);
                    } )
                } )

                

                
            })

            describe('POST /credit_card', ()=>{

                it('Should retun status 200', async () => {
                    await truncateModel(models.creditCard.model)
                    return request(app)
                    .post('/credit_card/')
                    .send({
                            cc_company_name:'מאסטרקארד',
                            cc_number:'5000129456783333',
                            cc_holder_name:'test',
                            cc_holder_id:'1234',
                            cc_cvv_number:'123',
                            cc_validity_year: 2003,
                            cc_validity_month: 11,
                            description:''
                        })
                    .then((response) => {
                        expect(response.statusCode).toBe(200);
                    } )
                } )

                it('Should match', async () => {
                    await truncateModel(models.creditCard.model)
                    return request(app)
                        .post('/credit_card/')
                        .send({
                            cc_company_name:'מאסטרקארד',
                            cc_number:'5000129456783010',
                            cc_holder_name:'test',
                            cc_holder_id:'1234',
                            cc_cvv_number:'123',
                            cc_validity_year: 2004,
                            cc_validity_month: 5,
                            description:''
                        })
                        .then(async (response) => {
                            let card = await models.creditCard.model.findOne()
                            expect(card || {}).toMatchObject({
                                id: 2,
                                cc_company_name:'מאסטרקארד',
                                cc_number:'5000129456783010',
                                cc_holder_name:'test',
                                cc_holder_id:'1234',
                                cc_cvv_number:'123',
                                // cc_validity: moment([2004, 5 - 1 ]),
                                description:''
                            });
                    } )
                } )

                

                
            })

            describe('PUT /credit_card', ()=>{

                it('Should retun status 200', async () => {
                    return request(app)
                    .put('/credit_card/2')
                    .send({
                            cc_company_name:'מאסטרקארד',
                            cc_number:'5000129456783333',
                            cc_holder_name:'test',
                            cc_holder_id:'1234',
                            cc_validity_year: 2003,
                            cc_validity_month: 8,
                            description:''
                        })
                    .then((response) => {
                        expect(response.statusCode).toBe(200);
                    } )
                } )

                it('Should match', async () => {
                    return request(app)
                        .put('/credit_card/2')
                        .send({
                            cc_company_name:'test update',
                            cc_number:'5000129456712345',
                            cc_holder_name:'test update',
                            cc_holder_id:'1234555',
                            cc_validity_year: 2004,
                            cc_validity_month: 5,
                            description:'test test'
                        })
                        .then(async (response) => {
                            let card = await models.creditCard.model.findOne()
                            expect(card || {}).toMatchObject({
                                id: 2,
                                cc_company_name:'test update',
                                cc_number:'5000129456712345',
                                cc_holder_name:'test update',
                                cc_holder_id:'1234555',
                                 cc_validity: moment([2004, 5 - 1 ]),
                                description:'test test'
                            });
                    } )
                } )
                
                it('not delete cc_number if not exist match', async () => {
                    return request(app)
                        .put('/credit_card/2')
                        .send({
                            cc_company_name:'test update',
                            cc_number:'',
                            cc_holder_name:'test update',
                            cc_holder_id:'1234555',
                            cc_validity_year: 2004,
                            cc_validity_month: 5,
                            description:'test test'
                        })
                        .then(async (response) => {
                            let card = await models.creditCard.model.findOne()
                            expect(card || {}).toMatchObject({
                                id: 2,
                                cc_company_name:'test update',
                                cc_number:'5000129456712345',
                                cc_holder_name:'test update',
                                cc_holder_id:'1234555',
                                // cc_validity: moment([2004, 5 - 1 ]),
                                description:'test test'
                            });
                    } )
                } )

                

                
            })

            describe('DELETE /credit_card', ()=>{

                it('Should retun status 200', async () => {
                    return request(app)
                    .delete('/credit_card/2')
                    .then((response) => {
                        expect(response.statusCode).toBe(200);
                    } )
                } )

                it('Should be null', async () => {
                    return request(app)
                        .delete('/credit_card/2')
                        .then(async (response) => {
                            let card = await models.creditCard.model.findOne()
                            expect(card).toBe(null);
                    } )
                } )

                

                
            })

      } )
