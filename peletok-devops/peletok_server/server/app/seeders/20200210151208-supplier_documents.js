'use strict';
const moment = require('moment');

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    let dateNow = moment().toDate();
    let documentSuppliers = [{
      supplier_id: 95,
      document_id: 2,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      supplier_id: 95,
      document_id: 1,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      supplier_id: 94,
      document_id: 3,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      supplier_id: 94,
      document_id: 4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      supplier_id: 127,
      document_id: 5,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      supplier_id: 127,
      document_id: 6,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      supplier_id: 196,
      document_id: 8,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      supplier_id: 196,
      document_id: 7,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      supplier_id: 124,
      document_id: 10,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      supplier_id: 124,
      document_id: 9,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      supplier_id: 203,
      document_id: 12,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      supplier_id: 203,
      document_id: 11,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      supplier_id: 205,
      document_id: 14,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      supplier_id: 205,
      document_id: 13,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      supplier_id: 201,
      document_id: 15,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      supplier_id: 201,
      document_id: 16,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      supplier_id: 202,
      document_id: 17,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      supplier_id: 210,
      document_id: 18,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      supplier_id: 209,
      document_id: 19,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      supplier_id: 211,
      document_id: 20,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      supplier_id: 185,
      document_id: 21,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      supplier_id: 186,
      document_id: 22,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      supplier_id: 187,
      document_id: 23,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      supplier_id: 194,
      document_id: 24,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      supplier_id: 999,
      document_id: 25,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      supplier_id: 192,
      document_id: 26,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      supplier_id: 193,
      document_id: 27,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      supplier_id: 1,
      document_id: 28,
      created_at: dateNow,
      updated_at: dateNow
    },]
    return queryInterface.bulkInsert('DOC_SupplierDocument', documentSuppliers, {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.bulkDelete('DOC_SupplierDocument', null, {});
  }
};
