var BaseModel = require('../../BaseModel.js');
const DataTypes = require("sequelize").DataTypes;
const israeliIdValidator = require("../../utils").israeliIdValidator;

/**
 * User model
 */
class User extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            login_name: {
                type: DataTypes.STRING(12),
                allowNull: true
            },
            first_name: {
                type: DataTypes.STRING(36),
                allowNull: true
            },
            last_name: {
                type: DataTypes.STRING(36),
                allowNull: true
            },
            israeli_id: {
                type: DataTypes.STRING(9),
                allowNull: true,
                validate: { israeliIdValidator }
            },
            passport_number: {
                type: DataTypes.STRING(16),
                allowNull: true
            },
            passport_country: {
                type: DataTypes.STRING(2),
                allowNull: true
            },
            id_validity: {
                type: DataTypes.DATEONLY,
                allowNull: true
            },
            pt_admin: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false
            },
            num_ip_allowed: {
                type: DataTypes.INTEGER,
                allowNull: true // 'null' means not specific IPs
            },
            num_device_allowed: {
                type: DataTypes.INTEGER,
                allowNull: true // 'null' means not specific devices
            },
        };
        this.options["indexes"] = [
            {
                unique: true,
                fields: ['login_name']
            },
            {
                unique: true,
                fields: ['israeli_id']
            },];
        this.statusAuthor = true;
        this.statusTimestampField = true;
        this.prefix = 'USR';
        this.statusField = true;
        this.getterMethods = {
            isPtAdmin() {
              return this.pt_admin
            }
          }
    }

    /**
     * creates associate for user model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.hasMany(models.userPassword.model, {as: 'UserPassword' });
        this.model.hasMany(models.userAddress.model, {as: 'UserAddress' });
        this.model.hasMany(models.userContact.model, {as: 'UserContact' });
        this.model.hasMany(models.userDocument.model, {as: "UserDocument" });
        this.model.hasMany(models.userBusiness.model, {as: 'UserBusiness'});
        this.model.hasMany(models.supplierUser.model, {as: 'SupplierUSer' });
        this.model.hasMany(models.userBranch.model, {as: 'UserBranch' });
        this.model.hasMany(models.transaction.model, {as: 'Transaction' });
        this.model.hasMany(models.distributor.model, {as: 'Distributor' });
        this.model.hasMany(models.businessProduct.model, {

            as: 'BusinessProduct',
            foreignKey: "approved_by_id"
        });
        this.model.hasMany(models.businessSupplier.model, {

            as: 'BusinessSupplier',
            foreignKey: "approved_by_id"
        });
        this.model.hasMany(models.productDetails.model, {

            as: 'ProductDetails',
            foreignKey: "seller_id"
        });
        this.model.belongsTo(models.language.model, { as: 'Language' });
        this.model.hasMany(models.message.model, { as: 'MessagesSend', foreignKey: "from_user_id" });
        this.model.hasMany(models.message.model, { as: 'MessagesReceipt', foreignKey: "to_user_id" });
        this.model.hasMany(models.loginHistory.model, { as: 'LoginHistory' });
        this.model.hasMany(models.manualCard.model, {foreignKey: "author_id", as: 'ManualCard' });
        this.model.hasMany(models.giftOrder.model, { as: 'GiftOrder' });
        this.model.belongsToMany(models.role.model, {through: "RoleUser"});
        this.model.hasMany(models.business.model, { as: 'Business' });
        this.model.hasMany(models.activitySession.model, { as: 'ActivitySession' });
        this.model.hasMany(models.userIp.model, { as: 'UserIp' });
        this.model.hasMany(models.userToken.model, { as: 'UserToken' });
    }
}


module.exports = new User();

