import React from 'react';
import { Row, Col, Container, Button, Collapse, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import InputTypeCheckBox from '../InputUtils/InputTypeCheckBox'
import loc from '../../Assets/Icons/MenuIcons/location-pin-8.svg'
import './Gifts.css';
import { LanguageManager as LM } from "../LanguageManager/Language";
import { Gifts } from './Gifts'
import { getMyDetails, getCurrentUserData, getAllGifts } from '../DataProvider/Functions/DataGeter'
import { postOrderGifts } from '../DataProvider/Functions/DataPoster'
import { Notifications } from '../Notifications/Notifications'
import { UserDetailsService } from '../../services/UserDetailsService';
import { Loader } from '../Loader/Loader'
import {GiftStatusEnum} from "../../enums/GiftStatusEnum";

/**
 * A component that calls Components for presenting each gift
 *TODO -- Everything needs to come from the database
 */

export class MainGifts extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            newAddress: '',
            selectedGiftArray: [],
            giftToOrder: { address: '', utilizing_points: 0, gift_id: [] },
            selectedGiftNamesArray: [],
            modal: false,
            points_required: 0,//TODO -- Need to get from the database
            // previousPoints: 0,//TODO -- Need to get from the database
            inputTypeCheckBoxChangeAddress: {
                notValidMessage: 'Value checked (true) is FORBIDEN ',
                checked: false,
                lableText: '  ',
                tooltip: "",
                title: "",
                id: "checkBoxId1",
            },
            gifts: []

        };

        this.toggle = this.toggle.bind(this);
        this.onChange = this.onChange.bind(this);
        this.orderGift = this.orderGift.bind(this);
        this.toChangeAddress = this.toChangeAddress.bind(this);
        this.changeAddress = this.changeAddress.bind(this);
        this.getTheGifts = this.getTheGifts.bind(this);
        this.changeAddressCheckBox = this.changeAddressCheckBox.bind(this);
    }

    componentDidMount() {
        this.getTheGifts();
        getCurrentUserData().then(
            res => {
                this.setState({ balanceData: res.balance }, () => {
                    let points = this.state.balanceData.filter(b => b.strRepr === LM.getString("pointToUse"))
                    this.setState({ points_required: points[0].value })
                })
            }
        );
    }

    getTheGifts() {
        this.setState({ gifts: [] })
        getAllGifts().then(
            (res) => {
                this.setState({ gifts: res.filter(gift => gift.status === GiftStatusEnum.ACTIVE) }, () => {
                })
            }
        ).catch(
            (err) => {
                Notifications.show(err, 'danger');
            }
        )
    }

    toChangeAddress() {
        if (this.state.giftToOrder.utilizing_points > 0) {
            let giftToOrder = Object.assign({}, this.state.giftToOrder);
            /**
             * get and save The registered address
             */

            getMyDetails().then(
                (res) => {
                    let str = [];
                    str.push(res[0].street);
                    str.push(res[0].entrance);
                    str.push(res[0].floor);
                    str.push(res[0].number);
                    str.push(res[0].city);
                    str.push(res[0].country);
                    str.push(res[0].zip_code);
                    giftToOrder.address = str.join(' ');
                    giftToOrder.newAddress = str.join(' ');
                    this.setState({ giftToOrder });

                }
            ).catch(
                (err) => {
                    Notifications.show(err, 'danger');
                }
            )
            this.toggle()
        } else {
            Notifications.show(LM.getString('noGiftSelected'))
        }
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    changeAddress(e) {
        let giftToOrder = Object.assign({}, this.state.giftToOrder)
        this.setState({
            newAddress: e.target.value
        }, () => {
            giftToOrder.newAddress = this.state.newAddress
            this.setState({ giftToOrder })
        });
    }

    changeAddressCheckBox() {
        let inputTypeCheckBoxChangeAddress = Object.assign({}, this.state.inputTypeCheckBoxChangeAddress);
        inputTypeCheckBoxChangeAddress.checked = !this.state.inputTypeCheckBoxChangeAddress.checked;
        this.setState({ inputTypeCheckBoxChangeAddress });
    }

    orderGift() {
        let address;
        if (this.state.inputTypeCheckBoxChangeAddress.checked) {
            address = this.state.giftToOrder.newAddress
        }
        else {
            address = this.state.giftToOrder.address;
        }
        let giftToOrder = {
            "address": address,
            "utilizing_points": this.state.giftToOrder.utilizing_points,
            "gift_id": this.state.giftToOrder.gift_id
        };
        this.setState({ giftToOrder: { address: '', utilizing_points: 0, gift_id: [] } })

        Loader.show()
        postOrderGifts(giftToOrder)
            .then(
                (res) => {
                    if (res[0].message == 'success') {
                        Loader.hide()
                        UserDetailsService.getUserDetails()
                        Notifications.show(LM.getString('GiftOrderSuccessfully'))
                        this.changeAddressCheckBox()
                        this.toggle();
                        this.getTheGifts()
                    }
                }
            ).catch(
                (err) => {
                    Loader.hide()
                    Notifications.show(err, 'danger');
                }
            )
    }

    onChange(bool, obj) {
        var arr = [...this.state.selectedGiftArray];
        var nameArr = [...this.state.selectedGiftNamesArray];
        let giftToOrder = Object.assign({}, this.state.giftToOrder);
        if (bool) {
            arr.push(obj)
            nameArr.push(obj.name);
            giftToOrder.utilizing_points += obj.points_required;
            giftToOrder.gift_id.push(obj.id);
            this.setState({ selectedGiftArray: arr, selectedGiftNamesArray: nameArr, giftToOrder });
        } else {
            var newArray = [];
            var newNameArray = [];
            giftToOrder.utilizing_points = 0;
            giftToOrder.gift_id = [];

            arr.map((newObj, key) => {
                if (newObj.id != obj.id) {

                    newArray.push(newObj);
                    newNameArray.push(newObj.name)
                    giftToOrder.utilizing_points += newObj.points_required;
                    giftToOrder.gift_id.push(newObj.id);

                }

            })
            this.setState({
                selectedGiftArray: newArray,
                selectedGiftNamesArray: newNameArray,
                giftToOrder
            });
        }
    }

    setPoints = async (a, b) => {
        if (b) {
            await this.setState({
                points_required: (this.state.points_required - a)
                // previousPoints: (this.state.previousPoints - a) 
            });
        }
        else {
            await this.setState({
                points_required: (this.state.points_required + a)
                // previousPoints: (this.state.previousPoints + a)
            });
        }
    }

    render() {
        return (
            <div >
                <h3 className='maing'>{LM.getString('pointUsage')}</h3>
                <div className='cards'>
                    <Container>
                        <Row>
                            <Col className='startHeader' sm='6'>{LM.getString('ChooseGifts')}</Col>
                            <Col className='endHeader' sm='6'>{LM.getString('TotalPointsToChoose')}{this.state.points_required}</Col>
                        </Row>
                        {this.state.gifts.map((gift, index) =>
                            (index % 2 == 0) ?
                                <Row className="my-2">
                                    <Col sm='6'>
                                        <Gifts onChange={this.onChange} setPoints={this.setPoints} enabled={this.state.gifts[index].points_required <= this.state.points_required ? true : false}  {...gift} />
                                        {/* <Gifts  onChange={this.onChange} setPoints={this.setPoints} enabled={this.state.gifts[index].points_required <= this.state.previousPoints ? true : false}  {...gift} /> */}
                                    </Col>

                                    {
                                        (index + 1 < this.state.gifts.length) ?
                                            <Col sm='6'>
                                                <Gifts onChange={this.onChange} setPoints={this.setPoints} enabled={this.state.gifts[index + 1].points_required <= this.state.points_required ? true : false}
                                                    {...this.state.gifts[index + 1]}
                                                // <Gifts  onChange={this.onChange} setPoints={this.setPoints} enabled={this.state.gifts[index + 1].points_required <= this.state.previousPoints ? true : false}
                                                />
                                            </Col>
                                            :
                                            <React.Fragment />
                                    }
                                </Row>
                                :
                                <React.Fragment />)}
                        <Row>
                            <Col ><Button className='myButton' id='InviteSelectedGifts' onClick={this.toChangeAddress}>{LM.getString('InviteSelectedGifts')}</Button></Col>
                        </Row>
                        <Row>
                            <Col >{LM.getString('Pointsleftaftertheorder')}{"  "}{this.state.points_required}{"  "}{LM.getString('points')}</Col>
                            {/* <Col >{LM.getString('Pointsleftaftertheorder')}{"  "}{this.state.previousPoints}{"  "}{LM.getString('points')}</Col> */}
                        </Row>
                    </Container>
                    <Modal isOpen={this.state.modal} toggle={this.toggle} style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <ModalHeader toggle={this.toggle}>
                            <div className='modalGifts1'>{LM.getString('DeliveryGifts')}</div>
                            <div className='modalGifts2'> {this.state.selectedGiftNamesArray}</div>
                        </ModalHeader>
                        <div className='adressForGifts1'>
                            {LM.getString('address')}{"       " + this.state.giftToOrder ? this.state.giftToOrder.address : ' '}
                        </div>
                        <Container>
                            <Row >
                                <Col sm='2'>
                                    <InputTypeCheckBox
                                        {...this.state.inputTypeCheckBoxChangeAddress}
                                        onChange={this.changeAddressCheckBox}
                                    />
                                </Col>
                                <Col >
                                    {LM.getString('deliverToAnotherAddress')}
                                </Col>
                            </Row>
                            <Collapse isOpen={this.state.inputTypeCheckBoxChangeAddress.checked}>
                                <Row className='rowForGiftsAdress' >
                                    <Col sm='10'>
                                        <input
                                            className='inputForGiftsAdress'
                                            onChange={this.changeAddress}
                                            placeholder={LM.getString('typeNewAddressForGifts')}
                                        />
                                    </Col>
                                    <Col sm='1'>
                                        <img className='myImg' src={loc} />
                                    </Col>
                                </Row>
                            </Collapse>
                        </Container>
                        <ModalBody>  </ModalBody>
                        <ModalFooter style={{ alignItems: 'center', justifyContent: 'center' }}>

                            <Button
                                style={{ background: 'var(--regButton)', margin: 'var(--marginButton)' }}
                                onClick={this.orderGift}
                            >
                                {LM.getString('orderGift')}
                            </Button>
                            {'    '}
                            <Button
                                color="secondary"
                                onClick={() => {
                                    this.changeAddressCheckBox()
                                    this.toggle()
                                }}
                            >
                                {LM.getString('cancel')}
                            </Button>
                        </ModalFooter>
                    </Modal>
                </div>
            </div>
        );
    }
}
