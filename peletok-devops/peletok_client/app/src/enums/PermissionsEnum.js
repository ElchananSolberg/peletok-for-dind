export class PermissionsEnum{
    static ACTION_SHOW      = 1;
    static ACTION_EDIT      = 2;
    static ACTION_TEMP_EDIT = 3;

    static mapPermisionRoleToAction(permissionRole, action){
        switch(parseInt(action)){
            case PermissionsEnum.ACTION_TEMP_EDIT: 
                return permissionRole.action == PermissionsEnum.ACTION_EDIT && permissionRole.date;
            case PermissionsEnum.ACTION_EDIT:
                return permissionRole.action == PermissionsEnum.ACTION_EDIT && !permissionRole.date; 
            case PermissionsEnum.ACTION_SHOW:
                return permissionRole.action == PermissionsEnum.ACTION_SHOW;
            default:
                return false;
        }
    }

    static permissionTypeTextToNumber(type){
        return type == 'show' ? PermissionsEnum.ACTION_SHOW : PermissionsEnum.ACTION_EDIT
    }
}