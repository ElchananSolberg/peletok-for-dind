import React, { Component } from 'react';
import { Button, Row, Col, Collapse } from 'reactstrap';

import { LanguageManager as LM } from "../LanguageManager/Language"
import { getDistributors, getSellers, getReportRes } from '../DataProvider/DataProvider'
import { ReportTableContent } from "../ReportComponent/ReportTableContent"
import InputTypeRadio from '../InputUtils/InputTypeRadio'
import InputTypeNumber from '../InputUtils/InputTypeNumber';
import InputTypeSearchList from '../InputUtils/InputTypeSearchList'
import InputTypeDate from '../InputUtils/InputTypeDate';
import InputTypeTime from '../InputUtils/InputTypeTime';

export class ManualCardsReport extends Component {
    constructor(props) {
        super(props);
        this.state = {
            distributors: [],
            suppliers: [],
            agents: [],
            tableRows: [],
            tableHeadersObj: {},
            tableFootersObj: {},
            openReport: false,

        }
        this.getDistributorsNames = this.getDistributorsNames.bind(this);
        this.getBusinessNames = this.getBusinessNames.bind(this);
        this.getAgentNames = this.getAgentNames.bind(this);
        this.fields = {};
        this.reportID = "16";
    }

    getDistributorsNames() {
        getDistributors().then(
            (res) => {
                this.setState({ distributors: res });
            }
        ).catch(
            (err) => {
                alert("Failed loading Distributors list: " + err)

            }
        )
    }

    getBusinessNames() {
        getSellers().then(
            (res) => {
                this.setState({ business: res });
            }
        ).catch(
            (err) => {
                alert("Failed loading business list: " + err)

            }
        )
    }

    getAgentNames() {
        // This is mock we need to get it from the server
        // TODO add server call for agents
        this.setState({
            agents: [
                {
                    name: "סמי",
                    id: 1,
                },
                {
                    name: "רונן",
                    id: 2,
                },
                {
                    name: "דליה",
                    id: 3,
                },
            ]
        })
    }

    componentDidMount() {
        this.getDistributorsNames()
        this.getBusinessNames()
        this.getAgentNames()
        
    }

    showReportClick = () => {
        let params = {};
        let requestValid = true;
        // this is a list of valid params for generating the report
        let validParams = ['startDate', "startTime", "endDate", "endTime", "distributor", "distributor_id", 'busines', 'agent', "agent_id", "business_id", "business_status", "by_distributor", "product", "supplier"];
        Object.keys(this.fields).forEach((key) => {
            let isValid = validParams.indexOf(key) != -1
            if (this.fields[key] !== null && isValid) {
                let result = this.fields[key].getValue();
                if (!result.valid) {
                    requestValid = false;
                }
                params[key] = result.value;
            }
        })
        if (requestValid) {
            getReportRes(this.reportID, { params }).then(
                (res) => {
                    if (res === "No reports found") {
                        alert(LM.getString("noReportErrMsg"))
                    }
                    else {
                        let headers = [];
                        Object.values(res['headers']).map((value) => {
                            return headers.push(LM.getString(value))
                        });
                        this.setState({
                            tableFootersObj: res['sum'],
                            tableHeadersObj: headers,
                            tableRows: res['data'],
                            openReport: true,
                            sumOfProfit: res['sum'].priceSum + LM.getString('monnySymbol')
                        }
                        )
                    }
                }
            ).catch(
                (err) => {
                    alert(err);
                })
        }
        else {
            alert(LM.getString("wrongInputErrMsg"))
        }

    }

    render() {
        let time_now = new Date()
        let nowHour = time_now.getHours() + "";
        let nowMinute = time_now.getMinutes() + "";
        if (nowHour.length < 2) { nowHour = "0" + nowHour }
        if (nowMinute.length < 2) { nowMinute = "0" + nowMinute }

        return (
            <div>
                <h3 className={'color-page-header fs-26 font-weight-light'}>{LM.getString("manualCards")}</h3>
                <div className="main_border_padding" >
                    <Row>
                        <Col sm="4">
                            <InputTypeSearchList
                                title={LM.getString("supplier")}
                                id='selectSupplier'
                                options={this.state.suppliers.map(supplier => supplier.name)}
                                onChange={() => {
                                    let theName = this.fields['supplier'].state.value;
                                    let supplierObj = this.state.business.filter((el) => { return el.name == theName })[0];
                                    this.fields["supplier_id"].setValue(supplierObj.id);
                                }}
                                ref={(componentObj) => { this.fields['supplier'] = componentObj }}
                            />
                        </Col>
                        <Col sm="4">
                            <InputTypeSearchList
                                title={LM.getString("product")}
                                id='selectProduct'
                                options={this.state.suppliers.map(product => product.name)}
                                onChange={() => {
                                    let theName = this.fields['product'].state.value;
                                    let productObj = this.state.business.filter((el) => { return el.name == theName })[0];
                                    this.fields["product_id"].setValue(productObj.id);
                                }}
                                ref={(componentObj) => { this.fields['product'] = componentObj }}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="4">
                            <InputTypeSearchList
                                title={LM.getString("selectDistributor")}
                                id='selectDistributors'
                                options={this.state.distributors.map(distributor => distributor.business_name)}
                                onChange={
                                    () => {
                                        // TODO Add functionalety 
                                        // let theName = this.fields['distributor'].state.value;
                                        // let distObj = this.state.distributors.filter((el) => { return el.name == theName })[0]
                                        // this.fields['distributor_id'].setValue(distObj.id)

                                    }
                                }
                                ref={(componentObj) => { this.fields['distributor'] = componentObj }}
                            />
                        </Col>
                        <Col sm="4">
                            <InputTypeNumber
                                title={LM.getString("distributorNum")}
                                id="distributorNumber"
                                ref={(componentObj) => { this.fields["distributor_id"] = componentObj }} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="4">
                            <InputTypeSearchList
                                title={LM.getString("business_name")}
                                id='selectBusiness'
                                // options={this.state.business.map(busines => busines)}
                                options={[]}
                                onChange={() => {
                                    let theName = this.fields['busines'].state.value;
                                    let businesObj = this.state.business.filter((el) => { return el.name == theName })[0];
                                    this.fields["business_id"].setValue(businesObj.business_identifier);
                                }}
                                ref={(componentObj) => { this.fields['busines'] = componentObj }}
                            />
                        </Col>
                        <Col sm="4">
                            <InputTypeNumber
                                title={LM.getString("customNum")}
                                id="costumerNumber"
                                ref={(componentObj) => { this.fields["business_id"] = componentObj }} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="4">
                            <InputTypeSearchList
                                title={LM.getString("agentSearch")}
                                id='selectAgent'
                                options={this.state.agents.map(agent => agent.name)}
                                onChange={
                                    () => {
                                        let theName = this.fields['agent'].state.value;
                                        let agenObj = this.state.agents.filter((el) => { return el.name == theName })[0]
                                        this.fields['agent_id'].setValue(agenObj.id)
                                    }
                                }
                                ref={(componentObj) => { this.fields['agent'] = componentObj }}
                            />
                        </Col>
                        <Col sm="4">
                            <InputTypeNumber
                                title={LM.getString("agentNum")}
                                id="agentNumber"
                                ref={(componentObj) => { this.fields["agent_id"] = componentObj }} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="4">
                            <InputTypeSearchList
                                multi={true}
                                title={LM.getString('businessStatus')}
                                id="businessStatus"
                                default={LM.getString("any")}
                                options={[LM.getString("froze"), LM.getString("locked"), LM.getString("active"), LM.getString("deleted")]}
                                ref={(componentObj) => { this.fields['business_status'] = componentObj }} />

                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <InputTypeRadio
                                options={[LM.getString("byDistributor")]}
                                id="byDistributor"
                                ref={(componentObj) => { this.fields["by_distributor"] = componentObj }} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="4">
                            <InputTypeDate
                                title={LM.getString("startingDate") + ":"}
                                id="startingDate"
                                ref={(componentObj) => { this.fields["startDate"] = componentObj }} />
                        </Col>
                        <Col sm="4">
                            <InputTypeTime
                                title={LM.getString("startingTime") + ":"}
                                id="startingTime"
                                ref={(componentObj) => { this.fields["startTime"] = componentObj }} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="4">
                            <InputTypeDate
                                title={LM.getString("endingDate") + ":"}
                                id="endingDate"
                                ref={(componentObj) => { this.fields["endDate"] = componentObj }} />
                        </Col>
                        <Col sm="4">
                            <InputTypeTime
                                title={LM.getString("endingTime") + ":"}
                                id="endingTime"
                                default={nowHour + ':' + nowMinute}
                                ref={(componentObj) => { this.fields["endTime"] = componentObj }} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="auto">
                            <Button className="paymentsButton" id="paymentsShowReportButton" onClick={this.showReportClick} > {LM.getString("showReport")} </Button>
                        </Col>
                    </Row>
                </div>
                <Collapse isOpen={this.state.openReport}>
                    <ReportTableContent reportName={LM.getString("distributionActivities")} tableRows={this.state.tableRows} tableHeadersObj={this.state.tableHeadersObj} tableFootersObj={this.state.tableFootersObj} />
                </Collapse>
            </div>
        )
    }

}
