const express = require('express');
const { getTypeNum, getNumStatus } = require("./utils");
const { wrapperUpdate, wrapperCreate, wrapperGet, wrapperGetUser } = require("./mapping/crudMiddleware");
const Op = require("sequelize").Op;
const { Info } = require("./mapping/crudMiddleware");
const moment = require('moment')
const adminRouter = express.Router();
const sequelizeInstance = require('./setupSequelize')
const {getReportLength} = require("./rest/reportsApi");
const {reportSender} = require("./rest/reportsApi");
const { deleteCreditCard } = require("../app/bank/CreditCard")
const { getUser30LastActions } = require('./user_management/user_management_utils')
const { getUsersRole } = require('./user_management/user_management_utils')
const models = require('./index.js');
const {PELETOK_BUSINESS_ID} = require('./constants')
/***
 * create  new  credit card
 */
adminRouter.post("/credit_card", wrapperCreate);

/***
     * update    credit card
     */
adminRouter.put("/credit_card", wrapperUpdate);
/***
     * delete card
     */
adminRouter.delete("/credit_card/:id", async (req, res) => {

    return deleteCreditCard(req, res)
}
);
// When sequelize 'finAll',findOne', 'get' etc'.. function finds no values it returns empty array. when performing a query that uses that returned value it converts to NULL
// for example  IN([]) = IN (NULL), to avoid errors in such situations were pushing 'avoidNull' const to the empty array and the output is: IN(-1)
const avoidNull = -1
let transaction;
adminRouter.get("/user/", async (req, res) => {
    //TODO: Add permission to map for this request
    let filter = {};
    if (req.query.business) {
        filter["$UserBusiness.business_id$"] = req.query.business;
    }
    if (req.query.distributor) {
        filter["$UserBusiness.Business.distributor_id$"] = req.query.distributor;
    }
    if (req.query.phone_number) {
        filter[Op.and] = [{ "$UserContact.value$": req.query.phone_number },
        { "$UserContact.type$": getTypeNum("phone_number", "UserContact") }];
    }
    if (req.query.license_number) {
        filter["$UserBusiness.Business.business_license_number$"] = req.query.license_number;
    }
    let users = await wrapperGetUser(req, res, null, null, filter);
    for (let i = 0; i < users.result.length; i++) {
        const user = users.result[i];
        let [login_history, sales, role] = await Promise.all([
            models.loginHistory.model.findAll({where: {user_id: user.id}}),
            getUser30LastActions(user.id),
            getUsersRole(user.id)
        ])
        user['login_history'] = login_history.map(lh => {return {timestamp_login: lh.timestamp_login, user_ip: lh.user_ip}} )
        user['sales'] = sales
        user['role'] = role
    }
    res.send(users)
});

async function calculateArticle20PercentageProfit(percentageProfit) {
    let VAT = await models.vATRate.model.findOne({
        order: [
            ['start_timestamp', 'DESC']
        ],
        attributes: ['value'],
        raw: true
    })
    let PercentageOfVAT = parseFloat(VAT.value)

    let calculatedArticle20PercentageProfit = percentageProfit - ((percentageProfit * PercentageOfVAT) / 100)

    return calculatedArticle20PercentageProfit
}

function synchCommissionAndPercenrageArticle20Calacultaion(vat, value) {
    let calculatedValue = value - ((value * vat) / 100)
    return calculatedValue
}
/**
 * update businessProduct percentage_profit points and authorization
 * @param {*} req
 * @param {*} res
 * @param updateObj
 * @param businessId {Number}businessId for which to update (default: req.body.businessId)
 */
async function updateBusinessProduct(req, res, updateObj, businessId = null) {
    businessId = businessId || req.body.business_id;
    if (!businessId) {
        res.status(400).send('Business id is required')
        return
    }
    const isAuthorizedBusiness = req.user.isPtAdmin || await models.business.model.count({ where: { distributor_id: req.user.business_id, id: businessId } }) > 0
    if (!isAuthorizedBusiness) {
        res.status(400).send('Unauthorized action')
        return
    }
    let info = new Info()
    const currentBusinessProductsIds = req.user.isPtAdmin ? (await models.product.model.findAll({ attributes: ['id'], raw: true })).map(p => p.id) : (await models.businessProduct.model.findAll({ where: { business_id: req.user.business_id, is_authorized: true } })).map((businessProductsRow) => { return businessProductsRow.product_id })
    

    let businessProducts = await models.businessProduct.model.findAll({
        where: { business_id: req.body.business_id, product_id: currentBusinessProductsIds.filter(id=>req.body.product_id.indexOf(id) > -1)},
        include: [
            {
                model: models.product.model,
                as: 'Product',
                 include: [
                    {
                        model: models.supplier.model,
                        as: 'Supplier',
                    },

                ]
            }
        ]
    })

    await Promise.all(businessProducts.map(p=>models.businessProduct.model.update(Object.assign({
            timestamp_start: moment().toDate(),
            author_id: req.user.id,
            status_author_id: req.user.id
        },
        req.body.business_commission ? {business_commission: req.body.business_commission <= p.Product.Supplier.max_percentage_profit ? req.body.business_commission : p.Product.Supplier.max_percentage_profit} : {},
        req.body.final_commission ? {final_commission: req.body.final_commission >= p.Product.Supplier.min_percentage_profit ? req.body.final_commission : p.Product.Supplier.min_percentage_profit} : {},
        req.body.percentage_profit ? {percentage_profit: req.body.percentage_profit <= p.Product.Supplier.max_percentage_profit ? req.body.percentage_profit : p.Product.Supplier.max_percentage_profit} : {},
        req.body.points ? {points: req.body.points} : {}
        ),{where: {id: p.id}}
    ).catch(err=>info.errors.push(err))))

    
        res.customSend(info)
}


    



async function multiUpdateBusinessProduct(req, res) {
    if (!req.body.business_id) {
        res.status(400).send('Business id is required')
        return
    }
    const isAuthorizedBusiness = req.user.isPtAdmin || await models.business.model.count({ where: { distributor_id: req.user.business_id, id: req.body.business_id } }) > 0
    if (!isAuthorizedBusiness) {
        res.status(400).send('Unauthorized action')
        return
    }
    let info = new Info()
    let currentBusinessProducts = await models.businessProduct.model.findAll({ 
        where: { business_id: req.user.business_id, is_authorized: true },
        include: [
            {
                model: models.product.model,
                as: 'Product',
                include: [
                    {
                        model: models.supplier.model,
                        as: 'Supplier',
                        attributes: ['max_percentage_profit', 'min_percentage_profit']
                    },
                    {
                        model: models.productPrice.model,
                        as: 'ProductPrice',
                        attributes: ['distribution_fee']
                    }
                ]
            }
        ] 
    })
    // to reduce loops we convert the array to object {[id: number]: object: BusinessProduct}
    let currentBusinessProductsByProductIds = currentBusinessProducts.reduce((products, item)=>Object.assign(products,{[item.product_id]: item}),{})
    const currentBusinessProductsIds = currentBusinessProducts.map((businessProductsRow) => { return businessProductsRow.product_id })
    let editedProducts = req.body.products_list.filterByWhiteList('product_id', currentBusinessProductsIds)

   for (let i = 0; i < editedProducts.length; i++) {
       let p = editedProducts[i]
       let businessProduct = currentBusinessProductsByProductIds[p.product_id]
       let p_max = businessProduct.percentage_profit || businessProduct.Product.Supplier.max_percentage_profit
       let c_max = businessProduct.business_commission || businessProduct.Product.Supplier.max_percentage_profit
       let fn_max = businessProduct.final_commission
       let min =  businessProduct.Product.Supplier.min_percentage_profit
       let distribution_fee = [null, undefined, ''].indexOf(businessProduct.distribution_fee) > -1 ? businessProduct.Product.ProductPrice.distribution_fee : businessProduct.distribution_fee
       if (!req.user.isPtAdmin) {
           if(p.percentage_profit >  p_max || p.percentage_profit < min || p.business_commission >  c_max || p.distribution_fee >distribution_fee || p.final_commission> fn_max){
               res.status(403).send('Unauthorized action')
               return
           }
       }
   }
        
    await Promise.all(editedProducts.map((businessProductsRow) => {
            return models.businessProduct.model.update({
                ...businessProductsRow,
                timestamp_start: moment().toDate(),
                author_id: req.user.id,
                status_author_id: req.user.id,
                approved_by_id: req.user.id,
            }, { where: { business_id: req.body.business_id, product_id: businessProductsRow.product_id } }).catch(err => {
                info.errors.push(err)
            })
    
        })).catch((err)=>{
            info.errors.push(err)
        })
    if(req.body.roles){
        let profile = await models.business.model.findOne({
            where: {
                id: req.body.business_id,
                is_abstract: true
            }
        })
        let roles = await models.role.model.findAll({
            where: {
                name: req.body.roles
            }
        })
        await profile.setRoles(roles)
    }

    res.customSend(info)
}

async function updateBusinessUseDistributionFee(req, res) {
    let info = new Info()
    await models.business.model.update({ use_distribution_fee: req.body.use_distribution_fee }, { where: { id: req.body.business_id } }).catch(err => {
        info.errors.push(err)
    })
    res.customSend(info)
}
async function updateSupplierBusiness(req, res, ) {
    let businesses = await models.business.model.findAll({
        where: { id: req.body.business_id },
        include: [{
            model: models.supplier.model,
            as: 'Supplier',
            attributes: [],
            required: false,
            through: {
                where: { supplier_id: req.body.supplier_id },
            }
        }]
    })
    let businessesIds = businesses.map((business) => {
        return business.id
    })
    let info = new Info()
    await models.businessSupplier.model.update({ is_authorized: req.body.is_authorized, approved_by_id: req.user.id }, { where: { supplier_id: req.body.supplier_id, business_id: businessesIds } }).catch(err => {
        info.errors.push(err)
    })
    res.customSend(info)
}
/**
 * gets a list of suppliers according to the logged in business authorizations
 * @param {*} req 
 * @param {*} res 
 */
async function getCurrentSupplierBusiness(req, res) {
    let arrangedBusinessSuppliers = []
    let currentBusiness = await models.business.model.findOne({ where: { id: req.user.business_id } })
    let suppliers = await currentBusiness.getSuppliers({
        through: {
            attributes: ['id'],
            where: req.user.business_id == PELETOK_BUSINESS_ID ? {} : { is_authorized: true }
        },
        order: [
         ['name', 'asc']
        ]
    })
    suppliers.forEach(supplier => {
        if (supplier.status!= getNumStatus('active', 'Supplier')) {
            return
        }
        let arrangedBusinessSupplierObj = {}
        arrangedBusinessSupplierObj.supplier_id = supplier.id
        arrangedBusinessSupplierObj.supplier_name = supplier.name
        arrangedBusinessSupplierObj.business_supplier_id = supplier.BusinessSupplier.id
        arrangedBusinessSupplierObj.is_authorized = supplier.BusinessSupplier.is_authorized
        arrangedBusinessSuppliers.push(arrangedBusinessSupplierObj)
    });

    res.customSend(arrangedBusinessSuppliers)
}
/**
 * gets a list of suppliers according to the request chosen business authorizations
 * @param {*} req 
 * @param {*} res 
 */
async function getChosenSupplierBusiness(req, res) {
    let arrangedBusinessSuppliers = []
    let chosenBusinessWithDistributor = await models.business.model.scope({ method: ['businessWithDistributor', req.params.business_id]}).findOne();
    let distributorPermittedBusiness_supplierIds = (await models.businessSupplier.model.findAll({
        where: { is_authorized: true, business_id: chosenBusinessWithDistributor.Distributor.Business[0].id }
    })).map(bs => { return bs.supplier_id })
    chosenBusinessSuppliers = (await chosenBusinessWithDistributor.getSuppliers({
        through: {
            attributes: ['id'],
        }
    })).filter(s => distributorPermittedBusiness_supplierIds.indexOf(s.id) > -1 || req.user.business_id == PELETOK_BUSINESS_ID)
    chosenBusinessSuppliers.forEach(supplier => {
        if (supplier.status!= getNumStatus('active', 'Supplier')) {
            return
        }
        let arrangedBusinessSupplierObj = {}
        arrangedBusinessSupplierObj.supplier_id = supplier.id
        arrangedBusinessSupplierObj.supplier_name = supplier.name
        arrangedBusinessSupplierObj.business_supplier_id = supplier.BusinessSupplier.id
        arrangedBusinessSupplierObj.is_authorized = supplier.BusinessSupplier.is_authorized
        arrangedBusinessSupplierObj.type = supplier.type
        arrangedBusinessSuppliers.push(arrangedBusinessSupplierObj)
    });

    res.customSend(arrangedBusinessSuppliers)
}

/**
 * gets a list of products according to the logged in business authorizations
 * @param {*} req 
 * @param {*} res 
 */
async function getCurrentBusinessProducts(req, res, commissionFlag) {
    let whereObj = {}
    if (commissionFlag) {
        whereObj.type = getTypeNum('bills_payment', 'product')
    }
    let arrangedBusinessProducts = []
    let products = await models.product.model.findAll({
        attributes: ['id', 'type', 'supplier_id', 'status'],
        include: [
            {
                model: models.businessProduct.model,
                as: 'BusinessProduct',
                where: Object.assign({
                    business_id: req.user.business_id,
                }, req.user.business_id == PELETOK_BUSINESS_ID ? {} : {is_authorized: true}),
                attributes: ['is_authorized', 'percentage_profit', 'business_commission', 'final_commission']
            }, 
            {
                model: models.productDetails.model,
                as: 'ProductDetails',
                attributes: ['product_name'],
                where: { language_code: 'HE' }

            },
            {
                model: models.supplier.model,
                as: 'Supplier',
                include:[
                    {
                        model:models.business.model,
                        as:'Business',
                        where:{id:req.user.business_id},
                        through:{
                            where:{
                                business_id:req.user.business_id
                            }
                        }
                    }
                ]

            },
             {
                model: models.productPrice.model,
                as: 'ProductPrice',
                attributes: ['distribution_fee'],

            }
        ], 
        raw: true,
        order: [
         [{ model: models.productDetails.model, as: 'ProductDetails' }, 'product_name', 'asc']
        ]
    })
    
    products.forEach(product => {
        if (product["Supplier.Business.BusinessSupplier.is_authorized"] != true || product.status!= getNumStatus('active', 'Product')  ) {
            return
        }

        let arrangedBusinessProductsObj = {}
        arrangedBusinessProductsObj.supplier_id = product.supplier_id
        arrangedBusinessProductsObj.product_id = product.id
        arrangedBusinessProductsObj.product_type = product.type
        arrangedBusinessProductsObj.product_name = product["ProductDetails.product_name"]
        arrangedBusinessProductsObj.business_commission = product["BusinessProduct.business_commission"] != null ? product["BusinessProduct.business_commission"] : product["Supplier.max_percentage_profit"]
        arrangedBusinessProductsObj.max_percentage_profit = product["BusinessProduct.percentage_profit"] != null ? product["BusinessProduct.percentage_profit"] : product["Supplier.max_percentage_profit"]
        arrangedBusinessProductsObj.min_percentage_profit = product["BusinessProduct.final_commission"] != null ? product["BusinessProduct.final_commission"] : product["Supplier.min_percentage_profit"]
        arrangedBusinessProductsObj.distribution_fee =  product["BusinessProduct.distribution_fee"] || product["ProductPrice.distribution_fee"]
        arrangedBusinessProducts.push(arrangedBusinessProductsObj)
    });

    res.customSend(arrangedBusinessProducts)
}

/**
* gets a list of products according to the request chosen business authorizations
* @param {*} req 
* @param {*} res 
*/
async function getChosenBusinessProducts(req, res, commissionFlag) {
    let currentBusiness = await models.business.model.findOne({ where: { id: req.params.business_id } })
    let percentageOfVAT;
    if (currentBusiness.use_article_20) {
        let VAT = await models.vATRate.model.findOne({
            order: [
                ['start_timestamp', 'DESC']
            ],
            attributes: ['value'],
            raw: true
        })
        percentageOfVAT = parseFloat(VAT.value)

    }
    let whereObj = {}
    if (commissionFlag) {
        whereObj.type = getTypeNum('bills_payment', 'product')
    }
    let arrangedBusinessProducts = []
    let products = await models.product.model.findAll({
        attributes: ['id', 'type', 'supplier_id', 'status'],
        include: [
            {
                model: models.businessProduct.model,
                as: 'BusinessProduct',
                where: {
                    business_id: req.params.business_id
                },
                attributes: ['percentage_profit', 'points', 'distribution_fee', 'is_authorized', 'business_commission', 'final_commission']
            }, {
                model: models.productDetails.model,
                as: 'ProductDetails',
                attributes: ['product_name'],
                where: { language_code: 'HE' }

            },   {
                model:models.supplier.model,
                as:'Supplier',
               include:[
                   {
                     model: models.business.model,
                     as:'Business',
                     where:{id:req.params.business_id},
                     through:{
                         where:{
                             business_id:req.params.business_id
                         }
                     }
                   }
               ]
            }
        ], raw: true,
        where: whereObj
    })
    products.forEach(product => {
        if (product["Supplier.Business.BusinessSupplier.is_authorized"] != true || product.status!= getNumStatus('active', 'Product')  ) {
            return
        }
        let arrangedBusinessProductsObj = {}
        arrangedBusinessProductsObj.supplier_id = product.supplier_id
        arrangedBusinessProductsObj.business_commission = product["BusinessProduct.business_commission"]
        arrangedBusinessProductsObj.product_id = product.id
        arrangedBusinessProductsObj.product_type = product.type
        arrangedBusinessProductsObj.percentage_profit = product["BusinessProduct.percentage_profit"]
        arrangedBusinessProductsObj.points = product["BusinessProduct.points"]
        arrangedBusinessProductsObj.distribution_fee = product["BusinessProduct.distribution_fee"]
        arrangedBusinessProductsObj.is_authorized = product["BusinessProduct.is_authorized"]
        arrangedBusinessProductsObj.final_commission = product["BusinessProduct.final_commission"]
        arrangedBusinessProductsObj.product_name = product["ProductDetails.product_name"]
        if (currentBusiness.use_article_20 && !commissionFlag) {
            arrangedBusinessProductsObj.article_20_percentage_profit = synchCommissionAndPercenrageArticle20Calacultaion(percentageOfVAT, arrangedBusinessProductsObj.percentage_profit)
        } else if (currentBusiness.use_article_20 && commissionFlag) {
            arrangedBusinessProductsObj.article_20_business_commission = synchCommissionAndPercenrageArticle20Calacultaion(percentageOfVAT, arrangedBusinessProductsObj.business_commission)
        }
        
        arrangedBusinessProducts.push(arrangedBusinessProductsObj)
    });
    res.customSend(arrangedBusinessProducts)
}
/**
 * create payment in "businessPayment" table and updating business balance
 * @param {*} req 
 * @param {*} res 
 */
async function updateBusinessBalance(req, res) {
    transaction = await sequelizeInstance.transaction();
    let info = new Info()
    let currentHour = new Date().toTimeString().split(' ')[0];
    if (req.body.invoice_number && req.body.approver_id) {
        let chosenBusinessFinance = await models.businessFinance.model.findOne({ where: { business_id: req.body.business_id } }, { transaction })
        await chosenBusinessFinance.update({ balance: parseFloat(chosenBusinessFinance.balance) + parseFloat(req.body.sum) }, { transaction }).catch(async err => {
            info.errors.push(err)
            await transaction.rollback();
        })
        await models.businessPayment.model.create({
            date: moment().toDate(), hour: currentHour,
            sum: req.body.sum, invoice_number: req.body.invoice_number,
            approver_id: req.body.approver_id, business_id: req.body.business_id, author_id: req.user.id, business_credit: parseFloat(chosenBusinessFinance.balance),
            note: req.body.note
        }, { transaction }).catch(async err => {
            info.errors.push(err)
            await transaction.rollback();
        })
        await transaction.commit();
    } else {
        info.errors.push('invoice_number and approver_id required ')
    }
    res.customSend(info)
}

adminRouter.put("/business/use_distribution_fee", async (req, res) => {
    updateBusinessUseDistributionFee(req, res)
});
adminRouter.put("/business_product/multi", async (req, res) => {
    multiUpdateBusinessProduct(req, res)
});
adminRouter.put("/business_product/percentage_profit", async (req, res) => {
    let updateObj = { timestamp_start: moment().toDate(), author_id: req.user.id, status_author_id: req.user.id, percentage_profit: req.body.percentage_profit }
    updateBusinessProduct(req, res, updateObj)
});


adminRouter.put("/business_product/points", async (req, res) => {
    let updateObj = { timestamp_start: moment().toDate(), author_id: req.user.id, status_author_id: req.user.id, points: req.body.points }
    updateBusinessProduct(req, res, updateObj)
});
adminRouter.put("/business_product/commission", async (req, res) => {
    let updateObj = { timestamp_start: moment().toDate(), author_id: req.user.id, status_author_id: req.user.id, business_commission: req.body.business_commission }
    updateBusinessProduct(req, res, updateObj)
});
adminRouter.put("/business_product/final_commission", async (req, res) => {
    let updateObj = { timestamp_start: moment().toDate(), author_id: req.user.id, status_author_id: req.user.id, final_commission: req.body.final_commission }
    updateBusinessProduct(req, res, updateObj)
});

adminRouter.put("/business_supplier/authorization", async (req, res) => {
    updateSupplierBusiness(req, res)
});
adminRouter.get("/business_supplier/authorization", async (req, res) => {
    getCurrentSupplierBusiness(req, res)
});
adminRouter.get("/business_supplier/business_authorization/:business_id", async (req, res) => {
    getChosenSupplierBusiness(req, res)
});
adminRouter.get("/business_product/authorization", async (req, res) => {
    getCurrentBusinessProducts(req, res)
});
adminRouter.get("/business_product/business_authorization/:business_id", async (req, res) => {
    getChosenBusinessProducts(req, res)
});
adminRouter.get("/business_product/commission_authorization", async (req, res) => {
    getCurrentBusinessProducts(req, res, true)
});
adminRouter.get("/business_product/commission_business_authorization/:business_id", async (req, res) => {
    getChosenBusinessProducts(req, res, true)
});
adminRouter.put("/business/update_balance", async (req, res) => {
    updateBusinessBalance(req, res)
});

adminRouter.get("/report/report_length/:reportId", getReportLength);

adminRouter.get("/report/report_data/:reportId", reportSender);


module.exports.adminRouter = adminRouter;
module.exports.multiUpdateBusinessProduct = multiUpdateBusinessProduct;
module.exports.updateBusinessProduct = updateBusinessProduct;