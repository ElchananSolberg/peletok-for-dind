
import { configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

// https://stackoverflow.com/questions/55088482/jest-not-implemented-window-alert
window.alert = () => {};

configure({ adapter: new Adapter() });