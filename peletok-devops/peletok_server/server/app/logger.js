const log4 = require('log4js');
const {getLoggerLevel, getLoggerName, getConsoleLogLevel} = require('./config/config');
const LOG_SUFFIX = ".log";
const {MongoLogger} = require('./mongoLogger')

const LOGGING_LEVEL = getLoggerLevel();
const maxLogSize = 1048576; //1MB
const path = require("path");

/***
 config logger
 */
function getLogger() {

  log4.configure({
    appenders:
        {
          log: {
            type: 'multiFile',
            base: path.join(__dirname, '/log/'), property: 'categoryName', extension: getLogFileName(),
            maxLogSize: maxLogSize, backups: 10, compress: true,
          },
            stderr: {
                type: 'stderr',
            },
            filter: {
              type: "logLevelFilter",
                appender: "stderr", level: getConsoleLogLevel()
            }
        },
    categories: {default: {appenders: ['log', "filter"], level: LOGGING_LEVEL}}});
  let logger = log4.getLogger(getLoggerName())
  const errorLogger = logger.error;
  logger.error = function(error, req = {}){
    MongoLogger.logTransactionErrors({
      transactionId: req.transactionId,
      activityId: req.activity_id,
      userId: req.user ? req.user.id : null,
      businessId: req.user ? req.user.business_id : null,
      error,

    })
    
    errorLogger.apply(this, [error]);

  }
  return logger;
}


/**
 * Returns the log file name
 */
function getLogFileName() {
  return "_log_peletok_" + new Date(new Date().toString().split('GMT')[0] + ' UTC').toISOString().replace(/:/g, '-').slice(0, 13) + LOG_SUFFIX
}

module.exports = getLogger();