const BaseModel = require('../../BaseModel');
const DataTypes = require("sequelize").DataTypes;

/**
 * ProductDetails model
 */
class ProductDetails extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            product_name: {
                type: DataTypes.STRING,
                allowNull: true
            },
            product_title: {
                type: DataTypes.STRING,
                allowNull: true
            },
            product_description: {
                type: DataTypes.STRING,
                allowNull: true
            }
        };
        this.prefix = "SUPL";
        this.options["indexes"] = [{unique: true, fields: ["product_name"], where: {"deleted_at": null}}]

    }

    /**
     * creates associate for ProductDetails model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.product.model, { as: 'Product' });
        this.model.belongsTo(models.language.model, { as: 'Language' });
        this.model.belongsTo(models.user.model, { as: 'User', foreignKey: "seller_id" });
    }
}
module.exports = new ProductDetails();
