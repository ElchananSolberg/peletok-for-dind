var BaseModel = require('../../BaseModel.js');
const DataTypes = require("sequelize").DataTypes;

/**
 * ActivitySessions model
 */
class ActivitySession extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            passport_session: {
                type: DataTypes.STRING(255),
                allowNull: true
            }
        };

        this.prefix = 'USR'

    }
     /**
     * creates associate for ActivitySessions model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.user.model, { as: 'User' });
        this.model.belongsTo(models.userIp.model, { as: 'UserIp' });
        this.model.belongsTo(models.userToken.model, { as: 'UserToken' });

    } 


}

module.exports = new ActivitySession();