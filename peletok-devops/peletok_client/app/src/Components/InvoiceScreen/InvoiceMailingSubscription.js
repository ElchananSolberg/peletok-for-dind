import React, { Component } from 'react';
import './Invoice.css';

import { Button, Row, Col, Collapse } from 'reactstrap';

import { postInvoiceSubscribeEmail } from '../DataProvider/DataProvider'

import InputTypeEmail from '../InputUtils/InputTypeEmail';
import InputTypeCheckBox from '../InputUtils/InputTypeCheckBox'

import { LanguageManager as LM } from '../LanguageManager/Language';
import { Notifications } from '../Notifications/Notifications';
import { Loader } from '../Loader/Loader';

/** A children of <Invoice> */
export default class InvoiceMailingSubscription extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isAuthorized: false,
        };
    }

    authorizeSubscriptionChanged = (_e) => {
        const value = this.authorizeSubscription.getValue().value;
        this.setState({ isAuthorized: value });
    }

    signUpClicked = (_e) => {
        if (this.email.getValue().valid) {
            const value = this.email.getValue().value;
            Loader.show();
            postInvoiceSubscribeEmail({ address: value }
            ).then(
                (res) => {
                    Loader.hide();
                    Notifications.show(LM.getString("signUpWasSuccessful") , 'success');
                    this.props.setIsSubscribeTrue();
                }
            ).catch(
                (err) => {
                    Loader.hide();
                    Notifications.show(err, 'danger');
                }
            )
        }
    }

    render() {
        const emailProps = {
            placeholder: 'Example: mymail@google.com',
            required: true,
            title: LM.getString("enterMail") + ":",
            id: "invoiceMailingSubscription_Email",
        };

        return (
            <>
                <Row>
                    <Col>
                        הסכמה לקבלת חשבוניות אלקטרוניות באתר חברת פלאטוק www.peletop.co.il
                    </Col>
                </Row>
                <br />
                <Row>
                    <Col>
                        א. הריני מסכים/ה בזאת כי מעתה פלאטוק מסחר ושיווק בע"מ יספקו לי: "יוסי ניסוי" מספר לקוח: "5002" את החשבוניות החודשיות שלי דרך ממשק באתר החברה www.peletop.co.il בתצורה של חשבונית אלקטרונית, בצורה מאובטחת תוך שמירה על חיסיון המידע המצוי בהם. ידוע לי ואני מסכים/ה כי קבלת חשבונות בממשק האתר באמצעות השם משתמש והסיסמה האישיים שלי ותיחשב כקבלתה על- ידי לכל דבר ועניין.
                    </Col>
                </Row>
                <br />
                <Row>
                    <Col>
                        ב. כמו כן ידוע לי שלא חלה עליי חובה חוקית למסור נתונים אלה ומסירתם היא לפי רצוני בלבד.
                    ידוע לי כי שבכל עת אוכל לבקש לבטל בקשתי זו, ולקבל את החיוב התקופתי באמצעות הדואר.
                    </Col>
                </Row>
                <br />
                <Row>
                    <Col>
                        ג. יודגש כי ביכולתי לצרף דואר אלקטרוני בממשק האתר ושאליו אני אוכל לשלוח העתק של החשבונית החודשית.
                    </Col>
                </Row>
                <br />
                <Row>
                    <Col>
                        ד. באישור כל התנאים לעיל אני מודע שאני מוותר מרגע אישור מסמך זה על קבלת החשבוניות החודשיות במשלוח דואר כתוב.
                    </Col>
                </Row>
                <br />
                <Row>
                    <Col sm="3">
                        קראתי את ההסכם, ואני מאשר.
                    </Col>
                    <Col>
                        <InputTypeCheckBox
                            id="invoiceMailingSubscription_AuthorizeSubscription"
                            onChange={this.authorizeSubscriptionChanged}
                            ref={(componentObj) => { this.authorizeSubscription = componentObj }}
                        />
                    </Col>
                </Row>
                <Collapse isOpen={this.state.isAuthorized}>
                    <br />
                    <Row className="align-items-center" >
                        <Col sm="4">
                            <InputTypeEmail
                                {...emailProps}
                                ref={(componentObj) => { this.email = componentObj }}
                            />
                        </Col>
                        <Col sm="auto">
                            <Button
                                className="border-radius-3 btn-lg btn-md-block font-weight-bold regularButton px-4 mx-3 mx-md-0 fs-17"
                                id="invoiceMailingSubscription_SignUp"
                                onClick={this.signUpClicked}
                            >
                                {LM.getString("signUp")}
                            </Button>

                        </Col>
                    </Row>
                </Collapse>
                <br />
            </>
        )
    }
}