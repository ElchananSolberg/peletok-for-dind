# Change these four parameters as needed for your own environment
AKS_PERS_STORAGE_ACCOUNT_NAME=volumeforfiles
AKS_PERS_RESOURCE_GROUP=akssharefilesgroup



# Export the connection string as an environment variable, this is used when creating the Azure file share
export AZURE_STORAGE_CONNECTION_STRING=$(az storage account show-connection-string -n $AKS_PERS_STORAGE_ACCOUNT_NAME -g $AKS_PERS_RESOURCE_GROUP -o tsv)

# Get storage account key
STORAGE_KEY=$(az storage account keys list --resource-group $AKS_PERS_RESOURCE_GROUP --account-name $AKS_PERS_STORAGE_ACCOUNT_NAME --query "[0].value" -o tsv)

# Echo storage account name and key
echo Storage account name: $AKS_PERS_STORAGE_ACCOUNT_NAME
echo Storage account key: $STORAGE_KEY


# See instructions for it
# https://docs.microsoft.com/en-us/azure/aks/azure-files-volume