import React, { Component } from 'react';
import { LanguageManager as LM } from "../../LanguageManager/Language";
import { MenuLargeButton } from "../Utils/MenuLargeButton";
import IconCharge from "../../../Assets/Icons/MenuIcons/charge.svg";
import IconReports from '../../../Assets/Icons/MenuIcons/acount-status.svg'
import {ManagerPaymentsEnum} from "../../../enums";
// TODO get updated SVGs

export class ReportMenu extends Component {
    render() {
        let reportsList = [
            { name: LM.getString("transactions"), id: ManagerPaymentsEnum.TRANSACTION },
            { name: LM.getString("payments"), id: ManagerPaymentsEnum.PAYMENT },
            { name: LM.getString("actions"), id: ManagerPaymentsEnum.ACTION },
            { name: LM.getString("actionsAndPayments"), id: ManagerPaymentsEnum.ACTIONANDPAYMENT },
            { name: LM.getString("distributionActivities"), id: ManagerPaymentsEnum.DISTRIBUTIONACTIVITIES },
            // { name: LM.getString("distributionActivities"), id: "distributionActivities" },
            { name: LM.getString("obligo"), id: ManagerPaymentsEnum.OBLIGO },
            { name: LM.getString("manualCards"), id: ManagerPaymentsEnum.MANUALCARDS },
            { name: LM.getString("cardsAssemblage"), id: ManagerPaymentsEnum.CARDSASSEMBLAGE },
            { name: LM.getString("emailReport"), id: ManagerPaymentsEnum.EMAILREPORT },
            { name: LM.getString("remainderByDate"), id: "remainderByDate" },
        ];
        // let paymentsReportList = [
        //     { name: "FILL ME", id: "general" },
        // ];
        let hashavshevetList = [
            { name: LM.getString("dueToDate_of_ExportationToHashavshevet_Report"), id: "export/orderByDate" },
            { name: LM.getString("commissionsExportedToHashavshevetReport"), id: "export/CommissionReport" },
            { name: LM.getString("ordersExportedToHashavshevetReport"), id: "export/report" },
            { name: LM.getString('exportCommissionsToHashavshevetReport'), id: 'export/CommissionExport'},
            { name: LM.getString('exportOrdersToHashavshevetReport'), id: 'export/profitExport'},
            { name: LM.getString('hashavshevetPaymentsReport'), id: 'hashavshevetPaymentsReport'},
            { name: LM.getString('importReceiptsFromHashavshevet'), id: 'importReceiptsFromHashavshevet'}
        ];
        let dataAnalysisList = [
            { name: LM.getString("inactive"), id: "inactive" },
            { name: LM.getString("activityDistribution"), id: "activityDistribution" },
            { name: LM.getString("resellerDiscount"), id: "resellerDiscount" },
            { name: LM.getString("userLogins"), id: "userLogins" },
            { name: LM.getString("giftOrders"), id: "giftOrders" },
        ];
        return (
            <div className="rounded" style={{ overflow: "hidden" }}>
                <MenuLargeButton
                    list={reportsList}
                    path={`${this.props.match.url}/reports`}
                    icon={IconReports}
                    text={LM.getString("reports")}
                    location={this.props.location}
                    toggle={this.props.toggle}
                />
                {/* <MenuLargeButton
                    list={paymentsReportList}
                    path={`${this.props.match.url}/paymentsReport`}
                    icon={IconCharge}
                    text={LM.getString("paymentsReport")}
                    location={this.props.location}
                    toggle={this.props.toggle}
                /> */}
                <MenuLargeButton
                    list={hashavshevetList}
                    path={`${this.props.match.url}/hashavshevet`}
                    icon={IconCharge}
                    text={LM.getString("hashavshevet")}
                    location={this.props.location}
                    toggle={this.props.toggle}
                />
                <MenuLargeButton
                    list={dataAnalysisList}
                    path={`${this.props.match.url}/dataAnalysis`}
                    icon={IconCharge}
                    text={LM.getString("dataAnalysis")}
                    location={this.props.location}
                    toggle={this.props.toggle}
                />
            </div>
        );
    }
}