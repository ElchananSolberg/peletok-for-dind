import React, { Component } from 'react';
import { LanguageManager as LM } from '../LanguageManager/Language';
import './selectUser.css';
import { Container, Row, Col, Button } from 'reactstrap';
import { getUsersDocuments } from '../DataProvider/Functions/DataGeter'
import { Document } from './Document'



export class UserDocuments extends Component {
    constructor(props) {
        super(props);


        this.state = {
            UsersDocData: [

            ]
        }
    }
    componentDidMount() {
        getUsersDocuments(this.props.id).then(
            (res) => {
                this.setState({ UsersDocData: res })

            }).catch(
                (err) => {
                    alert(err);
                }
            )
    }
    delete = () => {

    }
    render() {

        return (
            <React.Fragment>
                <Container>
                    <Row style={{ margin: '3px' }} >
                        <Col sm='3'>
                            <p style={{ textAlign: "start", fontSize: "0.75rem", fontWeight: 'bold' }}>{LM.getString("name")}</p>
                        </Col>
                        <Col sm='3'>
                            <p style={{ textAlign: "start", fontSize: "0.75rem", fontWeight: 'bold' }}>{LM.getString("type")}</p>
                        </Col>
                        <Col sm='3'>
                            <p style={{ textAlign: "start", fontSize: "0.75rem", fontWeight: 'bold' }}>{LM.getString("validity")}</p>
                        </Col>
                        <Col sm='3'>
                        </Col>
                    </Row>
                    {
                        this.state.UsersDocData ? this.state.UsersDocData.map((doc, index) =>
                            <Document updateUsers= {this.props.updateUsers}{...doc} user_id={this.props.id} />
                        )
                            : <React.Fragment />
                    }
                </Container>
            </React.Fragment>

        )
    }
}