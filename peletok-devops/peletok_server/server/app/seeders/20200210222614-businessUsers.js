'use strict';
const moment = require('moment');
module.exports = {
  up: (queryInterface, Sequelize) => {
    let dateNow = moment().toDate();
    let businessUser = [
      {
        id: 1, business_id: 1, user_id: 4,
        created_at: dateNow,
        updated_at: dateNow,
      },
      // {
      //   id: 10, business_id: 1, user_id: 13,
      //   created_at: dateNow,
      //   updated_at: dateNow,
      // },
      // {
      //   id: 2, business_id: 2, user_id: 5,
      //   created_at: dateNow,
      //   updated_at: dateNow,
      // },
      // {
      //   id: 3, business_id: 3, user_id: 6,
      //   created_at: dateNow,
      //   updated_at: dateNow,
      // },
      // {
      //   id: 4, business_id: 4, user_id: 7,
      //   created_at: dateNow,
      //   updated_at: dateNow,
      // },
      // {
      //   id: 5, business_id: 5, user_id: 8,
      //   created_at: dateNow,
      //   updated_at: dateNow,
      // },
      // {
      //   id: 6, business_id: 6, user_id: 9,
      //   created_at: dateNow,
      //   updated_at: dateNow,
      // },
      // {
      //   id: 7, business_id: 7, user_id: 10,
      //   created_at: dateNow,
      //   updated_at: dateNow,
      // },
      // {
      //   id: 8, business_id: 8, user_id: 11,
      //   created_at: dateNow,
      //   updated_at: dateNow,
      // },
      // {
      //   id: 9, business_id: 9, user_id: 12,
      //   created_at: dateNow,
      //   updated_at: dateNow,
      // },
    ]
    return queryInterface.bulkInsert('BSN_UserBusiness', businessUser, {});

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('BSN_UserBusiness', null, {});

  }
};
