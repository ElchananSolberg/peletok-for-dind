import React, { Fragment } from 'react';
import { Button } from 'reactstrap';
import { putBaner } from '../DataProvider/Functions/DataPuter';
import InputTypeCheckBox from '../InputUtils/InputTypeCheckBox';
import { LanguageManager as LM } from "../LanguageManager/Language";


/**
 * BanerTableRow - component that builds and returns an array of <td> elements from the data sended by baners
 */
export default class BannerTableRow extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            checked: false,
            iAmEdit: this.props.SomeLineIsChangingNow,
        }
    }

    activeCheckBox = () => {

        if (this.checkBoxAct.getValue().value) {
            this.putBanerFun(this.props.tableRowObj.id, { "status": 2 })
        }
        else {
            this.putBanerFun(this.props.tableRowObj.id, { "status": 1 })
        }

    }
    putBanerFun = (id, obj) => {
        putBaner(id, obj).then(
            (res) => {

            }
        ).catch(
            (error) => {
                if (error && error.response && error.response.data && error.response.data[0].message) {
                    alert(error.response.data[0].message);
                }
                else (alert(LM.getString("unrecognizedError")))
            }
        )
    }
    editFun = () => {
        if (!this.props.SomeLineIsChangingNow) {

            this.setState({ iAmEdit: true })
            if (this.props.editRowFromTable) {
                this.props.editRowFromTable(this.props.tableRowObj.id)
            }
            if (this.props.onClick) {
                this.props.onClick()
            }
        }
        else {
            alert(LM.getString("DuplicateEditErrorMessage"))
        }
    }
    unEditFun = () => {
        this.iAmNotEditFun()
        if (this.props.cleanPage) {
            this.props.cleanPage()
        }
        if (this.props.onClick) {
            this.props.onClick()
        }
    }
    getId = () => {
        return this.props.tableRowObj.id
    }
    iAmNotEditFun = () => {
        this.setState({ iAmEdit: false })
    }



    render() {
        var tableRowObj = this.props.tableRowObj;
        let button =
            this.state.iAmEdit ?
                <Button className='mr-auto tagsManagementTableBtn tagsManagementTableBtnDelete fs-17' onClick={this.unEditFun} id={`edit_${this.props.id}`} >{LM.getString("selectUseredit")}</Button> :
                <Button className='mr-auto tagsManagementTableBtn tagsManagementTableBtnEdit fs-17' onClick={this.editFun} id={`edit_${this.props.id}`} >{LM.getString("selectUseredit")}</Button>

        var tdArray = [];
        var mapKey = 0;
        tdArray.push(<td className='font-weight-bold fs-13 pr-3' style={{ width: '40%' }} key={mapKey++}>{tableRowObj["name"]}</td>)
        tdArray.push(<td className='fs-13 pr-3' style={{ width: '30%' }} key={mapKey++}>{tableRowObj["order"]}</td>)
        tdArray.push(<td key={mapKey++}>
            <div className={'d-flex custom-checkbox pr-2 ' + (LM.getDirection() === "rtl" ? "checkbox-rtl" : "")}>
                <InputTypeCheckBox id={`isActiveCheckBoxProps_${this.props.id}`} checked={tableRowObj["status"] == 1} lableText={tableRowObj["status"] == 1 ? LM.getString("active") : LM.getString("notActive")} ref={(componentObj) => { this.checkBoxAct = componentObj }}
                    onChange={this.activeCheckBox} />
                {button}
            </div>
        </td>
        )
        return (
            <Fragment>
                {tdArray}
            </Fragment>
        );
    }
}