import axios from 'axios';
import { AuthService } from '../../../services/authService';
import {
    loadedData,
    DEFAULT_REFRESH_TIME,
    getData,
    SERVER_BASE,
    shouldRefetch
} from './../DataProvider'

/**
 * general post function that stores its data and returns it if up to date
 * @param {*} url the url to post
 * @param sentData
 * @param config
 * @param replaceWithBusinessId
 */
function dataPoster(url, sentData = {}, config = {}, replaceWithBusinessId = true) {
    let data = getData(sentData, config, replaceWithBusinessId);
    return new Promise(function (resolve, reject) {
        axios.post(SERVER_BASE + AuthService.addQueryParams(url), data, Object.assign({ withCredentials: true }, config)).then(
            function (response) {
                resolve(response.data);
            }
        ).catch(
            function (error) {
                // TODO - add real error handling
                reject(error);
            }
        );
    });
}

/*
 * ########################################
 *       ONLY WRITE BELLOW THIS LINE
 * ########################################
 */

export function postIsSubscribe(data, forceReload = true) {
    return dataPoster("/invoice/is_subscribe", data);
}

export function postHashavshevetExpoertOrder(data, config = {}, forceReload = true) {
    return dataPoster(`/hashavshevet/export_xml`, data);
}

export function postHashavshevetImport(data, forceReload = true) {
    return dataPoster("/hashavshevet/import", data);
}

export function postHashavshevetShowReport(data, forceReload = true) {
    return dataPoster("/hashavshevet/show_report", data);
}

export function postDuplicatProductAout(data, forceReload = true) {
    return dataPoster("/product/clone_porduct_auth", data);
}

export function postCreditCards( data) {
    return dataPoster(`/credit_card/`, data);
}

export function postVat(data, forceReload = true) {
    return dataPoster("/config/vat", data);
}

export function postSendIvoiceByMail(data, forceReload = true) {
    return dataPoster("/invoice/send_email", data);
}

export function postUnsubscribe(data, forceReload = true) {
    return dataPoster("/invoice/unsubscribe", data, {}, false);
}

export function postNewBanner(data) {
    return dataPoster(`/banner`, data);
}

export function postCompleetCancelTransaction(supplierId, data) {
    return dataPoster(`/payment/cancel/${supplierId}`, data);
}

export function postRejectCancelTransaction(supplierId, data) {
    return dataPoster(`/payment/cancel/reject/${supplierId}`, data);
}

export function postPermissionRoles(data) {
    return dataPoster(`/permission_roles`, data);
}

export function postNewUsersData(data) {
    return dataPoster(`/user`, data, {}, false);
}

export function virtualChargePost(supplierID, data) {
    return dataPoster(`/payment/payment/${supplierID}`, data);
}
export function manualChargePost(data) {
    return dataPoster(`/payment/payment/1`, data);
}

export function freezeUser(id) {
    return dataPoster(`/user/frozen/${id}`);
}
export function defrostUser(id) {
    return dataPoster(`/user/activate/${id}`);
}
export function routesChargePost(supplierid, data) {
    return dataPoster(`/payment/payment/${supplierid}`, data)
}
export function paymentIECTotalPost(total) {
    return dataPoster('/payment/payment/185', total)
}
export function paymentIECaccountPost(billNum) {
    return dataPoster('/payment/details/185', billNum)
}
export function ChargingElectricityBillAccountPost(sentIce) {
    return dataPoster('/payment/payment/186', sentIce)
}
export function postOrderGifts(data) {
    return dataPoster(`/gift/gift_order/`, data, { withCredentials: true });
}
export function postNewSuppliers(data) {
    return dataPoster(`/supplier/`, data);
}
export function postNewManualCard(newCard) {
    return dataPoster(`/product/manual_card`, newCard);
}
export function postProduct(data) {
    return dataPoster(`/product/`, data);
}
export function postDocument(data, userId) {
    return dataPoster(`/user_doc/${userId}`, data);
}
export function postNewGifts(data) {
    return dataPoster(`/gift/gift`, data);
}
export function postMessage(data) {
    return dataPoster(`/message/`, data);
}
export function postFavoriteMessage(data) {
    return dataPoster(`/message/favorite_message`, data);
}
export function cityPaymentShowInvoicePost(data) {
    return dataPoster(`/payment/details/193`, data)
}
export function cityConfirmPaymentPost(data) {
    return dataPoster(`/payment/payment/193`, data)
}
export function postDistributor(data) {
    return dataPoster(`/distributor/distributor/`, data);
}
export function postSeller(data) {
    return dataPoster(`/business/business`, data, {}, false);
}
export function postTag(data) {
    return dataPoster(`/tag/tag/`, data);
}
export function postUnpaidBill(data) {
    return dataPoster(`/transaction/Unpaid`, data);
}
export function postNewBarcode(data) {
    return dataPoster('/barcode_type', data)
}
export function postForPrepaidCancelDetails(data, supplierID) {
    return dataPoster(`/payment/cancelDetails/${supplierID}`, data, {}, false)
}
export function postCardCancel(data, supplierID) {
    return dataPoster(`/payment/cancel/${supplierID}`, data, {}, false) /** false - please do not replace id with business_identifier */
}
export function postBusinessSuppliers(data) {
    return dataPoster(`/business/business_supplier/`, data, {}, false);  /** false - please do not replace id with business_identifier */
}
export function postSuppliersProfile(data) {
    return dataPoster(`/business/profile/`, data, {});
}
export function postProfitPercentageProfile(data) {
    return dataPoster(`/profit/profile/`, data, {});
}
export function favoriteStarPost(id, data) {
    return dataPoster(`/product/favorite/${id}`, data)
}
export function postServerSmsChecker(username, smsCode) {
    return dataPoster(`/reset/check_verification_code/?username=${username}&verificationCode=${smsCode}`, false);
}
export function postResetPassWithVrarificationCode(username, smsCode, newPassword) {
    let data = { "new_password": newPassword }
    return dataPoster(`/reset/reset_with_verification_code/?username=${username}&verificationCode=${smsCode}`, data, {}, false);
}
export function postCancelTransaction(transactionId, config = {}, forceReload = true) {
    return dataPoster(`/transaction/cancelation_request/`, { transactionId: transactionId }, config);
}
export function postActivitySessionIP(data) {
    return dataPoster(`/activity_session/ip`, data, {}, false);  /** false - please do not replace id with business_identifier */
}
export function postActivitySessionToken(data) {
    return dataPoster(`/activity_session/token`, data, {}, false);  /** false - please do not replace id with business_identifier */
}
export function postActivitySessionIPCancel(data) {
    return dataPoster(`/activity_session/ip/cancel`, data, {}, false);  /** false - please do not replace id with business_identifier */
}
export function postActivitySessionTokenCancel(data) {
    return dataPoster(`/activity_session/token/cancel`, data, {}, false);  /** false - please do not replace id with business_identifier */
}
export function postRole(data) {
    return dataPoster(`/roles`, data, {});
}
export function saveInvoicePost(data) {
    return dataPoster('/invoice', data, {}, false)
}
export function postInvoiceBusinessId(data) {
    return dataPoster(`/invoice/is_subscribe`, data, {}, false);
}
export function postInvoiceSubscribeEmail(data) {
    return dataPoster(`/invoice/subscribe`, data, {});
}

export function postChargeCancel(data, supplier) {
    return dataPoster(`/payment/revert/${supplier}`, data, {}, false);
}
export function postConfirmTheDeal(data, supplier) {
    return dataPoster(`/payment/approval/${supplier}`, data, {}, false);
}
export function postCancelTheDeal(data, supplier) {
    return dataPoster(`/payment/iec_cancel/${supplier}`, data, {}, false);
}
export function postConfirmationNumberSave(data, supplier){
    return dataPoster(`/payment/confirmationNumberSave/${supplier}` , data, {}, false)
}
export function postStartRavKavSession(){
    return dataPoster(`/rav_kav/` , {}, {}, false)
}
export function postRavKavGetProducts(data){
    return dataPoster(`/rav_kav/getProducts/`, data)
}

export function postBarcodeTesting(data) {
    // return dataPoster('/product/validate_barcode/', data, {}, false);
    return Promise.resolve(true)
}
export function postBezeqPaymentDetails(data, supplierId) {
    return dataPoster(`/payment/details/${supplierId}`, data)
}
export function postBezeqPayment(data, supplierId){
    return dataPoster(`/payment/payment/${supplierId}`, data, {}, false)
}