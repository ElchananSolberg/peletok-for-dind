import React, { Component } from 'react';
import { LanguageManager as LM } from '../LanguageManager/Language';
import { config } from '../../utils/Config';
import img from './sample_image.png';
import './Banner.css';
import { getAllBanners } from '../DataProvider/DataProvider'
import { AuthService } from '../../services/authService'



/**
 *Component that displays a full-screen advertisement, receives the advertisement from the server, and sets a time to display if not * * downloaded.
 * But the user can give her an advertisement, and also give her a time to show it
 */

export class LigthBox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: true,
            allBanners: [],
            banner: this.props.banner ? this.props.banner : '',
            adUrl: config.baseUrl,

        }

    }

    componentDidMount() {
        getAllBanners()
            .then(
                (res) => {
                    this.setState({ allBanners: res }, () => {
                        if (!this.props.banner) {

                            this.state.allBanners.forEach(element => {

                                if (element.location == "לייטבוקס") {
                                    this.setState({ banner: element })
                                    return
                                }
                            })
                        }

                    })

                }
            )
            .catch(
                (err) => {
                    alert(LM.getString("loadBannersListFailed") + err);
                }
            )
        setTimeout(() => {
            this.setState({ isOpen: false })
        }, this.props.time != undefined ? this.props.time : 15000)
    }
    bannerClicked = () => {

        window.open(this.state.banner !== undefined ? this.state.banner.link_url : this.state.adUrl);
    };
    closeBanner = () => {

        this.setState({ isOpen: false })
    };

    render() {

        return (

            this.state.isOpen && !AuthService.allowed('/prevent_baners') ? <div>
                <h5 onClick={this.closeBanner} className="myX">x</h5>
                <img className='LigthBox' onClick={this.bannerClicked} src={this.state.banner && this.state.banner.banner_url !== undefined ? (config.baseUrl + this.state.banner.banner_url) : img} alt={"AD"} />
            </div> : <></>


        )
    }
}