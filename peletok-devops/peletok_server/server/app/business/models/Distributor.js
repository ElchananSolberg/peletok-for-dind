const DataTypes = require('sequelize').DataTypes;
const BaseModel = require('../../BaseModel');

/**
 * Distributor model
 */

class Distributor extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            free_text: {
                type: DataTypes.STRING(128),
                allowNull: true
            },
            is_admin: {
                type: DataTypes.BOOLEAN,
                allowNull: true
            },
            theme: {
                type: DataTypes.STRING,
                allowNull: true
            },
            support_phone: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            sub_domain: {
                type: DataTypes.STRING,
                allowNull: true
            }
        };
        this.prefix = "BSN";
    }

    /**
     * creates associate for Distributer model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.hasMany(models.business.model, {as: 'Business' });
        this.model.hasMany(models.bannerDistributor.model, {as: 'BannerDistributor' });
        this.model.hasMany(models.distributorDocument.model, {as: 'DistributorDocument' });
        this.model.belongsTo(models.user.model, { as: 'User' });


    }
}

module.exports = new Distributor();
