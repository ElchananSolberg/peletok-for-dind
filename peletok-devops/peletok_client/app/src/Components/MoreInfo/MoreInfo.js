import React from 'react';
import './MoreInfo.css'
import {
    Container,
    Row,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    ModalFooter,
    Button,
    Table,
    Input,
    Label,

} from 'reactstrap'
import { LanguageManager as LM } from '../LanguageManager/Language'
import { SERVER_BASE } from '../DataProvider/DataProvider';
import ProductText from '../Ui/ProductText';
import PropTypes from 'prop-types';
import sanitizeHtml from 'sanitize-html';
import printJs from 'print-js'

export class MoreInfo extends React.Component {
    isOpen = false;

    constructor(props) {
        super(props);
        this.state = {
            filterString: '',
            talkPrices: props.product.talk_prices
        }

    }
    /**
     * @ product    {
     *                name_he: string;  
     *                name_en: string;  
     *                name_ar: string;  
     *                description: string;  
     *                price: number;  
     *              }
     *
     * @moreInfo    { description:string;
     *                image_url: string;
     *                youtube_url: string;
     *                site_url: string;
     *                talkPrices: [
     *                    {
     *                        country: string;
     *                        outcomePrice: number;
     *                        incomePrice: number;
     *                        smsPrice:number;
     *                    },
     *                ]}
     */

    static propTypes = {
        product: PropTypes.object.isRequired,
        isOpen: PropTypes.bool,
        toggle: PropTypes.func,
        color: PropTypes.string.isRequired
    }

    isBlankImage(imgUrl) {
        // the file input return url by default
        return imgUrl == 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAooAAAHCAQMAAABSWXpTAAAAA1BMVEX///+nxBvIAAAAOklEQVR42u3BAQ0AAADCIPuntscHDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIOGR5gABvI0ScgAAAABJRU5ErkJggg=='
    }

    getIdFromYoutubeUrl(url) {
        let urlArr = url.split('/');
        return urlArr[urlArr.length - 1]
    }

    filterTalkPrices = (e) => {
        let testStart = new RegExp('^' + e.target.value);
        this.setState({
            filterString: e.target.value,
            talkPrices: this.props.product.talk_prices.filter(tp => e.target.value ? testStart.test(tp.country) : true)
        })
    }

    static getDerivedStateFromProps(props, state) {
        let testStart = new RegExp('^' + state.filterString);
        return Object.assign(state, { talkPrices: props.product.talk_prices ? props.product.talk_prices.filter(tp => state.filterString ? testStart.test(tp.country) : true) : [] })
    }

    render() {

        this.isOpen = this.props.isOpen;
        return (
            <Modal size="lg" isOpen={this.isOpen} toggle={this.props.toggle} >
                <div id="printJS-content">
                    <ModalHeader className="header-group" toggle={this.props.toggle} style={{ backgroundColor: this.props.color }}>
                        <Container >

                        </Container>
                    </ModalHeader  >
                    <ModalBody id="header-content" className="header-group" toggle={this.props.toggle} style={{ backgroundColor: this.props.color }} >
                        <Container >
                            <div className={'d-flex justify-content-between'}>
                                <h2 id="product-name" dangerouslySetInnerHTML={{ __html: LM.getStringFromDb(this.props.product, 'name') }}></h2>
                                <h2 id="product-price" dangerouslySetInnerHTML={{ __html: `${sanitizeHtml(this.props.product.price)} ${this.props.product.price ? '&#8362;' : ''}` }}></h2>
                            </div>
                            <ProductText product={this.props.product} withSeperator={true} />
                        </Container>
                    </ModalBody>
                    <ModalBody id="more-info-body">
                        <Container >
                            {(this.props.product.talk_prices || []).length > 0 &&
                                <span>
                                    <Row>
                                        <h5>
                                            {LM.getString('wordTalkPrices')}
                                        </h5>
                                    </Row>
                                    <Row className={'search'}>
                                        <Label for="search-country" sm={1}>{LM.getString('search')}</Label>
                                        <Col sm="5" className='mb-3'>
                                            <Input id="search-country" onChange={this.filterTalkPrices} />
                                        </Col>
                                    </Row>
                                    <Row className={'table-height'}>
                                        <Table id="more-info-table" striped bordered responsive size="sm">
                                            <thead>
                                                <tr>
                                                    <th>{LM.getString('country')}</th>
                                                    <th>{LM.getString('outcomePrice')}</th>
                                                    <th>{LM.getString('imcomePrice')}</th>
                                                    <th>{LM.getString('smsPrice')}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    this.state.talkPrices.map(talkPrice => {
                                                        return (<tr>
                                                            <td>{talkPrice.country}</td>
                                                            <td>{talkPrice.outcomePrice}</td>
                                                            <td>{talkPrice.incomePrice}</td>
                                                            <td>{talkPrice.smsPrice}</td>
                                                        </tr>)
                                                    })
                                                }
                                            </tbody>
                                        </Table>
                                    </Row>
                                </span>
                            }
                            {this.props.product.more_info_description &&
                                <span>
                                    <Row>
                                        <h5>
                                            {LM.getString('moreInfoDescription')}
                                        </h5>
                                    </Row>
                                    <Row className="content-row">
                                        <p>
                                            {this.props.product.more_info_description}
                                        </p>
                                    </Row>
                                </span>
                            }
                            {this.props.product.more_info_image && !this.isBlankImage(this.props.product.more_info_image) &&
                                <Row className="content-row">
                                    <img id="more-info-img" src={/^data/.test(this.props.product.more_info_image) ? this.props.product.more_info_image : SERVER_BASE + this.props.product.more_info_image} alt="product image" />
                                </Row>
                            }
                            {this.props.product.more_info_youtube_url &&
                                <Row className="content-row video">
                                    <iframe
                                        width="560"
                                        height="315"
                                        src={`https://www.youtube.com/embed/${this.getIdFromYoutubeUrl(this.props.product.more_info_youtube_url)}`}
                                        frameborder="0"
                                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen
                                    ></iframe>
                                </Row>
                            }
                            {this.props.product.more_info_site_url &&
                                <Row className="content-row">
                                    <a href={this.props.product.more_info_site_url} target="_blank">
                                        {LM.getString('siteUrlTitle')}
                                    </a>
                                </Row>
                            }

                        </Container>
                    </ModalBody>
                </div>
                <ModalFooter style={{ alignItems: 'center', justifyContent: 'center' }}>

                    <Button
                        style={{ background: 'var(--regButton)', margin: 'var(--marginButton)' }}
                        onClick={() => window.print()}
                    >
                        {LM.getString('printButton')}
                    </Button>
                    {'    '}
                    <Button
                        color="secondary"
                        onClick={this.props.toggle}
                    >
                        {LM.getString('cancel')}
                    </Button>
                </ModalFooter>
            </Modal>
        );
    }
}
