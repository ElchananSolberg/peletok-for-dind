import React, { Component } from 'react';
import './CopyBusinessProfile.css';
import { Button, Container, Row, Col } from 'reactstrap';

import { LanguageManager as LM } from '../LanguageManager/Language';

/** TODO: getAgents() should be added to DataProvider or the name of this imported function should be changed */
import { getSellers, getAgents } from '../DataProvider/DataProvider';

import InputTypeSearchList from '../InputUtils/InputTypeSearchList';
import InputTypeNumber from '../InputUtils/InputTypeNumber';

/**
 * Main component of CopyBusinessProfile screen.  
 */
export default class CopyBusinessProfile extends Component {
    constructor(props) {
        super(props);

        this.state = {

        }
    }
    render() {
        /** Screen Header text */
        const header = LM.getString('copyResellerProfile');
        /** "Select a Source Seller" sub header text */
        const selectSourceSellerHeader = LM.getString('selectSourceSeller');
        /** "Select a Target Seller" sub header text */
        const selectTargetSellerHeader = LM.getString('selectTargetSeller');

        /** data for "Select Agent" fields (InputTypeSearchList InputUtils component) */
        const selectAgentProps = {
            title: LM.getString("selectAgent") + ":",
            options: [],
        };
        /** data for "Select Seller" fields (InputTypeSearchList InputUtils component) */
        const selectSellerProps = {
            title: LM.getString("selectSeller"),
            options: [],
        };

        /** data for "Type Terminal Number" fields (InputTypeNumber InputUtils component) */
        const terminalNumberProps = {
            title: LM.getString("typeTerminalNumber"),
        }

        /** data for "Search" button  */
        const buttonSearch = LM.getString('search');
        /** data for "Save" button */
        const buttonSave = LM.getString('save');

        return (
            <Container className="copyBusinessProfileContainer">
                <Row >
                    <Col className="copyBusinessProfileHeader">
                        {header}
                    </Col>
                </Row>
                <Container className="copyBusinessProfileSettings">
                    <Row >
                        <Col className="copyBusinessProfileSubHeader">
                            {selectSourceSellerHeader}
                        </Col>
                    </Row>
                    <Row>
                        <Col sm='4'>
                            <InputTypeSearchList
                                {...selectAgentProps}
                                onChange={this.sourceAgentChanged}
                                // options={this.state.agents.map(agent => agent.name)}
                                ref={(componentObj) => { this.sourceSelectAgent = componentObj }}
                                id="CopyBusinessProfile_SourceSeller_SelectAgent"
                            />
                        </Col>
                        <Col sm='4'>
                            <InputTypeSearchList
                                {...selectSellerProps}
                                onChange={this.sourceSellerChanged}
                                // options={this.state.sellers.map(seller => seller.name)}
                                ref={(componentObj) => { this.sourceSelectSeller = componentObj }}
                                id="CopyBusinessProfile_SourceSeller_SelectSeller"
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm='4'>
                            <InputTypeNumber
                                {...terminalNumberProps}
                                ref={(componentObj) => { this.sourceTerminalNumber = componentObj }}
                                id="CopyBusinessProfile_SourceSeller_TerminalNumber"
                            />
                        </Col>
                        <Col sm='auto'>
                            <Button
                                className="copyBusinessProfileButton copyBusinessProfileButtonSearch"
                                onClick={this.sourceSellerSearchClicked}
                                id="copyBusinessProfile_SourceSeller_SearchButton"
                            >
                                {buttonSearch}
                            </Button>
                        </Col>
                    </Row>

                    <Row >
                        <Col className="copyBusinessProfileSubHeader">
                            {selectTargetSellerHeader}
                        </Col>
                    </Row>
                    <Row>
                        <Col sm='4'>
                            <InputTypeSearchList
                                {...selectAgentProps}
                                onChange={this.targetAgentChanged}
                                // options={this.state.agents.map(agent => agent.name)}
                                ref={(componentObj) => { this.targetSelectAgent = componentObj }}
                                id="CopyBusinessProfile_TargetSeller_SelectAgent"
                            />
                        </Col>
                        <Col sm='4'>
                            <InputTypeSearchList
                                {...selectSellerProps}
                                onChange={this.sourceTargetChanged}
                                // options={this.state.sellers.map(seller => seller.name)}
                                ref={(componentObj) => { this.targetSelectSeller = componentObj }}
                                id="CopyBusinessProfile_TargetSeller_SelectSeller"
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm='4'>
                            <InputTypeNumber
                                {...terminalNumberProps}
                                ref={(componentObj) => { this.targetTerminalNumber = componentObj }}
                                id="CopyBusinessProfile_TargetSeller_TerminalNumber"
                            />
                        </Col>
                        <Col sm='auto'>
                            <Button
                                className="copyBusinessProfileButton copyBusinessProfileButtonSearch"
                                onClick={this.targetSellerSearchClicked}
                                id="copyBusinessProfile_TargetSeller_SearchButton"
                            >
                                {buttonSearch}
                            </Button>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="auto" >
                            <Button
                                className="copyBusinessProfileButton"
                                onClick={this.buttonSaveClicked}
                                id="CopyBusinessProfileSaveButton"
                            >
                                {buttonSave}
                            </Button>
                        </Col>
                    </Row>
                </Container>
            </Container>
        )
    }
}