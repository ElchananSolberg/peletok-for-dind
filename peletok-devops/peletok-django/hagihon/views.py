from selenium.common.exceptions import TimeoutException
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import BasePermission
from rest_framework.decorators import api_view, permission_classes
from utils.elements_selectors import *
from utils.main_functions import MainFunctions


@api_view(['POST'])
@permission_classes([BasePermission])
def post_get_price(request):
    """
    {
    "url": "https://secure.hagihon.co.il/",
    "payment_info": {
        "client_id": "10340116",
        "bill": "1017873971"
        }
    }
    :param request:
    :return {status: <str> 'success' / 'error', price: <float> || error_message: <str>}
    """
    payment_handler = HagihonPayment(str(request.data.get("url")), request.data.get("payment_info"))
    res = payment_handler.get_price()
    return Response(res, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes([BasePermission])
def post_pay(request):
    payment_handler = HagihonPayment(str(request.data.get("url")), request.data.get("payment_info"))
    res = payment_handler.pay()
    return Response(res, status=status.HTTP_200_OK)


class HagihonPayment:

    def __init__(self, url: str, payment_info: dict):
        self.url = url
        self.payment_info = payment_info
        self.driver = MainFunctions(self.url)

    def first_steps(self):
        return_obj = {}
        try:
            self.driver.send_keys(Hagihon.idClientIidInput, self.payment_info["client_id"])
        except TimeoutException:
            return_obj["status"] = 'error'
            return_obj["error_message"] = "המערכת לא הצליחה להתחבר, אנא נסה שוב מאוחר יותר."
            return return_obj
        # self.driver.send_keys(id_client_id_input_hagihon, self.payment_info["client_id"])
        # print('---------------------')
        # # client_id = self.wait.until(EC.visibility_of_element_located(sel.id_client_id_input_hagihon))
        # # client_id.send_keys(self.payment_info["client_id"])
        #
        # self.driver.send_keys(id_bill_id_input_hagihon, self.payment_info["bill"])

        # bill_id = self.wait.until(EC.visibility_of_element_located(sel.id_bill_id_input_hagihon))
        # bill_id.send_keys(self.payment_info["bill"])

        # amount_button = self.wait.until(EC.element_to_be_clickable(sel.id_amount_button))
        # amount_button.click()
        #
        # self.wait.until(EC.element_to_be_clickable(sel.id_error_payment_paid))
        # msg_txt = self.driver.find_element(*sel.id_error_payment_paid).text
        # if msg_txt != '':
        #     # return get_message(msg_txt)
        #     pass
        #
        # else:
        #     self.waitErr.until(EC.text_to_be_present_in_element_value(sel.id_amount_field, ""))
        #     price = self.driver.find_element(*sel.id_amount_field).get_attribute('value')
        #     return price

        #     try:
        #         error_messgae = self.waitErr.until(
        #             EC.presence_of_element_located((By.XPATH, "//*[@id='dialogMSG']//div")))
        #
        #         print(error_messgae.text)
        #         self.driver.close()
        #
        #     except TimeoutException:
        #         return True
        #
        # #
        # except TimeoutException as e:
        #     return False

    def get_price(self):
        begin = self.first_steps()
        return_obj = {}
        if isinstance(begin, dict):
            return begin
        elif isinstance(begin, float):
            return_obj["success"] = 'error'
            return_obj["price"] = begin
        else:
            return_obj['message'] = begin
        return return_obj


    def pay(self):
        bigin = self.first_steps()
        # if not bigin.isnumeric():
        #     return bigin
        # else:
        # # if bigin:
        #     btn = self.wait.until(EC.element_to_be_clickable(id_confirm_button))
        #     btn.click()
        #
        #     first_name = self.wait.until(EC.presence_of_element_located(sel.id_first_name_field))
        #     first_name.send_keys(self.payment_info["full_name"])
        #
        #     last_name = self.wait.until(EC.presence_of_element_located(sel.id_last_name_field))
        #     last_name.send_keys(self.payment_info["full_name"])
        #
        #     phone = self.wait.until(EC.presence_of_element_located(sel.id_phone_field))
        #     phone.send_keys(self.payment_info["phone"])
        #
        #     email = self.wait.until(EC.presence_of_element_located(sel.id_email_field))
        #     email.send_keys(self.payment_info["email"])
        #
        #     id_number = self.wait.until(EC.presence_of_element_located(sel.id_id_number_field))
        #     id_number.send_keys(self.payment_info["personalId"])
        #
        #     card_number = self.wait.until(EC.presence_of_element_located(sel.id_card_number_field))
        #     card_number.send_keys(self.payment_info["card_num"])
        #
        #     card_valid_year = self.payment_info["card_valid_year"]
        #     card_valid_month = self.payment_info["card_valid_month"]
        #
        #     Select(self.driver.find_element(*sel.id_year_valid)).select_by_value(card_valid_year)
        #     Select(self.driver.find_element(*sel.id_month_valid)).select_by_value(card_valid_month)
        #
        #     btn_pay = self.wait.until(EC.element_to_be_clickable(sel.id_submit_button))
        #     btn_pay.click()
        #
        #     # check if the sum is the same.
        #     self.wait.until(EC.presence_of_element_located(sel.id_total_amount))
        #     amount = self.driver.find_element(*sel.id_total_amount).text
        #     print(amount)
        #     print(type(amount))
        #     print('****')
        #     print(bigin)
        #     print(type(bigin))
        #     if amount != bigin:
        #         print('Error 2')
        #         # return error
        #
        #     self.wait.until(EC.presence_of_element_located(sel.id_deal_type_selector))
        #     Select(self.driver.find_element(*sel.id_deal_type_selector)).select_by_index(1)
        #     sleep(0.5)
        #     Select(self.driver.find_element(*sel.id_num_of_pay_selector)).select_by_index(1)
        #
        #     pay_btn = self.wait.until(EC.element_to_be_clickable(sel.id_confirm_pay_button))
        #     pay_btn.click()
        #
        #     # self.wait.until(EC.presence_of_element_located(sel.id_payments_details))
        #     # payments_details = self.driver.find_element(*sel.id_payments_details).text
        #     # return payments_details
        #
        #     self.wait.until(EC.presence_of_element_located(sel.class_payments_details))
        #     payments_details = self.driver.find_elements(*sel.class_payments_details)[1].text
        #     return payments_details
        #
        #     # sleep(180)
        # # else:
        # #     print("error 3")
        #
        #     # try:
        #     #     error_payment = self.waitErr.until(
        #     #         EC.presence_of_element_located((By.XPATH, "//*[@id='dialogMSG']")))
        #     #     print(error_payment.text)
        #     #     self.driver.close()
        #     #     # return_obj["error"] = "????? ??? " + error_messgae.text
        #     #
        #     # except TimeoutException:
        #     #     sleep(1)
        #     #     confirmation = self.waitErr.until(
        #     #         EC.presence_of_element_located(
        #     #             (By.XPATH, "//span[@ng-bind='payBillResponse.IskaNumber']"))).get_attribute(
        #     #         'text')
        #     #     print("yes")
        #     #     print(confirmation)
        #     #     sleep(30)