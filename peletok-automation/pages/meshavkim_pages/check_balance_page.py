from selenium.webdriver.common.by import By


class CheckBalancePage:
    input_number_balance = (By.ID, 'InputTypeNumbertotalBalance')
    cheshbon_chashmal_icon = (By.ID, 'id185Link')
    input_contract_number = (By.ID, 'InputTypeNumbercontractNumberInput')
    electricity_charge_button = (By.ID, 'electricityChargeButton')
    alert_credit_limit = (By.CSS_SELECTOR, 'div.myAlert.alert.alert-danger.fade.show')
    input_phone_pre_number = (By.ID, 'InputTypeNumberInputTypePhonePrefixphoneNumberInput')
    input_phone_number = (By.ID, 'InputTypeNumberInputTypePhoneNumberphoneNumberInput')
    total_amount_payment = (By.ID, 'TotalamountPaymentCol')
    pay_button = (By.ID, 'payButton')
    message_green_color = (By.CLASS_NAME, 'fs-20 messageGreenColor')
    back_to_previous_screen_btn = (By.ID, 'backToPreviousScreenBtn')
    input_number_frame = (By.ID, 'InputTypeNumberFrame')
