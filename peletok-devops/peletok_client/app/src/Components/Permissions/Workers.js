import React, { Component } from 'react';
import { UsersScreen } from '../selectUser/selectUser';
import { Row, Col, Button } from 'reactstrap';
import { LanguageManager as LM } from '../LanguageManager/Language';
import {ourWorkers}from './WorkersFakeList';
import InputTypeText from  '../InputUtils/InputTypeText';
import InputTypeNumber from '../InputUtils/InputTypeNumber';

export class WorkerManagement extends Component {
  //**TODO--Search for an agent by name and terminal number and display it
  SearchAgent(e) {
  
  }
  render() {

    return (
      <div  style={{ textAlign: "start" }}>
           <h3 className="firstDivString">{LM.getString("workerManagement")}</h3>
        <div className="workers">

          <Row style={{ height: "4rem" }}>
            <Col sm="4">
            <InputTypeText id="searchAgentByName" title={LM.getString("searchAgentByName")}tooltip=''maxLength='30'/>
            </Col>
            <Col sm="4">
            <InputTypeNumber id="typeTerminalNumber" title={LM.getString("typeTerminalNumber")}tooltip=''maxLength='30'/>

            </Col>
            <Col sm="2">
              <Button style={{ margin: '25%',backgroundColor:'var(--regButton)'  }}  id ="search" onClick={this.SearchAgent.bind(this)}>{LM.getString("search")}</Button>
            </Col>
          </Row>
          <hr width="95%" />
          <UsersScreen Managment={false}  ourUsers = {ourWorkers()}/>
        </div>
      </div>
    );
  }
}
