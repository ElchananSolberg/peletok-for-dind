var BaseModel = require('../../BaseModel.js');

/**
 * UserBranch model
 */
class UserBranch extends BaseModel {
    /**
     * configs fields and options
    */
    config() {
        this.prefix = 'BSN';
    }
    /**
     * creates associate for userBranch model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.branch.model, { as: 'Branch' });
        this.model.belongsTo(models.user.model, { as: 'User' });


    }
}

module.exports = new UserBranch();