'use strict';
const {addColumnsToTable} = require('../dbUtils/addPermission')
module.exports = {
  async up(queryInterface, Sequelize) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    
    await  addColumnsToTable(queryInterface, 'BSN_Business', [{
        name: 'distributor_business_id', typeOptions: {
          type: Sequelize.INTEGER,
          allowNull: true
        }
      }])
    
  },
  async down(queryInterface, Sequelize) {
    
  }
};
