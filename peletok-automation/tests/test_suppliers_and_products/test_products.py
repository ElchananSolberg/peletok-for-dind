from tests.test_suppliers_and_products.base_suppliers_and_products_test_class import *


class TestProduct(BaseSuppliersAndProductsTestClass):

    def login_and_click_on_suppliers_profile_tab(self):
        self.login_and_get_in_suppliers_and_products()
        self.click_on_a_tab("products")

    def product_detail_part(self):
        self.driver.wait.wait_for_element_to_be_present(ProductsPage.conversation_minutes_input)
        assert self.driver.tools.wait_for_element_and_get_attribute(ProductsPage.conversation_minutes_input, 'value') \
               == '1', "recorded value error"
        assert self.driver.tools.wait_for_element_and_get_attribute(ProductsPage.sms_input, 'value') == '1', \
            "recorded value error "
        assert self.driver.tools.wait_for_element_and_get_attribute(ProductsPage.nefah_glicha_input, 'value') == '', \
            "recorded value error"
        # TODO : Need to verify field tokef_bayamim_input, not working for now
        assert self.driver.tools.wait_for_element_and_get_attribute(ProductsPage.conversations_conditions, 'value') \
               == '', "recorded value error"
        assert self.driver.tools.wait_for_element_and_get_attribute(ProductsPage.sihot_lirchout_input, 'checked') \
               == 'true', "recorded checkbox error"
        assert self.driver.tools.wait_for_element_and_get_attribute(ProductsPage.conversation_houl_price_input, 'value') \
               == '1', "recorded value error"
        assert self.driver.tools.wait_for_element_and_get_attribute(
            (ProductsPage.other_input[0], ProductsPage.other_input[1].format('1')), 'value') == '', \
            "recorded value error"
        assert self.driver.tools.wait_for_element_and_get_attribute(
            (ProductsPage.other_input[0], ProductsPage.other_input[1].format('2')), 'value') == '', \
            "recorded value error"

    def product_name_part(self):
        assert self.driver.tools.wait_for_element_and_get_attribute((
            ProductsPage.product_name[0], ProductsPage.product_name[1].format('Hebrew')), 'value') == "טוקמן 59 ש''ח", \
            "recorded name supplier error"
        assert self.driver.tools.wait_for_element_and_get_attribute((
            ProductsPage.product_name[0], ProductsPage.product_name[1].format('English')), 'value') == "Talkmen 59 nis", \
            "recorded name supplier error"
        assert self.driver.tools.wait_for_element_and_get_attribute((
            ProductsPage.product_name[0], ProductsPage.product_name[1].format('Arabic')), 'value') == "توكمان هو 59 " \
                                                                                                      "شيكل", \
            "recorded name supplier error"

    def recognize_to_supplier_part(self):
        assert self.driver.tools.wait_for_element_and_get_attribute(ProductsPage.supplier_recognize, 'value') == '1000', \
            "recorded value error"
        # TODO : all the tests on barcode; Yedidia said that we can't for now

    def tag_choice_part(self):
        assert self.driver.tools.get_text_from_element((ProductsPage.tag_choice[0],
                                                        ProductsPage.tag_choice[1].format('0'))) == 'שטחי הרשות',\
            "The tag is not שטחי הרשות"

    def product_cost_part(self):
        assert self.driver.tools.wait_for_element_and_get_attribute(ProductsPage.price_for_consumer, 'value') == '59',\
            "recorded final price error"
        assert self.driver.tools.wait_for_element_and_get_attribute(ProductsPage.product_price, 'value') == '',\
            "recorded price error"
        assert self.driver.tools.wait_for_element_and_get_attribute(ProductsPage.distribution_fee, 'value') == '0',\
            "recorded distribution fee error"

    def miscellaneous_part(self):
        assert self.driver.tools.wait_for_element_and_get_attribute(ProductsPage.active_button_checkBox, 'checked') \
               is None
        assert self.driver.tools.wait_for_element_and_get_attribute(ProductsPage.product_default_checkBox, 'checked') \
               == 'true', "recorded checkbox error"
        assert self.driver.tools.wait_for_element_and_get_attribute(ProductsPage.permitted_cancel_checkbox, 'checked') \
               == 'true', "recorded checkbox error"
        assert self.driver.tools.wait_for_element_and_get_attribute(ProductsPage.product_order, 'value') \
               == '', "recorded value error"
        assert self.driver.tools.get_text_from_element(ProductsPage.select_profit_model) == '% רווח',\
            "recorded profit percentage error"
        assert self.driver.tools.get_text_from_element(ProductsPage.select_alternative_product) == '',\
            "recorded error"

    def test_flow_product_page(self):
        self.login_and_click_on_suppliers_profile_tab()
        self.check_radio_buttons(ProductsPage.radio_button)
        self.click_on_input_and_choose_option(ProductsPage.supplier_choice_input, 'סלקום  QA')
        self.click_on_input_and_choose_option(ProductsPage.product_choice_input, "טוקמן 59 ש''ח")
        self.product_detail_part()
        self.product_name_part()
        self.recognize_to_supplier_part()
        self.tag_choice_part()
        self.product_cost_part()
        self.miscellaneous_part()
        self.remove_logs_dir = True
