const BaseModel = require('../../BaseModel');
const DataTypes = require("sequelize").DataTypes;
const moment = require('moment');
const Sequelize = require('sequelize');
const Op = Sequelize.Op
const Business = require('../../business/models/Business').model
const BusinessSupplier = require('../../supplier/models/BusinessSupplier').model
/**
 * Supplier model
 */
class Supplier extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: false,
                primaryKey: true
            },
            name: {
                type: DataTypes.STRING(128),
                allowNull: true
            },
            type: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            order: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            cancel_option: {
                type: DataTypes.BOOLEAN,
                allowNull: true
            },
            max_percentage_profit: {
                type: DataTypes.DOUBLE,
                allowNull: false,
                defaultValue:0
            },
            min_percentage_profit: {
                type: DataTypes.DOUBLE,
                allowNull: false,
                defaultValue:0
            },
            max_payment: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            default_creation: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false
            },
            is_abstract: { // supplier for profile
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false
            }
           
        };

        this.scopes = {
            BusinessSupplier: (businessId) => {
                 return {
                     where: {
                         id: businessId,
                         '$Business.id$': {[Op.ne]: null}
                     },
                     include: [
                         {
                            model: Business,
                            through: {                   
                                where: {is_authorized: true}
                            },
                            as: 'Business',
                            require: true
                        }
                     ]
                 }
            } 
        }

        this.prefix = "SUPL";
        this.author = true;
        this.statusAuthor = true;
        this.statusField = true;
        this.statusTimestampField = true;
    }

    /**
     * creates associate for supplier model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsToMany(models.document.model, { through: models.supplierDocument.model, as: "Document" });
        this.model.hasMany(models.supplierUser.model, {as: 'SupplierUser' });
        this.model.hasMany(models.supplierTag.model, {as: 'SupplierTag' });
        this.model.hasMany(models.product.model, {as: 'Product' });
        this.model.hasMany(models.supplierColor.model, {as: 'SupplierColor' });
        this.model.belongsTo(models.address.model, {as: 'Address' });
        this.model.hasMany(models.transaction.model, {as: 'Transaction' });
        this.model.hasOne(models.supplierContact.model, {as: 'SupplierContact' });
        this.model.belongsTo(models.creditCard.model, { as: 'CreditCard' });
        this.model.belongsToMany(models.business.model,{ through: models.businessSupplier.model,  as:'Business' })
    }
}

const supplier = new Supplier();

// Hook for generate id if no given
supplier.model.beforeCreate((sup, options)=>{
    if(sup.id==null){
        return supplier.model.max("id", {paranoid: false}).then(maxId=>{
        sup.id =maxId ? maxId+1 : 1;
    })}
});
// Hook for filter suppliers (only real suppliers)
// To get the suppliers that are past abstract in where object
//     ["is_abstract"] = true;
supplier.model.beforeFind((options) => {
    options.where = options.where || {};
    if(options.where["is_abstract"]===undefined)
        options.where["is_abstract"] = false;
});

// Hook for create businessSupplier after supplier is created
supplier.model.afterCreate( async (supplier, options)  => {
    let businessSuppliersToCrate = []
    let authorization;
    let defaultCreation
      let businesses = await Business.findAll()
      businesses.forEach((business)=>{
        defaultCreation = supplier.default_creation
        if (business.id == 1 || supplier.is_authorized) {
            defaultCreation = true
        }
let businessSupplier = {timestamp_start: moment().toDate(),business_id: business.id, supplier_id: supplier.id, is_authorized: defaultCreation}
            businessSuppliersToCrate.push(businessSupplier)
    })
await BusinessSupplier.bulkCreate(businessSuppliersToCrate)
});
module.exports = supplier;