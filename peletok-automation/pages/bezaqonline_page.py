from selenium.webdriver.common.by import By


class BezaqOnlinePage:
    pre_phone_number = (By.ID, 'InputTypeNumberInputTypePhonePrefixsupplierTelnumTitle')
    phone_number = (By.ID, 'InputTypeNumberInputTypePhoneNumbersupplierTelnumTitle')
    radio_btn_tz = (By.ID, "InputUtilsRadiosearchMethodRadioButton0")
    radio_btn_Invoice_amount = (By.ID, "InputUtilsRadiosearchMethodRadioButton1")
    ID_client = (By.ID, 'InputTypeNumberbezeqIdNumberInput')
    Invoice_amount = (By.ID, 'InputTypeNumberreceiptAmountInput')
