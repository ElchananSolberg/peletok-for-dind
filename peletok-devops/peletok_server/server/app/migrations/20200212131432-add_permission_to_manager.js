'use strict';
const moment = require('moment');
var Sequelize = require('sequelize');
var Permission = require('../permission/models/Permission').model;
var Op = Sequelize.Op;

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const permissions = await queryInterface.select(Permission, 'PRM_Permission', {
      attributes: ['id', 'path'],
      where: {
        path: {[Op.in]: ['GET /user/valid_fields']},
      },
    });
    return queryInterface.bulkInsert('PRM_PermissionRole',  permissions.map((p) => {
      return {
          action: 'edit',
          role_id: 1,
          permission_id: p.id,
          created_at: moment().toDate(),
          updated_at: moment().toDate(),
        }
      }), {});
    

  },

  down: (queryInterface, Sequelize) => {
    return Promise.resolve()
  }
};
