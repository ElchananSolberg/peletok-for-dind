const paymentBasic = require('../paymentBasic');
const logs = require('../../logger')
const ManualCardApi = require('../../manualCardApi');
const Info = require('../../resInfo')
const models = require('../../index');
const {MongoLogger} = require('../../mongoLogger')

/***
 *Class  to  pay manualCards
 */
class ManualCards extends paymentBasic {
    
    constructor(supplierId, productId,req) {
        super()
        this.req=req
        this.supplierId = supplierId
        this.productId = productId
        this.info = new Info()
    }

    /***
   * check   price  of  bil/product 
   * 
   */
    async getPrice() {
        const manualCardApi = new ManualCardApi(this.productId)
        const res = await manualCardApi.getPrice()
        if (res.error) {
            return res
        }
        this.info.price = res
        logs.info("price  of  card  is ", res)
        return this.info
    }

    /***
         * pay manualCards
         */
    async pay(transactionId) {
        if ((await this.isDemoTransactionUser())) {
            return this.demoRespons()
        }
        else {
        logs.info("supplierID= ",this.supplierId, "  productId= ",this.productId);
        const manualCardApi = new ManualCardApi(this.productId)
        const res = await manualCardApi.pay()
        if (res.error)  {
            return res
        }
        this.info.isPaid = true
        logs.info(res)
        this.info.send = { "success": res }
        MongoLogger.logTransaction({
            transactionId,
            supplierId: this.req.params.supplierID,
            requestParams: this.req.body,
            request: 'manual card',
            response: this.info ,
        })
        return this.info
    }
}
}
module.exports = ManualCards
