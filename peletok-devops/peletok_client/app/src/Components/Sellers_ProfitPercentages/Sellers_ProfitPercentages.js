import React, { Component } from 'react';
import './Sellers_ProfitPercentages.css';

import { Button, Container, Row, Col, Collapse } from 'reactstrap';

import { LanguageManager as LM } from "../LanguageManager/Language";
import { Notifications } from '../Notifications/Notifications';
import { Loader } from '../Loader/Loader';
import { Confirm } from '../Confirm/Confirm';
import { SelectedSeller } from '../SelectedSeller/SelectedSeller';

import CommissionProfileTable from '../CommissionProfile/CommissionProfileTable';

import {
    getBusinessProductDueToUser, getBusinessProductAuthorizationDueToBusiness,
    getBusinessSupplierAuthorizationDueToUser, getBusinessSupplierAuthorizationDueToBusiness,
    putBusinessProducts, putBusinessProductsProfitPercentage, putBusinessProductsEarningPoints, putBusinessUseDistributionFee
} from '../DataProvider/DataProvider'

import InputTypeSearchList from '../InputUtils/InputTypeSearchList';
import InputTypeRadio from '../InputUtils/InputTypeRadio';
import InputTypeCheckBox from '../InputUtils/InputTypeCheckBox';
import InputTypeNumber from '../InputUtils/InputTypeNumber';
import { TypesEnum } from '../../enums/TypesEnum';


/**
* Sellers_ProfitPercentages is a main component of Sellers_ProfitPercentages Page.  Sellers_ProfitPercentages and its children are working with separate .css file: Sellers_ProfitPercentages.css.
*/
export default class Sellers_ProfitPercentages extends Component {


    constructor(props) {
        super(props);

        this.state = {
            currentUserProducts: [],
            /** "Use Distribution Fee" checkbox field Value  */
            useDistributionFeeValue: false,
            /** working variable that will contain an array of suppliers from the server */
            suppliers: [],
            /** working variable that will contain an array of products from the server */
            products: [],
            /** working variable that will contain a selected seller object */
            selectedSeller: {},
            /** tableRows - an array of objects for <CommissionProfileTable/> component. The values of the objects are the data of the rows of a table.*/
            tableRows: [],
        }
        /** working variable that will contain an ALWAYS FULL array of products from the server */
        this.products = [];
        /** working variable that will contain an array of products from the server, FILTERED due to supplier */
        this.productsFilteredDueToSupplier = [];
    }

    /** TODO: CHECK functionality */
    /** "Use Distribution Fee" checkbox field onChange handler */
    useDistributionFeeChanged = (e) => {
        this.setState({ useDistributionFeeValue: this.useDistributionFee.getValue().value });
    }
    /** TODO: CHECK functionality */
    /** "Save"  (Use Distribution Fee) button click handler */
    saveUseDistributionFeeClicked = (e) => {
        if (SelectedSeller.currentSeller.getValue().id) {
            Loader.show();
            putBusinessUseDistributionFee({ "business_id": this.state.selectedSeller.id, 'use_distribution_fee': this.state.useDistributionFeeValue }).then(
                res => {
                    Loader.hide();
                    Notifications.show(LM.getString("successSellerUpdating") + this.state.selectedSeller.name, 'success');
                    SelectedSeller.createSellerClicked.next();
                    this.loadProductAuthorizationDataIntoTable();
                }
            ).catch(
                err => {
                    Loader.hide();
                    Notifications.show(err, 'danger');
                }
            );
        }
        else Notifications.show(LM.getString("sellerNotSelectedError"), 'warning');
    }
    /** "Service Type" radio buttons field onChange handler */
    serviceTypeChanged = (e) => {
        let selectedServiceTypeId = e.target.id.split("-")[1];
        let productsFiltered;
        if (selectedServiceTypeId != 0) {
            productsFiltered = this.productsFilteredDueToSupplier.filter(element => element.product_type == selectedServiceTypeId);
        }
        else productsFiltered = this.productsFilteredDueToSupplier;
        this.selectProduct.setValue("");
        this.setState({ products: productsFiltered }, () => {
            if (SelectedSeller.currentSeller.getValue().id) this.loadProductAuthorizationDataIntoTable();
            else this.loadProductDataIntoTable();
        });
    }
    /** "Select Supplier" field onChange handler */
    supplierChanged = (e) => {
        let value = this.selectSupplier.getValue().value;
        let selectedSupplier = this.state.suppliers.find(supplier => (supplier.supplier_name === value));
        let productsFiltered = this.products.filter(element => element.supplier_id === selectedSupplier.supplier_id);
        this.setState({ products: productsFiltered }, () => {
            this.selectProduct.setValue("");
            this.loadProductAuthorizationDataIntoTable();

        });
        this.productsFilteredDueToSupplier = productsFiltered;
    }
    /** "Select Product" field onChange handler */
    productChanged = (e) => {
        let value = this.selectProduct.getValue().value;
        let selectedProduct = this.productsFilteredDueToSupplier.find(product => (product.product_name === value));
        let productsFiltered = this.productsFilteredDueToSupplier.filter(element => element.product_id === selectedProduct.product_id);
        this.setState({ products: productsFiltered }, () => {
            this.loadProductAuthorizationDataIntoTable();
        });
    }

    /** Function that enters a data from a response on getBusinessProductDueToUser() request into the table. */
    loadProductDataIntoTable = () => {
        let tableRows = this.state.products.map(product => { return { 'product': product.product_name, 'cardType': product.product_type } });
        this.setState({ tableRows: tableRows })
    }
    /** Function that enters a data from a response on getBusinessProductAuthorizationDueToBusiness() request into the table. */
    loadProductAuthorizationDataIntoTable = () => {

        let tableRows = this.state.currentUserProducts.map(currentProduct => {

            let product = this.state.products.find(p => p.product_id == currentProduct.product_id)
            let data = product ? {
                'product': product.product_name, 'cardType': product.product_type, 'profitPercentage': product.percentage_profit, 'profitPerParagraph20': product.article_20_fee,
                'earningPoints': product.points, 'checkbox2': product.distribution_fee, checkbox: product.is_authorized, validationObjec: {
                    business_commission: currentProduct.business_commission,
                    max_percentage_profit: currentProduct.max_percentage_profit,
                    min_percentage_profit: currentProduct.min_percentage_profit,
                    distribution_fee: currentProduct.distribution_fee,
                }
            } : false;
            if (!this.state.useDistributionFeeValue) {
                delete data['checkbox2']
            }
            return product ? data : false;
        }).filter(p => p);
        this.setState({ tableRows: tableRows })
    }
    /** Function that loads from a server a new data for the table due to selected Seller. */
    fieldsLoad = () => {
        let useDistributionFee = this.state.selectedSeller.use_distribution_fee;
        this.useDistributionFee.setValue(useDistributionFee !== null ? useDistributionFee : false);
        this.setState({ useDistributionFeeValue: useDistributionFee });

        let selectedSellerId = this.state.selectedSeller.id;

        getBusinessSupplierAuthorizationDueToBusiness(selectedSellerId).then(
            (res) => {
                this.setState({ suppliers: res.filter(s => s.type != TypesEnum.SUPPLIER_BILL) });
            }
        ).catch(
            (err) => {
                Notifications.show(LM.getString('loadSuppliersListFailed') + ' ' + err, 'danger');
            }
        )

        getBusinessProductAuthorizationDueToBusiness(selectedSellerId).then(
            (res) => {
                this.products = res;
                this.setState({ products: res }, () => this.loadProductAuthorizationDataIntoTable());
            }
        ).catch(
            (err) => {
                Notifications.show(LM.getString('loadProductsListFailed') + ' ' + err, 'danger');
            }
        )
    }

    /** "Save" button click handler */
    saveButtonClicked = (e) => {
        if (SelectedSeller.currentSeller.getValue().id) {
            let productsList = this.state.currentUserProducts.map((product, index = 0) => {
                let tableRowCheckBoxChecked = this.commissionProfileTable.tableRows['tableRow' + index].tableRowCheckBox.props.checked;
                let tableRowProfitPercentage = this.commissionProfileTable.tableRows['tableRow' + index].profitPercentage.getValue().value;
                // let tableRowProfitPerParagraph20 = this.commissionProfileTable.tableRows['tableRow' + index].profitPerParagraph20.getValue().value;
                let tableRowEarningPoints = this.commissionProfileTable.tableRows['tableRow' + index].earningPoints.getValue().value;
                let tableRowDistributionFee;
                if(this.commissionProfileTable.tableRows['tableRow' + index].distributionFee){
                    tableRowDistributionFee = this.commissionProfileTable.tableRows['tableRow' + index].distributionFee.props.checked;
                }
                if (tableRowProfitPercentage === '' && product.percentage_profit === null) tableRowProfitPercentage = null;
                // if (tableRowProfitPerParagraph20 === '' && product.article_20_fee === null) tableRowProfitPerParagraph20 = null;
                // if (tableRowProfitPerParagraph20 === '' && product.article_20_fee === undefined) tableRowProfitPerParagraph20 = undefined;
                if (tableRowEarningPoints === '' && product.points === null) tableRowEarningPoints = null;
                if (tableRowDistributionFee === '' && product.distribution_fee === null) tableRowDistributionFee = null;

                if (tableRowCheckBoxChecked !== product.is_authorized
                    || tableRowProfitPercentage !== product.percentage_profit
                    // || tableRowProfitPerParagraph20 !== product.article_20_fee 
                    || tableRowEarningPoints !== product.points
                    || tableRowDistributionFee !== product.distribution_fee)
                    return {
                        "product_id": product.product_id,
                        "is_authorized": tableRowCheckBoxChecked,
                        "percentage_profit": (tableRowProfitPercentage === '') ? null : tableRowProfitPercentage,
                        // "article_20_fee": (tableRowProfitPerParagraph20 === '') ? null : tableRowProfitPerParagraph20,
                        "points": (tableRowEarningPoints === '') ? null : tableRowEarningPoints,
                        "distribution_fee": tableRowDistributionFee,
                    };
                else return null;
            })
            let cleanedFromNullProductsList = productsList.filter(product => product !== null);
            let businessProductData = {
                "business_id": this.state.selectedSeller.id,
                "products_list": cleanedFromNullProductsList,
            };
            if (cleanedFromNullProductsList[0]) {
                Loader.show();
                putBusinessProducts(businessProductData).then(
                    res => {
                        Loader.hide();
                        Notifications.show(LM.getString("successSellerUpdating") + this.state.selectedSeller.name, 'success');
                    }
                ).catch(
                    err => {
                        Loader.hide();
                        Notifications.show(err, 'danger');
                    }
                );
            }
            else Notifications.show(LM.getString("noChanges"), 'warning');
        }
        else Notifications.show(LM.getString("sellerNotSelectedError"), 'warning');
    }
    /** "Spread Profit Percentage" button click handler */
    spreadProfitPercentageButtonClicked = (e) => {
        if (SelectedSeller.currentSeller.getValue().id) {
            let value;
            let sellerId = this.state.selectedSeller.id;
            if (this.profitPercentage.getValue().valid) value = this.profitPercentage.getValue().value;
            if (value) {
                let productsIds = this.state.products.map((product) => { return product.product_id });
                let profitPercentageData = {
                    "business_id": sellerId,
                    "percentage_profit": value,
                    "product_id": productsIds,
                };
                Confirm.confirm(LM.getString('thisActionwillLeadToChangesInAllProducts')).then(() => {
                    Loader.show();
                    putBusinessProductsProfitPercentage(profitPercentageData).then(
                        res => {
                            Loader.hide();
                            Notifications.show(LM.getString("profitPercentage") + ' ' + LM.getString('updatedSuccessfully'), 'success');
                            this.fieldsLoad();
                        }
                    ).catch(
                        err => {
                            Loader.hide();
                            Notifications.show(err, 'danger');
                        }
                    )
                });
            }
            else Notifications.show(LM.getString("profitPercentage") + ' ' + LM.getString('notDefined'), 'warning');
        }
        else Notifications.show(LM.getString("sellerNotSelectedError"), 'warning');
    }
    /** "Spread Earning Points" button click handler */
    spreadEarningPointsButtonClicked = (e) => {
        if (SelectedSeller.currentSeller.getValue().id) {
            let value;
            let sellerId = this.state.selectedSeller.id;
            if (this.earningPoints.getValue().valid) value = this.earningPoints.getValue().value;
            if (value) {
                let productsIds = this.state.products.map((product) => { return product.product_id });
                let earningPointsData = {
                    "business_id": sellerId,
                    "points": value,
                    "product_id": productsIds,
                };
                Confirm.confirm(LM.getString('thisActionwillLeadToChangesInAllProducts')).then(() => {
                    Loader.show();
                    putBusinessProductsEarningPoints(earningPointsData).then(
                        res => {
                            Loader.hide();
                            Notifications.show(LM.getString("earningPoints") + ' ' + LM.getString('updatedSuccessfully'), 'success');
                            this.fieldsLoad();
                        }
                    ).catch(
                        err => {
                            Loader.hide();
                            Notifications.show(err, 'danger');
                        }
                    )});
            }
            else Notifications.show(LM.getString("earningPoints") + ' ' + LM.getString('notDefined'), 'warning');
        }
        else Notifications.show(LM.getString("sellerNotSelectedError"), 'warning');
    }

    launchGetBusinessProductDueToUser = () => {
        getBusinessSupplierAuthorizationDueToUser().then(
            (res) => {
                this.setState({ suppliers: res });
            }
        ).catch(
            (err) => {
                Notifications.show(LM.getString('loadSuppliersListFailed') + ' ' + err, 'danger');
            }
        )

        getBusinessProductDueToUser().then(
            (res) => {
                this.setState({ currentUserProducts: res }, () => this.loadProductDataIntoTable());
            }
        ).catch(
            (err) => {
                Notifications.show(LM.getString('loadProductsListFailed') + ' ' + err, 'danger');
            }
        )
    }

    componentDidMount() {
        SelectedSeller.setHeader.next(LM.getString('profitPercentages'));
        if (SelectedSeller.newSellerState) SelectedSeller.createSellerClicked.next();
        if (SelectedSeller.newDistributorState) SelectedSeller.createDistributorClicked.next();
        if (SelectedSeller.currentSeller.getValue().id) this.setState({ selectedSeller: SelectedSeller.currentSeller.getValue() }, () => this.fieldsLoad());
        this.launchGetBusinessProductDueToUser();
        SelectedSeller.currentSeller.subscribe((x) => {
            if(x.id){
                this.setState({ selectedSeller: x },() => this.fieldsLoad())
            }
        });
        
        SelectedSeller.currentDistributor.subscribe((x) => {
            if(x.id){
                this.setState({ selectedDistributor: x }, () =>{
                    this.selectSupplier && this.selectSupplier.setValue("");
                    this.selectProduct && this.selectProduct.setValue("");
                })
            }
        })
    }

    render() {
        /** data for "Use Distribution Fee" field (InputTypeCheckBox InputUtils component) */
        const useDistributionFeeProps = {
            lableText: LM.getString("useDistributionFee"),
            id: "Sellers_ProfitPercentages__UseDistributionFee",
        };

        /** data for service type (InputTypeRadio  InputUtils component) */
        const serviceTypeProps = {
            options: LM.getString('serviceTypeOptions'),
            default: LM.getString('serviceTypeOptions')[0],
            id: "Sellers_ProfitPercentages_ServiceType-",
        };

        /** props for "Select Supplier" field (<InputTypeSearchList/> component) */
        const selectSupplierProps = {
            options: LM.getString('suppliersOptions'),
            title: LM.getString('selectSupplier'),
            id: "Sellers_ProfitPercentages_SelectSupplier",
        };
        /** props for "Select Product From List" field (<InputTypeSearchList/> component) */
        const selectProductProps = {
            options: LM.getString('productsOptions'),
            title: LM.getString('selectProductFromList'),
            id: "Sellers_ProfitPercentages_SelectProduct",
        };

        /** data for "Profit Percentage" field (InputTypeNumber InputUtils component) */
        const profitPercentageProps = {
            title: LM.getString("profitPercentage") + ':',
            id: "Sellers_ProfitPercentages_ProfitPercentage",
        };
        /** data for "Earning Points" field (InputTypeNumber InputUtils component) */
        const earningPointsProps = {
            title: LM.getString("earningPoints") + ':',
            id: "Sellers_ProfitPercentages_EarningPoints",
        };

        /** data for "Spread Profit Percentage" button */
        const spreadProfitPercentageButtonText = LM.getString("spreadProfitPercentage");
        /** data for "Spread EarningPoints" button */
        const spreadEarningPointsButtonText = LM.getString("spreadEarningPoints");
        /** tableHeadersObj - an object for <CommissionProfileTable/> component. The values of the object are the HEADERS of a table */
        let use_distribution_fee = this.state.useDistributionFeeValue;

        /** data for "Save" button */
        const saveButtonText = LM.getString("save");
        const tableHeadersObj =
            use_distribution_fee ? {
                product: LM.getString('product'),
                cardType: LM.getString('cardType'),
                profitPercentage: LM.getString("profitPer"),
                profitPerParagraph20: LM.getString("profitPer") + ' ' + LM.getString("paragraph20"),
                earningPoints: LM.getString("earningPoints"),
                distributionFee: LM.getString("distributionFee").slice(0, -1),
                authorizedUser: LM.getString("authorizedUser")
            } : {
                    product: LM.getString('product'),
                    cardType: LM.getString('cardType'),
                    profitPercentage: LM.getString("profitPer"),
                    profitPerParagraph20: LM.getString("profitPer") + ' ' + LM.getString("paragraph20"),
                    earningPoints: LM.getString("earningPoints"),
                    authorizedUser: LM.getString("authorizedUser")
                };

        return (
            <Container className="sellersProfitPercentagesContainer">
                {SelectedSeller.currentSeller.getValue().id ?
                    <Container className="sellersProfitPercentagesSettings">
                        <Col >
                            <Row className="businessBackground align-items-center justify-content-start">
                                <Col sm='auto'>
                                    <InputTypeCheckBox
                                        {...useDistributionFeeProps}
                                        onChange={this.useDistributionFeeChanged}
                                        ref={(componentObj) => { this.useDistributionFee = componentObj }}
                                    />
                                </Col>
                                <Col sm='auto'>
                                    <Button
                                        className="businessButton businessButtonSmall businessButtonSave"
                                        id="sellers_ProfitPercentages_ButtonSaveUseDistributionFee"
                                        onClick={this.saveUseDistributionFeeClicked}
                                    >
                                        {saveButtonText}
                                    </Button>
                                </Col>
                            </Row>
                        </Col>
                        <Row >
                            <Col lg="8">
                                <InputTypeRadio
                                    {...serviceTypeProps}
                                    onChange={this.serviceTypeChanged}
                                    ref={(componentObj) => { this.serviceType = componentObj }}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col sm='4'>
                                <InputTypeSearchList
                                    {...selectSupplierProps}
                                    onChange={this.supplierChanged}
                                    options={this.state.suppliers.map(supplier => supplier.supplier_name) || ['']}
                                    ref={(componentObj) => { this.selectSupplier = componentObj }}
                                />
                            </Col>
                            <Col sm='4'>
                                <InputTypeSearchList
                                    {...selectProductProps}
                                    onChange={this.productChanged}
                                    options={this.productsFilteredDueToSupplier.map(product => product.product_name) || ['']}
                                    ref={(componentObj) => { this.selectProduct = componentObj }}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col sm='4'>
                                <InputTypeNumber
                                    {...profitPercentageProps}
                                    ref={(componentObj) => { this.profitPercentage = componentObj }}
                                />
                            </Col>
                            <Col sm='4'>
                                <Button
                                    className="sellersProfitPercentagesButton sellersProfitPercentagesSpreadButton"
                                    id="sellers_ProfitPercentages_SpreadProfitPercentageButton"
                                    onClick={this.spreadProfitPercentageButtonClicked}
                                >
                                    {spreadProfitPercentageButtonText}
                                </Button>
                            </Col>
                        </Row>
                        <Row >
                            <Col sm='4'>
                                <InputTypeNumber
                                    {...earningPointsProps}
                                    ref={(componentObj) => { this.earningPoints = componentObj }}
                                />
                            </Col>
                            <Col sm='4'>
                                <Button
                                    className="sellersProfitPercentagesButton sellersProfitPercentagesSpreadButton"
                                    id="sellers_ProfitPercentages_SpreadEarningPointsButton"
                                    onClick={this.spreadEarningPointsButtonClicked}
                                >
                                    {spreadEarningPointsButtonText}
                                </Button>
                            </Col>
                        </Row>
                        <Row style={{ marginTop: '30px' }}>
                            <Col >
                                <CommissionProfileTable
                                    tableRows={this.state.tableRows}
                                    tableHeadersObj={tableHeadersObj}
                                    tableFootersObj={{}}
                                    tableRowsEditable={[false, false, true, false, true, false, false]}
                                    ref={(componentObj) => { this.commissionProfileTable = componentObj }}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col sm="auto">
                                <Button
                                    className="sellersProfitPercentagesButton"
                                    id="sellers_ProfitPercentages_SaveButton"
                                    onClick={this.saveButtonClicked}
                                >
                                    {saveButtonText}
                                </Button>
                            </Col>
                        </Row>
                    </Container> : <></>}

            </Container>
        )
    }
}
