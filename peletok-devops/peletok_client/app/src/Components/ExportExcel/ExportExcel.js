import React, {Component} from 'react';
import ReactExport from "react-export-excel";
import {LanguageManager as LM} from '../LanguageManager/Language';
import './ExportExcel.css'

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;


/**
 * The component accepts an array of objects and downloads an excel file
 *  You can also pass a name in Props = fileName  to set a name for the file
 *
 */

export class ExportExcel extends Component {

    constructor(props) {
        super(props);
        this.state = {multiDataSet: []};
    }

    loadData = () => {
        let tableHeadersObj;
        let tableKeys = null;
        if (Array.isArray(this.props.tableHeadersObj)) {
            tableHeadersObj = this.props.tableHeadersObj
        } else if (!Array.isArray(this.props.tableHeadersObj)) {
            tableHeadersObj = Object.values(this.props.tableHeadersObj);
            tableKeys = Object.keys(this.props.tableHeadersObj)
        }
        if (!tableHeadersObj) {
            tableHeadersObj = this.props.data && this.props.data[0] ? Object.keys(this.props.data[0]) : [];
        }

        let data = [];
        let subArray = [];
        for (let indexRow = 0; indexRow < this.props.data.length; indexRow++) {
            let row = this.props.data[indexRow];
            subArray = [];
            let keys = tableKeys || Object.keys(row);
            keys.forEach((key) => {
                subArray.push(row[key]);
            });
            data.push(subArray)
        }
        this.setState({multiDataSet: [{columns: tableHeadersObj, data: data}]}, ()=>{
            this.hiddenButton.click();
        });
    };

    render() {
        return (
            <div>
                <button onClick={this.loadData}
                        className='exportExcelButton'>{LM.getString("ExportToExcel")}>
                </button>
                <ExcelFile filename={this.props.fileName}
                           element={<button hidden={true} ref={(r) => this.hiddenButton = r}/>}>
                    <ExcelSheet dataSet={this.state.multiDataSet} name={this.props.fileName}/>
                </ExcelFile>
            </div>)
    }
}