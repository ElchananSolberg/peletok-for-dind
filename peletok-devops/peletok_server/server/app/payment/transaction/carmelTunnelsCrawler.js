peletalkCrawler = require('./peletalkCrawler') ;

/***
 * Payment manager CarmelTunnel
 * @returns link   for  payment
 */

class CarmelTunnelsCrawler extends peletalkCrawler {

    getHostname() { return "mybill.carmeltunnels.co.il"; }
    
}

module.exports =  CarmelTunnelsCrawler;

