import React, { Component } from 'react';
import { Button, Row, Col, Collapse } from 'reactstrap';

import { LanguageManager as LM } from "../LanguageManager/Language"
import {
    getDistributors,
    getSellers,
    getManagerReportLength, getReportRes
} from '../DataProvider/DataProvider'
import InputTypeNumber from '../InputUtils/InputTypeNumber';
import InputTypeSearchList from '../InputUtils/InputTypeSearchList'
import InputTypeDate from '../InputUtils/InputTypeDate';
import InputTypeTime from '../InputUtils/InputTypeTime';
import {Loader} from "../Loader/Loader";
import {Notifications} from "../Notifications/Notifications";
import ReportPagination from "../ReportComponent/ReportPagination";
import {ExportSellerReportToExcel} from "../ExportExcel/ExportSellerReportToExcel";
import {getManagerReportRes} from "../DataProvider/Functions/DataGeter";
import {ReportTableContent} from "../ReportComponent/ReportTableContent";

const PAYMENT_METHOD_OPTIONS = ["הוראת קבע", "תשלום לסוכן", "תשלום מראש"];
let NAN_SELECT = LM.getString("any");


export class RemainderByDateReport extends Component {
    constructor(props) {
        super(props);
        this.state = {
            distributorsList: [],
            businessList: [],
            openReport: false,
            key: 0
        };
        this.selectDistributor = null;
        this.selectDistributorId = null;
        this.selectBusiness = null;
        this.selectBusinessId = null;
        this.paymentMethod = null;
        this.date = '';
        this.time = '';
        this.reportID = "remainderByDate";
        this.reportName = "remainderByDate";

        this.allBusiness = [];
    }

    getDistributorsNames() {
        getDistributors().then(
            (res) => {
                this.setState({ distributorsList: res });
            }
        ).catch(
            (err) => {
                alert("Failed loading Distributors list: " + err)

            }
        )
    }

    getBusinessNames() {
        getSellers().then(
            (res) => {
                this.allBusiness = res;
                this.setState({ businessList: res });
            }
        ).catch(
            (err) => {
                alert("Failed loading business list: " + err)

            }
        )
    }


    componentDidMount() {
        this.getDistributorsNames();
        this.getBusinessNames();
    }


    onSelectBusinessName = ()=>{
        let selectedName = this.selectBusiness.getValue().value;
        if(selectedName===NAN_SELECT){
            this.selectBusinessId.setValue('') // not number of any business
        }
        let businessLength = this.state.businessList.length;
        for (let i = 0; i < businessLength; i++) {
            let b = this.state.businessList[i];
            if (b.name === selectedName) {
                this.selectBusinessId.setValue(b.business_identifier);
                this.paymentMethod.setValue(b.payment_method);
                return;
            }
        }
    };

    onSelectBusinessId = ()=>{
        let businessLength = this.state.businessList.length;
        for (let i = 0; i < businessLength; i++) {
            let b = this.state.businessList[i];
            if (b.business_identifier === this.selectBusinessId.getValue().value) {
                this.selectBusiness.setValue(b.name);
                return;
            }
        }
        this.selectBusiness.setValue('') // not number of any business
    };

    onSelectDistributorName = ()=>{
        let selectedName = this.selectDistributor.getValue().value;
        if(selectedName===NAN_SELECT){
            this.selectDistributorId.setValue('');
            this.setState({businessList: [...this.allBusiness]});
            return;
        }
        let distributorsLength = this.state.distributorsList.length;
        for (let i = 0; i < distributorsLength; i++) {
            let d = this.state.distributorsList[i];
            if (d.name === selectedName) {
                this.selectDistributorId.setValue(d.business_identifier);
                this.selectBusiness.setValue(NAN_SELECT);
                this.selectBusinessId.setValue('');
                this.setState({businessList: this.allBusiness.filter(b=>b.distributor_id===d.distributor_id)});
                return;
            }
        }

    };
    onSelectDistributorId = ()=>{
        let selectedId = this.selectDistributorId.getValue().value;
        let distributorsLength = this.state.distributorsList.length;
        for (let i = 0; i < distributorsLength; i++) {
            let d = this.state.distributorsList[i];
            if (d.business_identifier === selectedId) {
                this.selectDistributor.setValue(d.name);
                this.selectBusiness.setValue(NAN_SELECT);
                this.selectBusinessId.setValue('');
                this.setState({businessList: this.allBusiness.filter(b=>b.distributor_id===d.distributor_id)});
                return;
            }
        }
        this.selectDistributor.setValue('');  // not number of any distributor
        if(!selectedId)
            this.setState({businessList: [...this.allBusiness]});

    };

    onSelectPaymentMethod = () =>{
        this.selectBusiness.setValue(NAN_SELECT);
        this.selectBusinessId.setValue('');
    };

    noData() {
        Notifications.show(LM.getString("noReportErrMsg"), 'danger');
        this.setState({
            openReport: false,
            key: this.state.key+1
        });
    }

    showReportClick = () => {
        let {params, validParams} = this.buildRequestParams();
        if (validParams) {
            Loader.show();
            getManagerReportLength(this.reportID, {params: {...params}}).then(
                (res) => {
                    Loader.hide();
                    // noinspection EqualityComparisonWithCoercionJS
                    if (res.length == '0') {
                        this.noData();
                    }
                    else {
                        this.setState({
                            reportLength: res.length,
                            openReport: true,
                            params: params,
                            key: this.state.key+1
                        });
                    }
                }
            ).catch(
                (err) => {
                    Loader.hide();
                    Notifications.show(err, 'danger')
                })
        }
        else {
            alert(LM.getString("wrongInputErrMsg"))
        }
    };

    buildRequestParams = () => {
        let params = {};
        let validParams = true;

        let fieldsData = this.getRelevantFieldsDataAsObject();
        let fieldsDataKeys = Object.keys(fieldsData);
        let fieldsDataKeysLength = fieldsDataKeys.length;
        for(let i=0;i<fieldsDataKeysLength;i++){
            let k = fieldsDataKeys[i];
            let val = fieldsData[k];
            if(val.valid){
                params[k] = val.value;
            }
            else {
                validParams = false;
                break;
            }
        }
        return {params, validParams};
    };

    getRelevantFieldsDataAsObject = () => {
        // valid false backus its required field
        let selectDate = this.date.getValue() || {valid: false, value: null};
        // valid false backus its required field
        let selectTime = this.time.getValue() || {valid: false, value: null};

        let fieldsData = {date: selectDate, time: selectTime};

        let selectBusinessId = this.selectBusinessId.getValue() || {valid: false, value: null};
        if (selectBusinessId.value&&selectBusinessId.value!==NAN_SELECT) {
            fieldsData.business_id = selectBusinessId;
        } else {

            let selectDistributorId = this.selectDistributorId.getValue() || {
                valid: false,
                value: null
            };
            if (selectDistributorId.value&&selectDistributorId.value!==NAN_SELECT) {
                const distributorsList = this.state.distributorsList;
                let foundId = false;
                for (let i = 0; i < distributorsList.length; i++) {
                    if(distributorsList[i].business_identifier === selectDistributorId.value) {
                        selectDistributorId.value = distributorsList[i].id;
                        foundId = true;
                        break;
                    }
                }
                if(!foundId){
                    selectDistributorId.valid = false;
                }
                fieldsData.distributor_id = selectDistributorId
            }

            let selectPaymentMethod = this.paymentMethod.getValue() || {valid: false, value: null};
            if (selectPaymentMethod.value&&selectPaymentMethod.value!==NAN_SELECT) {
                fieldsData.payment_method = selectPaymentMethod;
            }
        }
        return fieldsData;
    };


    /***
     * Loads all rows from server (for excel)
     * @returns {Promise<{data: *, columns: {}}>}
     */
    loadData = async () => {
        let res = await getManagerReportRes(this.reportID, {params: this.state.params});
        let columnName = {};
        Object.values(res['headers']).map((value) => {
            return columnName[value] = LM.getStringWithoutColon(value)
        });
        return {columns: columnName, data: res["data"]}
    };


    render() {
        return (
            <div>
                <h3 className={'color-page-header fs-26 font-weight-light'}>{LM.getString(this.reportName)}</h3>
                <div className="main_border_padding" >
                    <Row>
                        <Col sm="4">
                            <InputTypeSearchList
                                addDefaultToOptions={true}
                                default={NAN_SELECT}
                                title={LM.getString("business_name")}
                                id='selectBusiness'
                                options={this.state.businessList.map(busines => busines.name)}
                                onChange={this.onSelectBusinessName}
                                ref={(componentObj) => { this.selectBusiness = componentObj }}
                            />
                        </Col>
                        <Col sm="4">
                            <InputTypeNumber
                                title={LM.getString("customNum")}
                                id="costumerNumber"
                                onChange={this.onSelectBusinessId}
                                ref={(componentObj) => { this.selectBusinessId = componentObj }} />
                        </Col>
                        <Col sm="4">
                            <InputTypeSearchList
                                addDefaultToOptions={true}
                                default={NAN_SELECT}
                                title={LM.getString("paymentMethod")}
                                id="paymentMethod"
                                options={PAYMENT_METHOD_OPTIONS}
                                onChange={this.onSelectPaymentMethod}
                                ref={(componentObj) => { this.paymentMethod = componentObj }} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="4">
                            <InputTypeSearchList
                                title={LM.getString("selectDistributor")}
                                addDefaultToOptions={true}
                                default={NAN_SELECT}
                                id='selectDistributors'
                                options={this.state.distributorsList.map(distributor => distributor.name)}
                                onChange={this.onSelectDistributorName}
                                ref={(componentObj) => { this.selectDistributor = componentObj }}
                            />
                        </Col>
                        <Col sm="4">
                            <InputTypeNumber
                                title={LM.getString("distributorNum")}
                                id="distributorNumber"
                                onChange={this.onSelectDistributorId}
                                ref={(componentObj) => { this.selectDistributorId = componentObj }} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="4">
                            <InputTypeDate
                                title={LM.getString("date") + ":"}
                                id="date"
                                ref={(componentObj) => { this.date = componentObj }} />
                        </Col>
                        <Col sm="4">
                            <InputTypeTime
                                title={LM.getString("time") + ":"}
                                id="time"
                                ref={(componentObj) => { this.time = componentObj }} />
                        </Col>
                    </Row>
                    <Button id='showReport' className={'border-radius-3 btn btn-lg btn-md-block btn-secondary font-weight-bolder' +
                    ' fs-17 px-4 regularButton'} onClick={this.showReportClick}>
                        {LM.getString("showReport")}
                    </Button>
                </div>
                <Collapse isOpen={this.state.openReport}>
                    <React.Fragment>
                        <ReportPagination reportLength={this.state.reportLength}
                                          id={this.reportName}
                                          params={this.state.params}
                                          reportID={this.reportID}
                                          key={this.state.key}
                                          diaplayDatesHeader={false}
                                          defaultRowsPerPage={15}
                                          getReportMethd={getManagerReportRes}/>

                        <ExportSellerReportToExcel
                            loadData={this.loadData} fileName={this.reportName}
                        />
                    </React.Fragment>
                </Collapse>
            </div>
        )
    }

}
