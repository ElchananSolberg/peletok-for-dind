const request = require('supertest');
const app = require('../../routing')
const models = require('../../index')

describe('cancelation_request', () => {
     beforeEach(async () => {

         Transaction = models.transaction.model
         await Transaction.update({transaction_type: 3, parent_id: 1, supplier_id: 1}, {where: {id: 2}}) 
         await Transaction.update({transaction_type: 3, parent_id: 3, supplier_id: 94}, {where: {id: 4}}) 
     } )

     describe('/transaction/cancelation_request/1', () => {
         
        it('should return status 200',  () => {
            return request(app).get('/transaction/cancelation_request/1').then((response) => {
                expect(response.statusCode).toBe(200);
            });
        })
        it('should return an array',  () => {
            return request(app).get('/transaction/cancelation_request/1').then((response) => {
                expect(Array.isArray(response.body)).toBe(true);
            });
        })

        it('should return only one transaction',  () => {
            return request(app).get('/transaction/cancelation_request/1').then((response) => {
                expect(response.body.length).toBe(1);
            });
        })

        it('should have this fields',  () => {
            return request(app).get('/transaction/cancelation_request/1').then((response) => {
                expect(Object.keys(response.body[0]).sort()).toEqual(
                           [
                                 'transactionDate',
                                 'transactionTime',
                                 'businessName',
                                 'customerNumber',
                                 // 'agentName',
                                 // 'agentNumber',
                                 'customerPrice',
                                 'businessPrice',
                                 'supplierName',
                                 'productName',
                                 'action',
                                 'phoneNumber',
                                 'requestDate',
                                 'requestTime',
                                 'requesterName',
                                 'parentTransactionId'
                           ].sort())
            });
        })

        test('transaction supplier id should be 1', () => {
             return request(app).get('/transaction/cancelation_request/1').then(async (response) => {
               let supplier = await models.supplier.model.findByPk(1)
               expect(response.body[0].supplierName).toBe(supplier.name);
                

            });
         })

     } )

    describe('/transaction/cancelation_request/allSuppliers', () => {
        it('should return all transctions', () => {
             return request(app).get('/transaction/cancelation_request/allSuppliers').then(async (response) => {
               expect(response.body.length).toBe(2);
                

            });
         })
    } )
})
