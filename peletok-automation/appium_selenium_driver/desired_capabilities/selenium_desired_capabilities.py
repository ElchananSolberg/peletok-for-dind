import os

selenium_mobile_view = {'browserName': 'chrome',
                        'version': '',
                        'platform': 'ANY',
                        'chromeOptions': {
                            'args': ['--headless', "--no-sandbox", "--disable-gpu"],
                            'debuggerAddress': 'localhost: 5555',
                            'mobileEmulation': {
                                'deviceName': 'iPhone 6'
                            },
                            'extensions': []}
                        }

selenium_mobile_view_for_lighthouse = {'browserName': 'chrome', 'version': '', 'platform': 'ANY',
                                       'goog:chromeOptions': {'mobileEmulation': {'deviceName': 'iPhone 6'},
                                                              'extensions': [],
                                                              'args': ['user-agent=I LIKE CHOCOLATE',
                                                                       f"--remote-debugging-port="
                                                                       f"{os.environ.get('PORT', '5555')}",
                                                                       '--headless',
                                                                       '--disable-infobars', '--no-sandbox',
                                                                       '--disable-gpu']}}

selenium_remote_docker = {'browserName': 'chrome',
                          'version': '',
                          'platform': 'ANY',
                          'acceptSslCerts': True,
                          'acceptInsecureCerts': True}

selenium_headless = {'browserName': 'chrome',
                     'version': '', 'platform': 'ANY',
                     'goog:chromeOptions': {'prefs': {'download.default_directory': 'downloads/',
                                                      'download.prompt_for_download': False,
                                                      'download.directory_upgrade': True,
                                                      'safebrowsing.enabled': False,
                                                      'safebrowsing.disable_download_protection': True},
                                            'extensions': [],
                                            'args': ['--headless',
                                                     '--disable-gpu',
                                                     '--window-size=1280,1696',
                                                     '--no-sandbox',
                                                     ]
                                            }}
