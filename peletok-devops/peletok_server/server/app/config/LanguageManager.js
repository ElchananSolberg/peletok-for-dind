const allStrings = require("./strings.json");
const supportedLanguages = ['He', 'En', 'Ar'];
const models = require('../index')
const logger = require('../logger')


class LanguageManager {
    constructor() {
        this.LangSelected = 'He';
    };
    /**
    * changes language to a given language
    * only works if lang is supported
    * @param lang the language to switch to
    * TODO when setLang is called it should change the value of the user language in the DB
    */
   async setLang(lang, userId) {
        if (supportedLanguages.includes(lang)) {
            this.LangSelected = lang;
        }
    }

    /**
     * returns the language defined by the object
     * @returns {string}
     */
    getLang() {
        return this.LangSelected;
    }

    /**
     * returns the text value corresponding to the language selected and the text required
     * @param name the name of the string to get
     * @returns {string} The text value of the requested name as defined in Strings.JSON -
     *                   if 'name' does not appear in String.JSON - an error value is returned
     *                   if name appears but selected language does not exist - hebrew value is returned
     */
    getString(name) {
        if (allStrings && Object.keys(allStrings).includes(name)) {
            switch (this.LangSelected) {
                case 'En':
                    if (Object.keys(allStrings[name]).includes('en')){
                        return allStrings[name].en;
                    }
                    break;
                case 'Ar':
                    if (Object.keys(allStrings[name]).includes('ar'))
                        return allStrings[name].ar;
                    break;
                case 'He':
                    if (Object.keys(allStrings[name]).includes('he')){
                        return allStrings[name].he;
                    }
                    break
                default: // default is hebrew
                    return allStrings[name].he;
            }
            return allStrings[name].he;
        }
        return "STRING NOT DEFINED IN STRING.JSON";
    }
getErrorBasic(){
    return this.getString("ErrorBasic")
}

}
module.exports.LM = new LanguageManager()