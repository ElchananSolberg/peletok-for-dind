import React, { Component } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import './CityPayments.css';
import { LanguageManager as LM } from '../../LanguageManager/Language';
import InputTypeNumber from '../../InputUtils/InputTypeNumber'
import { cityPaymentShowInvoicePost, cityConfirmPaymentPost } from '../../DataProvider/DataProvider';
import cityImg from '../../../Assets/images/aviv.bmp';
import InputTypePhone from '../../InputUtils/InputTypePhone';
import { Loader } from '../../Loader/Loader'
import { Notifications } from '../../Notifications/Notifications';
import { SuccessModalWindow } from './SuccessModalWindow';
import { UserDetailsService } from '../../../services/UserDetailsService';
export class CityPayProceed extends Component {
    constructor(props) {
        super(props)
        this.state = {
            modal: false,
            modalMessage: LM.getString("successModalMessage"),
            modalWindowImage: undefined,
            modalMessageClassName: false,
            amount: '',
            transactionId: 0
        }
    }
    toggle = () => {
        this.setState({
            modal: !this.state.modal
        })
    }
    showInvoice = () => {
        let data = {
            itemId: this.props.productId,
            idItem: this.props.match.params.id,
            billNum: this.terminalInput.getValue().value,
            idClient: this.clientNumberInput.getValue().value,
            telephoneNum: this.supplierTelnumTitle.getValue().value
        }
        if (this.terminalInput.getValue().valid && this.clientNumberInput.getValue().valid) {
            Loader.show()
            cityPaymentShowInvoicePost(data).then(
                (res) => {
                    Loader.hide()
                    this.setState({ amount: res.success })
                    this.cityAmount.setValue(this.state.amount)
                    this.totalAmountPayment.setValue(Number(this.state.amount + this.props.finalCommission))

                }).catch((error) => {
                    Loader.hide()
                    if (error && error.response && error.response.data && error.response.data.error) {
                        Notifications.show(error.response.data.error, 'danger')
                    } else {
                        Notifications.show(LM.getString("error_while_saving"), 'danger')
                    }
                })
        }
    }

    confirmPayment = () => {
        let sum = this.props.maxPayment + " " + LM.getString("NIS")
        if (this.totalAmountPayment.getValue().value > Number(this.props.maxPayment)) {
            Notifications.show(LM.getString('thisDealIsLimitedTo') + sum, 'danger')
        }
        else if (this.cityAmount.getValue().valid && this.totalAmountPayment.getValue().valid) {
            Loader.show()
            let data = {
                itemId: this.props.productId,
                idItem: this.props.match.params.id,
                billNum: this.terminalInput.getValue().value,
                idClient: this.clientNumberInput.getValue().value,
                price: this.totalAmountPayment.getValue().value
            }
            cityConfirmPaymentPost(data).then(
                (res) => {
                    Notifications.show(res.success, 'success')
                    UserDetailsService.getUserDetails()
                    Loader.hide()
                    this.setState({
                        modalMessage: LM.getString("successModalMessage"), modalMessageClassName: true,
                        successMessage: res.success,
                        transactionId: res.currentTransaction
                    })
                }
            ).catch((error) => {
                Loader.hide()
                if (error && error.response && error.response.data && error.response.data.error) {
                    Notifications.show(error.response.data.error, 'danger')
                } else {
                    Notifications.show(LM.getString("error_while_saving"), 'danger')
                }
            })
        }
    }
    totalAmountSetter = () => {
        this.totalAmountPayment.setValue(this.cityAmount.getValue().value)
    }
    render() {
     

        return (
            <div className='main_border_padding CityPayProceed'>
                <p className='mt-2 fs-17'>
                    {LM.getString('payment')}
                    {` ${this.props.service} ${LM.getString("of")}${this.props.city}`}
                </p>
                <Row className="mt-4">
                    <Col lg='4' sm='6'>
                        <InputTypeNumber
                            id={'ClientNumberInput'}
                            title={LM.getString("clientNumber")}
                            required='true'
                            ref={(objReference) => { this.clientNumberInput = objReference }}
                        />
                    </Col>
                    <Col lg='4' sm='6'>
                        <InputTypeNumber
                            id={'TerminalInput'}
                            title={LM.getString("clearingHouseNumber")}
                            required='true'
                            ref={(objReference) => { this.terminalInput = objReference }}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col lg='4' sm='6'>
                        <InputTypePhone
                            id={'SupplierTelnumTitle'}
                            title={LM.getString("supplierTelnumTitle")}
                            required='true'
                            ref={(objReference) => { this.supplierTelnumTitle = objReference }}
                        />
                    </Col>
                </Row>
                <Button size='lg' className={'border-radius-3 btn-lg font-weight-bold fs-17 px-3 regularButton ' +
                    'mt-md-3 btn-sm-block'} onClick={this.showInvoice}>
                    {LM.getString("showAmount")}
                </Button>
                <div className='mt-4 mt-md-5 top-border'>
                    <Row className={'mt-4'}>
                        <Col md='5' className={(LM.getDirection() === 'rtl' ? 'left-border ml-lg-2 pl-lg-4' : 'right-border mr-lg-2 pr-lg-4')}>
                            <span className='fs-17'>{LM.getString("authorityPayment")}</span>
                            <Row className='mt-3'>
                                <Col xl='10' md='12' sm='6'>
                                    <InputTypeNumber
                                        id={'CitytotalPayment'}
                                        title={LM.getString("totalPayment")}
                                        required='true'
                                        onChange={this.totalAmountSetter}
                                        ref={(objReference) => { this.cityAmount = objReference }}
                                    />
                                </Col>
                                <Col xl='10' md='12' sm='6'>
                                    <InputTypeNumber
                                        id={'TotalamountPayment'}
                                        title={LM.getString("TotalamountPayment")}
                                        required='true'
                                        disabled='true'
                                        ref={(objReference) => { this.totalAmountPayment = objReference }}
                                    />
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    {LM.getString("Commission0")}
                                </Col>
                            </Row>
                            <div className={'d-sm-flex'}>
                                <Button onClick={this.confirmPayment}
                                    className={'border-radius-3 btn-lg font-weight-bold fs-17 px-4 regularButton ' +
                                        'mt-md-3 btn-sm-block'}>{LM.getString("confirm")}</Button>
                                <Button className={'border-radius-3 btn-lg font-weight-bold fs-17 px-4 mt-md-3 mt-sm-0 mt-2 mx-sm-2 btn-sm-block'}>{LM.getString("cancel")}</Button>
                            </div>
                        </Col>
                        <Col className={'mt-3 mt-md-0'}>
                            <img className='imgSize' src={cityImg} alt='' />
                        </Col>
                        <SuccessModalWindow
                            printTransactionId={this.state.transactionId}
                            isOpen={this.state.modal}
                            className={this.props.className}
                            toggle={this.toggle}
                            modalMessage={this.state.modalMessage}
                            messageDivClassName={this.state.modalMessageClassName ? 'messageGreenColor' : 'messageRedColor'}
                            cancelClick={this.toggle}
                            printClick={this.toggle}
                            validationImage={this.state.modalWindowImage}
                        />
                    </Row>
                </div>
            </div>
        );
    }
}