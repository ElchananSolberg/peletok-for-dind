from selenium.webdriver.common.by import By


class PersonalAreaPage:
    change_password_button = (By.ID, 'personalAreaChangePasswordButton')
    exit_changes = (By.CSS_SELECTOR, ':nth-child(9)>div>div.modal.fade.show>div>div>div.mr-sm-1.pr-sm-4.modal-header>button')
    input_current_password = (By.ID, 'InputTypeTextcurrentPassword')
    input_new_password = (By.ID, "InputTypeTextnewPassword")
    input_new_password_again = (By.ID, 'InputTypeTexttypeNewPasswordAgain')
    update_password_button = (By.ID, 'updatePasswordButton')