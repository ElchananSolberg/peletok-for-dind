import React from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import './Gifts.css';
import { LanguageManager as LM } from "../LanguageManager/Language";
import { Gifts } from './Gifts'
import { getOrderedGifts } from '../DataProvider/Functions/DataGeter'
import { putCancelGift } from '../DataProvider/Functions/DataPuter'
import { Notifications } from '../Notifications/Notifications'
import { UserDetailsService } from '../../services/UserDetailsService';
import { Loader } from '../Loader/Loader'

/**
 * A component that displays selected gifts to allow the user to cancel an invitation
* TODO --Get a real list from the database, and put functionality in the check box
 */
export class Cancelation extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            giftsIDs: [],
            points_required: 0,
            gifts: []
        };
        this.onChange = this.onChange.bind(this);
        this.newList = this.newList.bind(this);
        this.cancelSelectedGifts = this.cancelSelectedGifts.bind(this);

    }
    componentDidMount() {
        this.newList()
    }
    cancelSelectedGifts() {
        Loader.show()
        putCancelGift({
            "gift_id": this.state.giftsIDs,
        }).then(
            (res) => {
                Loader.hide()
                UserDetailsService.getUserDetails()
                this.setState({ giftsIDs: [] })
                this.newList()

            }
        ).catch(
            (err) => {
                Loader.hide()
                Notifications.show(err, 'danger');
            }
        )
    }
    onChange(isCheckd, prop) {
        let giftsIDs = [...this.state.giftsIDs];
        let points_required = this.state.points_required;
        if (isCheckd) {
            giftsIDs.push(prop.gift_id)
            points_required += prop.points_required;
        } else {
            giftsIDs = giftsIDs.filter(id => prop.gift_id !== id)
            points_required = points_required - prop.points_required;
        }

        this.setState({ giftsIDs })
        this.setState({ points_required })
    }
    newList() {
        this.setState({ gifts: [] })
        getOrderedGifts().then(
            (res) => {
                var list;
                list = res.filter(obj => obj.status == 1)

                this.setState({ gifts: list })
            }
        ).catch(
            (err) => {
                Notifications.show(err, 'danger');
            }
        )

    }


    render() {

        return (
            <div >
                <h3 className='maing'>{LM.getString('cancelGift')}</h3>
                <div className='cards'>
                    <Container>
                        <Row>
                            <Col className='startHeader' sm='6'>{LM.getString('ChooseGiftsToCancel')}</Col>
                        </Row>
                        {this.state.gifts.map((gift, index) =>
                            (index % 2 == 0) ?
                                <Row className="my-2">
                                    <Col sm='6'>
                                        <Gifts  onChange={this.onChange} enabled='true'{...gift} />
                                    </Col>
                                    {
                                        (index + 1 < this.state.gifts.length) ?
                                            <Col sm='6'>
                                                <Gifts  onChange={this.onChange} enabled='true'{...this.state.gifts[index + 1]} />                                            </Col>
                                            :
                                            <React.Fragment />
                                    }
                                </Row>
                                :
                                <React.Fragment />)}
                        <Row>
                            <Col >
                                <Button className='myButton' onClick={this.cancelSelectedGifts}>{LM.getString('CancelSelectedGifts')}</Button>
                            </Col>
                        </Row>

                    </Container>

                </div>
            </div>
        );
    }
}
