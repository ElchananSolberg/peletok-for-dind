const BaseModel = require('../../BaseModel');
const DataTypes = require("sequelize").DataTypes;

/**
 * Address model
 */
class Address extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            country: {
                type: DataTypes.STRING(64),
                allowNull: true
            },
            city: {
                type: DataTypes.STRING(64),
                allowNull: true
            },
            street: {
                type: DataTypes.TEXT(),
                allowNull: true
            },
            number: {
                type: DataTypes.STRING(8),
                allowNull: true
            },
            entrance: {
                type: DataTypes.STRING(10),
                allowNull: true
            },
            floor: {
                type: DataTypes.STRING(5),
                allowNull: true
            },
            zip_code: {
                type: DataTypes.STRING(7),
                allowNull: true
            },
            latitude: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            longitude: {
                type: DataTypes.DOUBLE,
                allowNull: true
            }
        };
        this.author = true;
        this.statusAuthor = true;
        this.statusField = true;
        this.statusTimestampField = true;
        this.prefix = "USR";
    }

    /**
     * creates associate for user model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.hasMany(models.branchAddress.model, {as: 'BranchAddress' });
        this.model.hasMany(models.businessAddress.model, {as: 'BusinessAddress' });
    }
}

module.exports = new Address();
