const BasicCalc = require('./baseCalc')
const models = require('../index');

const businessProduct = models.businessProduct
const businessProductMode = businessProduct.model
const product = models.product
const productMode = product.model
const supplier = models.supplier
const supplierMode = supplier.model
const business = models.business
const businessMode = business.model



/**
 * to  calculation   commissions  for   business_price 
 */
class CommissionCalk extends BasicCalc {


    /**
     * to  calculation BusinessProfit
     */
    async calculateBusinessProfit(businessId) {

        let profit = (await businessProductMode.findOne({ where: { business_id: businessId, product_id: this.req.body.itemId }, attributes: ["business_commission"] })).business_commission
        return this.commitBusinessProfit(profit)


    }



}
module.exports = CommissionCalk;