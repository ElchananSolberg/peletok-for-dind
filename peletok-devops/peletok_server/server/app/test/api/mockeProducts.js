const {getTypeNum, getStrType, getNumStatus} = require('../../utils')
const moment = require('moment')
const models = require('../../index.js')


let params = {
        more_details_link: 'test',
        video_link: 'test',
        call_terms: 'text',
        order: 4,
        id: 1,
        status: 2,
        // status_author_id: auther.id,
        status_timestamp: new Date,
        usage_time_limit: 5,
        
        is_abstract: false,
        profit_model: 'profit',
        

        // productDetails fields
        // seller_id: seller.id, // ?
        call_minute: 30,
        sms: 90,
        browsing_package: '100',
        call_to_palestinian: true,
        abroed_price: 12,
        other1: 'test other1',
        other2: 'test other1',
        status: 1,
        default_is_allow: true,
        cancelable: true,

        language_id: '',
        name_he: 'he name',
        name_en: 'en name',
        name_ar: 'ar name',
        product_title: 'product title',
        description_he: 'he description',
        description_en: 'en description',
        description_ar: 'ar diescription',
        title: 'title',
        title_he: 'he title',
        title_en: 'en title',
        title_ar: 'ar title',
        name: 'product name',
        name_by_language: '',
        more_info_description: 'more_info_description',
        more_info_youtube_url: 'youtube_url',
        more_info_site_url: 'site_url',

        //documents
        image: 'image',
        more_info_image: 'more info image',

        // price
        // author_id: auther.id,
        price: 23,
        buying_price: 22,
        distribution_fee: 3
     }
async function mockProducts(){

            let tags = await models.tag.model.bulkCreate([
                {"tag_name":"cancellations","order":null,"type":1,"show_on_product":null,"id":1},
                {"tag_name":"credits","order":1,"type":2,"show_on_product":null,"id":2},
                {"tag_name":"המגזר החרדי","order":"1","type":1,"show_on_product":true,"id":3},
                {"tag_name":"A שטחי","order":"2","type":2,"show_on_product":false,"id":4},
                {"tag_name":"שטחי הרשות","order":"3","type":2,"show_on_product":true,"id":5}
             ])
        
            let current_user = await models.user.model.create({
                 login_name: 'admin',
                 first_name: 'user',
                 last_name: 'family',   
             })
           

            let supplier1 = await models.supplier.model.create({
                 name: 'supplier 1', 
             })

             let supplier2 = await models.supplier.model.create({
                 name: 'supplier 2', 
             }) 
             let seller = await models.business.model.create({
                 name: 'seller name', 
             })
             
             let business = await models.business.model.bulkCreate([
                {name: 'business test', }
             ]) 

             let languages = await models.language.model.bulkCreate([
                 {
                    code: 'HE'
                 },
                 {
                    code: 'EN'
                 },
                 {
                    code: 'AR'
                 },
             ])

             let barcodTypes = await models.barcodeType.model.bulkCreate([
                {
                    name: 'peletok',
                    required: true,
                    status: 2,
                },
                {
                    name: 'delek',
                    required: false,
                    status: 2,
                },
                 {
                    name: 'delek11',
                    required: false,
                    status: 2,
                }
             ])
             
             for(key in [1,2,3]){
                 await models.product.model.create({
                        call_terms: params.call_terms,
                        order: params.order,
                        supplier_id: key == 2 ? supplier2.id : supplier1.id,
                        // id: params.id,
                        status: params.status,
                        status_author_id: current_user.id,
                        status_timestamp: params.status_timestamp,
                        usage_time_limit: params.usage_time_limit,
                        type: key == 2 ? 1 : parseInt(key) + 1,
                        is_abstract: params.is_abstract,
                        profit_model: params.profit_model,
                        more_info_description: params.more_info_description,
                        more_info_youtube_url: params.more_info_youtube_url,
                        more_info_site_url: params.more_info_site_url,
                        call_minute: params.call_minute,
                        sms: params.sms,
                        browsing_package: params.browsing_package,
                        call_to_palestinian: params.call_to_palestinian,
                        abroed_price: params.abroed_price,
                        other1: params.other1,
                        other2: params.other2,
                        status: params.status ? getNumStatus('active', 'Product') : getNumStatus('inactive', 'Product'),
                        default_is_allow: params.default_is_allow,
                        cancelable: params.cancelable,
                        ProductDetails: [ 
                            {
                                 product_description: params.description_he + key,
                                 product_name: params.name_he + key,
                                 language_code: 'HE',
                            },
                            {
                                 product_description: params.description_en + key,
                                 product_name: params.name_en + key,
                                 language_code: 'EN',
                            },
                            {
                                 product_description: params.description_ar + key,
                                 product_name: params.name_ar + key,
                                 language_code: 'AR',
                            },
                        ],
                        Document: [
                            {url: params.image, type: getTypeNum('image', 'Document')},
                            {url: params.more_info_image, type: getTypeNum('other_image', 'Document')}
                        ], 
                        ProductPrice: [
                            {
                                fixed_price: params.price,
                                buying_price: params.buying_price,
                                distribution_fee: params.buying_price,
                                timestamp_start: moment(),
                                timestamp_end: moment('2099-12-31')
                            }
                        ],
                        ProductBarcode: [
                            {
                                barcode: '12345',
                                barcode_type_id: 1,
                            },
                             {
                                barcode: '123456',
                                barcode_type_id: 2,
                            }
                        ], 
                        TalkPrice: [
                            {country: "ישראל", outcome_price: 7, income_price: 9, sms_price: 10},
                            {country: "ארהב", outcome_price: 7, income_price: 9, sms_price: 10},
                            {country: "אוקרינה", outcome_price: 7, income_price: 9, sms_price: 10},
                            {country: "צרפת", outcome_price: 7, income_price: 9, sms_price: 10},
                        ]
                    },{
                        include: [
                            {
                                model: models.productDetails.model, as: 'ProductDetails'
                            },
                            {
                                model: models.document.model, as: 'Document'
                            },
                            {
                                model: models.productPrice.model, as: 'ProductPrice'
                            },
                            {
                                model: models.productBarcode.model, as: 'ProductBarcode'
                            },
                            {
                                model: models.talkPrice.model, as: 'TalkPrice'
                            }
                        ]
                 })
             }
             return [current_user, supplier1, business]
         }

  module.exports = {
      params,
      mockProducts,
  }