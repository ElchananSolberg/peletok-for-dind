'use strict';
const {addColumnsToTable} = require('../dbUtils/addPermission')
module.exports = {
  async up(queryInterface, Sequelize) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    
    await  addColumnsToTable(queryInterface, 'HASH_HashavshevetExport', [
        {
          name: 'meta', typeOptions: {
            type: Sequelize.TEXT,
            allowNull: true
          }
        },
        {
          name: 'max_order_number', typeOptions: {
            type: Sequelize.INTEGER,
            allowNull: false
          }
        }
      ])
    
  },
  async down(queryInterface, Sequelize) {
    
  }
};
