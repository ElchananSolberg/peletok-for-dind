var models = require('../index.js');
const removeAbsolutePath = require("../utils").removeAbsolutePath;
var supplier = models.supplier;
supplierModel = supplier.model;
const {GenericGet} = require('../mapping/GenericGet');
const GenericCreate = require('../mapping/GenericCreate');
const logger = require('../logger');
const {getMappingPath, get} = require("../mapping/crudMiddleware");
const mapByType = require('../utils.js').mapByType

//TODO: Use in type.json file
/**
 * Sorts suppliers by type (prepaid, bill)
 * @param arr suppliers array
 * @return {{prepaid: Array, bills: Array}}
 */
function mapSuppliersByType(arr) {
    Object.values(arr).forEach(value => {
        //TODO: Change
        value["available"] = true;
    });
    let newObj = mapByType('supplier',arr)
    return newObj;
}

/**
 * fetching all suppliers data and returns it for charging
 */
async function wrapperGetSupplier(req, res) {
    let mappingPath = getMappingPath(req);
    const filter = req.params.id ? {id: req.params.product_id} : {};
    let info = await get(mappingPath, filter);
    if(info.result){
        info.result = mapSuppliersByType(info.result);
    }
    res.customSend(info);
}

/**
 * fetching all suppliers data and returns it for edition
 */
async function wrapperGetSupplierList(req, res) {
    let mappingPath = getMappingPath(req);
    const filter = req.params.id ? {id: req.params.product_id} : {};
    let info = await get(mappingPath, filter);
    res.customSend(info);
}

/**
 * Creates supplier and returns it
 */
async function createSupplier(path, objData) {
    // TODO: use err to checking if success
    let err;
    let result = await new GenericCreate(path, objData).create().catch(e => {
        logger.error(e);
        err = e;
    });
    Object.values(result).forEach(r => removeAbsolutePath(r, 0));
    return result;
}



module.exports.wrapperGetSupplier = wrapperGetSupplier;
module.exports.wrapperGetSupplierList = wrapperGetSupplierList;
module.exports.createSupplier = createSupplier;