/**
 * static'ish class that manages the language resources for the app
 * strings must be defined in the Strings.JSON file in the same directory.
 *
 * IMPORTANT - 'he' value is mandatory as it is the fallback of a value not existing in a certain language
 *
 */
export class LanguageManager {
    static LangSelected = 'He';
    static direction = 'ltr';
    static supportedLanguages = ['He', 'En', 'Ar'];
    static strings = require("./Strings");

    /**
     * changes language to a given language
     * only works if lang is supported
     * (also manages the direction variable)
     * @param lang the language to switch to
     */
    static setLang(lang) {
        if (this.supportedLanguages.includes(lang)) {
            this.LangSelected = lang;
            switch (lang) {
                case 'He':
                case 'Ar':
                    this.direction = 'rtl';
                    break;
                default:
                    this.direction = 'ltr';
                    break;
            }
        }
        let html = document.getElementsByTagName('html')[0];
        if(this.direction === 'rtl'){
            html.style.setProperty('--indexBorders','0 4px 4px 0');
            html.style.setProperty('--contentBorders','4px 0 0 4px');
            html.style.setProperty('--logoPaddingFix','-40px');
            html.style.setProperty('--priceBorders','0px 20px 0px 5px');
            html.style.setProperty('--noteAlign','right');
            html.style.setProperty('--imageAlign','left');
            html.style.setProperty('--agentsHeaderTable9','0px 5px  0px 0px');
            html.style.setProperty('--agentsHeaderTable3','5px 0px  0px 0px');
            html.style.setProperty('--marginButton','0px 0px  0px 5px');
            html.style.setProperty('--marginManualCards','0px 2%  0px 0px');
            html.style.setProperty('--marginManualCardsButton','2.5% 2%  0px 0px');
            html.style.setProperty('--marginConfigVat','0% 0%  0px 0px');
            
        } else {
            html.style.setProperty('--indexBorders','4px 0 0 4px');
            html.style.setProperty('--contentBorders','0 4px 4px 0');
            html.style.setProperty('--logoPaddingFix','0');
            html.style.setProperty('--priceBorders','20px 0px 5px 0px');
            html.style.setProperty('--noteAlign','left');
            html.style.setProperty('--imageAlign','right');
            html.style.setProperty('--agentsHeaderTable9','5px 0px  0px 0px');
            html.style.setProperty('--agentsHeaderTable3','0px 5px  0px 0px');
            html.style.setProperty('--marginButton','0px 5px  0px 0px');
            html.style.setProperty('--marginManualCards','0px 0px  0px 2%');
            html.style.setProperty('--marginManualCardsButton','2.5% 0px  2% 0px');
            html.style.setProperty('--marginConfigVat','0% 0%  0px 2px');
            
        }
    }

    /**
     * returns the language defined by the object
     * @returns {string}
     */
    static getLang() {
        return this.LangSelected;
    }

    /**
     * returns the direction of the language
     * @returns {string} string repr of the direction of the language selected
     */
    static getDirection() {
        return this.direction;
    }

    /**
     * returns the text value corresponding to the language selected and the text required
     * @param name the name of the string to get
     * @returns {string} The text value of the requested name as defined in Strings.JSON -
     *                   if 'name' does not appear in String.JSON - an error value is returned
     *                   if name appears but selected language does not exist - hebrew value is returned
     */
    static getString(name) {
        if (Object.keys(this.strings).includes(name)) {
            switch (this.LangSelected) {
                case 'En':
                    if (Object.keys(this.strings[name]).includes('en'))
                        return this.strings[name].en;
                    break;
                case 'Ar':
                    if (Object.keys(this.strings[name]).includes('ar'))
                        return this.strings[name].ar;
                    break;
                default: // default is hebrew
                    return this.strings[name].he;
            }
            return this.strings[name].he;
        }
        return "STRING NOT DEFINED IN STRING.JSON";
    }

    /**
     * returns the text value corresponding to the language selected and the text required
     * And clear : from end text (for reports headers)
     * @param name the name of the string to get
     * @returns {string} The text value of the requested name as defined in Strings.JSON -
     *                   if 'name' does not appear in String.JSON - an error value is returned
     *                   if name appears but selected language does not exist - hebrew value is returned
     */
    static getStringWithoutColon(name){
        let str = this.getString(name).trimEnd();
        let strLen = str.length;
        let lastChar = str.charAt(strLen-1);
        if(lastChar===':'){
            str = str.substring(0, strLen-1);
        }
        return str;

    }

    static getStringFromDb(object, attrName, defaultString) {
        return object[`${attrName}_${this.LangSelected}`.toLowerCase()] || defaultString;
    }

}
