import axios from 'axios';
import {config as SERVER_BASE} from "../utils/Config";
import {reloadData} from "../Components/DataProvider/DataProvider";

export class AuthService{
    static _isAuth
    static allowEdit = false;
    static currentPath = false;
    roles;
    permission;

    static allowed(url){
        // return true;
        if(['/prevent_baners', '/activate_business', '/show_navbar_main_button'].indexOf(url) == -1){

            AuthService.currentPath = url
        }
        const permissions = JSON.parse(window.atob(window.localStorage.getItem('permissions'))) || [];
        const currentPermission = permissions.find(p=>p.path == url)
        if(currentPermission){
            AuthService.allowEdit = currentPermission.allowEdit;
            return true
        }else{
            return false
        }
          
    }

    static canEdit(){
        return AuthService.allowEdit;
    }

    static hasAllowedChild(url){
        // return true;
        let pathes = (JSON.parse(window.atob(window.localStorage.getItem('permissions'))) || []).map(p=>p.path);
        let basePathRegExp = new RegExp(`^/${url}`)
        return !!pathes.find(p=>basePathRegExp.test(p)) ;
          
    }

    static isAuth(){
        return new Promise((resolve, reject)=>{
            if(AuthService._isAuth){
                resolve(true)
            }else{
                
                axios.get(`${SERVER_BASE.baseUrl}/auth/is_auth`, { withCredentials: true }).then(res => {
                    if(res.data) reloadData();
                    AuthService._isAuth = res.data                    
                    resolve(AuthService._isAuth)
                }).catch(err => {
                    reject(err);
                })
            }
        })
        
    }

    static login(userAndPass){
        return new Promise((resolve, reject)=>{
            let token = window.localStorage.getItem('token')
            let params = Object.assign(userAndPass, {token: token || ''})
            axios.post(`${SERVER_BASE.baseUrl}/auth/login`, params, {withCredentials: true}).then(res => {
                let first_response = res.data
                if (first_response.msg === "success") {
                    if(first_response.token){
                        window.localStorage.setItem('token', first_response.token)
                    }
                    axios.get(`${SERVER_BASE.baseUrl}/permissions/user_permissions`, { withCredentials: true }).then((res)=>{
                        window.localStorage.setItem('permissions', res.data)
                        resolve(first_response.msg)
                    }).catch(err => {
                        resolve(first_response.msg)
                        // reject(err)
                    })
                }
                 window.localStorage.removeItem('distributor')
                 window.localStorage.removeItem('seller')
                
            }).catch(err => {
                reject(err)
            })
        })   
    }

    static logout(userAndPass){
         return new Promise((resolve, reject) => {
             axios.post(`${SERVER_BASE.baseUrl}/auth/logout`, {}, { withCredentials: true }).then((res) => {
                 if (res.data === "success") {
                    window.localStorage.removeItem('permissions');

                    window.localStorage.removeItem('seller');
                    window.localStorage.removeItem('distributor');

                    window.location.reload();
                 }
                 resolve(res.data)
             } ).catch(err => {
                reject(err)
            })
         } )
    }

    static addQueryParams(url){
        if(url == '/banner'){
            return url
        }
        if(/\?/.test(url)){
            return `${url}&currentPath=${AuthService.currentPath}`
        } else {
            return `${url}?currentPath=${AuthService.currentPath}`
        }
    }
}
