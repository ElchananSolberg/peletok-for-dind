const express = require('express');
const permissoinsRouter = express.Router();
const models = require('../index.js');
const {PermissonGroupsTypesEnum} = require('../enums/permission_role_types_enum');


const User           = models.user.model;
const Role           = models.role.model;
const Permission     = models.permission.model
const PermissionRole = models.permissionRole.model
const PermissionGroup = models.permissionGroup.model


permissoinsRouter.get('/user_permissions', (req, res) => {
    const UserRoles = Role.scope({ method: ['userRoles', req.user.id]});
    UserRoles.findAll().then(async (roles) => {
        const rolesValidPermissions = Permission.scope({ method: ['rolesValidPermissions', roles.map(r=>r.id)]});
        const permissions = await rolesValidPermissions.findAll({raw: true});
        let response = permissions.map(p=>{return {path: p.path, allowEdit: p['Roles.PermissionRole.action'] == PermissonGroupsTypesEnum[2]}})
        let encodedResponse = Buffer.from(JSON.stringify(response)).toString('base64')
        res.send(encodedResponse)
    } ).catch((err) => {
        res.send(err)
    } )
    
    
})




/*-----------------------------    REST section    ---------------------------------*/

permissoinsRouter.get('/', async (req, res) => {
    const [permissions, permissionGroups, roles] = await Promise.all([
        Permission.findAll({
            where: {type: 'ui'},
            attributes: ['id', 'name', ['permission_group_id', 'permissionGroupId']],
            order: ['permission_group_id']
        }),
        PermissionGroup.findAll({
            attributes: ['id', 'name'],
            order: ['id']
        }),
        models.role.model.findAll({
            attributes: ['id', 'name'],
            include: {
                attributes: ['name'],
                model: models.role.model, as: 'AllowedRoles'
            }
        })
    ])

    res.send({permissions, permissionGroups, roles})
    
} )


module.exports.permissoinsRouter = permissoinsRouter;
