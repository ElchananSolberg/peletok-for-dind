'use strict';
const {addPermission, renamePermission} = require('../dbUtils/addPermission');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      addPermission(queryInterface, 'פרטי ספק', 'api', 'GET /product/supplier_details', 13),
      addPermission(queryInterface, 'עדכון עסק מוצר', 'api', 'PUT /product/update_authorization', 13),
      addPermission(queryInterface, 'קבלת נעולים', 'api', 'POST /business/get_locked', 12),
      renamePermission(queryInterface, '/config/main/profitPercentages', '/config/main/productAuth/porfit'),
      renamePermission(queryInterface, '/config/main/commissions', '/config/main/productAuth/commissions'),
    ])
  },

  down: (queryInterface, Sequelize) => {
   return Promise.resolve()
  }
};
