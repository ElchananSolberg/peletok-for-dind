

var webdriver = require('selenium-webdriver');
var until = webdriver.until;

//TODO: hardcodded vars:
var payerName = "ravtech"
    , payerMail = "ravtech@gmail.com"
    , creditNumber = "88888888"
    , payersidNumber = "032778888"
    , cvvNum = "024"
    , year = "2019"
    , month = "03"
    , link = 'https://ecom.gov.il/Voucher/InputPage.aspx?vid=318'

    //TODO: comes from client:
    , reportNumber = 50153021923;

var driver = new webdriver.Builder().usingServer().withCapabilities({ 'browserName': 'chrome' }).build();


/*
 * 
 * reverse String
 * 
 * @param {string} strToReverse 
 * 
 */
function reverseString(str) {
    return str.split("").reverse().join("");
}


/**
 * close the browser
 */
function closeBrowser() {
    driver.quit();
}

/**
 * driver Wait to find element by id
 * 
 * @param {string} idElementToFind
 * 
 */
function driverWaitById(idElementToFind) {
    return (
        driver.wait(until.elementLocated(webdriver.By.id(idElementToFind)))
    )
}

/**
 * driver Wait to find element by xpath
 * 
 * @param {string} xpathElementToFind
 * 
 */
function driverWaitByXpath(xpathElementToFind) {
    return (
        driver.findElement(webdriver.By.xpath(xpathElementToFind))
    )
}


/**
 * Check Police Report
 * 
 * @param {string} reportNumber 
 */
async function checkPoliceRepot(reportNumber) {
    await driver.get(link);

    // Select a button according to the report number

    const police_button = await driverWaitById('ctl00_serviceSelect5')
    await police_button.click()

    // Entering a report number

    const police_input = await driverWaitById('ctl00_rowInputText_RowId_3_0_0')
    await police_input.sendKeys(reportNumber)

    const police_enter = await driverWaitById('ctl00_btnConfirm')
    await police_enter.click();



    // Getting error if report number does not exist
    try {
        var errorPage = await driverWaitByXpath('//*[@class="Common"]/li').getText();
        var existed = await driverWaitByXpath('//*[@class="Common"]/li').then(function () {
            console.log(reverseString(errorPage))
            closeBrowser()
        })
    } catch (StaleElementReferenceError) { }



    report_number_from_page = await driverWaitByXpath('//*[@id="ctl01_gridItems"]/tbody/tr[2]/td[5]').getAttribute("innerText");
    if (report_number_from_page == reportNumber) {

        checking_correct_row = await driverWaitByXpath('//*[@id="ctl01_gridItems"]/tbody/tr[2]/td[5]/parent::tr/td/*[@id="ctl01_gridItems_ctl02_checkboxCol"]')
        if (checking_correct_row.getAttribute("checked") == null) {
            await checking_correct_row.click();
        }
    } else {
        return ("Report number not found")
    }
    const cardContinue = await driverWaitById('ctl01_CardContinueImage');
    await cardContinue.click();
}

/**
 * penter credit card police report
 */
async function enterCreditCardPoliceReport() {

    const cardTbReceipientName = await driverWaitById('CreditCardUserControl1_cardTbReceipientName');
    await cardTbReceipientName.sendKeys(payerName)
    const txtUserMail = await driverWaitById('CreditCardUserControl1_txtUserMail');
    await txtUserMail.sendKeys(payerMail)
    const cardNumber = await driverWaitById('CreditCardUserControl1_cardNumber');
    await cardNumber.sendKeys(creditNumber)
    const years = await driverWaitById('CreditCardUserControl1_years');
    await years.click();
    await years.sendKeys(year)
    await years.click();
    const months = await driverWaitById('CreditCardUserControl1_months');
    await months.click();
    await months.sendKeys(month)
    await months.click();
    const idNumber = await driverWaitById('CreditCardUserControl1_idNumber');
    await idNumber.sendKeys(payersidNumber)
    const cvvNumber = await driverWaitById('CreditCardUserControl1_cvvNumber');
    await cvvNumber.sendKeys(cvvNum)
    const iAgree = await driverWaitById('CreditCardUserControl1_chkBoxEula');
    await iAgree.click();
    const buy_img = await driverWaitById('buy_img');
    await buy_img.click();
}

/**
 * 
 * Clicks to confirm payment, after entering credit card details
 */
async function payPoliceReport() {
    const pay = await driver.wait(until.elementLocated(webdriver.By.xpath('//*[@id="CreditCardUserControl1_payAnchor"]/img')));
    await pay.click();
    // Write an error message
    try {
        var errorPageEnd = await driverWaitByXpath('//*[@id="ErrorDisplayUserControl_lblErrorMsg"]/ul/li').getText();
        var existedEnd = await driverWaitByXpath('//*[@id="ErrorDisplayUserControl_lblErrorMsg"]/ul/li').then(function () {
            console.log(reverseString(errorPageEnd))
        });
    } catch{
    }
    closeBrowser()
}
/**
 * Calls the three functions synchronously
 */
async function payPoliceReportMinFunction() {

    await checkPoliceRepot(reportNumber);

    await enterCreditCardPoliceReport();

    await payPoliceReport();


}
payPoliceReportMinFunction();