import json
from rest_framework import status
from django.test import SimpleTestCase, Client
from django.urls import reverse
from functools import reduce



# initialize the APIClient app
client = Client()

class MastPaymentTest(SimpleTestCase):
    """ Test module for GET all products API """


    # def test_get_all_products(self):
    #     # get API response
    #     response = client.get(reverse('get_products'))
    #     # get data from db
    #     products = Product.objects.all()
    #     serializer = ProductSerializer(products, many=True)
    #     self.assertEqual(response.data, {'items': serializer.data})
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_details(self):
        response = client.post(reverse('post_get_details'), data={'category_id': 2})
        self.assertEqual(response.status_code, status.HTTP_200_OK)