from selenium.webdriver.common.by import By


class CallingcardsPage:
    calling_cards = (By.XPATH, '//*[@class="priceText" and contains(text(),"{}")]')
    details_of_cards = (By.XPATH,
                        "//*[@class='prepaidLink' and  @href='/main/charge/prepaid/{num_of_picture}' and div//div[contains(text(),'{price_on_detail}')]] \
                         /div[@class='product-more-details' and contains(text(),'לפרטים')]")
    alert_details = (By.XPATH, '//*[@id="product-price" and contains(text(), "{}")]')

