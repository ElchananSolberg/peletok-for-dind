from tests.test_configuration.base_configuration_test_class import *


class TestGeneralVat(BaseConfigurationTestClass):


    def test_general_vat_login_and_get_in_configuration(self):
        self.login_and_get_in_Configuration()
        self.select_config_link_options('general')
        self.remove_logs_dir = True

    def test_general_vat_changes_and_saved(self):
        self.login()
        vat = self.get_vat()
        self.change_vat(vat + 1)
        self.restart_driver()
        self.login()
        assert vat + 1 == self.get_vat(), 'vat changes are not saved'
        self.change_vat(vat)
        self.remove_logs_dir = True