'use strict';
const {addPermission} = require('../dbUtils/addPermission');

module.exports = {
  up: (queryInterface, Sequelize) => {
   return addPermission(queryInterface, 'עסקאות דמה', 'ui', '/demo_transaction', 26)
  },

  down: (queryInterface, Sequelize) => {
   return Promise.resolve()
  }
};

