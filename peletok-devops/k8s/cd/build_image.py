import os
import argparse
import re
import subprocess
from _decimal import Decimal


class BuildImage:
    def __init__(self, namespace: str, service_name='web'):
        self.namespace = namespace
        self.image = service_name + '-' + namespace
        self.full_image = f'noahregistries.azurecr.io/{self.image}'
        self.namespace_dir = namespace.replace('-', '_')
        if service_name == 'server':
            self.service_dir = 'server'
            self.Dockerfilename = 'server/docker/Dockerfile-server-node'
        elif service_name == 'client':
            self.service_dir = 'client'
            self.Dockerfilename = 'client/docker/Dockerfile-client-angular-k8s'
        elif service_name == 'postgres':
            self.service_dir = 'postgres'
            self.Dockerfilename = 'postgres/docker/Dockerfile-postgres'
        elif service_name == 'flowable':
            self.service_dir = 'postgres'
            self.Dockerfilename = 'postgres/docker/Dockerfile-postgres'
        else:
            raise Exception('service directory dose note existed')

    def get_last_version(self) -> dict:
        """
        Get the last tag, from the repository of Azure.
        :return dict {last_tag: '', next tag: ''}:
        """
        print('Get the last tag')
        try:
            str_tags = subprocess.check_output(
                f"az acr repository show-tags -n noahregistries --repository {self.image}",
                shell=True).decode('utf-8')

            list_tags = re.findall(r'"(.*)"', str_tags)
            str_last_tag = max(list_tags)
            last_tag_flout = Decimal(re.findall("\d+\.\d+", str_last_tag)[0])
            str_next_tag = f"v{last_tag_flout + Decimal('0.01')}"
            return {"last_tag": str_last_tag, "next_tag": str_next_tag}

        except subprocess.CalledProcessError:
            # use for new image
            return {"last_tag": 'v0.00', "next_tag": 'v1.01'}

    def build_image(self):
        versions = self.get_last_version()
        last_version = versions.get('last_tag')
        new_version = versions.get('next_tag')
        print(f'last_tag: {last_version}\nnext_tag {new_version}\n\n')
        print('Build Image')
        # remove old image from local
        os.system(f"docker rmi {self.full_image}:{last_version}")
        os.system(f"docker rmi {self.full_image}:latest")

        # remove latest tag from azure
        os.system(f"az acr repository untag -n noahregistries --image {self.image}:latest")

        # docker build image
        # os.system(f"docker build -f k8s/{self.namespace_dir}/{self.service_dir}/Dockerfile -t {self.full_image} .")
        if self.service_dir != 'flow':
            print(self.service_dir)
            os.system(f"docker build --no-cache	-f {self.Dockerfilename} -t {self.full_image} .")
        elif self.service_dir == 'flow':
            os.system("cp -rf .git ./flow/.git1")
            os.system(f"docker build -f flow/{self.Dockerfilename} -t {self.full_image} .")
            os.system("rm -rf flow/.git1")


        # add tag to image
        os.system(f"docker tag {self.full_image} {self.full_image}:{new_version}")

        # push image to azure repository
        os.system(f"docker push {self.full_image}:{new_version}")
        os.system(f"docker push {self.full_image}:latest")


class Main:

    @staticmethod
    def get_args():
        """
        get args from command line.
        :return: args
        """
        print("\nget args\n")
        # initiate the parser
        parser = argparse.ArgumentParser()
        parser.add_argument("-n", "--namespace", help="namespace of k8s", default='jordan')
        parser.add_argument("-s", "--service", help="service of deployment", default='client')

        # read arguments from the command line
        command_line_args = parser.parse_args()
        return command_line_args

    @staticmethod
    def main():
        os.system("pwd")
        args = Main.get_args()
        # os.chdir("../..")  # go to root location
        buid_image = BuildImage(namespace=args.namespace, service_name=args.service)
        buid_image.build_image()


if __name__ == '__main__':
    Main.main()
