var soap = require('soap');
var logs = require('../../logger')
const path = require('path')
const PaymentBasic = require('../paymentBasic')
const models = require('../../index')
const Resinfo = require('../../resInfo')
const LM = require('../../config/LanguageManager').LM
const utils = require("../../utils")
const product = models.product
const productModel = product.model
const {MongoLogger} = require('../../mongoLogger')
const productPrice = models.productPrice
const productPriceModel = productPrice.model

var wsdlPath = path.join(__dirname, "./wsdl/pelephone.wsdl")
const loginName = "ptalk"
const password = "f8rjw2"
const dealerNumber = "ptalk"
const lTypeIDTerminal = "177"
const lTimeOut = "30"
const dQty = "1"
const iAccessDaysApply = "1"

/***
 * Payment manager Pelephone
 *
 */
class Pelephone extends PaymentBasic {


    constructor(contractNumber, productId, businessId,req) {
        super()
        this.req=req
        this.info = new Resinfo(req)
        this.productId = productId
        this.businessId = businessId
        this.contractNumber = contractNumber
        this.timeout=3000

    }

  

    /***
            * Pay  to pelephone    
            @returns number  of   confirmation
            */
    async pay(transactionId) {
        if ((await this.isDemoTransactionUser())) {
            return this.demoRespons()
        }
        else {
        /***
                TODO 
            change  Date.now(), to    sOrderNumber: transactionId,
                  */
    
      let item = await productModel.findOne(
            {
                where: {id: this.productId}
            })

        let itemName=item["supplier_identity_number"]
        if (!itemName){
            this.info.setErrMessage(LM.getErrorBasic(), "Error in supplier_identity_number ")
            return this.info
        }

        const args = {
            sLoginName: loginName,
            sPassword: password,
            sDealerNumber: dealerNumber,
            sTerminalID: this.businessId,
            lTypeIDTerminal: lTypeIDTerminal,
            sOrderNumber: Date.now(),
            sItemNumber: itemName,
            dQty: dQty,
            sMSISDN: this.contractNumber,
            lTimeOut: lTimeOut,
            iAccessDaysApply: iAccessDaysApply
        };

        logs.info("data is " + utils.stringifyExcept(args, "sPassword", "sLoginName"))
        return this.createSoapRequest(args, transactionId)


    }
}
    /***
    * soapRequest for  step1 
    @return (resolve) number  of lXRefID
   */
    async createSoapRequest(args, transactionId) {
        let client = await soap.createClientAsync(wsdlPath);
        await client.OrderLineInsertTopUpSyncAsync(args, { timeout: this.timeout })    
         .then(
            result => {

            MongoLogger.logTransaction({
                transactionId,
                supplierId: this.req.params.supplierID,
                requestParams: this.req.body,
                request:args,
                response:result,
            })

            logs.info("result_1=", result);

            if (result[0]["OrderLineInsertTopUpSyncResult"]["lReturnCode"] == 0 && result[0]["OrderLineInsertTopUpSyncResult"]["iStatusID"] == 'AwaitingFulfullment') {
                let ixRef = result[0]["OrderLineInsertTopUpSyncResult"]["iXRefID"]
               this.requestPayment(ixRef, transactionId)


            }
            else if (result[0]["OrderLineInsertTopUpSyncResult"]["iStatusID"] == "ClosedInvalidSubscriberNumber") {
                var resErr = result[0]["OrderLineInsertTopUpSyncResult"]["sMessage"]
                this.info.error.err_in_SubscriberNumber = true
                this.info.setErrMessage(LM.getString("invalidSubscriber"), resErr)
               
            }

            else {
                var resErr = result[0]["OrderLineInsertTopUpSyncResult"]["sMessage"]
                this.info.setErrMessage(LM.getErrorBasic(), resErr)
               
            }
        })
            .catch(err => {
                this.info.error.err_connection_site = true
                this.info.setErrMessage(LM.getErrorBasic(), "Error in connection " + err)
                
            })
           
            return this.info
    }

    async requestPayment(ixRef, transactionId) {
        let argsPayment = {
            sLoginName: loginName,
            sPassword: password,
            sDealerNumber: dealerNumber,
            sTerminalID: this.businessId,
            lTypeIDTerminal: lTypeIDTerminal,
            iXRefID: ixRef
        }
        let client = await soap.createClientAsync(wsdlPath);
     await client.OrderLineXRefProcessAsync(argsPayment, { timeout: this.timeout })
            .then(result => {

                MongoLogger.logTransaction({
                    transactionId,
                    supplierId: this.req.params.supplierID,
                    requestParams: this.req.body,
                    request:argsPayment,
                    response:result,
                })

                logs.info("result_2=", result);
                if (result[0]["OrderLineXRefProcessResult"]["lReturnCode"] == 0 && result[0]["OrderLineXRefProcessResult"]["iStatusID"] == 'Unknown') 
                {
                    var res = { "success": result[0]["OrderLineXRefProcessResult"]["iXRefID"] }
                    logs.info(res)
                    this.info.confirmation = result[0]["OrderLineXRefProcessResult"]["iXRefID"]
                    this.info.isPaid = true
                    this.info.send = res
                }
                else {
                    var resErr = result[0]["OrderLineXRefProcess"]["sMessage"]
                    this.info.setErrMessage(LM.getErrorBasic(), resErr)
                }
            })
            .catch(err => {
                this.info.error.err_connection_site = true
                this.info.setErrMessage(LM.getErrorBasic(), "Error in connection " + err)
            })
                   return this.info
 }

}


module.exports = Pelephone;

