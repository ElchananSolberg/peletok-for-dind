const express = require('express');
const permissoinsRoleRouter = express.Router();
const models = require('../index.js');
const {PermissonGroupsTypesEnum} = require('../enums/permission_role_types_enum');

const Role           = models.role.model;
const User           = models.user.model;
const Permission     = models.permission.model
const PermissionRole = models.permissionRole.model
const ValidPermissionRoles = PermissionRole.scope('validPermissionRoles')





/*-----------------------------    REST section    ---------------------------------*/

permissoinsRoleRouter.get('/',(req, res) => {
    ValidPermissionRoles.findAll({
        attributes: [['role_id', 'roleId'], ['permission_id', 'permissionId'], 'action', ['expiry_date', 'date']],
        include: [
            {
                model: models.permission.model,
                as: 'Permission',
                where: {type: 'ui'},
                required: true
            }
        ]
    }).then((pr) => {
        res.send(pr);
    } ).catch((err) => {
        res.send(err)
    } )
} )



permissoinsRoleRouter.post('/',async (req, res) => {
    const roleId = req.body.roleId;
    let oldPermissionRoles = await PermissionRole.findAll({
        where: {role_id: roleId}
    })

    const permissionRolesForm = req.body.permissionRolesForm
    let existPermisionRoles = []
    for(let permissionId in permissionRolesForm){
        for(let i = 0; i < permissionRolesForm[permissionId].length; i++){
            let permissionRole = permissionRolesForm[permissionId][i];
            // If permmistionRole exist we should update date and expiry else creation new one 
            let oldPermissionRole = oldPermissionRoles.find(pr=>{return pr.permission_id == permissionRole.permissionId && pr.role_id == permissionRole.roleId})
            if(oldPermissionRole){
                await oldPermissionRole.update({
                    action: PermissonGroupsTypesEnum[permissionRole.action],
                    expiry_date: permissionRole.date || null
                })
                existPermisionRoles.push(oldPermissionRole)

            } else {
                await PermissionRole.create({
                    role_id: permissionRole.roleId,
                    permission_id: permissionRole.permissionId,
                    action: PermissonGroupsTypesEnum[permissionRole.action],
                    expiry_date: permissionRole.date
                })
            }
        }
    }
    let editedRole = await models.role.model.findOne({
        where: {
            id: roleId,
        }
    })
    let roles = await models.role.model.findAll({
        where: {
            name: req.body.roles
        }
    })
    await editedRole.setAllowedRoles(roles)
    
    
    // Delete non existing permissionRoles
    oldPermissionRoles.forEach((oldPermissionRole) => {
        if(!existPermisionRoles.find(pr=>{return pr.permission_id == oldPermissionRole.permission_id && pr.role_id == oldPermissionRole.role_id}) ){
            oldPermissionRole.destroy();
        }
    } )
    res.send('success')
} )



module.exports.permissoinsRoleRouter = permissoinsRoleRouter;
