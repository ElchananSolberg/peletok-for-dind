import React from 'react';
import { Container, Row, Col, Button, Collapse } from 'reactstrap';
import './Gifts.css';
import { LanguageManager as LM } from "../LanguageManager/Language";
import ImageUpload from '../InputUtils/ImageUpload'
import { postNewGifts } from '../DataProvider/Functions/DataPoster'
import InputTypeCheckBox from '../InputUtils/InputTypeCheckBox';
import InputTypeText from '../InputUtils/InputTypeText';
import InputTypeNumber from '../InputUtils/InputTypeNumber'
import { getAllGifts } from '../DataProvider/Functions/DataGeter'
import { putGift } from '../DataProvider/Functions/DataPuter'
import { Notifications } from '../Notifications/Notifications'
import InputTypeSimpleSelect from '../InputUtils/InputTypeSimpleSelect';
import { Confirm } from '../Confirm/Confirm';
import { config } from '../../utils/Config'


/**
 * A component that displays gift information and the option to select it
* TODO -- making user information from the database and checking how many points it has
 */
export class InsertGift extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            renderFields: true,
            id: '',
            description: "",
            points_required: "",
            status: "",
            name: "",
            giftChanged: undefined,
            creatingNew: false,
            mainGiftsList: [],
            chosenId: undefined
        }
        this.giftsList = {
            title: LM.getString("giftsList"),
            id: "GiftList",
        }
        this.giftName = {
            maxLength: '15',
            minLength: '1',
            title: LM.getString("giftName"),
            id: "giftname",
            required: true,
        }
        this.giftDescription = {
            maxLength: '15',
            minLength: '1',
            title: LM.getString("giftDescription"),
            id: "giftDescription",
            required: true,
        }
        this.pointToUse = {
            minLength: '1',
            maxLength: '4',
            title: LM.getString("pointToUse"),
            id: "pointToUse",
            required: true,
        }
        this.supplieractive = {
            checked: "",
            lableText: LM.getString("active"),
            id: "supplieractive",
        }
        this.defaultGift = {
            checked: "",
            lableText: LM.getString("default"),
            id: "default",
        }
        this.productImage = {
            title: LM.getString("productImage"),
            id: "productImage",
            required: true,
        }
        this.setValues = this.setValues.bind(this);
    }

    componentDidMount() {
        this.newList(null, true)
    }

    setValues = () => {

        let chosenGift = this.state.mainGiftsList.filter(item => item.id == this.giftsListFun.getValue().value);
        this.giftsListFun.setValue(chosenGift[0].id)
        this.giftNameFun.setValue(chosenGift[0].name);
        this.giftDescriptionFun.setValue(chosenGift[0].description);
        this.pointToUseFun.setValue(chosenGift[0].points_required);
        this.checkBoxactive.setValue(chosenGift[0].status === 1 ? true : false);
        this.checkBoxdefault.setValue(chosenGift[0].status === 1 ? false : true);
        this.imageUpload.setValue(config.baseUrl + '/' + chosenGift[0].picture);

        this.setState({ chosenId: this.giftsListFun.getValue().value })
    }

    outerOnChange = (e) => {

        this.setState({
            description: this.giftDescriptionFun.getValue().value,
            points_required: this.pointToUseFun.getValue().value,
            status: this.checkBoxactive.getValue().value ? 1 : 2,
            name: this.giftNameFun.getValue().value
        })
    }

    save = (e) => {
        this.giftNameFun.runValidation();
        this.giftDescriptionFun.runValidation();
        this.pointToUseFun.runValidation();
        this.imageUpload.runValidation();
        let validation = this.giftNameFun.getValue().valid &&
            this.giftDescriptionFun.getValue().valid &&
            this.pointToUseFun.getValue().valid &&
            this.imageUpload.getValue().valid;
        if (this.state.creatingNew && validation) {
            let data = {
                description: this.giftDescriptionFun.getValue().value,
                points_required: this.pointToUseFun.getValue().value,
                status: this.checkBoxactive.getValue().value == true ? 1 : 2,
                name: this.giftNameFun.getValue().value,
                image: this.imageUpload.getValue().value
            }
            postNewGifts(data).then(
                (res) => {
                    if (res == 'success') {
                        Notifications.show(LM.getString('newGiftEntredSuccessfully'), 'success')
                        this.setState({ creatingNew: !this.state.creatingNew });
                        this.newList();
                        this.giftsListFun.setValue(this.giftNameFun.getValue().value)
                    }
                }
            ).catch(
                (err) => {
                    Notifications.show(err, 'danger');
                })
        }
        else {

            let chosenGift = this.state.mainGiftsList.filter(item => item.id == this.state.chosenId);

            if (!this.pointToUseFun.getValue().valid) {
                Notifications.show(LM.getString('invalidPointsAmount'), 'danger')
                return
            }
            else if (validation && this.state.description != "" && this.state.name != "" && this.state.points_required != "" && this.state.status != "") {

                let updateGift = {
                    description: this.giftDescriptionFun.getValue().value,
                    points_required: this.pointToUseFun.getValue().value,
                    status: this.checkBoxactive.getValue().value == true ? 1 : 2,
                    name: this.giftNameFun.getValue().value,
                    image: this.imageUpload.getValue().value
                }

                putGift(chosenGift[0].id, updateGift).then(
                    (res) => {
                        if (res == 'success') {
                            Notifications.show(LM.getString('GiftupdateSuccessfully'), 'success')
                            this.newList();
                        }
                    }

                ).catch(
                    (err) => {
                        Notifications.show(err);
                    }
                )
            }
        }
    }


    cancel = (e) => {

        this.setState({ creatingNew: !this.state.creatingNew });
        this.setValues();
    }

    newList = (e, dontChoose = false) => {

        getAllGifts().then(
            (res) => {
                if (!dontChoose && !this.state.chosenId && this.state.mainGiftsList.length > 0) {
                    this.setState({
                        chosenId: res.map(r => r.id).find(giftId => this.state.mainGiftsList.map(g => g.id).indexOf(giftId) < 0)
                    })
                } else if (!dontChoose && res.length == 1) {
                    this.setState({
                        chosenId: res[0].id
                    })
                }
                this.setState({ mainGiftsList: res })
                if (!dontChoose && this.state.chosenId) {
                    this.giftsListFun.setValue(this.state.chosenId)
                }
            }
        ).catch(
            (err) => {
                Notifications.show(err, 'danger');
            }
        )
    }

    new = (e) => {
        this.giftsListFun.setValue('');
        this.giftDescriptionFun.setValue('');
        this.pointToUseFun.setValue('');
        this.checkBoxactive.setValue(false);
        this.checkBoxdefault.setValue(false);
        this.giftNameFun.setValue('');
        this.imageUpload.setValue('');
        this.setState({ creatingNew: !this.state.creatingNew, chosenId: null });
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        });
    }

    render() {
        return (
            <div >
                <h3 className='maingy'>{LM.getString('gifts') + (this.state.creatingNew ? ` - ${LM.getString("new")}` : "")}</h3>                <div className='cards'>
                    {this.state.renderFields && <Container>
                        <Collapse isOpen={!this.state.creatingNew}>
                            <Row >
                                <Col sm='4'>
                                    <InputTypeSimpleSelect
                                        {...this.giftsList}
                                        options={this.state.mainGiftsList.map(item => { return ({ label: item.name, value: item.id }) })}
                                        onChange={this.setValues}
                                        ref={(componentObj) => { this.giftsListFun = componentObj }}
                                    />
                                </Col>
                            </Row>
                        </Collapse>
                        <Row className='myRow'>
                            <Col sm='4'>
                                <InputTypeText
                                    {...this.giftName}
                                    onChange={this.outerOnChange}
                                    ref={(componentObj) => { this.giftNameFun = componentObj }} />
                            </Col>
                            <Col sm='4'>
                                <InputTypeText
                                    {...this.giftDescription}
                                    onChange={this.outerOnChange}
                                    ref={(componentObj) => { this.giftDescriptionFun = componentObj }} />
                            </Col>
                        </Row>
                        <Row className='myRow'>
                            <Col sm='4'>
                                <InputTypeNumber
                                    {...this.pointToUse}
                                    validationFunction={this.outerValidationFunction}
                                    onChange={this.outerOnChange}
                                    ref={(componentObj) => { this.pointToUseFun = componentObj }} />
                            </Col>
                        </Row>
                        <Row className='myRow'>
                            <Col sm='4'>
                                <InputTypeCheckBox
                                    {...this.supplieractive}
                                    validationFunction={this.outerCheckedValidationFunction}
                                    onChange={this.outerOnChange}
                                    ref={(componentObj) => { this.checkBoxactive = componentObj }} />
                            </Col>
                            <Col sm='4'>
                                <div style={{ display: 'none' }}>
                                    <InputTypeCheckBox
                                        {...this.defaultGift}
                                        validationFunction={this.outerCheckedValidationFunction}
                                        onChange={this.outerOnChange}
                                        ref={(componentObj) => { this.checkBoxdefault = componentObj }} />
                                </div>
                            </Col>
                        </Row>
                        <Row className='myRow'>
                            <Col sm='8'>
                                <ImageUpload
                                    {...this.productImage}
                                    onChange={this.outerOnChange}
                                    ref={(componentObj) => { this.imageUpload = componentObj }} />
                            </Col>
                            <Col sm='4'></Col>
                        </Row>
                        <Row>
                            <Button onClick={this.save} id='supplierSAve' style={{ marginTop: '15px', marginLeft: '20px', width: "4.5rem", backgroundColor: 'var(--newSaveButton)', borderColor: 'var(--newSaveButton)' }} >
                                {LM.getString("save")}
                            </Button>
                            <Button color="secondary" hidden={this.state.creatingNew} size="sm" onClick={this.new} id='new' style={{ marginTop: '15px', marginLeft: '20px', width: "4.5rem" }} active >
                                {LM.getString("new")}
                            </Button>
                            <Button color="secondary" hidden={!this.state.creatingNew} size="sm" onClick={this.new} id='cancel' style={{ marginTop: '15px', marginLeft: '20px', width: "4.5rem" }} active >
                                {LM.getString("cancel")}
                            </Button>
                        </Row>
                    </Container>}
                </div>
            </div>
        );
    }
}
