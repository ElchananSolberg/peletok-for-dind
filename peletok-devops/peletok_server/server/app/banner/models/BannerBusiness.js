const DataTypes = require('sequelize').DataTypes;
const BaseModel = require('../../BaseModel');

/**
 * BannerBusiness model
 */

class BannerBusiness extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {};
        this.prefix = "BAN";
    }

    /**
     * creates associate for BannerBusiness model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.business.model, { as: 'Business' });
        this.model.belongsTo(models.banner.model, { as: 'Banner' });
    }
}

module.exports = new BannerBusiness();
