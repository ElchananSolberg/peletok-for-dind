const models = require('../../index');
//TODO: replace hardcoded type
const awaitingForCancellation = 3;
const { Info } = require("../../mapping/crudMiddleware");
const moment = require('moment');
const {createCancelRequestActionNotification} = require("../../message/message_utils");

async function createCancellationTransaction(req, res) {
    let info = new Info();
    await models.transaction.model.update({cancel_process_status: "AWAITING"}, {
        where: {id: req.body.transactionId, cancel_process_status: "NO"},
        returning: true,
    }).then(async (r) => {
        //In postgres, length of r = 2, see sequelize doc
        let updatedRows = r[1];
        //updated rows is array
        let chosenTransaction = updatedRows[0];
        delete chosenTransaction.dataValues.id;
        chosenTransaction.dataValues.parent_transaction_id = req.body.transactionId;
        chosenTransaction.dataValues.transaction_end_timestamp = moment().toDate();
        chosenTransaction.dataValues.created_at = moment().toDate();
        chosenTransaction.dataValues.transaction_start_timestamp = moment().toDate();
        chosenTransaction.dataValues.transaction_type = awaitingForCancellation;
        chosenTransaction.dataValues.is_cancelable = false;
        chosenTransaction.dataValues.user_id = req.user.id;
        let cancelRequestAsTransaction = await models.transaction.model.create(chosenTransaction.dataValues);
        await createCancelRequestActionNotification(req.user.business_id, req.body.transactionId, cancelRequestAsTransaction.id)
        }).catch(err => {
        info.errors.push(err)
    });
    res.customSend(info);
}
module.exports.createCancellationTransaction = createCancellationTransaction;
module.exports.awaitingForCancellation = awaitingForCancellation;