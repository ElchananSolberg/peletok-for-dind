import React, { Component } from 'react';
import { LanguageManager as LM } from '../LanguageManager/Language';
import './selectUser.css';
import { Container, Row, Col, Button } from 'reactstrap';
import { getSellerUsersData } from '../DataProvider/Functions/DataGeter'
import { UserDocuments } from './UserDocuments';
import { postDocument } from '../DataProvider/Functions/DataPoster'
import { UserFieldComponent } from './userFieldComponent';
import { UsersScreen } from './selectUser';
import { Notifications } from '../Notifications/Notifications'



/**
A component that manages user documents
 */

export class UserDocumentsManagement extends Component {
    constructor(props) {
        super(props);


        this.state = {
            UsersData: null,
            newDoc: false,
            docUpload: '',
            docType: '',
            docValidity: this.theDate(),
            providers: { options: LM.getString("docTypes") }
        }
        this.toggle = this.toggle.bind(this)
        this.docFun = this.docFun.bind(this)
        this.sendNewDoc = this.sendNewDoc.bind(this)
    }

    /**
    A function that preserves the documents and their definition
     */
    docFun(e) {
        if (e.target.id === 'InputdocUpload') {
            let reader = new FileReader();
            let file = e.target.files[0];
            reader.onloadend = () => {
                this.setState({ docUpload: file });
            }
            if (e.target.files[0]) {
                reader.readAsDataURL(e.target.files[0]);
            }
        }
        else if (e.target.id === 'InputdocValidity') { this.setState({ docValidity: e.target.value }) }
        else if (e.target.id === 'InputSelectdocType') { this.setState({ docType: e.target.value }) }
    }
    /**
     * A function that gives the current date in 00/00/0000 format
     */
    theDate() {
        let curDate;
        let dateNow = new Date();
        let yearNow = dateNow.getFullYear();
        let monthNow = dateNow.getMonth() + 1;
        let dayNow = dateNow.getDate();

        let stringMonth;
        if (monthNow.toString().length === 1) stringMonth = "0" + monthNow
        if (monthNow.toString().length === 2) stringMonth = monthNow
        let stringDay;
        if (dayNow.toString().length === 1) stringDay = "0" + dayNow
        else if (dayNow.toString().length === 2) stringDay = dayNow

        curDate = yearNow + "-" + stringMonth + "-" + stringDay;
        return curDate
    }
    /**
    * A function that manages the sending of the document and its settings
    */
    toggle() {
        if (this.state.newDoc) {
            if (this.state.docUpload != "" && this.state.docValidity != "" && this.state.docType != "") {
                this.sendNewDoc();
                this.setState({ docUpload: "", docValidity: this.theDate(), docType: "" }, () => {
                    this.setState({ newDoc: !this.state.newDoc })
                })

            }
            else {
                alert(LM.getString("enterAllFields"))
                return
            }
        } else {
            this.setState({ newDoc: !this.state.newDoc })

        }
    }
    sendNewDoc() {
        var newDoc = {}
        newDoc.document = this.state.docUpload;
        newDoc.type = this.state.docType;
        newDoc.expired_time = this.state.docValidity;
        postDocument(newDoc, this.props.id).then(
            function (response) {
                { Notifications.show(LM.getString("UserUpdatedSuccessfully"), "info", 5000) }
            }
        ).catch(
            function (error) {
                alert(error)
            }
        );

    }
    render() {

        return (
            <React.Fragment>
                {this.state.newDoc ? <React.Fragment>
                    <Row style={{ margin: '15px' }}>
                        <UserFieldComponent type="file" size={4} fieldTitle={LM.getString("docUpload")} id={`docUpload`} changeHandler={this.docFun} />
                        <UserFieldComponent select="true" providers={this.state.providers} type="text" size={4} fieldTitle={LM.getString("docType")} id={`docType`} changeHandler={this.docFun} />
                    </Row>
                    <Row style={{ margin: '15px' }}>
                        <UserFieldComponent type="date" value={this.state.docValidity} size={4} fieldTitle={LM.getString("docValidity")} id={`docValidity`} changeHandler={this.docFun} />

                        <Button className={'btn-lg font-weight-bold fs-17 px-4 regularButton border-radius-3'} onClick={this.toggle} id={`addDocButton${this.props.id}`}>{LM.getString("addDoc")}</Button>
                    </Row>
                </React.Fragment> :
                    <Button className={'btn-lg font-weight-bold fs-17 px-4 regularButton border-radius-3'} onClick={this.toggle} id={'addDocButton'}>{LM.getString("addDoc")}</Button>
                }

                <UserDocuments updateUsers={this.props.updateUsers} id={this.props.id} />

            </React.Fragment>

        )
    }
}