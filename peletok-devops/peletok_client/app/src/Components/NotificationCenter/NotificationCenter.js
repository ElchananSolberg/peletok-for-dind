import React, { Component } from 'react';
import { Button, Container, Row, Col } from 'reactstrap';

import './NotificationCenter.css';

import NotificationCenterTable from './NotificationCenterTable';

import { LanguageManager as LM } from "../LanguageManager/Language"
import {getUserMessages} from "../DataProvider/Functions/DataGeter";



/**
 * NotificationCenter is a main component of NotificationCenter Page.  NotificationCenter and its children are working with separate .css file: NotificationCenter.css.
 */
class NotificationCenter extends Component {
    constructor(props) {
        super(props);

        this.state = {
            /** Notification Center Headder  */
            notificationCenterHeadder: {
                headder: LM.getString('notificationCenter'),
                back: LM.getString('selectUserBack'),
            },
            /** button Show All data  */
            buttonShowAll: LM.getString('showAll'),
            /** button Show Unread data  */
            buttonShowUnread: LM.getString('unread'),
            /** button Show Favorites data  */
            buttonShowFavorites: LM.getString('favorites'),

            /** button Show Cancellations data  */
            buttonShowCancellations: LM.getString('cancellations'),
            /** button Show Notifications data  */
            buttonShowNotifications: LM.getString('notifications'),
            /** button Show Credits data  */
            buttonShowCredits: LM.getString('credits'),

            /** button Show Credits data  */
            buttonShowGiftOrders: LM.getString('giftOrdersNotification'),

            /** How much more notifications field data  */
            howMuchMoreNotificationsStart: LM.getString('unreadMessagesStart'),
            howMuchMoreNotificationsEnd: LM.getString('unreadMessagesEnd'),
            /** No more notifications field data  */
            noMoreNotifications: LM.getString('noMoreNotifications'),
            /** field data of two buttons of view message area of the NotificationCenterTableRow component, that gets this data from the NotificationCenterTable component   */
            viewMessageWindow: {
                replyButton: LM.getString('reply'),
                closeButton: LM.getString('close'),
            },
            /** field data of two buttons of type message area of the NotificationCenterTableRow component, that gets this data from the NotificationCenterTable component   */
            typeMessageWindow: {
                sendButton: LM.getString('send'),
                cancelButton: LM.getString('cancel'),
            },

            /** tableHeadersObj - an object for <NotificationCenterTable/> component. The values of the object are the HEADERS of the table */
            tableHeadersObj:
            {
                senderName: LM.getString('senderName'),
                subject: LM.getString('subject'),
                date: LM.getString('date'),
                time: LM.getString('time'),
                '': '',
            },
            /** tableRows - an array for <NotificationCenterTable/> component. The elements of this array are objects - each object has a data of each of a row of the table */
            tableRows: [],
            /** Notification Center Buttons working variable */
            tabValue: "All",
        }
    }
    componentDidMount() {
        let messagesToView = [];
        getUserMessages().then(allMessages=>{
            for(const message of allMessages){
                let splitMessage = message.message?message.message.split(" "):null;
                let subject = "";
                if (splitMessage) {
                    subject += splitMessage[0];
                    if (splitMessage.length > 1) subject += " " + splitMessage[1];
                }
                messagesToView.push({
                    senderName: message.sender_name,
                    subject: subject,
                    fullMessage: message.message,
                    date: message.timestamp_sending.slice(0, 10),
                    time: message.timestamp_sending.slice(11, 19),
                    images: {
                        favorite: message["business_tags"]?message["business_tags"]["favorite"]: false,
                        unread: message["business_tags"]?!(message["business_tags"]["read"]): true
                    },
                    senderId: message["from_business_id"],
                    messageId: message["id"],
                    credit: message["tags"]?message["tags"]["credit"]:false,
                    cancellation: message["tags"]?message["tags"]["cancellation"]:false,
                    giftOrder: message["tags"]?message["tags"]["giftOrder"]:false
                    },

                )
            }
            this.setState({"tableRows": messagesToView})
        })


    }

    /** Show All notifications Button click handler */
    showAllClick = (e) => {
        this.setState({ tabValue: "All" });
    }
    /** Show Unread notifications Button click handler */
    showUnreadClick = (e) => {
        this.setState({ tabValue: "Unread" });
    }
    showFavoritesClick = (e) => {
        this.setState({ tabValue: "Favorites" });
    }
    /** Show Cancellations Button click handler */
    showCancellationsClick = (e) => {
        this.setState({ tabValue: "Cancellations" });
    }
    /** Show Cancellations Button click handler */
    showNotificationsClick = (e) => {
        this.setState({ tabValue: "Notifications" });
    }
    /** Show Credits Button click handler */
    showCreditsClick = (e) => {
        this.setState({ tabValue: "Credits" });
    }
    /** Show Credits Button click handler */
    showGiftOrdersClick = (e) => {
        this.setState({ tabValue: "GiftOrders" });
    }

    deleteRow = (messageId) => {
            let tableRows = [...this.state.tableRows];
            for (let i = 0; i < tableRows.length; i++) {
                if (tableRows[i].messageId === messageId) {
                    tableRows.splice(i, 1);
                    break;
                }
            }
            this.setState({tableRows: tableRows})
    };

    updateMessageReadStatus= (messageId, readStatus)=>{
        let tableRows = [...this.state.tableRows];
        for (let i = 0; i < tableRows.length; i++) {
            if (tableRows[i].messageId === messageId) {
                tableRows[i].images.unread = !readStatus;
                break;
            }
        }
        this.setState({tableRows: tableRows})
    };

    updateMessageFavoriteStatus= (messageId, favoriteStatus)=>{
        let tableRows = [...this.state.tableRows];
        for (let i = 0; i < tableRows.length; i++) {
            if (tableRows[i].messageId === messageId) {
                tableRows[i].images.favorite = favoriteStatus;
                break;
            }
        }
        this.setState({tableRows: tableRows})
    };

    render() {
        let howMuchMoreNotificationsCounter = 0;
        this.state.tableRows.forEach((current) => { if (current.images.unread) howMuchMoreNotificationsCounter++ });
        return (
            <div className="notificationCenterContainer">
                <div className="d-flex justify-content-between" >
                    <h3 className="color-page-header font-weight-light fs-26">{this.state.notificationCenterHeadder.headder}</h3>
                </div>

                <Container className="notificationCenterSettings ">
                    <Row className="justify-content-between">
                        <Row className="justify-content-start mx-0" >
                            <Col sm='auto' className={(LM.getDirection() === "rtl" ? 'pr-0' : 'pl-0')}>
                                <Button className={"notificationCenterButton " + (this.state.tabValue === "All" ? "notificationCenterButtonChecked" : "notificationCenterButtonUnChecked")}
                                    id="notificationCenterButtonShowAll"
                                    onClick={this.showAllClick} >
                                    {this.state.buttonShowAll}
                                </Button>
                            </Col>
                            <Col sm='auto'>
                                <Button className={"notificationCenterButton " + (this.state.tabValue === "Unread" ? "notificationCenterButtonChecked" : "notificationCenterButtonUnChecked")}
                                    id="notificationCenterButtonShowUnread"
                                    onClick={this.showUnreadClick} >
                                    {this.state.buttonShowUnread}
                                </Button>
                            </Col>
                            <Col sm='auto'>
                                <Button className={"notificationCenterButton " + (this.state.tabValue === "Favorites" ? "notificationCenterButtonChecked" : "notificationCenterButtonUnChecked")}
                                    id="notificationCenterButtonShowFavorites"
                                    onClick={this.showFavoritesClick}>
                                    {this.state.buttonShowFavorites}
                                </Button>
                            </Col>
                            <Col sm='auto'>
                                <Button className={"notificationCenterButton " + (this.state.tabValue === "Cancellations" ? "notificationCenterButtonChecked" : "notificationCenterButtonUnChecked")}
                                    id="notificationCenterButtonShowCancellations"
                                    onClick={this.showCancellationsClick}>
                                    {this.state.buttonShowCancellations}
                                </Button>
                            </Col>
                            <Col sm='auto'>
                                <Button className={"notificationCenterButton " + (this.state.tabValue === "Notifications" ? "notificationCenterButtonChecked" : "notificationCenterButtonUnChecked")}
                                    id="notificationCenterButtonShowNotifications"
                                    onClick={this.showNotificationsClick}>
                                    {this.state.buttonShowNotifications}
                                </Button>
                            </Col>
                            <Col sm='auto'>
                                <Button className={"notificationCenterButton " + (this.state.tabValue === "Credits" ? "notificationCenterButtonChecked" : "notificationCenterButtonUnChecked")}
                                    id="notificationCenterButtonShowCredits"
                                    onClick={this.showCreditsClick}>
                                    {this.state.buttonShowCredits}
                                </Button>
                            </Col>
                            <Col sm='auto'>
                                <Button className={"notificationCenterButton " + (this.state.tabValue === "GiftOrders" ? "notificationCenterButtonChecked" : "notificationCenterButtonUnChecked")}
                                    id="notificationCenterButtonShowGiftOrders"
                                    onClick={this.showGiftOrdersClick}>
                                    {this.state.buttonShowGiftOrders}
                                </Button>
                            </Col>
                        </Row>
                        <Col sm='auto' style={{ padding: "0px", fontSize: "12px" }} className="notificationCenterWeakText">
                            {this.state.howMuchMoreNotificationsStart + howMuchMoreNotificationsCounter + this.state.howMuchMoreNotificationsEnd}
                        </Col>
                    </Row>
                    <Row>
                        <NotificationCenterTable tableHeadersObj={this.state.tableHeadersObj} tableRows={this.state.tableRows}
                            viewMessageWindow={this.state.viewMessageWindow} typeMessageWindow={this.state.typeMessageWindow}
                            tabValue={this.state.tabValue} deleteRow={this.deleteRow} updateMessageReadStatus={this.updateMessageReadStatus} updateMessageFavoriteStatus={this.updateMessageFavoriteStatus}
                        />
                    </Row>
                    <Row >
                        <Col className="notificationCenterNoMoreNotifications notificationCenterWeakText" >
                            -- {this.state.noMoreNotifications} --
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}
export default NotificationCenter;