const BaseModel = require('../../BaseModel');
const {createMessageBusiness} = require("../message_utils");
const DataTypes = require("sequelize").DataTypes;
const {Op} = require('sequelize');

/**
 * TagOfBusinessMessage model
 */
class TagOfBusinessMessage extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            tag_name: {
                type: DataTypes.STRING(128),
                allowNull: true
            },
            order: {
                type: DataTypes.STRING,
                allowNull: true
            },
            default_value: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
                allowNull: false
            }
        };
        this.prefix = "MSG";
        this.author = true;
    }
    /**
     * creates associate for TagOfBusinessMessage model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.hasMany(models.messageBusinessTag.model, { as: 'MessageBusinessTag' });
    }
}

let tagOfBusinessMessage = new TagOfBusinessMessage();

tagOfBusinessMessage.model.afterCreate(async (tag, options) => {
    const models = require("../../index");
    const messageModel = models.message.model;
    const messageBusinessModel = models.messageBusiness.model;
    const messageBusinessTagModel = models.messageBusinessTag.model;
    // const businessModel = models.business.model;
    // const messages = await messageModel.findAll({where: {to_business_id: {[Op.ne]: null}}});
    let messagesBusinessTagsToCreate = [];
    let allMessageBusiness = await messageBusinessModel.findAll();
    for (const mb of allMessageBusiness) {
        messagesBusinessTagsToCreate.push({
            tag_of_message_id: tag.id,
            mark: tag.default_value,
            message_business_id: mb
        });
    }
    //     if (m.business_id) {
    //         messagesBusinessTagsToCreate.push({
    //             ...tagsData,
    //             business_id: m.to_business_id
    //         })
    //     } else {
    //         let allBusiness = await businessModel.findAll({attributes: ["id"]});
    //         for (const b of allBusiness) {
    //             messagesBusinessTagsToCreate.push({
    //                 ...tagsData,
    //                 business_id: b.id
    //             })
    //         }
    //     }
    // }
    await messageBusinessTagModel.bulkCreate(messagesBusinessTagsToCreate);
});

module.exports = tagOfBusinessMessage;