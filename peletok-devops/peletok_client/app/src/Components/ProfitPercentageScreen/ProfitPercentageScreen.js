import React, { Component } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import './ProfitPercentageScreen.css';
import InputTypeRadio from '../InputUtils/InputTypeRadio';
import InputTypeSelect from '../InputUtils/InputTypeSelect';
import InputTypeNumber from '../InputUtils/InputTypeNumber';
import Slider from '../InputUtils/Slider';
import { LanguageManager as LM } from '../LanguageManager/Language'
import {
    getSuppliers,
    getProducts,
    putUpdateProductAuthorization,
    getProductsSupplierMax
} from '../DataProvider/DataProvider'
import InputTypeSimpleSelect from '../InputUtils/InputTypeSimpleSelect';
import { Notifications } from '../Notifications/Notifications'
import { Loader } from '../Loader/Loader'


export class ProfitPercentageScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {    
            edited: false,
            renderFields: true,
            suppliers: [],
            currentSupplierID: -1,
            currentSupplierProducts: [],
            selectedProductId: null,
            selectedServiceId: '0',
            selectedBusinessType: 0,
            maxProfit: null,
        }
        this.fields = {}
        
    }

    

    loadSuppliersList(forceUpdate = false) {
        getSuppliers(forceUpdate).then(
            (res) => {
                this.setState({ suppliers: Object.keys(res).filter(k => this.isProfit() ? k != 'bills' : k == 'bills').reduce((newArr, key) => [...newArr, ...res[key]], []) })
            }
        ).catch(
            (err) => {
                alert("loadSuppliersList failed: " + err)

            }
        )
    }

    productChange = (e, shouldSelectNewProduct = false) => {
        
        if (e.value) {
            this.setState({
                selectedProductId: e.value
            })
        }
        getProductsSupplierMax(e.value).then((res)=>{
            this.setState({
                maxProfit: res.supplierProfitPrecentage,
                minFinalCommission: res.minFinalCommission
            })
        })

    }

    supplierChange = (e, newProductId = false) => {
        this.setState({ currentSupplierID: e.value }, () => {
            getProducts(this.state.currentSupplierID, {}, true).then(
                (res) => {
                    this.setState({
                        currentSupplierProducts: {
                            manual: res.manual ? res.manual.map(p => p.name ? p : Object.assign(p, { name: p.name_he })) : [],
                            virtual: res.virtual ? res.virtual.map(p => p.name ? p : Object.assign(p, { name: p.name_he })) : [],
                            bills_payment: res.bills_payment ? res.bills_payment.map(p => p.name ? p : Object.assign(p, { name: p.name_he })) : [],
                        }
                    })
                    

                }).catch(
                    (err) => {
                        Notifications.show(err);
                    }
                )
        })
    }

    serviceChange = (e) => {
        let activeId = e.target.id.split("-")[1]
        this.setState({ selectedServiceId: activeId });
        this.selectProduct.setValue('')
    }

    businessTypeChanged = (e) => {
        let activeId = e.target.id.split("-")[1]
        const options = ['all', 'distributor', 'seller']
        this.setState({ selectedBusinessType: options[activeId] });
    }

    productsLoad = () => {
        if (Object.keys(this.state.currentSupplierProducts).length !== 0) {
            let virtualProduct = this.state.currentSupplierProducts.virtual ? this.state.currentSupplierProducts.virtual.map(p => {
                return { label: p.name, value: p.id, id: p.id, type: p.type }
            }) : [];
            let manualProduct = this.state.currentSupplierProducts.manual ? this.state.currentSupplierProducts.manual.map(p => {
                return { label: p.name, value: p.id, id: p.id, type: p.type }
            }) : [];
            let billsPayment = this.state.currentSupplierProducts.bills_payment ? this.state.currentSupplierProducts.bills_payment.map(p => {
                return { label: p.name, value: p.id, id: p.id, type: p.type }
            }) : [];
            if (this.state.selectedServiceId === '0') {
                return virtualProduct.concat(manualProduct, billsPayment);
            }
            if (this.state.selectedServiceId === '2') {
                return manualProduct;
            }
            if (this.state.selectedServiceId === '1') {
                return virtualProduct;
            }
            if (this.state.selectedServiceId === '3') {
                return billsPayment
            }
        }
        return false
    }

    getSendHandler = (field, value) => {
        return () => {
            let fieldValid = true;
            let productValid = this.selectProduct.runValidation()
            if(this.fields[field]){
                fieldValid = this.fields[field].runValidation()
            }
            
            if(productValid && fieldValid){ 
                let data = {
                    [field]: value == undefined ? this.fields[field].getValue().value : value,
                    businessType: this.state.selectedBusinessType
                }
                Loader.show()
                putUpdateProductAuthorization(this.state.selectedProductId, data).then((res)=>{
                    this.setState({
                        renderFields: false,
                        selectedBusinessType: 0,
                    },()=>{
                        Loader.hide()
                        this.setState({
                            renderFields: true
                        }, ()=>{
                            this.loadSuppliersList();
                        })
                    })
                })
            }else{
                Notifications.show(LM.getString('completeFields'), 'danger')
            }
        }
    }

    isProfit = () => {
        return this.props.match.params.profitModel == 'porfit'
    }

    componentDidMount = async () => {
        
        this.loadSuppliersList();
    
    }
    
    componentDidUpdate(prevProps) {
        if (this.props.match.params.profitModel !== prevProps.match.params.profitModel) {
            this.onRouteChanged();
        }
    }

    onRouteChanged() {
         this.setState({
            renderFields: false,
            selectedBusinessType: 0,
        },()=>{
            Loader.hide()
            this.setState({
                renderFields: true
            }, ()=>{
                this.loadSuppliersList();
            })
        })
    }


    render() {
        let header = this.isProfit() ? LM.getString("profitPercentages") : LM.getString("commissions");

        return (

            <Container className='profitPercentageScreenContainer'>
                <Row >
                    <Col className="profitPercentageScreenHeader">
                        {header}
                    </Col>
                </Row>
                {this.state.renderFields && <Container className="profitPercentageScreenSettings">
                     <Row className="align-items-end">
                        <Col sm='4'>
                            <InputTypeSimpleSelect
                                title={LM.getString("selectSupplier")}
                                id="CreateProductSelectSupplier"
                                required={this.state.creatingNewState}
                                options={this.state.suppliers.map(supplier => {
                                    return { label: supplier.name, value: supplier.id }
                                })} onChange={this.supplierChange}
                                ref={(componentObj) => {
                                    this.selectSupplier = componentObj
                                }}
                            />
                        </Col>
                        <Col sm='4'>
                                <InputTypeSimpleSelect
                                    required={true}
                                    title={LM.getString('selectProductFromList')}
                                    id="CreateProductSelectProduct"
                                    options={this.productsLoad() || ['']}
                                    onChange={this.productChange}
                                    ref={(componentObj) => {
                                        this.selectProduct = componentObj
                                    }}
                                />
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <InputTypeRadio
                                default={LM.getString('productsRadioButtonsRow')[0]}
                                id="CreateProductSelectService-"
                                options={LM.getString('productsRadioButtonsRow').slice(0, 3)}
                                onChange={this.serviceChange}
                            />
                        </Col>
                    </Row>
                   {this.isProfit() ? <Row>
                        <Col sm='4'>
                            <InputTypeNumber
                                max={this.state.maxProfit || 0} 
                                required={true}
                                id='percentGainNumberInput'
                                title={LM.getString("PercentGainInput")}
                                ref={(obj=>this.fields.percentage_profit=obj)}
                                onChange={()=>{this.fields.percentage_profit.runValidation()}}
                            />
                        </Col>
                        <Col sm='4'>
                            <InputTypeNumber
                                required={true} 
                                id='earningPointsNumberInput'
                                title={LM.getString("earningPoints")}
                                ref={(obj=>this.fields.points=obj)}
                            />
                        </Col>
                    </Row> :

                     <Row>
                        <Col sm='4'>
                            <InputTypeNumber
                                max={this.state.maxProfit || 0} 
                                required={true}
                                id='commissionsInput'
                                title={LM.getString("commission")}
                                ref={(obj=>this.fields.business_commission=obj)}
                                onChange={()=>{this.fields.business_commission.runValidation()}}
                            />
                        </Col>
                        <Col sm='4'>
                            <InputTypeNumber
                                required={true}
                                min={this.state.minFinalCommission || 0} 
                                id='finalCommissionInput'
                                title={LM.getString("finalCommission")}
                                ref={(obj=>this.fields.final_commission=obj)}
                                onChange={()=>{this.fields.final_commission.runValidation()}}
                            />
                        </Col>
                    </Row>}

                    <Row>
                        <Col className='mt-2' >
                            <InputTypeRadio 
                                id='clientRadioButtonsInput-'
                                title={LM.getString("clientType")}
                                options={LM.getString("clientTypeOptions")}
                                onChange={this.businessTypeChanged}
                            />
                        </Col>
                    </Row>
                    {/*<Row>
                        <Col sm='8'>
                            <Slider
                                title={LM.getString("costRange")}
                                id='costRange'
                                defaultValue='0'
                                min='0'
                                max='999'
                            />
                        </Col>
                    </Row>*/}
                    {this.isProfit() ? <>
                        <Row>
                            <Col sm='auto' >
                                <Button
                                    id='startPercentProfitButton'
                                    className='profitPercentageScreenBtn'
                                    onClick={this.getSendHandler('percentage_profit')}
                                >
                                    {LM.getString("spreadPercentProfit")}
                                </Button>
                            </Col>
                        </Row>
                        <Row>
                            <Col sm='auto' >
                                <Button
                                    id='spreadPointsButton'
                                    className='profitPercentageScreenBtn'
                                    onClick={this.getSendHandler('points')}
                                >
                                    {LM.getString("spreadPoints")}
                                </Button>
                            </Col>
                        </Row>
                    </>:
                    <>
                        <Row>
                            <Col sm='auto' >
                                <Button
                                    id='startPercentProfitButton'
                                    className='profitPercentageScreenBtn'
                                    onClick={this.getSendHandler('business_commission')}
                                >
                                    {LM.getString('spreadCommissionOnAllSellers')}
                                </Button>
                            </Col>
                        </Row>
                        <Row>
                            <Col sm='auto' >
                                <Button
                                    id='spreadPointsButton'
                                    className='profitPercentageScreenBtn'
                                    onClick={this.getSendHandler('final_commission')}
                                >
                                    {LM.getString('spreadFinalCommissionOnAllSellers')}
                                </Button>
                            </Col>
                        </Row>
                    </>}
                    <Row>
                        <Col sm='auto' >
                            <Button
                                id='spreadPermissionButton'
                                className='profitPercentageScreenBtn'
                                onClick={this.getSendHandler('is_authorized', true)}
                            >
                                {LM.getString("spreadPermission")}
                            </Button>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm='auto' >
                            <Button
                                id='removePermissionButton'
                                className='profitPercentageScreenBtn'
                                onClick={this.getSendHandler('is_authorized', false)} 
                            >{LM.getString("removePermission")}
                            </Button>
                        </Col>
                    </Row>
                </Container>}
            </Container>
        )
    }
}

