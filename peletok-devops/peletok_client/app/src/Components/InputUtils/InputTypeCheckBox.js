import React, { Component } from 'react';
import './InputUtils.css';
import { Row, Col, CustomInput } from 'reactstrap';
import { Tooltip } from './Tooltip';
import { LanguageManager as LM } from "../LanguageManager/Language";
import Asterisk from '../Ui/Asterisk';


/**
 * InputTypeCheckBox component lets to render a checkbox button.
 * it can render the title above the checkbox button.
 * it can render the tooltip to the side of the title.
 * It validates the value is valid to the function sent in  this.props.validationFunction;
 * if there are any validation issues, the div that contains rendered options will change a border color to red;
 * if there are any validation issues bellow the div will be the error message
 * It can add outer onChange function to checkbox onChange.
 * It can change a view to disabled view due to this.props.disabled.
 */
class InputTypeCheckBox extends Component {

    constructor(props) {
        super(props);

        this.state = {
            checked: this.props.checked,
            validationFuncs: [
                {
                    /**  Validates the value is valid to the function sent in  this.props.validationFunction */
                    f: this.props.validationFunction || (() => (true)),
                    msg: this.props.notValidMessage,
                },
            ],
            errorMessage: '',
        }
    }

    /** can add outer onChange function to checkbox onChange. */
    InputTypeCheckBoxChange = (e) => {
        e.persist();
        // We change the target checked attribute for future use.
        !this.state.checked ? e.target.setAttribute("checked", "checked") :
            e.target.removeAttribute("checked");

        this.setState({ checked: !this.state.checked },
            () => {
                if (this.props.onChange) {
                    this.props.onChange(e);
                }
            }
        );
    }

    /** summarizing validation function
    * is running 2 times: when input is loosing focus and when getValue function is running (by programmer-user)
    */
    runValidation = () => {
        let summarizingValidationResult = (!this.state.checked) || this.state.validationFuncs.every((validation) => validation.f(this.state.checked));
        if (!summarizingValidationResult) {
            let messages = [];
            this.state.validationFuncs.forEach((validation) => {
                if (!validation.f(this.state.checked)) {
                    messages.push(validation.msg);
                }
            })
            this.setState(({ errorMessage: messages.join(`\n`) }));
        } else {
            this.setState(({ errorMessage: '' }));
        }
        return summarizingValidationResult;
    }

    getValue = () => {
        let summarizingValidationResult = this.runValidation();
        return { valid: summarizingValidationResult, value: this.state.checked };
    }

    setValue = (newValue) => {
        if (typeof newValue === 'boolean') {
            this.setState({ checked: newValue });
            return true;
        }
        return false;
    }



    render() {
        let disabled = this.props.disabled;
        let id = this.props.id + "".replace(/\s+/g, '')
        let title = this.props.title;
        let tooltipText = this.props.tooltip;
        let lableText = this.props.lableText;
        return (
            <>
                {title ?
                    <Row>
                        <Col className={"d-flex justify-content-between line-height-1 " + (title ? 'py-1' : '')}>
                            {title ?
                                <div className="fs-17 white-space">
                                    {title}
                                    <Asterisk show={this.props.required} />
                                </div>
                                : <><Asterisk show={this.props.required} /></>
                            }
                            {tooltipText ?
                                <Tooltip tooltipText={tooltipText} id={id} />
                                : <></>
                            }
                        </Col>
                    </Row>
                    : <></>}

                {/*<Row>*/}
                <div className={'d-flex'}>

                    <CustomInput disabled={disabled} type="checkbox" checked={this.state.checked} value={lableText}
                        id={`InputTypeCheckBox${id}`}
                        label={lableText}
                        onChange={this.InputTypeCheckBoxChange}
                        onBlur={this.runValidation}

                        className={(LM.getDirection() === "rtl" ? "checkbox-rtl " : "") + (this.state.errorMessage ? "invalidDiv" : "")}
                    />
                    <div className='ws-pre errorMessageDiv'>{this.state.errorMessage}</div>
                </div>
                {/*</Row>*/}
            </>

        );
    }
}

export default InputTypeCheckBox;