from selenium.webdriver.common.by import By


class MainSuppliersAndProductPage:
    suppliers_and_products_link = (By.ID, 'suppliersLink')
    suppliers_and_products_tabs = (By.XPATH, '//div[@class="temp_background main-body pt-5"] '
                                             '//a[@href="/suppliers/suppliersAndProducts/{}"]')
    any_pop_up_alert = (By.CSS_SELECTOR, 'div.myAlert.alert.alert-success.fade.show')
    suppliers_and_products_head_line = (By.CLASS_NAME, 'suppliersHeader')
    choose_options_from_input_by_name = (By.XPATH, '//div[@class="css-11unzgr"]//div[text()="{}"]')
