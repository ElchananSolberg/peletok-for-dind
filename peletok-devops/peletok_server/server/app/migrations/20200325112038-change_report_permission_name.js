'use strict';
const {addPermission, renamePermission} = require('../dbUtils/addPermission');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      renamePermission(queryInterface, '/reports/reports/transactions', '/reports/reports/11'),
      renamePermission(queryInterface, '/reports/reports/payments', '/reports/reports/12'),
      renamePermission(queryInterface, '/reports/reports/actions', '/reports/reports/13'),
      renamePermission(queryInterface, '/reports/reports/actionsAndPayments', '/reports/reports/14'),
      renamePermission(queryInterface, '/reports/reports/distributionActivities', '/reports/reports/15'),
    ])
  },

  down: (queryInterface, Sequelize) => {
   return Promise.resolve()
  }
};
