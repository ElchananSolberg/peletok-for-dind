const BaseModel = require('../../BaseModel');

/**
 * ProductDocument model
 */
class ProductDocument extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.prefix = 'DOC';
    }
    /**
     * creates associate for ProductDocument model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.product.model, { as: 'Product' });
        this.model.belongsTo(models.document.model, { as: 'Document' });
    }

}
// exporting instance
module.exports = new ProductDocument();