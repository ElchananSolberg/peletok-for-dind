from selenium.webdriver.common.by import By


class HomePage:

    cellcom_icon = (By.ID, "id94Link")
    pelephone_icon = (By.ID, "id95Link")
    bezek_benleoumi_014 = (By.ID, "id205Link")
    kvish_6 = (By.ID, "id187Link")
    carmel_tunnels_icon = (By.ID, "id192Link")
    jawwal_icon = (By.ID, "id210Link")
    text_voice_icon = (By.ID, "id196Link")
    freePhone_icon = (By.ID, "id202Link")
    benleumiHomeCard_icon = (By.ID, "id211Link")
    benleumi_icon = (By.ID, "id124Link")
    zero_one_two_mobile_icon = (By.ID, 'id201Link')
    partner_icon = (By.ID, "id127Link")
    tehinatChashmal_icon = (By.ID, "id186Link")
    globalSim_icon = (By.ID, "id203Link")
    bezaqonline_icon = (By.ID, 'id194Link')
    wataniya_icon = (By.ID, "id209Link")
    meshavkim_page = (By.ID, "resellersLink")
    account_status_btn = (By.XPATH, '//div[@class="temp_background main-body pt-5"]//*[text()="מצב חשבון"]')
    kvish6_payment_report_btn = (By.XPATH, '//div[@class="temp_background main-body pt-5"]'
                                           '//span[contains(text(),"לכביש")]')
    bezeq_payment_report_btn = (By.XPATH, '//div[@class="temp_background main-body pt-5"]'
                                          '//span[contains(text(),"לבזק"]')
    configuration_Link = (By.ID, 'configLink')
    user_btn = (By.ID, "customerBtn")
    user_area = (By.ID, "userAreaDrop")
    exit_adding = (By.CSS_SELECTOR, '#root>div>div>div:nth-child(1)>h5')