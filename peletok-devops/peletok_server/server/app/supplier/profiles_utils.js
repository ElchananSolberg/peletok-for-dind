const models = require('../index')
const moment = require('moment')
const { Info } = require("../mapping/crudMiddleware");
let billsPaymentSuppliersType = 2
let prePaidSuppliersType = 1
/**
 * updates the chosen products from req according to supplier info
 * @param {*} req 
 * @param {*} res 
 */
async function updateProfileBusinessProduct(req, res) {
    let info = new Info()
    let whereObj = {}
    if (req.body.type == 'profit') {
        whereObj.type = prePaidSuppliersType
    } else if (req.body.type == 'commission') {
        whereObj.type = billsPaymentSuppliersType
    }
    if (req.body.supplier_id) {
        whereObj.id = req.body.supplier_id
    }
    let suppliers = await models.supplier.model.findAll({
        where: whereObj,
        include: [
            {
                model: models.product.model,
                as: 'Product',
                include: [
                    {
                        model: models.businessProduct.model,
                        as: 'BusinessProduct',
                        where: { business_id: req.body.profile_id }
                    },

                ]
            }
        ]
    })
    for (let i = 0; i < suppliers.length; i++) {
        const supplier = suppliers[i];
        for (let index = 0; index < supplier.Product.length; index++) {
            const product = supplier.Product[index];
            for (let j = 0; j < product.BusinessProduct.length; j++) {
                const businessProduct = product.BusinessProduct[j];
                if (req.body.business_commission) {
                    let business_commission = req.body.business_commission
                    if (req.body.business_commission > supplier.max_percentage_profit) {
                        business_commission = supplier.max_percentage_profit
                    }
                    await models.businessProduct.model.update({ timestamp_start: moment().toDate(), author_id: req.user.id, status_author_id: req.user.id, business_commission: business_commission }, { where: { id: businessProduct.id } }).catch(err => {
                        info.errors.push(err)
                    })
                }
                if (req.body.final_commission) {
                    let final_commission = req.body.final_commission
                    if (req.body.final_commission < supplier.min_percentage_profit) {
                        final_commission = supplier.min_percentage_profit
                    }
                    await models.businessProduct.model.update({ timestamp_start: moment().toDate(), author_id: req.user.id, status_author_id: req.user.id, final_commission: final_commission }, { where: { id: businessProduct.id } }).catch(err => {
                        info.errors.push(err)
                    })
                }
                if (req.body.percentage_profit) {
                    let percentage_profit = req.body.percentage_profit
                    if (req.body.percentage_profit > supplier.max_percentage_profit) {
                        percentage_profit = supplier.max_percentage_profit
                    }
                    await models.businessProduct.model.update({ timestamp_start: moment().toDate(), author_id: req.user.id, status_author_id: req.user.id, percentage_profit: percentage_profit }, { where: { id: businessProduct.id } }).catch(err => {
                        info.errors.push(err)
                    })
                }
                if (req.body.points) {
                    await models.businessProduct.model.update({ timestamp_start: moment().toDate(), author_id: req.user.id, status_author_id: req.user.id, points: req.body.points }, { where: { id: businessProduct.id } }).catch(err => {
                        info.errors.push(err)
                    })
                }
            }

        }

    }
    res.customSend(info)

}

module.exports.updateProfileBusinessProduct = updateProfileBusinessProduct
