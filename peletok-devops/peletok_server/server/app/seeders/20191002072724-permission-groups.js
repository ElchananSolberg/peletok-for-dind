'use strict';
const moment = require('moment');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('PRM_PermissionGroup', [
    {
        id: 1,
        name: 'ראשי',
        created_at: moment().toDate(),
        updated_at: moment().toDate(),
    },
     {
        id: 2,
        name: 'מתנות',
        created_at: moment().toDate(),
        updated_at: moment().toDate(),
    },
    {
        id: 3,
        name: 'ספקים ומוצרים',
        created_at: moment().toDate(),
        updated_at: moment().toDate(),
    },
     {
        id: 4,
        name: 'משווקים',
        created_at: moment().toDate(),
        updated_at: moment().toDate(),
    },
    {
        id: 5,
        name: 'הגדרות',
        created_at: moment().toDate(),
        updated_at: moment().toDate(),
    },
    {
        id: 6,
        name: 'ביטולים',
        created_at: moment().toDate(),
        updated_at: moment().toDate(),
    },
    {
        id: 7,
        name: 'דוחות מנהל',
        created_at: moment().toDate(),
        updated_at: moment().toDate(),
    },
    {
        id: 8,
        name: 'חשבשבת',
        created_at: moment().toDate(),
        updated_at: moment().toDate(),
    },
    {
        id: 9,
        name: 'ניתוח מידע',
        created_at: moment().toDate(),
        updated_at: moment().toDate(),
    },
    {
        id: 10,
        name: 'שונות',
        created_at: moment().toDate(),
        updated_at: moment().toDate(),
    },
    //*************************Api groups******************************/
    {
        id: 11,
        name: 'משתמש Api',
        created_at: moment().toDate(),
        updated_at: moment().toDate(),
    },
    {
        id: 12,
        name: ' Api עסק',
        created_at: moment().toDate(),
        updated_at: moment().toDate(),
    },
    {
        id: 13,
        name: 'מוצר Api',
        created_at: moment().toDate(),
        updated_at: moment().toDate(),
    },
    {
        id: 14,
        name: 'ספק Api',
        created_at: moment().toDate(),
        updated_at: moment().toDate(),
    },
    {
        id: 15,
        name: 'מתנות Api',
        created_at: moment().toDate(),
        updated_at: moment().toDate(),
    },
    {
        id: 16,
        name: 'כרטיס אשראי Api',
        created_at: moment().toDate(),
        updated_at: moment().toDate(),
    },
    {
        id: 17,
        name: 'טרנזקציות Api',
        created_at: moment().toDate(),
        updated_at: moment().toDate(),
    },
    {
        id: 18,
        name: 'באנרים Api',
        created_at: moment().toDate(),
        updated_at: moment().toDate(),
    },
    {
        id: 19,
        name: 'הודעות Api',
        created_at: moment().toDate(),
        updated_at: moment().toDate(),
    },
    {
        id: 20,
        name: 'דוחות Api',
        created_at: moment().toDate(),
        updated_at: moment().toDate(),
    },
    {
        id: 21,
        name: 'חשבשבת Api',
        created_at: moment().toDate(),
        updated_at: moment().toDate(),
    },
    {
        id: 22,
        name: 'תשלומים Api',
        created_at: moment().toDate(),
        updated_at: moment().toDate(),
    },
    {
        id: 23,
        name: 'מפיץ Api',
        created_at: moment().toDate(),
        updated_at: moment().toDate(),
    },
    {
        id: 24,
        name: 'תגיות Api',
        created_at: moment().toDate(),
        updated_at: moment().toDate(),
    },
    {
        id: 25,
        name: 'מנהל Api',
        created_at: moment().toDate(),
        updated_at: moment().toDate(),
    },
    {
        id: 26,
        name: 'הרשאות Api',
        created_at: moment().toDate(),
        updated_at: moment().toDate(),
    },
    {
        id: 27,
        name: 'api הרשאות נוספות',
        created_at: moment().toDate(),
        updated_at: moment().toDate(),
    },
    {
        id: 28,
        name: 'api קונפיגורציה',
        created_at: moment().toDate(),
        updated_at: moment().toDate(),
    },
    {
        id: 29,
        name: 'api ניהול משתמשים',
        created_at: moment().toDate(),
        updated_at: moment().toDate(),
    },
  ], {});
 
  },

  down: (queryInterface, Sequelize) => {
   return queryInterface.bulkDelete('PRM_PermissionGroup', null, {});
  }
};
