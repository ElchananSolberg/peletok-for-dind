import React, { Component } from 'react';
import { ElectricityCharge } from '../Bills/ElectricityCharge';
import { ChargingElectricity } from '../Bills/ChargingElectricity';
import { RoutesCharge } from './RoutesCharge';
import { LanguageManager as LM } from '../../LanguageManager/Language';
import { getSuppliers } from '../../DataProvider/DataProvider';
import { SERVER_BASE } from '../../DataProvider/DataProvider';
import Image from '../../../Assets/images/billNums2.png'
import { backToPreviousScreenHandler } from '../../../utils/BackToPreviousScreenHandler'
import { CityRenderer } from './CityRenderer';
import { RavKavCharge } from './RavKavCharge';
import { BezeqOnline } from './BezeqOnline';

let routeSixId = '187';
let electricityId = '185';
let carmelTunnelsId = '192';
let ChargingElectric = '186';
let CityPaymentsId = '193';
let RavKavId = '999';
let BezeqOnlineId = '194'


export class BillsRenderer extends Component {

    constructor(props) {
        super(props)

        this.state = {
            bills: []
        }
    }

    componentDidMount() {
        getSuppliers().then(
            (res) => {
                this.setState({ bills: res.bills.find((obj => obj.id == this.props.match.params.supplierId)) });
            }
        ).catch(
            (err) => {
                alert(err);
            }
        )
    }
    render() {
        let billView = <React.Fragment />;
        switch (this.props.match.params.supplierId) {

            case routeSixId:
                billView = <RoutesCharge supplierId={this.props.match.params.supplierId} header={LM.getString("routeSixCharge")} image={`${SERVER_BASE}/${this.state.bills.image}`} />
                break;
            case electricityId:
                billView = <ElectricityCharge supplierId={this.props.match.params.supplierId} header={LM.getString("electricCompanyCharge")} image={Image} />
                break;
            case ChargingElectric:
                billView = <ChargingElectricity supplierId={this.props.match.params.supplierId} header={LM.getString("electricCompanyCharge")} image={Image} />
                break;
            case carmelTunnelsId:
                billView = <RoutesCharge supplierId={this.props.match.params.supplierId} header={LM.getString("CarmelTunnelsCharge")} image={`${SERVER_BASE}/${this.state.bills.image}`} />
                break;
            case CityPaymentsId:
                billView = <CityRenderer match={this.props.match} />
                break;
            case RavKavId:
                billView = <RavKavCharge match={this.props.match} />
                break;
            case BezeqOnlineId:
                billView = <BezeqOnline supplierId={this.props.match.params.supplierId} image={`${SERVER_BASE}/${this.state.bills.image}`}/>
        }
        return (
            <React.Fragment>
                {backToPreviousScreenHandler(this.props.history)}
                {billView}
            </React.Fragment>
        )
    }
}