const DataTypes = require('sequelize').DataTypes;
const BaseModel = require('../../BaseModel');

/**
 * ProductCategory model
 */
class ProductCategory extends BaseModel {

    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            upscale_product_id: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
        };
        this.prefix = 'SUPL';
    }

    /**
     * creates associate for ProductCategory model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {

        this.model.belongsTo(models.category.model, { as: 'Category' });
        this.model.belongsTo(models.product.model, { as: 'Product' });


    }

}

module.exports = new ProductCategory();
