from __future__ import unicode_literals
from datetime import datetime
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import BasePermission
from rest_framework.decorators import api_view, permission_classes
from selenium.common.exceptions import TimeoutException
from utils.elements_selectors import RoadSix
from utils.main_functions import MainFunctions


@api_view(['POST'])
@permission_classes([BasePermission])
def post_pay(request):
    """
    {
    # "uri": "https://mybill.kvish6.co.il/FastPaymentLogin.do?Button_desktop",
    "uri": "https://mybill.carmeltunnels.co.il/FastPaymentLogin.do",
    "payment_info": {
        "id_number": "26385773",
        "bill": "230562",
        "card_num": "4580110705790910",
        "card_cvv": "969",
        "card_valid_month": "06",
        "card_valid_year": "2023",
        "personalId": "012163234",
        "name": "peletok",
        "price": "14.04"
        }
    }
    :param request:
    :return {Additional invoices: <list>, price: <str> status: <str> 'success' / 'error',
    confirmation: <str> || error_message: <str>}
    """
    handler = SixRoad(str(request.data.get("uri")), request.data.get("payment_info"))
    res = handler.pay()
    return Response(res, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes([BasePermission])
def post_check_availability(request):
    """
    {
    "uri": "https://mybill.kvish6.co.il/FastPaymentLogin.do?Button_desktop",
    # "uri": "https://mybill.carmeltunnels.co.il/FastPaymentLogin.do",
    "payment_info": {
        "id_number": "26385773",
        "bill": "230562",
        "card_num": "4580110705790910",
        "card_cvv": "969",
        "card_valid_month": "06",
        "card_valid_year": "2023",
        "personalId": "012163234",
        "name": "peletok",
        "price": "14.04"
        }
    }
    :param request:
    :return True or False
    """
    handler = SixRoad(str(request.data.get("uri")), request.data.get("payment_info"))
    res = handler.check_if_payment_availability()
    return Response(res, status=status.HTTP_200_OK)


class SixRoad:

    def __init__(self, url: str, payment_info: dict):
        self.payment_info = payment_info
        self.url = url
        self.driver = MainFunctions(self.url)

    def first_steps(self):
        return_obj = {}
        try:
            self.driver.send_keys(RoadSix.idNumberInput, self.payment_info["id_number"])
            self.driver.send_keys(RoadSix.billNumberInput, self.payment_info["bill"])
            self.driver.click_button(RoadSix.sendButton)
            if self.driver.check_if_element_exits(RoadSix.errorMsg)[0]:
                error_message = self.driver.get_text_from_element(RoadSix.errorMsg)
                return_obj["status"] = 'error'
                return_obj["error_message"] = error_message
                return return_obj
            else:
                return True
        except TimeoutException:
            return_obj["status"] = 'error'
            return_obj["error_message"] = "המערכת לא הצליחה להתחבר, אנא נסה שוב מאוחר יותר."
            return return_obj

    def check_if_payment_availability(self):
        begin = self.first_steps()
        if type(begin) is dict:
            return False
        elif begin:
            if self.driver.check_if_element_exits(RoadSix.noBill)[0]:
                return False
            self.driver.wait_for_element(RoadSix.table)
            all_table_data = [x for x in self.driver.driver.find_element_by_tag_name(
                'tbody').text.split('\n') if len(x) > 4]
            all_six_numbers_in_table = [x.split()[3][-6:] for x in all_table_data]
            all_prices_in_table = [x.split()[-1] for x in all_table_data]
            if self.payment_info["bill"] in all_six_numbers_in_table:
                index = all_six_numbers_in_table.index(self.payment_info["bill"])
                if self.payment_info["price"] == all_prices_in_table[index]:
                    return True
                else:
                    return False
            else:
                return False

    def pay(self):
        begin = self.first_steps()
        return_obj = {}
        if type(begin) is dict:
            return begin
        elif begin:
            if self.driver.check_if_element_exits(RoadSix.noBill)[0]:
                return_obj['status'] = 'error'
                return_obj['error'] = self.driver.get_text_from_element(RoadSix.noBill)
                return return_obj
            self.driver.wait_for_element(RoadSix.table)
            [x.click() for x in self.driver.driver.find_elements(*RoadSix.checkbox)]
            all_table_data = [x for x in self.driver.driver.find_element_by_tag_name(
                'tbody').text.split('\n') if len(x) > 4]
            all_six_numbers_in_table = [x.split()[3][-6:] for x in all_table_data]
            all_prices_in_table = [x.split()[-1] for x in all_table_data]
            if self.payment_info["bill"] not in str(all_table_data):
                return_obj['error'] = 'לא נמצא חיוב לחשבונית זו.'
                return_obj["Additional invoices"] = [x for x in all_table_data if
                                                     self.payment_info["bill"] not in x]
                return return_obj
            if self.payment_info["bill"] in all_six_numbers_in_table:
                index = all_six_numbers_in_table.index(self.payment_info["bill"])
                if self.payment_info["price"] != all_prices_in_table[index]:
                    return_obj['status'] = 'error'
                    return_obj['error'] = \
                        f'סכום התשלום שהזנת {self.payment_info["price"]} לא שווה לסכום המעודכן במערכת' \
                        f' {all_prices_in_table[index]}.'
                    return_obj["Additional invoices"] = [x for x in all_table_data if
                                                         self.payment_info["bill"] not in x]
                    return return_obj
            return_obj["Additional invoices"] = [x for x in all_table_data if self.payment_info[
                "bill"] not in x]
            for i in range(len(all_table_data)):
                if self.payment_info["bill"] in all_table_data[i]:
                    self.driver.driver.find_elements(*RoadSix.checkbox)[i].click()
                    return_obj["price"] = self.driver.get_text_from_element(RoadSix.sumDisplayId)
            self.driver.click_button(RoadSix.actionNext)
            self.driver.click_button_in_elements(RoadSix.selection, 1)
            index = (int(self.payment_info["card_valid_year"]) - datetime.now().year) + 2
            self.driver.click_button((RoadSix.year[0], RoadSix.year[1].format(str(index))))
            self.driver.click_button_in_elements(RoadSix.selection, 2)
            index = int(self.payment_info["card_valid_month"]) + 1
            self.driver.click_button((RoadSix.month[0], RoadSix.month[1].format(str(index))))
            self.driver.send_keys(RoadSix.creditCardOwnerName, self.payment_info["name"])
            self.driver.send_keys(RoadSix.creditOwnerId, self.payment_info["personalId"])
            self.driver.send_keys(RoadSix.creditNumber, self.payment_info["card_num"])
            self.driver.send_keys(RoadSix.cvv, self.payment_info["card_cvv"])
            self.driver.click_button(RoadSix.actionNext)
            self.driver.wait_for_element_until_invisibility(RoadSix.loader)
            if self.driver.check_if_element_exits(RoadSix.confirmation):
                confirmation = self.driver.get_text_from_element(RoadSix.confirmation)
                return_obj["status"] = 'success'
                return_obj["confirmation"] = ''.join(x for x in confirmation if x.isdigit())
            else:
                error_messages = [x.text for x in self.driver.driver.find_elements(
                    *RoadSix.errorMsg) if x.text != '']
                return_obj["status"] = 'error'
                return_obj["error_message"] = error_messages[0]
        return return_obj
