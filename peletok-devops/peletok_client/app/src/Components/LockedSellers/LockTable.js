import React from 'react';
import { Button, Table, Row, Col } from 'reactstrap';
import { Scrollbars } from 'react-custom-scrollbars';
import InputTypeCheckBox from '../InputUtils/InputTypeCheckBox'
import { LanguageManager as LM } from "../LanguageManager/Language";
import LockTableRow from './LockTableRow';
import LockTableHead from './LockTableHead'
/**
 * BanerTable - component that builds the table from the data (this.props.tableRows and this.props.tableHeadersObj) sended by the father component.
 * It use BanerTableHead and BanerTableRow components.
 */
export default class LockTable extends React.Component {
    constructor(props) {
        super(props);
       
      this.tableHeadersObj= {
            businessName: LM.getString("business_name"),
            customNum: LM.getString("customNum"),
            agentNum: LM.getString("agentNum"),
            status: LM.getString("status"),
        }
        this.refsobj={}
    }


    render() {
        var key = 0;
        var tableRows = this.props.tableRows;
        var trId = 'TableRowID';
        var index = 0;
        var trArray = tableRows.map((current) => {
          
            return <tr key={key++}>
                <LockTableRow ref={(componentObj) => { this.refsobj[current.id] = componentObj }} id={trId + index++} tableRowObj={current} />
            </tr>
        });

        return (
            <React.Fragment>

                <div className={'table-responsive table-css ' + (LM.getDirection() === "rtl" ? "table-rtl" : "")}>
                    <Scrollbars style={{ width: '100%', height: '490px' }}>
                        <Table bordered striped size="sm" >
                            <thead className='color-gray' >
                                <tr>
                                    <LockTableHead tableHeadersObj={this.tableHeadersObj} />
                                </tr>
                            </thead>
                            <tbody className='color-gray' >
                                {trArray}
                            </tbody>
                            <tfoot >
                                <tr >
                                    <th colSpan="6">
                                        <div style={{ minHeight: '23.5px' }} />
                                    </th>
                                </tr>
                            </tfoot>
                        </Table>
                    </Scrollbars>
                </div>

            </React.Fragment >
        );
    }
}