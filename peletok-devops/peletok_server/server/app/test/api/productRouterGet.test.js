const models = require('../../index')
const sequelize = require('../../setupSequelize')
const request = require('supertest');
// const routing = require('../../routing')
const {productRouter} = require('../../api/productRouter')
const bodyParser = require('body-parser');
var express = require('express');
const {getStrType, getTypeNum} = require('../../utils')
const {params, mockProducts} = require('./mockeProducts')


let truncateModel = (model) => {
       return sequelize.getQueryInterface().bulkDelete(model.getTableName(),{},{
           truncate: true,
           cascade: true,
       }, model)
}

describe('productRouterGet', () => {

    

    describe('GET /product', () => {

            
            let current_user;
            let app;
            let supplier1;

            beforeAll(async  (done) => {

                await sequelize.sync({
                         force: true,
                         logging: false
                });
                
                [supplier1, current_user] = await mockProducts();
                
                app = express();
                app.use(express.json());
                app.use(express.urlencoded({extended: false}));
                app.use(bodyParser.urlencoded({ extended: false })); 
                // mock req.user
                app.use((req, res, next)=>{      
                    req.user = {
                        id: current_user.id,
                        language_code: 'HE',
                        business_id: 1
                    };
                    next();
                })
                // app.use('/', routing)
                app.use('/product/', productRouter );                
                done()
            } ) 

            it('Should retun status 200', () => {
                return request(app).get('/product/products/' + supplier1.id).then((response) => {
                    expect(response.statusCode).toBe(200);
                } )
            } )

            

            it('Should retun only selected supplier products', () => {
                return request(app).get('/product/products/' + supplier1.id).then((response) => {
                    expect([...new Set(response.body[getStrType(1, 'product')]
                        .map(p=>p.supplier_id))].toString()).toBe([supplier1.id].toString());
                } )
            } )

            

            test('response should be {[product_type_name: string]: product[]}', async () => {
                
                let products = await models.product.model.findAll({
                    where: { supplier_id: supplier1.id }, include: [{
                        model: models.businessProduct.model,
                        as: 'BusinessProduct',
                        attributes: ['favorite']
                    }]
                })
                let tags = await models.tag.model.findAll({
                    where: {type: getTypeNum('product', 'Tag')}
                })

                let productsIds = []
                for (key in products) {
                    productsIds.push(products[key].id)
                    await products[key].addTags(tags)
                }
                await models.businessProduct.model.update({favorite:true},{where:{product_id:productsIds}})
                return request(app).get('/product/products/' + supplier1.id).then((response) => {
                    expect(response.body).toMatchObject({
                        virtual: [
                            {
                                id:1,
                                name: 'he name0',
                                name_he: 'he name0',
                                description: params.description_he + 0,
                                price: 23,
                                buying_price: 22,
                                distribution_fee: 3,
                                image: 'image',
                                tags:tags.map(t=>t.id),
                                favorite: true,
                                call_terms: params.call_terms,
                                call_minute: params.call_minute,
                                sms: params.sms,
                                browsing_package: params.browsing_package,
                                call_to_palestinian: params.call_to_palestinian,
                                abroed_price: params.abroed_price,
                                other1: params.other1,
                                other2: params.other2,
                            },
                            
                        ],
                        manual: [
                           {
                                id: 2,
                                name: 'he name1',
                                name_he: 'he name1',
                                description: params.description_he + 1,
                                price: 23,// TODO handle currency
                                buying_ price: 23,// TODO handle currency
                                distribution_feee: 3,// TODO handle currency
                                image: 'image',
                                tags:tags.map(t=>t.id),
                                favorite: true,
                                call_terms: params.call_terms,
                                call_minute: params.call_minute,
                                sms: params.sms,
                                browsing_package: params.browsing_package,
                                call_to_palestinian: params.call_to_palestinian,
                                abroed_price: params.abroed_price,
                                other1: params.other1,
                                other2: params.other2,
                           },
                        ]
                    });
                } )
            } )

            it('Should return only user\'s business product', async () => {
                await models.businessProduct.model.update({
                    is_authorized: false
                },{
                    where: {
                        business_id: 1,
                        product_id: 1,
                    }
                })
                return request(app).get('/product/products/' + supplier1.id).then((response) => {
                    
                    expect(response.body.virtual).toBe(undefined);
                } )
            } )



      } )
} )