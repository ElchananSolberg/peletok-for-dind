from tests.base_project_test_class import *

@pytest.mark.skip
class TestGlobalSim(BaseProjectTestClass):

    def login_and_get_to_globalSim(self):
        self.login()
        self.driver.tools.wait_and_click(HomePage.globalSim_icon)

    def test_globalSim_talkman_69(self):
        self.login_and_get_to_globalSim()
        self.pay_for_calling_card_vc(69, "203/virtual/2032")
        self.remove_logs_dir = True
