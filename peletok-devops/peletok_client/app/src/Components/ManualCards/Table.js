import React from 'react';
import { Row } from 'reactstrap';
import { LanguageManager as LM } from '../LanguageManager/Language';
import './ManualCards.css';
import { UserFieldComponent } from '../selectUser/userFieldComponent';


export class Table extends React.Component {
    constructor(props) {
        super(props);


        this.state = {
            card_number: this.props.card_number,
            code: this.props.code,
            insert_date: this.props.insert_date != null ? this.props.insert_date : this.props.created_at,
            Time_of_income: this.props.Time_of_income,
            expiry_date: this.props.expiry_date,
            status: this.props.status,
            author: this.props.author

        }
    }

    /**
     * creates a change handling function to be sent to input components that changes
     * 'key' value in this component's state
     * @param {*} key the key to change(/create) in the state upon calling
     */
    onChangeWraper(key, validation) {

        var f = (e) => {
            if (validation(e.target.value)) {
                this.setState({ [key]: e.target.value });
            }
        }
        return f;
    }

    isNumber(value) {
        return [...value].every(c => '0123456789:'.includes(c));
    }

    always(value) {
        return true;
    }

    maxLength(length) {
        return (value) => {
            return value.length <= length;
        }
    }

    numberWithMaxLength(maxLength) {
        return (value) => this.isNumber(value) & this.maxLength(maxLength)(value);
    }

    getValue = () => {
        var card = {
            card_number: this.state.card_number,
            code: this.state.code,
            insert_date: this.state.insert_date,
            Date_of_income: this.state.Date_of_income,
            Time_of_income: this.state.Time_of_income,
            expiry_date: this.state.expiry_date,
            status: this.state.status,
            author: this.state.author
        }
        return card

    }
    render() {

        return (
            <React.Fragment >
                {this.props.row}
                {!this.props.open ?
                    [
                        <Row className='tabl'>
                            <UserFieldComponent type="text" value={this.state.card_number} size={4} fieldTitle={LM.getString("CardsNumManualCards")} id={"CardsNumManualCards" + this.props.id}
                                changeHandler={this.onChangeWraper("card_number", this.numberWithMaxLength(15))} />
                            <UserFieldComponent disabled='true' type="text" value={this.state.insert_date + ' ' + this.state.Time_of_income} size={4} fieldTitle={LM.getString("TimeOfIncome")} id={"TimeOfIncome" + this.props.id} changeHandler={this.onChangeWraper("Time_of_income", this.numberWithMaxLength(15))} />
                            <UserFieldComponent type="date" value={this.state.expiry_date} size={4} fieldTitle={LM.getString("ExpiryDate")} id={"ExpiryDate" + this.props.id} changeHandler={this.onChangeWraper("expiry_date", this.maxLength(11))} />
                        </Row>,
                        <Row className='tabl'>
                            <UserFieldComponent {...this.props.code} type="text" value={this.state.code} size={4} fieldTitle={LM.getString("cardCode")} id={"codeCard" + this.props.id}
                                changeHandler={this.onChangeWraper("code", this.numberWithMaxLength(15))} />

                            <UserFieldComponent disabled='true' type="text" value={this.state.author} size={4} fieldTitle={LM.getString("EnteredBy")} id={"EnteredBy" + this.props.id
                            } changeHandler={this.onChangeWraper("author", this.maxLength(9))} />
                            <UserFieldComponent disabled='true' type="text" value={this.state.status == 2 ? LM.getString("yes") : LM.getString("no")} size={4} fieldTitle={LM.getString("used")} id={"status" + this.props.id} changeHandler={this.onChangeWraper("status", this.maxLength(15))} />
                        </Row>,
                        <Row className='tabl'>
                        </Row>
                    ]
                    : [
                        <Row className='tabl'>

                            <UserFieldComponent type="text" value={this.state.card_number} size={4} fieldTitle={LM.getString("CardsNumManualCards")} id={"CardsNumManualCards" + this.props.id}
                                changeHandler={this.onChangeWraper("card_number", this.numberWithMaxLength(15))} />

                            <UserFieldComponent type="text" value={this.state.code} size={4} fieldTitle={LM.getString("cardCode")} id={"codeCard" + this.props.id}
                                changeHandler={this.onChangeWraper("code", this.numberWithMaxLength(15))} />

                            <UserFieldComponent type="date" value={this.state.expiry_date} size={4} fieldTitle={LM.getString("ExpiryDate")} id={"ExpiryDate" + this.props.id} changeHandler={this.onChangeWraper("expiry_date", this.maxLength(11))} />
                        </Row>,
                        <Row className='tabl'>

                            <UserFieldComponent disabled='true' type="text" value={this.state.status} size={4} fieldTitle={LM.getString("used")} id={"Used" + this.props.id}
                                changeHandler={this.onChangeWraper("status", this.maxLength(15))} />
                        </Row>
                    ]
                }
            </React.Fragment>
        );
    }
}