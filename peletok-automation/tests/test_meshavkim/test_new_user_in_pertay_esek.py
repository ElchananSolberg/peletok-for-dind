from tests.test_meshavkim.base_meshavkim_test_class import *
from datetime import date


class TestMeshavkimPertayEsek(BaseMeshavkimTestClass):

    def login_and_get_to_meshavkim_in_pertay_esek(self):
        self.login_and_get_in_meshavkim()
        self.click_on_side_menu_options("פרטי עסק")

    def test_create_a_new_business_in_pertay_esek(self):
        self.login_and_get_to_meshavkim_in_pertay_esek()
        self.create_a_new_business()
        self.add_details_to_create_a_new_user()
        self.remove_logs_dir = True

    def create_a_new_business(self):
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.click_to_create_a_new_business)
        self.choose_mefitz_in_the_new_business_page()
        self.type_business_name()
        self.insert_number_of_authorized_business()
        self.type_the_responsible_agent()
        self.insert_to_the_first_input_phone_number()
        self.insert_to_the_additional_input_phone_number()
        self.insert_the_address_of_user()
        self.verify_autocomplete_address('רחבת הכ', 'רחבת הכותל, ירושלים')
        self.insert_the_business_hours()
        self.insert_the_email()
        self.check_the_input_business_created_date()
        self.choose_a_suppliers_profile()
        self.choose_percentage_profile()
        self.choose_commissions_profile()
        self.choose_a_tag()
        self.click_on_checkBox_define_seller()

    def choose_mefitz_in_the_new_business_page(self):
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.distributor_input, "peletok")

    def type_business_name(self):
        self.driver.tools.set_text(GeneralMeshavkimPage.input_type_text_business_name,
                                   Meshavkim_users.name_of_new_business)

    def insert_number_of_authorized_business(self):
        self.driver.tools.set_text(GeneralMeshavkimPage.input_authorized_dealer_number,
                                   Meshavkim_users.numbers_for_use[0])

    def type_the_responsible_agent(self):
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.input_agent_responsible, "סוכן סוכן")

    def insert_to_the_first_input_phone_number(self):
        self.driver.tools.set_text(GeneralMeshavkimPage.first_input_pre_phone_number,
                                   Meshavkim_users.numbers_for_use[:3])
        self.driver.tools.set_text(GeneralMeshavkimPage.first_input_seven_num_phone_number,
                                   Meshavkim_users.numbers_for_use[3:])

    def insert_to_the_additional_input_phone_number(self):
        self.driver.tools.set_text(GeneralMeshavkimPage.additional_input_pre_phone_number,
                                   Meshavkim_users.numbers_for_use[:3])
        self.driver.tools.set_text(GeneralMeshavkimPage.additional_input_seven_num_phone_number,
                                   Meshavkim_users.numbers_for_use[3:])

    def insert_the_address_of_user(self):
        self.driver.tools.set_text(GeneralMeshavkimPage.address_business, Meshavkim_users.user_address)

    def verify_autocomplete_address(self, part_address, complete_address):
        self.driver.tools.clear_value(GeneralMeshavkimPage.address_business)
        self.driver.tools.set_text(GeneralMeshavkimPage.address_business, part_address)
        self.driver.tools.wait_and_click(
            selector=(
                GeneralMeshavkimPage.autocomplete_option[0],
                GeneralMeshavkimPage.autocomplete_option[1].format(str(complete_address))))
        complete_address_business = self.driver.tools.wait_for_element_and_get_attribute(
            GeneralMeshavkimPage.address_business, 'value')
        assert complete_address_business == complete_address, "autocomplete address failed"

    def insert_the_business_hours(self):
        self.driver.tools.set_text(GeneralMeshavkimPage.input_business_hours, Meshavkim_users.business_hours)

    def insert_the_email(self):
        self.driver.tools.set_text(GeneralMeshavkimPage.input_business_Email, Meshavkim_users.user_mail)
        self.driver.tools.set_text(GeneralMeshavkimPage.input_accountant_Email, Meshavkim_users.user_mail)

    def check_the_input_business_created_date(self):
        today_is = date.today().strftime("%Y-%m-%d")
        business_date = self.driver.tools.wait_for_element_and_get_attribute(
            GeneralMeshavkimPage.input_business_created_date, attribute='value')
        assert today_is in business_date

    def choose_a_suppliers_profile(self):
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.input_suppliers_profile, 'supplier profile')

    def choose_percentage_profile(self):
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.input_profit_percentage_profile, 'profit profile')

    def choose_commissions_profile(self):
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.input_commissions_profile, 'commission profil')

    def choose_a_tag(self):
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.click_on_tag)

    def click_on_checkBox_define_seller(self):
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.checkBox_defines_seller)

    def add_details_to_create_a_new_user(self):
        self.insert_full_user_name()
        self.insert_user_id_with_invalid_id()
        self.insert_user_id_with_valid_id()
        self.insert_user_phone_num()
        self.insert_user_mail_with_invalid_mail()
        self.insert_user_mail_with_valid_mail()
        self.insert_id_valid_date()
        self.insert_user_address()
        self.insert_user_name()
        self.insert_user_password()
        self.select_user_permission()
        self.save_the_details()
