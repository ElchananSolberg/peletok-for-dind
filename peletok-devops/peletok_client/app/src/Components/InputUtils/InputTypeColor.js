import React from 'react'
import { ChromePicker, SketchPicker, SwatchesPicker } from 'react-color'
import { Button } from 'reactstrap'
import {LanguageManager as LM} from "../LanguageManager/Language";


class InputTypeColor extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            displayColorPicker: false,
            color: /*"#1796e6"*/"var(--regButton)"
        };
    }
    
    
    inputTypeColorChange = (color) => {
        this.handleChange(color)
    }

    runValidation = () => {
        return true
    }

    getValue = () => {
        let summarizingValidationResult = this.runValidation();
        return { valid: summarizingValidationResult, value: this.state.color };
    }

    isRequiredValidation = () => {
        return false;
    }

    setValue(a) {
        this.setState({ color: a })
        return true;
    }

    handleClick = () => {
        this.setState({ displayColorPicker: !this.state.displayColorPicker })
    };

    handleClose = () => {
        this.setState({ displayColorPicker: false })
    };

    handleChange = (color) => {
        this.setState({ color: color.hex })
    };

    render() {
        const popover = {
            position: 'absolute',
            zIndex: '2',
        }
        const cover = {
            position: 'fixed',
            top: '0px',
            right: '0px',
            bottom: '0px',
            left: '0px',
        }
        return (
            <div>
                <Button outline block className={'badge-light align-items-center d-flex p-0'} onClick={this.handleClick} id={this.props.id} >
                    <span className={'span-color ' + (LM.getDirection() === "rtl" ? "ml-2" : "mr-2")} style={{backgroundColor:this.state.color || ""}}></span>
                    {this.props.title}</Button>
                {this.state.displayColorPicker ? <div style={popover}>
                    <div style={cover} onClick={this.handleClose} />
                    <SketchPicker color={this.state.color} onChange={this.handleChange} />
                </div> : null}
            </div>
        )
    }
}


export default InputTypeColor
