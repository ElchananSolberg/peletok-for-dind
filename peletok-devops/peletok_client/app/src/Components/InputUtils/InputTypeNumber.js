import React, { Component } from 'react';
import './InputUtils.css';
import { FormFeedback, Input, Row, Col, Card, CardText, InputGroup, InputGroupAddon } from 'reactstrap';
import { Tooltip } from './Tooltip';
import { LanguageManager as LM } from '../LanguageManager/Language';
import InputTypeRadio from './InputTypeRadio';
import Asterisk from '../Ui/Asterisk';

/**
 * InputTypeNumber component lets to input only the Numbers. 
 * it can render the title above the input (if this.props.title is not an empty string).
 * it can render the tooltip to the side of the title (if this.props.tooltip is not an empty string).
 * it validates the value's length is not less than this.props.minLength and is not bigger than this.props.maxLength
 * It validates the value was entered (if this.props.required is true).
 * It validates the value is valid to the function sent in  this.props.validationFunction;
 * if there are any validation issues the input will get a prop "invalid" and will change a border color to red;
 * if there are any validation issues bellow the input will be the error message 
 * It can add outer onChange function to checkbox onChange.
 * It can change a view to disabled view due to this.props.disabled.
 */
class InputTypeNumber extends Component {
    constructor(props) {
        super(props);
        

        this.setValue = this.setValue.bind(this);

        this.state = {
            value: '',
            validationFuncs: [
                {
                    /**  Validates the value is valid to the function sent in  this.props.validationFunction */
                    f: this.props.validationFunction || (() => (true)),
                    msg: this.props.notValidMessage
                },
                {
                    f: this.innerValidationFunction,
                    msg: this.getInnerValidMsg,
                },
                {
                    f: this.isRequiredValidation,
                    msg: LM.getString("isRequiredValidationFunctionMsg"),
                }
            ],
            inputTypeNumberInnerRadioButtonProps: {
                disabled: false,
                options: [""],
                id: "_" + this.props.id.replace(/\s+/g, '') + "_NumberInnerRadio-",
            },
            errorMessage: '',
        }
    }

    getInnerValidMsg = () => {
        let innerValidMsg = "";
        if (this.props.minLength && this.props.minLength !== -1) {
            innerValidMsg += LM.getString("numberInnerValidationFunctionMsgStart");
            innerValidMsg += LM.getString("numberInnerValidationFunctionMsgNotLess") + this.props.minLength + LM.getString("numberInnerValidationFunctionMsgCharacters");
        }
        if (this.props.maxLength && this.props.maxLength !== -1) {
            if (innerValidMsg === '') {
                innerValidMsg += LM.getString("numberInnerValidationFunctionMsgStart");;
            } else {
                innerValidMsg += LM.getString("numberInnerValidationFunctionMsgAnd");
            }
            innerValidMsg += LM.getString("numberInnerValidationFunctionMsgNotMore") + this.props.maxLength + LM.getString("numberInnerValidationFunctionMsgCharacters");
        }
        if(this.props.max){
            innerValidMsg += LM.getString("noMoreThan") + this.props.max;
        }
        if(this.props.min){
            innerValidMsg += LM.getString("noLessThan") + this.props.min;
        }
        return innerValidMsg
    }

    /** can add outer onChange function to checkbox onChange. */
    inputTypeNumberChange = (e) => {
        e.persist();
        if (this.props.enableNegative) {
            this.setState({ value: e.target.value }, () => {
                if (this.props.onChange) {
                    this.props.onChange(e);
                }
            })
        }
        else {
            this.setState({ value: e.target.value < 0 ? 0 : e.target.value },
                () => {
                    if (this.props.onChange) {
                        this.props.onChange(e);
                    }
                }
            )
        }
    }
    /** summarizing validation function
    * is running 2 times: when input is loosing focus and when getValue function is running (by programmer-user) 
    * */
    runValidation = () => {
        let summarizingValidationResult = (!this.props.required && !this.state.value) || this.state.validationFuncs.every((validation) => validation.f(this.state.value));
        if (!summarizingValidationResult) {
            let messages = [];
            this.state.validationFuncs.forEach((validation) => {
                if (!validation.f(this.state.value)) {
                    messages.push(typeof validation.msg == 'string' ? validation.msg : validation.msg());
                }
            })
            this.setState(({ errorMessage: messages.join(`\n`) }));
        } else {
            this.setState(({ errorMessage: '' }));
        }
        return summarizingValidationResult;
    }

    getValue = () => {
        let summarizingValidationResult = this.runValidation();
        return { valid: summarizingValidationResult, value: this.state.value };
    }

    /** If this.props.required is true validates the value was entered. */
    isRequiredValidation = (value) => {
        if (!value && this.props.required) {
            return false;
        }
        return true;
    }

    /** Validates the value's length is not less than this.props.minLength and is not bigger than this.props.maxLength */
    innerValidationFunction = (value) => {
        let minLength = this.props.minLength;
        let maxLength = this.props.maxLength;
        let max = this.props.max;
        let min = this.props.min;
        if ((value.length < minLength) || (value.length > maxLength) || (value > max) || (value < min)) {
            return false;
        }
        return true;
    }

    setValue(newVal) {
        this.setState({
            value: newVal
        })
    }

    render() {
        let disabled = this.props.disabled;
        let placeholder = this.props.placeholder;
        let id = this.props.id.replace(/\s+/g, '');
        let title = this.props.title;
        let tooltipText = this.props.tooltip;

        let type2 = this.props.type2;
        var money = this.props.money;
        var radio = this.props.radio;
        let header = <Col className={"d-flex justify-content-between line-height-1 " + (title ? 'py-1' : '')}>
            {title ?
                <div className="fs-17 white-space">{title}
                    <Asterisk show={this.props.required} />
                </div>
                : <></>
            }
            {tooltipText ?
                <Tooltip tooltipText={tooltipText} id={id} />
                : <></>
            }
        </Col>;
        return (type2 ?
            <>
                <Row >
                    <Col >
                        <Card className="type2Card">
                            <CardText className="type2CardText"  >
                                {title} <Asterisk show={this.props.required} />
                            </CardText>
                            <Input className={"numberInput type2Input " + (LM.getDirection() === "rtl" ? "rtl" : "")} type='number'
                                id={`InputTypeNumber${id}`} placeholder={placeholder}
                                disabled={disabled} invalid={this.state.errorMessage.length !== 0}
                                onChange={this.inputTypeNumberChange} onBlur={this.runValidation}
                                value={this.state.value}
                            />
                            {/** if there are any validation issues bellow the input will be the error message */}
                            <FormFeedback className='ws-pre'>{this.state.errorMessage}</FormFeedback>
                        </Card>
                    </Col>
                </Row>
            </>
            :
            <>
                <Row >
                    {radio ?
                        <>
                            <Col style={{ maxWidth: '4%' }}>
                                <InputTypeRadio {...this.state.inputTypeNumberInnerRadioButtonProps} onChange={this.props.numberInnerRadioChange} />
                            </Col>
                            {header}

                        </>
                        :
                        <>
                            {header}
                        </>
                    }

                </Row>
                {money ?
                    <Row className={'mt-auto'}>
                        <Col style={{ paddingStart: '0px' }}>
                            <InputGroup>
                                {/** if there are any validation issues the input will get a prop "invalid" and will change a border color to red */}
                                <Input type='number' id={`InputTypeNumber${id}`} placeholder={placeholder}
                                    disabled={disabled} invalid={this.state.errorMessage.length !== 0}
                                    onChange={this.inputTypeNumberChange} onBlur={this.runValidation}
                                    value={this.state.value}
                                    className={"numberInput " + (LM.getDirection() === "rtl" ? "rtl inputUtilsBorderRadiusLeftNole" : "")}
                                />
                                <InputGroupAddon className={LM.getDirection() === "rtl" ? "inputUtilsBorderRadiusLeft" : "inputUtilsBorderRadiusRight"} addonType="append">₪</InputGroupAddon>
                                {/** if there are any validation issues bellow the input will be the error message */}
                                <FormFeedback className='ws-pre'>{this.state.errorMessage}</FormFeedback>
                            </InputGroup>

                        </Col>
                    </Row>
                    :
                    <Row className={'mt-auto'}>
                        <Col>
                            {/** if there are any validation issues the input will get a prop "invalid" and will change a border color to red */}
                            <Input type='number' id={`InputTypeNumber${id}`} placeholder={placeholder}
                                disabled={disabled} invalid={this.state.errorMessage.length !== 0}
                                onChange={this.inputTypeNumberChange} onBlur={this.runValidation}
                                value={this.state.value}
                                className={"numberInput " +
                                    (LM.getDirection() === "rtl" ? "rtl" : "") +
                                    (this.props.enableNegative ? " enableNegative" : "")
                                }
                                style={this.props.outerStyle}
                            />
                            {this.props.disableErrorMessage ?
                                <></>
                                :
                                /** if there are any validation issues bellow the input will be the error message */
                                <FormFeedback className='ws-pre'>{this.state.errorMessage}</FormFeedback>
                            }
                        </Col>
                    </Row>
                }
            </>
        );
    }
}

export default InputTypeNumber;