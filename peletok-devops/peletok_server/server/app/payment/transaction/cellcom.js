const fs = require("fs")
const logs = require('../../logger')
const PaymentBasic = require('../paymentBasic');
const models = require('../../index');
const ResInfo = require('../../resInfo')
const xml_string = fs.readFileSync(__dirname + "/template/cellcom.xml")
const LM = require('../../config/LanguageManager').LM
const axios = require('axios').default;
const utils = require("../../utils")
const xml2js = require('xml2js')
const product = models.product
const productModel = product.model
const {MongoLogger} = require('../../mongoLogger')

const productPrice = models.productPrice
const productPriceModel = productPrice.model
var success = "1"
var actionPay = 2
var actionCancel = 4
const parser = new xml2js.Parser();
var builder = new xml2js.Builder();

const urlToken = "https://api.cellcom.co.il/api/services/getProviderToken"
const urlPayment = "https://api.cellcom.co.il/api/services/provider/virtualDeposit"
const clientID = "d96f5236-f0fe-47f0-8c34-333b71546ec0"
const clientKod = "ee48a746-711d-4025-913e-958ca606dac9"


/***
 * Payment manager Cellcom
 *
 */
class Cellcom extends PaymentBasic {

    constructor(contractNumber, productId, businessId, req) {
        super()
        this.req=req
        this.contractNumber = contractNumber
        this.productId = productId
        this.businessId = businessId
        this.info = new ResInfo(req)
    }


    
    /***
            * step 1  to  pay (get token for connection)   
            @returns number  of   token
            */
    async getToken() {
        let token = null
        await axios({
            method: 'post',
            url: urlToken,
            headers: {
                Format: "Xml",
                client_secret: clientKod,
                grant_type: "client_credentials",
                client_id: clientID,
                scope: "resource.READ"
            }
        })
        .then(res => {
            token = res.data["access_token"];
        }).catch(e => {
            this.info.setErrMessage("err_connection_site" + e.toString())
            this.info.error.err_connection_site = true

        })

        return token
    }


    /***
            Pay(buy) in Cellcom
            */
    async pay(transactionId) {
        console.log("111111111",this.req.user.id);
        
        let userDemo= await this.isDemoTransactionUser()
        if(userDemo){
            return this.demoRespons()
        }else{
            return this.transactionToCellcom(actionPay, transactionId)
        }
        
        

    }
    /***
          cancel payment in Cellcom
           */
        async  cancelPayment(transactionId) {
            
            if((await this.isDemoTransactionUser())){
                return this.demoRespons()
            }else{
                return this.transactionToCellcom(actionCancel, transactionId)
            }
            
        }


    /***
            * Pay or  cancell  to Cellcom    
            @returns number  of   confirmation
          
            */
    async transactionToCellcom(action, transactionId) {
        const date = utils.getCurrentTime(0, 8)
        const time = utils.getCurrentTime(9, 15)
        let reqPrice = await this.getPrice()
        let price = reqPrice.price
        let item = await productModel.findOne(
            {
                where: { id: this.productId }
            })

        let itemName = item["supplier_identity_number"]
        if (!itemName) {
            this.info.setErrMessage(LM.getErrorBasic(), "Error in supplier_identity_number ")
            return this.info
        }
        logs.info("time is ", time)
        const reference = transactionId
        const BaseData = await this.importTemplate()
        const token = await this.getToken()

        
        const data = BaseData["soapenv:Envelope"]['soapenv:Body'][0]["tkm:virtualDeposit"][0]
        data["action"] = action
        data["referenceId"] = reference
        data["depositDate"] = date
        data["depositTime"] = time
        data["subscriberNo"] = this.contractNumber
        data["depositAmt"] = price
        data["cardCode"] = itemName
        data["pointOfSale"] = this.businessId
        var xml = builder.buildObject(BaseData);

        await axios({
            method: 'post',
            url: urlPayment,
            headers: {
                Authorization: token
            },
            data: xml
        }).then(res => {

            MongoLogger.logTransaction({
                transactionId,
                supplierId: this.req.params.supplierID,
                requestParams: this.req.body,
                request:xml,
                response:res.data,
            })
            if (!res.data["soap:Envelope"]) {

                this.info.setErrMessage(LM.getErrorBasic(), res.data['env:Envelope']["env:Header"]["env:ReturnCodeMessage"])
                return this.info
            }
            else {
                const reply = res.data["soap:Envelope"]['soap:Body']['ns2:virtualDepositResponse']["return"]


                if (reply["returnCode"] == success) {
                    this.info.confirmation = reply["referenceId"]
                    this.info.send = { "success": reply["referenceId"] }
                    this.info.isPaid = true
                    return this.info
                }
                else if (reply["returnCode"] == 8) {
                    this.info.error.err_in_payment = true
                    this.info.setErrMessage(LM.getString("invalidSubscriber"), reply["returnCodeMessage"])
                    return this.info
                }

                else {
                    this.info.error.err_in_payment = true
                    this.info.setErrMessage(LM.getErrorBasic(), reply["returnCodeMessage"])

                    return this.info
                }
            }


        }).catch(err => {
            this.info.error.err_connection_site = true
            this.info.setErrMessage(LM.getErrorBasic(), "Error in connection" + err)
            
        })

        return this.info
    }
    /***
          import  xml Template for soap requst 
         */
    async importTemplate() {

        return new Promise(resolve => {
            parser.parseString(
                xml_string,
                (error, result) => {
                    resolve(result)

                });

        })
    }


}
module.exports = Cellcom;