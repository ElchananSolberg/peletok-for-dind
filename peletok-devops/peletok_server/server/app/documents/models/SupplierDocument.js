const BaseModel = require('../../BaseModel');
const DataTypes = require("sequelize").DataTypes;

/**
 * SupplierDocument model
 */
class SupplierDocument extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            }
        };
        this.prefix = 'DOC';
    }
    /**
     * creates associate for SupplierDocument model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.supplier.model, { as: 'Supplier' });
        this.model.belongsTo(models.document.model, { as: 'Document' });
    }

}
// exporting instance
module.exports = new SupplierDocument();