from tests.base_project_test_class import *
from tests.test_meshavkim.meshavkim_users import Meshavkim_users


class BaseMeshavkimTestClass(BaseProjectTestClass):

    def login_and_get_in_meshavkim(self):
        self.login()
        self.driver.tools.wait_and_click(HomePage.meshavkim_page)

    def click_on_side_menu_options(self, name_of_test):
        self.driver.tools.wait_and_click(
            (GeneralMeshavkimPage.side_menu_options[0], GeneralMeshavkimPage.side_menu_options[1].format(name_of_test)))
        self.driver.wait.wait_for_element_to_be_invisible(GeneralMeshavkimPage.wait_for_spiner_loading)

    def search_detail_buisness_by_client_number(self, client_number):
        self.driver.tools.set_text(GeneralMeshavkimPage.input_business_client_number, client_number)

    def click_on_input_and_choose_option(self, input_selector, choice_name):
        self.driver.tools.wait_and_click(input_selector)
        self.driver.tools.wait_and_click((MainSuppliersAndProductPage.choose_options_from_input_by_name[0],
                                          MainSuppliersAndProductPage.choose_options_from_input_by_name[1]
                                          .format(choice_name)))
        assert choice_name.replace(" ", "") in self.driver.tools.get_text_from_element(input_selector) \
            .replace(" ", ""), 'your choice is not in the input'

    def input_filter_client_number_and_click_btn_search(self, client_number):
        self.driver.tools.set_text(GeneralMeshavkimPage.business_client_number, client_number)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.btn_search)

    def check_if_input_client_number_matches(self):
        client_num = self.driver.tools.wait_for_element_and_get_attribute(
            GeneralMeshavkimPage.input_client_number_after_search_btn, attribute='textContent')
        assert client_num in self.driver.tools.wait_for_element_and_get_attribute(
            GeneralMeshavkimPage.business_client_number, attribute='textContent'), "the input's text doesn't match"

    def click_on_input_and_choose_option(self, input_selector, choice_name):
        self.driver.tools.wait_and_click(input_selector)
        self.driver.tools.wait_and_click((MainSuppliersAndProductPage.choose_options_from_input_by_name[0],
                                          MainSuppliersAndProductPage.choose_options_from_input_by_name[1]
                                          .format(choice_name)))
        assert choice_name.replace(" ", "") in self.driver.tools.get_text_from_element(input_selector) \
            .replace(" ", ""), 'your choice is not in the input'

    def input_filter_behirah_mefizt_and_meshavek(self, mefitz, meshavek):
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.filter_behirat_mefit)
        self.driver.tools.wait_and_click(mefitz)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.filter_behirat_meshavek)
        self.driver.tools.wait_and_click(meshavek)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.btn_search)

    def verify_name_business(self):
        shem_essec = self.driver.tools.wait_for_element_and_get_attribute(GeneralMeshavkimPage.business_name, 'value')
        meshavek = self.driver.tools.get_text_from_element(GeneralMeshavkimPage.filter_behirat_meshavek)
        assert shem_essec == meshavek, "filter problem"

    def input_filter_phone_number_and_click_btn_search(self):
        self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.pre_number_filter)
        self.driver.tools.set_text(GeneralMeshavkimPage.pre_number_filter, User.phone_meshavkim[:3])
        self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.number_filter)
        self.driver.tools.set_text(GeneralMeshavkimPage.number_filter, User.phone_meshavkim[3:])
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.btn_search)

    def verify_phone_number_business(self):
        pre_phone_number = self.driver.tools.wait_for_element_and_get_attribute(
            GeneralMeshavkimPage.pre_phone_number_business, 'value')
        self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.phone_number_business)
        assert User.phone_meshavkim[:3] == pre_phone_number, "filter pre phone number failed"
        phone_number = self.driver.tools.wait_for_element_and_get_attribute(
            GeneralMeshavkimPage.phone_number_business, 'value')
        self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.phone_number_business)
        assert User.phone_meshavkim[3:] == phone_number, "filter phone number failed"

    def input_number_business_authorized_dealer_and_click_btn_search(self):
        self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.business_authorized_dealer_filter)
        self.driver.tools.set_text(GeneralMeshavkimPage.business_authorized_dealer_filter,
                                   User.num_business_authorized_dealer)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.btn_search)

    def verify_number_business_authorized_dealer(self):
        business_authorized_dealer = self.driver.tools.wait_for_element_and_get_attribute(
            GeneralMeshavkimPage.business_authorized_dealer, 'value')
        assert User.num_business_authorized_dealer == business_authorized_dealer, "filter business authorized dealer " \
                                                                                  "failed "

    def verify_name_business(self):
        shem_essec = self.driver.tools.wait_for_element_and_get_attribute(GeneralMeshavkimPage.business_name, 'value')
        meshavek = self.driver.tools.get_text_from_element(GeneralMeshavkimPage.filter_behirat_meshavek)
        assert shem_essec == meshavek, "filter problem"

    def verify_autocomplete_address(self, part_address, complete_address):
        self.driver.tools.clear_value(GeneralMeshavkimPage.address_business)
        self.driver.tools.set_text(GeneralMeshavkimPage.address_business, part_address)
        self.driver.tools.wait_and_click(
            selector=(
                GeneralMeshavkimPage.autocomplete_option[0],
                GeneralMeshavkimPage.autocomplete_option[1].format(str(complete_address))))
        complete_address_business = self.driver.tools.wait_for_element_and_get_attribute(
            GeneralMeshavkimPage.address_business, 'value')
        assert complete_address_business == complete_address, "autocomplete address failed"

    def verify_if_address_mail_exist(self):
        self.input_filter_behirah_mefizt_and_meshavek(GeneralMeshavkimPage.peletok_mefitz,
                                                      GeneralMeshavkimPage.yedidia_naooul_meshavek)
        mail_address = self.driver.tools.wait_for_element_and_get_attribute(GeneralMeshavkimPage.email_business,
                                                                            'value')
        assert mail_address != "", "there is no email address"

    def verify_email_business_invoice(self):
        mail_business_invoice = self.driver.tools.wait_for_element_and_get_attribute(
            GeneralMeshavkimPage.email_buisness_invoice, 'value')
        if mail_business_invoice == "":
            self.driver.tools.set_text(GeneralMeshavkimPage.email_buisness_invoice, 'email@exemple.com')
            mail_address = self.driver.tools.wait_for_element_and_get_attribute(
                GeneralMeshavkimPage.email_buisness_invoice,
                'value')
            assert mail_address != "", "This input does not work"

    def verify_if_exist_date_create_business(self):
        date_create_business = self.driver.tools.wait_for_element_and_get_attribute(
            GeneralMeshavkimPage.date_create_business,
            'value')
        assert date_create_business != "", "a creation date is missing"

    def verify_if_changes_have_been_saved(self):
        self.driver.tools.clear_value(GeneralMeshavkimPage.add_phone_number)
        self.driver.tools.set_text(GeneralMeshavkimPage.add_phone_number, User.add_phone_meshavkim)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.save_button)
        self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.alert_success)

    def input_filter_client_number_and_click_btn_search(self, client_number):
        self.driver.tools.set_text(GeneralMeshavkimPage.business_client_number, client_number)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.btn_search)

    def insert_full_user_name(self):
        self.driver.tools.set_text(GeneralMeshavkimPage.input_user_first_name, Meshavkim_users.first_name)
        self.driver.tools.set_text(GeneralMeshavkimPage.input_user_last_name, Meshavkim_users.last_name)

    def insert_user_id_with_invalid_id(self):
        self.driver.tools.set_text(GeneralMeshavkimPage.input_user_id, Meshavkim_users.user_id_invalid)
        self.check_if_the_id_is_valid()

    def check_if_the_id_is_valid(self):
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.input_user_phone_num)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.click_on_cancel_button_in_alert_wrong)
        self.driver.tools.clear_value(GeneralMeshavkimPage.input_user_id)
        self.driver.tools.set_text(GeneralMeshavkimPage.input_user_id, Meshavkim_users.user_id_invalid)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.input_user_phone_num)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.click_on_primary_button_in_alert_wrong)

    def insert_user_id_with_valid_id(self):
        self.driver.tools.clear_value(GeneralMeshavkimPage.input_user_id)
        self.driver.tools.set_text(GeneralMeshavkimPage.input_user_id, Meshavkim_users.user_valid_id)

    def insert_user_phone_num(self):
        self.driver.tools.set_text(GeneralMeshavkimPage.input_user_phone_num, Meshavkim_users.numbers_for_use)

    def insert_user_mail_with_invalid_mail(self):
        self.driver.tools.set_text(GeneralMeshavkimPage.input_user_mail, Meshavkim_users.user_invalid_mail)
        self.close_alert_by_cancel_btn()
        self.close_alert_by_primary_btn()

    def close_alert_by_cancel_btn(self):
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.input_user_address)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.click_on_cancel_button_in_alert_wrong)

    def close_alert_by_primary_btn(self):
        self.driver.tools.clear_value_and_set_text(GeneralMeshavkimPage.input_user_mail, Meshavkim_users.user_invalid_mail)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.input_user_address)
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.click_on_primary_button_in_alert_wrong)

    def insert_user_mail_with_valid_mail(self):
        self.driver.tools.clear_value(GeneralMeshavkimPage.input_user_mail)
        self.driver.tools.set_text(GeneralMeshavkimPage.input_user_mail, Meshavkim_users.user_mail)

    def insert_id_valid_date(self):
        self.driver.tools.set_text(GeneralMeshavkimPage.input_user_id_card_valid_date, Meshavkim_users.user_id_valid_date)

    def insert_user_address(self):
        self.driver.tools.set_text(GeneralMeshavkimPage.input_user_address, Meshavkim_users.user_address)

    def insert_user_name(self):
        self.driver.tools.set_text(GeneralMeshavkimPage.input_select_user_name, Meshavkim_users.user_name)

    def insert_user_password(self):
        self.driver.tools.set_text(GeneralMeshavkimPage.input_select_password, Meshavkim_users.password)

    def select_user_permission(self):
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.select_user_permission, 'משווק')

    def save_the_details(self):
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.save_button)
