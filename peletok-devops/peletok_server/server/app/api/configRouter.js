const express = require('express');
const configRouter = express.Router();
const models = require('../index.js');
const Sequelize = require('sequelize');
const Op = Sequelize.Op
const moment = require('moment')




configRouter.post('/vat', async (req, res)=>{
	await models.vATRate.model.create({
		value: req.body.vat,
		start_timestamp: moment()
	})

	res.send('success')
})

configRouter.get('/vat', async (req, res)=>{
	const vat = (await models.vATRate.model.findOne({
        order: [
            ['start_timestamp', 'DESC']
        ],
        attributes: ['value'],
        raw: true
    })).value;
    res.send({vat})
})



module.exports.configRouter = configRouter;



