const BaseModel = require('../../BaseModel');
const DataTypes = require("sequelize").DataTypes;

/**
 * SupplierColor model
 */
class SupplierColor extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            logo_background: {
                type: DataTypes.STRING(16),
                allowNull: true
            },
            font: {
                type: DataTypes.STRING(16),
                allowNull: true
            },
            product_background: {
                type: DataTypes.STRING(16),
                allowNull: true
            },
            chosen_background: {
                type: DataTypes.STRING(16),
                allowNull: true
            },
            favorite_star_color: {
                type: DataTypes.STRING(16),
                allowNull: true
            }
        };
        this.prefix = "SUPL";
        this.options["indexes"] = [
            {
                fields: ["supplier_id"],
                unique: true
            }
        ];
    }

    /**
     * creates associate for SupplierColor model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.supplier.model, { as: 'Supplier'});
    }
}

module.exports = new SupplierColor();
