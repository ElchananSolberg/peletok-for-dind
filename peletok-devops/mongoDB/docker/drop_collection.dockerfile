FROM python:3.6

WORKDIR /app

RUN pip install pymongo[srv]

COPY ./drop_mongo.py /app

