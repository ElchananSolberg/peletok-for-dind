import React, { Component } from 'react';
import { Button, Container, Row, Col } from 'reactstrap';
import { LanguageManager as LM } from '../LanguageManager/Language';
import InputTypeText from '../InputUtils/InputTypeText';
import InputTypeNumber from '../InputUtils/InputTypeNumber';
import InputTypeSelect from '../InputUtils/InputTypeSelect';
import {Notifications} from '../Notifications/Notifications';
import {Loader} from '../Loader/Loader';
import './CreditCard.css'
import {
    putCreditCards,
    postCreditCards
} from '../DataProvider/DataProvider'
/**
 * A componenent thatWhich receives credit card details

 * TODO -To actually produce the receipt and preservation of information in the database
 * TODO -To set up functionalities waiting for a real or imagined database
 */

export class CardForm extends Component {

    fields = {};

    constructor(props) {
        super(props);
        this.state = {
            
        }


    }

    componentDidMount(){
        Object.keys(this.props.card).forEach((k)=>{
            this.fields[k] && this.fields[k].setValue(this.props.card[k] || '')
        })
    }

    save = () =>{
        let isValid = true;
        let data = {}
        Object.keys(this.fields).forEach((k)=>{
            isValid = this.fields[k].runValidation() && isValid
            data[k] = this.fields[k].getValue().value
            if(k == 'cc_validity_month'){
                data[k] = LM.getString('monthArrayWithNum').find(o=>o.label == data[k]).value
            }
        })

        if(!isValid){
            Notifications.show('invalid', 'danger')
        } else {
            if(this.props.card.id){
                Loader.show()
                putCreditCards(this.props.card.id, data).then(res=>{
                    Loader.hide()
                    Notifications.show(LM.getString('updatedSuccessfully'), 'success')
                    this.props.cancelEdit()
                }).catch((err)=>{
                    Loader.hide()
                    Notifications.show(LM.getString('error_while_saving'), 'danger')
                })
            } else {
                Loader.show()
                postCreditCards(data).then(res=>{
                    Loader.hide()
                    Notifications.show(LM.getString('savedSuccessfully'), 'success')
                    this.props.cancelEdit()
                }).catch((err)=>{
                    Loader.hide()
                    Notifications.show(LM.getString('error_while_saving'), 'danger')
                })
            }
        }



    }

    getValue = () => {
        return this.state.add;
    }
    


    render() {
        
            return (<div className={/*this.props.add ?*/ 'creditCardSettings' /*: ''*/}>
                <p className='mainCreditCard'>{this.props.header}</p>
                <Row >
                    <Col lg="4" sm="6">
                        <InputTypeText
                            required={true}
                            title={LM.getString("CardholdersName")}
                            id="CardholdersName"
                            ref={(componentObj) => { this.fields.cc_company_name = componentObj }} />
                    </Col>
                    <Col lg="4" sm="6">
                        <InputTypeText
                            required={!this.props.card.id}
                            title={LM.getString("CardsNum")}
                            id="CardsNum"
                            ref={(componentObj) => { this.fields.cc_number = componentObj }} />
                    </Col>
                </Row>
                <Row>
                    <Col lg="4" sm="6">
                        <InputTypeText
                            required={true}
                            title={LM.getString("idNumCreditCard")}
                            id="idNumCreditCard"
                            ref={(componentObj) => { this.fields.cc_holder_id = componentObj }} />
                    </Col>
                    <Col lg="4" sm='6'>
                        <div className='fs-17'>{LM.getString("selectUseridCardValidDate")}</div>
                        <Row>
                            <Col sm="6" className={'white-space'}>
                                <InputTypeSelect
                                    options={LM.getString("yearsArray")}
                                    default={LM.getString("yearsArray")[1]}
                                    id="IdentityCardValidityyearsArray"
                                    required={true}
                                    ref={(componentObj) => { this.fields.cc_validity_year = componentObj }} />
                            </Col>
                            <Col sm="6">
                                <InputTypeSelect
                                    options={LM.getString("monthArray")}
                                    default={LM.getString("monthArray")[1]}
                                    id="IdentityCardValiditymonthArray"
                                    required={true}
                                    ref={(componentObj) => { this.fields.cc_validity_month = componentObj }} />
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row>
                    <Col lg="4" sm="6">
                        <InputTypeText
                            required={true}
                            title={LM.getString("DescriptionCreditCard")}
                            id="DescriptionCreditCard"
                            ref={(componentObj) => { this.fields.description = componentObj }} />
                    </Col>
                    {this.props.card.id ? <Col lg="4" sm="6">
                        <InputTypeText
                            disabled={true}
                            title={LM.getString("Last4NumCreditCard")}
                            id="Last4NumCreditCard"
                            ref={(componentObj) => { this.fields.last_4_numbers = componentObj }} />
                    </Col> : <Col lg="4" sm="6">
                        <InputTypeText
                            required={true}
                            title="cvv"
                            id="cvvField"
                            ref={(componentObj) => { this.fields.cc_cvv_number = componentObj }} />
                    </Col>
                }
                </Row>
                <Button 
                    className={'creditCardButton fs-17 px-4 btn-sm-block mb-1'}
                    onClick={this.save}
                >{LM.getString("save")}</Button>
                <Button className={'btn-lg fs-17 px-4 mx-lg-3 mx-md-3 font-weight-bold border-radius-3 btn-sm-block mb-1'} color="secondary" onClick={this.props.cancelEdit}>{LM.getString("cancel")}</Button>
            </div>)

        
    }
}