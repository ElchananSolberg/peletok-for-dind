import React, { Component } from 'react';
import { Button, Container, Row, Col } from 'reactstrap';
import { LanguageManager as LM } from '../LanguageManager/Language';
import './CreditCard.css';
import { CardForm } from './CardForm'
import {
    getCreditCards, deleteCreditCards
} from '../DataProvider/DataProvider'
import PtTable from '../PtTable/PtTable'
import TableHead from '../PtTable/TableHead'
import TableBody from '../PtTable/TableBody'
import TableRow from '../PtTable/TableRow'
import {Notifications} from '../Notifications/Notifications';
import {Loader} from '../Loader/Loader';
import {Confirm} from '../Confirm/Confirm';
/**
 * A component that receives credit card details
 * TODO -To set up functionalities waiting for a real or imagined database
 * TODO -To actually produce the receipt and preservation of information in the database
 */

export class CreditCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cards: [],
            add: true,
            selectedCard: null,
            isCreateMode: false,
        }
    
        this.tableHeaders = [
            'cc_company_name',
            'last_4_numbers',
            'cc_holder_name',
            'cc_holder_id',
            'cc_cvv_number',
            'cc_validity',
            'description',
            'actions'
        ]
    }

    
    getEditHandler = (card) => {
        return () => {
            this.setState({
                selectedCard: card
            })
        }
    }

    componentDidMount(){
        
        getCreditCards().then(res=>{
            this.setState({
                cards: res
            })
        })
    }

    resetSelectedCard = ()=>{
        getCreditCards().then(res=>{
            this.setState({
                cards: res
            }, ()=>{
            this.setState({
                selectedCard: null
            })
        })
        })
        

    }


    getDeleteCardHandler = (card) => {
        return ()=>{
            Confirm.confirm(LM.getString('confirmDeleteCard')).then(()=>{
                Loader.show()
                deleteCreditCards(card.id).then((res)=>{
                    getCreditCards().then(res=>{
                        this.setState({
                            cards: res
                        })
                    })
                    Loader.hide()
                    Notifications.show(LM.getString('deletedSuccessfully'), 'success')
                }).catch((err)=>{
                    Loader.hide()
                    Notifications.show(LM.getString('error_while_deleting'), 'danger')
                })
            })
        }
    }
    
    addCardHandler = () => {
        this.setState({
            selectedCard: {}
        })

    }

    render() {

        return (
            <Container className="creditCardContainer">
                {!this.state.selectedCard ? <React.Fragment>
                    <Row >
                        <Col className='creditCardHeader'>
                            {LM.getString("paymentInfo")}
                        </Col>
                    </Row>
                    <Row >
                        <Col sm="auto">
                                <Button
                                    id='creditCardAdd'
                                    onClick={this.addCardHandler}
                                    className="addtButton"
                                    className="suppliersButton"
                                >
                                    {LM.getString("new")}
                                </Button>
                            </Col>
                    </Row>
                    <Row style={{ marginTop: '30px' }}>
                        <Col >
                            {this.state.cards.length > 0 ?
                                <PtTable>
                                    <TableHead>
                                        {this.tableHeaders.map((h, i) => <th style={{ whiteSpace: 'nowrap' }} key={i}>{LM.getString(h)}</th>)}
                                    </TableHead>
                                    <TableBody>
                                        {this.state.cards.map((t, i) => {
                                            return <TableRow key={i} className="trx-row">
                                                {this.tableHeaders.filter(h => h != 'actions').map((k, index) => { return <td key={index}>{t[k]}</td> })}
                                                <td>
                                                    <Button className="CommissionProfileSmallButton" style={{ marginLeft: '1px' }} onClick={this.getEditHandler(t)} color="warning">{LM.getString('edit')}</Button>
                                                    <Button className="CommissionProfileSmallButton" style={{ marginLeft: '1px' }} onClick={this.getDeleteCardHandler(t)} color="danger">{LM.getString('delete')}</Button>
                                                </td>
                                            </TableRow>

                                        })}
                                    </TableBody>
                                </PtTable> :
                                <PtTable>
                                    <tr><td colspan={this.tableHeaders.length} >{LM.getString('noTrxFoundToCancel')}</td></tr>
                                </PtTable>
                            }
                        </Col>
                    </Row>
                    </React.Fragment> :
                    <CardForm
                        cancelEdit={this.resetSelectedCard}
                        isCreateMode={this.state.isCreateMode}
                        card={this.state.selectedCard} 
                        header={LM.getString('CreditCard')}
                        ref={(componentObj) => { this.name = componentObj }}

                    />}

            </Container>
        )
    }
}