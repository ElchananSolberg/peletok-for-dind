const BaseModel = require("../../BaseModel");
const DataTypes = require("sequelize").DataTypes;

/**
 * CityPaymentTypeSite model
 */
class CityPaymentTypeSite extends BaseModel {


    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            params: {
                type: DataTypes.STRING(1024),
                allowNull: false
            }
        };
        this.prefix = "PAY";
        this.author = true;
    }

    /**
     * creates associate for CityPaymentTypeSite model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.city.model, {as: "City"});
        this.model.belongsTo(models.cityPaymentType.model, {as: "CityPaymentType"});
        this.model.belongsTo(models.site.model, {as: "Site"});
    }
}

module.exports = new CityPaymentTypeSite();