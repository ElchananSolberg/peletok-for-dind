import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {CookiesProvider} from 'react-cookie';
import * as Sentry from '@sentry/browser';

import 'bootstrap/dist/css/bootstrap.min.css';

if(process.env.REACT_APP_ENV === 'staging-k8s'){
	Sentry.init({dsn: "https://77f08756710946a8830cec8187bbb685@sentry.io/5182803"});
}

if(process.env.REACT_APP_ENV === 'prod-k8s'){
	Sentry.init({dsn: "https://d478498cd6af4bed839835a0e0d0882e@sentry.io/5182891"});
}


ReactDOM.render(
    <CookiesProvider>
        <App/>
    </CookiesProvider>
    , document.getElementById('root'));


