import React, { Component } from 'react'
import { LanguageManager as LM } from '../../../LanguageManager/Language';
import { Button, Container, Row, Col } from 'reactstrap';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import './ElectricityChargeCancelation.css';
import InputTypeNumber from '../../../InputUtils/InputTypeNumber';
import InputTypePhone from '../../../InputUtils/InputTypePhone';
import {postCancelTheDeal, postChargeCancel, postConfirmTheDeal} from "../../../DataProvider/Functions/DataPoster";
import {Notifications} from "../../../Notifications/Notifications";

export class ElectricityChargeCancelation extends Component {

    /**
     * Onchange and validation stoped working after the use 
     * of Input component, in the next version of component will
     * be validation func  
     *  
     */
    constructor(props) {
        super(props)

        this.state = {

            contractNumber: undefined,
            transactionNumber: undefined,
            terminalNumber: undefined,
            phoneNumber: undefined,
            modal: false,
            data: undefined,
            idAppeal: undefined,
        }
    }

    toggle = () => {
        this.setState({
            modal: !this.state.modal
        })
    }

    chargeCancelClick = () => {
        if (this.contractNumberInput.getValue().valid &&
            this.transactionNumberInput.getValue().valid &&
            this.terminalNumberInput.getValue().valid &&
            this.phoneNumberInput.getValue().valid) {
            let data = {
                    contractNumber: this.contractNumberInput.getValue().value,
                    transationId: this.transactionNumberInput.getValue().value,
                    businessId: this.terminalNumberInput.getValue().value

            };
            this.setState({date: data})
            postChargeCancel(data, 185).then(
                res => {
                    this.setState({idAppeal: res})
                    this.toggle()
                }
            ).catch(
                err => {
                    Notifications.show(err.response.data.error, 'danger');
                    this.toggle()
                }
            );
        }
    }

    confirmTheDeal = () => {
        this.toggle()
        let date = {
            'idAppeal': this.state.idAppeal,
            'businessId': this.state.data.businessId,
            'contractNumber': this.state.contractNumber
        };
        // console.log(date);
        postConfirmTheDeal(date, 185).then(
            res => {
                Notifications.show(res.send, "success");
            }
        ).catch(
            err => {
                Notifications.show(err.response.data.error, 'danger');
            }
        );
    };

    cancelTheDeal = () => {
        this.toggle()
        let date = {
            'idAppeal': this.state.idAppeal,
            'businessId': this.state.data.businessId,
            'contractNumber': this.state.data.contractNumber
        };
        postCancelTheDeal(date, 185).then(
            res => {
                Notifications.show(res.send, "success");
                // console.log(res);
            }
        ).catch(
            err => {
                Notifications.show(err.response.data.error, 'danger');
            }
        );
    };

    render() {
        return (
            <Container className='border'>
                <Row >
                    <Col className='headerCol'>
                        {this.props.header}
                    </Col>

                    <Col className='imageCol'>
                        <img className='ecImage' src={this.props.image} alt='electricityCharge' />
                    </Col>
                </Row>
                <Col>
                    <Row>
                        <Col sm="6">
                            <Row >
                                <Col sm='4' className='p-1 mx-1'>
                                    <InputTypeNumber id='contractNumberInput'
                                        title={LM.getString("contractNumber")}
                                        required={true}
                                        ref={objReference => this.contractNumberInput = objReference}
                                    />
                                </Col >
                                <Col sm='4' className='p-1'>
                                    <InputTypeNumber id='transactionNumberInput'
                                        title={LM.getString("transactionNumber")}
                                        required={true}
                                        ref={objReference => this.transactionNumberInput = objReference} />
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Col>
                <Col sm='6'>
                    <Row>
                        <Col sm='4' className='p-1 mx-1'>
                            <InputTypeNumber id='clientIdInput'
                                title={LM.getString("clientId")}
                                required={true}
                                ref={objReference => this.terminalNumberInput = objReference} />
                        </Col>
                        <Col sm='6' className='p-1'>
                            <InputTypePhone  id='phoneNumberInputs'
                                title={LM.getString("supplierTelnumTitle")}
                                required={true}
                                ref={objReference => this.phoneNumberInput = objReference} />
                        </Col>
                    </Row>
                    <Row>
                        <Button className='regularButton' id='electricityChargeButton'
                                onClick={this.chargeCancelClick}>
                            {LM.getString("retrieveDetails")}
                        </Button>
                    </Row>
                </Col>
                <Modal isOpen={this.state.modal} toggle={this.toggle}>
                    <ModalHeader toggle={this.toggle}>{LM.getString("electricityCompany") + " " + LM.getString("retrieveDetails")}</ModalHeader>
                    <ModalBody>
                        {LM.getString('openActionNumber')}: {this.state.idAppeal}
                    </ModalBody>
                    <ModalFooter>
                        <Button className='regularButton' onClick={this.confirmTheDeal}>{LM.getString('confirmTheDeal')}</Button>{' '}
                        <Button color="secondary" onClick={this.cancelTheDeal}>{LM.getString('cancelTheDeal')}</Button>
                    </ModalFooter>
                </Modal>
            </Container >
        )
    }
}
