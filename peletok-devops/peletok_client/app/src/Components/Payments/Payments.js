import React, { Component } from 'react';
import { Button, Container, Row, Col } from 'reactstrap';

import './Payments.css';

import { LanguageManager as LM } from "../LanguageManager/Language"

import InputTypeRadio from '../InputUtils/InputTypeRadio'
import InputTypeSelect from '../InputUtils/InputTypeSelect'
import InputTypeText from '../InputUtils/InputTypeText'
import InputTypeNumber from '../InputUtils/InputTypeNumber';

import InputTypeDate from '../InputUtils/InputTypeDate';
import InputTypeTime from '../InputUtils/InputTypeTime';


/**
 * Payments is a main component of Payments Page.  Payments and its children are working with separate .css file: Payments.css.
 */
class Payments extends Component {
    constructor(props) {
        super(props);

        this.state = {
            /** Payments Headder  */
            paymentsHeadder: LM.getString('payments'),

            /** data for Payments Type element (InputTypeRadio component) */
            paymentsTypeProps: {
                disabled: false,
                options: LM.getString("paymentsType"),
                //  default: LM.getString("paymentsType")[0],
                id: "paymentsType-",
                required: true,
            },
            /** data for Resellers element (InputTypeRadio component) */
            resellersProps: {
                disabled: false,
                options: LM.getString("resellersTypes"),
                //  default: LM.getString("resellersTypes")[0],
                id: "resellers-",
                required: true,
            },

            /** data for Select Reseller element (InputTypeSelect component) */
            selectResellerProps: {
                disabled: false,
                options: ["bla-bla", "bla-bla-bla"],
                default: "bla-bla",
                title: LM.getString("selectReseller"),
                id: "selectReseller",
                required: true,
            },

            /** data for Salesperson element (InputTypeText component) */
            salespersonProps: {
                disabled: false,
                maxLength: 5,
                minLength: 1,
                placeholder: 'Type any TEXT',
                title: LM.getString("salesperson"),
                id: "salesperson",
                required: true,
            },

            /** data for Terminal Number element (InputTypeNumber component) */
            terminalNumberProps: {
                disabled: false,
                minLength: 2,
                maxLength: 4,
                placeholder: 'Type only NUMBERS',
                title: LM.getString("terminalNumber"),
                id: "terminalNumber",
                required: true,
            },

            /** data for Starting Date element (InputTypeDate component) */
            startingDateProps: {
                /** format: YY-MM-DD */
                min: "1990-01-01",
                /** format: YY-MM-DD */
                default: this.nowDateCalc(),
                placeholder: 'Select a Date',
                title: LM.getString("startingDate") + ":",
                id: "paymentsStartingDate",
                required: true,
            },
            /** data for Ending Date element (InputTypeDate component) */
            endingDateProps: {
                /** format: YY-MM-DD */
                min: "1990-01-01",
                /** format: YY-MM-DD */
                default: this.nowDateCalc(),
                placeholder: 'Select a Date',
                title: LM.getString("endingDate") + ":",
                id: "paymentsEndingDate",
                required: true,
            },

            /** data for Starting Time element (InputTypeTime component) */
            startingTimeProps: {
                placeholder: 'Select a Time',
                title: LM.getString("startingTime") + ":",
                id: "paymentsStartingTime",
                required: true,
            },
            /** data for Ending Time element (InputTypeTime component) */
            endingTimeProps: {
                placeholder: 'Select a Time',
                title: LM.getString("endingTime") + ":",
                id: "paymentsEndingTime",
                required: true,
            },

            /** data for Show Report button */
            showReportButton: LM.getString("showReport"),

        }
    }

    /** Show Report Button click handler
     * TODO: Add functionality */
    showReportClick = () => {
    }

    nowDateCalc = () => {
        let nowDate = new Date();
        let nowDateString;
        let month = (nowDate.getMonth() + 1) + "";
        let date = (nowDate.getDate()) + "";

        if (month.length < 2 && date.length === 2) {
            nowDateString = nowDate.getFullYear() + "-" + "0" + month + "-" + date;
        } else if (month.length === 2 && date.length < 2) {
            nowDateString = nowDate.getFullYear() + "-" + month + "-" + "0" + date;
        } else if (month.length < 2 && date.length < 2) {
            nowDateString = nowDate.getFullYear() + "-" + "0" + month + "-" + "0" + date;
        }
        return nowDateString;
    }

    render() {
        return (
            <Container className="paymentsContainer">
                <Row >
                    <Col className="paymentsHeadder">
                        {this.state.paymentsHeadder}
                    </Col>
                </Row>
                <Container className="paymentsSettings">
                    <Row>
                        <Col>
                            <InputTypeRadio {...this.state.paymentsTypeProps} ref={(componentObj) => { this.paymentsType = componentObj }} />
                        </Col>
                    </Row>
                    <Row >
                        <Col >
                            <hr></hr>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <InputTypeRadio {...this.state.resellersProps} ref={(componentObj) => { this.resellers = componentObj }} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="4">
                            <InputTypeSelect {...this.state.selectResellerProps} ref={(componentObj) => { this.selectReseller = componentObj }} />
                        </Col>
                        <Col sm="4">
                            <InputTypeText {...this.state.salespersonProps} ref={(componentObj) => { this.salesperson = componentObj }} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="4">
                            <InputTypeNumber {...this.state.terminalNumberProps} ref={(componentObj) => { this.terminalNumber = componentObj }} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="4">
                            <InputTypeDate {...this.state.startingDateProps} ref={(componentObj) => { this.startingDate = componentObj }} />
                        </Col>
                        <Col sm="4">
                            <InputTypeTime {...this.state.startingTimeProps} ref={(componentObj) => { this.startingTime = componentObj }} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="4">
                            <InputTypeDate {...this.state.endingDateProps} ref={(componentObj) => { this.endingDate = componentObj }} />
                        </Col>
                        <Col sm="4">
                            <InputTypeTime {...this.state.endingTimeProps} ref={(componentObj) => { this.endingTime = componentObj }} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="auto">
                            <Button className="paymentsButton" id="paymentsShowReportButton" onClick={this.showReportClick} > {this.state.showReportButton} </Button>
                        </Col>
                    </Row>
                </Container>
            </Container>
        );

    }

}
export default Payments;