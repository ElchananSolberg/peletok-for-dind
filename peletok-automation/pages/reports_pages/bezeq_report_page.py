from selenium.webdriver.common.by import By


class BezeqReportPage:
    title = (By.XPATH, '//h3[contains(text(),"לבזק")]')
    phone_number_input = (By.ID, "InputTypeNumberphone")
    transaction_number_input = (By.ID, "InputTypeNumbertransaction")
    bezeq_number_input = (By.ID, "InputTypeNumberbezeq")
    first_li = (By.ID, "דוחתשלוםלבזק0")
