const DataTypes = require('sequelize').DataTypes;
const BaseModel = require('../../BaseModel');

/**
 *  Login History model
 */
class LoginHistory extends BaseModel {

    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            timestamp_login: {
                type: DataTypes.DATE,
                allowNull: false
            },
            passport_session: {
                type: DataTypes.STRING(255),
                allowNull: true
            },
            user_ip: {
                type: DataTypes.STRING(255),
                allowNull: true
            }
        };
        this.options["indexes"] = [{
            unique: false,
            fields: ['user_id']
        }];
        this.prefix = 'USR';
        this.author = false;
        this.statusField = false;
        this.statusAuthor = false;
        this.statusTimestampField = false;
    }
    /**
     * creates associate for Login History model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.user.model, { as: 'User'});
    }

}

module.exports = new LoginHistory();