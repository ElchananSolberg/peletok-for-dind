from selenium.webdriver.common.by import By


class TransactionsPage:
    transaction_status_select = (By.ID, "InputTypeSearchListtransactionsStatus")
    number_of_transaction_input = (By.ID, "InputTypeNumbertransactionNumber")


class PaymentPage:
    receipt_no_input = (By.ID, "InputTypeNumberreceiptNumber")


class ActionsPage:
    tags_select = (By.ID, 'InputTypeSearchListtags')


class BalanceByDatePage:
    date_input = (By.ID, 'InputTypeDatedate')
    time_in_minuts_input = (By.ID, 'InputTypeTimeMinutetime')
    time_in_hours_input = (By.ID, 'InputTypeTimeHourtime')
    show_reports_btn = (By.ID, 'showReport')
