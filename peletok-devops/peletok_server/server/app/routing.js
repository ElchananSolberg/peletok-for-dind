var express = require('express');
var path = require('path');

var app = express();
var resetPassRouter = require('./user_management/resetPasswordAPI')
var userManagementRouter = require('./user_management/UserAuthAPI');
const {adminRouter} = require("./adminApi");
const {barcodeTypeRouter} = require("./restAPI");
var {userRouter, userDocumentRouter} = require('./user_management/userAPI');
var {isAuthMiddleware} = require('./user_management/userAPI');
let {invoiceRouter} = require('./api/invoiceRouter')
var {paymentRouter, giftRouter,businessRouter, cityPayRouter, bannerRouter,distributorRouter,tagRouter, hashavshevetRouter,businessProductRouter, profiteRouter, commissionRouter} = require('./restAPI');
const productRouterOld = require('./restAPI').productRouter;
const supplierRouterOld = require('./restAPI').supplierRouter;
var {electricityCompanyRouter} = require('./rest/electricityCompany');
var {fakeUserRouter} = require('./rest/fakeUserApi');
const {reportsRouter} = require('./rest/reportsApi.js')
const {permissoinsRouter} = require('././api/permissionsApi')
const {permissoinsRoleRouter} = require('././api/permissionRolesApi')
const {configRouter} = require('././api/configRouter')
const {creditCardRouter} = require('././api/creditCardRouter')


const {activitySessionRouter} = require('./api/activitySessionRouter')

const {rolesRouter} = require('././api/rolesApi')
const {ravKavRouter} = require('././api/ravKavRouter')
const {transactionRouter} = require('./api/transactionRouter')
const {productRouter} = require('./api/productRouter')
const {supplierRouter} = require('./api/supplierRouter')
const {messageRouter, messageManagementRouter, messageTagRouter} = require("./message/message_api");
var dir = path.join(__dirname, 'rest/restData/public/');

app.response.customSend = function (info, useOriginalSend = false) {
    //TODO: Use hear in LM
    //TODO: Remove duplicate Info classes
    const {Info} = require("./mapping/crudMiddleware");
    if (useOriginalSend || !(info instanceof Info)) {
        return this.send(info);
    }
    const logger = require("./logger");
    if (info.errors[0]) {
        logger.error(info);
        return this.status(400).send(info.errors.map(e=>e.message));
    } else {
        logger.info(info);
        if(typeof info.result==='number'){
            info.result+='';
        }
        return this.status(200).send(info.message && info.message !== '' ? info.message : info.result||"success")
    }
};
app.use(express.static(dir));
// THIS ROUTER SHOULD NOT BE UNDER isAuthMiddleware
app.use('/auth/', userManagementRouter);
app.use('/reset/', resetPassRouter);

app.use(isAuthMiddleware);
app.use('/user/', userRouter, fakeUserRouter);
app.use('/user_doc/', userDocumentRouter);
app.use('/electricity/', electricityCompanyRouter);
app.use('/supplier/', supplierRouter, supplierRouterOld);
app.use('/product/', productRouter , productRouterOld);
app.use('/config/', configRouter);
app.use('/report/', reportsRouter);
app.use('/payment/', paymentRouter);
app.use('/business/', businessRouter);
app.use('/gift/', giftRouter);
app.use('/city_pay/', cityPayRouter);
app.use('/distributor/', distributorRouter);
app.use('/barcode_type/', barcodeTypeRouter);
app.use('/banner/', bannerRouter);
app.use('/tag/', tagRouter);
app.use('/hashavshevet/', hashavshevetRouter);
app.use('/transaction/', transactionRouter);
app.use('/activity_session/', activitySessionRouter);
app.use('/admin/', adminRouter);
app.use('/permissions/', permissoinsRouter);
app.use('/permission_roles/', permissoinsRoleRouter);
app.use('/roles/', rolesRouter);
app.use('/rav_kav/', ravKavRouter);
app.use('/invoice/', invoiceRouter);
app.use('/business_product/', businessProductRouter);
app.use('/commission/', commissionRouter);
app.use('/profit/', profiteRouter);
app.use('/credit_card/', creditCardRouter);
app.use('/message/', messageRouter, messageTagRouter);
app.use('admin/message/', messageManagementRouter);

module.exports = app;