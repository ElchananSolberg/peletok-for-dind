import React, {Component} from 'react';
import {Badge, Col, Collapse, Input, Pagination, PaginationItem, PaginationLink, Row} from 'reactstrap';

import '../InputUtils/InputUtils.css';
import './report.css'

import {LanguageManager as LM} from '../LanguageManager/Language'

import ReportPaginationItem from './ReportPaginationItem';
import {ReportTableContent} from "./ReportTableContent";
import {Notifications} from "../Notifications/Notifications";
import {getReportRes} from "../DataProvider/Functions/DataGeter";
import {Loader} from "../Loader/Loader";


const MAX_PAGINATION_LINKS_TO_DISPLAY = 9;
const DEFAULT_ROWS_PER_PAGE = 10;
const ROWS_PER_PAGE_OPTIONS = [DEFAULT_ROWS_PER_PAGE, 15, 25, 50, 100];


class ReportPagination extends Component {
    constructor(props) {
        super(props);
        this.pageAmount = 0;
        this.state = {
            tableRows: [],
            tableHeadersObj: {},
            tableFootersObj: {},
            openReport: true,
            reportDate: "",
            activeIndex: 0,
            data: [],
            rowsPerPage:  this.props.defaultRowsPerPage||DEFAULT_ROWS_PER_PAGE
        };
        this.isLoaded = false;
        let max = this.getMaxToDisplay();
        this.maxToDisplayBefore = max.maxToDisplayBefore;
        this.maxToDisplayAfter = max.maxToDisplayAfter;

        this.reportSumStructure = {};
    }


    /**
     * Returns number of pages to display before current page and  number of pages to display after current page
     * @returns {{amountDisplayAfter: number, amountDisplayBefore: number}}
     */
    getAmountDisplay = () => {
        const lastPageIndex = this.pageAmount - 1; // backus index start from 0
        let amountDisplayBefore = this.maxToDisplayBefore;
        let amountDisplayAfter = this.maxToDisplayAfter;
        const amountPagesAfter = lastPageIndex - this.state.activeIndex;
        if (this.maxToDisplayAfter > amountPagesAfter) { // not need use max after
            amountDisplayBefore += this.maxToDisplayAfter - amountPagesAfter
        }
        const amountPagesBefore = this.state.activeIndex;
        if (this.maxToDisplayBefore > amountPagesBefore) { // not need use max before
            amountDisplayAfter += this.maxToDisplayBefore - amountPagesBefore
        }
        return {amountDisplayBefore: amountDisplayBefore, amountDisplayAfter: amountDisplayAfter};

    };

    /***
     * Returns maximum pages to display before current page and  maximum pages to display after current page
     * @returns {{maxToDisplayAfter: number, maxToDisplayBefore: number}}
     */
    getMaxToDisplay() {
        const maxToDisplay = MAX_PAGINATION_LINKS_TO_DISPLAY - 1; // maxToDisplay without activeIndex self
        let maxToDisplayBefore = Math.floor(maxToDisplay / 2); // maxToDisplay before activeIndex
        let maxToDisplayAfter = Math.floor(maxToDisplay / 2); // maxToDisplay before activeIndex
        if (maxToDisplay % 2 !== 0) {
            maxToDisplayAfter++;
        }
        return {maxToDisplayBefore, maxToDisplayAfter};
    }

    /**
     * Sets state where no data result from server
     * @param activeIndexAfterClick {number}
     * @returns {function(...[*]=)}
     */
    noData = (activeIndexAfterClick) => {
        return () => {
            this.setState({
                tableRows: [],
                tableHeadersObj: {},
                tableFootersObj: {},
                openReport: false,
                reportDate: "",
                activeIndex: activeIndexAfterClick
            });
            Notifications.show(LM.getString("noReportErrMsg"), 'danger')
        }
    };

    /**
     * Loads report data (for specific page) from server
     * @param offset {number} rows to skip
     * @param activeIndexAfterClick {number} new active index
     * @returns {function(...[*]=)}
     */
    loadData(offset, activeIndexAfterClick) {
        return () => {
            this.setState({activeIndex: activeIndexAfterClick});
            Loader.show();
            const params = {...this.props.params} || {};
            params.limit = this.state.rowsPerPage;
            params.offset = offset;
            // default method for get report data is getReportRes
            const getReport = this.props.getReportMethd||getReportRes;
            getReport(this.props.reportID, {params: params}).then(res => {
                Loader.hide();
                if (res === 'No reports found') {
                    this.noData(activeIndexAfterClick);
                } else {
                    let headers = {};
                    Object.values(res['headers']).map((value) => {
                        return headers[value] = LM.getStringWithoutColon(value)
                    });
                    const printable = this.reportSumStructure.is_printable;
                    if (printable) {
                        headers['print'] = (LM.getString('print'));
                    }
                    const cancelable = this.reportSumStructure.is_cancelable;
                    if (cancelable) {
                        headers['mailToCancel'] = (LM.getString('mailToCancel'));
                    }
                    const oldRows = res['data'];
                    Object.values(oldRows).map((key) => {
                        if (printable) {
                            key['print'] = LM.getString('print');
                        }
                        if (cancelable) {
                            key['mailToCancel'] = LM.getString('mailToCancel');
                        }
                        return oldRows
                    });
                    let tableFootersObj = null;
                    let sumProfitObj = null;
                    let sumPaymentObj = null;
                    let currentBalanceObj = null;
                    const firstColumn = Object.keys(headers)[0];
                    if (activeIndexAfterClick + 1 === this.pageAmount) {
                        const sumBusinessPriceIsDisplayed = this.reportSumStructure.sum_business_price_is_displayed;
                        const sumConsumerPriceIsDisplayed = this.reportSumStructure.sum_consumer_price_is_displayed;
                        if (sumBusinessPriceIsDisplayed || sumConsumerPriceIsDisplayed) {
                            tableFootersObj = {
                                [firstColumn]: LM.getString('total'),
                                business_price: sumBusinessPriceIsDisplayed ? this.props.sumBusinessPrice : '',
                                consumer_price: sumConsumerPriceIsDisplayed ? this.props.sumConsumerPrice : ''
                            }
                        }
                        if (this.reportSumStructure.sum_profit_is_displayed) {
                            sumProfitObj = {
                                [firstColumn]: LM.getString('sum_profit'),
                                business_price: this.props.sumProfit
                            }
                        }
                        if (this.reportSumStructure.sum_payment_is_displayed) {
                            sumPaymentObj = {
                                [firstColumn]: LM.getString('total'),
                                payment: this.props.sumPayment
                            }
                        }
                        if (this.reportSumStructure.current_balance_is_displayed) {
                            currentBalanceObj = {
                                [firstColumn]: LM.getString('currentBalance'),
                                business_price: this.props.currentBalance
                            }
                        }
                    }
                    this.setState({
                            sumObj: tableFootersObj,
                            sumProfitObj: sumProfitObj,
                            sumPaymentObj: sumPaymentObj,
                            currentBalanceObj: currentBalanceObj,
                            tableHeadersObj: headers,
                            tableRows: oldRows,
                            reportDate: params["startDate"] + " - " + params["endDate"],
                            activeIndex: activeIndexAfterClick,
                            openReport: true
                        }
                    );
                    Loader.hide();
                    return oldRows;
                }
            })
        }
    };

    /**
     * Sets rows per page
     * @param e
     */
    setRowsPerPage = (e) => {
        this.isLoaded = false; // Reload first page
        this.setState({rowsPerPage: e.target.value})
    };

    render() {
        this.reportSumStructure = this.props.reportData||{}; // default not display any sum
        if (!this.props.reportLength) {
            return (<div/>)
        }
        let {id, reportPaginationItems} = this.getPaginationDetails();
        let diaplayDatesHeader = this.props.diaplayDatesHeader!==false;  // default true
        return (
            <React.Fragment>
                <Collapse isOpen={this.state.openReport}>
                    {diaplayDatesHeader&&this.getReportHeaders()}
                    <ReportTableContent reportName={this.state.reportName} tableRows={this.state.tableRows}
                                        tableHeadersObj={this.state.tableHeadersObj}
                                        sumObj={this.state.sumObj}
                                        sumProfitObj={this.state.sumProfitObj}
                                        sumPaymentObj={this.state.sumPaymentObj}
                                        currentBalanceObj={this.state.currentBalanceObj}
                                        useReportExport={false}
                    />
                    {this.pageAmount>1&&this.getRow(id, reportPaginationItems)}
                </Collapse>
            </React.Fragment>

        )
    }

    getReportHeaders = () => {
        return <h1>
            <Badge id='dateOfReport' color="primary"
                   className={'border-radius-3 btn btn-lg btn-md-block btn-secondary font-weight-bolder' +
                   ' fs-17 px-4 regularButton'}>
                {LM.getString("dateOfReport")}<br/>
                {this.state.reportDate}
            </Badge>
            {" "}
            {(this.props.sumConsumerPrice !== null &&
                this.props.sumConsumerPrice !== undefined && this.props.sumConsumerPrice !== 'NaN' &&
                this.reportSumStructure.sum_consumer_price_is_displayed_in_head) ?
                <Badge id='moneySymbol' color="primary"
                       className={'border-radius-3 btn btn-lg btn-md-block btn-secondary font-weight-bolder' +
                       ' fs-17 px-4 regularButton'}>
                    {LM.getString("sumOfProfit")}<br/>
                    {this.props.sumConsumerPrice + LM.getString('moneySymbol')}
                </Badge> : <div/>

            }
            <div/>
        </h1>;
    };

    getPaginationDetails() {
        const x = this.props.reportLength / this.state.rowsPerPage;
        this.pageAmount = Math.floor(x);
        if (x > this.pageAmount) this.pageAmount++;
        let id = this.props.id ? this.props.id.replace(/\s+/g, '') : this.state.reportName;
        let reportPaginationItems = [];
        let {amountDisplayBefore, amountDisplayAfter} = this.getAmountDisplay();
        const indexOfFirstDisplay = Math.max(0, this.state.activeIndex - amountDisplayBefore);
        const indexOfLastDisplay = Math.min(this.pageAmount - 1, this.state.activeIndex + amountDisplayAfter);
        for (let i = indexOfFirstDisplay; i <= indexOfLastDisplay; i++) {
            reportPaginationItems.push(<ReportPaginationItem activeIndex={this.state.activeIndex}
                                                             key={i} index={i} id={id}
                                                             onClick={this.loadData((i) * this.state.rowsPerPage, i)}
            />)
        }
        if (!this.isLoaded) {
            this.loadData(0, 0)();
            this.isLoaded = true;
        }
        return {id, reportPaginationItems};
    }

    getRow(id, reportPaginationItems) {
        return <div className={'d-flex justify-content-between mt-3'}>
            <div>
                <Pagination aria-label="Pages navigation">
                    <React.Fragment>
                        <PaginationItem><PaginationLink first id={id + 'Start'}
                                                        onClick={this.loadData(0, 0)}
                                                        className={(LM.getDirection() === "rtl" ? "rtl" : "")}> {LM.getString('start')}
                        </PaginationLink></PaginationItem>

                        <PaginationItem hidden={this.state.activeIndex <= 0}>
                            <PaginationLink previous id={id + 'Previous'}
                                            onClick={this.loadData((this.state.activeIndex - 1) * this.state.rowsPerPage, this.state.activeIndex - 1)}
                            /></PaginationItem>

                        {reportPaginationItems}
                        <PaginationItem hidden={this.state.activeIndex >= this.pageAmount - 1}>
                            <PaginationLink next id={id + 'Next'}
                                            onClick={this.loadData((this.state.activeIndex + 1) * this.state.rowsPerPage, this.state.activeIndex + 1)}/>
                        </PaginationItem>

                        <PaginationItem>
                            <PaginationLink last id={id + 'End'}
                                            onClick={this.loadData((this.pageAmount - 1) * this.state.rowsPerPage, this.pageAmount - 1)}
                                            className={(LM.getDirection() === "rtl" ? "rtl" : "")}> {LM.getString('end')} </PaginationLink>
                        </PaginationItem>

                    </React.Fragment>
                </Pagination>
            </div>
            <div className={'d-flex'}>


                <p className={'m-2'}>{LM.getString("rowsPerPaper")}</p>
                <div>
                    <Input
                        value={this.state.rowsPerPage}
                        type="select"
                        id={"rowsPerPage" + this.state.reportName}
                        onChange={this.setRowsPerPage}
                        defaultValue={this.state.rowsPerPage}
                        className={(LM.getDirection() === "rtl" ? "rtl height-select" : "height-select")}
                    >{
                        ROWS_PER_PAGE_OPTIONS.map((current, index) => {
                            return <option key={index}> {current} </option>
                        })
                    }</Input>
                </div>
            </div>
        </div>;
    }
}

export default ReportPagination;