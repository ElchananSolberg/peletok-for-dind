const sequelizeInstance = require("../setupSequelize");
const sequelize = require("sequelize");

/**
 * Copies permission from one supplier to other supplier
 * @param user {Object} connected user
 * @param from {number} supplier id
 * @param to {number} supplier id
 * @param info {Object} info object for saving result and errors
 * @return {Promise<void>}
 */
async function copySupplierPermission(user, from, to, info) {
    let author = user.id;
    let db = sequelizeInstance.config.database;
    let now = new Date().toISOString();
    const query = `INSERT into peletok.public."SUPL_BusinessSupplier" (status, created_at, updated_at, business_id, supplier_id, author_id, status_author_id) select status, '${now}', '${now}', business_id, :to, :author, :status_author from peletok.public."SUPL_BusinessSupplier" where supplier_id=:from`;
    const replacements = {
        replacements: {to: to, from: from, author: author, status_author: author},
        type: sequelize.QueryTypes.SELECT
    };
    await sequelizeInstance.query(query, replacements).catch(e => {
        info.errors.push(e);
    });

}

module.exports.copySupplierPermission = copySupplierPermission;