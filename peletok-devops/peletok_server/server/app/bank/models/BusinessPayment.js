const DataTypes = require('sequelize').DataTypes;
const BaseModel = require('../../BaseModel');
/**
 * BusinessPayment model
 * this model is to keep the records of the payments that the business paid to Peletok
 */
class BusinessPayment extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            date: {
                type: DataTypes.DATE,
                allowNull: false
            },
            hour: {
                type: DataTypes.STRING,
                allowNull: false
            },
            sum: {
                type: DataTypes.DOUBLE,
                allowNull: false
            },
            invoice_number: {
                type: DataTypes.STRING,
                allowNull: true
            },
            business_credit: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            is_credit_from_supplier: {
                type: DataTypes.BOOLEAN,
                defaultValue: false
            },
            approver_id: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            note: {
                type: DataTypes.STRING,
                allowNull: true
            }

        };

        this.author = true;
        this.prefix = 'BNK';

    }
    /**
     * creates associate for BusinessPayment model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.business.model, { as: 'Business' });
        this.model.belongsTo(models.hashavshevetImport.model, { as: 'HashavshevetImport' });

    }
}
// exporting instance
module.exports = new BusinessPayment();