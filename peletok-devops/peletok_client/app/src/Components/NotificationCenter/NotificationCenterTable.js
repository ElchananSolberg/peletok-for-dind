import React, {Component} from 'react';
import {Container, Row, Col} from 'reactstrap';

import './NotificationCenter.css';

import {LanguageManager as LM} from "../LanguageManager/Language"

import NotificationCenterTableRow from './NotificationCenterTableRow'


/**
 * NotificationCenterTable component renders the table with headers with use of reactstrap Layout Components.
 */
class NotificationCenterTable extends Component {
    constructor(props) {
        super(props);

        /** 'subject' key working variable */
        this.state = {
            /** working variable for Rows component  that changes due to imagesColMouseEnter and imagesColMouseLeave functions - handlers of onMouseEnter and onMouseLeave
             * events of the Col with images
             */
            pointerEvents: 'auto',
        }
    }

    /** <Col> with images onMouseEnter handler */
    imagesColMouseEnter = (e) => {
        e.persist();
        this.setState({pointerEvents: 'none'});
    }
    /** <Col> with images onMouseLeave handler */
    imagesColMouseLeave = (e) => {
        e.persist();
        this.setState({pointerEvents: 'auto'});
    }

    render() {
        /** defines the xs attribute of the Col that containes the subject key */
        var subjectRowXS = 4;

        var mapKey = 0;
        var tableHeadersArray = Object.keys(this.props.tableHeadersObj).map((key) => {
            if (key === "subject") {
                return <Col xs={subjectRowXS} key={mapKey++}><b id={'header-'+key} >{this.props.tableHeadersObj[key]}</b></Col>
            }
            return <Col key={mapKey++}><b id={'header-'+key}>{this.props.tableHeadersObj[key]}</b></Col>
        });

        var tableRowsMapKey = 0;

        var tabValue = this.props.tabValue;
        var tableRows = "";
        tableRows = [...this.props.tableRows];
        if (tabValue === "Favorites") {
            tableRows = this.props.tableRows.filter(current => current.images.favorite);
        }
        if (tabValue === "Unread") {
            tableRows = this.props.tableRows.filter(current => current.images.unread);
        }
        if (tabValue === "Cancellations") {
            tableRows = this.props.tableRows.filter(current => current.cancellation);
        }
        if (tabValue === "Credits") {
            tableRows = this.props.tableRows.filter(current => current.credit);
        }
        if (tabValue === "GiftOrders") {
            tableRows = this.props.tableRows.filter(current => current.giftOrder);
        }
        const colToDisplay = ["senderName", "subject", "date", "time"];
        var tableRowsRendered = tableRows.map((current, index) => {
            let fullMessage=current.fullMessage;
            let subject = current.subject;
            var columns = colToDisplay.map((key) => {
                if (key === "subject") {
                    return <Col className={'white-space-nowrap'} xs={subjectRowXS} key={tableRowsMapKey++}>
                        {current.images.unread ? <b id={key+'-'+current.messageId}>{current[key]}</b> : current[key]}
                    </Col>
                }
                if (key === "senderName") {
                    return <Col className={'white-space-nowrap'} xs='2' key={tableRowsMapKey++}> <b id={key+'-'+current.messageId}>{current[key]}</b> </Col>
                }
                else return <Col xs='2' key={tableRowsMapKey++}id={key+'-'+current.messageId}>{current[key]}</Col>
            });

            return <NotificationCenterTableRow pointerEvents={this.state.pointerEvents} key={current.messageId}
                                               messageId={current.messageId} subject={subject} fullMessage={fullMessage}
                                               columns={columns}
                                               viewMessageWindow={this.props.viewMessageWindow}
                                               typeMessageWindow={this.props.typeMessageWindow}
                                               favorite={current.images.favorite} unread={current.images.unread}
                                               senderId={current.senderId}
                                               updateMessageReadStatus={this.props.updateMessageReadStatus}
                                               updateMessageFavoriteStatus={this.props.updateMessageFavoriteStatus}
                                               deleteRow={this.props.deleteRow}
            />
        });

        return (
            <>
                <Container>
                    <Row className="notificationCenterTableHeaders" id={"notificationCenterTableHeaders"}>
                        {tableHeadersArray}
                    </Row>
                </Container>
                <Container className="notificationCenterTableScrolling" id={"notificationCenterTable"}>
                    {tableRowsRendered}
                </Container>
            </>
        );
    }
}

export default NotificationCenterTable;

