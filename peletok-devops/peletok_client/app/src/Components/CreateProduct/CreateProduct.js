import React, { Component } from 'react';
import './CreateProduct.css';

import { Button, Collapse, Container, Row, Col } from 'reactstrap';

import { LanguageManager as LM } from '../LanguageManager/Language'

import {
    getProductTags,
    getSuppliers,
    getProducts,
    putProduct,
    postProduct,
    getProductDetails,
    getBarcodeTypes
} from '../DataProvider/DataProvider'

import { config } from '../../utils/Config'

import InputTypeSearchList from '../InputUtils/InputTypeSearchList'
import InputTypeRadio from '../InputUtils/InputTypeRadio'
import InputTypeText from '../InputUtils/InputTypeText'
import ImageUpload from '../InputUtils/ImageUpload'
import Tag from '../InputUtils/Tag';
import InputTypeNumber from '../InputUtils/InputTypeNumber';
import InputTypeSelect from '../InputUtils/InputTypeSelect';
import InputTypeCheckBox from '../InputUtils/InputTypeCheckBox';
import InputTypeSimpleSelect from '../InputUtils/InputTypeSimpleSelect';
import Notice from '../InputUtils/Notice';
import InputTypeFile from '../InputUtils/InputTypeFile';

import whiteBlock from '../InputUtils/white_block.png';
import TagsField from '../TagsField/TagsField';

import CatalogNumberTable from './CatalogNumberTable';
import { MoreInfo } from '../MoreInfo/MoreInfo'
import { ExcelUtils } from '../../utils/ExcelUtils'
import { FileUtiles } from '../../utils/FileUtiles'
import { Confirm } from '../Confirm/Confirm'
import { Notifications } from '../Notifications/Notifications'
import { Loader } from '../Loader/Loader'
import { objectToFormData } from '../../utilities/utilities'
import { AuthService } from '../../services/authService'
import { ProductStatusEnum } from '../../enums/'

/**
 * Main component of Products Page.  CreateProduct and its children are working with separate .css file: CreateProduct.css.
 */
class CreateProduct extends Component {
    constructor(props) {
        super(props);

        this.state = {
            render: true,

            renderAllFields: true,

            editMode: false,
            // If edited is true the user should confirm before he moves to next item
            edited: false,

            disabled: true,
            /** working variable that will contain an array of suppliers from the server */
            suppliers: [],
            /** working variable that will contain a selected supplier id */
            currentSupplierID: -1,
            /** working variable that will contain an object (object of arrays of objects) of all the selected supplier products */
            currentSupplierProducts: [],
            /** working variable that will contain a selected product object */
            selectedProduct: {},
            /** working variable that contain end of a selected service radio button id */
            selectedServiceId: '0',
            /** working variable that indicates if a user is on the stage of creating a new product */
            creatingNewState: false,
            /** working variable that will contain alternative product (to the selected product) object */
            alternativeProduct: {},
            /** working variable that indicates if a user has clicked on a Show More Info checkbox */
            showMoreInfo: false,

            previewMoreInfo: false,

            barcodeTypes: [],

            tags: [],

            upscaleProducts: []
        }

        this.loadSuppliersList = this.loadSuppliersList.bind(this);
        /** working variable that will contain an object of all the input fields (on the CreateProduct screen), which are getting data from the selectedProduct object (from the server ), and sending data to the server */
        this.fields = {};
        this.selectProductTypeField = null
        this.selectService = null
        this.editBtn = null
    }

    /** Array of TalkPrices arraive form xlsx file  */
    talk_prices = null
    moreInfoFileDataUrl = null

    moreInfoPrepareProductFromField = () => {
        if (Object.keys(this.fields).length == 0) {
            return {};
        }
        return {
            name_he: this.fields['name_he'].getValue().value,
            name_en: this.fields['name_en'].getValue().value,
            name_ar: this.fields['name_ar'].getValue().value,
            price: this.fields['price'].getValue().value,
            more_info_description: this.fields['more_info_description'] ? this.fields['more_info_description'].getValue().value : '',
            more_info_image: this.fields['more_info_image'] ? this.fields['more_info_image'].state.imagePreviewUrl : '',
            more_info_youtube_url: this.fields['more_info_youtube_url'] ? this.fields['more_info_youtube_url'].getValue().value : '',
            more_info_site_url: this.fields['more_info_site_url'] ? this.fields['more_info_site_url'].getValue().value : '',
            call_terms: this.fields['call_terms'] ? this.fields['call_terms'].getValue().value : '',
            call_minute: this.fields['call_minute'] ? this.fields['call_minute'].getValue().value : '',
            sms: this.fields['sms'] ? this.fields['sms'].getValue().value : '',
            browsing_package: this.fields['browsing_package'] ? this.fields['browsing_package'].getValue().value : '',
            call_to_palestinian: this.fields['call_to_palestinian'] ? this.fields['call_to_palestinian'].getValue().value : '',
            abroed_price: this.fields['abroed_price'] ? this.fields['abroed_price'].getValue().value : '',
            other1: this.fields['other1'] ? this.fields['other1'].getValue().value : '',
            other2: this.fields['other2'] ? this.fields['other2'].getValue().value : '',
            talk_prices: this.talk_prices || this.state.selectedProduct.talk_prices || []
        }
    }


    /** Function that opens the GET html request (getSuppliers from DataProvider).
     *  Saves an array of suppliers from the server into suppliers variable. */
    loadSuppliersList(forceUpdate = false) {
        getSuppliers(forceUpdate).then(
            (res) => {
                this.setState({ suppliers: Object.keys(res).reduce((newArr, key) => [...newArr, ...res[key]], []) })
            }
        ).catch(
            (err) => {
                alert("loadSuppliersList failed: " + err)

            }
        )
    }

    /** "Select Supplier" field onChange handler.
     * Saves the selected supplier id into currentSupplierID variable.
     * Opens the GET html request (getProducts from DataProvider).
     * Saves an object from the server (list of products of the selected supplier) into products variable.
     * At the end it clears a value of the 'Select Product' field and calls the clearFields() function. */
    supplierChange = (e, newProductId = false) => {
        this.state.creatingNewState ? this.clearFields() : this.initComponentState();
        this.setState({ currentSupplierID: e.value }, () => {
            getProducts(this.state.currentSupplierID, {}, true).then(
                (res) => {
                    this.setState({
                        currentSupplierProducts: {
                            manual: res.manual ? res.manual.map(p => p.name ? p : Object.assign(p, { name: p.name_he })) : [],
                            virtual: res.virtual ? res.virtual.map(p => p.name ? p : Object.assign(p, { name: p.name_he })) : [],
                            bills_payment: res.bills_payment ? res.bills_payment.map(p => p.name ? p : Object.assign(p, { name: p.name_he })) : [],
                        }
                    }, () => {
                        if (newProductId) {
                            this.productChange({ value: newProductId }, true);
                        }
                    }
                    )
                    // this.selectProduct.setValue('')
                    // this.clearFields(false);

                }).catch(
                    (err) => {
                        Notifications.show(err);
                    }
                )
        })
    }

    /** "Select Product" field onChange handler
     * Due to the selected product name it saves selected product object into selectedProduct variable.
     * At the end it calls fieldsLoad() function. */
    productChange = (e, shouldSelectNewProduct = false) => {
        if (shouldSelectNewProduct) {
            this.selectProduct.setValue(e.value);
        }
        if (e.value) {
            getProductDetails(e.value).then(
                res => {
                    this.setState({
                        selectedProduct: res,
                        renderAllFields: false,
                        edited: false,
                        upscaleProducts: this.filteredProducts(res)
                    },
                        () => {
                            this.setState({
                                renderAllFields: true
                            },
                                () => this.fieldsLoad(false)
                            )
                        }
                    );
                }
            )
        }

    }

    /** "Select Service" field onChange handler
     * It saves the end of a selected service radio button id into selectedServiceId variable.
     * At the end it clears a value of the 'Select Product' field and calls the clearFields() function.*/
    serviceChange = (e) => {
        let activeId = e.target.id.split("-")[1]
        this.setState({ selectedServiceId: activeId });
        this.selectProduct.setValue('')
        this.clearFields(false);
    }

    selectProductType = (e) => {
        this.productType = parseInt(e.target.id.split("-")[1]) + 1;
        this.setState({ selectedServiceId: this.productType + "", renderAllFields: false }, () => {
            if (this.state.creatingNewState && this.state.currentSupplierID && Object.keys(this.state.currentSupplierProducts).length !== 0) {
                let products = this.productsLoad()
                this.setState({ upscaleProducts: products, renderAllFields: true })
            }
        })

    }

    /** "Alternative Product" field onChange handler
     * It saves the selected alternative product into alternativeProduct variable.
     */
    alternativeProductChange = (e) => {
        let value = this.fields["upscale_product_id"].getValue().value;
        let virtualFind = this.state.currentSupplierProducts.virtual ? this.state.currentSupplierProducts.virtual.find(element => (element.name === value)) : undefined;
        let manualFind = this.state.currentSupplierProducts.manual ? this.state.currentSupplierProducts.manual.find(element => (element.name === value)) : undefined;
        let billsPaymentFind = this.state.currentSupplierProducts.bills_payment ? this.state.currentSupplierProducts.bills_payment.find(element => (element.name === value)) : undefined;
        let selectedAlternativeProduct = virtualFind || manualFind || billsPaymentFind;
        if (selectedAlternativeProduct) {
            this.setState({ alternativeProduct: selectedAlternativeProduct });
        }
    }

    /** Function that prepares (according to the selected service) a list of product names for the "Select Product" and the "Alternative Product" fields */
    productsLoad = () => {
        if (Object.keys(this.state.currentSupplierProducts).length !== 0) {
            let virtualProduct = this.state.currentSupplierProducts.virtual ? this.state.currentSupplierProducts.virtual.map(p => {
                return { label: p.name, value: p.id, id: p.id, type: p.type }
            }) : [];
            let manualProduct = this.state.currentSupplierProducts.manual ? this.state.currentSupplierProducts.manual.map(p => {
                return { label: p.name, value: p.id, id: p.id, type: p.type }
            }) : [];
            let billsPayment = this.state.currentSupplierProducts.bills_payment ? this.state.currentSupplierProducts.bills_payment.map(p => {
                return { label: p.name, value: p.id, id: p.id, type: p.type }
            }) : [];
            if (this.state.selectedServiceId === '0') {
                return virtualProduct.concat(manualProduct, billsPayment);
            }
            if (this.state.selectedServiceId === '2') {
                return manualProduct;
            }
            if (this.state.selectedServiceId === '1') {
                return virtualProduct;
            }
            if (this.state.selectedServiceId === '3') {
                return billsPayment
            }
        }
        return false
    }

    filteredProducts = (product) => {
        return (this.productsLoad() || []).filter(p => p.type == product.type && p.id != product.id)
    }

    /** Function that loads a data from the selected product object into the this.fields object. */
    fieldsLoad = () => {
        Object.keys(this.fields).forEach((key) => {
            if (key === "image" || key === 'more_info_image') {
                if (this.state.selectedProduct[key] === null || this.state.selectedProduct[key] === undefined) {
                } else {
                    this.fields[key].setValue(config.baseUrl + this.state.selectedProduct[key]);
                }
            } else if (key === "more_info_image") {
                if (this.state.selectedProduct[key] === null || this.state.selectedProduct[key] === undefined) {
                } else {
                    this.fields[key].setValue(config.baseUrl + this.state.selectedProduct[key]);
                }
            }
            else if (this.fields[key].constructor.name == 'InputTypeCheckBox') {
                this.fields[key].setValue(this.state.selectedProduct[key] == ProductStatusEnum.ACTIVE);
            }

            else {
                if (this.state.selectedProduct[key] === null || this.state.selectedProduct[key] === undefined) {
                    if (key === "upscale_product_id") {
                        this.fields[key].setValue("");
                        this.setState({ alternativeProduct: {} });
                    }
                } else {
                    if (key === "upscale_product_id") {
                        let virtualFind = this.state.currentSupplierProducts.virtual ? this.state.currentSupplierProducts.virtual.find(element => (element.id === this.state.selectedProduct[key])) : undefined;
                        let manualFind = this.state.currentSupplierProducts.manual ? this.state.currentSupplierProducts.manual.find(element => (element.id === this.state.selectedProduct[key])) : undefined;
                        let billsPaymentFind = this.state.currentSupplierProducts.bills_payment ? this.state.currentSupplierProducts.bills_payment.find(element => (element.id === this.state.selectedProduct[key])) : undefined;
                        let alternativeProduct = virtualFind || manualFind || billsPaymentFind;
                        if (alternativeProduct) {
                            this.fields[key].setValue(alternativeProduct.name);
                            this.setState({ alternativeProduct: alternativeProduct });
                        }
                    } else {
                        this.fields[key].setValue(this.state.selectedProduct[key]);
                    }
                }
            }
        });

    }


    /** Button "Preview" onClick handler. */
    previewClicked = (e) => {
        this.setState({
            previewMoreInfo: !this.state.previewMoreInfo
        })
    }

    /** Function that clears a data from the selected product object and the alternative product object and the this.fields object. */
    clearFields = (withRender = true) => {
        this.setState({
            selectedProduct: {},
            render: !withRender
        }, () => {
            this.setState({
                render: true
            })
            Object.keys(this.fields).forEach((key) => {

                if (key === "image") {
                    this.fields[key].setValue(whiteBlock);
                } else if (key == "barcodes") {
                    this.fields[key].setValue([]);
                } else if (key == "tags") {
                    this.fields[key].setValue([]);
                } else {
                    if (this.fields[key].getValue().value === true) {
                        this.fields[key].setValue(false)
                    } else this.fields[key].setValue("");
                }
            })
        });
        this.selectProductTypeField = null
        this.productType = null
        this.setState({ alternativeProduct: {} });
    }

    edit = () => {
        if (!this.state.selectedProduct.id) {
            Notifications.show(LM.getString('shouldSelectProduct'), 'warning')
        } else {
            this.setState({
                disabled: false,
                editMode: true
            }, () => {
                if (this.state.selectedProduct.type) {
                    this.selectProductTypeField.setValue(LM.getString('productsRadioButtonsRow').slice(1)[parseInt(this.state.selectedProduct.type) - 1])
                }
            })
        }
    }

    /** Button "Save" onClick handler.
     * It prepares a 'data' object with the same keys as the this.fields object. It saves inside of it the values of all the fields that are inside of this.fields object .
     * Due to this.state.creatingNewState variable it opens the POST or PUT html request (postProduct or putProduct from DataProvider) */
    save = (e) => {
        let isValid = true;
        if (this.selectProductTypeField) {
            isValid = this.selectProductTypeField.runValidation()
        }
        isValid = this.selectSupplier.runValidation() ? isValid : false
        // TODO sent array of talk prices instead of xls file
        let data = {};
        let requestValid = true;
        Object.keys(this.fields).forEach((key) => {
            if (this.fields[key].runValidation) {
                isValid = this.fields[key].runValidation() ? isValid : false;
            }
            let value = this.fields[key].getValue();
            if (!value.valid) requestValid = false;
            else {
                if (key === "upscale_product_id") {
                    // data[key] = this.state.alternativeProduct.id ? this.state.alternativeProduct.id : null;
                    data[key] = this.fields["upscale_product_id"].getValue().valid ? this.fields["upscale_product_id"].getValue().value : null;
                } else if (!this.state.creatingNewState && value.value === "") {
                    data[key] = null;
                } else {
                    data[key] = value.value == undefined ? null : value.value;
                }        
            }
        });
        if (!isValid) {
            Notifications.show(LM.getString('completeFields'), 'danger')
            return
        }

        if (!requestValid) {
            alert(LM.getString('notValidField'));
            return;
        }

        if (this.talk_prices) {
            data.talk_prices = JSON.stringify(this.talk_prices)
        }
        Loader.show()

        if (/^data:image/.test(data.image) || /^http/.test(data.image)) {
            data.image = null
        }
        if (/^data:image/.test(data.more_info_image) || /^http/.test(data.more_info_image)) {
            data.more_info_image = null
        }
        const productName = data.name_he;
        if (this.state.creatingNewState) {
            data =Object.keys(data).reduce((n, k)=>Object.assign(n, {[k]: data[k] == undefined ? null : data[k]}),{})
            data.type = this.productType;
            data.supplier_id = this.state.currentSupplierID;
            data = objectToFormData(data)
            postProduct(data).then(
                res => {
                    Loader.hide();
                    let newProductId = res.id;
                    this.supplierChange({ value: this.state.currentSupplierID }, newProductId);
                    this.initComponentState(false);
                    Notifications.show(LM.getString('successProductAdding') + productName, 'success');
                }).catch(
                    err => {
                        Loader.hide();
                        Notifications.show(err, 'danger');
                    }
                );
        } else {
            data = objectToFormData(data)
            putProduct(this.state.selectedProduct.id, data).then(
                res => {
                    Loader.hide()
                    this.supplierChange({ value: this.state.currentSupplierID }, this.state.selectedProduct.id);
                    Notifications.show(LM.getString('successProductUpdating') + productName, 'success');
                    this.initComponentState();
                }
            ).catch(
                err => {
                    Loader.hide();
                    Notifications.show(err, 'danger');
                }
            );
        }
    }

    showMoreInfoChange = () => {
        this.setState({ showMoreInfo: !this.state.showMoreInfo });
    }

    /** Button "New" onClick handler.
     * Changes the value of creatingNewState variable to true, that will indicate, that a user is on the stage of creating a new product.
     * At the end it clears a value of the 'Select Product' field and calls the clearFields() function. */
    moveToNewStateAndClearAllFields = (e) => {
        Confirm.confirm(LM.getString('areYouSureLoseData'), !this.state.edited).then(() => {

            this.setState({
                creatingNewState: true,
                disabled: false,
                edited: false,
                editMode: false
            });
            window.scrollTo({
                top: 0,
                behavior: "smooth"
            });
            this.selectProduct.setValue("");
            this.clearFields();
        })
    }


    cancel = (e) => {
        Confirm.confirm(LM.getString('areYouSureLoseData'), !this.state.edited).then(() => {
            if (this.state.editMode) {
                getProductDetails(this.state.selectedProduct.id).then(
                    res => {
                        this.setState({
                            selectedProduct: res,
                            edited: false,
                            editMode: false,
                            disabled: true
                        }, () => {
                            this.fieldsLoad()
                        }
                        );
                    }
                )
            } else {
                this.initComponentState()
            }
        })
    }
    initComponentState = (shouldClearSelectProduct = true) => {
        this.setState({
            creatingNewState: false,
            edited: false,
            disabled: true,
            editMode: false
        });
        if (shouldClearSelectProduct) {
            this.selectProduct.setValue("");
            this.clearFields();
        }
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        });

    }

    componentDidMount = async () => {
        document.addEventListener('keyup', () => {
            this.setState({ edited: true })
        })
        this.loadSuppliersList();
        const [barcodes, tags] = await Promise.all([
            getBarcodeTypes(),
            getProductTags()
        ])

        this.setState({
            barcode_types: barcodes,
            tags: tags,
        })

    }

    highlightEditBtn = (e) => {
        if (!this.state.editMode && !this.state.creatingNewState && this.editBtn) {
            this.editBtn.setAttribute('style', 'background-color:#81abc5 !important; border: solid 2px red !important');
            setTimeout(() => {
                this.editBtn.setAttribute('style', '');
            }, 200)
        }
    }

    render() {
        return (
            <Container className="createProductContainer">
                <Row>
                    <Col className="createProductHeader">
                        {LM.getString('products') + (this.state.creatingNewState ? ` - ${LM.getString('new')}` : "")}
                    </Col>

                    {AuthService.canEdit() && !!(this.state.editMode || this.state.creatingNewState) && <Col sm='auto'>
                        <Button
                            className="regularButton"
                            disabled={this.state.disabled}
                            id="createProductButtonSave"
                            onClick={this.save}
                        >
                            {LM.getString('save')}
                        </Button>
                    </Col>}
                    {AuthService.canEdit() && !(this.state.editMode || this.state.creatingNewState) && <Col sm='auto'>
                        <Button
                            className="regularButton"
                            id="createProductButtonEdit"
                            onClick={this.edit}
                            innerRef={obj => this.editBtn = obj}
                        >
                            {LM.getString('edit')}
                        </Button>
                    </Col>}
                    {AuthService.canEdit() && !(this.state.creatingNewState || this.state.editMode) && <Col sm='auto'>
                        <Button
                            color="secondary"
                            id="createProductButtonNew"
                            onClick={this.moveToNewStateAndClearAllFields}
                        >
                            {LM.getString('new')}
                        </Button>
                    </Col>}
                    {AuthService.canEdit() && !!(this.state.creatingNewState || this.state.editMode) && <Col sm='auto'>
                        <Button
                            id="createProductButtonCancel"
                            color="secondary"
                            onClick={this.cancel}
                        >
                            {LM.getString('cancel')}
                        </Button>
                    </Col>}

                </Row>

                <Container className="createProductSettings" style={{ height: '500px', overflowY: 'scroll' }}>
                    <Row className="align-items-end">
                        <Col sm='4'>
                            <InputTypeSimpleSelect
                                disabled={this.state.editMode}
                                title={LM.getString("selectSupplier")}
                                id="CreateProductSelectSupplier"
                                required={this.state.creatingNewState}
                                options={this.state.suppliers.map(supplier => {
                                    return { label: supplier.name, value: supplier.id }
                                })} onChange={this.supplierChange}
                                ref={(componentObj) => {
                                    this.selectSupplier = componentObj
                                }}
                            />
                        </Col>
                        <Col sm='4'>
                            <Collapse isOpen={!this.state.creatingNewState}>
                                <InputTypeSimpleSelect
                                    disabled={this.state.editMode}
                                    title={LM.getString('selectProductFromList')}
                                    id="CreateProductSelectProduct"
                                    options={this.productsLoad() || ['']}
                                    onChange={this.productChange}
                                    ref={(componentObj) => {
                                        this.selectProduct = componentObj
                                    }}
                                />
                            </Collapse>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            {this.state.render && !this.state.creatingNewState && !this.state.editMode && <InputTypeRadio
                                default={LM.getString('productsRadioButtonsRow')[0]}
                                id="CreateProductSelectService-"
                                options={LM.getString('productsRadioButtonsRow')}
                                onChange={this.serviceChange}
                            />}
                            {this.state.render /*&& this.state.renderAllFields*/ && (this.state.creatingNewState || this.state.editMode) && <InputTypeRadio
                                disabled={this.state.editMode}
                                id="CreateProductSelectServiceNew-"
                                required={true}
                                title={LM.getString('selectProductType')}
                                options={LM.getString('productsRadioButtonsRow').slice(1)}
                                onChange={this.selectProductType}
                                ref={(componentObj) => {
                                    this.selectProductTypeField = componentObj
                                }}
                            />}
                        </Col>
                    </Row>
                    {this.state.renderAllFields && <React.Fragment>
                        <Container className="createProductInputBlock" onClick={this.highlightEditBtn}>
                            <Row><Col
                                className="subHeader createProductBottom">{LM.getString('productDetails') + ':'}</Col></Row>

                            <Row>
                                <Col>
                                    <InputTypeNumber
                                        disabled={this.state.disabled}
                                        ref={(componentObj) => {
                                            this.fields["call_minute"] = componentObj
                                        }}
                                        type2={true}
                                        title={LM.getString('productsInputBlockFieldsNames')[0]}
                                        id="CreateProductCallsMinutes"
                                    />
                                </Col>
                                <Col>
                                    <InputTypeNumber
                                        disabled={this.state.disabled}
                                        ref={(componentObj) => {
                                            this.fields["sms"] = componentObj
                                        }}
                                        type2={true}
                                        title={LM.getString('productsInputBlockFieldsNames')[1]}
                                        id="CreateProductSMS"
                                    />
                                </Col>
                                <Col>
                                    <InputTypeText
                                        disabled={this.state.disabled}
                                        ref={(componentObj) => {
                                            this.fields["browsing_package"] = componentObj
                                        }}
                                        type2={true}
                                        title={LM.getString('productsInputBlockFieldsNames')[2]}
                                        id="CreateProductBrowsingSize"
                                    />
                                </Col>
                            </Row>
                            <Row style={{ height: '15px' }} />
                            <Row>
                                <Col>
                                    <InputTypeSearchList
                                        disabled={this.state.disabled}
                                        ref={(componentObj) => {
                                            this.fields["usage_time_limit"] = componentObj
                                        }}
                                        type2={true}
                                        options={[1, 2, 3, 4, 5, 6, 7, 30, 60, 90, 120, 150, 180, 210, 240, 270, 300, 330, 360]}
                                        title={LM.getString('productsInputBlockFieldsNames')[3]}
                                        id="CreateProductValidityDays"
                                    />
                                </Col>
                                <Col>
                                    <InputTypeText
                                        disabled={this.state.disabled}
                                        ref={(componentObj) => {
                                            this.fields["call_terms"] = componentObj
                                        }}
                                        type2={true}
                                        title={LM.getString('productsInputBlockFieldsNames')[4]}
                                        id="CreateProductConditionOfCalls"
                                    />
                                </Col>
                                <Col>
                                    <InputTypeCheckBox
                                        disabled={this.state.disabled}
                                        type2={true}
                                        lableText={LM.getString('productsInputBlockFieldsNames')[5]}
                                        id="CreateProductCallsToWestBank"
                                        ref={(componentObj) => {
                                            this.fields["call_to_palestinian"] = componentObj
                                        }}
                                    />

                                </Col>
                            </Row>
                            <Row style={{ height: '15px' }}></Row>
                            <Row>
                                <Col>
                                    <InputTypeNumber
                                        disabled={this.state.disabled}
                                        ref={(componentObj) => {
                                            this.fields["abroed_price"] = componentObj
                                        }}
                                        type2={true}
                                        placeholder={LM.getString('shekel')}
                                        title={LM.getString('productsInputBlockFieldsNames')[6]}
                                        id="CreateProductCostInIsraelAndWorld"
                                    />
                                </Col>
                                <Col>
                                    <InputTypeText
                                        disabled={this.state.disabled}
                                        ref={(componentObj) => {
                                            this.fields["other1"] = componentObj
                                        }}
                                        type2={true}
                                        title={LM.getString('productsInputBlockFieldsNames')[7]}
                                        id="CreateProductOther1"
                                    />
                                </Col>
                                <Col>
                                    <InputTypeText
                                        disabled={this.state.disabled}
                                        ref={(componentObj) => {
                                            this.fields["other2"] = componentObj
                                        }}
                                        type2={true}
                                        title={LM.getString('productsInputBlockFieldsNames')[8]}
                                        id="CreateProductOther2"

                                    />
                                </Col>
                            </Row>
                        </Container>

                        <Container className="createProductInputBlock" onClick={this.highlightEditBtn}>
                            <Row>
                                <Col sm='4'>
                                    <InputTypeText
                                        disabled={this.state.disabled}
                                        title={LM.getString('productName') + ': ' + LM.getString('hebrew')}
                                        required={true}
                                        id="CreateProductProductNameHebrew"
                                        ref={(componentObj) => {
                                            this.fields["name_he"] = componentObj
                                        }}
                                    />
                                </Col>
                                <Col sm='4'>
                                    <InputTypeText
                                        disabled={this.state.disabled}
                                        title={LM.getString('productName') + ': ' + LM.getString('english')}
                                        id="CreateProductProductNameEnglish"
                                        ref={(componentObj) => {
                                            this.fields["name_en"] = componentObj
                                        }}
                                    />
                                </Col>
                                <Col sm='4'>
                                    <InputTypeText
                                        disabled={this.state.disabled}
                                        title={LM.getString('productName') + ': ' + LM.getString('arabic')}
                                        id="CreateProductProductNameArabic"
                                        ref={(componentObj) => {
                                            this.fields["name_ar"] = componentObj
                                        }}
                                    />
                                </Col>
                            </Row>
                        </Container>

                        <Container className="createProductInputBlock" onClick={this.highlightEditBtn}>
                            <Row tyle={{ marginBottom: '15px' }}>
                                <Col sm='4'>
                                    <InputTypeText
                                        disabled={this.state.disabled}
                                        required={true}
                                        ref={(componentObj) => {
                                            this.fields["supplier_identity_number"] = componentObj
                                        }}
                                        title={LM.getString('productId')}
                                        id="CreateProductProductId"
                                    />
                                </Col>
                            </Row>
                            <Row><Col
                                className="subHeader createProductBottom">{LM.getString('barcodes') + ':'}</Col></Row>
                            <Row style={{ marginBottom: '15px' }}>
                                <CatalogNumberTable
                                    disabled={this.state.disabled}
                                    barcodeTypes={this.state.barcode_types || []}
                                    ref={(componentObj) => {
                                        this.fields["barcodes"] = componentObj
                                    }}
                                />
                            </Row>
                        </Container>
                        <Container className="createProductInputBlock" onClick={this.highlightEditBtn}>
                            <Row><Col
                                className="subHeader createProductBottom">{LM.getString('selectTags') + ':'}</Col></Row>
                            <TagsField
                                disabled={this.state.disabled}
                                tags={this.state.tags}
                                ref={(componentObj) => {
                                    this.fields["tags"] = componentObj
                                }}
                            />
                        </Container>
                        <Container className="createProductInputBlock" onClick={this.highlightEditBtn}>
                            <Row><Col
                                className="subHeader createProductBottom">{LM.getString('prices') + ':'}</Col></Row>
                            <Row>
                                <Col sm='6'>
                                    <InputTypeNumber
                                        disabled={this.state.disabled}
                                        money={true}
                                        title={LM.getString('finalPriceForConsumer')}
                                        id="CreateProductFinalPriceForConsumer"
                                        ref={(componentObj) => {
                                            this.fields["price"] = componentObj
                                        }}
                                    />
                                </Col>
                                <Col sm='6'>
                                    <InputTypeNumber
                                        disabled={this.state.disabled}
                                        money={true}
                                        title={LM.getString('purchasePrice')}
                                        id="CreateProductPurchasePrice"
                                        ref={(componentObj) => {
                                            this.fields["buying_price"] = componentObj
                                        }}
                                    />
                                </Col>
                            </Row>
                            <Row>
                                <Col sm='6'>
                                    <InputTypeNumber
                                        disabled={this.state.disabled}
                                        money={true}
                                        title={LM.getString('distributionFee')}
                                        id="CreateProductDistributionFee"
                                        ref={(componentObj) => {
                                            this.fields["distribution_fee"] = componentObj
                                        }}
                                    />
                                </Col>
                            </Row>

                        </Container>
                        <Container className="createProductInputBlock" onClick={this.highlightEditBtn}>
                            <Row><Col
                                className="subHeader createProductBottom">{LM.getString('miscelenios') + ':'}</Col></Row>
                            <Row>
                                <Col sm='auto'>
                                    <InputTypeCheckBox
                                        disabled={this.state.disabled}
                                        lableText={LM.getString('active')}
                                        id="CreateProductActive"
                                        ref={(componentObj) => {
                                            this.fields["status"] = componentObj
                                        }}
                                    />
                                </Col>
                                <Col sm='auto'>
                                    <InputTypeCheckBox
                                        disabled={this.state.disabled}
                                        lableText={LM.getString('default')}
                                        id="CreateProductDefault"
                                        ref={(componentObj) => {
                                            this.fields["default_is_allow"] = componentObj
                                        }}
                                    />
                                </Col>
                                <Col sm='auto'>
                                    <InputTypeCheckBox
                                        disabled={this.state.disabled}
                                        lableText={LM.getString('permittedCancel')}
                                        id="CreateProductPermittedCancel"
                                        ref={(componentObj) => {
                                            this.fields["cancelable"] = componentObj
                                        }}
                                    />
                                </Col>
                                <Col sm="12">
                                    <Row>
                                        <Col sm='4'>
                                            <InputTypeNumber
                                                disabled={this.state.disabled}
                                                title={LM.getString('order')}
                                                id="CreateProductOrder"
                                                ref={(componentObj) => {
                                                    this.fields["order"] = componentObj
                                                }}
                                            />
                                        </Col>
                                        <Col sm='4'>
                                            <InputTypeSimpleSelect
                                                required={true}
                                                disabled={this.state.disabled}
                                                title={LM.getString('porfitModle')}
                                                id="CreateProductSelectProfitModel"
                                                options={[{
                                                    value: 'profit',
                                                    label: LM.getString('profitPer')
                                                }, { value: 'commission', label: LM.getString('commission') }]}
                                                onChange={this.alternativeProductChange}
                                                ref={(componentObj) => {
                                                    this.fields["profit_model"] = componentObj
                                                }}
                                            />
                                        </Col>
                                        <Col sm='4'>
                                            <InputTypeSimpleSelect
                                                disabled={this.state.disabled}
                                                title={LM.getString('alternativeProduct')}
                                                id="CreateProductSelectAlternativeProduct"
                                                options={this.state.upscaleProducts}
                                                onChange={this.alternativeProductChange}
                                                ref={(componentObj) => {
                                                    this.fields["upscale_product_id"] = componentObj
                                                }}
                                            />
                                        </Col>
                                    </Row>
                                </Col>
                                <Col sm='12'>
                                    <ImageUpload
                                        disabled={this.state.disabled}
                                        title={LM.getString('productImage')}
                                        id="CreateProductProductImage"
                                        ref={(componentObj) => {
                                            this.fields["image"] = componentObj
                                        }}
                                    />
                                </Col>
                            </Row>
                        </Container>

                        <Container className="createProductInputBlock" onClick={this.highlightEditBtn}>
                            <Row>
                                <Col sm='12'>
                                    <Notice
                                        noticeText={LM.getString('productsNoticeText')}
                                        tooltipId="CreateProductNoticeTooltip"
                                    />
                                </Col>
                            </Row>
                        </Container>

                        <Row>
                            <Col sm='auto'>
                                <InputTypeCheckBox
                                    lableText={LM.getString('showMoreInfo')}
                                    id="CreateProductShowMoreInfo"
                                    onChange={this.showMoreInfoChange}
                                    ref={(componentObj) => {
                                        this.showMoreInfo = componentObj
                                    }}
                                />
                            </Col>
                        </Row>

                        <Collapse isOpen={this.state.showMoreInfo}>
                            <Container className="createProductInputBlock" onClick={this.highlightEditBtn}>

                                <Row>
                                    <Col className="subHeader createProductBottom">
                                        {LM.getString('moreInfo')}
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm='6'>
                                        <InputTypeText
                                            disabled={this.state.disabled}
                                            title={LM.getString('moreDetailsLink')}
                                            id="CreateProductMoreDetailsLink"
                                            ref={(componentObj) => {
                                                this.fields['more_info_site_url'] = componentObj
                                            }}
                                        />
                                    </Col>
                                    <Col sm='6'>
                                        <InputTypeFile
                                            disabled={this.state.disabled}
                                            title={LM.getString('loadProfileFile')}
                                            id="CreateProductSelectProfileFile"
                                            ref={(componentObj) => {
                                                this.profileFile = componentObj
                                            }}
                                            onLoadend={(file) => {
                                                ExcelUtils.parseExcel(file).then(json => this.talk_prices = json)
                                            }}
                                        />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm='12'>
                                        <InputTypeText
                                            disabled={this.state.disabled}
                                            title={LM.getString('freeText')}
                                            id="CreateProductFreeText"
                                            ref={(componentObj) => {
                                                this.fields['more_info_description'] = componentObj
                                            }}
                                        />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm='12'>
                                        <ImageUpload
                                            disabled={this.state.disabled}
                                            title={LM.getString('image')}
                                            id="CreateProductMoreDetailsImage"
                                            ref={(componentObj) => {
                                                this.fields['more_info_image'] = componentObj
                                            }}
                                        />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm='12'>
                                        <InputTypeText
                                            disabled={this.state.disabled}
                                            title={LM.getString('explanationVideo')}
                                            id="CreateProductLinkToVideo"
                                            ref={(componentObj) => {
                                                this.fields['more_info_youtube_url'] = componentObj
                                            }}
                                        />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm='auto'>
                                        <Button className="createProductButton createProductButtonRegular"
                                            id="createProductButtonPreview"
                                            onClick={this.previewClicked}
                                        >
                                            {LM.getString('preview')}
                                        </Button>
                                    </Col>
                                </Row>
                            </Container>

                        </Collapse>

                    </React.Fragment>
                    }
                </Container>
                {this.state.renderAllFields && this.state.previewMoreInfo && <MoreInfo
                    color={(this.state.suppliers.find(s => s.id == this.currentSupplierID) || {}).backgroundColor || '#16577d'}
                    product={this.moreInfoPrepareProductFromField()}
                    isOpen={this.state.previewMoreInfo}
                    toggle={() => {
                        this.previewClicked()
                    }}
                />
                }
            </Container>
        )
    }
}

export default CreateProduct;