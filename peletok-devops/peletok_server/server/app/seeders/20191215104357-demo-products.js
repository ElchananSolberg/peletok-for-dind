'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {


    let productDocs = [
      { product_id: 1, document_id: 29 },
      {
        product_id: 3,
        document_id: 30
      },
      {
        product_id: 11,
        document_id: 31
      },
      {
        product_id: 12,
        document_id: 32
      },
      {
        product_id: 13,
        document_id: 33
      },
      {
        product_id: 994,
        document_id: 34
      },
      {
        product_id: 995,
        document_id: 35
      },
      {
        product_id: 996,
        document_id: 36
      },
      {
        product_id: 1024,
        document_id: 37
      },
      {
        product_id: 1025,
        document_id: 38
      },
      {
        product_id: 1026,
        document_id: 39
      },
      {
        product_id: 103,
        document_id: 40
      },
      {
        product_id: 101,
        document_id: 41
      },
      {
        product_id: 102,
        document_id: 42
      },
      {
        product_id: 1030,
        document_id: 43
      },
      {
        product_id: 1031,
        document_id: 44
      },
      {
        product_id: 1032,
        document_id: 45
      },
      {
        product_id: 25,
        document_id: 46
      },
      {
        product_id: 50,
        document_id: 47
      },
      {
        product_id: 100,
        document_id: 48
      },
      {
        product_id: 10,
        document_id: 49
      },
      {
        product_id: 20,
        document_id: 50
      },
      {
        product_id: 30,
        document_id: 51
      },
      {
        product_id: 1020,
        document_id: 52
      },
      {
        product_id: 1021,
        document_id: 53
      },
      {
        product_id: 1022,
        document_id: 54
      },
      {
        product_id: 2026,
        document_id: 55
      },
      {
        product_id: 2002,
        document_id: 56
      },
      {
        product_id: 2003,
        document_id: 57
      },
      {
        product_id: 1506,
        document_id: 58
      },
      {
        product_id: 1500,
        document_id: 59
      },
      {
        product_id: 1501,
        document_id: 60
      },
      {
        product_id: 1600,
        document_id: 61
      }

    ]
    // products = [{
    // supplier_id: '124',
    // name_he: 'כרטיס 10 דק חיוג לכל העולם בתוקף שבוע  בלבד מיום הטעינה',
    // name_en: '10-minute calling to the whole world is valid for only one week from the day of loading',
    // name_ar: 'الاتصال لمدة 10 دقائق للعالم كله صالح لمدة أسبوع واحد فقط من يوم التحميل',
    // id: '103',
    // description_he: "חייג 1834 או 1809331834מכל טלפון ופעל לפי ההוראות",
    // description_en: "Dial 1834 or 1809331834 from any phone and follow the instructions",
    // description_ar: "اتصل بالرقم 1834 أو 1809331834 من أي هاتف واتبع التعليمات",
    // title_he: '',
    // title_en: '',
    // title_ar: '',
    // price: '13',
    // favorite: true,
    // recommended: false,
    // internet: true,
    // upscale_product_id: '101',
    // image: "/productsImages/benleumi/benleumi-13-product.png",
    // usage_time_limit: '7',
    // type: 2,
    // order: 1,
    // buyingPrice: 13,
    // lowestPrice: null,
    // highestPrice: null,
    // timestamp_end: moment('30.12.2099', "DD-MM-YYYY"),
    // timestamp_start: moment().toDate(),
    // },]
    // return queryInterface.bulkInsert('SUPL_Product', products.map(p=>{return {
    //     call_terms: p.call_terms,
    //     order: p.order,
    //     supplier_id: p.supplier_id,
    //     id: p.id,
    //     status: p.status,
    //     status_author_id: current_user.id,
    //     status_timestamp: p.status_timestamp,
    //     usage_time_limit: p.usage_time_limit,
    //     type: p.type,
    //     profit_model: p.profit_model,
    //     more_info_description: p.more_info_description,
    //     more_info_youtube_url: p.more_info_youtube_url,
    //     more_info_site_url: p.more_info_site_url,
    // }})
    // , {});

    // return queryInterface.bulkInsert('SUPL_ProductDetails', products.map(p=>{return 
    //     [ 
    //         {
    //              product_description: p.description_he,
    //              product_name: p.name_he + key,
    //              language_code: 'HE'
    // ,,
    //              product_id: p.id
    //         },
    //         {
    //              product_description: p.description_en,
    //              product_name: p.name_en + key,
    //              language_code: 'EN'
    // ,,
    //              product_id: p.id
    //         },
    //         {
    //              product_description: p.description_ar,
    //              product_name: p.name_ar + key,
    //              language_code: 'AR'
    // ,,
    //              product_id: p.id
    //         },
    //     ]
    // }).reduce((newArr, arr)=>[...newArr,...arr],[])
    // , {});

    //  return queryInterface.bulkInsert('DOC_Document', products.map(p=>{return {
    //    url: p.image, type: getTypeNum('image', 'Document')
    // }})
    // , {});

    // return queryInterface.bulkInsert('SUPL_ProductPrice', products.map(p=>{return {
    //     fixed_price: p.price,
    //     buying_price: p.buying_price,
    //     timestamp_start: moment(),
    //     timestamp_end: moment('2099-12-31'),
    //     product_id: p.id
    // }})
    // , {});
    return Promise.resolve()

  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return Promise.resolve()
  }
};
