import React, { Component } from 'react';
import './EditProfile.css';

import { Container, Row, Col, Button, Collapse, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

import { LanguageManager as LM } from '../LanguageManager/Language';
import { Notifications } from '../Notifications/Notifications';

import CommissionProfileTable from '../CommissionProfile/CommissionProfileTable';

import {
    getProfitPercentageProfiles, postProfitPercentageProfile, putProfitPercentageProfile,
    putProfitProfileProfitPercentage, putBusinessProductsEarningPoints,
    getSuppliers, putBusinessUseDistributionFee,  getRole,
} from '../DataProvider/DataProvider'
import { Confirm } from '../Confirm/Confirm';

import InputTypeSearchList from '../InputUtils/InputTypeSearchList';
import InputTypeNumber from '../InputUtils/InputTypeNumber';
import InputTypeCheckBox from '../InputUtils/InputTypeCheckBox';
import InputTypeRadio from '../InputUtils/InputTypeRadio';
import InputTypeText from '../InputUtils/InputTypeText';
import InputTypeSimpleSelect from '../InputUtils/InputTypeSimpleSelect';
/** ProfitPercentageProfile is a main component of Profit Percentage Profile Page.  
 * ProfitPercentageProfile and its children are working with .css file: ProfitPercentageProfile.css. */
export default class EditProfile extends Component {
    constructor(props) {
        super(props);

        this.state = {
            roles: [],

            renderFields: true,

            currentSupplierId: null,

            /** working variable that will contain an array (of objects) of all the Profit Percentage Profiles */
            profitPercentageProfiles: [],
            /** working variable that will contain an object of a SELECTED suppliersProfile */
            profitPercentageProfile: {},
            /** working variable that will contain an array of suppliers from the server */
            suppliers: [],
            /** working variable that will contain an array of products from the server */
            products: [],
            /** working variable that indicates if a user has selected profit percentage profile */
            isProfileSelected: false,
            isSupplierSelected: false,
            /** tableRows - an array of objects for <CommissionProfileTable/> component. The values of the objects are the data of the rows of a table.
            */
            tableRows: [],

            useDistributionFee: false,

            openNewProfileModal: false,
        }
        /** working variable that will contain an ALWAYS FULL array of products from the server */
        this.products = [];
        /** working variable that will contain an array of products from the server, FILTERED due to supplier */
        this.productsFilteredDueToSupplier = [];


    }

    /** "Add New Profile" Button click handler */
    addNewProfileButtonClicked = () => {
        if (this.profileName.getValue().valid) {
            let name = this.profileName.getValue().value;
            let checkOnDoubleNameResult = this.state.profitPercentageProfiles.find(element => {
                if (element.name === name) return true;
                else return false;
            });
            if (!checkOnDoubleNameResult) {
                let profileData = {
                    "name": name,
                    profitModle: this.props.match.params.profitModel
                }
                postProfitPercentageProfile(profileData).then(
                    res => {
                        Notifications.show(LM.getString("profitPercentageProfile") + ' ' + LM.getString('createdSuccessfully'), 'success');
                        this.setState({ tableRows: [] });
                        this.profileName.setValue("")
                        this.launchGetProfitPercentageProfiles().then(
                            () => {
                                let selectedProfitPercentageProfile = this.state.profitPercentageProfiles.find(profitPercentageProfile => (profitPercentageProfile.name === name));
                                this.selectProfitPercentageProfile.setValue(name)
                                this.setState({ 
                                    products: selectedProfitPercentageProfile.BusinessProduct,
                                    profitPercentageProfile: selectedProfitPercentageProfile,
                                 }, () => this.loadProfileDataIntoTable())
                            }
                        )
                        this.setState({
                            openNewProfileModal: !this.state.openNewProfileModal,
                            isProfileSelected: true
                        })

                    }
                ).catch(
                    err => {
                        Notifications.show(err, 'danger');
                        this.setState({ openNewProfileModal: !this.state.openNewProfileModal })
                    }
                );
            }
            else Notifications.show(LM.getString("profileName") + " " + LM.getString("isAlreadyExist") + " ", 'warning');
        }
        else Notifications.show(LM.getString("field") + " '" + LM.getString("profileName") + "' " + LM.getString("isNotValid") + " ", 'warning');
    }

    /** "Select Profit Percentage Profile" onChange handler. */
    profitPercentageProfileChanged = (_e) => {
        this.setState({
            renderFields: false
        }, () => {
            this.setState({
                renderFields: true
            }, () => {
                let name = this.selectProfitPercentageProfile.getValue().value
                let selectedProfitPercentageProfile = this.state.profitPercentageProfiles.find(profitPercentageProfile => (profitPercentageProfile.name === name));
                this.setState({ isProfileSelected: true, profitPercentageProfile: selectedProfitPercentageProfile, useDistributionFee: selectedProfitPercentageProfile.use_distribution_fee });
                this.props.match.params.profitModel == 'profit' && this.useDistributionFee.setValue(selectedProfitPercentageProfile.use_distribution_fee)                
                /** if selected Profit Percentage Profile has a list of products inside it*/
                if (selectedProfitPercentageProfile.BusinessProduct[0]) {
                    this.setState({ products: selectedProfitPercentageProfile.BusinessProduct }, () => { if (!this.state.isSupplierSelected) this.loadProfileDataIntoTable() });
                    this.products = selectedProfitPercentageProfile.BusinessProduct;
                    if (this.state.isSupplierSelected) {
                        this.supplierChanged();
                    }
                }
            })
        })
    }

    /** "Select Supplier" field onChange handler */
    supplierChanged = (e) => {
        this.setState({
            currentSupplierId: e.value
        }, () => {
            this.loadProfileDataIntoTable()
        })
    }

    /** "Select Product" field onChange handler */
    productChanged = (e) => {
    }

    /** "Spread Profit Percentage" button click handler */
    spreadProfitPercentageButtonClicked = (_e) => {
        if (this.state.isProfileSelected && this.state.products[0]) {
            let value;
            let profileId = this.state.profitPercentageProfile.id;

            if (this.profitPercentage.getValue().valid) value = this.profitPercentage.getValue().value;
            if (value) {
                Confirm.confirm(LM.getString('thisActionwillLeadToChangesInAllProducts')).then(() => {
                    let productsIds = this.state.products.map((product) => { return product.product_id });
                    let profitPercentageData = {
                        "profile_id": profileId,
                        "percentage_profit": value,
                        "product_id": productsIds,
                        "supplier_id": this.state.currentSupplierId,
                        type: this.props.match.params.profitModel == 'profit' ? 'profit' : 'commission'
                    };
                    putProfitProfileProfitPercentage(profitPercentageData).then(
                        res => {
                            Notifications.show(LM.getString("profitPercentage") + ' ' + LM.getString('updatedSuccessfully'), 'success');

                            this.setState({ tableRows: [], profitPercentageProfile: {}, products: [], isProfileSelected: false, isSupplierSelected: false }, () => {
                                this.launchGetProfitPercentageProfiles().then(()=>{
                                    this.profitPercentageProfileChanged()
                                })
                            })
                        }
                    ).catch(
                        err => {
                            Notifications.show(err, 'danger');
                        }
                    );
                })
            }
            else Notifications.show(LM.getString("profitPercentage") + ' ' + LM.getString('notDefined'), 'warning');
        }
        else Notifications.show(LM.getString("notSelected") + LM.getString("profitPercentageProfile") + ' ' + LM.getString("orProductsListEmpty"), 'warning');
    }

    getSpreadCommissionButtonClickedHendler = (type) => {
        return (e) => {
            if (this.state.isProfileSelected && this.state.products[0]) {
                let value;
                let profileId = this.state.profitPercentageProfile.id;
                if (type == 'commission' && this.commissionMaxLimit.getValue().valid) value = this.commissionMaxLimit.getValue().value;
                if (type == 'finalCommission' && this.commissionToCustomerMinLimit.getValue().valid) value = this.commissionToCustomerMinLimit.getValue().value;
                if (value) {
                    Confirm.confirm(LM.getString('thisActionwillLeadToChangesInAllProducts')).then(() => {
                        let productsIds = this.state.products.map((product) => { return product.product_id });
                        let profitPercentageData = Object.assign({
                            "profile_id": profileId,
                            "product_id": productsIds,
                            "supplier_id": this.state.currentSupplierId,
                            type,
                        }, type == 'commission' ? { "business_commission": value } : { "final_commission": value });
                        putProfitProfileProfitPercentage(profitPercentageData).then(
                            res => {
                                Notifications.show(LM.getString("commission") + ' ' + LM.getString('updatedSuccessfully'), 'success');
                                this.setState({ tableRows: [], profitPercentageProfile: {}, products: [], isProfileSelected: false, isSupplierSelected: false }, () => {
                                    this.launchGetProfitPercentageProfiles().then(()=>{
                                        this.profitPercentageProfileChanged();
                                    })
                                })
                            }
                        ).catch(
                            err => {
                                Notifications.show(err, 'danger');
                            }
                        );
                    })
                }
                else Notifications.show(LM.getString("profitPercentage") + ' ' + LM.getString('notDefined'), 'warning');
            }
            else Notifications.show(LM.getString("notSelected") + LM.getString("profitPercentageProfile") + ' ' + LM.getString("orProductsListEmpty"), 'warning');
        }
    }
    /** "Spread Earning Points" button click handler */
    spreadEarningPointsButtonClicked = (_e) => {
        if (this.state.isProfileSelected && this.state.products[0]) {
            let value;
            let profileId = this.state.profitPercentageProfile.id;
            if (this.earningPoints.getValue().valid) value = this.earningPoints.getValue().value;
            if (value) {
                Confirm.confirm(LM.getString('thisActionwillLeadToChangesInAllProducts')).then(() => {
                    let productsIds = this.state.products.map((product) => { return product.product_id });
                    let earningPointsData = {
                        "profile_id": profileId,
                        "points": value,
                        "product_id": productsIds,
                        "supplier_id": this.state.currentSupplierId,
                        type: this.props.match.params.profitModel == 'profit' ? 'profit' : 'commission'
                    };
                    /** TODO: change request function to new from Shimon */
                    putProfitProfileProfitPercentage(earningPointsData).then(
                        res => {
                            Notifications.show(LM.getString("earningPoints") + ' ' + LM.getString('updatedSuccessfully'), 'success');

                            this.setState({ tableRows: [], profitPercentageProfile: {}, products: [], isProfileSelected: false, isSupplierSelected: false }, () => {
                                this.launchGetProfitPercentageProfiles().then(()=>{
                                    this.profitPercentageProfileChanged();
                                })
                            })
                        }
                    ).catch(
                        err => {
                            Notifications.show(err, 'danger');
                        }
                    );
                })
            }
            else Notifications.show(LM.getString("earningPoints") + ' ' + LM.getString('notDefined'), 'warning');
        }
        else Notifications.show(LM.getString("notSelected") + LM.getString("earningPoints") + ' ' + LM.getString("orProductsListEmpty"), 'warning');
    }

    saveUseDistributionFeeClicked = (e) => {
        putBusinessUseDistributionFee({ "business_id": this.state.profitPercentageProfile.id, 'use_distribution_fee': this.state.useDistributionFeeValue }).then(
            res => {
                this.setState({
                    profitPercentageProfile: Object.assign(this.state.profitPercentageProfile, { use_distribution_fee: this.state.useDistributionFeeValue })
                })
                this.state.profitPercentageProfiles.use_distribution_fee = this.state.useDistributionFeeValue
                this.loadProfileDataIntoTable();
            }
        ).catch(
            err => {
                Notifications.show(err, 'danger');
            }
        );
    }

    useDistributionFeeChanged = (e) => {
        this.setState({ useDistributionFeeValue: this.useDistributionFee.getValue().value });
    }

    /** "Save" button click handler */
    saveButtonClicked = (_e) => {
        let productsList = this.getFilteredProducts().map((product, index = 0) => {
            if (this.props.match.params.profitModel == 'profit') {

                let tableRowCheckBoxChecked = this.commissionProfileTable.tableRows['tableRow' + index].tableRowCheckBox.props.checked;
                let tableRowProfitPercentage = this.commissionProfileTable.tableRows['tableRow' + index].profitPercentage.getValue().value;
                let tableRowEarningPoints = this.commissionProfileTable.tableRows['tableRow' + index].earningPoints.getValue().value;
                let tableRowDistributionFee;
                if(this.commissionProfileTable.tableRows['tableRow' + index].distributionFee){
                    tableRowDistributionFee = this.commissionProfileTable.tableRows['tableRow' + index].distributionFee.props.checked;
                }

                if (tableRowProfitPercentage === '' && product.percentage_profit === null) tableRowProfitPercentage = null;
                if (tableRowEarningPoints === '' && product.points === null) tableRowEarningPoints = null;
                if (tableRowDistributionFee === '' && product.distribution_fee === null) tableRowDistributionFee = null;

                if (tableRowCheckBoxChecked !== product.is_authorized
                    || tableRowProfitPercentage !== product.percentage_profit
                    || tableRowEarningPoints !== product.points
                    || tableRowDistributionFee !== product.distribution_fee)
                    return {
                        "product_id": product.product_id,
                        "is_authorized": tableRowCheckBoxChecked,
                        "percentage_profit": (tableRowProfitPercentage === '' || tableRowProfitPercentage === null) ? null : tableRowProfitPercentage,
                        "points": (tableRowEarningPoints === '' || tableRowEarningPoints === null) ? null : tableRowEarningPoints,
                        "distribution_fee": tableRowDistributionFee,
                    };
                else return null;
            } else {
                let tableRowCheckBoxChecked = this.commissionProfileTable.tableRows['tableRow' + index].tableRowCheckBox.props.checked;
                let tableRowResellerCommission = this.commissionProfileTable.tableRows['tableRow' + index].resellerCommission.getValue().value;
                let tableRowCommissionForFinalClient = this.commissionProfileTable.tableRows['tableRow' + index].commissionForFinalClient.getValue().value;
                if (tableRowResellerCommission === '' && product.business_commission === null) tableRowResellerCommission = null;
                if (tableRowCommissionForFinalClient === '' && product.final_commission === null) tableRowCommissionForFinalClient = null;
                if (
                    tableRowCheckBoxChecked !== product.is_authorized
                    || tableRowResellerCommission !== product.business_commission
                    || tableRowCommissionForFinalClient !== product.final_commission
                ) {
                    return {
                        "product_id": product.product_id,
                        "is_authorized": tableRowCheckBoxChecked,
                        "business_commission": (tableRowResellerCommission === '') ? null : tableRowResellerCommission,
                        "final_commission": (tableRowCommissionForFinalClient === '') ? null : tableRowCommissionForFinalClient,
                    };
                }
                else return null;
            }
        });
        let cleanedFromNullProductsList = productsList.filter(product => product !== null);
        let roles = this.rolesSelect.getValue().value
        let profileData = {
            "profile_id": this.state.profitPercentageProfile.id,
            "products_list": cleanedFromNullProductsList,
            roles
        }
        // if (cleanedFromNullProductsList[0]) {
            putProfitPercentageProfile(profileData).then(
                res => {
                    Notifications.show(LM.getString("profitPercentageProfile") + ' ' + LM.getString('updatedSuccessfully'), 'success');
                    /** TODO:  add functionality after successfull update */
                }
            ).catch(
                err => {
                    Notifications.show(err.response.data.error, 'danger');
                }
            );


        // }
        // else Notifications.show(LM.getString("noChanges"), 'warning');
    }

    getFilteredProducts = () => {
        if (this.state.currentSupplierId) {
            return this.state.products.filter(p => p.Product.supplier_id == this.state.currentSupplierId)
        } else {
            return this.state.products
        }
    }

    /** Function that enters a data from Selected Profit Percentage Profile into the table. */
    loadProfileDataIntoTable = () => {
        this.rolesSelect.setValue(this.state.profitPercentageProfile.Roles ? this.state.profitPercentageProfile.Roles.map(r=>r.name) : [])

        let tableRows = this.getFilteredProducts().map(product => {
            let obj = this.props.match.params.profitModel == 'profit' ?
                {
                    'product': product.Product.ProductDetails[0].name,
                    'cardType': product.Product.type,
                    'profitPercentage': product.percentage_profit,
                    'profitPerParagraph20': product.article_20_fee,
                    'earningPoints': product.points,
                    'checkbox2': product.distribution_fee,
                    'checkbox': product.is_authorized,
                } :
                {
                    'product': product.Product.ProductDetails[0].name,
                    'resellerCommission': product.business_commission,
                    'resellerCommissionP20': product.percentage_profit,
                    'commissionForFinalClient': product.final_commission,
                    'checkbox': product.is_authorized,
                }

            if (!this.state.profitPercentageProfiles.use_distribution_fee) {
                delete obj['checkbox2']
            }
            return {
                ...obj,
                validationObjec: {
                    business_commission: product.Product.Supplier.max_percentage_profit || 0,
                    max_percentage_profit: product.Product.Supplier.max_percentage_profit || 0,
                    min_percentage_profit: 0,
                    distribution_fee: product.Product.ProductPrice.distribution_fee || 0,
                }
            }
        });
        this.setState({ tableRows: tableRows })

    }

    isProfitModel = () => {
        return this.props.match.params.profitModel == 'profit';
    }

    launchGetSupplier = () => {
        getSuppliers(true).then(
            (res) => {
                let suppliers = Object.keys(res).filter(k => this.isProfitModel() ? k != 'bills' : k == 'bills').reduce((newArr, key) => [...newArr, ...res[key]], []);

                this.setState({ suppliers: suppliers });
            }
        ).catch(
            (err) => {
                Notifications.show(LM.getString('loadSuppliersListFailed') + ' ' + err, 'warning');
            }
        )
    }

    launchGetProfitPercentageProfiles = () => {
        return new Promise((resolve, reject) => {
            getProfitPercentageProfiles(this.props.match.params.profitModel, true)
                .then(
                    res => {
                        this.setState({ profitPercentageProfiles: res }, () => {
                            resolve(res)
                        });
                    }
                )
                .catch((err) => {return reject(), Notifications.show(err, 'danger')});
        })
    }

    componentDidMount() {
        getRole().then(res=>{
            this.setState({
                roles: res
            })
        })
        this.launchGetProfitPercentageProfiles();
        this.launchGetSupplier();
    }

    componentDidUpdate(prevProps) {
        if (this.props.match.params.profitModel !== prevProps.match.params.profitModel) {
            this.onRouteChanged();
        }
    }

    onRouteChanged() {
        this.selectProfitPercentageProfile.setValue('')


        this.setState({
            renderFields: false,
            profitPercentageProfile: {},
            products: []
        }, () => {
            this.setState({
                renderFields: true
            }, () => {
                this.launchGetSupplier();
                this.loadProfileDataIntoTable();
                this.launchGetProfitPercentageProfiles();
            })
        })
    }

    render() {
        /** Screen header  */
        const header = this.props.match.params.profitModel == 'profit' ? LM.getString('profitPercentageProfile') : LM.getString('commissionProfile');
        /** "Profile Creation" sub header  */
        const profileCreationHeader = LM.getString("profileCreation");
        /** "Profile Edition" sub header  */
        const profileEditionHeader = LM.getString("profileEdition");

        /** data for "Profile Name" field (InputTypeText InputUtils component) */
        const profileNameProps = {
            required: true,
            title: LM.getString("profileName") + ':',
            id: "ProfitPercentageProfile_ProfileName",
        };

        /** data for "Select Profit Percentage Profile" field (InputTypeSearchList InputUtils component) */
        const selectProfitPercentageProfileProps = {
            title: this.props.match.params.profitModel == 'profit' ? LM.getString("selectProfitPercentageProfile") + ':' : LM.getString('selectCommissionsProfile'),
            id: "ProfitPercentageProfile_SelectProfitPercentageProfile",
        };
        /** data for "Select Supplier" field (InputTypeSearchList InputUtils component) */
        const selectSupplierProps = {
            title: LM.getString("selectSupplier"),
            id: "ProfitPercentageProfile_SelectSupplier",
        };
        /** data for "Select Product" field (InputTypeSearchList InputUtils component) */
        const selectProductProps = {
            title: LM.getString("selectProductFromList"),
            id: "ProfitPercentageProfile_SelectProduct",
        };

        /** data for "Use Distribution Fee" field (InputTypeCheckBox InputUtils component) */
        const useDistributionFeeProps = {
            lableText: LM.getString("useDistributionFee"),
            id: "ProfitPercentageProfile_UseDistributionFee",
        };
        /** data for "Save as Default Profile" field (InputTypeCheckBox InputUtils component) */
        const saveAsDefaultProfileProps = {
            lableText: LM.getString("saveAsDefaultProfile"),
            id: "ProfitPercentageProfile_SaveAsDefaultProfile",
        };

        /** data for service type (InputTypeRadio  InputUtils component) */
        const serviceTypeProps = {
            options: LM.getString('serviceTypeOptions'),
            default: LM.getString('serviceTypeOptions')[0],
            id: "ProfitPercentageProfile_ServiseType",
        };

        /** data for "Profit Percentage" field (InputTypeNumber InputUtils component) */
        const profitPercentageProps = {
            title: LM.getString("profitPercentage") + ':',
            id: "ProfitPercentageProfile_ProfitPercentage",
        };
        /** data for "Earning Points" field (InputTypeNumber InputUtils component) */
        const earningPointsProps = {
            title: LM.getString("earningPoints") + ':',
            id: "ProfitPercentageProfile_EarningPoints",
        };

        /** tableHeadersObj - an object for <CommissionProfileTable/> component. The values of the object are the HEADERS of a table */
        let tableHeadersObj = this.props.match.params.profitModel == 'profit' ?
            (this.state.profitPercentageProfiles.use_distribution_fee ? {
                product: LM.getString('product'),
                cardType: LM.getString('cardType'),
                profitPercentage: LM.getString("profitPer"),
                profitPerParagraph20: LM.getString("profitPer") + ' ' + LM.getString("paragraph20"),
                earningPoints: LM.getString("earningPoints"),
                distributionFee: LM.getString("distributionFee").slice(0, -1),
                authorizedUser: LM.getString("authorizedUser"),
            } : {
                    product: LM.getString('product'),
                    cardType: LM.getString('cardType'),
                    profitPercentage: LM.getString("profitPer"),
                    profitPerParagraph20: LM.getString("profitPer") + ' ' + LM.getString("paragraph20"),
                    earningPoints: LM.getString("earningPoints"),
                    authorizedUser: LM.getString("authorizedUser"),
                }) :
            {
                product: LM.getString('product'),
                // cardType: LM.getString('cardType'),
                resellerComission: LM.getString('resellerCommission'),
                resellerComissionP20: LM.getString('resellerCommissionP20'),
                finalCommision: LM.getString('finalCommission'),
                authorizedUser: LM.getString('authorizedUser'),
            };

        /** data for "Spread Profit Percentage" button */
        const spreadProfitPercentageButtonText = LM.getString("spreadProfitPercentage");
        /** data for "Spread EarningPoints" button */
        const spreadEarningPointsButtonText = LM.getString("spreadEarningPoints");
        /** data for "Save" button */
        const saveButtonText = LM.getString("save");
        /** data for "Add a New Profile" button */
        const addNewProfileButtonText = LM.getString('addProfile');

        return (
            <Container className="profitPercentageProfileContainer">
                <Row >
                    <Col className="profitPercentageProfileHeader">
                        {header}
                    </Col>
                </Row>
                <Container className="profitPercentageProfileSettings">
                    <Row >
                        <Col >
                            <hr></hr>
                        </Col>
                    </Row>
                    <Row >
                        <Col className="profitPercentageProfileSubHeader">
                            {profileEditionHeader}
                        </Col>
                    </Row>
                    <Row>
                        <Col sm='4'>
                            <InputTypeSearchList
                                {...selectProfitPercentageProfileProps}
                                onChange={this.profitPercentageProfileChanged}
                                options={this.state.profitPercentageProfiles.map(profitPercentageProfile => profitPercentageProfile.name)}
                                ref={(componentObj) => { this.selectProfitPercentageProfile = componentObj }}
                            />
                        </Col>
                        <Col sm="auto">
                            <Button
                                className="profitPercentageProfileButton"
                                id="profitPercentageProfile_AddNewProfileButton"
                                onClick={() => {
                                    this.setState({ openNewProfileModal: !this.state.openNewProfileModal })
                                }}
                            >
                                {addNewProfileButtonText}
                            </Button>
                        </Col>
                    </Row>
                </Container>
                {this.state.renderFields ? <Container className={'profitPercentageProfileSettings'}>
                    {this.props.match.params.profitModel == 'profit' &&
                        <Row className="businessBackground align-items-center justify-content-start">
                            <Col sm='auto'>
                                <InputTypeCheckBox
                                    {...useDistributionFeeProps}
                                    onChange={this.useDistributionFeeChanged}
                                    ref={(componentObj) => {
                                        this.useDistributionFee = componentObj
                                    }}
                                />
                            </Col>
                            <Col sm='auto'>
                                <Button
                                    className="businessButton businessButtonSmall businessButtonSave"
                                    id="sellers_ProfitPercentages_ButtonSaveUseDistributionFee"
                                    onClick={this.saveUseDistributionFeeClicked}
                                >
                                    {saveButtonText}
                                </Button>
                            </Col>
                        </Row>
                    }
                    {/*<Row >
                        <Col lg="8">
                            <InputTypeRadio
                                {...serviceTypeProps}
                                ref={(componentObj) => { this.serviceType = componentObj }}
                            />
                        </Col>
                     </Row>*/}
                    <Row>
                        <Col sm='4'>
                            <InputTypeSimpleSelect
                                {...selectSupplierProps}
                                onChange={this.supplierChanged}
                                options={this.state.suppliers.map(supplier => {
                                    return { label: supplier.name, value: supplier.id }
                                })}
                                ref={(componentObj) => { this.selectSupplier = componentObj }}
                            />
                        </Col>
                        {/* <Col sm='4'>
                           <InputTypeSearchList
                               {...selectProductProps}
                               onChange={this.productChanged}
                               options={this.state.isSupplierSelected ? this.productsFilteredDueToSupplier.map(product => product.Product.ProductDetails[0].name) : this.products.map(product => product.Product.ProductDetails[0].name) }
                               ref={(componentObj) => { this.selectProduct = componentObj }}
                           />
                        </Col>*/}
                    </Row>
                    {this.props.match.params.profitModel == 'profit' ? <span><Row>
                        <Col sm='4'>
                            <InputTypeNumber
                                {...profitPercentageProps}
                                ref={(componentObj) => { this.profitPercentage = componentObj }}
                            />
                        </Col>
                        <Col sm='4'>
                            <Button
                                className="profitPercentageProfileButton profitPercentageProfileSpreadButton"
                                id="profitPercentageProfile_SpreadProfitPercentageButton"
                                onClick={this.spreadProfitPercentageButtonClicked}
                            >
                                {spreadProfitPercentageButtonText}
                            </Button>
                        </Col>
                    </Row>
                        <Row >
                            <Col sm='4'>
                                <InputTypeNumber
                                    {...earningPointsProps}
                                    ref={(componentObj) => { this.earningPoints = componentObj }}
                                />
                            </Col>
                            <Col sm='4'>
                                <Button
                                    className="profitPercentageProfileButton profitPercentageProfileSpreadButton"
                                    id="profitPercentageProfile_SpreadEarningPointsButton"
                                    onClick={this.spreadEarningPointsButtonClicked}
                                >
                                    {spreadEarningPointsButtonText}
                                </Button>
                            </Col>
                        </Row></span> : <span>
                            <Row>
                                <Col sm='4'>
                                    <InputTypeNumber
                                        title={LM.getString('commission')}
                                        id="Commission"
                                        ref={(componentObj) => { this.commissionMaxLimit = componentObj }}
                                    />
                                </Col>
                                <Col sm='4'>
                                    <Button
                                        className="profitPercentageProfileButton profitPercentageProfileSpreadButton"
                                        id="profitPercentageProfile_SpreadProfitPercentageButton"
                                        onClick={this.getSpreadCommissionButtonClickedHendler('commission')}
                                    >
                                        {LM.getString('spreadCommissionOnAll')}
                                    </Button>
                                </Col>
                            </Row>
                            <Row >
                                <Col sm='4'>
                                    <InputTypeNumber
                                        title={LM.getString('finalCommission') + ":"}
                                        id="CommissionFinal"
                                        ref={(componentObj) => { this.commissionToCustomerMinLimit = componentObj }}
                                    />
                                </Col>
                                <Col sm='4'>
                                    <Button
                                        className="profitPercentageProfileButton profitPercentageProfileSpreadButton"
                                        id="profitPercentageProfile_SpreadEarningPointsButton"
                                        onClick={this.getSpreadCommissionButtonClickedHendler('finalCommission')}
                                    >
                                        {LM.getString('spreadFinalCommissionOnAll')}
                                    </Button>
                                </Col>
                            </Row></span>}
                    <Row>
                        <Col sm='4'>
                            <InputTypeCheckBox
                                {...saveAsDefaultProfileProps}
                                ref={(componentObj) => { this.saveAsDefaultProfile = componentObj }}
                            />
                        </Col>
                    </Row>
                    <Row style={{ marginTop: '30px' }}>
                        <Col >
                            <InputTypeSearchList
                                multi={true}
                                title={LM.getString('allowedToRole')}
                                id="allowedToRole"
                                options={this.state.roles.map(r=>r.name)}
                                ref={(componentObj) => { this.rolesSelect = componentObj }} />
                        </Col>
                    </Row>
                    <Row style={{ marginTop: '30px' }}>
                        <Col >
                            <CommissionProfileTable
                                tableRows={this.state.tableRows}
                                tableHeadersObj={tableHeadersObj}
                                tableFootersObj={{}}
                                tableRowsEditable={this.props.match.params.profitModel == 'profit' ? [false, false, true, false, true, false, false] : [false, true, false, true, false]}
                                ref={(componentObj) => { this.commissionProfileTable = componentObj }}
                            />
                        </Col>
                    </Row>
                                        
                    <Collapse isOpen={this.state.isProfileSelected} >
                        <Row>
                            <Col sm="auto">
                                <Button
                                    className="profitPercentageProfileButton profitPercentageButtonSave"
                                    id="profitPercentageProfileSaveButton"
                                    onClick={this.saveButtonClicked}
                                >
                                    {saveButtonText}
                                </Button>
                            </Col>
                        </Row>
                    </Collapse>
                </Container> : <></>}
                <Modal isOpen={this.state.openNewProfileModal} >
                    <ModalHeader toggle={() => { this.setState({ openNewProfileModal: !this.state.openNewProfileModal }) }}></ModalHeader>
                    <ModalBody>
                        <Row>
                            <Col>
                                <InputTypeText
                                    {...profileNameProps}
                                    ref={(componentObj) => { this.profileName = componentObj }}
                                />
                            </Col>
                        </Row>
                    </ModalBody>
                    <ModalFooter>
                        <Button className='regularButton m-2' onClick={this.addNewProfileButtonClicked}>{LM.getString("save")}</Button>{' '}
                        <Button color="secondary" onClick={() => { this.setState({ openNewProfileModal: !this.state.openNewProfileModal }) }}>{LM.getString("cancel")}</Button>
                    </ModalFooter>
                </Modal>
            </Container >
        )
    }
}