import React from 'react'
import { LanguageManager as LM } from '../LanguageManager/Language'
import SimpleText from './SimpleText'

export default function ProductText({product, withSeperator, withBr}){
	return (
			<p id="product-description">
                <SimpleText label={LM.getString('call_terms')} text={product.call_terms} withSeperator={withSeperator} withBr={withBr} />
                <SimpleText label={LM.getString('call_minute')} text={product.call_minute} withSeperator={withSeperator} withBr={withBr} />
                <SimpleText label={LM.getString('sms')} text={product.sms} withSeperator={withSeperator} withBr={withBr} />
                <SimpleText label={LM.getString('browsing_package')} text={product.browsing_package} withSeperator={withSeperator} withBr={withBr} />
                <SimpleText label={LM.getString('call_to_palestinian')} text={product.call_to_palestinian ? String.fromCharCode(10003) : String.fromCharCode(9747)} withSeperator={withSeperator} withBr={withBr} />
                <SimpleText label={LM.getString('abroed_price')} text={product.abroed_price} withSeperator={withSeperator} withBr={withBr} />
                <SimpleText text={product.other1} withSeperator={withSeperator} withBr={withBr} />
                <SimpleText text={product.other2} withSeperator={withSeperator} withBr={withBr} end={true}/>
           </p>
		)
}