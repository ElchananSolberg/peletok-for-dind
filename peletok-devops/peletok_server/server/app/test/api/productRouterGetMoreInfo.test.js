const models = require('../../index')
const sequelize = require('../../setupSequelize')
const request = require('supertest');
// const routing = require('../../routing')
const {productRouter} = require('../../api/productRouter')
const Product = models.product.model;
const bodyParser = require('body-parser');
var express = require('express');
const {getTypeNum} = require('../../utils')
const {params, mockProducts} = require('./mockeProducts')
const Sequelize = require('sequelize');
const Op = Sequelize.Op
const moment = require('moment')


describe('productRouterGet', () => {

    

    describe('GET /product', () => {
            let app;
            let supplier1
            let current_user
            beforeAll(async  (done) => {
                await sequelize.sync({
                         force: true,
                         logging: false
                 });

                [supplier1, current_user] = await mockProducts();
                app = express();
                app.use(express.json());
                app.use(express.urlencoded({extended: false}));
                app.use(bodyParser.urlencoded({ extended: false })); 
                // mock req.user
                app.use((req, res, next)=>{      
                    req.user = {
                        id: current_user.id,
                        language_code: 'HE',
                        business_id: 1
                    };
                    next();
                })
                // app.use('/', routing)
                app.use('/product/', productRouter );                
                
                console.log('here')
                done()
            } )

            it('Should retun status 200', () => {
                return request(app).get('/product/more_info/1').then((response) => {
                    expect(response.statusCode).toBe(200);
                } )
            } )

  

            

            test('response should be ...', async () => {
                const product = await Product.findOne({
                    where: {
                        id: 1,
                    },
                    include: [
                        {model: models.talkPrice.model, as: 'TalkPrice'},
                        {
                            model: models.productDetails.model,
                            as: 'ProductDetails',
                        },
                        {
                            model: models.productPrice.model,
                            as: 'ProductPrice',
                            required: false,
                            where: {
                                timestamp_end: {
                                    [Op.gte]: moment()
                                }
                            }
                        },
                        {
                            model: models.document.model,
                            as: 'Document',
                            where: {type: getTypeNum('other_image', 'Document')}
                        }
                    ]
                })
                
                
                return request(app).get('/product/more_info/1').then(async(response) => {
                    
                    expect(response.body).toMatchObject({
                                name_he: (product.ProductDetails.find(p=>p.language_code == 'HE') || {}).product_name,
                                name_en: (product.ProductDetails.find(p=>p.language_code == 'EN') || {}).product_name,
                                name_ar: (product.ProductDetails.find(p=>p.language_code == 'AR') || {}).product_name,
                                price: product.ProductPrice[0].fixed_price,
                                more_info_image: product.Document[0].url,
                                more_info_description: product.more_info_description,
                                more_info_youtube_url: product.more_info_youtube_url,
                                more_info_site_url: product.more_info_site_url,
                                call_terms: product.call_terms,
                                call_minute: product.call_minute,
                                sms: product.sms,
                                browsing_package: product.browsing_package,
                                call_to_palestinian: product.call_to_palestinian,
                                abroed_price: product.abroed_price,
                                other1: product.other1,
                                other2: product.other2,
                                talk_prices:  product.TalkPrice.map(b=>{
                                    return {
                                        country:b.country,
                                        outcomePrice:b.outcome_price,
                                        incomePrice:b.income_price,
                                        smsPrice:b.sms_price,
                                        
                                    }
                                }),

                    })
                 })
            })

//             it('Should return only user\'s business product else return 401', async () => {
//                 await models.businessProduct.model.update({
//                     is_authorized: false
//                 },{
//                     where: {
//                         business_id: 1,
//                         product_id: 1,
//                     }
//                 })
//                 return request(app).get('/product/' + supplier1.id).then((response) => {
//                     expect(response.statusCode).toBe(401);
//                     expect(response.text).toBe('Unauthorized');
                    
//                 } )
//             } )



      } )
} )