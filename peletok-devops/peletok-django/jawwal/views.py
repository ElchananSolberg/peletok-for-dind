from __future__ import unicode_literals
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import BasePermission
from rest_framework.decorators import api_view, permission_classes
from selenium.common.exceptions import TimeoutException
from utils.elements_selectors import Jawwal
from utils.main_functions import MainFunctions


@api_view(['POST'])
@permission_classes([BasePermission])
def post_pay_card(request):
    """
    {
        "uri": "https://www.etopup.ps/Account/LogOn?ReturnUrl=%2f",
        "payment_info": {
            "user_name": "972592931986",
            "user_pass": "55584335",
            "price": "10",
            "client_id": "8202202"
        }
    }
    :param request:
    :return status: <str> 'success' / 'error', confirmation: <str> || error_message: <str>}
    """
    handler = JawwalPayment(str(request.data.get("uri")), request.data.get("payment_info"))
    res = handler.pay()
    return Response(res, status=status.HTTP_200_OK)


class JawwalPayment:

    def __init__(self, url: str, payment_info: dict):
        self.payment_info = payment_info
        self.url = url
        self.driver = MainFunctions(self.url)

    def first_steps(self):
        return_obj = {}
        try:
            self.driver.send_keys(Jawwal.userName, self.payment_info["user_name"])
            self.driver.send_keys(Jawwal.password, self.payment_info["user_pass"])
            self.driver.click_button(Jawwal.loginButton)
            if self.driver.check_if_element_exits(Jawwal.errorMessage)[0]:
                error = self.driver.get_text_from_element(Jawwal.errorMessage)
                return_obj["status"] = 'error'
                return_obj["error_message"] = error
                return_obj["error_message_heb"] = 'שם משתמש או סיסמא לא נכונים'
                return return_obj
        except TimeoutException:
            return_obj["status"] = 'error'
            return_obj["error_message"] = "המערכת לא הצליחה להתחבר, אנא נסה שוב מאוחר יותר."
            return return_obj

    def pay(self):
        begin = self.first_steps()
        return_obj = {}
        if type(begin) is dict:
            return begin
        else:
            self.driver.send_keys(Jawwal.recipientNumber, self.payment_info["client_id"])
            self.driver.send_keys(Jawwal.amount, self.payment_info["price"])
            self.driver.click_button(Jawwal.callConfirm)
            self.driver.click_button(Jawwal.okButton)
            self.driver.click_button(Jawwal.show5RecentMovements)

            for i in range(1, 6):
                phone_number_in_table = self.driver.get_text_from_element(
                    (Jawwal.phoneNumberInTable[0], Jawwal.phoneNumberInTable[1].format(i)))
                if phone_number_in_table == f'+97259{self.payment_info["client_id"]}':
                    status_in_table = self.driver.get_text_from_element(
                        (Jawwal.statusInTable[0], Jawwal.statusInTable[1].format(i)))
                    if status_in_table == 'ناجحة':
                        confirmation = self.driver.get_text_from_element(
                            (Jawwal.confirmation[0], Jawwal.confirmation[1].format(i)))
                        return_obj['status'] = 'success'
                        return_obj['confirmation'] = confirmation
                        return return_obj
                elif self.driver.get_text_from_element(Jawwal.errorMessage2) != '':
                    return_obj['status'] = 'error'
                    return_obj['error_message'] = \
                        self.driver.get_text_from_element(Jawwal.errorMessage2)
                    return_obj['error_message_heb'] = 'מספר טלפון לא תקין'
                    return return_obj
                else:
                    return_obj['status'] = 'error'
                    return_obj['error_message'] = 'מספר טלפון לא נמצא בתוך הטבלה,' \
                                                  ' נראה שהמערכת לא הצליחה לטעון, נסה שוב.'
                    return return_obj
