from tests.base_project_test_class import *


class TestBezekBenleoumi014Cards(BaseProjectTestClass):

    def login_and_get_to_bezek_benleoumi_014_page(self):
        self.login()
        self.driver.tools.wait_and_click(HomePage.bezek_benleoumi_014)
        self.driver.wait.wait_for_element_to_be_present(VerificationPage.price_bezek_beleoumi_014)

    def test_bezek_benleoumi_014_click_on_back_button(self):
        self.login_and_get_to_bezek_benleoumi_014_page()
        self.click_on_back_botton()
        self.after_the_click_back_button()
        self.remove_logs_dir = True

    def test_card_benleoumi_50(self):
        self.login_and_get_to_bezek_benleoumi_014_page()
        self.pay_for_calling_card_vc(50, "205/1600")
        self.remove_logs_dir = True

    def test_check_alert_favorite_tag_and_wait_btn_annul_benleoumi_014(self):
        self.login_and_get_to_bezek_benleoumi_014_page()
        self.click_favorite_tag_and_wait_btn_annul()
        self.remove_logs_dir = True

    def test_check_alert_favorite_tag_and_wait_btn_confirm_benleoumi_014(self):
        self.login_and_get_to_bezek_benleoumi_014_page()
        self.click_favorite_tag_and_wait_btn_confirm()
        self.remove_logs_dir = True

    def test_check_if_add_favorite_cards_benleoumi_014(self):
        self.login_and_get_to_bezek_benleoumi_014_page()
        self.click_favorite_tag_and_check_if_add_moadafim()
        self.remove_logs_dir = True
