const DataTypes = require('sequelize').DataTypes;
const BaseModel = require('../../BaseModel');
/**
 * ExternalAccounting model
 */

class ExternalAccounting extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            name: {
                type: DataTypes.STRING,
                allowNull: true
            }
        }

        this.prefix = 'EXAC'
    }
/**
 * creates associate for ExternalAccounting model
 * @param models: objects in format {modelName: model}
 */
    associate(models) {
        this.model.hasMany(models.externalAccountingProductCode.model, {as: '' });

    }
}

module.exports = new ExternalAccounting();