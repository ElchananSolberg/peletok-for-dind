const DataTypes = require('sequelize').DataTypes;
const BaseModel = require('../../BaseModel');

/**
 * BannerLanguage model
 */

class BannerLanguage extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {};
        this.prefix = "BAN";
    }

    /**
     * creates associate for BannerLanguage model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.language.model, { as: 'Language' });
        this.model.belongsTo(models.banner.model, { as: 'Banner' });
    }
}

module.exports = new BannerLanguage();
