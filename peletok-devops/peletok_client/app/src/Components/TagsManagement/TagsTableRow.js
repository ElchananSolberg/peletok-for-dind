import React, { Component } from 'react';
import './TagsManagement.css';

import { Input, Button, Row, Col } from 'reactstrap';

import { LanguageManager as LM } from '../LanguageManager/Language'

import { putTag, deleteTag } from '../DataProvider/DataProvider'

import InputTypeCheckBox from '../InputUtils/InputTypeCheckBox';
import {faTrashAlt, faPencilAlt, faSave, faWindowClose} from '@fortawesome/free-solid-svg-icons';

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
/** TagsTableRow component is a part of a Tags Management page. 
 * It renders one row of a tags table using a data from props (object sended by a father component - TagsTable).
*/
class TagsTableRow extends Component {
    constructor(props) {
        super(props);

        this.state = {
            editDisplay: 'block',
            saveDisplay: 'none',

            ordersIncludes: null,
        };
        /** working variable that will contain an object of all the refs of SAVING input fields */
        this.saveFields = {
            order: this.props.order,
            name: this.props.name,
            show_on_product: this.props.show_on_product,
        };
    }

    tagOrderChange = (e) => {
        let value = e.target.value;
        this.saveFields["order"] = value;
        if (this.props.orders.includes(value)) this.setState({ ordersIncludes: true });
        else this.setState({ ordersIncludes: false });
    }
    tagNameChange = (e) => {
        this.saveFields["name"] = e.target.value;
    }
    showOnProductChange = (e) => {
        this.saveFields["show_on_product"] = e.target.checked;

    }

    /** Button "Edit" onClick handler. 
     * When it was clicked:
     *                      - Edit button should become INVISIBLE 
     *                      - Save button  should become VISIBLE 
     *                      - Cancel button  should become VISIBLE 
     *                      - Tag Order text should be turned into input
     *                      - Tag Name text should be turned into input
    */
    editClick = (e) => {
        this.setState({ editDisplay: 'none' });
        this.setState({ saveDisplay: 'block' });

    }
    /** Button "Delete" onClick handler.*/
    deleteClick = (e) => {
        let tagId = this.props.id;
        if (tagId === undefined || tagId === null) {
            alert(LM.getString("tagIdError"));
            return;
        }
        deleteTag(tagId)
            .then(
                res => {
                    this.props.loadTagsList(true);
                    alert(LM.getString("successTagDeleting") + this.saveFields.name);
                }
            )
            .catch(
                err => {
                    alert(err);
                }
            );
    }
    /** Button "Save" onClick handler. */
    saveClick = (e) => {
        let data = {};
        let tagId = this.props.id;
        if (tagId === undefined || tagId === null) {
            alert(LM.getString("tagIdError"));
            return;
        }
        Object.keys(this.saveFields).forEach((key) => {
            let value = this.saveFields[key];
            if (value === "") {
                data[key] = null;
            }
            else data[key] = value;
        });

        putTag(tagId, data).then(
            res => {
                this.props.loadTagsList(true);
                alert(LM.getString("successTagUpdating") + data["name"]);
                this.setState({ editDisplay: 'block' });
                this.setState({ saveDisplay: 'none' });
            }
        ).catch(
            err => {
                alert(err);
            }
        );

    }
    /** Button "Cancel" onClick handler. 
     * When it was clicked:
     *                      - Edit button should become VISIBLE 
     *                      - Save button  should become INVISIBLE
     *                      - Cancel button  should become INVISIBLE
     *                      - Tag Order input should be turned into text
     *                      - Tag Name input should be turned into text
    */
    cancelClick = (e) => {
        this.setState({ editDisplay: 'block' });
        this.setState({ saveDisplay: 'none' });
    }

    render() {
        let lableText = LM.getString("showTagOnProductCard");
        let edit = LM.getString("edit");
        let deleteBtn = LM.getString("delete1");
        let save = LM.getString("save1");
        let cancel = LM.getString("cancel");
        let warningMsg = LM.getString("warningOrderExist");

        let tagOrder = this.props.order;
        let tagName = this.props.name;
        let type = this.props.type;
        let show_on_product = this.props.show_on_product;
        let id = this.props.id;

        return (
            <div className="tagsManagementTableRow align-items-center d-flex flex-wrap">
                <Col xs='1'>
                    {this.state.editDisplay === 'block' ?
                        <>{tagOrder}</>
                        :
                        <Input type='number' className="tagsManagementEditingTagNameInTable tagsManagementNumberInput p-0"
                            defaultValue={tagOrder}
                            id={`tagsManagementTableRowOrder` + id}
                            onChange={this.tagOrderChange}
                        />
                    }
                </Col>
                <Col md='3' sm='4' xs='4'>
                    {this.state.editDisplay === 'block' ?
                        <div className="tagsManagementTagNameInTable pr-2"> {tagName} </div>
                        :
                        <Input type='text' className="tagsManagementEditingTagNameInTable"
                            defaultValue={tagName}
                            id={`tagsManagementTableRowTagName` + id}
                            onChange={this.tagNameChange}
                        />
                    }
                </Col>
                {type === 2 ?
                    <div>
                        <InputTypeCheckBox lableText={lableText} id={"TagsManagementTableCheckbox" + id} onChange={this.showOnProductChange} checked={show_on_product} />
                    </div>
                    :
                    <div/>
                }
                <Col className='justify-content-end d-md-flex d-none'>
                    <Button style={{ display: this.state.editDisplay }} className="mx-1 tagsManagementTableBtn tagsManagementTableBtnEdit" id="tagsManagementTableBtnEdit" onClick={this.editClick}> {edit} </Button>
                    <Button style={{ display: this.state.saveDisplay }} className="mx-1 tagsManagementTableBtn tagsManagementTableBtnSave" id="tagsManagementTableBtnSave" onClick={this.saveClick}> {save} </Button>
                    <Button style={{ display: this.state.editDisplay }} className="tagsManagementTableBtn tagsManagementTableBtnDelete" id="tagsManagementTableBtnDelete" onClick={this.deleteClick}> {deleteBtn} </Button>
                    <Button style={{ display: this.state.saveDisplay }} className="tagsManagementTableBtn tagsManagementTableBtnDelete" id="tagsManagementTableBtnCancel" onClick={this.cancelClick}> {cancel} </Button>
                </Col>
                <Col className='justify-content-end d-md-none d-flex'>
                    <a style={{ display: this.state.editDisplay }} className="pointer mx-3" id="tagsManagementTableBtnEdit" onClick={this.editClick}><FontAwesomeIcon icon={faPencilAlt} size="1x" /></a>
                    <a style={{ display: this.state.saveDisplay }} className="pointer mx-3" id="tagsManagementTableBtnSave" onClick={this.saveClick}><FontAwesomeIcon icon={faSave} size="1x" /></a>
                    <a style={{ display: this.state.editDisplay }} className="pointer" id="tagsManagementTableBtnDelete" onClick={this.deleteClick}><FontAwesomeIcon icon={faTrashAlt} size="1x" /></a>
                    <a style={{ display: this.state.saveDisplay }} className="pointer" id="tagsManagementTableBtnCancel" onClick={this.cancelClick}><FontAwesomeIcon icon={faWindowClose} size="1x" /></a>
                </Col>
                {this.state.ordersIncludes && this.state.editDisplay === 'none' ? <Col sm='12' className="tagsManagementWarningMsg" > {warningMsg} </Col> : <></>}
            </div>
        )
    }
}
export default TagsTableRow;
