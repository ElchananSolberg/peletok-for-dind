const BaseModel = require('../../BaseModel');
const DataTypes = require("sequelize").DataTypes;

/**
 * Tag SupplierTag
 */
class SupplierTag extends BaseModel {
    /**
     *
     * configs fields and options
     */
    config() {
        this.attributes = {};
        this.prefix = "TAG";
        this.author = true;
    }
    /**
     * creates associate for SupplierTag model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.tag.model, { as: 'Tag' });
        this.model.belongsTo(models.supplier.model, { as: 'Supplier' });
    }
}

module.exports = new SupplierTag();