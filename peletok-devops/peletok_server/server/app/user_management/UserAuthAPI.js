var express = require('express');
var router = express.Router();
var { passport } = require('./setupPassport.js');
var models = require('../index.js');
const LM = require('../config/LanguageManager').LM;

/**
 * API for user_management (login, logout, is_auth)
 * All user_management urls starts with user_management, for example: user_management/login
 */
var loginApi = "/login";
var logoutApi = "/logout";
var isAuth = "/is_auth";



router.post(loginApi, function(req, res, next) {
    passport.authenticate('local', function (err, user, info) {
        if (!user) {
           return res.status(401).send(info)
           
        }
        req.logIn(user, function(err) {
            LM.setLang(req.body.lang, user.id)
            const token = user.my_token
           return res.send({ msg: "success", token })
        })
    })(req, res, next)
})



router.post(logoutApi, function (req, res) {
    req.logout();
    res.send("success")
});

router.get(isAuth, function (req, res) {
    res.send(req.isAuthenticated());
});

module.exports = router;
