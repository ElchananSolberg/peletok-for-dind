'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('USR_UserActivityLog',
      {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        path: {
          type: Sequelize.TEXT,
        },
        full_url: {
          type: Sequelize.TEXT,
        },
        query: {
          type: Sequelize.TEXT,
        },
        params: {
          type: Sequelize.TEXT,
        },
        body: {
          type: Sequelize.TEXT,
        },
        user: {
          type: Sequelize.TEXT,
        },
        ip: {
          type: Sequelize.STRING,
        },
        created_at: Sequelize.DATE,
        updated_at: Sequelize.DATE,
        
      }
  )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('USR_UserActivityLog')
  }
};
