import React from 'react';
import { Link, Route } from 'react-router-dom'
import {
    Container, Navbar,
    Nav, NavLink,
    UncontrolledDropdown,
    DropdownToggle, DropdownMenu,
    DropdownItem, Button, NavbarBrand, NavbarToggler, Collapse, Col, Row,
} from 'reactstrap';
import axios from 'axios';
import { SupportButton } from './SupportButton'
import { LanguageManager as LM } from "../LanguageManager/Language";
import './HeaderBar.css';
import { ColorLoader } from '../ColorLoader/ColorLoader';
import customer from "../../Assets/Icons/customer.svg";
import prvAreaIcon from "../../Assets/Icons/prvAreaIcon.svg";
import notifCenterIcon from "../../Assets/Icons/msg-icon.svg";
import exitIcon from "../../Assets/Icons/exitIcon.svg";
import ReactSVG from "react-svg";
import { SideMenu } from "../SideMenu/SideMenu";
import { config as SERVER_BASE } from "../../utils/Config";
import { getCurrentUserData, reloadData } from "../DataProvider/DataProvider"
import { AuthService } from '../../services/authService'





export class HeaderBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            error: '',
            user: { isLoggedIn: props.isLoggedIn },
            linkButtons: [{ name: LM.getString("main"), link: "main" },
            { name: LM.getString("chargeCancel"), link: "cancel" },
            { name: LM.getString("suppliersAndProducts"), link: "suppliers" },
            { name: LM.getString("resellers"), link: "resellers" },
            { name: LM.getString("configuration"), link: "config" },
            { name: LM.getString("reports"), link: "reports" }],
            userNameAndId: ''

        };
        this.toggle = this.toggle.bind(this);
        this.langChanger = this.langChanger.bind(this);
        this.logOutOfSystem = this.logOutOfSystem.bind(this);
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }


    logOutOfSystem() {
        AuthService.logout().then(res => {
            reloadData();
            if (res === "success") {
                this.setState({
                    error: '',
                    user: { isLoggedIn: false }
                }, () => {
                    this.props.authenticate({ isLoggedIn: this.state.isLoggedIn });
                });
            } else {
                this.setState({
                    error: res.data.message
                });
            }
        }).catch(err => {
            console.error(err);
        });
    }

    langChanger(e) {
        LM.setLang(e.target.innerHTML);
        let root = document.getElementsByTagName('body')[0];
        root.setAttribute('style', 'direction:' + LM.getDirection());
        this.setState({
            linkButtons: [{ name: LM.getString("main"), link: "main" },
            { name: LM.getString("chargeCancel"), link: "cancel" },
            { name: LM.getString("suppliersAndProducts"), link: "suppliers" },
            { name: LM.getString("resellers"), link: "resellers" },
            { name: LM.getString("configuration"), link: "config" },
            { name: LM.getString("reports"), link: "reports" }]
        });
    }

    // The method works but need to test its behavior without URL parametr

    redirectToMainMenuFunc = () => {
        this.props.history.go()
    }
    componentDidMount() {
        getCurrentUserData().then(
            res => {
                this.setState({ userNameAndId: res.details.businessName + " - " + res.details.business_identifier })
            }
        );

    }

    render() {
        let allowedLinkButtons = this.state.linkButtons.filter(l => AuthService.hasAllowedChild(l.link))
        return (

            <Navbar className='container_restriction' color="light" light expand="md" style={{ minHeight: "75px", border: "1px solid rgba(38,43,57,0.1)" }}>
                <Container className={'px-0'} fluid>
                    <NavbarToggler onClick={this.toggle} />
                    <NavbarBrand className="peletokLogo" onClick={this.redirectToMainMenuFunc} >פלאטוק</NavbarBrand> {/* TODO - get Logo src */}
                    <Nav className="my-auto d-xl-block d-none" style={{ alignSelf: "start" }} navbar>
                        <UncontrolledDropdown nav inNavbar>
                            <DropdownToggle nav caret>
                                {LM.getString("choosStyel")}
                            </DropdownToggle>
                            <DropdownMenu right>
                                <DropdownItem onClick={() => ColorLoader('main')}>
                                    Main Theme
                                    <svg width={10} height={10}>
                                        <rect width={10} height={10} style={{ fill: "#16577D" }} />
                                    </svg>
                                </DropdownItem>
                                <DropdownItem onClick={() => ColorLoader('neon')}>
                                    Neon City
                                    <svg width={10} height={10}>
                                        <rect width={10} height={10} style={{ fill: "#8134B3" }} />
                                    </svg>
                                </DropdownItem>
                                <DropdownItem onClick={() => ColorLoader('blue')}>
                                    Blue Lagoon
                                    <svg width={10} height={10}>
                                        <rect width={10} height={10} style={{ fill: "#144594" }} />
                                    </svg>
                                </DropdownItem>
                                <DropdownItem onClick={() => ColorLoader('fresh')}>
                                    Fresh
                                    <svg width={10} height={10}>
                                        <rect width={10} height={10} style={{ fill: "#1FA6A0" }} />
                                    </svg>
                                </DropdownItem>
                                <DropdownItem onClick={() => ColorLoader('earthly')}>
                                    Earthly Warm
                                    <svg width={10} height={10}>
                                        <rect width={10} height={10} style={{ fill: "#920000" }} />
                                    </svg>
                                </DropdownItem>

                            </DropdownMenu>
                        </UncontrolledDropdown>
                    </Nav>
                    <div className="my-auto d-flex d-md-none" style={{ alignSelf: "end" }}>
                        <SupportButton buttonLabel={LM.getString("supportButton")} />
                        <UncontrolledDropdown inNavbar className={'customer'}>
                            <DropdownToggle nav className={'p-0'}>
                                <Button outline className="d-flex py-2">
                                    <ReactSVG src={customer} />
                                    <span
                                        className={"d-lg-block d-none " + (LM.getDirection() === "rtl" ? "ml-1 mr-2" : "ml-2 mr-1")}>{this.state.userNameAndId}</span>
                                </Button>
                            </DropdownToggle>
                            <DropdownMenu right>
                                <DropdownItem className={'d-flex py-2'} tag={Link}
                                    to={`${this.props.match.url.slice(0, this.props.match.path.indexOf(":tab"))}main/NotificationCenter`}>
                                    <ReactSVG src={notifCenterIcon} />
                                    <span
                                        className={"d-lg-block d-none " + (LM.getDirection() === "rtl" ? "ml-1 mr-2" : "ml-2 mr-1")}>{LM.getString("notificationCenter")}</span>
                                </DropdownItem>
                                <DropdownItem className={'d-flex py-2'} tag={Link}
                                    to={`${this.props.match.url.slice(0, this.props.match.path.indexOf(":tab"))}main/userArea`}>
                                    <ReactSVG src={prvAreaIcon} />
                                    <span
                                        className={"d-lg-block d-none " + (LM.getDirection() === "rtl" ? "ml-1 mr-2" : "ml-2 mr-1")}>{LM.getString("personalArea")}</span>
                                </DropdownItem>
                                <DropdownItem className={'d-flex py-2'} onClick={this.logOutOfSystem}>
                                    <ReactSVG src={exitIcon} />
                                    <span
                                        className={"d-lg-block d-none " + (LM.getDirection() === "rtl" ? "ml-1 mr-2" : "ml-2 mr-1")}>{LM.getString('exit')}</span>
                                </DropdownItem>
                            </DropdownMenu>
                        </UncontrolledDropdown>
                    </div>

                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Row className={'page d-flex flex-row d-md-none'} navbar>
                            <Col className={' p-1 index col-sm-20'}>
                                <Route path={`${this.props.match.path}`} render={(routeProps => (<SideMenu {...routeProps} toggle={this.toggle.bind(this)} />))} />
                            </Col>
                        </Row>
                    </Collapse>
                    <Nav className="my-auto px-0 px-md-3 d-md-flex d-none" style={{ alignSelf: "center" }} navbar>
                        {allowedLinkButtons.map((obj) => {
                            return AuthService.allowed('/show_navbar_main_button') &&
                                <NavLink id={`${obj.link}Link`} tag={Link}
                                    to={`${this.props.match.url.slice(0, this.props.match.path.indexOf(":tab"))}${obj.link}`}
                                    style={{ color: "transparent" }}
                                    key={"headerBarLink-" + obj.link}>
                                    <span
                                        className={"font-weight-bold py-0 px-1 white-space headerLinkButton " +
                                            (this.props.match.params.tab === obj.link ? "headerLinkTextSelected" : "headerLinkText")}>
                                        {obj.name}
                                    </span>
                                </NavLink>
                        })}
                    </Nav>
                    <div className="my-auto d-md-flex d-none" style={{ alignSelf: "end" }}>
                        <SupportButton buttonLabel={LM.getString("supportButton")} />
                        <UncontrolledDropdown inNavbar className={'customer'}>
                            <DropdownToggle nav className={'p-0'}>
                                <Button id='customerBtn' outline className="d-flex py-2">
                                    <ReactSVG src={customer} />
                                    <span
                                        className={"d-lg-block d-none " + (LM.getDirection() === "rtl" ? "ml-1 mr-2" : "ml-2 mr-1")}>{this.state.userNameAndId}</span>
                                </Button>
                            </DropdownToggle>
                            <DropdownMenu right>
                                <DropdownItem id='notificationCenterDrop' className={'d-flex py-2'} tag={Link}
                                    to={`${this.props.match.url.slice(0, this.props.match.path.indexOf(":tab"))}main/NotificationCenter`}>
                                    <ReactSVG src={notifCenterIcon} />
                                    <span
                                        className={"d-lg-block d-none " + (LM.getDirection() === "rtl" ? "ml-1 mr-2" : "ml-2 mr-1")}>{LM.getString("notificationCenter")}</span>
                                </DropdownItem>
                                <DropdownItem id='userAreaDrop' className={'d-flex py-2'} tag={Link}
                                    to={`${this.props.match.url.slice(0, this.props.match.path.indexOf(":tab"))}main/userArea`}>
                                    <ReactSVG src={prvAreaIcon} />
                                    <span
                                        className={"d-lg-block d-none " + (LM.getDirection() === "rtl" ? "ml-1 mr-2" : "ml-2 mr-1")}>{LM.getString("personalArea")}</span>
                                </DropdownItem>
                                <DropdownItem id='logOutDrop' className={'d-flex py-2'} onClick={this.logOutOfSystem}>
                                    <ReactSVG src={exitIcon} />
                                    <span
                                        className={"d-lg-block d-none " + (LM.getDirection() === "rtl" ? "ml-1 mr-2" : "ml-2 mr-1")}>{LM.getString('exit')}</span>
                                </DropdownItem>
                            </DropdownMenu>
                        </UncontrolledDropdown>
                    </div>
                </Container>
            </Navbar>
        );
    }
}