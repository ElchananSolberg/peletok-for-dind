
/**
*Pay  for  Confidential number of phone 
* 
*/
var logs = require('../../logger')
var axios = require('axios')
const PaymentBasic = require('../paymentBasic');
const models = require('../../index');
const ResInfo = require('../../resInfo')
const LM = require('../../config/LanguageManager').LM
const {MongoLogger} = require('../../mongoLogger')

const productPrice = models.productPrice
const productPriceModel = productPrice.model
const SUCCESS_CODE = "1"
const basicUrl = "https://api.textvoice.co.il/"
const action = "ResellerSetBundle"
const name = "Peletop"
const pass = "dSq4@o31"

class Confidential extends PaymentBasic {
  constructor(phoneNumber, productId,req) {
    super()
    this.req=req
    this.productId = productId
    this.phoneNumber = phoneNumber
    this.info= new ResInfo(req)


    logs.info("phoneNumber is ", phoneNumber + " productId  is ", productId)
  }

  
  /**
   * function  to   buy for  confidential
   *  return response.data [num  of confirmationPayment ]
   * 
   */
  async  pay(transactionId) {
    const strQuery  = basicUrl +
      '?action=' + action + '&reseller_name=' + name +
      '&reseller_Password=' + pass +
      '&phone_number=' + this.phoneNumber + '&bundle=' + this.productId;
      if((await this.isDemoTransactionUser())){
        return this.demoRespons()
    }else{
      return await this.fill_detailes(strQuery, transactionId);
    }
    

    
    
  }
  /**
   * to send request to  pay   
   * return number  of   confirmation
   * 
   */
  fill_detailes(strQuery, transactionId) {
    return new Promise((resolve, reject) =>{
      axios.get(strQuery)
        .then((response) => {
              MongoLogger.logTransaction({
                  transactionId,
                  supplierId: this.req.params.supplierID,
                  requestParams: this.req.body,
                  request: strQuery,
                  response: response ,
              })    


          if (response.data["status"] == SUCCESS_CODE) {
            logs.info("confirmation is " + response.data["Approve Number"])
            this.info.confirmation=response.data["Approve Number"]
            this.info.isPaid=true
            this.info.send = { "success": {"cardNumber": response.data["Approve Number"]}}
            resolve(this.info)
          }else if (response.data ["mismatch"]== "phone_number") {
            this.info.error. err_in_SubscriberNumber=true
            this.info.setErrMessage(LM.getString("invalidSubscriber"),response.data["mismatch"]) 
            resolve(this.info)
          }else if (response.data ["mismatch"]) {
            this. info.error.err_in_Payment=true 
            this.info.setErrMessage(LM.getErrorBasic(),response.data["mismatch"])
            resolve(this.info)
          }else{this. info.error.err_in_Payment=true 
            this.info.setErrMessage(LM.getErrorBasic())}
          
        }).catch(e => {
          this.info.setErrMessage(LM.getErrorBasic(),"err_connection_site"+e.toString())
          this.info.error.err_connection_site = true
          return this.info
        })

      })
  }
}

module.exports = Confidential;





