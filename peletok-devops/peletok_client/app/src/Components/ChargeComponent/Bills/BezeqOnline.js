import React, { Component } from 'react';
import InputTypeNumber from '../../InputUtils/InputTypeNumber';
import InputTypePhone from '../../InputUtils/InputTypePhone';
import { Button, Container, Row, Col, Table } from 'reactstrap';
import { LanguageManager as LM } from '../../LanguageManager/Language';
import InputTypeCheckBox from '../../InputUtils/InputTypeCheckBox';
import InputTypeRadio from '../../InputUtils/InputTypeRadio';
import { getProducts, postBezeqPaymentDetails, postBezeqPayment } from '../../DataProvider/DataProvider';
import './BezeqOnline.css';
import { Notifications } from '../../Notifications/Notifications';
import { Loader } from '../../Loader/Loader';


export class BezeqOnline extends Component {

    constructor(props) {
        super(props)

        this.state = {
            billsPaymentObj: {},
            searchMethod: LM.getString("customer_id"),
            paymentProceedScreen: false,
            invoices: [],
            currentInvoiceNumber: ''
        }

        this.bezeqCheckBoxRef = {}

    }

    confirmDetails = () => {
        Loader.show()
        if (this.telephoneNum.getValue().valid && this.bezeqIdNumberInput.getValue().value != '' || this.receiptAmountInput.getValue().value != '') {
            let data = {

                idClient: this.bezeqIdNumberInput.getValue().value,
                price: this.receiptAmountInput.getValue().value,
                contractNumber: this.telephoneNum.getValue().value
            }
            postBezeqPaymentDetails(data, this.props.supplierId).then((res) => {
                   
                    Loader.hide()
                                        
                    this.setState({ invoices: res[0]["res"] ,ban:res[0]["ban"]}, () => { this.setState({ paymentProceedScreen: this.state.invoices.length > 0 }) })
                }
            ).catch(
                (err) => {
                    Loader.hide()
                    Notifications.show(err.response.data.error, 'danger')
                }
            )
        }
    }
    confirmPayment = () => {
        Loader.show()
        if(this.contactNumber.getValue().valid && this.TotalPayment.getValue().value != '') {
        let data = {
            itemId: this.state.billsPaymentObj.bills_payment[0].id,
            details: this.state.invoiceDetails,
            invToPay: this.state.currentInvoiceNumber,
            price: this.TotalPayment.getValue().value,
            phoneNumber: this.contactNumber.getValue().value,
            ban:this.state.ban
        }

        postBezeqPayment(data, this.props.supplierId).then((res) => {
                Loader.hide()
                Notifications.show(res.success.data, 'success')
            }
        ).catch(
            (err) => {
                Loader.hide()
                Notifications.show(err.response.data.error, 'danger')
            }
        )
        }
    }
    componentDidMount() {
        getProducts(this.props.supplierId).then(
            (res) => {
                this.setState({ billsPaymentObj: res })
            })
    }

    render() {

        let dataRows = this.state.invoices.map((invoice, index) => {
            return (
                <>
                    <tr>
                        <td><InputTypeCheckBox
                            id={`bezeqInvoice ${index}`}
                            onChange={() => {
                                return this.amountInput.setValue(invoice.invBalance), this.TotalPayment.setValue(invoice.invBalance),
                                    this.setState({ currentInvoiceNumber: invoice.invToPay, invoiceDetails: invoice.details })
                            }}

                            ref={(objReference) => { this.bezeqCheckBoxRef[index] = objReference }} />
                        </td>
                        <td>{invoice.Ben}</td>
                        <td>{invoice.invDueDate}</td>
                        <td>{invoice.invToPay}</td>
                        <td>{invoice.invBalance}</td>
                    </tr>
                </>
            )
        })

        return (
            <Container className='border'>
                <Row >
                    <Col>
                        <img className='bezeqOnlineImage' src={this.props.image} alt='bezeqOnline' />
                    </Col>
                </Row>
                <Row className='my-3 m-2' >
                    <Col sm='12' className='bezeqHeader'>
                        {LM.getString("bezeqOnlineHeader")}
                    </Col>
                </Row>
                {!this.state.paymentProceedScreen ?
                    <Container>
                        <Row >
                            <Col sm='3'>
                            </Col>
                            <Col sm='4'>
                                <InputTypePhone
                                    id='supplierTelnumTitle'
                                    title={LM.getString("supplierTelnumTitle")}
                                    required='true'
                                    prefixTwoDigits='true'
                                    ref={(componentObj) => { this.telephoneNum = componentObj }} />

                                {LM.getString("searchMethod")}
                                <InputTypeRadio
                                    id='searchMethodRadioButton'
                                    options={[LM.getString("customer_id"), LM.getString("receiptAmount")]}
                                    onChange={(e) => this.setState({ searchMethod: e.target.value }, () => this.bezeqIdNumberInput.setValue(''), this.receiptAmountInput.setValue(''))}
                                    default={LM.getString("customer_id")}
                                    ref={(componentObj) => { this.searchMethodRadioButton = componentObj }} />
                                <InputTypeNumber
                                    id='bezeqIdNumberInput'
                                    title={LM.getString("customer_id")}
                                    required={this.state.searchMethod == LM.getString("customer_id")}
                                    disabled={this.state.searchMethod == LM.getString("receiptAmount")}
                                    onChange={this.total}
                                    ref={(componentObj) => { this.bezeqIdNumberInput = componentObj }} />
                                <InputTypeNumber
                                    id='receiptAmountInput'
                                    title={LM.getString("receiptAmount")}
                                    disabled={this.state.searchMethod == LM.getString("customer_id")}
                                    required={this.state.searchMethod == LM.getString("receiptAmount")}
                                    ref={(componentObj) => { this.receiptAmountInput = componentObj }} />
                                <Button className='mx-0 p-1 regularButton' size='lg'
                                    onClick={this.confirmDetails}>
                                    {LM.getString("confirm")}
                                </Button>
                            </Col>
                        </Row>
                        <Row>
                            <Col sm='12' className='BottomCol border-radius-3 p-3 bezeqNote mt-4 mb-0 fs-26'>
                                {LM.getString("bezeqNote")}
                            </Col>
                        </Row>
                    </Container>
                    :
                    <Container>
                        <Row>

                            <Table className='cancelationTable' size='sm' bordered striped  >
                                <thead >
                                    <tr >
                                        <th ></th>
                                        <th >{LM.getString("invoiceCode")}</th>
                                        <th >{LM.getString("toPayBefore")}</th>
                                        <th >{LM.getString("invoiceNumberAdditional")}</th>
                                        <th > {LM.getString("balance")}</th>
                                    </tr>
                                </thead>
                                <tbody >
                                    {dataRows}
                                </tbody>
                            </Table >
                        </Row>
                        <Row>
                            <Col sm='4'>
                                <InputTypeNumber id='RecieptAmountInput'
                                    title={LM.getString("totalPayment")}
                                    required='true'
                                    onChange={this.totalAmountHandler}
                                    validationFunction={this.amountValidation}
                                    ref={(objReference) => { this.amountInput = objReference }}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col sm='4'>
                                <InputTypeNumber
                                    id='TotalPayment'
                                    title={LM.getString("TotalamountPayment")}
                                    ref={(componentObj) => { this.TotalPayment = componentObj }} />
                            </Col>
                        </Row>
                        <Row className='commissionNotesRow'>
                            {LM.getString("Commission0")}
                        </Row>
                        <Row>
                            <Col sm='4'>
                                <InputTypePhone
                                    id='cellularNumberTitle'
                                    title={LM.getString("supplierTelnumTitle")}
                                    required='true'
                                    ref={(componentObj) => { this.contactNumber = componentObj }} />
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Button size='lg' className='regularButton' onClick={this.confirmPayment}>
                                    {LM.getString("confirm")}
                                </Button>
                            </Col>
                        </Row>
                    </Container>
                }
            </Container>)
    }
}