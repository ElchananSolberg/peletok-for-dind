const express = require('express');
const activitySessionRouter = express.Router();
const models = require('../index.js');
const Op = require("sequelize").Op;
const deleteSessionByUser = require('../utils').deleteSessionByUser
const {PELETOK_BUSINESS_ID} = require('../constants')



/*-----------------------------    REST section    ---------------------------------*/



async function getUserWithBusiness(edited_user_id){
	let editedUser = await models.user.model.findOne({where:{id:edited_user_id},
	include:[
		{
			model:models.userBusiness.model,
			as: 'UserBusiness',
			include:[
				{
					model:models.business.model,
					as: 'Business'
				}
			]
		}
	]
	})
	return editedUser;
}

function allwedToUSer(req, user){
	
	return req.user.business_id == PELETOK_BUSINESS_ID || req.user.business_id == user.UserBusiness[0].Business.id || req.user.is_distributor && req.user.distributor_id == user.UserBusiness[0].Business.distributor_id;
}

activitySessionRouter.get('/:user_id', async (req, res) => {

	

	if (allwedToUSer(req, await getUserWithBusiness(req.params.user_id))) {
		let [ips,tokens, user] = await Promise.all([
			models.userIp.model.findAll({
				where: {user_id: req.params.user_id}
			}),
			models.userToken.model.findAll({
				where: {user_id: req.params.user_id}
			}),
			models.user.model.findOne({
				attribute: ['num_device_allowed', 'num_ip_allowed'],
				where: {id: req.params.user_id},
				raw: true
			}),
		])
		
		let response = {
			user,
			ips: ips.map(i => {return {id: i.id, ip_address: i.ip_address}}),
			tokens: tokens.map(t => {return {id: t.id, token: t.token.substring(0, 0) + '*******' + t.token.substring(12)}}),
		}
		res.send(response)
	}else {
		res.status(403).send('Unauthorized action')
	}


})

activitySessionRouter.post('/token/', async (req, res) => {
	let params = req.body
	if (allwedToUSer(req, await getUserWithBusiness(params.user_id))) {
		await models.user.model.update({
			num_device_allowed: params.num_device_allowed
		},{
			where: {id: params.user_id}
		})
		res.send('success')
	}else {
		res.status(403).send('Unauthorized action')
	}


})

activitySessionRouter.post('/ip/', async (req, res) => {
	let params = req.body
	if (allwedToUSer(req, await getUserWithBusiness(params.user_id))) {
		await models.user.model.update({
			num_ip_allowed: params.num_ip_allowed
		},{
			where: {id: params.user_id}
		})
		res.send('success')
	}else {
		res.status(403).send('Unauthorized action')
	}



})

activitySessionRouter.delete('/token/:token_id', async (req, res) => {
	

	let token = await models.userToken.model.findOne({where:{id:req.params.token_id},include:[

		{model:models.user.model,
		as: 'User',
		include:[
			{
				model:models.userBusiness.model,
				as:'UserBusiness',
				include:[

				{ 
					model:models.business.model,
					as: 'Business',
					attributes:['distributor_id']
				}
				]
			}
		]
	}]});


	if(allwedToUSer(req, token.User)){
		let userToDisconnect = token.User;
		token.destroy()
		try{
			deleteSessionByUser(req.sessionStore.sessions, userToDisconnect.id)
		}catch(e){
			console.log(e)
		}
			
		res.send({msg: 'success', token_id: token.id})

	} else {
		res.status(403).send('Unauthorized action')
	}




})

activitySessionRouter.delete('/ip/:ip_id', async (req, res) => {
	let ip = await models.userIp.model.findOne({where:{id:req.params.ip_id},include:[

		{model:models.user.model,
		as: 'User',
		include:[
			{
				model:models.userBusiness.model,
				as:'UserBusiness',
				include:[

				{ 
					model:models.business.model,
					as: 'Business',
					attributes:['distributor_id']
				}
				]
			}
		]
	}]});


	if(allwedToUSer(req, ip.User)){
		let userToDisconnect = ip.User;
		ip.destroy({
			where: {id: req.params.ip_id}
		})
		try{
			deleteSessionByUser(req.sessionStore.sessions, userToDisconnect.id)
		}catch(e){
			console.log(e)
		}
			
		res.send({msg: 'success', ip_id: ip.id})

	} else {
		res.status(403).send('Unauthorized action')
	}


	


})

activitySessionRouter.post('/ip/cancel', async (req, res) => {
	let params = req.body
	if (allwedToUSer(req, await getUserWithBusiness(params.user_id))) {
		
			await models.user.model.update({
				num_ip_allowed: null
			},{
				where: {id: params.user_id}
			})
			res.send('success')
	}else {
		res.status(403).send('Unauthorized action')
	}



})

activitySessionRouter.post('/token/cancel', async (req, res) => {
	let params = req.body
	if (allwedToUSer(req, await getUserWithBusiness(params.user_id))) {

		await models.user.model.update({
			num_device_allowed: null
		},{
			where: {id: params.user_id}
		})
		res.send('success')
	}else {
		res.status(403).send('Unauthorized action')
	}



})

module.exports.activitySessionRouter = activitySessionRouter;
