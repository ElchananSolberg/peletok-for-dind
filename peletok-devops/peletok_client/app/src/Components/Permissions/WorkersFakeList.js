/**
 * A fake list of users data
 */
export function ourWorkers() {
    return [
        {
            first_name: 'אברהם', last_name: "חג'ג'", israeli_id: "032564895", phone_number: '0502226666', account_mail: "ram@chagag.com", id_validity: "2013-01-08", city: "באר שבע", street: "התותים", number: '8',       entrance :  'a',
        floor : 2, username: 'rami111111', password: "jmy6y54g325", totalSales: 2345, lastLogin: '28/09/18', country: 'ישראל', userPass: '1q2w3e4r1', userid: 1, loginHistory: [{ num: 5, date: '25/12/2018', day: 'sun', hour: '18:06' }, { num: 4, date: '24/12/2018', day: 'sut', hour: '17:06' }, { num: 3, date: '23/12/2018', day: 'fry', hour: '8:06' }, { num: 2, date: '22/12/2018', day: 'thu', hour: '18:00' }, { num: 1, date: '21/12/2018', day: 'whe', hour: '01:06' }]


        },

        {
            first_name: 'יצחק', last_name: "חג'ג'", israeli_id: "032564895", phone_number: '0502226666', account_mail: "ram@chagag.com", id_validity:"2014-01-08", city: "באר שבע", street: "התותים", number: '8',       entrance :  'a',
        floor : 2, username: 'rami222222', password: "jmy6y54g325", totalSales: 2345, lastLogin: '28/09/18', country: 'ישראל', userPass: '1q2w3e4r1', userid: 2, loginHistory: [{ num: 5, date: '25/12/2018', day: 'sun', hour: '18:06' }, { num: 4, date: '24/12/2018', day: 'sut', hour: '17:06' }, { num: 3, date: '23/12/2018', day: 'fry', hour: '8:06' }, { num: 2, date: '22/12/2018', day: 'thu', hour: '18:00' }, { num: 1, date: '21/12/2018', day: 'whe', hour: '01:06' }]


        },

        {
            first_name: 'זלמן', last_name: "חג'ג'", israeli_id: "032564895", phone_number: '0502226666', account_mail: "ram@chagag.com", id_validity: "2015-01-08", city: "באר שבע", street: "התותים", number: '8',       entrance :  'a',
        floor : 2, username: 'rami333333', password: "jmy6y54g325", totalSales: 2345, lastLogin: '28/09/18', country: 'ישראל', userPass: '1q2w3e4r1', userid: 3, loginHistory: [{ num: 5, date: '25/12/2018', day: 'sun', hour: '18:06' }, { num: 4, date: '24/12/2018', day: 'sut', hour: '17:06' }, { num: 3, date: '23/12/2018', day: 'fry', hour: '8:06' }, { num: 2, date: '22/12/2018', day: 'thu', hour: '18:00' }, { num: 1, date: '21/12/2018', day: 'whe', hour: '01:06' }]


        },

        {
            first_name: 'יענקל', last_name: "חג'ג'", israeli_id: "032564895", phone_number: '0502226666', account_mail: "ram@chagag.com", id_validity: "2016-01-08", city: "באר שבע", street: "התותים", number: '8',       entrance :  'a',
        floor : 2, username: 'rami444444', password: "jmy6y54g325", totalSales: 2345, lastLogin: '28/09/18', country: 'ישראל', userPass: '1q2w3e4r1', userid: 4, loginHistory: [{ num: 5, date: '25/12/2018', day: 'sun', hour: '18:06' }, { num: 4, date: '24/12/2018', day: 'sut', hour: '17:06' }, { num: 3, date: '23/12/2018', day: 'fry', hour: '8:06' }, { num: 2, date: '22/12/2018', day: 'thu', hour: '18:00' }, { num: 1, date: '21/12/2018', day: 'whe', hour: '01:06' }]


        },

        {
            first_name: 'יונתן', last_name: "חג'ג'", israeli_id: "032564895", phone_number: '0502226666', account_mail: "ram@chagag.com", id_validity: "2017-01-08", city: "באר שבע", street: "התותים", number: '8',       entrance :  'a',
        floor : 2, username: 'rami555555', password: "jmy6y54g325", totalSales: 2345, lastLogin: '28/09/18', country: 'ישראל', userPass: '1q2w3e4r1', userid: 5, loginHistory: [{ num: 5, date: '25/12/2018', day: 'sun', hour: '18:06' }, { num: 4, date: '24/12/2018', day: 'sut', hour: '17:06' }, { num: 3, date: '23/12/2018', day: 'fry', hour: '8:06' }, { num: 2, date: '22/12/2018', day: 'thu', hour: '18:00' }, { num: 1, date: '21/12/2018', day: 'whe', hour: '01:06' }]


        }


    ]
}