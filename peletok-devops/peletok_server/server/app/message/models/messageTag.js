const BaseModel = require('../../BaseModel');
const DataTypes = require("sequelize").DataTypes;

/**
 * Tag MessageTag
 */
class MessageTag extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            mark: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false
            }
        };
        this.prefix = "MSG";
        this.author = true;
    }
    /**
     * creates associate for MessageTag model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.tagOfMessage.model, { as: 'TagOfMessage' });
        this.model.belongsTo(models.message.model, { as: 'Message' });
    }
}

module.exports = new MessageTag();