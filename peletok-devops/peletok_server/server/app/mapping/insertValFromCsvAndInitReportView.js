const fs = require("fs");
const pathLib = require("path");
const sequelizeInstance = require('../setupSequelize');
const logger = require('../logger')
const {isEmptyCell, getBoolean} = require("../utils");

const {getDropReportCommand, getSyncReportCommand} = require('../transaction/view/sqlQuery')

const {mapPathQuery, whereTable, mapFieldQuery, path, table} = require("./map_tables");

/***
 * Deletes all mapping data from DB
 * @returns {Promise<void>}
 */
async function deleteRoes(t) {
    await mapPathQuery.destroy({where: {}, transaction: t });
    await mapFieldQuery.destroy({where: {}, transaction: t });
    await path.destroy({where: {}, transaction: t });
    await table.destroy({where: {}, transaction: t });
}


/***
 * Inserts mapping data from one row (from csv file) to DB
 * @param row csv row with mapping data
 * @returns {Promise<void>}
 */
async function insertRow(row, t) {
    //TODO: split function to small functions
    let whereInstance;
    let pathId = (await path.findOrCreate({
        where: {'path': row["path"]},
        transaction: t 
    }))[0].id;
    let tableId = (await table.findOrCreate({
        where: {table_name: row["table"]},
        transaction: t 
    }))[0].id;
    let mapPathQueryInstance = await mapPathQuery.findOrCreate({
        where: {"path_id": pathId, "table_id": tableId},
        transaction: t 
    });
    const creation = getBoolean(row["post"]);
    const getting = getBoolean(row["get"]);
    const deleting = getBoolean(row["delete"]);
    let mapFieldQueryInstance = await mapFieldQuery.findOrCreate({
        where: {
            key_name: row["key"],
            field_name: row["field"],
            map_path_query_id: mapPathQueryInstance[0].id,
            creation: creation!==false,
            getting: getting!==false,
            updating: getBoolean(row["update"]),
            deleting: deleting!==false,
            sort_by_field: isEmptyCell(row["sort_by_field"])?null:row["sort_by_field"]
        },
        transaction: t 
    });
    if (getBoolean(row["where_key"])) {
        whereInstance = await whereTable.findOrCreate({
            where: {
                key_filter: row["where_key"],
                val_filter: row["where_value"]
            },
            transaction: t 
        });
        if (row["where_table"] === row["table"]) {
            if (row["type_of_filter"] === "col") {
                mapFieldQueryInstance[0].where_table_id = whereInstance[0].id;
                await mapFieldQueryInstance[0].save();
            } else {
                mapPathQueryInstance[0].where_table_id = whereInstance[0].id;
                await mapPathQueryInstance[0].save();
            }
        }

    }
    if (!isEmptyCell(row["through"])) {
        let through = row["through"].trim().split("-");
        for (let j = through.length - 1; j >= 0; j--) {
            let tableId = (await table.findOrCreate({
                where: {table_name: through[j]},
                transaction: t 
            }))[0].id;
            let previousInstance = mapPathQueryInstance[0];
            mapPathQueryInstance = await mapPathQuery.findOrCreate({
                where: {"path_id": pathId, "table_id": tableId},
                transaction: t 
            });
            if (row["where_table"] === through[j]) {
                mapPathQueryInstance[0].where_table_id = whereInstance[0].id;
                await mapPathQueryInstance[0].save({ transaction: t });
            }
            previousInstance.parent_id = mapPathQueryInstance[0].id;
            await previousInstance.save({ transaction: t });
        }
    }
}

/***
 * Inserts data to mapping tables
 * @param data data from csv (not sliced to lines)
 * @param deleteOld flag that says if delete old values from tables, default no
 * @returns {Promise<void>}
 */
async function insert(data, deleteOld) {
   const t = await sequelizeInstance.transaction();

    try {
        const arr = data.split(/\r?\n/); // Splits data to rows
        const headers = arr.shift().slice().split(",");
        const lenRow = headers.length;
        if (deleteOld) await deleteRoes(t);
        for (let i = 0; i < arr.length; i++) { // A loop that passes on each line
            let r = arr[i].split(",");
            let row = {};
            for (let i = 0; i < lenRow; i++) {
                let val = r[i];
                if (val) val = val.trim();
                row[headers[i]] = val
            }
            await insertRow(row, t);
        }
         await t.commit();

    } catch (error) {

      // If the execution reaches this line, an error was thrown.
      // We rollback the transaction.
      await t.rollback();
      logger.error(error)
      process.exit(1);

}
    
}

/**
 * Stores mapping data by csv file
 * @returns {Promise<void>}
 */
async function storeMap() {
    let strFile = await fs.readFileSync(pathLib.join(__dirname, "map.csv"), "utf8");
    await insert(strFile, true)

}

storeMap().then(()=>{
    logger.info("map stored");
    // init view
    sequelizeInstance.query(getDropReportCommand('REP_ReportView')).then(()=>{
        sequelizeInstance.query(getSyncReportCommand('REP_ReportView')).then(()=>{
        process.exit(0);
    }) 
    }).catch((err)=>{
        process.exit(1);

    })

})


