import React, { Component } from 'react';
import {
    Button, Input, FormFeedback,
    Label, FormGroup, Col
} from 'reactstrap';
import { LanguageManager as LM } from "../LanguageManager/Language"


export class ForgotPasswordSms extends Component {
    render() {
        return (
            <FormGroup>
                <Label>
                    {LM.getString("enterCodeSentToPhone")}: {this.props.phoneNumber}
                </Label>
                <Label size='sm'>{LM.getString('enterCodeNum')}</Label>
                <Input invalid={this.props.errMsg.length !== 0} size='sm' type="smsCode" name="smsCode" id="smsCode"
                    onChange={this.props.onChange} autoComplete='off'/>
                <FormFeedback id="enterCodeNumErrMsg" size='sm'>{this.props.errMsg}</FormFeedback>
                <Col md="5" sm="12" className='px-0 mt-2 mt-md-4'>
                    <Button block size='lg' className={'fs-17 border-radius-3 bc-blue-1 border-unset'} id="confirmSmsCode" onClick={this.props.onSubmit}>
                    {LM.getString("confirm")}
                </Button>
                </Col>
            </FormGroup>
        )

    }

}
