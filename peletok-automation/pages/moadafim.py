from selenium.webdriver.common.by import By


class Moadafim:
    product_name = (By.CLASS_NAME, "productName")
    favorites_cards = (By.XPATH, "//*[@class='allCardsHeaderSpan']/preceding-sibling::div""/div"
                                 "[@class='px-0 mb-4 col-sm-4 col-md-6 col-lg-4 col-xl-3']")
    all_cards = (By.XPATH, "//*[@class='allCardsHeaderSpan']/following-sibling::div""/div"
                           "[@class='px-0 mb-4 col-sm-4 col-md-6 col-lg-4 col-xl-3']")
