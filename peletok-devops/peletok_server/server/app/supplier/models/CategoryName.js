const DataTypes = require('sequelize').DataTypes;
const BaseModel = require('../../BaseModel');

/**
 * CategoryName model
 */
class CategoryName extends BaseModel {


    /**
     * configs fields and options
     */


    config() {
        this.attributes = {
            name: {
                type: DataTypes.STRING,
                allowNull: true
            }
        };
        this.prefix = "SUPL";

    }

    /**
     * creates associate for CategoryName model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.category.model, { as: 'Category' });
        this.model.belongsTo(models.language.model, { as: 'Language' });
    }
}

module.exports = new CategoryName();
