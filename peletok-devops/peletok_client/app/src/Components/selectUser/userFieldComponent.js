import React, { Component } from 'react';
import { Col, Card, CardBody, CardText, CardTitle, Input, FormFeedback } from 'reactstrap';
import './selectUser.css'
import { LanguageManager as LM } from '../LanguageManager/Language';
import { Confirm } from '../Confirm/Confirm';
import Asterisk from '../Ui/Asterisk'

/**
 * A component that returns a card with a title and a input box ,
 * Propes should move a size, field header, and function, and  you can also move a list for selection
 */
export class UserFieldComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isChange: false,
            invalid: false,
            fieldTitle: this.props.fieldTitle,
            errorMessage: this.props.errorMessage,
        };
    }

    getIsChange = (e) => {
        return this.state.isChange
    }
    setValue = () => {
        this.setState({ invalid: true })
    }
    onChange = (e) => {
        if (e.target.value.length > 0) {
            this.setState({ value: e.target.value })
            this.setState({ isChange: true })
            this.setState({ invalid: false })
        } else {
            this.setState({ isChange: false })
        }
        if (this.props.changeHandler) { this.props.changeHandler(e) }
    }
    isValidIsraeliID = (id) => {
        var id = String(id).trim();
        if (id.length > 9 || id.length < 5 || isNaN(id)) return false;

        // Pad string with zeros up to 9 digits
        id = id.length < 9 ? ("00000000" + id).slice(-9) : id;

        return Array.from(id, Number)
            .reduce((counter, digit, i) => {
                const step = digit * ((i % 2) + 1);
                return counter + (step > 9 ? step - 9 : step);
            }) % 10 === 0;
    }

    /** Validates the value to be a real correct email. */
    emailValidationFunction = (value) => {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(value);
    }
    Validation = (e) => {
        if (e.target.value.length == 0 && this.state.fieldTitle != LM.getString("selectUseridNum") && this.state.fieldTitle != LM.getString("selectUseridCardValidDate")) {
            this.setState({ invalid: true })
        }
        var isValid;

        switch (this.state.fieldTitle) {
            case LM.getString("selectUsermail"):
                isValid = this.emailValidationFunction(e.target.value)
                if (!isValid) {
                    Confirm.confirm(LM.getString("emailInnerValidationFunctionMsg") + ': email@example.com').then(() => { this.setState({ invalid: !this.state.invalid }) })
                }
                break;

            case LM.getString("selectUseridNum"):
                let isValid = this.isValidIsraeliID(e.target.value);
                if (isValid) {
                    return (isValid)
                } else {
                    Confirm.confirm(LM.getString("invalidIdNumber")).then(() => { this.setState({ invalid: !this.state.invalid }) })
                    break;
                }

            default:
                break;
        }

    }
    render() {

        var disabled = this.props.disabled;
        var providers = this.props.select && this.props.providers && this.props.providers.options ? this.props.providers.options.map((current, ind) => {
            return <option value={current} id={`option${current}`} defaultValue={ind === 0}>{current}</option>
        })
            : <React.Fragment />;

        return (this.props.select ?
            <Col sm={this.props.size} className={'edit_user_form'}>
                <Card>
                    <CardBody style={{ padding: "0.04rem 0.8rem" }}>
                        <div className={"card-text"} style={{ textAlign: "start", fontSize: "0.75rem", marginBottom: '0' }}>
                            {this.props.fieldTitle}
                            <Asterisk show={this.props.required} />
                        </div>
                        <Input className="transparentInputselect" style={{ border: 'none', boxShadow: 'none', minHeight: '2.3rem' }} id={`InputSelect${this.props.id}`} type='select' required={this.props.required} onChange={(e) => { this.props.changeHandler(e) }} >
                            {providers}
                        </Input>

                    </CardBody>

                </Card>
            </Col>

            :
            <Col sm={this.props.size} className={'edit_user_form'}>
                <Card>
                    <CardBody style={{ padding: "0.2rem 0.8rem" }}>
                        <CardTitle style={{ fontSize: "0.75rem", marginBottom: "0", textAlign: "start" }}>
                            {this.props.fieldTitle}
                            <Asterisk show={this.props.required} />
                        </CardTitle>
                        <CardText>
                            <Input style={{ minHeight: '2.3rem' }} disabled={disabled} type={this.props.type} id={`Input${this.props.id}`} className="transparentInput" onBlur={this.Validation} value={this.props.value} onChange={
                                (e) => { this.onChange(e); }} invalid={this.state.invalid} />
                        </CardText>
                    </CardBody>
                </Card>
                <p className='meassge'>{this.state.invalid ? this.state.errorMessage : <React.Fragment />}</p>
            </Col>
        )
    }
}
