const logger = require("../logger");

const GenericDBAction = require('./GenericDBAction');

const {Op} = require('sequelize');
const models = require('../index');

const {table, path, whereTable, mapPathQuery} = require("./map_tables");

const {isObjectEmpty, capitalize} = require("../utils");

const {filterResult, mergeDuplicate} = require('./fixDataFormat');

const filterSuffix = "_TF"; // temp filter
const sortFieldSuffix = "_SF"; // sort field

class GenericGet extends GenericDBAction {
    /***
     * Constructor
     * @param actionPath path of action (by mapping tables)
     * @param whereObject object with key value for where query (null for get all)
     * @param addedIdInto {Boolean} added id to any instance
     */
    constructor(actionPath, whereObject, addedIdInto=false) {
        super(actionPath);
        this.baseWhere["getting"] = true;
        this.mapByTable = {};
        this.addedIdInto=addedIdInto;
        this.include = {raw: true};
        if(whereObject)
            this.include['where'] = whereObject;
    }

    /**
     * Maps fields in DB to keys names for result query and maps fields by tables
     * @param fields fields for select query from DB
     * @param useKeyNameInResult {Boolean} called to the attribute the in key name (
     *  or called in the DB field name)
     * @return {Promise<void>}
     */
    async map(fields, useKeyNameInResult=true) {
        Object.values(fields).forEach(v => {
            let tableName = v["MapPathQuery.Table.table_name"];
            if (!this.mapByTable[tableName]) {
                this.mapByTable[tableName] = [];
            }
            let attr = {
                fieldName: v.field_name,
                asName: useKeyNameInResult?v.key_name:v.field_name,
                filter: {},
                sort_by_field: v["sort_by_field"]

            };
            if (v["WhereTable.key_filter"]) {
                attr["filter"] = {[v["WhereTable.key_filter"]]: v["WhereTable.val_filter"]};
            }
            this.mapByTable[tableName].push(attr)
            ;
        });
    }

    /***
     * Builds a include options for a query
     * @param parentId parent id in map_path_query table
     * @param includeObject object to add into it
     * @returns {Promise<void>}
     */
    async buildInclude(parentId, includeObject) {
        let findByParent = await mapPathQuery.findAll({
                where: {"parent_id": parentId},
                include:
                    [{model: table, as: "Table", attributes: ["table_name"]},
                        {model: path, as: "Path"}, {model: whereTable, as: "WhereTable"}]
            }
        );
        for (let i = 0; i < findByParent.length; i++) {
            let tableName = findByParent[i]["Table"].table_name;
            let asTag = capitalize(tableName);
            let whereQuery = {};
            let whereForQuery = findByParent[i]["WhereTable"];
            if (whereForQuery) {
                whereQuery[whereForQuery.key_filter] = whereForQuery.val_filter;
            }
            let includeThis = {
                model: models[tableName.charAt(0).toLowerCase() + tableName.slice(1)].model,
                required: false,
                as: asTag,
                attributes: (this.mapByTable[asTag] || []).map(i => [i.fieldName, i.asName]),
                where: whereQuery
            };
            if (!isObjectEmpty(includeThis.attributes)) {
                Object.values(this.mapByTable[asTag]).forEach(atr => {
                    if (!isObjectEmpty(atr.filter)) {
                        let filterField = Object.keys(atr.filter)[0];
                        includeThis.attributes.push([filterField,
                            `${filterField}${filterSuffix}`])
                    }
                    if(atr.sort_by_field){
                        includeThis.attributes.push([atr.sort_by_field,
                            `${atr.sort_by_field}${sortFieldSuffix}`])
                    }
                })
            }
            await this.buildInclude(
                findByParent[i].id, includeThis);
            if (!isObjectEmpty(includeThis["attributes"]) || !isObjectEmpty(includeThis["include"])) {
                if (!includeObject["include"]) {
                    includeObject["include"] = [];
                }
                if(this.addedIdInto) {
                    includeThis.attributes.push("id");
                }
                includeObject["include"].push(includeThis);
            }
        }
    }


    /***
     * Returns all info from DB by info tables
     * @param useKeyNameInResult {Boolean} called to the attribute the in key name (
     *  or called in the DB field name)
     * @return {Promise<Array of info<Model>>}
     */
    async get(useKeyNameInResult=true){
        await this.config();
        const allFields = await GenericDBAction.getFieldsToInclude(this.baseWhere);
        await this.map(allFields, useKeyNameInResult);
        this.include["attributes"] = (this.mapByTable[this.asTagOfMainTable] || []).map(i => [i.fieldName, i.asName]);
        this.include["attributes"].push("id");
        await this.buildInclude(this.startRow.id, this.include);
        let result = await models[this.lowerCaseNameOfMainTable].model.findAll(this.include).catch(err => {
            logger.error(JSON.stringify(this.include, null, 2));
            logger.error(err);
        });
        if(this.include.raw){
            try{
            filterResult(result, this.mapByTable);
            return mergeDuplicate(result, 'id');
            }catch (e) {
                logger.error(e);
            }
        }
        return result;
    }
}
module.exports.GenericGet = GenericGet;
module.exports.filterSuffix = filterSuffix;
module.exports.sortFieldSuffix = sortFieldSuffix;
