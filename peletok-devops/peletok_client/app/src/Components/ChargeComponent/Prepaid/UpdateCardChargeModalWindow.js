import React from 'react';
import { Button, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { LanguageManager as LM } from '../../LanguageManager/Language';
import '../Prepaid/UpdateCardChargeModalWindow.css'

export const UpdateCardChargeModalWindow = (props) => {

    let inputs = props.inputs;
    let updatedCard = props.updatedCard;
    let updatedCardCancelClick = props.updatedCardCancelClick;
    let updatedCardCharge = props.updatedCardCharge;
    // let updateDescription = props.updateDescription;
    return (
        <Modal size="lg"
            isOpen={props.isOpen}
            updateToggle={props.updateToggle}
            className={props.className}>

            <ModalHeader toggle={props.updateHeaderToggle}></ModalHeader>
            <ModalBody className='modalBody'>
                <Col sm='8' className='mx-1'>
                    {/* <Row className='d-flex mx-1'>
                        <span className='favoriteMessage'>{LM.getString('updateMessage')}</span>
                    </Row> */}
                    {/* <Row className='py-1 mx-1'>
                        <span className='updateDescriptiontext' >{updateDescription}</span>
                    </Row> */}
                    <Row >
                        <Col sm='8' className='mt-5'>
                            {inputs}
                        </Col>
                    </Row>
                </Col>
                <Col className='pointerEvent' sm='4'>
                    {updatedCard}
                </Col>
            </ModalBody>
            <ModalFooter className='modalFooter'>
                <Button id='updateModalWindowCancelClick' className='modalBtn' color="secondary" onClick={updatedCardCancelClick}>{LM.getString('cancel')}</Button>{' '}
                <Button id='updateModalWindowChargeClick' className='modalBtn' color="primary" onClick={updatedCardCharge}>{LM.getString('charge')}</Button>
            </ModalFooter>
        </Modal>)
}