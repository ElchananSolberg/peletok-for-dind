PermissonGroupsTypesEnum = {
    1: 'show',
    2: 'edit'
}

module.exports.PermissonGroupsTypesEnum = PermissonGroupsTypesEnum;