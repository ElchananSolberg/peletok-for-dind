import React from 'react';
import {Button, Modal, ModalBody, ModalHeader} from 'reactstrap';
import {SupportLayout} from "../SupportLayout/SupportLayout";
import ReactSVG from "react-svg";
import headPhones from "../../Assets/Icons/SupportIcons/headPhoneShape49x40.svg";
import {LanguageManager as LM} from "../LanguageManager/Language";


export class SupportButton extends React.Component {
    // TODO - get style from Ienat with icons
    constructor(props) {
        super(props);
        this.state = {
            modal: false
        };

        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    render() {
        return (
            <div className={'px-1'}>
                <Button onClick={this.toggle} className="regularButton d-flex py-2">
                    <ReactSVG src={headPhones}/>
                    <span className={"d-lg-block d-none " + (LM.getDirection() === "rtl" ? "ml-1 mr-2" : "ml-2 mr-1")}>{this.props.buttonLabel}</span>
                </Button>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className + " modal-dialog-centered modal-support"}>
                    <ModalHeader toggle={this.toggle} charCode="&#215;"/>
                    <ModalBody>
                        <SupportLayout supportLogin={true} />
                    </ModalBody>
                </Modal>
            </div>
        );
    }
}
