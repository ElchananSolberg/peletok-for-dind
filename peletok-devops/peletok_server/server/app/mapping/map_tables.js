const DataTypes = require('sequelize').DataTypes;

const sequelizeInstance = require('../setupSequelize');


/***
 * Defined tables for mapping requests to DB
 */

const prefix = "MAP_";

const table = sequelizeInstance.define("table", {
    table_name: DataTypes.STRING
}, {timestamps: false, underscored: true, tableName: prefix + "table"});

const path = sequelizeInstance.define("path", {
    path: DataTypes.STRING
}, {timestamps: false, underscored: true, tableName: prefix + "path"});


const mapPathQuery = sequelizeInstance.define("map_path_query", {
    from_exist: DataTypes.BOOLEAN
}, {timestamps: false, underscored: true, tableName: prefix + "map_path_query"});


const mapFieldQuery = sequelizeInstance.define("map_field_query", {
    key_name: DataTypes.STRING,
    field_name: DataTypes.STRING,
    creation: {type: DataTypes.BOOLEAN, allowNull: false},
    getting:  {type: DataTypes.BOOLEAN, allowNull: false},
    updating: {type: DataTypes.BOOLEAN, allowNull: true},
    deleting: {type: DataTypes.BOOLEAN, allowNull: false},
    sort_by_field: DataTypes.STRING(1024)
}, {timestamps: false, underscored: true, tableName: prefix + "map_field_query"});

const whereTable = sequelizeInstance.define("where_table", {
    key_filter: DataTypes.STRING,
    val_filter: DataTypes.STRING
}, {timestamps: false, underscored: true, tableName: prefix + "where_table"});

table.hasMany(mapPathQuery);
path.hasMany(mapPathQuery);
mapPathQuery.hasMany(mapFieldQuery);
mapPathQuery.hasMany(mapPathQuery, { as: "Parent", foreignKey: "parent_id"});
whereTable.hasMany(mapPathQuery);
whereTable.hasMany(mapFieldQuery);

mapPathQuery.belongsTo(table, {as: 'Table'});
mapPathQuery.belongsTo(path, {as: 'Path'});
mapFieldQuery.belongsTo(mapPathQuery, {as: 'MapPathQuery'});
mapPathQuery.belongsTo(mapPathQuery, {as: "Child", foreignKey: "parent_id"});
mapPathQuery.belongsTo(whereTable, {as: "WhereTable"});
mapFieldQuery.belongsTo(whereTable, {as: "WhereTable"});

module.exports.mapPathQuery = mapPathQuery;
module.exports.mapFieldQuery = mapFieldQuery;
module.exports.path = path;
module.exports.table = table;
module.exports.whereTable = whereTable;
