const BaseModel = require('../../BaseModel');

/**
 * BusinessDocument model
 */
class BusinessDocument extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.prefix = 'DOC';
    }
    /**
     * creates associate for businessDocument model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.business.model, { as: 'Business' });
        this.model.belongsTo(models.document.model, { as: 'Document' });
    }

}
// exporting instance
module.exports = new BusinessDocument();