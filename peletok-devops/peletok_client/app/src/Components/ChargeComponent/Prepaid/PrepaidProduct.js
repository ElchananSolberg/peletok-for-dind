import React, {Component} from 'react';
import {Col} from 'reactstrap';
import './PrepaidProduct.css'
import {SERVER_BASE, getMoreInfo} from '../../DataProvider/DataProvider';
import ReactSVG from 'react-svg';
import {LanguageManager as LM} from '../../LanguageManager/Language';
import {MoreInfo} from '../../MoreInfo/MoreInfo';
import ProductText from '../../Ui/ProductText'

export class PrepaidProduct extends Component {

    constructor(props) {
        super(props);
        this.state = {
            previewMoreInfo: false,
            moreInfo: null,
            product: null
        }

    }

    moreInfoHandler = (productId) => {
        return (e) => {
            e && e.preventDefault()
            getMoreInfo(productId).then((res) => {
                this.setState({
                    product: res,
                })
                this.previewClicked(e)
            })
        }
    }

    previewClicked = (e) => {
        e && e.preventDefault()
        this.setState({
            previewMoreInfo: !this.state.previewMoreInfo
        })
    }

    render() {
        return (
            <React.Fragment>
                <div onClick={this.props.updateClick} className='d-flex flex-column h-100 shadow border-r-5'>
                    <div className='d-flex'>
                        <div className='productName mx-1 px-2'>
                            {this.props.product.name}
                            <div className='recommendedTag mt-2'>
                                {this.props.recommended}
                            </div>
                        </div>

                        <div className={(LM.getDirection() === "rtl" ? "mr-auto" : "ml-auto")}>
                            <ReactSVG className={`${this.props.pointerEvents}`} onClick={(event) => {
                                this.props.onFavoriteStarClick(this.props.linkId, this.props.linkFavorite);
                                event.stopPropagation();
                                event.preventDefault();
                            }
                            }
                                      src={this.props.favoriteStarImage}
                                      svgStyle={{fill: this.props.favoriteStarColor}}/>
                        </div>
                    </div>
                    <div className="rowHeight display-linebreak mx-1 px-2 display-linebreak">
                        <ProductText product={this.props.product} withBr={true} styel={{marginTop: '30px'}}/>
                    </div>
                    <div className='d-flex mt-auto bottomBorderRadios'>
                            <Col xs='6'>
                                {this.props.product.image &&
                                    <img className='contractPurchase'
                                         src={SERVER_BASE + this.props.product.image} alt=''
                                    />
                                }
                            </Col>

                        <Col className={'priceFrame px-0 ' + (LM.getDirection() === 'rtl' ? 'mr-auto' : 'ml-auto')}>
                            <div className='priceText'>
                                <span>&#8362;</span>
                                {this.props.product.price}
                            </div>
                        </Col>
                    </div>

                </div>
                {this.props.showMoreInfo != false &&
                <div className="product-more-details"
                     onClick={this.moreInfoHandler(this.props.product.id)}>{LM.getString('more_details')}
                    {this.state.product && <MoreInfo
                        color={this.state.product.supplier_color || '#16577d'}
                        product={this.state.product}
                        isOpen={this.state.previewMoreInfo}
                        toggle={() => {
                            this.previewClicked()
                        }}
                    />}
                </div>

                }
            </React.Fragment>
        )
    }
}
