const BaseModel = require('../../BaseModel');
const DataTypes = require("sequelize").DataTypes;

/**
 * Tag model
 */
class Tag extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            tag_name: {
                type: DataTypes.STRING(128),
                allowNull: true
            },
            order: {
                type: DataTypes.STRING,
                allowNull: true
            },
            type: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            show_on_product: {
                type: DataTypes.BOOLEAN,
                allowNull: true
            }
        };
        this.prefix = "TAG";
        this.author = true;
    }
    /**
     * creates associate for Tag model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.hasMany(models.supplierTag.model, {as: 'SupplierTag' });
        // this.model.hasMany(models.businessTag.model, {as: 'BusinessTag' });
        this.model.belongsToMany(models.product.model, {through: models.productTag.model, as: 'Products'});
        this.model.belongsToMany(models.business.model, {through: models.businessTag.model, as: 'Businesses'});
    }
}

module.exports = new Tag();