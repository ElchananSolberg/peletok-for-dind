from tests.base_project_test_class import *


class TestBezeqPage(BaseProjectTestClass):

    def login_and_get_to_bezaqonline(self):
        self.login()
        self.driver.tools.wait_and_click(HomePage.bezaqonline_icon)

    def test_bezaqonline_click_on_back_button(self):
        self.login_and_get_to_bezaqonline()
        self.click_on_back_botton()
        self.after_the_click_back_button()
        self.remove_logs_dir = True

    def enter_pre_phone_num_and_num(self):
        self.driver.tools.set_text(BezaqOnlinePage.pre_phone_number, User.phone[:3])
        self.driver.tools.set_text(BezaqOnlinePage.phone_number, User.phone[2:])

    def click_radio_btn(self, name):
        # function gets name to choose what process it should go through
        radio_btn_tz = self.driver.wait.wait_for_element_to_be_present(BezaqOnlinePage.radio_btn_tz)
        if name == 'tz':
            if not radio_btn_tz.is_selected():
                tz_btn = self.driver.wait.wait_for_element_to_be_present(BezaqOnlinePage.radio_btn_tz)
                self.driver.driver.execute_script('arguments[0].click();', tz_btn)
        if name == 'Invoice amount':
            invoice_amount_btn = self.driver.wait.wait_for_element_to_be_present(
                BezaqOnlinePage.radio_btn_Invoice_amount)
            self.driver.driver.execute_script('arguments[0].click();', invoice_amount_btn)

    def enter_text_into_inputs(self, name):
        # function gets name to choose what process it should go through
        if name == 'tz':
            self.driver.tools.set_text(BezaqOnlinePage.ID_client, User.phone[:10])
        if name == 'Invoice amount':
            self.driver.tools.set_text(BezaqOnlinePage.Invoice_amount, '99')

    # the whole page is a bug therefore untill page sorted test is counted as a test
    # def test_bezaqonline_tz(self):
    #     self.login_and_get_to_bezaqonline()
    #     self.enter_pre_phone_num_and_num()
    #     self.click_radio_btn('tz')
    #     self.enter_text_into_inputs('tz')
    #
    # def test_bezaqonline_Invoice_amount(self):
    #     self.login_and_get_to_bezaqonline()
    #     self.enter_pre_phone_num_and_num()
    #     self.click_radio_btn('Invoice amount')
    #     self.enter_text_into_inputs('Invoice amount')
