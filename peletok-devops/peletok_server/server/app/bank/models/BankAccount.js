const DataTypes = require('sequelize').DataTypes;
const BaseModel = require('../../BaseModel');
/**
 * BankAccount model
 */

class BankAccount extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            bank_name: {
                type: DataTypes.STRING,
                allowNull: true
            },
            bank_branch: {
                type: DataTypes.STRING,
                allowNull: true
            },
            bank_account_number: {
                type: DataTypes.STRING,
                allowNull: true
            }


        }
        this.author = true;
        this.statusAuthor = true;
        this.statusField = true;
        this.statusTimestampField = true;
        this.prefix = 'BNK';
    }
    /**
     * creates associate for BankAccount model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.hasMany(models.branchBankAccount.model, {as: 'BranchBankAccount' });

    }
}

module.exports = new BankAccount();
