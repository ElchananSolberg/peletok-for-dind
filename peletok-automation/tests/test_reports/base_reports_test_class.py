from tests.base_project_test_class import *
from appium_selenium_driver.elements_tools.select_element import SelectElement
from datetime import datetime, timedelta, date


class BaseReportsTestClass(BaseProjectTestClass):

    def login_and_get_in_report(self, report_href_num, title):
        self.login_and_click_on_side_menu_option("מצב חשבון")
        self.driver.tools.wait_and_click(
            (GeneralReportsPage.report_buttons_by_href_number[0],
             GeneralReportsPage.report_buttons_by_href_number[1].format(report_href_num)))
        self.driver.wait.wait_for_element_to_be_present((GeneralReportsPage.title[0], GeneralReportsPage.title[1].
                                                         format(title)))

    def login_and_get_in_report_manager_area(self, report_href_num, view_report_button):
        self.login_and_click_on_side_menu_option_in_reports_manager_area("דוחות")
        self.driver.tools.wait_and_click(
            (GeneralElementsInReportsAreaManager.report_buttons_by_href_number[0],
             GeneralElementsInReportsAreaManager.report_buttons_by_href_number[1].format(report_href_num)))
        self.driver.wait.wait_for_element_to_be_present(view_report_button)

    def login_and_get_in_report_manager_area_hashavshevet(self, report_href_str, view_report_button):
        self.login_and_click_on_side_menu_option_in_reports_manager_area("חשבשבת")
        self.driver.tools.wait_and_click(
            (GeneralElementsInReportsAreaManagerHashavshevet.report_buttons_by_href_string[0],
             GeneralElementsInReportsAreaManagerHashavshevet.report_buttons_by_href_string[1].format(report_href_str)))
        self.driver.wait.wait_for_element_to_be_present(view_report_button)

    def check_if_got_options_in_which_select(self, select):
        self.driver.tools.wait_and_click(select)
        all_options = self.driver.tools.get_text_from_element(select)
        options_names = [option.strip() for option in all_options.split('\n')]
        options_names = list(dict.fromkeys(options_names))
        assert len(options_names) > 1, "no options to present"

    def check_if_current_date_in_input(self, input_selector):
        current_date = date.today().__str__()
        assert current_date in self.driver.tools.wait_for_element_and_get_attribute(
            input_selector, "value")

    def insert_current_time_to_select_and_check(self, hours_selector, minutes_selector):
        hours_select_element = SelectElement(self.driver, self.driver.wait, hours_selector)
        minutes_select_element = SelectElement(self.driver, self.driver.wait, minutes_selector)
        current_hour = f"{datetime.now().hour:02d}"
        hours_select_element.choose_option_by_text(current_hour)
        current_minutes = f"{datetime.now().minute:02d}"
        minutes_select_element.choose_option_by_text(current_minutes)

    def insert_minute_and_check(self, select_selector, minute=2):
        select_element = SelectElement(self.driver, self.driver.wait, select_selector)
        select_element.choose_option_by_text(f"{minute:02d}")
        assert f"{minute:02d}" in self.driver.tools.get_text_from_element(select_selector)

    def insert_hour_and_check(self, select_selector, hour=2):
        select_element = SelectElement(self.driver, self.driver.wait, select_selector)
        select_element.choose_option_by_text(f"{hour:02d}")
        assert f"{hour:02d}" in self.driver.tools.get_text_from_element(select_selector)

    def insert_number_to_input_and_verify(self, number):
        self.driver.wait.wait_for_element_to_be_present(GeneralReportsPage.deal_number_input)
        self.driver.tools.set_text(GeneralReportsPage.deal_number_input, number)
        assert self.driver.tools.wait_for_element_and_get_attribute(GeneralReportsPage.deal_number_input,
                                                                    'value') == str(number)

    def click_on_show_reports_btn_and_check(self):
        self.driver.tools.wait_and_click(GeneralReportsPage.show_report_btn)
        if not self.driver.wait.wait_for_element_to_be_present(
                GeneralReportsPage.no_report_alert, raise_exception=False, timeout=10):
            self.driver.wait.wait_for_element_to_be_present(GeneralReportsPage.yech_report_responsive_table)

    def check_total_sales_span(self, end_btn_id):
        self.driver.tools.wait_and_click((GeneralReportsPage.end_btn_in_table[0],
                                          GeneralReportsPage.end_btn_in_table[1].format(end_btn_id)),
                                         raise_exception=False)
        total_sales = self.driver.tools.get_text_from_element(GeneralReportsPage.total_sales_from_table)
        assert total_sales in self.driver.tools.get_text_from_element(GeneralReportsPage.total_sales_span)

    def click_on_show_reports_btn_in_reports_manager_area_and_check(self, view_report_button, alert_or_msg):
        self.driver.tools.wait_and_click(view_report_button)
        if not self.driver.wait.wait_for_element_to_be_present(
                alert_or_msg, raise_exception=False):
            self.driver.wait.wait_for_element_to_be_present(
                GeneralElementsInReportsAreaManager.table_of_reports_first_row_in_table)

    def export_report_to_excel_button(self, show_reports_btn):
        self.driver.tools.wait_and_click(show_reports_btn)
        if not self.driver.wait.wait_for_element_to_be_present(
                GeneralReportsPage.no_report_alert, raise_exception=False):
            self.driver.wait.wait_for_element_to_be_present(
                GeneralElementsInReportsAreaManager.button_export_to_excel)
        self.remove_logs_dir = True

    def check_dates_report_span(self):
        assert self.driver.tools.wait_for_element_and_get_attribute(
            GeneralReportsPage.start_date_input, "value") \
               and self.driver.tools.wait_for_element_and_get_attribute(
            GeneralReportsPage.end_date_input, "value") \
               in self.driver.tools.get_text_from_element(GeneralReportsPage.report_dates_span, timeout=10), \
            "The input date dont matches the button's text"

    def fill_all_dates_fields_on_report_pages(self, start_date=(date.today() -
                                                                timedelta(days=60)).strftime('%m %d %Y'),
                                              end_date=(date.today()).strftime('%m %d %Y')):
        self.driver.tools.set_text(GeneralReportsPage.start_date_input, text=start_date)
        self.driver.tools.set_text(GeneralReportsPage.end_date_input, text=end_date)
        self.driver.tools.set_text(GeneralReportsPage.start_hours_select, text='23')
        self.driver.tools.set_text(GeneralReportsPage.end_hours_select, text='23')
        self.driver.tools.set_text(GeneralReportsPage.start_minutes_select, text='59')
        self.driver.tools.set_text(GeneralReportsPage.end_minutes_select, text='59')

    def check_hour_minute_in_input(self):
        assert "00" == self.driver.tools.wait_for_element_and_get_attribute(
            GeneralReportsPage.start_hours_select, attribute="value"), 'wrong hour in start input date'
        assert "00" == self.driver.tools.wait_for_element_and_get_attribute(
            GeneralReportsPage.start_minutes_select, attribute="value"), 'wrong minute in start input date'
        assert "23" == self.driver.tools.wait_for_element_and_get_attribute(
            GeneralReportsPage.end_hours_select, attribute="value"), 'wrong hour in end input date'
        assert "59" == self.driver.tools.wait_for_element_and_get_attribute(
            GeneralReportsPage.end_minutes_select, attribute="value"), 'wrong minute in end input date'

        assert "00" == self.driver.tools.wait_for_element_and_get_attribute(GeneralReportsPage.start_hours_select, attribute="value"), 'wrong hour in start input date'
        assert "00" == self.driver.tools.wait_for_element_and_get_attribute(GeneralReportsPage.start_minutes_select, attribute="value"), 'wrong minute in start input date'
        assert "23" == self.driver.tools.wait_for_element_and_get_attribute(GeneralReportsPage.end_hours_select, attribute="value"), 'wrong hour in end input date'
        assert "59" == self.driver.tools.wait_for_element_and_get_attribute(GeneralReportsPage.end_minutes_select, attribute="value"), 'wrong minute in end input date'

    def check_select_btn_rows_per_page(self):
        select_object = SelectElement(self.driver, self.driver.wait, GeneralReportsPage.rows_doh_per_pages_select)
        for option in select_object.get_all_options():
            option.click()
            self.driver.wait.wait_for_element_to_be_present(GeneralReportsPage.td_rows_display_in_reponsive_table)
            assert option.text == len(self.driver.wait.wait_for_elements_to_be_present(
                GeneralReportsPage.rows_display_in_reponsive_table)), "the diplaying of all reports doesn't work"

    def check_paginator_buttons_of_doh(self, report_page_name):
        self.driver.tools.wait_and_click((GeneralReportsPage.end_btn_in_table[0],
                                         GeneralReportsPage.end_btn_in_table[1].format(report_page_name)))
        self.driver.tools.wait_and_click((GeneralReportsPage.previous_btn_in_table[0],
                                         GeneralReportsPage.previous_btn_in_table[1].format(report_page_name)))
        self.driver.tools.wait_and_click((GeneralReportsPage.start_btn_in_table[0],
                                         GeneralReportsPage.start_btn_in_table[1].format(report_page_name)))
        self.driver.tools.wait_and_click((GeneralReportsPage.next_btn_in_table[0],
                                         GeneralReportsPage.next_btn_in_table[1].format(report_page_name)))
        self.driver.tools.wait_and_click((GeneralReportsPage.digits_btn_in_table[0],
                                         GeneralReportsPage.digits_btn_in_table[1].format(report_page_name, '0')))
        # TODO: also verify if all changes of display (by change number of rows) really append

        assert "00" == self.driver.tools.wait_for_element_and_get_attribute(GeneralReportsPage.start_hours_select,
                                                                            attribute="value"), 'wrong hour in start input date'
        assert "00" == self.driver.tools.wait_for_element_and_get_attribute(GeneralReportsPage.start_minutes_select,
                                                                            attribute="value"), 'wrong minute in start input date'
        assert "23" == self.driver.tools.wait_for_element_and_get_attribute(GeneralReportsPage.end_hours_select,
                                                                            attribute="value"), 'wrong hour in end input date'
        assert "59" == self.driver.tools.wait_for_element_and_get_attribute(GeneralReportsPage.end_minutes_select,
                                                                            attribute="value"), 'wrong minute in end input date'

    def view_by_marketers_radio_button_reports_manager_general(self):
        self.driver.tools.wait_and_click(GeneralElementsInReportsAreaManager.radio_button)

    def phone_number_input(self):
        self.driver.tools.set_text(ActionReportPage.phone_number_input, User.phone)
        assert User.phone in self.driver.tools.wait_for_element_and_get_attribute(
            ActionReportPage.phone_number_input, 'value'), "phone number does not match"

    def login_and_navigate_to_reports_manager(self):
        self.login()
        self.driver.tools.wait_and_click(NavBarMainPage.reports)
        self.driver.tools.wait.wait_for_element_to_be_present(GeneralElementsInReportsAreaManager.business_name_input)

    def login_and_click_on_side_menu_option_in_reports_manager_area(self, option_from_menu):
        self.login_and_navigate_to_reports_manager()
        self.driver.tools.wait_and_click(
            (GeneralReportsPage.side_menu_options[0], GeneralReportsPage.side_menu_options[1].format(option_from_menu)))
