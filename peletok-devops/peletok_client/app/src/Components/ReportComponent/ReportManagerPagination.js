import React, {Component} from 'react';
import {Container, Row, Input, FormGroup, Pagination, PaginationItem, PaginationLink} from 'reactstrap';
import InputTypeSearchList from "../InputUtils/InputTypeSearchList";

export class ReportManagerPagination extends Component {
    constructor() {
        super();
        this.state = {
            render: false,
            currentPage: 1,
            postsPerPage: 10,
        };
        this.pageNumbers = [];
        this.pageNumbersView = [];
        this.lastPageNumbers = 0;
        this.rows = [10, 25, 50, 100]

    }

    componentDidMount() {
        this.setState({render: true});
        this.indexOfLastPost = this.state.currentPage * this.state.postsPerPage;
        this.indexOfFirstPost = this.indexOfLastPost - this.state.postsPerPage;
        this.getFilteredList();
    }

    componentDidUpdate() {
        this.pageNumbers = [];
        for (let i = 1; i <= Math.ceil(this.props.tableRows.length / this.state.postsPerPage); i++) {
            this.pageNumbers.push(i);
        }
        if (this.state.currentPage < 4){
            this.pageNumbersView = this.pageNumbers.slice(0 ,this.pageNumbers[4])
        }else this.pageNumbersView = this.pageNumbers.slice(this.state.currentPage - 3 ,this.state.currentPage + 2)
        console.log(this.pageNumbersView)
        this.lastPageNumbers = this.pageNumbers.slice(-1)[0];
        this.indexOfLastPost = this.state.currentPage * this.state.postsPerPage;
        this.indexOfFirstPost = this.indexOfLastPost - this.state.postsPerPage;
    }

    changeRows = (e) => {
        this.setState({
            postsPerPage: e.target.value
        });
        this.first()
    };


    paginate = (pageNumber) => {
        this.setState({
            currentPage: pageNumber,
            openReport: false
        }, ()=> {
            this.getFilteredList();
            this.setState({openReport: true})
        })
    };

    first = () => {
        this.setState({
            currentPage: 1,
            openReport: false
        }, ()=> {
            this.getFilteredList();
            this.setState({openReport: true})
        })
    };

    previous = () => {
        this.setState({
            currentPage: this.state.currentPage - 1,
            openReport: false
        }, ()=> {
            this.getFilteredList();
            this.setState({openReport: true})
        })
    };

    next = () => {
        this.setState({
            currentPage: this.state.currentPage + 1,
            openReport: false
        }, ()=> {
            this.getFilteredList();
            this.setState({openReport: true})
        })
    };

    last = (pageNumber) => {
        this.setState({
            currentPage: pageNumber,
            openReport: false
        }, ()=> {
            this.getFilteredList();
            this.setState({openReport: true})
        })
    };

    getFilteredList(){
        this.props.getFilteredList(this.props.tableRows.slice(this.indexOfFirstPost, this.indexOfLastPost))
    }

    render() {
        return (
            <Container>
                {this.state.render &&
                <Row className={'justify-content-between mt-2'}>
                    {this.pageNumbers.length > 1 ?
                        <Pagination aria-label="Page navigation example">
                            <PaginationItem disabled={this.state.currentPage === 1}>
                                <PaginationLink first onClick={() => this.first()}/>
                            </PaginationItem>
                            <PaginationItem disabled={this.state.currentPage === 1}>
                                <PaginationLink previous onClick={() => this.previous()}/>
                            </PaginationItem>
                            {this.pageNumbersView.map(number => (
                                <PaginationItem key={number} active={number === this.state.currentPage}>
                                    <PaginationLink onClick={() => this.paginate(number)}>
                                        {number}
                                    </PaginationLink>
                                </PaginationItem>
                            ))}
                            <PaginationItem disabled={this.state.currentPage === this.lastPageNumbers}>
                                <PaginationLink next onClick={() => this.next()}/>
                            </PaginationItem>
                            <PaginationItem disabled={this.state.currentPage === this.lastPageNumbers}>
                                <PaginationLink last onClick={() => this.last(this.lastPageNumbers)}/>
                            </PaginationItem>
                        </Pagination>
                        : <div></div>
                    }
                    <FormGroup>
                        {/*<Label for="exampleSelect">Select</Label>*/}
                        <Input type="select" name="select" id="selectNum" className={'selectNumPagination'}
                               onChange={this.changeRows}>
                                {this.rows.map(rows => <option key={rows} value={rows}>{rows}</option>)}
                        </Input>
                    </FormGroup>
                </Row>
                }
            </Container>
        )
    }
}

