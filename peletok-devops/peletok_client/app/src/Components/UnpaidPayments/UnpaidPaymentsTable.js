import React from 'react';
import { Button, Table, Row, Col } from 'reactstrap';
import UnpaidTableRow from './UnpaidTableRow.js';
import UnpaidTableHead from './UnpaidTableHead.js';
import { Scrollbars } from 'react-custom-scrollbars';
import InputTypeCheckBox from '../InputUtils/InputTypeCheckBox'
import { LanguageManager as LM } from "../LanguageManager/Language";
import { postUnpaidBill } from '../DataProvider/Functions/DataPoster';

/**
 * UnpaidPaymentsTable - component that builds the table from the data (this.props.tableRows and this.props.tableHeadersObj) sended by the father component.
 * It use UnpaidTableHead and TableRow components.
 */
export default class UnpaidPaymentsTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            InvoiceList: [],
            inputTypeCheckBoxIncludingPayments: {
                disabled: false,
                lableText: LM.getString("IncludingPayments"),
                id: "IncludingPayments",
            },
        }
        this.checkboxes = {};
    }
    markAll = () => {

        Object.keys(this.checkboxes).forEach(async (key) => {
            await this.checkboxes[key].checkMy();
        })
    }
    unMarkAll = () => {

        Object.keys(this.checkboxes).forEach((key) => {
            this.checkboxes[key].unCheckMy();
        })
        this.setState({ InvoiceList: [] })

    }
    onChecked = async (isChecked, contractNumber) => {

        if (isChecked) {
            this.setState(prevState => ({
                InvoiceList: [...prevState.InvoiceList, contractNumber]
            }))

        }
        if (!isChecked) {
            this.setState(prevState => ({
                InvoiceList: prevState.InvoiceList.filter(el => el != contractNumber)
            }));
        }
    }

    pay = () => {
        postUnpaidBill({ transaction_id: this.state.InvoiceList }).then(
            (res) => {
            }
        ).catch(
            (err) => {
                alert(err);
            }
        )
    }
    render() {
        var key = 0;
        var tableRows = this.props.tableRows;
        var trId = 'TableRowID';
        var index = 0;
        var trArray = tableRows.map((current) => {
            return <tr key={key++}>
                <UnpaidTableRow ref={(componentObj) => { this.checkboxes[current.orderNumber] = componentObj }} id={trId + index++} onChecked={this.onChecked} tableRowObj={current} />
            </tr>
        });

        return (
            <React.Fragment>
                <div className={'d-flex w-100 mb-3'}>
                    <div className={(LM.getDirection() === "rtl" ? "mr-auto" : "ml-auto")}>
                        <Button id={"markAll"} onClick={this.markAll} style={{ marginLeft: '1px' }} className="commissionProfileButton CommissionProfileSmallButton">{LM.getString('markAll')}</Button>
                        <Button id={"unMarkAll"} onClick={this.unMarkAll} style={{ marginLeft: '1px' }} className="CommissionProfileDarkButton CommissionProfileSmallButton" >{LM.getString('unMarkAll')}</Button>
                    </div>
                </div>
                <div className={'table-responsive table-css ' + (LM.getDirection() === "rtl" ? "table-rtl" : "")}>
                    <Scrollbars style={{ width: '100%', height: '490px' }}>
                        <Table bordered striped size="sm" >
                            <thead>
                                <tr>
                                    <UnpaidTableHead tableHeadersObj={this.props.tableHeadersObj} />
                                </tr>
                            </thead>
                            <tbody >
                                {trArray}
                            </tbody>
                            <tfoot >
                                <tr >
                                    <th colSpan="6">
                                        <div style={{ minHeight: '23.5px' }} />
                                    </th>
                                </tr>
                            </tfoot>
                        </Table>
                    </Scrollbars>
                </div>
                <Col style={{ marginTop: '2%' }}>
                    <InputTypeCheckBox id={"IncludingPayments"}{...this.state.inputTypeCheckBoxIncludingPayments} ref={(componentObj) => { this.IncludingPayments = componentObj }}
                        onChange={this.outerOnChange} />
                </Col>
                <Row>
                    <Col lg="2" id={"payCheckedInvoices"} style={{ marginTop: '15px', }}>
                        <Button style={{ width: '100%', overflow: 'hidden' }} className="commissionProfileButton " onClick={this.pay}> {LM.getString('payCheckedInvoices')} </Button>
                    </Col>
                </Row>
            </React.Fragment >
        );
    }
}