const IecBill = require("./transaction/iecBill")
const IecMatam = require("./transaction/iecMatam")
const CityPayment = require("./transaction/cityPayment")
const BezeqOnline=require("./transaction/bezeq")
const ResInfo = require('../resInfo')
let reportView = require("../transaction/view/ReportView")
const models = require("../index")
const LM = require('../config/LanguageManager').LM
const cellcomId = "94"
const pelephoneId = "95"
const confidentialId = "196"
const partnerId = "127"
const manualCardId = "1"
const highway6Id = "187"
const carmelTunnelsId = "192"
const iecBillId = "185"
const iecMatamId = "186"
const cityPayId = "193"
const bezeqId = "194"
const success = 3
const payCancel=2
const sequelize = require('sequelize')
const Op = sequelize.Op




/***
 class to return price  of  bill
Each inherited class should implement those functions. 

 */
class PaymentDetails {
    constructor(req) {
        this.req = req
        this.info = new ResInfo(req)
    }

    async confirmationSaveHandler() {

        let transaction = await models.transaction.model.findOne({
            where: {id: this.req.body.transactionId },
            include: {
                model: models.transactionData.model, as: 'transactionData'
            }
        })
        
     
        if (!transaction) {
            this.info.setErrMessage(LM.getString("transactionNotExist", this.req.user.language_code))

        }

        else {
            
            if (transaction.type == payCancel) {
                this.info.setErrMessage(LM.getString("transactionUnable to cancel", this.req.user.language_code))
                return this.info
            }
            
            if (transaction.supplier_id != this.req.params.supplierID) {
                this.info.setErrMessage(LM.getString("transactionNotbelongSupplier", this.req.user.language_code))
            }
            else {
                if (transaction.transactionData && transaction.transactionData["confirmation"]) {
                    this.info.setErrMessage(LM.getString("transactionHasApproval", this.req.user.language_code))
                }
                else {
                    await transaction.transactionData[0].update({ confirmation: this.req.body.confirmationNumber })
                    this.info.send = { success: LM.getString("transactionApprovedSuccesstully", this.req.user.language_code) }
                }
            }
        }

        return this.info

    }

    /***
       * check  what amound of  bil/product 
       * 
       *    */
    async  getPrice() {


        switch (this.req.params.supplierID) {
            case iecBillId:
                const iecBill = new IecBill(this.req.body.billNum,"","","",this.req)
                return iecBill.getBillPrice()
            case bezeqId:
                const bezeq = new BezeqOnline(this.req)
                return bezeq.getBillPrice()


            case cityPayId:
                const cityPay = new CityPayment(this.req.body.idItem, this.req.body.billNum, this.req.body.idClient,"", this.req)
                return cityPay.getBillPrice()

        }
    }
    /***
      *revert data of payment
      */

    async   revert() {
        switch (this.req.params.supplierID) {
            case iecBillId:
                const payment = new IecBill(this.req.body.contractNumber)
                return payment.revertPayment(this.req.body.transationId, this.req.body.businessId, this.req)
            case iecMatamId:
                const paymentMat = new IecMatam(this.req.body.contractNumber)
                return paymentMat.revertPayment(this.req.body.transationId, this.req.body.businessId, this.req)
        }
    }
    /***       * check  what product will be  canceled    
*     * TODO  add  price (  of product   to  peletok)    */
    async cancelPaymentDetails() {

        let details = [];
        switch (this.req.query.supplierID) {
            case cellcomId:
            case partnerId:
            case pelephoneId:
            case confidentialId:
                details = await reportView.model.findAll({
                    where: {
                        business_identifier: this.req.query.businessId, product_id: this.req.query.itemId, contract_number: this.req.query.contractNumber, status: success, is_canceled: false, is_cancelable: true, cancel_process_status: "NO", transaction_type: { [Op.or]: [1, null] }
                    },
                    group: ["id", "businessId", "price", "contract_number", "business_name", "transaction_end_timestamp", "product_name"],
                    attributes: ["id", ["business_identifier", 'businessId'], "price", "business_name", "contract_number", "transaction_end_timestamp", "product_name"],
                    raw: true
                });
                if (details.length == 0) {
                    this.info.setErrMessage(LM.getString("noData"))
                    return this.info
                }

                this.info.send = details

                return this.info

            case manualCardId:
                details = await reportView.model.findAll({
                    where: {
                        business_identifier: this.req.query.businessId, product_id: this.req.query.itemId, status: success, is_canceled: false, transaction_type: { [Op.or]: [1, null] }
                    },
                    group: ["id", "businessId", "price", "transaction_end_timestamp", "business_name", "product_name"],
                    attributes: ["id", ["business_identifier", 'businessId'], "business_name", "price", [sequelize.fn("to_char",
                        sequelize.col("transaction_end_timestamp"), 'YYYY-MM-DD'), "transaction_end_timestamp"], "product_name"],
                })

                if (details.length == 0) {
                    this.info.setErrMessage(LM.getString("noData"))
                    return this.info
                }

                this.info.send = details

                return this.info

            case highway6Id:
            case carmelTunnelsId:
            case iecBillId:
            case iecMatamId:
            case cityPayId:
                details = await reportView.model.findAll({
                    where: {
                        transaction_id: this.req.query.transactionId, status: success, is_canceled: false
                    },
                    group: ["transaction_id", "business_id", "price", "business_name", "contract_number", "transaction_end_timestamp", "product_name"],
                    attributes: ["transaction_id", "business_id", "business_name", "price", "contract_number", "transaction_end_timestamp", "product_name"],
                    raw: true
                })
                if (details.length == 0) {
                    this.info.setErrMessage(LM.getString("noData"))
                    return this.info
                }

                this.info.send = details

                return this.info
        }
    }

}

module.exports = PaymentDetails
