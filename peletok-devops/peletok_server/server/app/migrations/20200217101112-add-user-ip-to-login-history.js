'use strict';
const {addColumnsToTable} = require('../dbUtils/addPermission')
module.exports = {
  async up(queryInterface, Sequelize) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    
    await  addColumnsToTable(queryInterface, 'USR_LoginHistory', [{
        name: 'user_ip', typeOptions: {
          type: Sequelize.STRING,
          allowNull: true
        }
      }])
    
  },
  async down(queryInterface, Sequelize) {
    
  }
};
