from __future__ import unicode_literals
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import BasePermission
from rest_framework.decorators import api_view, permission_classes
from selenium.common.exceptions import TimeoutException
from utils.elements_selectors import RoadTzafon
from utils.main_functions import MainFunctions


@api_view(['POST'])
@permission_classes([BasePermission])
def post_get_price(request):
    """
    {
    "uri": "https://6cn.co.il/invoice-payment",
    "payment_info": {
        "bill": "0011925548"
        }
    }
    :param request:
    :return {status: <str> 'success' / 'error', price: <float> || error_message: <str>}
    """
    handler = CrossingNorthRoad(str(request.data.get("uri")), request.data.get("payment_info"))
    res = handler.get_price()
    return Response(res, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes([BasePermission])
def post_pay(request):
    """
    {
    "uri": "https://6cn.co.il/invoice-payment",
    "payment_info": {
        "bill": "9996428318",
        "card_num": "4580110705790910",
        "card_cvv": "900",
        "card_valid_month": "06",
        "card_valid_year": "2023",
        "personalId": "372097986"
        }
    }
    :param request:
    :return {status: <str> 'success' / 'error', confirmation: <str> || error_message: <str>}
    """
    handler = CrossingNorthRoad(str(request.data.get("uri")), request.data.get("payment_info"))
    res = handler.pay()
    return Response(res, status=status.HTTP_200_OK)


class CrossingNorthRoad:

    def __init__(self, url: str, payment_info: dict):
        self.payment_info = payment_info
        self.url = url
        self.driver = MainFunctions(self.url)

    def first_steps(self):
        return_obj = {}
        try:
            self.driver.click_button(RoadTzafon.radioByMasleka)
            self.driver.send_keys(RoadTzafon.maslekaNumber, self.payment_info["bill"])
            self.driver.click_button(RoadTzafon.sendButton)
            if self.driver.check_if_element_is_displayed(RoadTzafon.errorMessage)[0]:
                return False
            else:
                return True
        except TimeoutException:
            return_obj["status"] = 'error'
            return_obj["error_message"] = "המערכת לא הצליחה להתחבר, אנא נסה שוב מאוחר יותר."
            return return_obj

    def get_price(self):
        begin = self.first_steps()
        return_obj = {}
        if type(begin) is dict:
            return begin
        elif begin:
            price_sum = float(self.driver.get_text_from_element(RoadTzafon.priceSum))
            return_obj["status"] = 'success'
            return_obj["price"] = price_sum
        else:
            error_message = self.driver.get_text_from_element(RoadTzafon.errorMessage)
            return_obj["status"] = 'error'
            return_obj["error_message"] = error_message
            self.driver.driver.close()
        return return_obj

    def pay(self):
        begin = self.first_steps()
        return_obj = {}
        if type(begin) is dict:
            return begin
        elif begin:
            self.driver.wait_for_frame_available_and_switch(RoadTzafon.frame)
            self.driver.send_keys(RoadTzafon.cardNumber, self.payment_info["card_num"])
            self.driver.send_keys(RoadTzafon.idNumber, self.payment_info["personalId"])
            self.driver.send_keys(RoadTzafon.cvvNumber, self.payment_info["card_cvv"])
            self.driver.select_by_text(RoadTzafon.yearValid, self.payment_info["card_valid_year"])
            self.driver.select_by_text(RoadTzafon.monthValid,
                                       self.payment_info["card_valid_month"])
            self.driver.click_button(RoadTzafon.submitButton)

            if self.driver.check_if_element_exits(RoadTzafon.errorPay)[0]:
                error_payment = self.driver.get_text_from_element(RoadTzafon.errorPay)
                return_obj["status"] = 'error'
                return_obj["error_message"] = error_payment
            elif self.driver.check_if_element_exits(RoadTzafon.errorBalance)[0]:
                error_payment_balance = self.driver.get_text_from_element(RoadTzafon.errorBalance)
                return_obj["status"] = 'error'
                return_obj["error_message"] = error_payment_balance
            else:
                confirmation = self.driver.get_text_from_element(RoadTzafon.confirmation)
                return_obj["status"] = 'success'
                return_obj["confirmation"] = confirmation

        else:
            try:
                error_message = self.driver.get_text_from_element(RoadTzafon.errorMessage)
                return_obj["error"] = 'error'
                return_obj["error_message"] = error_message
                self.driver.driver.close()
            except TimeoutException:
                pass
        return return_obj
