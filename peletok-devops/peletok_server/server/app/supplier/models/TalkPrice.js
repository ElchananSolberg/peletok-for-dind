const BaseModel = require('../../BaseModel');
const {DataTypes} = require("sequelize");

/**
 * Country model
 */
class TalkPrice extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            country: {
                type: DataTypes.STRING(256),
                allowNull: false
            },
            outcome_price: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            income_price: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            sms_price: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
        };
        this.prefix = "SUPL";
        this.statusField = true;
        this.statusTimestampField = true;
        this.statusAuthor = true;
    }

    /**
     * creates associate for Country model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.product.model, { as: "Product"})

    }
}
module.exports = new TalkPrice();

