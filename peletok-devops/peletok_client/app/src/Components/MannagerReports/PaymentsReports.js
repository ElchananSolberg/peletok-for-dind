import React, { Component } from 'react';
import { Button, Row, Col, Collapse } from 'reactstrap';

import { LanguageManager as LM } from "../LanguageManager/Language"
import { getDistributors, getSellers, getReportRes } from '../DataProvider/DataProvider'
import { ReportTableContent } from "../ReportComponent/ReportTableContent"
import InputTypeRadio from '../InputUtils/InputTypeRadio'
import InputTypeSelect from '../InputUtils/InputTypeSelect'
import InputTypeText from '../InputUtils/InputTypeText'
import InputTypeNumber from '../InputUtils/InputTypeNumber';
import InputTypeSearchList from '../InputUtils/InputTypeSearchList'
import InputTypeDate from '../InputUtils/InputTypeDate';
import InputTypeTime from '../InputUtils/InputTypeTime';

export class PaymentsReports extends Component {
    constructor(props) {
        super(props);
        this.state = {
            transactionStatus: {},
            busnissStatus: {},
            distributors: [],
            business: [],
            agents: [],
            tableRows: [],
            tableHeadersObj: {},
            tableFootersObj: {},
            openReport: false,

        }
        this.getDistributorsNames = this.getDistributorsNames.bind(this);
        this.getBusinessNames = this.getBusinessNames.bind(this);
        this.getAgentNames = this.getAgentNames.bind(this);
        this.fields = {};
        this.reportID = "12";
    }

    getDistributorsNames() {
        getDistributors().then(
            (res) => {
                this.setState({ distributors: res });
            }
        ).catch(
            (err) => {
                alert("Failed loading Distributors list: " + err)

            }
        )
    }

    getBusinessNames() {
        getSellers().then(
            (res) => {
                this.setState({ business: res });
            }
        ).catch(
            (err) => {
                alert("Failed loading business list: " + err)

            }
        )
    }

    getAgentNames() {
        // This is mock we need to get it from the server
        // TODO add server call for agents
        this.setState({
            agents: [
                {
                    name: "סמי",
                    id: 1,
                },
                {
                    name: "רונן",
                    id: 2,
                },
                {
                    name: "דליה",
                    id: 3,
                },
            ]
        })
    }

    componentDidMount() {
        this.getDistributorsNames()
        this.getBusinessNames()
        this.getAgentNames()
    }

    showReportClick = () => {
        let params = {};
        let requestValid = true;
        // this is a list of valid params for generating the report
        let validParams = ['startDate', "startTime", "endDate", "endTime", "distributor", "distributor_id", 'busines', 'agent', "agent_id", "business_id", "receipt_number", "transaction_number", "by_distributor", "action_type"];
        Object.keys(this.fields).forEach((key) => {
            let isValid = validParams.indexOf(key) != -1
            if (this.fields[key] !== null && isValid) {
                let result = this.fields[key].getValue();
                if (!result.valid) {
                    requestValid = false;
                }
                params[key] = result.value;
            }
        })
        if (requestValid) {
            getReportRes(this.reportID, { params }).then(
                (res) => {
                    if (res === "No reports found") {
                        alert(LM.getString("noReportErrMsg"))
                    }
                    else {
                        let headers = [];
                        Object.values(res['headers']).map((value) => {
                            return headers.push(LM.getString(value))
                        });
                        this.setState({
                            tableFootersObj: res['sum'],
                            tableHeadersObj: headers,
                            tableRows: res['data'],
                            openReport: true,
                            sumOfProfit: res['sum'].priceSum + LM.getString('moneySymbol')
                        }
                        )
                    }
                }
            ).catch(
                (err) => {
                    alert(err);
                })
        }
        else {
            alert(LM.getString("wrongInputErrMsg"))
        }

    }

    render() {
        let time_now = new Date()
        let nowHour = time_now.getHours() + "";
        let nowMinute = time_now.getMinutes() + "";
        if (nowHour.length < 2) { nowHour = "0" + nowHour }
        if (nowMinute.length < 2) { nowMinute = "0" + nowMinute }

        return (
            <div>
                <h3 className={'color-page-header fs-26 font-weight-light'}>{LM.getString("payments")}</h3>
                <div className="main_border_padding" >
                    <Row>
                        <Col sm="4">
                            <InputTypeSearchList
                                title={LM.getString("business_name")}
                                id='selectBusiness'
                                options={this.state.business.map(busines => busines.name)}
                                onChange={() => {
                                    let theName = this.fields['busines'].state.value;
                                    let businesObj = this.state.business.filter((el) => { return el.name == theName })[0];
                                    this.fields["business_id"].setValue(businesObj.business_identifier);
                                }}
                                ref={(componentObj) => { this.fields['busines'] = componentObj }}
                            />
                        </Col>
                        <Col sm="4">
                            <InputTypeNumber
                                title={LM.getString("customNum")}
                                id="costumerNumber"
                                ref={(componentObj) => { this.fields["business_id"] = componentObj }} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="4">
                            <InputTypeSearchList
                                title={LM.getString("selectDistributor")}
                                id='selectDistributors'
                                options={this.state.distributors.map(distributor => distributor.business_name)}
                                onChange={
                                    () => {
                                        // TODO Add functionalety 
                                        // let theName = this.fields['distributor'].state.value;
                                        // let distObj = this.state.distributors.filter((el) => { return el.name == theName })[0]
                                        // this.fields['distributor_id'].setValue(distObj.id)

                                    }
                                }
                                ref={(componentObj) => { this.fields['distributor'] = componentObj }}
                            />
                        </Col>
                        <Col sm="4">
                            <InputTypeNumber
                                title={LM.getString("distributorNum")}
                                id="distributorNumber"
                                ref={(componentObj) => { this.fields["distributor_id"] = componentObj }} />
                        </Col>
                    </Row>
                    <Row>

                    </Row>
                    <Row>
                        <Col sm="4">
                            <InputTypeSearchList
                                title={LM.getString("agentSearch")}
                                id='selectAgent'
                                options={this.state.agents.map(agent => agent.name)}
                                onChange={
                                    () => {
                                        let theName = this.fields['agent'].state.value;
                                        let agenObj = this.state.agents.filter((el) => { return el.name == theName })[0]
                                        this.fields['agent_id'].setValue(agenObj.id)
                                    }
                                }
                                ref={(componentObj) => { this.fields['agent'] = componentObj }}
                            />
                        </Col>
                        <Col sm="4">
                            <InputTypeNumber
                                title={LM.getString("agentNum")}
                                id="agentNumber"
                                ref={(componentObj) => { this.fields["agent_id"] = componentObj }} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="4">
                            <InputTypeSearchList
                                multi={true}
                                title={LM.getString('actionType')}
                                id="actionType"
                                default={LM.getString("any")}
                                options={[
                                    LM.getString("payment"), LM.getString("credit"), 
                                    LM.getString("bounsedCheck"), LM.getString("bounsedBankTransfer"), 
                                    LM.getString("bounsedCheckFee"), LM.getString("bounsedBankTransferFee")
                                ]}
                                ref={(componentObj) => { this.fields['action_type'] = componentObj }} />
                        </Col>
                        <Col sm="4">
                            <InputTypeSearchList
                                multi={true}
                                title={LM.getString('businessStatus')}
                                id="businessStatus"
                                default={LM.getString("any")}
                                options={[LM.getString("froze"), LM.getString("locked"), LM.getString("active"), LM.getString("deleted")]}
                                ref={(componentObj) => { this.fields['business_status'] = componentObj }} />

                        </Col>
                    </Row>
                    <Row>
                        <Col sm="4">
                            <InputTypeNumber
                                title={LM.getString('receiptNumber')}
                                id="receiptNumber"
                                ref={(componentObj) => { this.fields['receipt_number'] = componentObj }} />
                        </Col>
                        <Col sm="4">
                            <InputTypeNumber
                                title={LM.getString('transactionNumber')}
                                id="transactionNumber"
                                ref={(componentObj) => { this.fields['transaction_number'] = componentObj }} />
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <InputTypeRadio
                                options={[LM.getString("byDistributor")]}
                                id="byDistributor"
                                ref={(componentObj) => { this.fields["by_distributor"] = componentObj }} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="4">
                            <InputTypeDate
                                title={LM.getString("startingDate") + ":"}
                                id="startingDate"
                                ref={(componentObj) => { this.fields["startDate"] = componentObj }} />
                        </Col>
                        <Col sm="4">
                            <InputTypeTime
                                title={LM.getString("startingTime") + ":"}
                                id="startingTime"
                                ref={(componentObj) => { this.fields["startTime"] = componentObj }} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="4">
                            <InputTypeDate
                                title={LM.getString("endingDate") + ":"}
                                id="endingDate"
                                ref={(componentObj) => { this.fields["endDate"] = componentObj }} />
                        </Col>
                        <Col sm="4">
                            <InputTypeTime
                                title={LM.getString("endingTime") + ":"}
                                id="endingTime"
                                default={nowHour + ':' + nowMinute}
                                ref={(componentObj) => { this.fields["endTime"] = componentObj }} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="auto">
                            <Button className="paymentsButton" id="paymentsShowReportButton" onClick={this.showReportClick} > {LM.getString("showReport")} </Button>
                        </Col>
                    </Row>
                </div>
                <Collapse isOpen={this.state.openReport}>
                    <ReportTableContent reportName={LM.getString("transactions")} tableRows={this.state.tableRows} tableHeadersObj={this.state.tableHeadersObj} tableFootersObj={this.state.tableFootersObj} />
                </Collapse>
            </div>
        )
    }

}
