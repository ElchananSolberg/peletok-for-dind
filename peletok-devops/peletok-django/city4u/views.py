from __future__ import unicode_literals
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import BasePermission
from rest_framework.decorators import api_view, permission_classes
from selenium.common.exceptions import TimeoutException
from utils.elements_selectors import City4u
from utils.main_functions import MainFunctions

site_url = 'https://city4u.co.il'


@api_view(['POST'])
@permission_classes([BasePermission])
def post_get_price(request):
    """
    {
    "uri": "/PortalServicesSite/cityPay/291000/mislaka/1",
    "payment_info": {
        "client_id": "69650331",
        "bill": "880965707"
        }
    }
    :param request:
    :return {status: <str> 'success' / 'error', price: <float> || error_message: <str>}
    """
    payment_handler = City4uPayment(site_url + str(request.data.get("uri")),
                                    request.data.get("payment_info"))
    res = payment_handler.get_price()
    return Response(res, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes([BasePermission])
def post_pay(request):
    """
    {
    "uri": "/PortalServicesSite/cityPay/291000/mislaka/1",
    "payment_info": {
        "client_id": "69650331",
        "bill": "880965707",
        "full_name": "פלאטוק",
        "phone": "0548408908",
        "email": "mendyb@ravtech.co.il",
        "card_num": "4580110705790910",
        "card_cvv": "969",
        "card_valid_month": "06",
        "card_valid_year": "2023",
        "personalId": "012163234"
        }
    }
    :param request:
    :return {status: <str> 'success' / 'error', confirmation: <str> || error_message: <str>}
    """
    payment_handler = City4uPayment(site_url + str(request.data.get("uri")),
                                    request.data.get("payment_info"))
    res = payment_handler.pay()
    return Response(res, status=status.HTTP_200_OK)


class City4uPayment:

    def __init__(self, url: str, payment_info: dict):
        self.payment_info = payment_info
        self.url = url
        self.driver = MainFunctions(self.url)

    def check_if_a_warning_displayed(self, selector):
        a = [x.is_displayed() for x in self.driver.driver.find_elements(*selector)]
        if True in a:
            return True
        else:
            return False

    def first_steps(self):
        try:
            self.driver.send_keys(City4u.payingNumber, self.payment_info["client_id"])
            self.driver.send_keys(City4u.voucherNumber, self.payment_info["bill"])
            self.driver.click_button(City4u.showSumButton)
            self.driver.wait_for_element_until_not_displayed(City4u.loading)
            if self.check_if_a_warning_displayed(City4u.warning):
                return self.driver.get_text_from_element(City4u.warning)
            elif self.driver.check_if_element_exits(City4u.closeButton)[0]:
                msg_txt = self.driver.get_text_from_element(
                    City4u.errorMessage).split('\n')[0]
                return msg_txt
            elif self.driver.check_if_element_is_displayed(City4u.message)[0]:
                price = float(self.driver.get_text_from_element(City4u.message))
                if price > 0:
                    return price
                else:
                    error_message = self.driver.get_text_from_element(City4u.errorMessage2)
                    return error_message
            elif self.driver.check_if_element_exits(City4u.allMessage)[0]:
                msg_txt = self.driver.get_text_from_element(City4u.allMessage)
                return msg_txt
        except TimeoutException:
            return "המערכת לא הצליחה להתחבר, אנא נסה שוב מאוחר יותר."

    def get_price(self):
        begin = self.first_steps()
        return_obj = {}
        if isinstance(begin, str):
            return_obj["status"] = 'error'
            return_obj["error_message"] = begin
        elif isinstance(begin, float):
            return_obj["status"] = 'success'
            return_obj["price"] = begin
        return return_obj

    def pay(self):
        return_obj = {}
        begin = self.first_steps()
        if isinstance(begin, str):
            return_obj["status"] = 'error'
            return_obj["error_message"] = begin
            return return_obj
        else:
            self.driver.click_button(City4u.nextButton)
            self.driver.send_keys(City4u.nameField, self.payment_info["full_name"])
            self.driver.send_keys(City4u.phoneField, self.payment_info["phone"])
            self.driver.send_keys(City4u.emailField, self.payment_info["email"])
            self.driver.click_button(City4u.phoneField)
            self.driver.click_button(City4u.nextButton2)

            if self.check_if_a_warning_displayed(City4u.warning):
                return_obj['status'] = 'error'
                return_obj["error_message"] =\
                    'לא הוכנסו כל הפרטים הנדרשים: שם מלא, טלפון ואימייל.'
                return return_obj

            elif self.driver.check_if_element_is_displayed(City4u.idNumberField)[0]:
                self.driver.send_keys(City4u.idNumberField, self.payment_info["personalId"])
                self.driver.send_keys(City4u.cardNumberField, self.payment_info["card_num"])
                self.driver.select_by_text(City4u.yearValid, self.payment_info["card_valid_year"])
                self.driver.select_by_text(City4u.monthValid,
                                           str(int(self.payment_info["card_valid_month"])))
                self.driver.send_keys(City4u.cvv, self.payment_info["card_cvv"])
                self.driver.click_button(City4u.nextButton3)

                self.driver.wait_for_element_until_not_displayed(City4u.loading)
                if self.check_if_a_warning_displayed(City4u.warning):
                    return_obj["status"] = 'error'
                    return_obj["error_message"] = "לא הוכנסו כל הפרטים הנדרשים:" \
                                                  " מס' כרטיס אשראי, תוקף, ת.ז בעל הכרטיס" \
                                                  " וקוד אבטחה, או שאחד מהנתונים שגויים."
                    return return_obj

                elif self.driver.check_if_element_exits(City4u.closeButton)[0]:
                    return_obj["status"] = 'error'
                    return_obj["error_message"] = self.driver.get_text_from_element(
                        City4u.errorMessage).split('\n')[0]
                    return return_obj

                else:
                    return_obj["status"] = 'success'
                    return_obj["confirmation"] = self.driver.get_text_from_element(
                        City4u.iskaNumber)
                    return return_obj

            else:
                self.driver.click_button(City4u.nextButton3)
                self.driver.driver.switch_to.frame(3)
                self.driver.send_keys(City4u.frameIdNumberField, self.payment_info["personalId"])
                self.driver.send_keys(City4u.frameCardNumberField, self.payment_info["card_num"])
                self.driver.select_by_text(City4u.frameYearValid,
                                           self.payment_info["card_valid_year"])
                self.driver.select_by_value(City4u.frameMonthValid,
                                            self.payment_info["card_valid_month"])
                self.driver.send_keys(City4u.frameCvv, self.payment_info["card_cvv"])
                self.driver.click_button(City4u.frameSubmit)

                if self.check_if_a_warning_displayed(City4u.frameWarning):
                    return_obj["status"] = 'error'
                    return_obj["error_message"] = "לא הוכנסו כל הפרטים הנדרשים:" \
                                                  " מס' כרטיס אשראי, תוקף, ת.ז בעל הכרטיס" \
                                                  " וקוד אבטחה, או שאחד מהנתונים שגויים."
                    return return_obj

                else:
                    self.driver.wait_for_element_contain_text(City4u.iskaNumber)
                    return_obj['status'] = 'success'
                    return_obj["confirmation"] = self.driver.get_text_from_element(
                        City4u.iskaNumber)
                    return return_obj
