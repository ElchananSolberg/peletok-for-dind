import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { PersonalArea } from "../PersonalArea/PersonalArea";
import { WorkerManagement } from "../Permissions/Workers";
import { Charge } from "../ChargeComponent/Charge";
import { ChargeCancelation } from "../ChargeComponent/Prepaid/ChargeCancelation";
import { Invoice } from '../InvoiceScreen/Invoice';

import { ManualCards } from '../ManualCards/ManualCards'

import { MainSuppliers } from '../Suppliers/MainSuppliers'
import { Permissions } from '../Permissions/Permissions';
import { SupplierPermissions } from '../Permissions/SupplierPermissions';
import { CreditCard } from '../CreditCard/CreditCard';
import { ProfitPercentageScreen } from '../ProfitPercentageScreen/ProfitPercentageScreen';
import { Sellers_Suppliers } from '../Sellers_Suppliers/Sellers_Suppliers';
import { Cancelation } from '../Gifts/Cancelation';
import TestScreen from '../InputUtils/TestScreen';
import { ContractInfo } from '../ContractInfo/ContractInfo'
import { Report } from '../ReportComponent/Report';
import { UnpaidPayments } from '../UnpaidPayments/UnpaidPayments'
import { CancelTransactions } from '../CancelTransactions/CancelTransactions'
import { ConfigGeneral } from '../ConfigGeneral/ConfigGeneral';
import CreateProduct from '../CreateProduct/CreateProduct'
import { InsertGift } from '../Gifts/InsertGift';
import { MainGifts } from '../Gifts/MainGifts';
import { BannerManagement } from '../BannerManagement/BannerManagement'
import { UserManagement } from '../UserManagement/UserManagement'
import NotificationCenter from '../NotificationCenter/NotificationCenter';
import { TransactionReports } from '../MannagerReports/TransactionReports'
import Payments from '../Payments/Payments';
import { PaymentsReports } from '../MannagerReports/PaymentsReports'
import { ActionAndPaymentReport } from '../MannagerReports/ActionAndPaymentReport'
import { ActionReports } from '../MannagerReports/ActionReports'
import { DistributionActivitiesReports } from '../MannagerReports/DistributionActivitiesReports'
import { EmailReports } from '../MannagerReports/EmailReports'
import { ManualCardsReport } from '../MannagerReports/ManualCardsReport'
import { RemainderByDateReport } from '../MannagerReports/RemainderByDateReport'
import Business from '../Business/Business';
import TagsManagement from '../TagsManagement/TagsManagement';
import { CatalogNumberManagement } from '../CatalogNumberManagement/CatalogNumberManagement';
import { LockedScreen } from '../LockedScreen/LockedScreen'
import DueToDateOfExportationToHashavshevetReport from '../Hashavshevet/DueToDateOfExportationToHashavshevetReport';
import OrdersExportedToHashavshevetReport from '../Hashavshevet/OrdersExportedToHashavshevetReport';
import ExportCommissionsToHashavshevetReport from '../Hashavshevet/ExportCommissionsToHashavshevetReport';
import ExportOrdersToHashavshevetReport from '../Hashavshevet/ExportOrdersToHashavshevetReport';
import HashavshevetPaymentsReport from '../Hashavshevet/HashavshevetPaymentsReport';
import ImportReceiptsFromHashavshevet from '../Hashavshevet/ImportReceiptsFromHashavshevet';
import BusinessCommissions from '../BusinessCommissions/BusinessCommissions';
import { PrivateRoute } from '../PrivateRouter/PrivateRouter';
import SearchBusiness from '../SearchBusiness/SearchBusiness';
import { InvoiceUploadAndConfirmUser } from '../InvoiceUpload/InvoiceUploadAndConfirmUser';
import {DuplicatProductAuth} from '../DuplicatProductAuth/DuplicatProductAuth.js';
import CopyPermissions from '../CopyPermissions/CopyPermissions';
import { RetrieveDetailsRenderer } from '../ChargeComponent/Bills/ElectricCompanyCancelation/RetrieveDetailsRenderer';
import EditProfile from '../EditProfile/EditProfile';
import SuppliersProfile from '../SuppliersProfile/SuppliersProfile';
import CopyBusinessProfile from '../CopyBusinessProfile/CopyBusinessProfile';
import Sellers_ProfitPercentages from '../Sellers_ProfitPercentages/Sellers_ProfitPercentages';

import { ElectricityChargeCancelationConfirmation } from '../ChargeComponent/Bills/ElectricCompanyCancelation/ElectricityChargeCancelationConfirmation';
import { SERVER_BASE } from '../DataProvider/DataProvider';
import { BillsLocalPaymentCancelationMainScreen } from '../ChargeComponent/Bills/LocalPaymentCancelation/BillsLocalPaymentCancelationMainScreen';
import Config_Commissions from '../Config_Commissions/Config_Commissions';
import {ManagerPaymentsEnum} from "../../enums";


export class ContentRouter extends Component {

    static defaults =
        {
            'main': 'charge',

        };

    render() {
        switch (this.props.match.params.tab) {
            case 'main':
                return (
                    <Switch>
                        <PrivateRoute path={`${this.props.match.path}/charge`} component={Charge} /> {/* default page for this tab*/}
                        <PrivateRoute path={`${this.props.match.path}/WorkerManagement`} component={WorkerManagement} />
                        <PrivateRoute path={`${this.props.match.path}/SupplierPermissions`} component={SupplierPermissions} />
                        <PrivateRoute path={`${this.props.match.path}/suppliers`} component={MainSuppliers} />
                        <PrivateRoute path={`${this.props.match.path}/userArea`} component={PersonalArea} />
                        <PrivateRoute path={`${this.props.match.path}/gifts/pointUsage`} component={MainGifts} />
                        <PrivateRoute path={`${this.props.match.path}/gifts/cancelGift`} component={Cancelation} />
                        <PrivateRoute path={`${this.props.match.path}/report`} component={Report} />
                        <PrivateRoute path={`${this.props.match.path}/invoice`} component={Invoice} />
                        {/* changes for testing of InputUtils */}
                        <PrivateRoute path={`${this.props.match.path}/testScreen`} component={TestScreen} />
                        <PrivateRoute path={`${this.props.match.path}/NotificationCenter`} component={NotificationCenter} />
                        <Redirect to={`${this.props.match.path}/charge`} /> {/* redirects to default page for this tab */}
                    </Switch>
                );
            case 'cancel':
                return (
                    <Switch>
                        <PrivateRoute path={`${this.props.match.path}/cancel`} component={ChargeCancelation} />
                        <PrivateRoute path={`${this.props.match.path}/ElectricityCompany/confirmDetails`} component={(routeProps) => {
                            return <ElectricityChargeCancelationConfirmation {...routeProps} image={`${SERVER_BASE}/suppliersLogos/israel-electric.svg`} />
                        }} />
                        <PrivateRoute path={`${this.props.match.path}/ElectricityCompany/retrieveDetails`} component={RetrieveDetailsRenderer} />
                        <PrivateRoute path={`${this.props.match.path}/endLocalPayment`} component={BillsLocalPaymentCancelationMainScreen} />
                        <PrivateRoute path={`${this.props.match.path}/UnpaidPayments`} component={UnpaidPayments} />
                        <PrivateRoute path={`${this.props.match.path}/CancelTransactions`} component={CancelTransactions} />
                        <Redirect to={`${this.props.match.path}/cancel`} /> {/* redirects to default page for this tab*/}
                    </Switch>
                );
            case 'suppliers':
                return (
                    <Switch>
                        <PrivateRoute path={`${this.props.match.path}/suppliersAndProducts/suppliers`} component={MainSuppliers} /> {/* default page for this tab*/}
                        <PrivateRoute path={`${this.props.match.path}/suppliersAndProducts/products`} component={CreateProduct} />
                        <PrivateRoute path={`${this.props.match.path}/suppliersAndProducts/SuppliersProfile`} component={SuppliersProfile} />
                        <PrivateRoute path={`${this.props.match.path}/suppliersAndProducts/profitPercentageProfile/:profitModel`} component={EditProfile}/>
                        <PrivateRoute path={`${this.props.match.path}/suppliersAndProducts/catalogNumberManagement`} component={CatalogNumberManagement} />
                        <PrivateRoute path={`${this.props.match.path}/suppliersAndProducts/manualCards`} component={ManualCards} />
                        <PrivateRoute path={`${this.props.match.path}/gifts`} component={InsertGift} />

                        <Redirect to={`${this.props.match.path}/suppliersAndProducts/suppliers`} /> {/* redirects to default page for this tab*/}
                    </Switch>
                );
            case 'resellers':
                return (
                    <>
                        <PrivateRoute path={`${this.props.match.path}/sellersAndDistributors/`} component={SearchBusiness} />
                        <Switch>
                            <PrivateRoute path={`${this.props.match.path}/sellersAndDistributors/business`} component={Business} />
                            <PrivateRoute path={`${this.props.match.path}/sellersAndDistributors/contractInfo`} component={ContractInfo} />
                            <PrivateRoute path={`${this.props.match.path}/sellersAndDistributors/suppliers`} component={Sellers_Suppliers} />
                            <PrivateRoute path={`${this.props.match.path}/sellersAndDistributors/userManagement`} component={UserManagement} />                            
                            <PrivateRoute path={`${this.props.match.path}/sellersAndDistributors/profitPercentages`} component={Sellers_ProfitPercentages} />
                            <PrivateRoute path={`${this.props.match.path}/sellersAndDistributors/commissions`} component={BusinessCommissions} />
                            <PrivateRoute path={`${this.props.match.path}/locked`} component={LockedScreen} />
                            <Redirect to={`${this.props.match.path}/sellersAndDistributors/business`} /> {/*redirects to default page for this tab*/}
                        </Switch>
                    </>

                );
            case 'config':
                return (
                    <Switch>
                        <PrivateRoute path={`${this.props.match.path}/main/general`} component={ConfigGeneral} />
                        <PrivateRoute path={`${this.props.match.path}/main/providerPermissions`} component={Permissions} />                        
                        <PrivateRoute path={`${this.props.match.path}/main/productAuth/:profitModel`} component={ProfitPercentageScreen} />
                        <PrivateRoute path={`${this.props.match.path}/main/profitCopy`} component={DuplicatProductAuth} />
                        <PrivateRoute path={`${this.props.match.path}/main/supplierToSupplierPerm`} component={CopyPermissions} />
                        <PrivateRoute path={`${this.props.match.path}/main/copyResellerProfile`} component={CopyBusinessProfile} />                        
                        <PrivateRoute path={`${this.props.match.path}/main/paymentInfo`} component={CreditCard} />
                        <PrivateRoute path={`${this.props.match.path}/main/invoiceUpload`} component={InvoiceUploadAndConfirmUser} />
                        <PrivateRoute path={`${this.props.match.path}/main/bannerManagement`} component={BannerManagement} />
                        <PrivateRoute path={`${this.props.match.path}/main/tagsManagement`} component={TagsManagement} />
                        
                        {/*<Redirect to={`${this.props.match.path}/main/general`} /> /!*redirects to default page for this tab*!/*/}
                        <Redirect to={`${this.props.match.path}/main/providerPermissions`} /> {/*redirects to default page for this tab*/}
                    </Switch>
                );
            case 'reports':
                return (
                    <Switch>
                        <PrivateRoute path={`${this.props.match.path}/reports/remainderByDate`} component={RemainderByDateReport}/>
                        <PrivateRoute path={`${this.props.match.path}/reports/:type`} component={TransactionReports} /> {/* default page for this tab*/}
                        {/*<PrivateRoute path={`${this.props.match.path}/reports/payments`} component={PaymentsReports} />*/}
                        {/*<PrivateRoute path={`${this.props.match.path}/reports/actions`} component={ActionReports} />*/}
                        {/*<PrivateRoute path={`${this.props.match.path}/reports/actionsAndPayments`} component={ActionAndPaymentReport} />*/}
                        {/*<PrivateRoute path={`${this.props.match.path}/reports/distributionActivities`} component={DistributionActivitiesReports} />*/}
                        {/*<PrivateRoute path={`${this.props.match.path}/reports/emailReports`} component={EmailReports} />*/}
                        {/*<PrivateRoute path={`${this.props.match.path}/reports/manualCards`} component={ManualCardsReport} />*/}
                        <PrivateRoute path={`${this.props.match.path}/hashavshevet/export/:type`} component={ExportOrdersToHashavshevetReport} />
                        <PrivateRoute path={`${this.props.match.path}/hashavshevet/hashavshevetPaymentsReport`} component={HashavshevetPaymentsReport} />
                        <PrivateRoute path={`${this.props.match.path}/hashavshevet/importReceiptsFromHashavshevet`} component={ImportReceiptsFromHashavshevet} />

                        <Redirect to={`${this.props.match.path}/reports/${ManagerPaymentsEnum.TRANSACTION}`} /> {/* redirects to default page for this tab*/}
                    </Switch>
                );
            default:
                return (
                    <Redirect to={`${this.props.match.url.slice(0, this.props.match.path.indexOf(":tab"))}`} />
                )
        }
    }
}