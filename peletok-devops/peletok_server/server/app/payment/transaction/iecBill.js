
const BasicIEC = require("./iecBasic")
const logs = require('../../logger')
const details = 5
const prePaidPay = 6
const approval = 3
const revert=7
const cancel=8
const models = require('../../index')


    /***
        * Payment  IECBill
           */
          class IecBill extends BasicIEC {

            constructor(contract, price, idAppeal, businessId,req) {
                   super(contract, price)
                   this.details=details
                   this.prePay=prePaidPay
                   this.approval=approval 
                   this.revert=revert 
                   this.cancel=cancel
                   this.req=req
            }
        
        /***
       * check  what price of  bill
          * return price of  bill 
          */
        async getBillPrice() {
            if ((await this.isDemoTransactionUser())) {
                const address ="only Russian"
                const name = "ravtech"
                const price ="250"
                const invoice = "test1"
                const details = { "address": address, "name": name, "price": price, "invoice": invoice }
                this.info.price = price
                this.info.send = { "success": details }
                this.info.details=details 
                return this.info
           
            }
            else {
            
            const res = await this.request(this.details, "")

            if (res.error) {
                return res
            }
            const address = res["o_ktovet"]["$value"];
            const name = res["o_shem"]["$value"]
            const price = res["o_schom_hovot"]["$value"]
            const invoice = res["o_invoice"]["$value"]
            const details = { "address": address, "name": name, "price": price, "invoice": invoice }
            this.info.price = price
            this.info.send = { "success": details }
            this.info.details=details 
            return this.info
        }
       
    }
}
    module.exports = IecBill
  
