import React from 'react';
import { Button, Container, Row, Col, Collapse } from 'reactstrap';

import { postNewManualCard } from '../DataProvider/DataProvider';

import { LanguageManager as LM } from "../LanguageManager/Language";
import { Notifications } from '../Notifications/Notifications';
import { Loader } from '../Loader/Loader';

import InputTypeDate from '../InputUtils/InputTypeDate';
import InputTypeText from '../InputUtils/InputTypeText';
export default class ManualCardsForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isCreatingNew: false,
        }
        this.fields = {};
    }
    componentDidMount() {
        let nowDate = new Date();
        let year = nowDate.getFullYear();
        let month = nowDate.getMonth() + 1;
        month = month.toString().length < 2 ? "0" + month : month;
        let day = nowDate.getDate();
        day = day.toString().length < 2 ? "0" + day : day;
        let nextYearToDay = (year + 1) + "-" + month + "-" + day;
        this.fields.expiry_date.setValue(nextYearToDay)
    }

    newClicked = (_e) => {
        this.setState({ isCreatingNew: !this.state.isCreatingNew }, () => { if (this.state.isCreatingNew) this.inUse.setValue(LM.getString("no")); });
    }

    saveClicked = () => {
        if (this.props.isProductFound) {
            let newManualCard = {};
            let requestValid = true;
            Object.keys(this.fields).forEach((key) => {
                let value = this.fields[key].getValue();
                if (!value.valid) requestValid = false;
                else {
                    if (value.value === "") newManualCard[key] = null;
                    else newManualCard[key] = value.value;
                }
                if (!requestValid) {
                    Notifications.show(LM.getString("notValidField"), 'danger');
                    return;
                }
            });
            if (requestValid) {
                newManualCard.product_id = this.props.selectedProductId;
                Loader.show();
                postNewManualCard(newManualCard).then((res) => {
                    setTimeout(() => {
                        Loader.hide();
                    }, 1000)
                    Notifications.show(LM.getString("product") + " " + LM.getString("updatedSuccessfully"), 'success');
                    this.setState({ isCreatingNew: false });

                    this.fields.card_number.setValue("");
                    this.fields.code.setValue("");
                    this.fields.expiry_date.setDefault();

                    this.props.getCards && this.props.getCards()

                }).catch(
                    (err) => {
                        setTimeout(() => {
                            Loader.hide();
                        }, 1000)
                        Notifications.show(err, 'danger');
                    }
                )
            }
        }
        else Notifications.show(LM.getString("notSelected") + LM.getString("product"), 'warning');
    }

    render() {
        /** data for "Card Number" field (InputTypeText InputUtils component) */
        const cardNumberProps = {
            type2: true,
            required: true,
            title: LM.getString("cardNumber"),
            id: "ManualCardsFormCardNumber",
        };
        /** data for "Card Code" field (InputTypeText InputUtils component) */
        const cardCodeProps = {
            type2: true,
            required: true,
            title: LM.getString("cardCode") + ":",
            id: "ManualCardsFormCardCode",
        };
        /** data for "In Use" field (InputTypeText InputUtils component) */
        const inUseProps = {
            disabled: true,
            type2: true,
            title: LM.getString("used"),
            id: "ManualCardsFormInUse",
        };

        /** data for "Expiry Date" field (InputTypeDate InputUtils component) */
        const expiryDateProps = {
            type2: true,
            required: true,
            /** format: YY-MM-DD */
            min: "1900-01-01",
            title: LM.getString("expiryDate"),
            id: "ManualCardsFormExpiryDate",
        };

        return (
            <>
                <Button
                    className='manualCardsButton mb-4'
                    onClick={this.newClicked}
                    id={`manualCardsFormNewButton`}
                    disabled={!this.props.isProductFound}
                >
                    {this.state.isCreatingNew ? LM.getString("cancel") : LM.getString("new")}
                </Button>
                <Collapse isOpen={this.state.isCreatingNew} >
                    <Container className="createProductInputBlock">
                        <Row>
                            <Col >
                                <InputTypeText
                                    {...cardNumberProps}
                                    ref={(componentObj) => { this.fields.card_number = componentObj }}
                                />
                            </Col>
                            <Col >
                                <InputTypeText
                                    {...cardCodeProps}
                                    ref={(componentObj) => { this.fields.code = componentObj }}
                                />
                            </Col>
                            <Col >
                                <InputTypeDate
                                    {...expiryDateProps}
                                    ref={(componentObj) => { this.fields.expiry_date = componentObj }}
                                />
                            </Col>
                        </Row>
                        <Row style={{ height: '15px' }} />
                        <Row>
                            <Col sm='4'>
                                <InputTypeText
                                    {...inUseProps}
                                    ref={(componentObj) => { this.inUse = componentObj }}
                                />
                            </Col>
                        </Row>
                    </Container>
                    <Row>
                        <Col sm='auto'>
                            <Button
                                className='contractDetailsButton contractDetailsButtonSave'
                                onClick={this.saveClicked}
                                id='manualCardsFormSaveButton'
                            >
                                {LM.getString("save")}
                            </Button>
                        </Col>
                    </Row>
                </Collapse>
            </>
        )
    }

}
