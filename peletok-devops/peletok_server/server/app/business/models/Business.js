const DataTypes = require('sequelize').DataTypes;
const BaseModel = require('../../BaseModel');
const moment = require('moment')

/**
 * Business model
 */
class Business extends BaseModel {

    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            name: {
                type: DataTypes.STRING(128),
                allowNull: true
            },
            business_identifier: {
                type: DataTypes.STRING(12),
                allowNull: true
            },
            delek_store_identifier: {
                type: DataTypes.STRING(12),
                allowNull: true
            },
            account_management: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            account_number: {
                type: DataTypes.STRING(24),
                allowNull: true
            },
            account_number_external: {
                type: DataTypes.STRING(24),
                allowNull: true
            },
            business_license_number: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            phone_number: {
                type: DataTypes.STRING,
                allowNull: true
            },
            use_article_20: {  // Use in Article 20 of the VAT Ordinance
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false
            },
            second_phone_number: {
                type: DataTypes.STRING,
                allowNull: true
            },
            opening_hours: {
                type: DataTypes.STRING,
                allowNull: true
            },
            payment_method: {
                type: DataTypes.ENUM(["הוראת קבע", "תשלום לסוכן", "תשלום מראש"]),
                allowNull: true
            },
            is_distributor: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false
            },
            use_point: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false
            },
            is_delek: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false
            },
            delek_num: {
                type: DataTypes.STRING,
                allowNull: true
            },
            invoice_email: {
                type: DataTypes.STRING,
                allowNull: true
            },
            is_admin: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false
            },
            is_abstract: { // business for profile
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false
            },
            abstract_type: {
                type: DataTypes.ENUM(["profit profile", "commission profile", "supplier profile"]),
                allowNull: true
            },
            use_distribution_fee: {
                type: DataTypes.BOOLEAN,
                allowNull: true,
            },
            distributor_business_id: {
                type: DataTypes.INTEGER,
                allowNull: true,
            }
        };
        this.scopes = {
            businessWithDistributor: (businessId) => {
                const models = require('../../index')
                 return {
                    where: {id: businessId},
                    include: [{
                        model: models.distributor.model,
                        as: 'Distributor',
                        include: [{
                            model: models.business.model,
                            as: 'Business',
                            where: {is_distributor: true}
                        }]
                    }]}
            } 
        }
        this.author = true;
        this.statusAuthor = true;
        this.statusField = true;
        this.statusTimestampField = true;
        this.prefix = 'BSN';
        this.options["indexes"] = [{ unique: true, fields: ["business_identifier"], where: { "deleted_at": null } }]

    }
    /**
     * creates associate for business model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.hasMany(models.branch.model, { as: 'Branch' });
        this.model.hasMany(models.userBusiness.model, { as: 'UserBusiness' });
        this.model.hasMany(models.businessDocument.model, { as: 'BusinessDocument' });
        this.model.hasMany(models.businessProduct.model, { as: 'BusinessProduct' });
        this.model.hasMany(models.businessAddress.model, { as: 'BusinessAddress' });
        // this.model.hasMany(models.businessTag.model, { as: 'BusinessTag' });
        this.model.hasMany(models.hashavshevetExport.model, { as: 'HashavshevetExport' });
        this.model.hasMany(models.businessPoint.model, { as: 'BusinessPoint' });
        this.model.hasMany(models.businessPointLog.model, { as: 'BusinessPointLog' });
        this.model.hasMany(models.businessContact.model, { as: 'BusinessContact' });
        this.model.belongsTo(models.distributor.model, { as: 'Distributor' })
        this.model.hasMany(models.transaction.model, { as: 'Transaction' });
        this.model.hasMany(models.bannerBusiness.model, { as: 'BannerBusiness' });
        this.model.hasMany(models.businessFinance.model, { as: 'BusinessFinance' });
        this.model.hasMany(models.businessFinanceLog.model, { as: 'BusinessFinanceLog' });
        this.model.hasMany(models.collectionDay.model, { as: 'CollectionDay' });
        this.model.belongsTo(models.business.model, { as: 'Seller', foreignKey: "seller_id" })
        this.model.hasMany(models.businessPayment.model, { as: 'BusinessPayment' });
        this.model.hasMany(models.message.model, { as: 'MessagesSend', foreignKey: "from_business_id" });
        this.model.hasMany(models.message.model, { as: 'MessagesReceipt', foreignKey: "to_business_id" });
        this.model.hasMany(models.messageBusiness.model, { as: 'MessageBusiness' });
        this.model.hasMany(models.businessNotificationByTag.model, { as: 'BusinessNotificationByTag' });
        this.model.belongsToMany(models.supplier.model, { as: 'Suppliers', through: models.businessSupplier.model })
        this.model.belongsToMany(models.role.model, {through: 'BusinessRole' })
        this.model.belongsTo(models.user.model, { as: 'Agent' })
        this.model.belongsToMany(models.tag.model, {through: models.businessTag.model, as: 'Tag'});

    }

}

let business = new Business()
// Hook for create businessProduct after business is created
business.model.afterCreate(async (business, options) => {
    const Product = require('../../supplier/models/Product').model
    const BusinessProduct = require('../../business/models/BusinessProduct').model;
    const businessFinanceLogModel = require("../../index").businessFinanceLog.model;
    const businessPointLogModel = require("../../index").businessPointLog.model;

    let businessProductsToCrate;
    let products = await Product.findAll()
    if (products.length != 0) {
        businessProductsToCrate = products.map((product) => {
            return {
                timestamp_start: moment().toDate(), business_id: business.id, product_id: product.id, is_authorized: true
            }
        })
        await BusinessProduct.bulkCreate(businessProductsToCrate)
    }
    const Supplier = require('../../supplier/models/Supplier').model
    const BusinessSupplier = require('../../supplier/models/BusinessSupplier').model
    let businessSuppliersToCrate;
    let suppliers = await Supplier.findAll()
    if (suppliers != undefined && suppliers.length != 0) {
        let defaultCreation;
        businessSuppliersToCrate = suppliers.map((supplier) => {
            defaultCreation = supplier.default_creation
            if (business.id == 1 || supplier.is_authorized) {
                defaultCreation = true
            }
            return {
                timestamp_start: moment().toDate(), business_id: business.id, supplier_id: supplier.id, is_authorized: defaultCreation
            }
        })
        await BusinessSupplier.bulkCreate(businessSuppliersToCrate)
    }
    const earliestDate = "0001-01-01T00:00:00.000Z";
    let row = await businessFinanceLogModel.findAll(
        {
            where: {business_id: business.dataValues.id, timestamp_end: null},
            attributes: ['timestamp_start']
        });
    let attributes = {
        balance: 0, frame: 0, temp_frame: 0, business_id: business.dataValues.id,
        timestamp_start: moment(earliestDate).toDate()
    };
    if (row.length > 0) {
        attributes.timestamp_end = row[0].timestamp_start;
    }
    await businessFinanceLogModel.create(attributes);

    let pointsRow = await businessPointLogModel.findAll(
        {
            where: {business_id: business.dataValues.id, timestamp_end: null},
            attributes: ['timestamp_start']
        });
    let pointsAttributes = {
        sum: 0, business_id: business.dataValues.id,
        timestamp_start: moment(earliestDate).toDate()
    };
    if (pointsRow.length > 0) {
        pointsAttributes.timestamp_end = pointsRow[0].timestamp_start;
    }
    await businessPointLogModel.create(pointsAttributes)
});

// Hook for filter businesses (only real business)
// To get the businesses that are past abstract in where object
//     ["is_abstract"] = true;
business.model.beforeFind((options) => {
    options.where = options.where || {};
    if (options.where["is_abstract"] === undefined)
        options.where["is_abstract"] = false;
});


module.exports = business;