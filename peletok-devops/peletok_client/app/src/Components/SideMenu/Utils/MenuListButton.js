import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
import { Link } from 'react-router-dom';
import '../SideMenu.css';

export class MenuListButton extends Component {
    render() {

        const pathname = this.props.location.pathname;
        let selected = (pathname.substring(pathname.lastIndexOf("/") + 1) == this.props.id.substring(this.props.id.lastIndexOf("/") + 1));

        return (
            <Row tag={Link}
                to={{ pathname: `${this.props.path}/${this.props.id}`, data: this.props.data }}
                onClick={this.props.toggle}
                className={"noTextDecor px-1"}>
                <Col className={"mx-2 px-2 py-2 menuListButton"}>
                    <span className={selected ? "menuSelected " : ""}>{this.props.name}</span>
                </Col>
            </Row>
        );
    }
}