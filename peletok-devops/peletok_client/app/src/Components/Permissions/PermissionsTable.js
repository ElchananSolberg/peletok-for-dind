import React from 'react';
import { Table } from 'reactstrap';
import { Scrollbars } from 'react-custom-scrollbars';
import { LanguageManager as LM } from "../LanguageManager/Language";
import PermissionsGroup from './PermissionsGroup';


export default class PermissionsTable extends React.Component {
    constructor(props) {
        super(props);
        
        
    }
  
    
 
    
    render() {
        return (
            <React.Fragment>
               
                <div className={'table-responsive table-css ' + (LM.getDirection() === "rtl" ? "table-rtl" : "")}>
                    <Scrollbars style={{ width: '100%', height: '490px' }}>
                        <Table bordered striped size="sm" id="permissoin-tabel" >
                            <thead>
                                <tr >
                                     {Object.keys(this.props.actionNames).map((key) => {
                                         return (
                                             <th style={this.props.actionNames[key].style} >
                                                 {this.props.actionNames[key].title}
                                             </th>)
                                     })}
                                </tr>
                            </thead>
                        
                                {Object.keys(this.props.permissions).map((key)=>{
                                    return <PermissionsGroup 
                                      groupName={this.props.permissionGroups[key] || null}                                     
                                      permissions={this.props.permissions[key]} 
                                      />
                                })}
                           
                        </Table>
                    </Scrollbars>
                </div>
               
               
            </React.Fragment >
        );
    }
}