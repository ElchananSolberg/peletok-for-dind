const moment = require('moment');
const {LM} = require('../config/LanguageManager');

/***
 * Creates messageBusiness row(s)
 * @param messageId {Number} message id
 * @param toBusinessId {Number} business id represents a recipient
 * (if null create row for any of business)
 * @returns {Promise<[]>}
 */
async function createMessageBusiness(messageId, toBusinessId) {
    const models = require("../index");
    const messageBusinessModel = models.messageBusiness.model;
    const businessModel = models.business.model;
    let messageBusiness = [];
    if (toBusinessId) {
        await messageBusinessModel.create({
            message_id: messageId,
            business_id: toBusinessId
        }).then(r => {
            messageBusiness.push(r.id)
        })
    } else {
        let allBusiness = await businessModel.findAll({attributes: ["id"]});
        if (allBusiness.length > 0) {
            await messageBusinessModel.bulkCreate(allBusiness.map(b => {
                return {message_id: messageId, business_id: b.id}
            }), {returning: ["id"]}).then(r => {
                messageBusiness = r.map(mb => {
                    return mb.id
                })
            })
        }
    }
    return messageBusiness;
}


/**
 * Returns sequelize query for tag id and the businesses that subscribe to notification for this tag
 * @param tagName {string} tag for tag details
 * @returns {Promise<Model>|Promise<Model | null>|Promise<Model>}
 */
function getTagDetails(tagName) {
    const models = require("../index");
    const tagOfMessageModel = models.tagOfMessage.model;
    const businessNotificationByTagModel = models.businessNotificationByTag.model;
    return tagOfMessageModel.findOne({
        where: {tag_name: tagName},
        attributes: ["id"],
        include: {model: businessNotificationByTagModel, as: "BusinessNotificationByTag", attributes: ["business_id"]}
    })
}

/***
 * Creates notification about gift order
 * @param orderUserId{number} the order (note order saved by user and message send from
 * business)
 * @param giftName{number} name of gift
 * @param orderGiftId{number} order id
 * @returns {Promise<void>}
 */
async function createOrderGiftNotification(orderUserId, giftName, orderGiftId) {
    const models = require("../index");
    const messageModel = models.message.model;
    const messageTagModel = models.messageTag.model;
    const userBusinessModel = models.userBusiness.model;
    let orderBusinessId = (await userBusinessModel.findOne({where: {user_id: orderUserId}})).business_id;
    let messageText = "בוצעה הזמנת " + giftName + "\n מזהה ההזמנה " + orderGiftId;
    let orderTagDetails = await getTagDetails("giftOrder");
    let data = {
        message: messageText,
        timestamp_sending: moment().toDate(),
        from_business_id: orderBusinessId,
        MessageTag: {tag_of_message_id: orderTagDetails.id, mark: true}
    };
    //if you changes to bulkCreate please add afterBulkCreate hook in Message class
    for (const recipient of orderTagDetails.BusinessNotificationByTag) {
        let message = {...data, to_business_id: recipient.business_id};
        await messageModel.create(message, {
            include: {
                model: messageTagModel,
                as: "MessageTag"
            }
        });
    }

}

/***
 * Creates notification about gift order
 * @param cancelUserId{number} the order (note order saved by user and message send from
 * business)
 * @param giftName{string} name of gift
 * @param orderGiftId{number} order id
 * @returns {Promise<void>}
 */
async function createCancelOrderNotification(cancelUserId, giftName, orderGiftId) {
    const models = require("../index");
    const messageModel = models.message.model;
    const messageTagModel = models.messageTag.model;
    const userBusinessModel = models.userBusiness.model;
    let cancelBusinessId = (await userBusinessModel.findOne({where: {user_id: cancelUserId}})).business_id;
    let messageText = "בוטלה הזמנת " + giftName + "\n מזהה ההזמנה " + orderGiftId;
    let cancelTagDetails = await getTagDetails("cancellation");
    let data = {
        message: messageText,
        timestamp_sending: moment().toDate(),
        from_business_id: cancelBusinessId,
        MessageTag: {tag_of_message_id: cancelTagDetails.id, mark: true}
    };
    //if you changes to bulkCreate please add afterBulkCreate hook in Message class
    for (const recipient of cancelTagDetails.BusinessNotificationByTag) {
        let message = {...data, to_business_id: recipient.business_id};
        await messageModel.create(message, {
            include: {
                model: messageTagModel,
                as: "MessageTag"
            }
        });
    }
}

/***
 * Creates notification about gift order
 * @param cancelBusinessId{number} the cancel
 * @param transactionIdToCancel{number} id of transaction to cancel
 * @param cancelIdentifier {number} transaction id of cancel request
 * @returns {Promise<void>}
 */
async function createCancelRequestActionNotification(cancelBusinessId, transactionIdToCancel, cancelIdentifier= null) {
    const models = require("../index");
    const messageModel = models.message.model;
    const messageTagModel = models.messageTag.model;
    let messageText = "הוגשה בקשה לביטול טרנזקציה מספר: " + transactionIdToCancel;
    if(cancelIdentifier){
        messageText += " מזהה הבקשה: " + cancelIdentifier;
    }
    let cancelTagDetails = await getTagDetails("cancellation");
    let data = {
        message: messageText,
        timestamp_sending: moment().toDate(),
        from_business_id: cancelBusinessId,
        MessageTag: {tag_of_message_id: cancelTagDetails.id, mark: true}
    };
//if you changes to bulkCreate please add afterBulkCreate hook in Message class
    for (const recipient of cancelTagDetails.BusinessNotificationByTag) {
        let message = {...data, to_business_id: recipient.business_id};
        await messageModel.create(message, {
            include: {
                model: messageTagModel,
                as: "MessageTag"
            }
        });
    }

}


/***
 * Creates notification about approved Cancel request
 * @param fromBusinessId{number} Approved business id (it's sender of the notification)
 * @param toBusinessId{number} Requester business id (it's recipient of the notification)
 * @param transactionIdToCancel{number} Canceled transaction ID
 * @returns {Promise<void>}
 */
async function createApprovedCancelActionNotification(fromBusinessId, toBusinessId, transactionIdToCancel, cancellationNote){
    const models = require("../index");
    const messageModel = models.message.model;
    const messageTagModel = models.messageTag.model;
    //Warning LM choice language by sender language not by recipient and according to the current language
    //TODO: Fix this Warning
    let messageText = LM.getString("approvedCancelStartMessage") +
        transactionIdToCancel + LM.getString("approvedCancelEndMessage");
    let cancelTagDetails = await getTagDetails("cancellation");
    let message = {
        timestamp_sending: moment().toDate(),
        from_business_id: fromBusinessId,
        to_business_id: toBusinessId,
        MessageTag: {tag_of_message_id: cancelTagDetails.id, mark: true}
    };
    if (cancellationNote && cancellationNote.length>0) {

     message.message = `${messageText}.| הערה:${cancellationNote}`
    } else {
        message.message = messageText
    }
    await messageModel.create(message, {
        include: {
            model: messageTagModel,
            as: "MessageTag"
        }
    });
}

/***
 * Creates notification about rejected Cancel request
 * @param fromBusinessId{number} rejecter business id (it's sender of the notification)
 * @param toBusinessId{number} Requester business id (it's recipient of the notification)
 * @param transactionIdToCancel{number} rejected cancel transaction ID
 * @returns {Promise<void>}
 */
async function createRejectedCancelActionNotification(fromBusinessId, toBusinessId, transactionIdToCancel){
    const models = require("../index");
    const messageModel = models.message.model;
    const messageTagModel = models.messageTag.model;
    //Warning LM choice language by sender language not by recipient and according to the current language
    //TODO: Fix this Warning
    let messageText = LM.getString("approvedCancelStartMessage") +
        transactionIdToCancel + LM.getString("rejectedCancelEndMessage");
    let cancelTagDetails = await getTagDetails("cancellation");
    let message = {
        message: messageText,
        timestamp_sending: moment().toDate(),
        from_business_id: fromBusinessId,
        to_business_id: toBusinessId,
        MessageTag: {tag_of_message_id: cancelTagDetails.id, mark: true}
    };
    await messageModel.create(message, {
        include: {
            model: messageTagModel,
            as: "MessageTag"
        }
    });
}
async function createBusinessActivationNotification(fromBusinessId, toBusinessId, languageCode,business){
    const models = require("../index");
    const messageModel = models.message.model;
    const messageTagModel = models.messageTag.model;
    let messageText = `${LM.getString("businessActivatedSuccessfully", languageCode)} ${ business.name} ${LM.getString("businessIdentifier", languageCode)} ${business.business_identifier} ${LM.getString("approved", languageCode)}.`;
    let cancelTagDetails = await getTagDetails("notifications");
    let message = {
        message: messageText,
        timestamp_sending: moment().toDate(),
        from_business_id: fromBusinessId,
        to_business_id: toBusinessId,
        MessageTag: {tag_of_message_id: cancelTagDetails.id, mark: true}
    };
    await messageModel.create(message, {
        include: {
            model: messageTagModel,
            as: "MessageTag"
        }
    });
}

module.exports.createMessageBusiness = createMessageBusiness;
module.exports.createOrderGiftNotification = createOrderGiftNotification;
module.exports.createCancelOrderNotification = createCancelOrderNotification;
module.exports.createCancelRequestActionNotification = createCancelRequestActionNotification;
module.exports.createApprovedCancelActionNotification = createApprovedCancelActionNotification;
module.exports.createRejectedCancelActionNotification = createRejectedCancelActionNotification;
module.exports.createBusinessActivationNotification = createBusinessActivationNotification;
