const BaseModel = require('../../BaseModel.js');
const DataTypes = require("sequelize").DataTypes;

/**
 * BusinessPointLog model
 */

class BusinessPointLog extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            sum: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            comment: {
                type: DataTypes.TEXT,
                allowNull: true
            },
            timestamp_start: {
                type: DataTypes.DATE,
                allowNull: false
            },
            timestamp_end: {
                type: DataTypes.DATE,
                allowNull: true
            }
        };
        this.author = true;
        this.prefix = "GFT";
    }

    /**
     * creates associate for BusinessPointLog model
     * @param models: objects in format {modelName: model}
     */
associate(models) {
        this.model.belongsTo(models.business.model, { as: 'Business' });
    }
}

module.exports = new BusinessPointLog();
