'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('ApiPermissionPermission',
      {
        permission_id: {
          type: Sequelize.INTEGER,
        },
        api_permission_id: {
          type: Sequelize.INTEGER,
        },
        created_at: Sequelize.DATE,
        updated_at: Sequelize.DATE,
        
      }
  )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('ApiPermissionPermission')
  }
};
