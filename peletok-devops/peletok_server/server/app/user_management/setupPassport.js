var passport = require('passport');
var models = require('../index.js');
const loginHistory = models.loginHistory;
const loginHistoryModel = loginHistory.model;
const userContact = models.userContact;
const userContactModel = userContact.model;
const ipModel = models.userIp.model;
const tokenModel = models.userToken.model;
const { getTypeNum } = require('../utils')
var LocalStrategy = require('passport-local').Strategy;
var { compareHash } = require('../utils')
const businessModel = models.business.model

const Op = require("sequelize").Op;
const { getNumStatus } = require("../utils");

const active = "active";

async function createToken() {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < 16; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

async function handelIpAndToken(req, user) {
    // If we have any issue with the current_ip: https://stackoverflow.com/a/14631683/6845383
    let is_alwoed_token = true
    let is_alwoed_ip = true
    let new_ip = false
    let new_token = ""
    let current_ip = (req.headers['x-forwarded-for'] || req.connection.remoteAddress).split(":");
    let current_token = req.body.token // Get token from req;
    current_ip = current_ip[current_ip.length - 1]
    let currentBusiness = await models.business.model.findOne({
        include: [
            {
                model: models.userBusiness.model,
                as: 'UserBusiness',
                where: { user_id: user.id },
                required: true,
                include:[
                    {
                        model:models.user.model,
                        as:'User',
                        include:[
                            {
                                model:ipModel,
                                as: 'UserIp'
                            },
                            {
                                model: tokenModel,
                                as: 'UserToken'
                            }
                        ]
                    }
                ]
            },
        ]
    });
        let user_id = currentBusiness.UserBusiness[0].user_id;
    if (currentBusiness.UserBusiness[0].User.num_ip_allowed) {
                let all_ips = currentBusiness.UserBusiness[0].User.UserIp.map(i => i.ip_address)
        if (all_ips.indexOf(current_ip) > -1) {
            is_alwoed_ip = true
        } else {
            if (all_ips.length < currentBusiness.UserBusiness[0].User.num_ip_allowed) {
                new_ip = true
                is_alwoed_ip = true;
            } else if (all_ips.length >= currentBusiness.UserBusiness[0].User.num_ip_allowed) {
                is_alwoed_ip = false
            }
        }
    }
    if (currentBusiness.UserBusiness[0].User.num_device_allowed) {
        let all_tokens = currentBusiness.UserBusiness[0].User.UserToken.map(t => t.token)
        if (all_tokens.indexOf(current_token) > -1) {
            is_alwoed_token = true
        } else {
            if (all_tokens.length < currentBusiness.UserBusiness[0].User.num_device_allowed) {
                new_token = await createToken()
                is_alwoed_token = true 
            } else if (all_tokens.length >= currentBusiness.UserBusiness[0].User.num_device_allowed) {
                is_alwoed_token = false
            }
        }
    }
    return [is_alwoed_token && is_alwoed_ip , new_token, user_id, new_ip, current_token, current_ip]
}


/**
 * Checks username and password in DB
 * @param username: string of username analogous to login_name field in DB
 * @param password: string of password
 * @param done: param that passing in calling of LocalStrategy
 * @returns {Promise<*>}
 */
async function auth(req, username, password, done) {
    const activeId = getNumStatus(active, "User");
    const passwordActiveId = getNumStatus(active, "UserPassword");
    var user = await models.user.model.findOne({
        where: {
            'login_name': username,
        },
        include: [{
            model: loginHistoryModel,
            as: "LoginHistory",
            limit: 1,
            order: [["timestamp_login", "DESC"]],
            attributes: [["timestamp_login", "last_login"], "user_id"],
            required: false
        }, {
            model: userContactModel,
            as: "UserContact",
            attributes: [["value", "phone_number"], "user_id"],
            required: false,
            where: { type: getTypeNum("phone_number", "UserContact") }
        },
        {
            model: models.userBusiness.model,
            as: 'UserBusiness',
            include: [{
                model: models.business.model,
                as: 'Business'
            }]
        }
        ]
    });

    if (user != null) {
        switch (user.status) {
            case getNumStatus('frozen', 'User'):
                return done(null, false, { message: 'User frozen' })
            case getNumStatus('blocked', 'User'):
                return done(null, false, { message: 'User blocked' })
            default:
                break;
        }
        let [is_ip_and_token_handelned, new_token, user_id, new_ip, current_token, current_ip] = await handelIpAndToken(req, user);
        if (is_ip_and_token_handelned) {
            var realPassword = await user.getUserPassword({
                where:
                    { status: { [Op.or]: [passwordActiveId, null] } }
            });
            let isPassCorect = await compareHash(password, realPassword[0]['password']);
            if (realPassword.length === 1 && isPassCorect) {
                await Promise.all([
                    new_token.length > 0 ? tokenModel.create({ token: new_token, user_id: user_id }).catch((err) => { console.log("err, ", err); }) : Promise.resolve(),
                    new_ip ? ipModel.create({ ip_address: current_ip, user_id: user_id }).catch((err) => { console.log("err, ", err); }) : Promise.resolve()
                ])


                //TODO: Change for getting the business id from the client
                let business_ids = await user.getUserBusiness({
                    include: {
                        model: models.business.model,
                        as: "Business",
                        include: {
                            model: models.branch.model,
                            as: "Branch",
                            include: {
                                model: models.terminal.model,
                                as: "Terminal"
                            }
                        }
                    }
                });

                //TODO we need to get specific terminal that associated to this user(meanwhile where getting pos[0])
                if (business_ids.length > 0) {
                    user.dataValues.business_id = business_ids[0].business_id;
                    if (business_ids[0].Business) {
                        user.dataValues.distributor_id = business_ids[0].Business.distributor_id;
                        user.dataValues.is_distributor = business_ids[0].Business.is_distributor;
                        user.dataValues.is_admin = business_ids[0].Business.is_admin;
                        if (business_ids[0].Business.Branch[0]) {
                            user.dataValues.branch_id = business_ids[0].Business.Branch[0].id
                            user.dataValues.branch = business_ids[0].Business.Branch[0]
                            if (business_ids[0].Business.Branch[0].Terminal[0]) {
                                user.dataValues.terminal_id = business_ids[0].Business.Branch[0].Terminal[0].id
                                user.dataValues.terminal = business_ids[0].Business.Branch[0].Terminal[0]
                            }

                        }
                    }
                }
                if (req.body && req.body.lang) {
                    await models.user.model.update({ language_code: req.body.lang.toUpperCase() }, { where: { id: user.id } }).then(() => { user.dataValues.language_code = req.body.lang.toUpperCase() }).catch(err => {
                        logger.error('Failed to update user language code: ' + err)
                    })
                }
                //TODO: move to after response or skip await
                const [token, ip_address] = await Promise.all([
                    models.userToken.model.findOne({ where: { token: new_token.length > 0 ? new_token : current_token } }),
                    models.userIp.model.findOne({ where: { ip_address: current_ip} })
                ])
                let rawCurrentUserIp = (req.headers['x-forwarded-for'] || req.connection.remoteAddress).split(":")
                let currentUserIp = rawCurrentUserIp[rawCurrentUserIp.length -1]
                await user.createLoginHistory({ timestamp_login: new Date(), business_token_id: (token || {}).id, business_ip_id: (ip_address || {}).id, user_ip: currentUserIp });
                user['my_token'] = new_token || "";
                let currentBusinessStatus = user.UserBusiness[0].Business.status
                switch (currentBusinessStatus) {
                    case getNumStatus('pending', 'Business'):
                        return done(null, false, { message: 'Business pending' })
                    case getNumStatus('locked', 'Business'):
                        return done(null, false, { message: 'Business locked' })
                    default:
                        break;
                }
                return done(null, user);
            }
        } else {
            return done(null, false, { message: 'Ip or token limit exceeded' })
        }

    }
    return done(null, false, { message: 'Incorrect credentials.' })
}


/**
 * Configuration for passport authentication (by username, password,
 * password status)
 */
passport.use(new LocalStrategy({ passReqToCallback: true }, auth));


/**
 * Session configuration
 */
passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser(function (user, done) {
    done(null, user);
});

module.exports.passport = passport;