var models = require('./index.js');
var supplier = models.supplier
supplierModel = supplier.model
var product = models.product
var productModel = product.model




async function createProduct(code_accounting, code_supplier, name, timestamp_start, timestamp_end, usage_time_limit, supplier_id, status, status_timestamp, status_author_id) {
    let newProduct = await productModel.create({
        code_accounting: code_accounting,
        code_supplier: code_supplier,
        name: name,
        timestamp_start: timestamp_start,
        timestamp_end: timestamp_end,
        usage_time_limit: usage_time_limit,
        supplier_id: supplier_id,
        status: status,
        status_timestamp: status_timestamp,
        status_author_id: status_author_id

    }).catch(
        function (err) {
            console.log(err.toString());
            return false
        })
}
async function createSupplier(code, name, statusField, statusTimestamp, statusAuthor, author) {
    let newSupplier = await supplierModel.create({
        code: code,
        name: name,
        status: statusField,
        status_timestamp: statusTimestamp,
        status_author_id: statusAuthor,
        author_id: author

    }).catch(
        function (err) {
            console.log(err.toString());
            return false
        })
}

// temp dev supplier vars:
var supplierCode = 'Code1'
var supplierName = 'name1'
var supplierStatus = 1
var supplierStatusTime = new Date
var supplierStatusAuthorId = 1
var supplierAuthorId = 1

//temp dev product vars:

var productCode_accounting = 'product code 1'
var productCodeSupplier = 'code supplier 1'
var productName = 'product Name 1'
var productTimestamp_start = new Date;
var productTimestamp_end = new Date
var productUsage_time_limit = 1
var productSupplierId = 1
var productStatus = 1
var productStatusTimestamp = 1
var productStatusAuthor_id = 3

// createProduct(productCode_accounting, productCodeSupplier, productName, productTimestamp_start, productTimestamp_end, productUsage_time_limit, productSupplierId, productStatus, productStatusTimestamp, productStatusAuthor_id)

createSupplier(supplierCode,supplierName,supplierStatus,supplierStatusTime,supplierStatusAuthorId,supplierAuthorId,)








