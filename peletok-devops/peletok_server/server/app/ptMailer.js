var nodemailer = require('nodemailer');
const smtpHost = process.env.SMTP_HOST || "smtp.mailtrap.io";
const smtpPort = process.env.SMTP_PORT || 2525;
const smtpUserName = process.env.SMTP_USER_NAME || "e402e395acf19c";
const smtpPassWord = process.env.SMTP_PASSWORD || "22ea03b66dda19";
const peletokMail = process.env.SMTP_MAIL || 'peletok@gmail.com'
const peletokDevMail = 'peletok_dev@ravtech.co.il'
const peletokSecretaryMail = 'peletok_secretary@ravtech.co.il'

class PtMailer {
    constructor(){
        this.transport = nodemailer.createTransport({
            host: smtpHost,
            port: smtpPort,
            auth: {
                user: smtpUserName,
                pass: smtpPassWord
            },
            tls: {
                // do not fail on invalid certs
                rejectUnauthorized: false
            },
            debug: false, // show debug output
            logger: false // log information in console
        });
        this.pathErrorStack = ['GET /headless-content.js.map', 'GET /contentDocumentStart.js', 'GET /undefined']
    }
    


    async send(from, to, subject, text, html, attachments) {

        let mailOptions = {
            from: from,
            to: to,
            subject: subject,
            text: text,
            html: html,
            attachments: attachments
        }
        return new Promise((resolve, reject) => {

            this.transport.sendMail(mailOptions, (error, info) => {
                if (error) {
                    console.log(error);
                    resolve(false)
                } else {
                    console.log('Message sent: %s', info.messageId);
                    resolve(true)
                }
            });
        })
    }

}

module.exports.PtMailer = new PtMailer();
module.exports.peletokMail = peletokMail;
module.exports.peletokDevMail = peletokDevMail;
module.exports.peletokSecretaryMail = peletokSecretaryMail;
