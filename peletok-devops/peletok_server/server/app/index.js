const fs = require('fs');
const path = require('path');

/**
 * Creates all models (by name directory)
 */
var models = {};
fs.readdirSync(__dirname, {withFileTypes: true}).filter(
    filesInBaseDir => filesInBaseDir.isDirectory()).forEach(dirsInBaseDir => {
        var dirName = path.join(__dirname, dirsInBaseDir.name);
        fs.readdirSync(dirName, { withFileTypes: true }).filter(
            dir => (dir.isDirectory()) && (dir.name === "models")).forEach(modelDir => {
                var modelDirName = path.join(dirName, modelDir.name);
                fs.readdirSync(modelDirName).forEach(f => {
                    models[f.slice(0, 1).toLowerCase() + f.slice(1, -3)] =
                        require(path.join(modelDirName, f));
                }
                )
            })

    });

/***
 * Creates associations for all models
 */
Object.values(models).forEach(model=>{model.wrapperAssociate(models);model.hooks()});
module.exports = models;