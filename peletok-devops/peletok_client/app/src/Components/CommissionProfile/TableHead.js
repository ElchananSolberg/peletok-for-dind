import React, { Fragment } from 'react';

/**
 * TableHead - component that builds and returns an array of <th> elements from the data sended by CommissionProfileTable component  . 
 */
export default class TableHead extends React.Component {
  render() {
    var mapKey = 0;
    var thArray = [];
    var tableHeadersObj = this.props.tableHeadersObj;
    thArray = Object.keys(tableHeadersObj).map((key) => {
                          return <th key={mapKey++}>{tableHeadersObj[key]}</th>
    });
    return (
      <Fragment>
        {thArray}
      </Fragment>


    );
  }
}