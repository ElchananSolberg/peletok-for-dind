import React, { Component } from 'react';
import './Hashavshevet.css';

import { Container, Row, Col, Button, Collapse, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

import { LanguageManager as LM } from '../LanguageManager/Language';

import InputTypeDate from '../InputUtils/InputTypeDate';
import InputTypeTime from '../InputUtils/InputTypeTime';
import InputTypeText from '../InputUtils/InputTypeText';

import { ReportTableContent } from '../ReportComponent/ReportTableContent';

/** DueToDateOfExportationToHashavshevetReport is a main component of DueToDateOfExportationToHashavshevetReport Page.  
 * DueToDateOfExportationToHashavshevetReport and its children are working with .css file: Hashavshevet.css - common for all Hashavshevet Reports Pages. */
class DueToDateOfExportationToHashavshevetReport extends Component {
    constructor(props) {
        super(props);

        this.state = {
            /** working variable that indicates if a user is on the stage of viewing a report */
            viewingReport: false,
            /** working variable that indicates if a user is on the stage of viewing a Orders table */
            viewingOrders: false,
            /** working variable that indicates if a user is on the stage of viewing a Order Details table */
            viewingOrderDetails: false,

            /** Table data */
            tableRows: [
                {
                    exportName: 'exportName',
                    dateOfExport: 'dateOfExport',
                },
                {
                    exportName: 'exportName',
                    dateOfExport: 'dateOfExport',
                },
            ],
            /** Orders table data */
            ordersTableRows: [
                {
                    forExport: 'forExport',
                    orderCode: 'order Code',
                    sellerName: 'sellerName',
                    terminalNumber: 'terminalNumber',
                    hashavshevetCustomerCode: 'hash. CustomerCode',
                    totalPayment: 'totalPayment',
                    balance: 'balance',
                    vat: 'VAT',
                    earliestDateInOrder: 'earliestDate InOrder',
                    latestDateInOrder: 'latestDate InOrder',
                },
                {
                    forExport: 'forExport',
                    orderCode: 'order Code',
                    sellerName: 'sellerName',
                    terminalNumber: 'terminalNumber',
                    hashavshevetCustomerCode: 'hash. CustomerCode',
                    totalPayment: 'totalPayment',
                    balance: 'balance',
                    vat: 'VAT',
                    earliestDateInOrder: 'earliestDate InOrder',
                    latestDateInOrder: 'latestDate InOrder',
                },
            ],
            /** Order Details table data */
            orderDetailsTableRows: [
                {
                    itemName: 'itemName',
                    hashavshevetItemNumber: 'hashavshevetItemNumber',
                    pricePerUnit: 'pricePerUnit',
                    quantity: 'quantity',
                    total: 'total'
                },
            ],
            /** working variable that indicates if a user is on the stage of viewing a Warning Modal window */
            warningModal: false,
            /** working variable that indicates if a user is on the stage of viewing a Export Orders Modal window */
            exportOrdersModal: false,
        }
    }

    /** Show Report Button click handler
     * TODO: Add functionality */
    showReportClicked = () => {
        this.setState({ viewingReport: true });
    }

    /** Report table row click handler
     * TODO: Add functionality: 
     *                          - due to e.target.parentElement.id send a GET request
     *                          - fill the Orders table with the data from the response (setState({ordersTableRows:res}) ).
     */
    reportRowClicked = (e) => {
        // e.target.parentElement.id;

        this.setState({ viewingOrders: true });
        e.persist();
    }

    /** Export Orders Button click handler
     * TODO: Add functionality */
    exportOrdersClicked = () => {
        this.toggleWarningModal();
    }
    /** toggles Warning modal window */
    toggleWarningModal = () => {
        this.setState({ warningModal: !this.state.warningModal });
    }
    /** Confirm Button inside Warning Modal window click handler
     * TODO: Add functionality */
    confirmClicked = () => {
        this.toggleWarningModal();
        this.toggleExportOrdersModal();
        setTimeout(() => {
            this.exportOrdersFileName.setValue("ExportOrdersToHashavshevetReport");
        }, 500)
    }
    /** toggle Export Orders Modal window */
    toggleExportOrdersModal = () => {
        this.setState({ exportOrdersModal: !this.state.exportOrdersModal });
    }
    /** Export Orders Button inside Export Orders Modal window click handler
     * TODO: Add functionality */
    finalExportOrdersClicked = () => {
        this.toggleExportOrdersModal();
    }

    /** Orders table row click handler
     * TODO: Add functionality: 
     *                          - due to e.target.parentElement.id send a GET request
     *                          - fill the Order Details Table with the data from the response (setState({orderDetailsTableRows:res}) ).
     */
    ordersRowClicked = (e) => {
        if (['TD', 'TR'].includes(e.target.tagName)) {
            // e.target.parentElement.id;

            this.setState({ viewingOrderDetails: true });
            e.persist();
        }
    }

    /** For Export checkbox change handler
    * TODO: Add functionality */
    forExportChanged = (e) => {
        // e.target.id;
        // e.target.checked;
    }

    render() {
        /** Payments Header  */
        const header = LM.getString('dueToDate_of_ExportationToHashavshevet_Report');

        /** data for "Starting Date" field (InputTypeDate InputUtils component) */
        const startingDateProps = {
            title: LM.getString("startingDate"),
            id: "DueToDateOfExportationToHashavshevet_StartingDate",
        };
        /** data for "Ending Date" field (InputTypeDate InputUtils component) */
        const endingDateProps = {
            title: LM.getString("endingDate"),
            id: "DueToDateOfExportationToHashavshevet_EndingDate",
        };

        /** data for "Starting Time" field (InputTypeDate InputUtils component) */
        const startingTimeProps = {
            title: LM.getString("startingTime"),
            id: "DueToDateOfExportationToHashavshevet_StartingTime",
        };
        /** data for "Ending Time" field (InputTypeDate InputUtils component) */
        const endingTimeProps = {
            title: LM.getString("endingTime"),
            id: "DueToDateOfExportationToHashavshevet_EndingTime",
        };

        const fileNameProps = {
            title: LM.getString("fileName") + ":",
            id: "ExportOrdersToHashavshevetFileName",
        }

        /** Table Header data */
        const tableHeadersObj = {
            exportName: LM.getString('exportName'),
            terminalNumber: LM.getString('dateOfExport'),
        };
        /** Orders table Header data */
        const ordersTableHeadersObj = {
            forExport: LM.getString('forExport'),
            orderCode: LM.getString('orderCode'),
            sellerName: LM.getString('sellerName'),
            terminalNumber: LM.getString('terminalNumber2'),
            hashavshevetCustomerCode: LM.getString('hashavshevetCustomerCode'),
            totalPayment: LM.getString('totalPayment2'),
            balance: LM.getString('balance2'),
            vat: LM.getString('vat'),
            earliestDateInOrder: LM.getString('earliestDateInOrder'),
            latestDateInOrder: LM.getString('latestDateInOrder'),
        };
        /** Order Details Table Header data */
        const orderDetailsTableHeadersObj = {
            itemName: LM.getString('itemName'),
            hashavshevetItemNumber: LM.getString('hashavshevetItemNumber'),
            pricePerUnit: LM.getString('pricePerUnit'),
            quantity: LM.getString('quantity'),
            total2: LM.getString('total2'),
        };

        /** data for Show Report button */
        const showReportButtonText = LM.getString("showReport");
        /** data for Confirm button */
        const confirmButtonText = LM.getString("confirm");
        /** data for Export Orders button */
        const exportOrdersButtonText = LM.getString("exportOrders");
        /** data for Cancel button */
        const cancelButtonText = LM.getString("cancel");
        /** data for Warning Modal window  */
        const warningMsgText = LM.getString("exportOrdersToHashavshevetWarningMsg");
        /** data for Orders sub header */
        const ordersHeaderText = LM.getString('orders');
        /** data for Order Details sub header */
        const orderDetailsHeaderText = LM.getString('orderDetails');

        return (
            <Container className="hashavshevetContainer">
                <Row >
                    <Col className="hashavshevetHeader">
                        {header}
                    </Col>
                </Row>
                <Container className="hashavshevetSettings">
                    <Row >
                        <Col sm='4'>
                            <InputTypeDate {...startingDateProps} ref={(componentObj) => { this.startingDate = componentObj }} />
                        </Col>
                        <Col sm='4'>
                            <InputTypeTime {...startingTimeProps} ref={(componentObj) => { this.startingTime = componentObj }} />
                        </Col>
                    </Row>
                    <Row >
                        <Col sm='4'>
                            <InputTypeDate {...endingDateProps} ref={(componentObj) => { this.endingDate = componentObj }} />
                        </Col>
                        <Col sm='4'>
                            <InputTypeTime {...endingTimeProps} ref={(componentObj) => { this.endingTime = componentObj }} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="auto">
                            <Button className="hashavshevetButton" id="hashavshevetShowReportButton" onClick={this.showReportClicked} > {showReportButtonText} </Button>
                        </Col>
                    </Row>
                </Container>
                <Collapse isOpen={this.state.viewingReport} >
                    <Row>
                        <Col >
                            <ReportTableContent
                                tableHeadersObj={tableHeadersObj}
                                tableRows={this.state.tableRows}
                                reportName={"DueToDateOfExportationToHashavshevetReport"}
                                trClick={this.reportRowClicked}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="auto">
                            <Button className="hashavshevetButton" id="hashavshevetExportOrdersButton" onClick={this.exportOrdersClicked} > {exportOrdersButtonText} </Button>
                        </Col>
                        <Modal isOpen={this.state.warningModal} toggle={this.toggleWarningModal} >
                            <ModalHeader className="hashavshevetModalHeader" >{exportOrdersButtonText}</ModalHeader>
                            <ModalBody className="hashavshevetModalBody" >
                                <Row>
                                    <Col >
                                        {warningMsgText}
                                    </Col>
                                </Row>
                            </ModalBody>
                            <ModalFooter className="hashavshevetModalFooter">
                                <Row>
                                    <Col sm="auto">
                                        <Button className="hashavshevetButton" onClick={this.confirmClicked}>{confirmButtonText}</Button>
                                    </Col>
                                    <Col sm="auto">
                                        <Button className="hashavshevetButton hashavshevetButtonCancel" onClick={this.toggleWarningModal}>{cancelButtonText}</Button>
                                    </Col>
                                </Row>
                            </ModalFooter>
                        </Modal>
                        <Modal isOpen={this.state.exportOrdersModal} toggle={this.toggleExportOrdersModal} >
                            <ModalHeader className="hashavshevetModalHeader" >{exportOrdersButtonText}</ModalHeader>
                            <ModalBody className="hashavshevetModalBody" >
                                <Row>
                                    <Col >
                                        <InputTypeText {...fileNameProps} ref={(componentObj) => { this.exportOrdersFileName = componentObj }} />
                                    </Col>
                                </Row>
                            </ModalBody>
                            <ModalFooter className="hashavshevetModalFooter">
                                <Row>
                                    <Col sm="auto">
                                        <Button className="hashavshevetButton" onClick={this.finalExportOrdersClicked}>{exportOrdersButtonText}</Button>
                                    </Col>
                                    <Col sm="auto">
                                        <Button className="hashavshevetButton hashavshevetButtonCancel" onClick={this.toggleExportOrdersModal}>{cancelButtonText}</Button>
                                    </Col>
                                </Row>
                            </ModalFooter>
                        </Modal>
                    </Row>
                </Collapse>
                <Collapse isOpen={this.state.viewingReport && this.state.viewingOrders} >
                    <Row >
                        <Col className="subHeader">
                            {ordersHeaderText}
                        </Col>
                    </Row>
                    <Row>
                        <Col >
                            <ReportTableContent
                                tableHeadersObj={ordersTableHeadersObj}
                                tableRows={this.state.ordersTableRows}
                                reportName={"Orders"}
                                trClick={this.ordersRowClicked}
                                inputTypeCheckBoxOnChange={this.forExportChanged}

                            />
                        </Col>
                    </Row>
                </Collapse>
                <Collapse isOpen={this.state.viewingReport && this.state.viewingOrders && this.state.viewingOrderDetails} >
                    <Row >
                        <Col className="subHeader">
                            {orderDetailsHeaderText}
                        </Col>
                    </Row>
                    <Row>
                        <Col >
                            <ReportTableContent
                                tableHeadersObj={orderDetailsTableHeadersObj}
                                tableRows={this.state.orderDetailsTableRows}
                                reportName={"OrderDetails"}
                            />
                        </Col>
                    </Row>
                </Collapse>

            </Container>
        )
    }
}

export default DueToDateOfExportationToHashavshevetReport;