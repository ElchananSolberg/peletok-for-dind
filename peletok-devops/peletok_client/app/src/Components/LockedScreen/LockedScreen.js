import React, { Component } from 'react';
import { Row, Col, Button, Spinner, Input, Label } from 'reactstrap'
import { LanguageManager as LM } from '../LanguageManager/Language';
import InputTypeSearchList from '../InputUtils/InputTypeSearchList';
import PtTable from '../PtTable/PtTable'
import TableHead from '../PtTable/TableHead'
import TableBody from '../PtTable/TableBody'
import TableRow from '../PtTable/TableRow'
// import UnpaidPaymentsTable from './UnpaidPaymentsTable'

import { getLockedBusiness, putSeller } from '../DataProvider/DataProvider'
import { Notifications } from '../Notifications/Notifications'
import { Confirm } from '../Confirm/Confirm';
import { Loader } from '../Loader/Loader';
import { BusinessStatusEnum } from '../../enums/'

/**
 * 
 */
export class LockedScreen extends Component {
    constructor(props) {
        super(props);
        this.tableHeaders = [
            'businessName',
            'customerNumber',
            'agentNumber',
            'status',
            'actions',
        ]

        this.state = {
            fileteredBusiness: [],
            loading: true,
            inProgress: [],
            businesses: [],
            listSuppliersNamesAndIDs: [],
            supplierSelectListProps: {
                disabled: false,
                options: [],
                default: LM.getString("AllSuppliers"),
                title: LM.getString("supplierSelect"),
                id: "supplierSelect",
            },


        };
    }


    getActivateHandler = (business) => {
        return (e) => {
            Confirm.confirm(LM.getString('sureAboutActivation')).then(()=>{
                this.setState({
                    inProgress: [business.id,...this.state.inProgress]
                },()=>{
                    putSeller(business.id, {status: BusinessStatusEnum.ACTIVE}).then(res => {
                           this.setState({
                                businesses: this.state.businesses.filter(b=>b.id!=business.id),
                                fileteredBusiness: this.state.businesses.filter(b=>b.id!=business.id),
                                inProgress: this.state.businesses.filter(id=>id!=business.id)
                           })
                    }).catch(err => {
                            
                            Notifications.show(LM.getString('freezeFailed'), 'danger')
                        }
                    );
                })
            })
        }
    }

    componentDidMount = async () => {
        getLockedBusiness().then((res)=>{
            this.setState({
                loading: false,
                businesses: res,
                fileteredBusiness: res
            })
        })
    }

    concatAllFields(business){
        return `${business.businessName}${business.customerNumber}${business.agentNumber}${LM.getString('businessStatuses')[business.status - 1]}`
    }

    filterAllFields = (e) => {
        let test = new RegExp(e.target.value);
        this.setState({
            fileteredBusiness: this.state.businesses.filter(b => e.target.value ? test.test(this.concatAllFields(b)) : true)
        })
    }

    

    render() {
        return (<div >
            <h3 className="firstDivString">  {LM.getString("locked")}</h3>

            <div >
                <Row className={'search'}>
                    <Label for="search-all" sm={2}>{LM.getString('searchAll')}</Label>
                    <Col sm="5" className='mb-3'>
                        <Input id="search-all" onChange={this.filterAllFields} />
                    </Col>
                </Row>
                <hr />
                <Row style={{ marginTop: '30px' }}>
                    <Col >
                        {this.state.fileteredBusiness.length > 0 ?
                            <PtTable>
                                <TableHead>
                                    {this.tableHeaders.map((h, i) => <th style={{ whiteSpace: 'nowrap' }} key={i}>{LM.getString(h)}</th>)}
                                </TableHead>
                                <TableBody>
                                    {this.state.fileteredBusiness.map((b, i) => {
                                        return <TableRow key={i} className="trx-row">
                                            {this.tableHeaders.filter(h => h != 'actions').map((k, index) => { return <td key={index}>{k == 'status' ? LM.getString('businessStatuses')[b[k] - 1]  : b[k]}</td> })}
                                            <td>
                                                <Button className="CommissionProfileSmallButton" style={{ marginLeft: '1px' }} onClick={this.getActivateHandler(b)} color="danger">{this.state.inProgress.indexOf(b.id) > -1 && <Spinner animation="border" size="sm" />}{LM.getString('activate')}</Button>
                                            </td>
                                        </TableRow>

                                    })}
                                </TableBody>
                            </PtTable> :
                            <PtTable>
                                <tr><td colspan={this.tableHeaders.length} >{this.state.loading ? <Spinner animation="border" size="sm" /> : LM.getString('noBusinessLockedFound')}</td></tr>
                            </PtTable>
                        }
                    </Col>
                </Row>


            </div>
        </div>


        );
    }
}
