var models = require('../index.js');
const { removeAbsolutePath } = require("../utils");
var product = models.product;
var barcodeType = models.barcodeType;
var productModel = product.model;
var barcodeTypeModel = barcodeType.model;
const GenericGet = require('../mapping/GenericGet').GenericGet;
const GenericCreate = require('../mapping/GenericCreate');
const logger = require('../logger');
const mapByType = require('../utils.js').mapByType
var Sequelize = require('sequelize');
const Op = Sequelize.Op;
var moment = require('moment')
const used = 2
const unUsed = 1


const manualCard = models.manualCard
const manualCardModel = manualCard.model


const categoryName = models.categoryName




const { getMappingPath, get, wrapperCreate } = require("../mapping/crudMiddleware");





/***
 * Fix category info to binary info (true, false)
 * @param prods array of products
 */
function fixCategoryVal(prods) {
    const keysToChange = ["internet", "recommended", "favorite"];
    for (const prod of prods) {
        for (const k of keysToChange) {
            prod[k] = prod[k] !== null;
        }
    }
}
/**
 * checks if manual cards db filters sent from client
 * @param {*} productId 
 * @param {*} filter 
 * @returns boolean
 */
function isFilterSent(productId, filter) {
    if (productId && filter !== undefined && filter !== null && filter === "true") { return true } return false
}


/**
 * Returns products by supplier id
 * @param supplierId supplierId for filter product (if null returns all products)
 * @return {Promise<{manualCards: Array, virtualCards: Array}>}
 */
async function getProducts(supplierId) {
    const GenericGet = require('../mapping/GenericGet').GenericGet;
    const whereObj = supplierId ? { supplier_id: supplierId } : null;
    let prods = await new GenericGet("product", whereObj).get();
    Object.values(prods).forEach(r => removeAbsolutePath(r, 0));
    fixCategoryVal(prods);
    let products = mapByType('product', prods)
    return products;
}

/**
 *
 * gets specific product by id
 @param req request object
 @param res response object
 */
async function wrapperGetProduct(req, res) {
    let mappingPath = getMappingPath(req);
    const filter = req.params.product_id ? { id: req.params.product_id } : {};
    let info = await get(mappingPath, filter);
    if (info.result) {
        fixCategoryVal(info.result);
    }
    res.customSend(info);
}


/**
 * 
 * @param {*} req 
 * @param {*} res 
 * delete product by id
 */
async function deleteProduct(req, res) {
    await productModel.findOne({
        where: { id: req.params.product_id }
    }).then(product => {
        if (product) {
            product.destroy().then(info => {
                res.send(info)
            })
        } else {
            res.status(400).send('failed')
        }
    })
}

/**
 *
 * @param {*} req
 * @param {*} res
 * delete barcodeType by id
 */
async function deleteBarcodeType(req, res) {
    await barcodeTypeModel.destroy({
        where: {id: req.params.id}, individualHooks:true
    }).then(() => {
        res.send("success");
    }).catch(e=>{
        logger.error(e);
        res.status(400).send('failed')
    })
}


/**
 * Converts the true and false to "true" and "false"
 * this is temporary
 * @param req request object
 */
function convertBoolDataToStr(req) {
    if (req.body.favorite === true) {
        req.body.favorite = "true";
    }
    if (req.body.favorite === false) {
        req.body.favorite = "false";
    }
    if (req.body.recommended === true) {
        req.body.recommended = "true";
    }
    if (req.body.recommended === false) {
        req.body.recommended = "false";
    }
    if (req.body.internet === true) {
        req.body.favorite = "true";
    }
    if (req.body.internet === false) {
        req.body.favorite = "false";
    }
}



/**
 * 
 * @param {*} req 
 * @param {*} res 
 * create manual card from req args
 */
async function createManualCard(req, res) {
    req.body['insert_date'] = moment().toDate();
    req.body['status'] = 1
    await wrapperCreate(req, res);
}
/**
 * gets manual cards from db to client by product id and "where" params
 * @param req request object
 * @param res response object
 * @returns a list of manual cards
 */

async function wrapperGetManualCards(req, res) {
    let productId = req.params.productId;
    let data = req.query;
    const whereObj = productId ? { product_id: productId, insert_date: { [Op.between]: [data.startDate, data.endDate] } } : null;
    if (isFilterSent(productId, data.isUsed)) {
        whereObj["status"] = used;
    }
    if (isFilterSent(productId, data.isActive)) {
        whereObj["expiry_date"] = {
            [Op.gte]: await moment().toDate()
        }
    }
    if (isFilterSent(productId, data.aboutToExpire)) {
        whereObj["expiry_date"] = { [Op.between]: [moment().toDate(), moment().add(+30, 'days').toDate()] }
    }
    if (isFilterSent(productId, data.expiredAndUnused)) {
        whereObj["status"] = unUsed;
        whereObj["expiry_date"] = {
            [Op.lt]: await moment().toDate()
        }
    }

    let info = await get("manual_card", whereObj);
    if (info.result) {
        fixCategoryVal(info.result);
    }
    res.customSend(info);
}

/**
 * delete manual card by card id
 * @param {*} req 
 * @param {*} res 
 */
async function deleteManualCard(req, res) {
    await manualCardModel.findOne({
        where: { id: req.params.id }
    }).then(card => {
        if (card) {
            card.destroy().then(info => {
                res.send(info)
            })
        } else {
            res.status(400).send('failed')
        }
    })
}

module.exports.wrapperGetProduct = wrapperGetProduct;
module.exports.deleteProduct = deleteProduct;
module.exports.getProducts = getProducts;
module.exports.wrapperCreateManualCards = createManualCard;
module.exports.wrapperGetManualCards = wrapperGetManualCards;
module.exports.deleteManualCard = deleteManualCard;
module.exports.fixCategoryVal = fixCategoryVal;
module.exports.deleteBarcodeType = deleteBarcodeType;


