const models = require('../index.js');
const whiteList = require('./path_white_list')

const Role = models.role.model;
const Permission = models.permission.model
const { getNumStatus } = require("../utils");


const TempPermissionLoggerMiddleware = async (req, res, next) => {
    try{
        if(req.method == 'OPTIONS' || !req.query.currentPath){
            return next();
        }
        

        let uri = req.url.split('/api/v1')[1]
        if(!uri) {uri = req.url}
        // remove query params from uri
        uri = uri.split('?')[0]
        
        // filter last uri element if uri has params example "/user/:userId"
        uriArr = uri.split('/');
        uri = uriArr.filter(u=>!/^\d+$/.test(u)).join('/')
            
        let UserRoles = await Role.scope({ method: ['userRoles', req.user.id] })
        const roles = await UserRoles.findAll();
        
        let path = req.method + " " + (uri[uri.length - 1] == '/' ? uri.slice(0, -1) : uri)
        let apiPermission = await Permission.findOne({
            where: {
                path: path
            },
        })
        if(!apiPermission){
                throw new Error('Path does not exist - ' + req.query.currentPath)
        }
        let uiPermission = await Permission.findOne({
            where: {
                path: req.query.currentPath
            },
            include: [{
                model: models.permission.model, as: 'ApiPermissions'
            }]
        })
        
        if(uiPermission){
            uiPermission.setApiPermissions([apiPermission,...uiPermission.ApiPermissions])
        }else{
            if(req.query.currentPath && req.query.currentPath != 'false'){
                throw new Error('Path does not exist - ' + req.query.currentPath)
            }
        }
         // for generic porpus
        delete req.query.currentPath
    
    } catch (e){
        console.error(e.message)
    }
    return next();
       
   
}

module.exports = TempPermissionLoggerMiddleware
