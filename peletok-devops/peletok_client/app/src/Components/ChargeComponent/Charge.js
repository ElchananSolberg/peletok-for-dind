import React, { Component } from 'react';
import './Charge.css';
import { Card, CardText, Container, Row, Col } from 'reactstrap';
import { Switch, Route, Link } from 'react-router-dom';
import { LanguageManager as LM } from '../LanguageManager/Language';
import { PrepaidChargeScreenRender } from './Prepaid/PrepaidChargeScreenRender';
import { BillsRenderer } from './Bills/BillsRenderer';
import { getSuppliers } from '../DataProvider/DataProvider';
import { SERVER_BASE } from '../DataProvider/DataProvider';
import {UserDetailsService} from "../../services/UserDetailsService";
import {BusinessStatusEnum} from "../../enums";

export class Charge extends Component {

     constructor(props) {
        super(props);
        this.state =
            {
                //TODO change mock data to promise based
                prePaid: [],
                bills: [],
                user_status: null
            };
        this.ChargeMenu = this.ChargeMenu.bind(this);
    }

    /**
     * renders a react-strap structure of the displayed buttons
     * @param dataArray - a list of the buttons to create . Objects must contain {id, image}
     * @param numCellsPerRow - the number of items to be displayed in each row.
     *                         can only be one of {1,2,3,4,6,12} if another value is given the function chooses the
     *                         grates value possible that is smaller then the given number
     * @param routePath - the sub route of /charge/ to route a click to. the result would be /charge/routePath/providerID
     * @returns {Component} react-strap structure of the displayed buttons
     */
    componentDidMount() {
        getSuppliers({}, true, true).then(
            res => {
                this.setState({ prePaid: res.prepaid || [] });
                this.setState({ bills: res.bills || [] });
                
            }).catch(
                (err) => {
                    alert(err);
            }
        )
        UserDetailsService.currentUserStatus.subscribe(userStatus => {
            this.setState({ user_status: userStatus})
        });
        UserDetailsService.getUserDetails()
    }


    cardRenderer(dataArray, routePath) {
        let rows = [];
        let cardArray = [];
        for (let j = 0; (j) < dataArray.length; j++) {
            let opacity = 1;
            let pointerEventValue = 'auto';
            if (dataArray[j].available === false) {
                opacity = 0.5;
                pointerEventValue = 'none';
            }
            cardArray[j] =
                <Col className="provider-col col-6 mb-3" key={"col" + dataArray[j].id} xl={2} lg={3} sm={4}>
                    <Link id={`id${dataArray[j].id}Link`} style={{ pointerEvents: `${pointerEventValue}`, textDecoration: "none" }} to={this.state.user_status !== BusinessStatusEnum.FROZEN ? `${this.props.match.url}/${routePath}/${dataArray[j].id}` : "#"}>
                        <Card className="providerCard providerCardBorderRadius" key={"card" + dataArray[j].id} body inverse
                            style={{ opacity: `${opacity}` }}>
                            <div className={"d-flex  " + (this.state.user_status === BusinessStatusEnum.FROZEN && "business_freeze")} style={{ backgroundColor: dataArray[j].logo_background_color }}>
                                <img alt='' className="provider-card-img m-auto" src={`${SERVER_BASE}/${dataArray[j].image}`} />
                            </div>
                            <CardText tag="h1" className={"provider-card-text " + (this.state.user_status === BusinessStatusEnum.FROZEN && "business_freeze")}>{dataArray[j].name}</CardText>
                        </Card>
                    </Link>
                </Col>
        }
        rows = (
        <React.Fragment>
                    {cardArray}
        </React.Fragment>
        );
        // }
        return (
            <React.Fragment>
                {rows}
            </React.Fragment>
        );
    }
    /**
    * renders the main view
    * @returns {Component} react-strap structure of the content page of the Charge main page.
    * TODO - consider exporting this to a different file (along with function cardRenderer(dataArray, numCellsPerRow))
    * TODO - switch mark lines to a real server call and add a loading display while info is not available
    */
    ChargeMenu() {
        return (
            <React.Fragment>
                <h3 className='color-page-header fs-26 font-weight-light'>
                    {LM.getString("charge")}
                    {this.state.user_status === BusinessStatusEnum.FROZEN && <span className={'message_business_freeze'}> {LM.getString("messageToBusinessFreeze")}</span>}
                </h3>
                <h5 className='top-border header pt-3'>{LM.getString("prepaid")}</h5>
                <Row className={'px-2'}>
                    {this.cardRenderer(this.state.prePaid, 'prepaid')}
                </Row>
                <h5 className='top-border header pt-3'>{LM.getString("bills")}</h5>
                <Row className={'px-2'}>
                    {this.cardRenderer(this.state.bills, 'bills')}
                </Row>
            </React.Fragment>
        )
    }
    /**
     * renders the correct page according to the route specified
     */
    render() {
        return (
            <React.Fragment>
                <Switch>
                    <Route exact path={`${this.props.match.path}/`} component={this.ChargeMenu} />
                    <Route path={`${this.props.match.path}/prepaid/:supplierId`} render={(routeProps)=> { return <PrepaidChargeScreenRender {...routeProps} operationType='charge'/>}} />
                    <Route path={`${this.props.match.path}/bills/:supplierId`} component={BillsRenderer} />
                </Switch>
            </React.Fragment>
        );
    }
}