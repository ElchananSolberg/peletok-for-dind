'use strict';
const moment = require('moment');

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
   let a =[{
    name: 'בדיקת שדות (כגון ת.ז.)',
    type: 'api',
    path: 'GET /user/valid_fields',
    permission_group_id: 11,
    created_at: moment().toDate(),
    updated_at: moment().toDate(),
},]
return queryInterface.bulkInsert('PRM_Permission', a,{})
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
   return Promise.resolve()
  }
};
