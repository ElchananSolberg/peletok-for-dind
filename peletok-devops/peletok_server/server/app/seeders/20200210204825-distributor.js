'use strict';
const moment = require('moment');
module.exports = {
  up: (queryInterface, Sequelize) => {
    let dateNow = moment().toDate();
    let distributers = [{
      id: 1, free_text: 'שעות פתיחה: 08:00-16:00',
      is_admin: true,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 2, free_text: 'שעות פתיחה: 08:00-16:00',
      is_admin: true,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 3, free_text: 'שעות פתיחה: 08:00-19:00',
      is_admin: false,
      created_at: dateNow,
      updated_at: dateNow
    }]
    return queryInterface.bulkInsert('BSN_Distributor', distributers, {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.bulkDelete('BSN_Distributor', null, {});
  }
};
