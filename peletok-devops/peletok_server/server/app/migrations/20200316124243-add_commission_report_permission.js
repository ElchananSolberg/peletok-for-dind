'use strict';
const {addPermission, renamePermission} = require('../dbUtils/addPermission');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      addPermission(queryInterface, 'דוח עמלות שיוצאו לחשבשבת', 'ui', '/reports/hashavshevet/export/CommissionReport', 7),
    ])
  },

  down: (queryInterface, Sequelize) => {
   return Promise.resolve()
  }
};
