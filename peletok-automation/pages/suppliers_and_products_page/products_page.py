from selenium.webdriver.common.by import By

class ProductsPage:
    product_choice_input = (By.ID, 'InputTypeSearchListCreateProductSelectProduct')
    product_choice_option = (By.XPATH, "//div[@class='css-fk865s-option' and contains(text(), '59')]")

    radio_button = (By.XPATH, '//label[@for="InputUtilsRadioCreateProductSelectService-{}"]')
    supplier_choice_input = (By.ID, 'InputTypeSearchListCreateProductSelectSupplier')
    conversation_minutes_input = (By.ID, 'InputTypeNumberCreateProductCallsMinutes')
    sms_input = (By.ID, 'InputTypeNumberCreateProductSMS')
    nefah_glicha_input = (By.ID, 'InputTypeTextCreateProductBrowsingSize')
    tokef_bayamim_input = (By.ID, 'InputTypeSearchListCreateProductValidityDays')
    conversations_conditions = (By.ID, 'InputTypeTextCreateProductConditionOfCalls')
    sihot_lirchout_input = (By.ID, 'InputTypeCheckBoxCreateProductCallsToWestBank')
    conversation_houl_price_input = (By.ID, 'InputTypeNumberCreateProductCostInIsraelAndWorld')
    other_input = (By.ID, 'InputTypeTextCreateProductOther{}')

    product_name = (By.ID, 'InputTypeTextCreateProductProductName{}')

    supplier_recognize = (By.ID, 'InputTypeTextCreateProductProductId')

    tag_choice = (By.ID, 'Tagtag-{}')

    price_for_consumer = (By.ID, 'InputTypeNumberCreateProductFinalPriceForConsumer')
    product_price = (By.ID, 'InputTypeNumberCreateProductPurchasePrice')
    distribution_fee = (By.ID, 'InputTypeNumberCreateProductDistributionFee')

    active_button_checkBox = (By.ID, 'InputTypeCheckBoxCreateProductActive')
    product_default_checkBox = (By.ID, 'InputTypeCheckBoxCreateProductDefault')
    permitted_cancel_checkbox = (By.ID, 'InputTypeCheckBoxCreateProductPermittedCancel')
    product_order = (By.ID, 'InputTypeNumberCreateProductOrder')
    select_profit_model = (By.ID, 'InputTypeSearchListCreateProductSelectProfitModel')
    select_alternative_product = (By.ID, 'InputTypeSearchListCreateProductSelectAlternativeProduct')