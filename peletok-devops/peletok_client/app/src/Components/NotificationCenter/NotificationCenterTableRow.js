import React, { Component } from 'react';
import { Input, Button, Collapse, CardBody, Card, CardSubtitle, CardFooter, Row, Col } from 'reactstrap';
import './NotificationCenter.css';

import { LanguageManager as LM } from "../LanguageManager/Language"
import {postMessage} from "../DataProvider/Functions/DataPoster";
import {Notifications} from "../Notifications/Notifications";
import eyeImage from "./eye.svg";
import eyeGrayImage from "./eye-gray.svg";
import starYellowImage from "./star-yellow.svg";
import starImage from "./star.svg";
import trashImage from "./trash.svg";
import {putMessage} from "../DataProvider/Functions/DataPuter";
import {businessTagOfMessageByName, deleteMessage, getUserMessages} from "../DataProvider/DataProvider";
import {Confirm} from "../Confirm/Confirm";


function getReadStatusImage(read) {
    return read ? eyeGrayImage: eyeImage;
}

function getFavoriteStatusImage(currentReadStatus) {
    return currentReadStatus ? starYellowImage : starImage;
}


function getStarButtonTitle(favorite) {
    return favorite?LM.getString("markAsUnfavoriteTitle"): LM.getString("markAsFavoriteTitle");
}

function getReadButtonTitle(read) {
    return read?LM.getString("markAsUnreadTitle"): LM.getString("markAsReadTitle");
}

class NotificationCenterTableRow extends Component {
    constructor(props) {
        super(props);

        this.state = {
            /** working variable for Collapse component*/
            collapse: false,
            /** two working variables for two different Card components - only one of them should be visible at a time */
            cardEditDisplay: 'none',
            cardViewDisplay: 'block',
            /** working variable for textAreaChange function. That function handles the onChange of Input type="textarea" (area where user types the text of his message) */
            textAreaValue: '',
            unread: this.props.unread,
            favorite: this.props.favorite,
        };
        this.eyeButton = {};
        this.starButton = {};
    }
    /** <Row> click handler */
    toggle = (e) => {
        /** When I click on the Row I want a Collapse to collapse and vice versa */
        this.setState({ collapse: !this.state.collapse });
    };

    /** Reply Button click handler */
    replyClick = (e) => {
        this.setState({ cardEditDisplay: 'block' });
        this.setState({ cardViewDisplay: 'none' });
        e.stopPropagation();
    };
    /** Close Button click handler */
    closeClick = (e) => {
        this.toggle();
        e.stopPropagation();
    };
    /** Close Button click handler  */
    sendClick = (e) => {
        if((!this.state.textAreaValue)||this.state.textAreaValue===""){
            Notifications.show(LM.getString("messageIsEmpty"), "danger");
            return;
        }
        let tagsInfo = {};
        let businessTagsInfo = {};
        postMessage({
            to_business_id: this.props.senderId,
            message: this.state.textAreaValue,
            tags: tagsInfo,
            business_tags: businessTagsInfo
        }).then(r=>{
            Notifications.show(LM.getString("messageSent"), "success")
        }).catch(e=>{
            Notifications.show(LM.getString("messageFailed"), "danger");
        });
        this.setState({ cardEditDisplay: 'none' });
        this.setState({ cardViewDisplay: 'block' });
        e.stopPropagation();
    };
    /** Cancel Button click handler */
    cancelClick = (e) => {
        this.setState({ cardEditDisplay: 'none' });
        this.setState({ cardViewDisplay: 'block' });
        e.stopPropagation();
    };

    /** User Message Text (Input type="textarea") onChange handler */
    textAreaChange = (e) => {
        e.persist();
        this.setState({ textAreaValue: e.target.value })
    };

    /**
     * Unread (Eye) Image click handler
     */
    eyeImageClicked = (e) => {
        e.stopPropagation();

        if(!businessTagOfMessageByName.read){
            Notifications.show(LM.getString("tagsNotLoaded"), 'danger');
            return;
        }
        const currentUnreadStatus = !this.state.unread;
        let currentReadStatus = !currentUnreadStatus;
        this.eyeButton.src = getReadStatusImage(currentReadStatus);
        this.eyeButton.title = getReadButtonTitle(currentReadStatus);
        putMessage(this.props.messageId, {business_tags: {[businessTagOfMessageByName.read]: currentReadStatus}}).then(() => {
            this.setState({unread: currentUnreadStatus});
            this.props.updateMessageReadStatus(this.props.messageId, currentReadStatus);
        }).catch(() => {
            Notifications.show(LM.getString("updateMessageStatusFailed"), "danger");
            currentReadStatus=!currentReadStatus;
            this.eyeButton.src = getReadStatusImage(currentReadStatus);
        })
    };

    starImageClicked = (e) => {
        e.stopPropagation();

        if(!businessTagOfMessageByName.favorite){
            Notifications.show(LM.getString("tagsNotLoaded"), 'danger');
            return;
        }

        let currentFavoriteStatus = !this.state.favorite;

        this.starButton.src = getFavoriteStatusImage(currentFavoriteStatus);
        this.starButton.title = getStarButtonTitle(currentFavoriteStatus);
        putMessage(this.props.messageId, {business_tags: {[businessTagOfMessageByName.favorite]: currentFavoriteStatus}}).then(()=>{
            this.setState({favorite: currentFavoriteStatus});
            this.props.updateMessageFavoriteStatus(this.props.messageId, currentFavoriteStatus);
        }).catch(e => {
            Notifications.show(LM.getString("updateMessageStatusFailed"), "danger");
            currentFavoriteStatus = !currentFavoriteStatus;
            this.starButton.src = getFavoriteStatusImage(currentFavoriteStatus);
        })
        };

    trashImageClicked = (e) => {
            e.stopPropagation();

            Confirm.confirm(LM.getString("areYouSureDeleteMessage")).then(() => {
                deleteMessage(this.props.messageId).then(() => {
                    this.props.deleteRow(this.props.messageId);
                    getUserMessages({}, true);
                }).catch(e => {
                    Notifications.show(LM.getString("deleteMessageFailed"), "danger");
                });
            })
        };


    render() {

        let pointerEvents = this.props.pointerEvents;
        let columns = this.props.columns;
        let subject = this.props.subject;
        let fullMessage = this.props.fullMessage;
        let viewMessageWindow = this.props.viewMessageWindow;
        let typeMessageWindow = this.props.typeMessageWindow;
        const messageId = this.props.messageId;
        const unread = this.state.unread;
        const read = !unread;
        const favorite = this.state.favorite;

        columns.push(
            <Col className={"px-2"} xs='2' key={'1'+messageId}>
                <Row className="notificationCenterMarginNone">
                <img onMouseLeave={this.imagesColMouseLeave}
                     className={"notificationCenterImage ml-2 " + (LM.getDirection() === "rtl" ? 'mr-auto' : 'ml-auto')}
                     id={"notificationCenterEyeIMG_Row#" + messageId}
                     src={getReadStatusImage(read)} alt="Eye Img"
                     title={getReadButtonTitle(read)}
                     ref={b => {
                         this.eyeButton = b;
                     }}
                     onClick={this.eyeImageClicked}/>
                    <img
                        className="notificationCenterImage mx-2"
                        title={getStarButtonTitle(favorite)}
                        id={"notificationCenterStarIMG_Row#" + messageId}
                        src={getFavoriteStatusImage(favorite)}
                        alt="Star Img"
                        ref={b => {
                            this.starButton = b;
                        }}
                        onClick={this.starImageClicked}/>
                    <img
                        className="notificationCenterImage mr-2"
                        id={"notificationCenterTrashIMG_Row#" + messageId}
                        title={LM.getString("trashTitle")}
                        src={trashImage} alt="Trash Img"
                        onClick={this.trashImageClicked}/>
                </Row>
            </Col>
        );
        return (
            <>
                <Row style={{ pointerEvents: pointerEvents }} onClick={this.toggle}
                    className={this.state.collapse ? "notificationCenterTableRowsOPENED" : (read ? "notificationCenterTableRows" : "notificationCenterTableRowsUNREAD")}>
                    {columns}
                    <Collapse onClick={(e) => {e.stopPropagation();}} isOpen={this.state.collapse} className="notificationCenterCollapseWidth">
                        <Card style={{ display: this.state.cardViewDisplay }} >
                            <CardBody className='notificationCenterCollapseBackground'>
                                <CardSubtitle className='notificationCenterCardSubtitle pb-2' id={"subject-message-" + messageId}>{subject}</CardSubtitle>
                                <div id={"text-message-" + messageId}>{fullMessage}</div>
                            </CardBody>
                            <CardFooter className='notificationCenterFlexContainer notificationCenterCollapseBackground notificationCenterCardFooterBorder'>
                                <Button className='notificationCenterSmallButtonsSameAttributes notificationCenterDarkButton'
                                    id={"notificationCenterCloseButton_Row#" + messageId}
                                    onClick={this.closeClick} >
                                    {viewMessageWindow.closeButton}
                                </Button>
                                <div className='notificationCenterFlexContainerRowBreaker' />
                                <Button className='notificationCenterSmallButtonsSameAttributes notificationCenterReplyButton'
                                    id={"notificationCenterReplyButton_Row#" + messageId}
                                    onClick={this.replyClick} >
                                    {viewMessageWindow.replyButton}
                                </Button>
                            </CardFooter>
                        </Card>

                        <Card style={{ display: this.state.cardEditDisplay }}>
                            <CardBody className='notificationCenterCollapseBackground' >
                                <CardSubtitle className='notificationCenterCardSubtitle'>
                                    <Row>
                                        <Col sm="2" className="notificationCenterWeakText">
                                            <span style={{ fontWeight: "normal" }} >{LM.getString('subject')}: </span>
                                        </Col>
                                        <Col sm="auto" >
                                            {'Re ' + subject}
                                        </Col>
                                    </Row>
                                </CardSubtitle>
                                <Row>
                                    <Col sm="2" className="notificationCenterWeakText">
                                        <span> {LM.getString('messageText')}: </span>
                                    </Col>
                                    <Col >
                                        <Input type="textarea" className='notificationCenterCollapseBackground notificationCenterCardFooterBorder notificationCenterTextarea' onChange={this.textAreaChange} name="text"
                                            id={"notificationCenterReplyTextarea_Row#" + messageId} placeholder={LM.getString('typeTextHere')} rows="4" />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm="2" />
                                    <Col className="notificationCenterWeakText notificationCenterOriginalMessageTextSize">
                                        <hr className="notificationCenterOriginalMessageMarginTop" />
                                        <b>{subject}</b>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm="2" />
                                    <Col className="notificationCenterWeakText notificationCenterOriginalMessageTextSize">
                                        {fullMessage}
                                    </Col>
                                </Row>
                            </CardBody>
                            <CardFooter className='notificationCenterFlexContainer notificationCenterCollapseBackground notificationCenterCardFooterBorder'>
                                <Button className='notificationCenterSmallButtonsSameAttributes notificationCenterDarkButton'
                                    id={"notificationCenterCancelButton_Row#" + messageId}
                                    onClick={this.cancelClick}>
                                    {typeMessageWindow.cancelButton}
                                </Button>
                                <div className='notificationCenterFlexContainerRowBreaker' />
                                <Button className="notificationCenterSmallButtonsSameAttributes notificationCenterSendButton"
                                    id={"notificationCenterSendButton_Row#" + messageId}
                                    onClick={this.sendClick} >
                                    {typeMessageWindow.sendButton}
                                </Button>
                            </CardFooter>
                        </Card>
                    </Collapse >
                </Row>
            </>
        );
    }
}
export default NotificationCenterTableRow;