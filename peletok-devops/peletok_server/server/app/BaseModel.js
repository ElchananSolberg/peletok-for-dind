const sequelizeInstance = require('./setupSequelize');
const DataTypes = require('sequelize').DataTypes;

/**
 * Hook for cascade in soft delete
 * @param {Object} instance instance to delete
 * @param options {Object}
 * @returns {Promise<*>}
 */
async function afterDestroy(instance, options){
    const Promise = require("sequelize/lib/promise");
    const logger = require("./logger");
    delete options.where; // for bulk delete
    let cascades = [];
    if (!!instance.constructor && !!instance.constructor.associations) {
        const keys = Object.keys(instance.constructor.associations);
        const length = keys.length;
        let association;

        association = instance.constructor.associations[keys[0]];
        for (let i = 0; i < length; i++) {
            association = instance.constructor.associations[keys[i]];
            if (association.options && (association.options.paranoidCascade === true ||
                (association.associationType==='HasMany' && association.options.paranoidCascade!==false))
            ) {
                cascades.push(association.accessors.get);
            }
        }
        if(association.associationType==='BelongsToMany' && association.options.paranoidCascade!==false){
            await association.options.through.model.destroy({where: {[association.foreignKey]: instance.id}}).catch(e=>{logger.error(e)});
            }
        return Promise.each(cascades, cascade => {
            return instance[cascade](options).then(instances => {
                // Check for hasOne relationship with non-existing associate ("has zero")
                if (!instances) {
                    return Promise.resolve();
                }
                if (!Array.isArray(instances)) instances = [instances];
                return Promise.each(instances, instance => {instance.destroy(options)});
            });
        })
    }

}


/**
 * General configuration for models
 * A instance represents a model and the instance.model represents Sequelize model
 */
class BaseModel {

    /**
     * Constructor
     * Configs the General configuration
     */
    constructor() {
        this.attributes = {}; // fields
        this.scopes = {}; 
        this.indexes = []; 
        this.options = {'underscored': true, 'paranoid': true}; // base options
        this.prefix = '';
        this.statusTimestampField = false;
        this.statusField = false;
        this.defaultStatus = null;
        this.author = false;
        this.statusAuthor = false;
        this.getterMethods = {}

        // Sequelize model name, default is model name
        this.modelName = this.constructor.name;
        this.config();
        this.define();
    }


    /***
     * Configs model
     */
    config() {
    }

    /**
     * defines Sequelize model
     */
    define() {
        if (this.prefix !== '') this.prefix += '_';
        this.addStatusFields();
        this.options["tableName"] = this.prefix + this.modelName;
        this.options["freezeTableName"] = true;
        this.options.scopes = this.scopes;
        this.options.indexes = this.indexes;
        this.options.getterMethods = this.getterMethods;
        this.model = sequelizeInstance.define(
            this.modelName,
            this.attributes,
            this.options);
        this.model.beforeUpdate((instance, options) => {
                // update data only if changed (0 is equal to '0')
                for (const k of Object.keys(instance._changed)){
                    // noinspection EqualityComparisonWithCoercionJS
                    if(instance.dataValues[k] == instance._previousDataValues[k]){
                        instance.dataValues[k] = instance._previousDataValues[k];
                        delete instance._changed[k]
                    }
                }
            }

        )
        ;
    }

    /**
     * Throws error if a Sequelize model for this model not defined yet
     */
    isDefined()
    {
        if (!this.model) {
            throw "a define method must be called before the associate method"
        }
    }

    /**
     * Creates author associate to user model
     * @param userModel: user sequelize model
     */
    createAuthorAssociate(userModel)
    {
        userModel.hasMany(this.model,
            {as: this.modelName + 'Author', foreignKey: 'author_id'});
        this.model.belongsTo(userModel,
            {as: 'Author', foreignKey: 'author_id'});
    }


    /**
     * Creates status_author associate to user model
     * @param userModel: user sequelize model
     */
    createStatusAuthor(userModel)
    {
        userModel.hasMany(this.model,
            {as: this.modelName + 'StatusesAuthor', foreignKey: 'status_author_id'});
        this.model.belongsTo(userModel,
            {as: 'StatusesAuthor', foreignKey: 'status_author_id'});
    }

    /**
     * Creates author and status author relation
     * @param user: user model
     */
    createUserAssociate(user)
    {
        this.isDefined();
        user.isDefined();
        if (this.author) {
            this.createAuthorAssociate(user.model)
        }
        if (this.statusAuthor) {
            this.createStatusAuthor(user.model)
        }
    }

    /**
     * Returns all fields of model
     * @returns {string[]}
     */
    getFields()
    {
        this.isDefined();
        return Object.keys(this.model.attributes)
    }


    /***
     * Adds a fields: status and statusTimestamp
     */
    addStatusFields()
    {
        if (this.statusField) {
            this.attributes["status"] = {
                type: DataTypes.INTEGER,
            };
            if(this.defaultStatus===undefined || this.defaultStatus===null){
                this.attributes["status"]["allowNull"] = true;
            }
            else{
                this.attributes["status"]["allowNull"] = false;
                this.attributes["status"]["defaultValue"] = this.defaultStatus;
            }
        }
        if (this.statusTimestampField) {
            this.attributes["status_timestamp"] = {
                type: DataTypes.DATE,
                allowNull: true
            }
        }

    }


    /***
     * Creates associate
     */
    associate()
    {
    }
    ;

    wrapperAssociate(models)
    {
        this.createUserAssociate(models.user);
        this.associate(models);
    }

    hooks(){
        this.model.afterDestroy(afterDestroy);
    }

}

module.exports = BaseModel;