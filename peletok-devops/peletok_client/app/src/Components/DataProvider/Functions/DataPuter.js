import axios from 'axios';
import { AuthService } from '../../../services/authService';
import {
    loadedData,
    getData,
    DEFAULT_REFRESH_TIME,
    SERVER_BASE,
    shouldRefetch
} from './../DataProvider'

/**
 * general put function that stores its data and returns it if up to date
 * @param {*} url the url to put
 * @param data
 * @param config
 * @param replaceWithBusinessId {Boolean} replace business identifier with business id or no
 */
function dataPuter(url, data = {}, config = {}, replaceWithBusinessId=true) {
    data = getData(data, config, replaceWithBusinessId);
    return new Promise(function (resolve, reject) {
        // check axios.put API
        // TODO check if we need to add withCredentials: true
        config.withCredentials = true
        axios.put(SERVER_BASE + AuthService.addQueryParams(url), data, config).then(
            function (response) {
                resolve(response.data);
            }
        ).catch(
            function (error) {
                // TODO - add real error handling
                reject(error);
            }
        );
    });
}

/*
 * ########################################
 *       ONLY WRITE BELLOW THIS LINE
 * ########################################
 */

export function putSellerUserData(id, data) {
    return dataPuter(`/user/${id}`, data);
}

export function putUpdateProductAuthorization(id, data) {
    return dataPuter(`/product/update_authorization/${id}`, data);
}

export function putCreditCards(id, data) {
    return dataPuter(`/credit_card/${id}`, data);
}
export function changePass(userId , twoPass) {
    return dataPuter(`/user/change_password/${userId}`, twoPass);
}
export function putSuppliersUpdate(id, data) {
    return dataPuter(`/supplier/${id}`,data, {});
}
export function putManualCard(cardId, data) {
    return dataPuter(`/product/manual_card/${cardId}`,data);
}
export function putProduct(id, data) {
    return dataPuter(`/product/${id}`,data);
}
export function putGift(id, data) {
    return dataPuter(`/gift/gift/${id}`,data);
}
export function putCancelGift( giftIDs) {
    return dataPuter(`/gift/cancel/`,giftIDs, {withCredentials: true});
}
export function putMessage(id, data) {
    return dataPuter(`/message/${id}`, data);
}
export function putSeller(id, data) {
    return dataPuter(`/business/business/${id}`, data, {}, false);
}
export function putApproveSeller(id, data) {
    return dataPuter(`/business/approve/${id}`,data, {}, false);
}
export function putDistributor(id, data) {
    return dataPuter(`/distributor/distributor/${id}`,data);
}
export function putTag(id, data) {
    return dataPuter(`/tag/tag/${id}`,data);
}
export function putBaner(id, data) {
    return dataPuter(`/banner/${id}`,data);
}
export function putCatalogNumberStatus(id, data){
    return dataPuter(`/barcode_type/${id}`, data)
}
export function putCatalogNumberEdit(id, data){
    return dataPuter(`/barcode_type/${id}`, data)
}
export function putBusinessProducts( data) {
    return dataPuter(`/admin/business_product/multi`, data, {}, false);  /** false - please do not replace id with business_identifier */
}
export function putBusinessProductsProfitPercentage( data) {
    return dataPuter(`/admin/business_product/percentage_profit`, data, {}, false);  /** false - please do not replace id with business_identifier */
}
export function putBusinessProductsEarningPoints( data) {
    return dataPuter(`/admin/business_product/points`, data, {}, false);  /** false - please do not replace id with business_identifier */
}
export function putBusinessUseDistributionFee( data) {
    return dataPuter(`/admin/business/use_distribution_fee`, data, {}, false);  /** false - please do not replace id with business_identifier */
}
export function putBusinessCommission( data) {
    return dataPuter(`/admin/business_product/commission`, data, {}, false);  /** false - please do not replace id with business_identifier */
}
export function putBusinessCommissionForFinalClient( data) {
    return dataPuter(`/admin/business_product/final_commission`, data, {}, false);  /** false - please do not replace id with business_identifier */
}
export function putSuppliersProfile(id, data){
    return dataPuter(`/business/profile/${id}`, data, {} );
}
export function putProfitPercentageProfile(data){
    return dataPuter(`/profit/profile`, data, {} );
}
export function putProfitProfileProfitPercentage( data) {
    return dataPuter(`/profit/profile/product`, data, {}, false);  /** false - please do not replace id with business_identifier */
}
export function putUserPassword(data) {
    return dataPuter(`/user/password/`, data, {});
}
export function putUpdateBalance(data){
    return dataPuter(`/admin/business/update_balance`, data, {}, false)
}