import React, { Component } from 'react';
import {
    Container, Row, Col, Button,
    Collapse
} from 'reactstrap';
import { Link } from 'react-router-dom';
import ReactSVG from 'react-svg';
import { LanguageManager as LM } from "../../LanguageManager/Language";
import { MenuListButton } from "./MenuListButton";

import Up from '../../../Assets/Icons/MenuIcons/upMenuArrow.svg';
import Down from '../../../Assets/Icons/MenuIcons/downMenuArrow.svg';
import '../SideMenu.css';
import {AuthService} from '../../../services/authService'

export class MenuLargeButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listOpen: false
        }
       
    }

    render() {
        let allowedLinks = this.props.list && this.props.list.filter(navItem=>AuthService.allowed(`${this.props.path}/${navItem.id}`))
        let selected = (this.props.location.pathname.indexOf(this.props.path) > -1);
        return (
            <Container tag={Button}
                className={"rounded-0 m-0 py-0 menuBigButton " + (selected ? "menuSelected" : "")}>
               { (!this.props.list || allowedLinks.length > 0) && // IF !this.props.list  the link is single link
                   <Row tag={this.props.list ? Row : Link}
                       to={(!this.props.list) ? this.props.path : ""}
                       onClick={(!this.props.list) ? this.props.toggle
                           : (e) => {
                               e.preventDefault();
                               this.setState({ listOpen: !this.state.listOpen })
                           }}
                       className={"py-1 noTextDecor"}>
                       <div className={"px-3"}>
                           <ReactSVG src={this.props.icon}
                               svgClassName={(selected ? "svgSelected" : "svgNotSelected") + " mx-0"} />
                       </div>
                       <div style={{ textAlign: "start" }} className={"px-0"}>
                           {this.props.text}
                       </div>
                       <div className={"px-3"}>
                           {this.props.list ?
                               <ReactSVG src={this.state.listOpen ? Up : Down}
                                   svgClassName={(selected ? "svgSelected svgFillSelected" : "svgNotSelected svgFillNotSelected") +
                                       (LM.getDirection() === "ltr" ? " mr-auto" : " ml-auto")} /> : <div />}
                       </div>
                   </Row>
               }
                {allowedLinks ?
                    <Collapse isOpen={this.state.listOpen}>
                        {allowedLinks.map((obj) => <
                            MenuListButton key={obj.id}
                            toggle={this.props.toggle}
                            {...obj}
                            data={obj.data}
                            path={this.props.path}
                            location={this.props.location} />)}
                    </Collapse> : <div />}
            </Container>
        );
    }
}