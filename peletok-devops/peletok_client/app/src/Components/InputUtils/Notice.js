import React, { Component } from 'react';
import './InputUtils.css';
import { Row, Col } from 'reactstrap';

import { Tooltip } from './Tooltip';


/**
 * Notice component lets to show a many-lines notice for user without an option to edit this notice. 
 * it can render the title above the notice.
 * it can render the tooltip to the side of the title.
 */
class Notice extends Component {

    render() {
        let title = this.props.title;
        let tooltipText = this.props.tooltip;
        let noticeText = this.props.noticeText ;
        let id = this.props.tooltipId;

        return (
            <>
                <Row>
                    <Col className={"d-flex justify-content-between line-height-1 " + (title ? 'py-1' : '') }>
                        {title ?
                            <div className="fs-17 white-space">{title}</div>
                            : <></>
                        }
                        {tooltipText ?
                            <Tooltip tooltipText={tooltipText} id={id} />
                            : <></>
                        }
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <textarea readOnly className="inputUtilsNotice" value={noticeText}>
                            
                        </textarea>
                    </Col>
                </Row>
            </>
        )
    }
}
export default Notice;