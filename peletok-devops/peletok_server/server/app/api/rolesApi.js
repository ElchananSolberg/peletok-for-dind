const express = require('express');
const rolesRouter = express.Router();
const models = require('../index.js');
const Op = require("sequelize").Op;
const Role = models.role.model;





/*-----------------------------    REST section    ---------------------------------*/

rolesRouter.get('/', async (req, res) => {
    let roles;
    let currentUserHasPermission = await models.user.model.findOne({
        include: [{
            model: models.role.model,
            attributes: [],
            include: [{
                model: models.permission.model,
                where: {
                    path: 'GET /sensitive_roles',
                },
                attributes: [],
                required: true,
                through: {
                    attributes: [],

                }
            }],
            required: true
        }],
        where: { id: req.user.id }
    })
    if (currentUserHasPermission) {
        roles = await Role.findAll({ attributes: ['id', 'name'] })
    } else {
        const UserRoles = models.role.model.scope({ method: ['userRoles', req.user.id]});
        const authorizedRoles = await UserRoles.findAll({
            attributes: ['id'],
            include: [{
                attributes: ['id', 'name'],
                model: models.role.model, as: 'AllowedRoles'
            }]
        })
        roles = authorizedRoles.reduce((n, r) => [...n, ...r.AllowedRoles], [])
    }
    res.send(roles)
})



rolesRouter.post('/', async (req, res) => {

    const role = await Role.create({
        name: req.body.name
    })
    res.send({id: role.id, msg: 'success'})
})



module.exports.rolesRouter = rolesRouter;
