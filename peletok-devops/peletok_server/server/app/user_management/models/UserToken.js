var BaseModel = require('../../BaseModel.js');
const DataTypes = require("sequelize").DataTypes;

/**
 * UserToken model
 */
class UserToken extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            token: {
                type: DataTypes.STRING(16),
                allowNull: true
            }
        };

        this.prefix = 'USR'

    }
     /**
     * creates associate for Token model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.user.model, { as: 'User' });
    } 


}

module.exports = new UserToken();