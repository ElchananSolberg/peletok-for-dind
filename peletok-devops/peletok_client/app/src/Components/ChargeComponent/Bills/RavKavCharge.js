import React, { Component } from 'react';
import { Button } from 'reactstrap';
import { Row, Col } from 'reactstrap';
import { Notifications } from '../../Notifications/Notifications';
import { postStartRavKavSession, postRavKavGetProducts } from '../../DataProvider/DataProvider'
import RavKavConnection from './ravkav-loader'

const RavKavId = '999';


export class RavKavCharge extends Component {

    constructor(props) {
        super(props)

        this.state = {
            services: [],
            sessionUrl: '',
            dumpUrl: '',
            selectedService: '',
            loadCard: 0,
            loadRes: null,
            billsPaymentObj: undefined
        }
    }

    connectToReader = (activationLink) => {
        console.log(activationLink);
        const frame = window.document.createElement('iframe');
        frame.src = activationLink;
        frame.style.display = 'None';
        window.document.body.appendChild(frame);
    }

    onGetCardDumpExecuteSuccess = (cardDump) => {
        this.setState({ dump: cardDump.result }, () => {
            postRavKavGetProducts(this.state.dump).then(res => {
                console.log("res: ", res);

                if (res.ravKavRes) {
                    this.setState({
                        serial_number: res.ravKavRes["serial_number"],
                        contracts: res.ravKavRes["contracts"],
                        contracts_display: res.ravKavRes["contracts_display"],
                        cancellables: res.ravKavRes["cancellables"]
                    }, () => {
                        console.log(this.state);
                    })
                }

            })
        })
    }


    renderContractDisplay = (contractsDisplay) => {
        let contractCards = [];
        contractCards = contractsDisplay.map(contract =>
            <Col lg={4} md={6} sm={4} xl={3} className='px-0 mb-4'>
                {contract}
            </Col>
        )
        return (<Row className={'m-0'}>{contractCards}</Row>)
    }

    renderContent = () => {

        return (
            <div>
                <div>{this.serial_number}</div>
                {this.state.contracts_display ?
                    this.renderContractDisplay(this.state.contracts_display)
                    : ""}
            </div>
        )
    }


    onStateChange = (status) => {
        this.setState({ status }, () => {
            if (this.state.status == "READY") {
                this.state.connection.execute(this.state.dumpUrl, 10000, this.onGetCardDumpExecuteSuccess,
                    function onFailure(error) {
                        alert("<%=Resources.Resource1.ErrorReadingCard %>", "<%=Resources.Resource1.Error %>", new function () { });
                        console.log(error).bind(this);
                    });
            }
        })
        return status;
    }

    onSuccess = (connection) => {
        this.setState({ connection }, () => {
            this.connectToReader(this.state.connection.getDesktopActivationLink())
        })
        return;
    }

    onError = (errorCode) => {
        alert("errorCode: ", errorCode);
        return;
    }

    componentDidMount() {
        postStartRavKavSession().then(res => {
            this.setState({
                sessionUrl: res.ravKavRes.data.session_url,
                dumpUrl: res.ravKavRes.data.dump_url,
            }, () => {
                RavKavConnection.connect(this.state.sessionUrl, 10000000, this.onStateChange, this.onSuccess, this.onError)
            })
        })
    }

    componentWillUnmount() {
        if (this.state.connection) this.state.connection.ws.close()
    }


    lodadContracts = (recomendedContractts) => {
        console.log("recomendedContractts: ", recomendedContractts);

    }

    render() {
        return (
            <div>
                {this.serial_number ?
                    this.renderContent()
                    : {}}
            </div>
        )
    }
}
