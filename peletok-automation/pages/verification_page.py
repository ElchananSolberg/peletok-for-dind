from selenium.webdriver.common.by import By


class VerificationPage:
    pre_number = (By.ID, 'InputTypeNumberInputTypePhonePrefixtelephoneNumberInput')
    number = (By.ID, 'InputTypeNumberInputTypePhoneNumbertelephoneNumberInput')
    validation_pre_number = (By.ID, "InputTypeNumberInputTypePhonePrefixnumberValidationInput")
    validation_number = (By.ID, "InputTypeNumberInputTypePhoneNumbernumberValidationInput")
    user_number = (By.ID, "InputTypeNumbercontactNumberIdInput")
    alert_phone_number = (By.CLASS_NAME, 'modal-body')
    alert_confirm_btn = (By.CLASS_NAME, 'm-2')
    alert_cancel_btn = (By.XPATH, "//button[contains(text(),'ביטול')]")
    alert_annul_btn = (By.CLASS_NAME, 'btn')
    alert_favorite_tag = (By.CLASS_NAME, 'modal-body')
    img_logo_pelephone = (By.XPATH, '//*[@class="d-flex mt-5 mt-sm-0 mr-auto"]')
    price_pelephone_easy_99 = (By.XPATH, '//*[@class="priceText" and contains(text(),99)]')
    price_bezek_beleoumi_014 = (By.XPATH, '//*[@class="priceFrame px-0 mr-auto col"]')
    img_logo_kvish_6 = (By.CLASS_NAME, "routesChargeImage")
    virtual_charge_button = (By.ID, 'chargeButton')
    success_msg = (By.CLASS_NAME, 'messageGreenColor')

    carmel_tunnels_input_id = (By.ID, 'InputTypeNumberIdInput')
    carmel_tunnels_input_last_six_digits_on_invoice_account = (By.ID, 'InputTypeNumberinvoiceInput')
    carmel_tunnels_input_invoice_account = (By.ID, 'InputTypeNumberRecieptAmountInput')
    carmel_tunnels_input_number = (By.ID, 'InputTypeNumberInputTypePhoneNumberPhoneNumberInput')
    carmel_tunnels_input_pre_number = (By.ID, 'InputTypeNumberInputTypePhonePrefixPhoneNumberInput')
    carmel_tunnels_button_charge = (By.ID, 'תשלום מנהרות כרמלButton')
    carmel_tunnels_success_msg = (By.CSS_SELECTOR, 'div.myAlert.alert.alert-success.fade.show')
    carmel_tunnels_commission_a_payment = (By.CSS_SELECTOR, "div.mx-1.my-2.commissionNotesRow.row > div")
    carmel_tunnels_total_payment = (By.ID, "InputTypeNumberTotalAmountInput")
    manual_charge_button = (By.ID, 'manualCardChargeButton')
    print_btn = (By.XPATH, '//*[@class="btn btn-primary" and contains(text(),הדפס)]')
    tehinatChashmal_contractNumber = (By.ID, "InputTypeNumbercontractNumberInput")
    tehinatChashmal_AmountMoneyToCharge = (By.ID, "InputTypeNumbermoneyToCharge")
    tehinatChashmal_totalPayment = (By.ID, "InputTypeNumberTotalPayment")
    tehinatChashmal_PhonePreNumber = (By.ID, "InputTypeNumberInputTypePhonePrefixsupplierTelnumTitle")
    tehinatChashmal_PhoneSenvenNumber = (By.ID, "InputTypeNumberInputTypePhonePrefixsupplierTelnumTitle")
    tehinatChashmal_ChargeButton = (By.ID, "electricityChargeButton")
    tehinatChashmal_success_msg = (By.CLASS_NAME, "detailBorder")
    tehinatChashmal_cancelButton = (By.ID, "cancelButton")
    back_botton = (By.ID, "backToPreviousScreenBtn")
    back_button_wait_before_the_close = (By.CSS_SELECTOR, 'div:nth-child(1) > h3')
    close_alert_detail_btn = (By.CLASS_NAME, 'close')
    price_on_the_success_msg = (By.XPATH, '//*[@role="document"]//div[@class="priceText" and contains(text(),"{}")]')
    print_btn = (By.XPATH, '//button[@class="btn btn-primary"]')
    alert_success_kvish_6 = (By.CLASS_NAME, "myAlert")
    close_btn_in_charge_success_alert = (By.ID, "cancelButton")
    login_error_msg = (By.CLASS_NAME, "invalid-feedback")
    user_area_drop = (By.ID, "customerBtn")
    btn_log_out = (By.ID, "logOutDrop")
