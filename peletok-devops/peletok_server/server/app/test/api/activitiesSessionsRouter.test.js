const request = require('supertest');
const sequelize = require('../../setupSequelize')
const bodyParser = require('body-parser');
var express = require('express');
const models = require('../../index')
const {activitySessionRouter} = require('../../api/activitySessionRouter')

app = express();
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(bodyParser.urlencoded({ extended: false })); 
app.use('/activity_session/', activitySessionRouter);
let business;
describe('ActivitySessionRouter', () => {
     beforeAll(async () => {

        await sequelize.sync({
             force: true,
             logging: false
         })

        const user = await models.user.model.create({
            first_name: 'jon',
            last_name: 'due'
        })
        const supplier = await models.supplier.model.create({
            name: 'peletok'
            
        }) 

        business = await models.business.model.create({
            name: 'peletok',
            num_device_allowed: 1,
            num_ip_allowed: 1

        })

        const ips = await models.userIp.model.bulkCreate([
            {
                ip_address: '127.0.0.1',
                business_id: business.id,
            },
            {
                ip_address: '127.0.0.2',
                business_id: business.id,
            },
            {
                ip_address: '127.0.0.3',
                business_id: business.id,
            }
        ])
        const tokens = await models.businessToken.model.bulkCreate([
            {
                token: '123454',
                business_id: business.id,
            },
            {
                token: '67894',
                business_id: business.id,
            },
            {
                token: '875544',
                business_id: business.id,
            }
        ])

        const sessions = await models.activitySession.model.bulkCreate([
            {
                pasport_session: '123454',
                user_id: user.id,
                business_ip_id: 1,
                business_token_id: 1,
            },
            {
                pasport_session: '67894',
                user_id: user.id,
                business_ip_id: 2,
                business_token_id: 2,
            },
            {
                pasport_session: '875544',
                user_id: user.id,
                business_ip_id: 3,
                business_token_id: 3,
            }
        ])

        
     } )

     describe('GET /activity_session/:business_id', () => {
         
        it('should return status 200',  () => {
            return request(app).get('/activity_session/1').then((response) => {
                expect(response.statusCode).toBe(200);
            });
        })
        
        it('should return objects', ()=>{
            return request(app).get('/activity_session/1').then((response) => {
                expect(response.body).toMatchObject(
                        { 
                            business: {num_device_allowed: 1, num_ip_allowed: 1},
                            ips:
                           [ { id: 1, ip_address: '127.0.0.1', names: 'jon due', status: true },
                             { id: 2, ip_address: '127.0.0.2', names: 'jon due', status: true },
                             { id: 3, ip_address: '127.0.0.3', names: 'jon due', status: true } ],
                          tokens:
                           [ { id: 1, token: '123454', names: 'jon due', status: true },
                             { id: 2, token: '67894', names: 'jon due', status: true },
                             { id: 3, token: '875544', names: 'jon due', status: true } ] 
                         });
            });
        })

        it('should return objects if some sessions doesnt exist', async ()=>{
            await models.activitySession.model.destroy({where: {business_ip_id:1}})
            return request(app).get('/activity_session/1').then((response) => {
                expect(response.body).toMatchObject(
                        { 
                            business: {num_device_allowed: 1, num_ip_allowed: 1},
                            ips:
                           [ { id: 1, ip_address: '127.0.0.1', names: '', status: false },
                             { id: 2, ip_address: '127.0.0.2', names: 'jon due', status: true },
                             { id: 3, ip_address: '127.0.0.3', names: 'jon due', status: true } ],
                          tokens:
                           [ { id: 1, token: '123454', names: '', status: false },
                             { id: 2, token: '67894', names: 'jon due', status: true },
                             { id: 3, token: '875544', names: 'jon due', status: true } ] 
                         });
            });
        })
    } )

     describe('POST /activity_session/token', () => {
         
        it('should return status 200',  () => {
            return request(app)
            .post('/activity_session/token' )
            .send({business_id: business.id, num_device_allowed: 3})
            .then((response) => {
                expect(response.statusCode).toBe(200);
            });
        })

        it('should change business  token constrains',  () => {
            return request(app)
            .post('/activity_session/token')
            .send({business_id: business.id, num_device_allowed: 3})
            .then(async (response) => {
                let b = await models.business.model.findOne({
                    where: {id: business.id}
                })

                expect(b.num_device_allowed).toBe(3);
            });
        })
    })

     describe('POST /activity_session/ip', () => {
         
        it('should return status 200',  () => {
            return request(app)
            .post('/activity_session/ip' )
            .send({business_id: business.id, num_ip_allowed: 3})
            .then((response) => {
                expect(response.statusCode).toBe(200);
            });
        })

        it('should change business  ip constrains',  () => {
            return request(app)
            .post('/activity_session/ip')
            .send({business_id: business.id, num_ip_allowed: 3})
            .then(async (response) => {
                let b = await models.business.model.findOne({
                    where: {id: business.id}
                })

                expect(b.num_ip_allowed).toBe(3);
            });
        })
    })

     describe('POST /activity_session/ip/cancel', () => {
         
        it('should return status 200',  () => {
            return request(app)
            .post('/activity_session/ip/cancel' )
            .send({business_id: business.id})
            .then((response) => {
                expect(response.statusCode).toBe(200);
            });
        })

        it('should change business  ip constrains',  () => {
            return request(app)
            .post('/activity_session/ip/cancel')
            .send({business_id: business.id})
            .then(async (response) => {
                let b = await models.business.model.findOne({
                    where: {id: business.id}
                })

                expect(b.num_ip_allowed).toBe(null);
            });
        })
    })

     describe('POST /activity_session/token/cancel', () => {
         
        it('should return status 200',  () => {
            return request(app)
            .post('/activity_session/token/cancel' )
            .send({business_id: business.id})
            .then((response) => {
                expect(response.statusCode).toBe(200);
            });
        })

        it('should change business  token constrains',  () => {
            return request(app)
            .post('/activity_session/token/cancel')
            .send({business_id: business.id})
            .then(async (response) => {
                let b = await models.business.model.findOne({
                    where: {id: business.id}
                })

                expect(b.num_device_allowed).toBe(null);
            });
        })
    })

    
     describe('DELETE /activity_session/token/:token_id', () => {
         
        it('should return status 200',  () => {
            return request(app)
            .delete('/activity_session/token/1')
            .then((response) => {
                expect(response.statusCode).toBe(200);
            });
        })

        it('should delete token',  () => {
            return request(app)
            .delete('/activity_session/token/1')
            .then(async (response) => {
                let token = await models.userToken.model.findOne({
                    where: {id: 1}
                })
                expect(token).toBe(null);
            });
        })

        
    })

     describe('DELETE /activity_session/ip/:ip_id', () => {
         
        it('should return status 200',  () => {
            return request(app)
            .delete('/activity_session/ip/1')
            .then((response) => {
                expect(response.statusCode).toBe(200);
            });
        })

        it('should delete ip',  () => {
            return request(app)
            .delete('/activity_session/ip/1')
            .then(async (response) => {
                let ip = await models.userIp.model.findOne({
                    where: {id: 1}
                })
                expect(ip).toBe(null);
            });
        })

        
    })
})
