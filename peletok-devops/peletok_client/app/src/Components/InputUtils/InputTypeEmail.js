import React, { Component } from 'react';
import './InputUtils.css';
import { Input, FormFeedback, Row, Col } from 'reactstrap';
import { Tooltip } from './Tooltip';
import { LanguageManager as LM } from '../LanguageManager/Language';
import Asterisk from '../Ui/Asterisk';

/**
 * InputTypeEmail component lets to input the email. It checks if it is valid
 * it can render the title above the input (if this.props.title is not an empty string).
 * it can render the tooltip to the side of the title (if this.props.tooltip is not an empty string).
 * It validates the value to be a real correct email.
 * It validates the value was entered (if this.props.required is true).
 * It validates the value is valid to the function sent in  this.props.validationFunction;
 * if there are any validation issues the input will get a prop "invalid" and will change a border color to red;
 * if there are any validation issues bellow the input will be the error message 
 * It can add outer onChange function to checkbox onChange.
 * It can change a view to disabled view due to this.props.disabled.
 */
class InputTypeEmail extends Component {
    constructor(props) {
        super(props);

        this.state = {
            value: '',
            validationFuncs: [
                {
                    /**  Validates the value is valid to the function sent in  this.props.validationFunction */
                    f: this.props.validationFunction || (() => (true)),
                    msg: this.props.notValidMessage
                },
                {
                    f: this.innerValidationFunction,
                    msg: LM.getString("emailInnerValidationFunctionMsg") + ': email@example.com',
                },
                {
                    f: this.isRequiredValidation,
                    msg: LM.getString("isRequiredValidationFunctionMsg"),
                }
            ],
            errorMessage: '',
        }
    }

    /** can add outer onChange function to checkbox onChange. */
    emailChange = (e) => {
        e.persist();
        this.setState({ value: e.target.value },
            () => {
                if (this.props.onChange) {
                    this.props.onChange(e);
                }
            }
        );
    }

    /** summarizing validation function
     * is running 2 times: when input is loosing focus and when getValue function is running (by programmer-user) 
     * */
    runValidation = () => {
        let summarizingValidationResult = (!this.props.required && !this.state.value) || this.state.validationFuncs.every((validation) => validation.f(this.state.value));
        if (!summarizingValidationResult) {
            let messages = [];
            this.state.validationFuncs.forEach((validation) => {
                if (!validation.f(this.state.value)) {
                    messages.push(validation.msg);
                }
            })
            this.setState(({ errorMessage: messages.join(`\n`) }));
        } else {
            this.setState(({ errorMessage: '' }));
        }
        return summarizingValidationResult;
    }

    getValue = () => {
        let summarizingValidationResult = this.runValidation();
        return { valid: summarizingValidationResult, value: this.state.value };
    }

    setValue = (newValue) => {
        if (this.innerValidationFunction(newValue)) {
            this.setState({ value: newValue });
            return true;
        }
        return false;
    }

    /** If this.props.required is true validates the value was entered. */
    isRequiredValidation = (value) => {
        if (!value && this.props.required) {
            return false;
        }
        return true;
    }

    /** Validates the value to be a real correct email. */
    innerValidationFunction = (value) => {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(value);
    }

    render() {
        let disabled = this.props.disabled;
        let placeholder = this.props.placeholder;
        let id = this.props.id.replace(/\s+/g, '');
        let title = this.props.title;
        let tooltipText = this.props.tooltip;

        return (
            <>
                <Row>
                    <Col className={"d-flex justify-content-between line-height-1 " + (title ? 'py-1' : '')}>
                        {title ?
                            <div className="fs-17 white-space">
                                {title}
                                <Asterisk show={this.props.required} />
                            </div>
                            : <><Asterisk show={this.props.required} /></>
                        }
                        {tooltipText ?
                            <Tooltip tooltipText={tooltipText} id={id} />
                            : <></>
                        }
                    </Col>
                </Row>
                <Row>
                    <Col>
                        {/** if there are any validation issues the input will get a prop "invalid" and will change a border color to red */}
                        <Input type="email" autoComplete="off"
                            invalid={this.state.errorMessage.length !== 0}
                            placeholder={placeholder}
                            value={this.state.value} id={`InputTypeEmail${id}`}
                            disabled={disabled}
                            onChange={this.emailChange} onBlur={this.runValidation}
                            className={(LM.getDirection() === "rtl" ? "rtl" : "")}
                        />
                        {/** if there are any validation issues bellow the input will be the error message */}
                        <FormFeedback className='ws-pre'>{this.state.errorMessage}</FormFeedback>
                    </Col>
                </Row>
            </>

        );
    }
}

export default InputTypeEmail;