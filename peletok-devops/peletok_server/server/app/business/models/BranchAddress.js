const DataTypes = require('sequelize').DataTypes;
const BaseModel = require('../../BaseModel');

/**
 * BranchAddress model
 */
class BranchAddress extends BaseModel{

    /**
     * configs fields and options
     */
    config() {
        this.prefix = "BSN";
    }

    /**
     * creates associate for Branch model
     * @param models: objects in format {modelName: model}
     */
    associate(models){
        this.model.belongsTo(models.branch.model, {as: 'Branch'});
        this.model.belongsTo(models.address.model, {as: 'Address'});
    }
}

module.exports = new BranchAddress();
