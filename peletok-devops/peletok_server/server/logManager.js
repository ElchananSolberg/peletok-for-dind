/***
 * Returns the number as string with padding
 * (for example: num=1 result:01, num=10 result: 10)
 * @param num number to padding
 * @returns string string with padding
 */
function padNum(num) {
    return num < 10 ? "0" + num : num;
}


/***
 * Returns now date as format year-month-day  hour:minute:second
 * @returns string date
 */
function getDate() {
    const d = new Date();
    return d.getFullYear() + '-' + d.getMonth() + 1 + '-' + padNum(d.getDate()) +
        '  ' + padNum(d.getHours()) + ':' + padNum(d.getMinutes()) + ':' + padNum(d.getSeconds())
}

/***
 * Wrapper of log message (add time to message)
 * @param message string to write
 * @param logManage manager of logging (default: console)
 */
function log(message, logManage = console) {
    logManage.log(getDate() + " :\t" + message);
}

module.exports.log = log;