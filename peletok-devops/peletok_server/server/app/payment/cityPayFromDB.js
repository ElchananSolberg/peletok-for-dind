const cityPayModel = require("../index").city.model;
const cityPaymentTypeSiteModel = require("../index").cityPaymentTypeSite.model;
const cityPaymentType = require("../index").cityPaymentType.model;

/**
 * Returns all citys with payment types
 * @returns {Promise<*>}
 */
async function getCityPay() {
    return cityPayModel.findAll({
        attributes: ['name_HE', "name_EN", "name_AR", ["id", "city_id"]],
        include: [
            {
                model: cityPaymentTypeSiteModel,
                as: 'CityPaymentTypeSite',
                attributes: ['id'],
                include: {
                    model: cityPaymentType,
                    as: 'CityPaymentType',
                    attributes: ["pay_name"],
                }
            },

        ]
    })
}

module.exports.getCityPay = getCityPay;