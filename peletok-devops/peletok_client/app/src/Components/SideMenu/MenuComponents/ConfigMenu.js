import React, {Component} from 'react';
import {LanguageManager as LM} from "../../LanguageManager/Language";
import {MenuLargeButton} from "../Utils/MenuLargeButton";
import {MenuListButton} from "../Utils/MenuListButton";
import IconCharge from "../../../Assets/Icons/MenuIcons/charge.svg";
// TODO get updated SVGs


export class ConfigMenu extends Component {
    render() {
        let mainConfigList = [
            {name: LM.getString("general"), id: "general"},
            {name: LM.getString("providerPermissions"), id: "providerPermissions"},
            {name: LM.getString("profitPercentages"), id: "productAuth/porfit"},
            {name: LM.getString("commissions"), id: "productAuth/commissions"},
            {name: LM.getString("profitCopy"), id: "profitCopy"},
            {name: LM.getString("supplierToSupplierPerm"), id: "supplierToSupplierPerm"},
            {name: LM.getString("copyResellerProfile"), id: "copyResellerProfile"},
            {name: LM.getString("paymentInfo"), id: "paymentInfo"},
            {name: LM.getString("invoiceUpload"), id: "invoiceUpload"},
            {name: LM.getString("bannerManagement"), id: "bannerManagement"},
            {name: LM.getString("tagsManagement"), id: "tagsManagement"},
        ];
        let administrationConfigList = [
            // {name: LM.getString("general"), id: "general"},
            {name: LM.getString("providerPermissions"), id: "providerPermissions"},
        ];
        return (
            <div className="rounded" style={{overflow: "hidden"}}>
                <MenuLargeButton
                    // list={mainConfigList}
                    path={`${this.props.match.url}/main`}
                    icon={IconCharge}
                    text={LM.getString("configuration")}
                    location={this.props.location}
                    toggle={this.props.toggle}
                />
                {mainConfigList.map((obj) => <MenuListButton key={obj.id}
                            id={obj.id}
                            toggle={this.props.toggle}
                            {...obj}
                            path={`${this.props.match.url}/main`}
                            location={this.props.location} />)}
                
            </div>
        )
    }
}
