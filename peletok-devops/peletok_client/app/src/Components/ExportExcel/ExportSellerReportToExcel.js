import React, {Component} from 'react';
import ReactExport from "react-export-excel";
import {LanguageManager as LM} from '../LanguageManager/Language';
import './ExportExcel.css'
import {Loader} from "../Loader/Loader";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;


/**
 * export to the excel for seller reports (it uses in loadData method that passed in props)
 */

export class ExportSellerReportToExcel extends Component {

    constructor(props) {
        super(props);
        this.state = {dataSet: [], columnNames: {}};
    }

    customLoadData = () => {
        Loader.show();
        this.props.loadData().then(({columns, data}) => {
            this.setState({dataSet: data, columnNames: columns}, () => {
                Loader.hide();
                this.hiddenButton.click();
            });
        })
    };

    render() {
        let col = [];
        Object.keys(this.state.columnNames).map(k => {
            col.push(<ExcelColumn label={this.state.columnNames[k]} value={k}/>)
        });
        return (
            <div>
                <button onClick={this.customLoadData}
                        className='exportExcelButton'>{LM.getString("ExportToExcel")}>
                </button>
                <ExcelFile filename={this.props.fileName}
                           element={<button hidden={true} ref={(r) => this.hiddenButton = r}/>}>
                    <ExcelSheet data={this.state.dataSet} name={this.props.fileName}>
                        {col}
                    </ExcelSheet>
                </ExcelFile>

            </div>)
    }

}