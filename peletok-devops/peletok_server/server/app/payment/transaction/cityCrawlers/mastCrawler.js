const axios = require('axios').default;
const jsdom = require("jsdom");
const logs = require('../../../logger')
const Info = require('../../../resInfo')
const qs = require("querystring");
const PaymentBasic = require('../../paymentBasic');
const LM = require('../../../config/LanguageManager').LM
axios.defaults.withCredentials = true
const { JSDOM } = jsdom;

const urlBasic = "https://www.pay24.co.il/payDesign"
const urlPay = "PayNow/PayNowClick";
const urlGetsum = "SearchParking/SearchParking_click";
const urlConf = "Invoice/Invoice"

/***
 * class to  pay  bills  from  www.mast.co.il
 */

class MastCrawler extends PaymentBasic {
  constructor(homeUrl, billNum, idClient, price, req) {
    super()
    this.homeUrl = homeUrl
    this.billNum = billNum
    this.idClient = idClient
    this.price = price
    this.info = new Info()
    this.req=req
  }



  /***
   * send  api request  to https://www.pay24.co.il/payDesign to  get  str of Encryption
   * return   str of Encryption
   * 
   *  Encryption of  site  mast=> https://www.pay24.co.il/payDesign/(S(pmhmsmzngbafipegm15qw1n2))/SearchParking/SearchParking_click
   * return  /(S(pmhmsmzngbafipegm15qw1n2))
   */
  static async getImage() {
    const res = await axios.get(urlBasic);
    return res.request["_redirectable"]["_options"]["href"].slice(0, 64)
  }

  /***
   *  Step 1 for  getPrice  &  payment 
   */
  async fillDetails() {

    const image = await MastCrawler.getImage();
    const payOrigin = "&payOrigin=MAST_Web"
    const authority = this.homeUrl.slice(23)
    const authorityMast = authority.slice(-11, -8)
    const url = "SearchPay/SearchPay?ReshutMast=" + authorityMast + payOrigin
    const logUrl = image + url
    const getSum = image + urlGetsum

    const bodyPost = {
      f_ReportNumber: this.billNum,
      f_LicenseNumber: this.idClient
    }

    await axios.get(logUrl);
    let res = await axios.post(getSum, qs.stringify(bodyPost)
    )
      .catch(e => {
        this.info.setErrMessage(LM.getErrorBasic(), " err_connection_site" + e.toString())
        this.info.error.err_connection_site_step1 = true
        return this.info
      })

    return {
      "htmlString": res.data,
      "image": image,
    }

  }

  /***
   * check  what amount of  bill
      * return price of  bill 
      */
  async getBillPrice() {
    if ((await this.isDemoTransactionUser())) {
      this.info.send = { "success": 150 }
      this.info.price="150"
      return  this.info
    }
    else {

    const postPrice = await this.fillDetails()
    const htmlString = postPrice["htmlString"]


    const dom = new JSDOM(htmlString);

    const resPrice = dom.window.document.getElementById('f_pay_now_stub_price')

    if (resPrice) {
      this.info.price = resPrice.value
      this.info.send = { "success": this.info.price }
    }
    else {
      this.info.setErrMessage(LM.getString("ErrorNumBill/ID"), "NumberBill or  NumberID is  null")

    }

    return this.info

  }
  }



  /***
   * check  what price of  bill/product for transaction 
   * 
   */
  getPrice() {
    this.info.price = this.price
    return this.info
  }

  /***
    * Pay  to bill  of  www.mast.co.il   
    @returns number  of   confirmation
              */
  async  pay() {

    if ((await this.isDemoTransactionUser())) {
      return this.demoRespons()
    }
    else {
      const post_price = await this.fillDetails()
      const urlPayment = post_price["image"] + urlPay
      const urlConfirm = post_price["image"] + urlConf




      /***
          *TODO change  f_pay_account, f_pay_cvv &...
          */

      const postBody = {
        f_pay_now_name: " peletok",
        f_payer_now_idnum: 1,
        f_pay_now_stub_price: this.price,
        f_pay_now_stub_code1: "",
        f_pay_now_stub_price1: this.price,
        f_pay_now_stub_code2: "",
        f_pay_now_stub_price2: "",
        Detail2: "",
        f_pay_now_stub_code3: "",
        f_pay_now_stub_price3: "",
        Detail3: "",
        SchumTotal: this.price,
        f_pay_account: "5326101241968611",
        f_pay_cvv: "435",
        dd_year: " 26",
        dd_month: " 05",
        f_payer_details: " gut",
        f_payer_idnum: "304255607",
        f_payer_phone: "0548408908",
        f_payer_email: ""
      }


      await axios.post(urlPayment, qs.stringify(postBody))
        .catch(e => {
          this.info.setErrMessage("err_connection_site" + e.toString())
          this.info.error.err_connection_site = true
          this.info.send = { "success": LM.getString("errPayment") }
          return this.info
        })

      const res = await axios.get(urlConfirm)
        .catch(e => {
          this.info.setErrMessage("err_connection_site" + e.toString())
          this.info.error.err_connection_site = true
          this.info.send = { "success": LM.getString("errPayment") }
          return this.info
        })

      const dom = new JSDOM(res.data);
      const confirmation = dom.window.document.getElementsByClassName("p-id")[0].textContent

      if (!confirmation) {
        this.info.err_in_payment = true
        this.info.send = { "success": LM.getString("errPayment") }
        return this.info
      }
      logs.info("confirmation", confirmation)
      this.info.isPaid = true
      this.info.send = { "success": "success" }
      this.info.confirmation = confirmation
      return this.info

    }
  }

}



module.exports = MastCrawler
