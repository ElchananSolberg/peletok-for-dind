import React, { Component } from 'react';
import { Container, Row, Col, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import './ContractInfo.css'

import { LanguageManager as LM } from '../LanguageManager/Language';
import { Notifications } from '../Notifications/Notifications';
import { Loader } from '../Loader/Loader';
import { Confirm } from '../Confirm/Confirm';

import {
    putSeller,
    getBalanceApprovals, putUpdateBalance,
    postActivitySessionIP, postActivitySessionToken, postActivitySessionIPCancel, postActivitySessionTokenCancel,
    getDoseModeFieldValueExsist
} from '../DataProvider/DataProvider';

import InputTypeSearchList from '../InputUtils/InputTypeSearchList';
import InputTypeNumber from '../InputUtils/InputTypeNumber';
import InputTypeText from '../InputUtils/InputTypeText';
import InputTypeCheckBox from '../InputUtils/InputTypeCheckBox';
import InputTypeSimpleSelect from '../InputUtils/InputTypeSimpleSelect';


import UserSecurityDetailsTable from '../selectUser/UserSecurityDetailsTable';
import { BusinessStatusEnum } from '../../enums/'
import { SelectedSeller } from '../SelectedSeller/SelectedSeller';


/**
 * A component that will manage the Seller contracts
 */
export class ContractInfo extends Component {

    constructor(props) {
        super(props);
        this.state = {
            /** working variable that will contain a selected distributor object */
            selectedDistributor: {},
            /** working variable that will contain a selected seller object */
            selectedSeller: {},
            /** working variable that indicates if a user is on the stage of viewing a Update Balance Modal window */
            updateBalanceModal: false,
            authorities: [],
            clientNumberValidationResult: true,
            delekStore: false,
        };
        /** working variable that will contain an object of all the input fields (on the Contract Info screen), which are getting data from the selectedSeller object (from the server ), and sending data to the server */
        this.fields = {};
        this.fields_pos_number = [];
        this.totalBalance = {};

    }

    /** "Delek User" checkbox onChange handler */
    delekUserChanged = (e) => {
        if (e.target.checked){
            Notifications.show(LM.getString('attentionAfterSavingAsDelekUser'), 'warning');
            this.setState({delekStore: true})
        } else {
            this.setState({delekStore: false})
        }
    }
    pointsUserChanged = (e) => {
        this.setState({
            selectedSeller: Object.assign(this.state.selectedSeller, {use_point: !this.state.selectedSeller.use_point})},() => {
                //this.fields.points.setValue(this.state.selectedSeller.points)
        })
    }

    /** toggles Update Balance modal window */
    toggleUpdateBalanceModal = () => {
        this.setState({ updateBalanceModal: !this.state.updateBalanceModal });
    }
    updateBalance = () => {
        this.toggleUpdateBalanceModal();
    }

    confirmBalanceUpdatingHandler = () => {
        if (this.totalReceived.getValue().valid && this.receiptNumber.getValue().valid && this.authority.getValue().valid) {
            let data = {
                sum: this.totalReceived.getValue().value,
                invoice_number: this.receiptNumber.getValue().value,
                approver_id: this.authority.getValue().value,
                note: this.note.getValue().value,
                business_id: this.state.selectedSeller.id
            }
            Loader.show();
            putUpdateBalance(data).then(
                (res) => {
                    Loader.hide();
                    Notifications.show(LM.getString("balance2") + " " + LM.getString("updatedSuccessfully"), 'success');
                    let oldBalance = this.fields.balance.getValue().value
                    this.setState({
                        selectedSeller: Object.assign(this.state.selectedSeller, { balance: parseFloat(oldBalance) + parseFloat(data.sum) })
                    })
                    this.toggleUpdateBalanceModal();
                    SelectedSeller.refreshSellers.next()
                    this.fieldsLoad();
                }).catch(
                    (err) => {
                        Loader.hide();
                        Notifications.show(err.response.data.error, 'danger')
                    }
                )
        }
        Notifications.show(LM.getString("completeFields"))
    }
    clientNumberOuterValidationFunction = (value) => {
        if (this.state.clientNumberValidationResult) {
            return true
        }
        return false
    }
    validateClientNumber = async () => {
        let clientNumber = this.fields.business_identifier.getValue().value;
        await getDoseModeFieldValueExsist('business', 'business_identifier', clientNumber).then(
            (res) => {
                if (!res) {
                    this.setState({ clientNumberValidationResult: true });
                }
                else {
                    this.setState({ clientNumberValidationResult: false });
                }
            }
        ).catch(
            (err) => {
                Notifications.show(err.response.data.error, 'danger');
            }
        )
    }
    clientNumberChanged = async () => {
        let newClientNumber = this.fields.business_identifier.getValue().value;
        if (  this.state.selectedSeller.business_identifier !== newClientNumber ) {
            await this.validateClientNumber();
            this.fields.business_identifier.runValidation();
        }
    }

    saveClicked = () => {
        // let pos_number = Object.values(this.fields_pos_number).filter(v => v);
        if (SelectedSeller.currentSeller.getValue().id) {
            let data = {};
            let requestValid = true;
            Object.keys(this.fields).forEach((key) => {
                if (key === "points" && !this.state.selectedSeller.use_point) {
                    return
                }
                let value = this.fields[key].getValue();
                if (!value.valid) requestValid = false;
                else {
                    if (value.value === "") data[key] = null;
                    else data[key] = value.value;
                }
            });
            if (!requestValid) {
                Notifications.show(LM.getString("notValidField", 'danger'));
                return;
            }
            Loader.show();
            putSeller(this.state.selectedSeller.id, data).then(
                res => {
                    Loader.hide();
                    Notifications.show(LM.getString("successSellerUpdating") + this.state.selectedSeller.name, 'success');
                    SelectedSeller.refreshSellers.next()
                    this.setState({ selectedSeller: Object.assign(this.state.selectedSeller, Object.keys(data).reduce((n, key) => Object.assign(n, this.state.selectedSeller.hasOwnProperty(key) ? { [key]: data[key] } : {}), {})) })
                    this.fieldsLoad()
                }
            ).catch(
                err => {
                    Loader.hide();
                    Notifications.show(err.response.data.error, 'danger');
                }
            );
        }
        else Notifications.show(LM.getString("sellerNotSelectedError"), 'warning');
    };
    freezeClicked = () => {
        let id = this.state.selectedSeller.id;
        let data = this.state.selectedSeller.status === BusinessStatusEnum.FROZEN ? { status: BusinessStatusEnum.ACTIVE } : { status: BusinessStatusEnum.FROZEN };
        Loader.show();
        putSeller(id, data).then(
            res => {
                Loader.hide();
                Notifications.show(this.state.selectedSeller.name + ' ' + LM.getString('updatedSuccessfully'), 'success');
                this.setState({ selectedSeller: Object.assign(this.state.selectedSeller, { status: data.status }) })
            }
        ).catch(
            err => {
                Loader.hide();
                Notifications.show(LM.getString('freezeFailed'), 'danger')
            }
        );
    };
    lockClicked = () => {
        let id = this.state.selectedSeller.id;
        let data = this.state.selectedSeller.status === BusinessStatusEnum.LOCKED ? { status: BusinessStatusEnum.ACTIVE } : { status: BusinessStatusEnum.LOCKED };
        Loader.show();
        putSeller(id, data).then(
            res => {
                Loader.hide();
                Notifications.show(this.state.selectedSeller.name + ' ' + LM.getString('updatedSuccessfully'), 'success');
                this.setState({ selectedSeller: Object.assign(this.state.selectedSeller, { status: data.status }) })
            }).catch(
                err => {
                    Loader.hide();
                    Notifications.show(LM.getString('lockFailed'), 'danger')
                }
            );
    };

    /** TODO: ADD functionality 
     * Function that loads a data from the selected seller object into the this.fields object. */
    fieldsLoad = () => {
        Object.keys(this.fields).forEach((key) => {
            if (key === "points" && !this.state.selectedSeller.use_point) {
                return
            }
            if (this.state.selectedSeller[key] !== null && this.state.selectedSeller[key] !== undefined) {
                this.fields[key].setValue(this.state.selectedSeller[key]);
            }
            else {
                if (this.fields[key].getValue().value === true) {
                    this.fields[key].setValue(false);
                }
                else this.fields[key].setValue("");
            }
            
        });
        let balance = this.state.selectedSeller['balance'] || 0;
        let frame = this.state.selectedSeller['frame'] || 0;
        let temp_frame = this.state.selectedSeller['temp_frame'] || 0;
        this.totalBalance.setValue(parseFloat(balance) + parseFloat(frame) + parseFloat(temp_frame));
        if(this.state.selectedSeller.is_delek){
            this.setState({delekStore: true})
        } else this.setState({delekStore: false})

    };

    /** Function that clears a data from the selected seller object and from the this.fields object. */
    clearFields = () => { 
            Object.keys(this.fields).forEach((key) => {
                if (key === "points" && !this.state.selectedSeller.use_point) {
                    return
                }
                if (this.fields[key].getValue().value === true) {
                    this.fields[key].setValue(false)
                }
                else this.fields[key].setValue("");
                
            })
            SelectedSeller.clearFields.next()    
    }

    componentDidMount() {
        getBalanceApprovals().then(
            (res) => {
                this.setState({ authorities: res })
                let approvalsArr = [];
                res.map(obj => approvalsArr.push({ label: obj.name, value: obj.id })
                )
                this.setState({ authorities: approvalsArr })
            }
        )

        SelectedSeller.setHeader.next(LM.getString('contractInfo'));
        if (SelectedSeller.newSellerState) SelectedSeller.createSellerClicked.next();
        if (SelectedSeller.newDistributorState) SelectedSeller.createDistributorClicked.next();
        if (SelectedSeller.currentSeller.getValue().id) this.setState({ selectedSeller: SelectedSeller.currentSeller.getValue() }, () => {
            this.fieldsLoad()
        });
        SelectedSeller.currentSeller.subscribe(x => {
            if(x.id){
                this.setState({ selectedSeller: x }, () => {
                    this.fieldsLoad()
                })
            }
        });
        SelectedSeller.currentDistributor.subscribe(x => {
            if(x.id){
                this.setState({ selectedDistributor: x }, () => {
                    if (SelectedSeller.currentSeller.getValue().id) {
                        this.clearFields()
                    }
                })
            }
        });

    }

    render() {
        /** "Balance Updating" modal window Header text */
        const balanceUpdatingModalHeader = LM.getString('balanceUpdating');

        /** data for Cancel button */
        const cancelButtonText = LM.getString("cancel");

        /** data for Update Balance button */
        const updateBalanceButtonText = LM.getString("UpdateBalance");

        /** data for Total Received (InputTypeNumber  InputUtils component) */
        const totalReceivedProps = {
            title: LM.getString("totalReceived") + ":",
            id: "ContractInfo_TotalReceived",
            required: true,
            enableNegative: true
        };
        /** data for Receipt Number (InputTypeNumber InputUtils component) */
        const receiptNumberProps = {
            title: LM.getString("receiptNumber") + ":",
            id: "ContractInfo_ReceiptNumber",
            required: true,
        };
        /** data for "Earning Points" field (InputTypeNumber InputUtils component) */
        const earningPointsProps = {
            title: LM.getString("earningPoints") + ':',
            id: "ContractInfo_EarningPoints",
        };

        /** data for Note (InputTypeText  InputUtils component) */
        const noteProps = {
            multi: true,
            title: LM.getString("note") + ":",
            id: "ContractInfo_Note",
        };

        const clientNumberProps = {
            title: LM.getString("clientNumber"),
            id: "ClientNumber",
            validationFunction: this.clientNumberOuterValidationFunction,
            notValidMessage: LM.getString("clientNumber").split(':', 2)[0] + " " + LM.getString("clientNumber").split(':', 2)[1] + " " + LM.getString("isAlreadyExist"),
        };
        const paymentMethodProps = {
            title: LM.getString("paymentMethod"),
            options: LM.getString("paymentMethodOptions"),
            id: "PaymentMethod",
        };
        const authority = {
            title: LM.getString("authority_2"),
            options: this.state.authorities,
            id: "Authority",
            required: true,
        };

        const commentsAndPermission = {
            multi: true,
            title: LM.getString("commentsAndPermission"),
            id: "CommentsAndPermission",
        };

        const sundayProps = {
            lableText: LM.getString("alef"),
            title: LM.getString("collectionDay"),
            id: "Alef",
        };
        const mondayProps = {
            lableText: LM.getString("bet"),
            id: "Bet",
        };
        const tuesdayProps = {
            lableText: LM.getString("gimel"),
            id: "Gimel",
        };
        const wednesdayProps = {
            lableText: LM.getString("dalet"),
            id: "Dalet",
        };
        const thursdayProps = {
            lableText: LM.getString("hey"),
            id: "Hey",
        };
        const fridayProps = {
            lableText: LM.getString("vav"),
            id: "Vav",
        };
        const monthProps = {
            lableText: LM.getString("month"),
            id: "Month",
        };
        const delekUser = {
            lableText: LM.getString("delekUser"),
            id: "DelekUser",
        };
        const storeNumber = {
            title: LM.getString("StoreNumber"),
            id: "StoreNumber",
        };
        const pointsUser = {
            lableText: LM.getString("pointsUser"),
            id: "PointsUser",
        };
        const balance = {
            disabled: true,
            title: LM.getString("balance"),
            id: "Balance",
        };
        const totalBalance = {
            disabled: true,
            title: LM.getString("totalBalance"),
            id: "totalBalance",
        };
        const frame = {
            title: LM.getString("frame"),
            id: "Frame",
        };
        const temporaryFrame = {
            title: LM.getString("temporaryFrame"),
            id: "TemporaryFrame",
        };

        return (
            <Container className="contractDetailsContainer">
                {SelectedSeller.currentSeller.getValue().id ?
                    <Container className="contractDetailsSettings">
                        <Row className='myRow'>
                            <Col sm='4'>
                                <InputTypeNumber
                                    {...clientNumberProps}
                                    required={true}
                                    onChange={this.clientNumberChanged}
                                    ref={(componentObj) => { this.fields.business_identifier = componentObj }}
                                />
                            </Col>
                            <Col sm='4'>
                                <InputTypeSearchList
                                    {...paymentMethodProps}
                                    ref={(componentObj) => { this.fields.payment_method = componentObj }}
                                />
                            </Col>
                        </Row>

                        <Row className="align-items-end justify-content-start">
                            <Col sm='auto'>
                                <InputTypeCheckBox
                                    {...sundayProps}
                                    ref={(componentObj) => { this.fields.sunday = componentObj }}
                                />
                            </Col>
                            <Col sm='auto'>
                                <InputTypeCheckBox
                                    {...mondayProps}
                                    ref={(componentObj) => { this.fields.monday = componentObj }}
                                />
                            </Col>
                            <Col sm='auto'>
                                <InputTypeCheckBox
                                    {...tuesdayProps}
                                    ref={(componentObj) => { this.fields.tuesday = componentObj }}
                                />
                            </Col>
                            <Col sm='auto'>
                                <InputTypeCheckBox
                                    {...wednesdayProps}
                                    ref={(componentObj) => { this.fields.wednesday = componentObj }}
                                />
                            </Col>
                            <Col sm='auto'>
                                <InputTypeCheckBox
                                    {...thursdayProps}
                                    ref={(componentObj) => { this.fields.thursday = componentObj }}
                                />
                            </Col>
                            <Col sm='auto'>
                                <InputTypeCheckBox
                                    {...fridayProps}
                                    ref={(componentObj) => { this.fields.friday = componentObj }}
                                />
                            </Col>
                            <Col sm='auto'>
                                <InputTypeCheckBox
                                    {...monthProps}
                                    ref={(componentObj) => { this.fields.monthly = componentObj }}
                                />
                            </Col>
                        </Row>

                        <Row className="justify-content-start myRow">
                            <Col sm='auto'>
                                <InputTypeCheckBox
                                    {...delekUser}
                                    onChange={this.delekUserChanged}
                                    ref={(componentObj) => { this.fields.is_delek = componentObj }}
                                />
                            </Col>
                            <Col sm='auto'>
                                <InputTypeCheckBox
                                    {...pointsUser}
                                    onChange={this.pointsUserChanged}
                                    ref={(componentObj) => { this.fields.use_point = componentObj }}
                                />
                            </Col>
                        </Row>
                        {this.state.delekStore &&
                            <>
                                <Row >
                                    <Col sm='12' >
                                        <hr className="businessLine" />
                                    </Col>
                                </Row>
                                <Row className="myRow">
                                    <Col sm='4'>
                                        <InputTypeText
                                            {...storeNumber}
                                            ref={(componentObj) => { this.fields.delek_store_identifier = componentObj }}
                                        />
                                    </Col>
                                </Row>
                                {/*<div className="fs-17 white-space">{LM.getString("POSNumber")}</div>*/}
                                {/*<Row>*/}
                                {/*    {[...Array(10).keys()].map(i=>{*/}
                                {/*    return (*/}
                                {/*        <Col className={'col-sm-20'}>*/}
                                {/*            <InputTypeText*/}
                                {/*                id={'pos_number' + i}*/}
                                {/*                onChange={((i) => {*/}
                                {/*                    return (e) => this.fields_pos_number[i] = e.target.value*/}
                                {/*                })(i)}*/}
                                {/*                ref={(obj)=>{*/}
                                {/*                    obj && obj.setValue('chaim');*/}
                                {/*                    this.fields_pos_number[i] = 'chaim'*/}
                                {/*                }}*/}
                                {/*            />*/}
                                {/*        </Col>)*/}
                                {/*    })}*/}
                                {/*</Row>*/}
                            </>
                        }
                        {
                            this.state.selectedSeller.use_point &&
                            <>
                                <Row >
                                    <Col sm='12' >
                                        <hr className="businessLine" />
                                    </Col>
                                </Row>
                                <Row className="myRow">
                                    <Col sm='4'>
                                        <InputTypeNumber
                                            {...earningPointsProps}
                                            ref={(componentObj) => { this.fields.points = componentObj }}
                                        />
                                    </Col>
                                </Row>
                            </>
                        }
                        <Row >
                            <Col sm='12' >
                                <hr className="businessLine" />
                            </Col>
                        </Row>
                        <Row className="myRow">
                            <Col sm='3'>
                                <InputTypeNumber
                                    {...frame}
                                    ref={(componentObj) => { this.fields.frame = componentObj }}
                                />
                            </Col>
                            <Col sm='3'>
                                <InputTypeNumber
                                    {...temporaryFrame}
                                    ref={(componentObj) => { this.fields.temp_frame = componentObj }}
                                />
                            </Col>
                            <Col sm='3'>
                                <InputTypeNumber
                                    {...balance}
                                    ref={(componentObj) => { this.fields.balance = componentObj }}
                                />
                            </Col>
                            <Col sm='auto'>
                                <Button
                                    className='contractDetailsButton'
                                    onClick={this.updateBalance}
                                    id='UpdateBalance'
                                >
                                    {updateBalanceButtonText}
                                </Button>
                            </Col>
                            <Modal isOpen={this.state.updateBalanceModal} toggle={this.toggleUpdateBalanceModal} >
                                <ModalHeader className="hashavshevetModalHeader" >{balanceUpdatingModalHeader}</ModalHeader>
                                <ModalBody className="hashavshevetModalBody" >
                                    <Row>
                                        <Col >
                                            <Row>
                                                <Col sm='6'>
                                                    <InputTypeNumber
                                                        {...totalReceivedProps}
                                                        ref={(componentObj) => { this.totalReceived = componentObj }}
                                                    />
                                                </Col>
                                                <Col sm='6'>
                                                    <InputTypeNumber
                                                        {...receiptNumberProps}
                                                        ref={(componentObj) => { this.receiptNumber = componentObj }}
                                                    />
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col sm='6'>
                                                    <InputTypeSimpleSelect
                                                        {...authority}
                                                        ref={(componentObj) => { this.authority = componentObj }}
                                                    />
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col >
                                                    <InputTypeText
                                                        {...noteProps}
                                                        ref={(componentObj) => { this.note = componentObj }}
                                                    />
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </ModalBody>
                                <ModalFooter className="hashavshevetModalFooter">
                                    <Row>
                                        <Col sm="auto">
                                            <Button className="hashavshevetButton" onClick={this.confirmBalanceUpdatingHandler}>{updateBalanceButtonText}</Button>
                                        </Col>
                                        <Col sm="auto">
                                            <Button className="hashavshevetButton hashavshevetButtonCancel" onClick={this.toggleUpdateBalanceModal}>{cancelButtonText}</Button>
                                        </Col>
                                    </Row>
                                </ModalFooter>
                            </Modal>
                        </Row>
                        <Row className="myRow">
                            <Col sm='3'>
                                <InputTypeNumber
                                    {...totalBalance}
                                    ref={(obj) => this.totalBalance = obj}
                                />
                            </Col>
                        </Row>

                        <Row>
                            <Col sm='8'>
                                <InputTypeText
                                    {...commentsAndPermission}
                                    ref={(componentObj) => {
                                        this.commentsAndPermission = componentObj
                                    }}
                                />
                            </Col>
                        </Row>

                        <Row>
                            <Col sm='auto'>
                                <Button

                                    className='font-weight-bold px-4 fs-17 mt-2 border-radius-3 regularButton btn btn-secondary btn-lg'
                                    onClick={this.saveClicked}
                                    id='sellersDistributorsSave'
                                >
                                    {LM.getString("save")}
                                </Button>
                                <Button
                                    className='font-weight-bold px-4 fs-17 mt-2 border-radius-3 regularButton btn btn-secondary btn-lg mx-3'
                                    onClick={this.freezeClicked}
                                    id='sellersDistributorsFreeze'
                                >
                                    {this.state.selectedSeller.status === 2 ? LM.getString('cancellationFreeze') : LM.getString("sellersDistributorsFreeze")}
                                </Button>
                                <Button
                                    className='font-weight-bold px-4 fs-17 mt-2 border-radius-3 regularButton btn btn-secondary btn-lg'
                                    onClick={this.lockClicked}
                                    id='sellersDistributorsLock'
                                >
                                    {this.state.selectedSeller.status === 3 ? LM.getString('unlock') : LM.getString("sellersDistributorsLock")}
                                </Button>
                            </Col>
                        </Row>
                    </Container>
                    : <></>
                }
            </Container>
        );
    }
}
