var soap = require('soap');
var logs = require('../../logger')
const path = require('path')
const PaymentBasic = require('../paymentBasic')
const models = require('../../index')
const Resinfo = require('../../resInfo')
const LM = require('../../config/LanguageManager').LM
const moment = require('moment')
const product = models.product
const productModel = product.model
const {MongoLogger} = require('../../mongoLogger')


const productPrice = models.productPrice
const productPriceModel = productPrice.model

var url = path.join(__dirname, "./wsdl/bezeq.wsdl")

/***
 * Payment manager Bezeq
 *
 */
class Bezeq extends PaymentBasic {


    constructor(req) {
        super()
        this.info = new Resinfo(req)
        this.price = 50.35
        this.contractNumber = "025332470"
        this.req = req
        this.timeout = 5000
        this.ban = 10003384
        this.ip = "84.95.87.242"
        this.appId = "ABC"
        this.userId = "666151"
        this.pymSourceId="PELTOP"
    }



    async  getBillPrice() {
          if ((await this.isDemoTransactionUser())) {
    
            this.info.send = [{ "ban": "00001", "res": {"invToPay":"test88273037","Ben":"0000001","invDueDate":"09/09/0112", "invBalance":"24.78"}
            }],
            this.info.externalId="11111"
            return this.info
        }
        else {
            if (this.req.body.idClient !=""){
                await this.getCustomerNumber(this.req.body.idClient)
            } else {
                await this.getBNHDebts()
            } 


        return this.info

        }
    }
    /***
   * check  what price of  bil/product 
   * 
   */
    async getPrice() {

        this.info.price = this.req.body.price
        logs.info("price  of bill  is ", this.req.body.price)
        return this.info

    }

    /***
            * Pay   
            @returns number  of   confirmation
            TODO change args{appId to   this.appId TST - for test}
            */
    async pay(transactionId) {
        if ((await this.isDemoTransactionUser())) {
            return this.demoRespons()
        }
        else {
            await this.payDebts(transactionId)
    
            return this.info
    
        }

      

    }

    /***
    * step 1 
    */
    async getBNHDebts() {
        const args = {
            debts: {
                appId: this.appId,
                userId: this.userId,
                invBalance: this.req.body.price,
                phoneNumber: this.req.body.contractNumber,
                ip: this.ip
            }
        }

        let client = await soap.createClientAsync(url);
        await client.getBNHDebtsAsync(args, { timeout: this.timeout })
            .then(result => {
               
                
                if (result[0].getBNHDebtsReturn["resonCode"] == 1) {
                    return this.getCustomerNumber(result[0].getBNHDebtsReturn["ban"])
                }

                else if (result[0].getBNHDebtsReturn["resonCode"] == 2) {
                    this.info.setErrMessage(LM.getString("PhoneNumberDidNotMatchTheInvoice"))
                    this.info.err_in_contractNumber = true
                    return this.info
                }
                else if (result[0].getBNHDebtsReturn["resonCode"] == 28) {
                    this.info.setErrMessage(LM.getString("wrongPhoneNumber"))
                    this.info.err_in_contractNumber = true
                    return this.info

                }
                else {
                    this.info.setErrMessage(LM.getErrorBasic(), result[0].getBNHDebtsReturn["resonCode"])
                    this.info.error.err_in_Payment = true
                    return this.info
                }


            }).catch(err => {
                this.info.setErrMessage(LM.getErrorBasic(), err)
                this.info.error.err_connection_site = true
            })

    }
    /***
step 2 (for bill without idClient)Id==Ban(from  getBNHDebts )or  id== idClient
    */
    async getCustomerNumber(id) {
        let zero = "0"
        let args = {
            customerData: {
                appId: this.appId,
                userId: this.userId,
                id: id,
                phoneNumber: this.req.body.contractNumber,
                ip: this.ip,
                customerNumber: zero,


            }
        }

        let client = await soap.createClientAsync(url);
        await client.getCustomerNumberAsync(args, { timeout: this.timeout })
            .then(result => {
             
                if (result[0].getCustomerNumberReturn["status"] == 1) {
                    return this.getDebts(result[0].getCustomerNumberReturn["customerNumber"])

                }
                
                else if (result[0].getBNHDebtsReturn["status"] == 2) {
                    this.info.setErrMessage(LM.getString("PhoneNumberDidNotMatchTheInvoice"))
                    this.info.err_in_contractNumber = true
                    return this.info
                }
                else if (result[0].getBNHDebtsReturn["resonCode"] == 28) {
                    this.info.setErrMessage(LM.getString("wrongPhoneNumber"))
                    this.info.err_in_contractNumber = true
                    return this.info

                }
                
                else {
                    this.info.error.err_in_Payment = true
                    this.info.setErrMessage(LM.getErrorBasic(), result[0].getCustomerNumberReturn["status"])
                }
            }).catch(err => {
                this.info.setErrMessage(LM.getErrorBasic(), err)
                this.info.error.err_connection_site = true
            })

    }
    /***
step 3 
   */

    async getDebts(customerNumber) {

        let args = {
            debts: {
                appId: this.appId,
                userId: this.userId,
                ban: customerNumber,
                ip: this.ip
            }
        }
        let client = await soap.createClientAsync(url)
        await client.getDebtsAsync(args, { timeout: this.timeout })
            .then(result => {
          
                if (result[0].getDebtsReturn["resonCode"]==6) {
                    this.info.send = [{ "ban": customerNumber, "res": result[0].getDebtsReturn.debtsResult }]
                    
                }
                else if (result[0].getDebtsReturn["resonCode"]==19) {
                    this.info.setErrMessage(LM.getString("noBill"))
                    this.info.error.noBills = true
                    
                }
                else {
                    this.info.error.err_in_Payment = true
                    this.info.setErrMessage(LM.getErrorBasic(), result[0].getDebtsReturn["resonCode"])
                }

            }).catch(err => {
                this.info.setErrMessage(LM.getErrorBasic(), err)
                this.info.error.err_connection_site = true
            })


    }

    // step 4  payment 

    async payDebts(transactionId) {
        let cash = "CA"
        let success = "6"

        let args = {
            payDebts: {
                appPassword:transactionId,
                appId: this.appId,
                invDetailToPay:
                    { invToPay:this.req.body.invToPay },
                pymType: cash,
                userId: this.userId,
                ip: this.ip,
                pymSourceId:this.pymSourceId,
                ban: this.req.body.ban,
                numOfInstalments: 1,
                numOfInvoices: 1,
                paymentAmt: this.req.body.price,
            }
        }

      
        
        let client = await soap.createClientAsync(url);
        await client.payDebtsAsync(args, { timeout: this.timeout })
            .then(result => {
                MongoLogger.logTransaction({
                    transactionId,
                    supplierId: this.req.params.supplierID,
                    requestParams: this.req.body,
                    request: url,
                    response: result ,
                })
                if (result[0].payDebtsReturn.resonCode == success) {

                    return this.getLastPayment(result[0].payDebtsReturn["keyfield"])
                }
                else {
                    this.info.error.err_in_Payment = true
                    this.info.setErrMessage(LM.getErrorBasic(), result[0].payDebtsReturn["resonCode"])
                }
            }).catch(err => {
                this.info.setErrMessage(LM.getErrorBasic(), err)
                this.info.error.err_connection_site = true
            })
    }


    // step 6  paymentConfirmation

    async sendPaymentConfirmation(extId) {
let  success=1
        let cash = "CA"
        let approved = "Y"
        const args = {
            paymentConfirmation: {
                ip: this.ip,
                userId: this.userId,
                appId: this.appId,
                ban: this.req.body.ban,
                pymDepositId: extId,
                invToPay: this.req.body.invToPay,
                pymType: cash,
                paymentAmt: this.req.body.price,
                paymentDate: moment().toDate().getTime(),
                paymentSts: approved,
                pymSourceId: this.pymSourceId
            }


        }

        let client = await soap.createClientAsync(url);
        await client.sendPaymentConfirmationAsync(args, { timeout: this.timeout })
            .then(result => {
                if (result[0].sendPaymentConfirmationReturn["status"] = success) {
                    this.info.confirmation = extId
                    this.info.send = {"success": extId }
                    this.info.isPaid = true
                    this.info.externalId = extId

                }
                else {
                    this.info.error.err_in_Payment = true
                    this.info.setErrMessage(LM.getErrorBasic(), result[0].sendPaymentConfirmationReturn["resonCode"])

                }

            }).catch(err => {
                this.info.setErrMessage(LM.getErrorBasic(), err)
                this.info.error.err_connection_site = true
            })




    }
    // step 5  payment
    async getLastPayment(keyfield) {
        const args = {
            ip: this.ip,
            userId: this.userId,
            appId: this.appId,
            key: keyfield


        }

        let client = await soap.createClientAsync(url);
        await client.getLastPaymentAsync(args, { timeout: this.timeout })
            .then(result => {

                if (result[0].getLastPaymentReturn["paymentStatus"] = "FINAL") {
                    return this.sendPaymentConfirmation(result[0].getLastPaymentReturn["extId"])
                }
                else {
                    this.info.setErrMessage(LM.getErrorBasic(), result[0].getLastPaymentReturn["resonCode"])
                    this.info.error.err_in_Payment = true
                }

            }).catch(err => {
                this.info.setErrMessage(LM.getErrorBasic(), err)
                this.info.error.err_connection_site = true
            })


    }


}


module.exports = Bezeq;






