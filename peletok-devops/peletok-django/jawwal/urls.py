from django.urls import path
from . import views


urlpatterns = [
    path(
        r'pay/',
        views.post_pay_card,
        name='pay'
    )
]
