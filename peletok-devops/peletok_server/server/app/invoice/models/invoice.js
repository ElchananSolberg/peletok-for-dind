const DataTypes = require('sequelize').DataTypes;
const {Op} = require("sequelize");
const BaseModel = require('../../BaseModel');

/**
 *  Invoice model
 */
class Invoice extends BaseModel {

    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            invoice_date: {
                type: DataTypes.DATE,
                allowNull: true
            },
            invoice_number: {
                type: DataTypes.STRING,
                allowNull: true
            },
            customer_number: {
                type: DataTypes.STRING,
                allowNull: true
            },
            path: {
                type: DataTypes.STRING,
                allowNull: true
            },
        };

    }

}

module.exports = new Invoice();