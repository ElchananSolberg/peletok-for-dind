import React, { Fragment } from 'react';
import { LanguageManager as LM } from "../LanguageManager/Language";
import InputTypeCheckBox from '../InputUtils/InputTypeCheckBox';
import PermissionsRow from './PermissionsRow';
import {  PermissionsContext } from './permissionsContext';
import { CollapseBtn } from '../btnUtils/collapseBtn'
import {PermissionsEnum} from '../../enums'
import {createArrayInObjectOrPushIfExist} from '../../utilities/utilities'

/**
 * PermissionsTableFirstRow - component that builds and returns an array of <td> elements .
 */
export default class PermissionsGroup extends React.Component {
    
    STATE_EMPTY     = 0;
    STATE_PARTIAL   = 1;
    STATE_FULL      = 2;


    constructor(props) {
        super(props);

        // Local var to determine if collapse open
        this.open = false;

        //  Calculate and save all permission ids in current group
        // this is static data that calculate once in component life cycle
        this.permissionIds = Object.keys(this.props.permissions).map(k=>this.props.permissions[k].id)
    }

    // when the collapse opened we should wait and change local open flag
    setOpenWhenRoleChanged(){
        setTimeout(()=>{
            this.open = true
        },1000);
        return true;
    }
    
    
    getActionCount(permissionsRolesForm, action){
         return Object.keys(permissionsRolesForm)
             .reduce((newArr,permissionId)=>(newArr.concat(permissionsRolesForm[permissionId])),[])
             .filter(pr=>this.permissionIds.indexOf(pr.permissionId) >= 0)
             .filter(pr=>PermissionsEnum.mapPermisionRoleToAction(pr, action)).length
            
    }


   



    render() {
        var mapKey = 0;
        let actionCount;

        return (
            
                <Fragment>
                    <PermissionsContext.Consumer>
                        {({ permissionsRolesForm,
                            setGroupsCollapse,
                            updateAll,
                            permissionActions,
                            groupsCollapse,
                            deleteAll,
                            permissionLableCellWidth,
                            permissionTempEditCellWidth
                        }) => (
                                <tbody>
                                <tr className={!!groupsCollapse[this.props.groupName] ? 'group-header' : ''}>
                                <td style={{width: permissionLableCellWidth}} className="collapse-td" key={mapKey++}>
                                    <CollapseBtn
                                        open={!!groupsCollapse[this.props.groupName]} 
                                        onClick={()=>{ setGroupsCollapse(this.props.groupName).then(()=>{this.open = !this.open})}} />
                                    <span>{ this.props.groupName }</span>
                                </td>
                                
                                    {Object.keys(permissionActions).map((key)=>{
                                        actionCount = this.getActionCount(permissionsRolesForm, key) 
                                        return  <td key={mapKey++} style={{width: parseInt(key) === PermissionsEnum.ACTION_TEMP_EDIT ? permissionTempEditCellWidth : ''}}>
                                            
                                                
                                                    <div className={"custom-control custom-checkbox " + (LM.getDirection() === "rtl" ? "checkbox-rtl" : "")}>
                                                        {                 
                                                          actionCount > 0 && actionCount < this.permissionIds.length && <div className="d-flex">
                                                              <div 
                                                                className="checkbox-rtl  custom-checkbox custom-control custom-control-div-fill"
                                                                onClick={()=>{
                                                                    updateAll(this.permissionIds, key, this.props.groupName)
                                                                    
                                                                }}
                                                              >
                                                                  <label 
                                                                    className="custom-control-label-fill"
                                                                 
                                                                    >
                                                                      
                                                                </label>
                                                              </div>
                                                            </div>
                                                        }
                                                        {actionCount == this.permissionIds.length && // we should switch between the two inputs to rerender the component ugly but work 
                                                            <InputTypeCheckBox                                                         
                                                                disabled={parseInt(key) === PermissionsEnum.ACTION_TEMP_EDIT && actionCount == 0} 
                                                                id={`Watching_${this.props.groupName}_${key}`} 
                                                                onChange={(e)=>{
                                                                    e.target.checked ?
                                                                        updateAll(this.permissionIds, key, this.props.groupName)
                                                                            :deleteAll(this.permissionIds, key, this.props.groupName)
                                                                    
                                                                }}
                                                                checked={true}
                                                             />
                                                         }
                                                         {actionCount == 0 && 
                                                            <InputTypeCheckBox                                                         
                                                                disabled={parseInt(key) === PermissionsEnum.ACTION_TEMP_EDIT && actionCount == 0} 
                                                                id={`Watching_${this.props.groupName}_${key}`} 
                                                                onChange={(e)=>{
                                                                    e.target.checked ?
                                                                        updateAll(this.permissionIds, key, this.props.groupName)
                                                                            :deleteAll(this.permissionIds, key, this.props.groupName)
                                                                    
                                                                }}
                                                                
                                                             />
                                                         }

                                                            
                                                    </div>
                                                </td>
                                    })}
                                </tr>
                                    </tbody>
                            )}
                    </PermissionsContext.Consumer>
                    <PermissionsContext.Consumer>
                    {({  groupsCollapse }) => (
                        groupsCollapse[this.props.groupName] && this.setOpenWhenRoleChanged() &&
                        
                            <tbody className={!this.open ? "group-collapse" : ''}>
                        <Fragment>
                            {this.props.permissions.map((permission, index)=>{
                                return <PermissionsRow
                                        groupName={this.props.groupName}
                                        permission={permission}
                                        last={this.props.permissions.length - 1 == index}                                     
                                    />
                            })}
                        </Fragment>
                            </tbody>

                    
                    )}
                    </PermissionsContext.Consumer>
                </Fragment>
           
        );
    }
}