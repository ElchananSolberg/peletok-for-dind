import React, { Component } from 'react';
import { LanguageManager as LM } from '../LanguageManager/Language';
import InputTypeFile from '../InputUtils/InputTypeFile';
import { Container, Row, Col, Button } from 'reactstrap';
import './InvoiceUpload.css';
import { saveInvoicePost } from '../DataProvider/DataProvider';
import { Notifications } from '../Notifications/Notifications';
import { Loader } from '../Loader/Loader';

export class InvoiceUpload extends Component {
    constructor(props) {
        super(props)

    }
    invoiceSaveHandler = () => {
        if(this.invoiceUploadInputFileRef.getValue().value){
            Loader.show()
            let data = new FormData();
            data.append('file', this.invoiceUploadInputFileRef.getValue().value)
            saveInvoicePost(data).then(
                (res) => {
                    Loader.hide()
                    if(res == 'success'){
                        Notifications.show(LM.getString('invoice_uploud_success'), 'success');
                    } else {
                        Notifications.show(res, 'danger');

                    }
                }
            ).catch(
                (err) => {
                    Loader.hide()
                    Notifications.show(LM.getString('invoice_uploud_error'), 'danger');
                }
            )
            this.invoiceUploadInputFileRef.setValue('')
        }
    }
    render() {
        let arr = ['Peletalk', 'Cellcom'];
        const header = LM.getString('invoiceUpload');

        return (
            <Container className="invoiceUploadContainer">
                <Row >
                    <Col className="invoiceUploadHeader">
                        {header}
                    </Col>
                </Row>
                <Container className="invoiceUploadSettings">
                    <Row >
                        <Col sm='4'>
                            <InputTypeFile
                                id="InvoiceUpload"
                                title={LM.getString("chooseInvoiceFiles")}
                                ref={objRef => this.invoiceUploadInputFileRef = objRef}
                            />
                        </Col>
                        <Col sm='4'>
                            <Button
                                className='invoiceUploadButton invoiceUploadSaveButtonMargin'
                                id="invoiceUploadSave"
                                onClick={this.invoiceSaveHandler}>
                                {LM.getString("save")}
                            </Button>
                        </Col>
                    </Row>
                </Container>
            </Container>)
    }
}
