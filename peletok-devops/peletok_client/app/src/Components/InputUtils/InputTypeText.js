import React, { Component } from 'react';
import './InputUtils.css';
import { FormFeedback, Input, Row, Col, Card, CardText } from 'reactstrap';

import { Tooltip } from './Tooltip';

import { LanguageManager as LM } from '../LanguageManager/Language'
import Asterisk from '../Ui/Asterisk'

/**
 * InputTypeText component lets to input the TEXT. 
 * it changes it's type to type="password" if you send it a prop password (one word inside of the InputTypeText tag OR password:true inside of a props object)
 * it can render the title above the input (if this.props.title is not an empty string).
 * it can render the tooltip to the side of the title (if this.props.tooltip is not an empty string).
 * it can set the maximum characters limit using this.props.maxLength 
 * it validates the value's length is not less than this.props.minLength
 * It validates the value was entered (if this.props.required is true).
 * It validates the value is valid to the function sent in  this.props.validationFunction;
 * if there are any validation issues the input will get a prop "invalid" and will change a border color to red;
 * if there are any validation issues bellow the input will be the error message 
 * It can add outer onChange function to checkbox onChange.
 * It can change an input to disabled view due to this.props.disabled.
 */
class InputTypeText extends Component {
    constructor(props) {
        super(props);

        this.state = {
            value: '',
            validationFuncs: [
                {
                    /**  Validates the value is valid to the function sent in  this.props.validationFunction */
                    f: this.props.validationFunction || (() => (true)),
                    msg: this.props.notValidMessage
                },
                {
                    f: this.innerValidationFunction,
                    msg: LM.getString("textInnerValidationFunctionMsg") + this.props.minLength,
                },
                {
                    f: this.isRequiredValidation,
                    msg: LM.getString("isRequiredValidationFunctionMsg"),
                }
            ],
            errorMessage: '',
        }
        this.setValue = this.setValue.bind(this);
    }

    /** can add outer onChange function to checkbox onChange. */
    inputTypeTextChange = (e) => {
        e.persist();
        this.setState({ value: e.target.value },
            () => {
                if (this.props.onChange) {
                    this.props.onChange(e);
                }
            }
        );
    }

    /** summarizing validation function
    * is running 2 times: when input is loosing focus and when getValue function is running (by programmer-user) 
    * */
    runValidation = (onlyResult = false) => {
        let summarizingValidationResult = (!this.props.required && !this.state.value) || this.state.validationFuncs.every((validation) => validation.f(this.state.value));
        if (!summarizingValidationResult && !onlyResult) {
            let messages = [];
            this.state.validationFuncs.forEach((validation) => {
                if (!validation.f(this.state.value)) {
                    messages.push(validation.msg);
                }
            })
            this.setState(({ errorMessage: messages.join(`\n`) }));
        } else if (!onlyResult) {
            this.setState(({ errorMessage: '' }));
        }
        return summarizingValidationResult;
    }

    getValue = () => {
        let summarizingValidationResult = this.runValidation(true);
        return { valid: summarizingValidationResult, value: this.state.value };
    }

    /** If this.props.required is true validates the value was entered. */
    isRequiredValidation = (value) => {
        if (!value && this.props.required) {
            return false;
        }
        return true;
    }

    /** Validates the value's length is not less than this.props.minLength */
    innerValidationFunction = (value) => {
        let minLength = this.props.minLength;
        if (value.length < minLength) {
            return false;
        }
        return true;
    }
    setValue(newVal) {
        this.setState({
            value: newVal
        })
    }

    render() {
        let disabled = this.props.disabled;
        let password = this.props.password;
        let maxLength = this.props.maxLength;
        let placeholder = this.props.placeholder;
        let id = this.props.id.replace(/\s+/g, '');
        let title = this.props.title;
        let tooltipText = this.props.tooltip;

        let type2 = this.props.type2;

        return (type2 ?
            <>
                <Row >
                    <Col >
                        <Card className="type2Card">
                            <CardText className="type2CardText"  >
                                {title}<Asterisk show={this.props.required}/>
                            </CardText>
                            <Input className={"type2Input " + (LM.getDirection() === "rtl" ? "rtl" : "")} type='text'
                                id={`InputTypeText${id}`} placeholder={placeholder}
                                disabled={disabled} invalid={this.state.errorMessage.length !== 0}
                                maxLength={maxLength}
                                onChange={this.inputTypeTextChange} onBlur={this.runValidation}
                                value={this.state.value}
                            />
                            {/** if there are any validation issues bellow the input will be the error message */}
                            <FormFeedback className='ws-pre'>{this.state.errorMessage}</FormFeedback>
                        </Card>
                    </Col>
                </Row>
            </>
            :
            <>
                <Row>
                    <Col className={"d-flex justify-content-between line-height-1 " + (title ? 'py-1' : '')}>
                        {title ?
                            <div className="fs-17 white-space">
                                {title}
                                <Asterisk show={this.props.required}/>
                            </div>
                            : <><Asterisk show={this.props.required}/></>
                        }
                        {tooltipText ?
                            <Tooltip tooltipText={tooltipText} id={id} />
                            : <></>
                        }
                    </Col>
                </Row>
                <Row className={'mt-auto'}>
                    <Col >
                        {!this.props.multi ?
                            /** if there are any validation issues the input will get a prop "invalid" and will change a border color to red */ 
                            < Input type={password ? "password" : "text"} id={`InputTypeText${id}`} placeholder={placeholder}
                                maxLength={maxLength} autoComplete={password ? "new-password" : ""}
                                disabled={disabled} invalid={this.state.errorMessage.length !== 0}
                                onChange={this.inputTypeTextChange} onBlur={this.runValidation}
                                value={this.state.value}
                                className={(LM.getDirection() === "rtl" ? "rtl" : "")}
                                style={this.props.outerStyle}
                        />
                        :
                            <Input 
                                type="textarea" id={`InputTypeText${id}`} placeholder={placeholder}
                                disabled={disabled} invalid={this.state.errorMessage.length !== 0}
                                onChange={this.inputTypeTextChange} onBlur={this.runValidation}
                                value={this.state.value}
                                className="inputUtilsNotice"
                                style={this.props.outerStyle}
                            />
                        }

                        {/** if there are any validation issues bellow the input will be the error message */}
                        <FormFeedback className='ws-pre'>{this.state.errorMessage}</FormFeedback>
                    </Col>
                </Row>
            </>
        );
    }
}

export default InputTypeText;