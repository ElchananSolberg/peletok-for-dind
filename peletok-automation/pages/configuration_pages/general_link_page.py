from selenium.webdriver.common.by import By


class GeneralLink:
    vat_input = (By.ID, 'InputTypeTextheaderVat')
    save_btn = (By.ID, 'vatSave')
    alert_successful = (By.CSS_SELECTOR, 'div.myAlert.alert.alert-success.fade.show')