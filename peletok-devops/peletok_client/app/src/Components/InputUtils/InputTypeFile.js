import React, { Component } from 'react';
import './InputUtils.css';
import { CustomInput, Input, Row, Col } from 'reactstrap';
import { Tooltip } from './Tooltip';
import { LanguageManager as LM } from '../LanguageManager/Language';
import Asterisk from '../Ui/Asterisk';

/**
 * InputTypeFile component lets the user to choose a file on his PC.
 * it can render the title above it.
 * it can render the tooltip to the side of the title.
 */
class InputTypeFile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            /** variable that will be returned by the getValue() function (= sent to the server) */
            file: null,
            validationFuncs: [
                {
                    f: this.isRequiredValidation,
                    msg: LM.getString("isRequiredValidationFunctionMsg"),
                }
            ],
            errorMessage: '',
        }
    }

    /** If this.props.required is true validates the file was selected (it is not null). */
    isRequiredValidation = (value) => {
        if (this.props.required && (value === null)) {
            return false;
        }
        return true;
    }

    /** summarizing validation function
    * is running 3 times: when a user is clicking on the div and when the file was selected and when getValue function is running (by programmer-user) 
    * */
    runValidation = () => {
        let summarizingValidationResult = (!this.props.required && !this.state.file) || this.state.validationFuncs.every((validation) => validation.f(this.state.file));
        if (!summarizingValidationResult) {
            let messages = [];
            this.state.validationFuncs.forEach((validation) => {
                if (!validation.f(this.state.file)) {
                    messages.push(validation.msg);
                }
            })
            this.setState(({ errorMessage: messages.join(`\n`) }));
        } else {
            this.setState(({ errorMessage: '' }));
        }
        return summarizingValidationResult;
    }

    getValue = () => {
        let summarizingValidationResult = this.runValidation();
        return { valid: summarizingValidationResult, value: this.state.file };
    }

    setValue = (newValue) => {
        this.setState({ file: newValue });
    }

    /** Change handler of an Input type="file". It will run only if user picked a file.
    * And the variable this.state.file that will be returned by the getValue().
    */
    fileChanged = (e) => {
        e.preventDefault();
        let reader = new FileReader();
        let file = e.target.files[0];

        reader.onloadend = ((object) => {
            return () => {
                object.setState({ file: file });
                this.runValidation();
                this.props.onLoadend && this.props.onLoadend(file)
            }
        })(this)

        if (e.target.files[0]) {
            reader.readAsDataURL(e.target.files[0]);
        }
    }

    render() {
        let id = this.props.id.replace(/\s+/g, '');
        let title = this.props.title;
        let tooltipText = this.props.tooltip;
        let disabled = this.props.disabled;
        return (
            <>
                <Row>
                    <Col className={"d-flex justify-content-between line-height-1 " + (title ? 'py-1' : '')}>
                        {title ?
                            <div className="fs-17 white-space">
                                {title}
                                <Asterisk show={this.props.required} />
                            </div>
                            : <> <Asterisk show={this.props.required}/></>
                        }
                        {tooltipText ?
                            <Tooltip tooltipText={tooltipText} id={id} />
                            : <></>
                        }
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <CustomInput type="file" label={this.state.file === null ? " " : this.state.file.name} id={`inputTypeFile${id}`}
                            disabled={disabled}
                            onChange={this.fileChanged} onBlur={this.runValidation}
                            ref="fileUploader"
                            className={LM.getDirection() === "rtl" ? "rtl" : ""}
                        />
                        {/** if there are any validation issues bellow the input will be the error message */}
                        <div className='ws-pre errorMessageDiv'>{this.state.errorMessage}</div>
                    </Col>
                </Row>
            </>
        )
    }
}
export default InputTypeFile;