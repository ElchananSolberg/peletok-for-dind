import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Row, Col, Table } from 'reactstrap';
import { LanguageManager as LM } from '../../LanguageManager/Language';

export const CancelationModal = (props) => {

    return (
        <Modal size="lg" isOpen={props.isOpen} toggle={props.toggle} className={props.className}>
            <ModalHeader toggle={props.headerToggle}>{props.header}</ModalHeader>
            <ModalBody>
                <Row>
                    {props.table}
                </Row>
                <Row>
                    {!props.disableAmountInput &&
                        <Col>
                            {props.amountInput}
                        </Col>}
                    <Row>
                        <Col className='my-5'>
                            {props.inSystemCheckbox}
                        </Col>
                    </Row>
                    </Row>
                    <Row>
                        <Col>
                            {props.note}
                        </Col>
                    </Row>
               
            </ModalBody>
            <ModalFooter>
                <Button className='m-2' color="primary" onClick={props.confirmClick} >{LM.getString("confirm")}</Button>{' '}
                <Button color="secondary" onClick={props.cancelClick} >{LM.getString("cancel")}</Button>
            </ModalFooter>
        </Modal>
    );
}