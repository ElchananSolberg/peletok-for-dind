const businessProduct = require("../index").businessProduct.model;
const logger = require("../logger");


/**
 * Copies percentage_profit or commission from one product to other product
 * @param req {Object} request object
 * @param column {string} percentage_profit or commission
 * @return {Promise<void>}
 */
async function copyProfit(req, column) {
    // TODO: copy row also if column is empty?
    if (["percentage_profit", "business_commission"].indexOf(column) === -1) throw new Error(`column must be percentage_profit or commission not ${column}`);
    const from = req.query.from;
    const to = req.query.to;
    const author = req.user.id;
    if (from && to) {
        let sourceProd = await businessProduct.findAll({
            where: {product_id: from},
            attributes: ["business_id", "is_authorized", "status", column],
            order: ["business_id"]
        });
        if (!(sourceProd[0])) {
            throw new Error(`product id ${from} not found`);
        }
        let targetProd = await businessProduct.findAll({
            where: {product_id: to},
            order: ["business_id"]
        });
        if (!(targetProd[0])) {
            throw new Error(`product id ${to} not found`);
        }
        let sourceLength = sourceProd.length;
        let targetLength = targetProd.length;
        if(sourceLength!==targetLength){
            throw new Error(`found ${sourceLength} rows for source product and
             ${targetLength} rows for target product, pleas fix data before copy`)
        }
        for(let i=0;i<sourceLength;i++){
            let row = targetProd[i];
            if (sourceProd[i].business_id!==row.business_id){
                throw new Error("pleas fix data before copy")
            }
            row.is_authorized = sourceProd[i].is_authorized;
            row[column] = sourceProd[i][column];
            if(row.status!==sourceProd[i].status){
                row.status_author = author;
                row.status = sourceProd[i].status;
            }
            // TODO: use sql transaction for all row
            row.save().catch(e => {
                throw new Error(e);
            });
        }
    } else {
        throw new Error("a query request must be include 'from' and 'to'");
    }
}

module.exports.copyProfit = copyProfit;
