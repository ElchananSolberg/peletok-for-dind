const logs = require('../../logger')
const PaymentBasic = require('../paymentBasic');
const models = require('../../index');

const Resinfo = require('../../resInfo')
const urlLogin = 'https://www.etopup.ps/Account/LogOn?ReturnUrl=%2f'
const LM = require('../../config/LanguageManager').LM
var webdriver = require('selenium-webdriver');
var until = webdriver.until;
const product = models.product
const productModel = product.model
const chrome = require('selenium-webdriver/chrome')
const driver = new webdriver.Builder()
    .forBrowser('chrome').setChromeOptions(new chrome.Options().headless())
    .build();
const {MongoLogger} = require('../../mongoLogger')

/***
    * TODO to add  userrNum & password to env file
       */
var userNum = "972592931986"
var password = "55584335"
const productPrice = models.productPrice
const productPriceModel = productPrice.model

/***
    * Payment jawwal
       */
class Jawwal extends PaymentBasic {
    constructor(contractNumber, productId, req) {
        super()
        this.req=req
        this.contractNumber = contractNumber
        this.info = new Resinfo(req)

        this.productId = productId
    }
    

    /***
        * Payment jawwal
           */
    async pay(transactionId) {

        if ((await this.isDemoTransactionUser())) {
            return this.demoRespons()
        }
        else {

            let phoneNumber = "+972" + this.contractNumber.slice(1)
            let item = await productModel.findOne(
                {
                    where: { id: this.productId }
                })

            let itemName = item["supplier_identity_number"]
            if (!itemName) {
                this.info.setErrMessage(LM.getErrorBasic(), "Error in supplier_identity_number ")
                return this.info
            }



            await
                driver.get(urlLogin)
            var clinetNumFld = await driver.wait(until.elementLocated(webdriver.By.id('UserName')))
            var clinetPassword = await driver.findElement(webdriver.By.id('Password'))
            var nextBtn = await driver.findElement(webdriver.By.id('btnLogin'))
            await clinetNumFld.sendKeys(userNum)
            await clinetPassword.sendKeys(password)
            await nextBtn.click()
            /***
                 * step2
                    */
            var numOfPelefon = await driver.wait(until.elementLocated(webdriver.By.id('RecipientNumber')))
            var summToPay = driver.findElement(webdriver.By.id('Amount'))
            var btnPay = driver.findElement(webdriver.By.id('callConfirm'))
            await numOfPelefon.sendKeys(parseInt(this.contractNumber))
            await summToPay.sendKeys(parseInt(itemName))
            await btnPay.click()

            try {
                var errorPageEnd = await driver.findElement(webdriver.By.xpath('//span[@class="field-validation-error"]')).getText()
                this.info.setErrMessage(LM.getErrorBasic(), "error in  payment" + errorPageEnd)

                return this.info
            } catch (err) { }
            var btnOK = await driver.wait(until.elementLocated(webdriver.By.xpath("//span[text()='موافق']")))
            await btnOK.click()
            /***
        * step3 сonfirmation
           */
            var btn = await driver.wait(until.elementLocated(webdriver.By.xpath("//*[@id='ajaxform']//input[@type='submit']")))
            btn.click()
            return await new Promise(async (resolve, reject) => {


                setTimeout(async () => {
                    try {
                        let reference = await driver.findElement(webdriver.By.xpath("//*[@id='last5Table']//tr//td['" + phoneNumber + "')]/following-sibling::td[1]")).getText();
                        logs.info("confirmation is ", reference)
                        this.info.send = { "success": reference }
                        this.info.confirmation = reference
                        this.info.isPaid = true
                        MongoLogger.logTransaction({
                            transactionId,
                            supplierId: this.req.params.supplierID,
                            requestParams: this.req.body,
                            request: 'robot',
                            response: this.info ,
                        })
                        resolve(this.info)

                    } catch (err) {
                        this.info.setErrMessage(LM.getErrorBasic(), "error in  reference  or  payment " + err)

                        resolve(this.info)

                    }
                }, 700)

            })
        }
    }
}

module.exports = Jawwal;

