'use strict';
const {addColumnsToTable} = require('../dbUtils/addPermission')
module.exports = {
  up: async (queryInterface, Sequelize) => {
  await  addColumnsToTable(queryInterface, 'BNK_BusinessPayment', [{
      name: 'business_identifier', typeOptions: {
        type: Sequelize.INTEGER,
        allowNull: true
      }
    },
   ])
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn('BNK_BusinessPayment', 'business_identifier', { transaction: t }),
      ])
    })
  }
};