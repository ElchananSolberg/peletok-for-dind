import React from 'react'

export default function Asterisk({show}){
	return show ? <span style={{color: '#dc3545'}}>&#42;</span> : <></>
}