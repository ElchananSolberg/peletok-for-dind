import React, { Component } from 'react';
import './Hashavshevet.css';

import { Container, Row, Col, Button, Collapse } from 'reactstrap';

import { LanguageManager as LM } from '../LanguageManager/Language';

import { getSellers } from '../DataProvider/DataProvider'

import InputTypeSearchList from '../InputUtils/InputTypeSearchList';
import InputTypeNumber from '../InputUtils/InputTypeNumber';
import InputTypeDate from '../InputUtils/InputTypeDate';
import InputTypeTime from '../InputUtils/InputTypeTime';

import { ReportTableContent } from '../ReportComponent/ReportTableContent';


/** HashavshevetPaymentsReport is a main component of HashavshevetPaymentsReport Page.  
 * HashavshevetPaymentsReport and its children are working with .css file: Hashavshevet.css - common for all Hashavshevet Reports Pages. */
class HashavshevetPaymentsReport extends Component {
    constructor(props) {
        super(props);

        this.state = {
            /** working variable that will contain an array (of objects) of all the sellers */
            sellers: [],
            /** working variable that indicates if a user is on the stage of viewing a report */
            viewingReport: false,
            /** Table data - TODO will be CHANGED by a data from server */
            tableRows: [
                {
                    sellerName: 'sellerName',
                    terminalNumber: 'terminalNumber',
                    salespersonNumber: 'salespersonNumber',
                    receiptNumber: 'receiptNumber',
                    date: 'date',
                    amount: 'amount',
                    type: 'type',
                },
                {
                    sellerName: 'sellerName',
                    terminalNumber: 'terminalNumber',
                    salespersonNumber: 'salespersonNumber',
                    receiptNumber: 'receiptNumber',
                    date: 'date',
                    amount: 'amount',
                    type: 'type',
                },
            ],
        }
    }

    /** TODO: Add functionality if selecting of a seller should affect other fields */
    sellerChanged = (e) => {

    }

    /** Show Report Button click handler
     * TODO: Add functionality */
    showReportClicked = () => {
        this.setState({ viewingReport: true });
    }

    componentDidMount() {
        getSellers(true)
            .then(
                res => {
                    this.setState({ sellers: res });
                }
            )
            .catch((err) => alert(err));
    }

    render() {
        /** Payments Header  */
        const header = LM.getString('hashavshevetPaymentsReport');

        /** data for "Select Seller" field (InputTypeSearchList InputUtils component) */
        const selectSellerProps = {
            title: LM.getString("selectSeller"),
            id: "ExportOrdersFromHashavshevetSelectSeller",
        };

        /** data for "Agent" field (InputTypeNumber InputUtils component) */
        const agentProps = {
            title: LM.getString("agent"),
            id: "OrdersExportedToHashavshevet_Agent",
        };
        /** data for "Terminal Number" field (InputTypeNumber InputUtils component) */
        const terminalNumberProps = {
            title: LM.getString("terminalNumber"),
            id: "ExportOrdersFromHashavshevetTerminalNumber",
        };

        /** data for "Starting Date" field (InputTypeDate InputUtils component) */
        const startingDateProps = {
            title: LM.getString("startingDate"),
            id: "ExportOrdersFromHashavshevetStartingDate",
        };
        /** data for "Ending Date" field (InputTypeDate InputUtils component) */
        const endingDateProps = {
            title: LM.getString("endingDate"),
            id: "ExportOrdersFromHashavshevetEndingDate",
        };

        /** data for "Starting Time" field (InputTypeDate InputUtils component) */
        const startingTimeProps = {
            title: LM.getString("startingTime"),
            id: "ExportOrdersFromHashavshevetStartingTime",
        };
        /** data for "Ending Time" field (InputTypeDate InputUtils component) */
        const endingTimeProps = {
            title: LM.getString("endingTime"),
            id: "ExportOrdersFromHashavshevetEndingTime",
        };

        /** Table Header data */
        const tableHeadersObj = {
            sellerName: LM.getString('sellerName'),
            terminalNumber: LM.getString('terminalNumber2'),
            salespersonNumber: LM.getString('salespersonNumber'),
            receiptNumber: LM.getString('receiptNumber'),
            date: LM.getString('date'),
            amount: LM.getString('amount'),
            type: LM.getString('type'),
        };

        /** data for Show Report button */
        const showReportButtonText = LM.getString("showReport");

        return (
            <Container className="hashavshevetContainer">
                <Row >
                    <Col className="hashavshevetHeader">
                        {header}
                    </Col>
                </Row>
                <Container className="hashavshevetSettings">
                    <Row>
                        <Col sm='4'>
                            <InputTypeSearchList {...selectSellerProps} onChange={this.sellerChanged}
                                options={this.state.sellers.map(seller => seller.name)}
                                ref={(componentObj) => { this.selectSeller = componentObj }} />
                        </Col>
                    </Row>
                    <Row >
                        <Col sm='4'>
                            <InputTypeNumber {...agentProps} ref={(componentObj) => { this.agent = componentObj }} />
                        </Col>
                        <Col sm='4'>
                            <InputTypeNumber {...terminalNumberProps} ref={(componentObj) => { this.terminalNumber = componentObj }} />
                        </Col>
                    </Row>
                    <Row >
                        <Col sm='4'>
                            <InputTypeDate {...startingDateProps} ref={(componentObj) => { this.startingDate = componentObj }} />
                        </Col>
                        <Col sm='4'>
                            <InputTypeTime {...startingTimeProps} ref={(componentObj) => { this.startingTime = componentObj }} />
                        </Col>
                    </Row>
                    <Row >
                        <Col sm='4'>
                            <InputTypeDate {...endingDateProps} ref={(componentObj) => { this.endingDate = componentObj }} />
                        </Col>
                        <Col sm='4'>
                            <InputTypeTime {...endingTimeProps} ref={(componentObj) => { this.endingTime = componentObj }} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="auto">
                            <Button className="hashavshevetButton" id="hashavshevetShowReportButton" onClick={this.showReportClicked} > {showReportButtonText} </Button>
                        </Col>
                    </Row>
                </Container>
                <Row>
                    <Col >
                        <Collapse isOpen={this.state.viewingReport} >
                            <ReportTableContent
                                tableHeadersObj={tableHeadersObj}
                                tableRows={this.state.tableRows}
                                reportName={"HashavshevetPaymentsReport"}
                            />
                        </Collapse>
                    </Col>
                </Row>
            </Container>
        )
    }
}

export default HashavshevetPaymentsReport;