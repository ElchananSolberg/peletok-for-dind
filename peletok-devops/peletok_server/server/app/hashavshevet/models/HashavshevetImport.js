const BaseModel = require('../../BaseModel');
const DataTypes = require("sequelize").DataTypes;

/**
 * HashavshevetImport model
 */
class HashavshevetImport extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            timestamp_import: {
                type: DataTypes.DATE,
                allowNull: true
            },
            file_name: {
                type: DataTypes.STRING(256),
                allowNull: true
            },
            start_date: {
                type: DataTypes.DATE,
                allowNull: true
            },
            end_date: {
                type: DataTypes.DATE,
                allowNull: true
            },
            is_hashavshevet_id: {
                type: DataTypes.BOOLEAN,
                allowNull: true
            }
        };
        this.prefix = "HASH";
        this.author = true;
    }

    /**
     * creates associate for HashavshevetImport model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.hasMany(models.businessPayment.model, {as: "BusinessPayment" })
    }
}

module.exports = new HashavshevetImport();
