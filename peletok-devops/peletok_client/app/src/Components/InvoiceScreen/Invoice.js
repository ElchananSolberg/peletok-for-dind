import React, { Component } from 'react';
import './Invoice.css';
import { Button, Row, Col, Modal, Collapse, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { LanguageManager as LM } from '../LanguageManager/Language'
import { postSendIvoiceByMail, getInvoice, postInvoiceBusinessId, getCurrentUserData } from '../DataProvider/DataProvider'
import InputTypeSelect from '../InputUtils/InputTypeSelect';
import InputTypeRadio from '../InputUtils/InputTypeRadio';
import InputTypeEmail from '../InputUtils/InputTypeEmail';
import InputTypeCheckBox from '../InputUtils/InputTypeCheckBox'
import ReactSVG from 'react-svg';
import pdfSVG from './file-pdf-1-1.svg';
import { Notifications } from '../Notifications/Notifications';
import { Loader } from '../Loader/Loader';
import { config as SERVER_BASE } from "../../utils/Config";
import InvoiceMailingSubscription from './InvoiceMailingSubscription';

/**
 * Main component of Invoice Page.  Invoice and its children are working with separate .css file: Invoice.css.
 */
export class Invoice extends Component {
    constructor(props) {
        super(props);

        let nowYear = new Date().getFullYear();
        let years = []
        for (let i = 0; i >= -10; i--) {
            years.push(nowYear + i);
        }

        this.state = {
            selectYearProps: {
                options: years,
                default: years[0],
                id: "InvoiceSelectYear",
            },
            selectMonthProps: {
                options: LM.getString("monthArray"),
                default: LM.getString("monthArray")[0],
                id: "InvoiceSelectMonth",
            },
            emailProps: {
                placeholder: 'email@example.com',
                title: LM.getString("enterMail") + ":",
                id: "InvoiceInputMail",
                required: true,
            },
            serviceTypeProps: {
                vertical: true,
                options: LM.getString("invoiceChooseProviderOptions"),
                title: LM.getString("invoiceChooseProviderFieldName") + ":",
                id: "InvoiceServiceType-",
                required: true,
            },
            invoices: [],
            sendEmailModalOpen: false,
            showTable: false,
            isSubscribe: false,
            renderAll: false,
        };
        this.fields = [];
    }
    /** Show Invoice button click handler */
    showInvoiceClick = (e) => {
        Loader.show()
        let params = {};
        params.year = this.selectYear.getValue().value;
        (this.selectMonth.getValue().value != LM.getString("monthArray")[0]) && (params.month = LM.getString("monthArray").indexOf(this.selectMonth.getValue().value))

        getInvoice(params).then((res) => {
            this.setState({
                showTable: true,
                invoices: res

            })
            Loader.hide()
        }).catch((e) => { Loader.hide() })

    }
    /** Send Invoice button click handler
     * TODO: add functionality */
    sendInvoiceClick = (e) => {
        if (this.state.sendEmailModalOpen) {
            let invoiceIds = this.getCheckedIds();
            if (this.serviceType.getValue().valid && this.mail.getValue().valid) {
                let emailToUse = this.mail.getValue().value;
                // TODO send email with all invoiceIds attached
                this.setState({ sendEmailModalOpen: !this.state.sendEmailModalOpen })
            }
        } else {
            if (this.getCheckedIds().length > 0) {
                this.setState({ sendEmailModalOpen: !this.state.sendEmailModalOpen })
            } else {
                Notifications.show(LM.getString('chooseInvoice'), 'danger')
            }
        }
    }
    getCheckedIds = () => {
        return this.fields.map(f => f.getValue().value ? f.props.id : false).filter(id => id)
    }
    showOrDownload = (e) => {
        let invoiceIds = this.getCheckedIds()
        if (invoiceIds.length === 1) {
            // TODO show PDF window.open(url:pdf)
        } else {
            // TODO get request with invoiceIds as params for downloading
        }
    }
    /** Download PDF button click handler */
    downloadPDFHandler = () => {
        let invoiceIds = this.getCheckedIds();
        if (invoiceIds.length > 0) {
            invoiceIds.forEach((id) => {
                let path = (this.state.invoices.find(i => i.id == id) || {}).path
                const url = SERVER_BASE.baseUrl + (path ? path.split('/rest/restData/public')[1] : '')

                window.open(url, "_blank")
            })
        } else {
            Notifications.show(LM.getString('chooseInvoice'), 'danger')
        }
    }

    runPostInvoiceBusinessId = () => {
        getCurrentUserData(
        ).then(
            (res) => {
                let businessId = res.details.business_id;
                postInvoiceBusinessId({ id: businessId }
                ).then(
                    (res) => {
                        this.setState({ isSubscribe: res.isSubscribe }, () => {
                            this.setState({ renderAll: true });
                        });
                    }
                ).catch(
                    (err) => {
                        Notifications.show(err, 'danger');
                    }
                )
            }
        ).catch(
            (err) => {
                Notifications.show(err, 'danger');
            }
        )
    }

    sendInvoiceByMail = () => {
        if (this.email.getValue().valid) {
            Loader.show()
            const data = {
                to: this.email.getValue().value,
                invoices: this.getCheckedIds()
            }
            postSendIvoiceByMail(data).then((res) => {
                Loader.hide()
                Notifications.show(LM.getString('invoiceSendedSuccessfully'), 'success')
                this.toggleSendMailModal()
            }).catch((err) => {
                Loader.hide()
                Notifications.show(LM.getString('fail'), 'danger')
            })
        }
    }

    setIsSubscribeTrue = () => {
        this.setState({ isSubscribe: true });
    }

    toggleSendMailModal = () => {
        this.setState({
            sendEmailModalOpen: !this.state.sendEmailModalOpen
        })
    }

    downloadAdobeReaderClick = () => {
        window.open("https://get.adobe.com/reader/", "_blank");
    }    

    componentDidMount() {
        this.runPostInvoiceBusinessId();
    }

    render() {
        return (
            <React.Fragment>
                <h3 className="color-page-header font-weight-light fs-26">{LM.getString("invoiceHeader")}</h3>
                <hr className={'d-md-none my-0'} />
                {/*<div className={''}></div>*/}
                <div className="main_border_padding">
                    <Collapse isOpen={this.state.renderAll}>
                        {/* {!this.state.isSubscribe ?
                        <> */}
                        <Collapse isOpen={!this.state.isSubscribe}>
                            < InvoiceMailingSubscription setIsSubscribeTrue={this.setIsSubscribeTrue} />
                        </Collapse>
                        {/* </>
                        :
                        <> */}
                        <Collapse isOpen={this.state.isSubscribe}>
                            <Row>
                                <Col>
                                    <span className="fs-17">{LM.getString("selectYear")}</span>
                                </Col>
                            </Row>
                            <Row className={'mx-md-0'}>
                                <Col md="4" sm="6" className={(LM.getDirection() === "rtl" ? "pr-md-0" : "pl-md-0")}>
                                    <InputTypeSelect {...this.state.selectYearProps} ref={(componentObj) => {
                                        this.selectYear = componentObj
                                    }} />
                                </Col>
                                <Col md="4" sm="6" className={(LM.getDirection() === "rtl" ? "pr-md-0" : "pl-md-0")}>
                                    <InputTypeSelect {...this.state.selectMonthProps} ref={(componentObj) => {
                                        this.selectMonth = componentObj
                                    }} />
                                </Col>
                                <Button className="border-radius-3 btn-lg btn-md-block font-weight-bold regularButton px-4 mx-3 mx-md-0 fs-17"
                                    id="invoiceShowInvoice"
                                    onClick={this.showInvoiceClick}
                                >
                                    {LM.getString("showInvoiceButton")}
                                </Button>
                            </Row>
                            <Collapse isOpen={this.state.showTable}>

                                {this.state.invoices[0] ?
                                    <div className="d-flex tagsManagementTableHeaders">
                                        <Col xs='3'>
                                            {LM.getString("date")}
                                        </Col>
                                        <Col xs='3'>
                                            {LM.getString("bill_number")}
                                        </Col>
                                        <Col xs='3'>
                                            {LM.getString("chooseInvoiceFiles")}
                                        </Col>

                                    </div> : <div className="d-flex tagsManagementTableHeaders">{LM.getString('invoicesNotFound')}</div>
                                }
                                {this.state.invoices.map((i) => {
                                    return (<div className="tagsManagementTableRow align-items-center d-flex flex-wrap">
                                        <Col xs='3'>
                                            {i.invoice_date}
                                        </Col>
                                        <Col xs='3'>
                                            {i.invoice_number}
                                        </Col>
                                        <InputTypeCheckBox id={i.id} ref={(componentObj) => { this.fields[i.id] = componentObj }}></InputTypeCheckBox>
                                    </div>)
                                })}
                            </Collapse>
                            <Modal isOpen={this.state.sendEmailModalOpen} >
                                <ModalHeader toggle={this.toggleSendMailModal}></ModalHeader>
                                <ModalBody>

                                    <Row  >
                                        <Col sm="8">
                                            <InputTypeEmail
                                                placeholder={'Example: mymail@google.com'}
                                                required={true}
                                                title={LM.getString("enterMail") + ":"}
                                                id={"invoiceMailingSubscription_Email"}
                                                ref={(componentObj) => { this.email = componentObj }}
                                            />
                                        </Col>
                                    </Row>

                                </ModalBody>
                                <ModalFooter>
                                    <Button className='regularButton m-2' onClick={this.sendInvoiceByMail}>{LM.getString("send")}</Button>{' '}
                                    <Button color="secondary" onClick={this.toggleSendMailModal}>{LM.getString("cancel")}</Button>
                                </ModalFooter>
                            </Modal>

                            {this.state.invoices.length > 0 && <Row>
                                <Button style={{ margin: '10px' }} className="border-radius-3 btn-lg btn-md-block font-weight-bold regularButton px-4 fs-17"
                                    id="invoiceSendInvoice"
                                    onClick={this.sendInvoiceClick}>{LM.getString("sendInvoiceButton")}</Button>

                                <Button style={{ margin: '10px' }} className="border-radius-3 btn-lg btn-md-block font-weight-bold regularButton px-4 fs-17" onClick={this.downloadPDFHandler}> {LM.getString("download")} </Button></Row>}
                        </Collapse>
                        {/* </>
                    } */}
                    </Collapse>

                </div >
                <Row className="justify-content-end mt-3">
                    <Col sm={{ size: "auto" }}>
                        <h6 className="invoiceSmallText invoiceSmallTextColor"
                            style={{ paddingTop: "auto", paddingBottom: "auto" }}>{LM.getString("pdfMessage")}</h6>
                    </Col>
                    <Col sm={{ size: "auto", offset: "0.5" }}>
                        <Button className="invoiceSmallText invoiceSmallButton" id="invoiceDownloadPdf"
                            onClick={this.downloadAdobeReaderClick}>
                            <Row>
                                <Col sm={{ size: "auto" }} className="invoicePaddingEnd">
                                    {LM.getString("pdfDownload")}
                                </Col>
                                <Col sm={{ size: "auto" }} className="invoicePaddingStart">
                                    <ReactSVG src={pdfSVG} />
                                </Col>
                            </Row>
                        </Button>
                    </Col>
                </Row>
            </React.Fragment >
        );
    }
}
