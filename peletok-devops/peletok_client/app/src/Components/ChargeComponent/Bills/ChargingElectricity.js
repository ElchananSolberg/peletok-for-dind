
import React, { Component } from 'react';
import { LanguageManager as LM } from '../../LanguageManager/Language'
import { Button, Container, Row, Col } from 'reactstrap';
import './ElectricityCharge.css';
import InputTypeNumber from '../../InputUtils/InputTypeNumber';
import IEClogo from '../../../Assets/Logos/BillsLogos/electricCo-logo.svg'
import { ChargingElectricityBillAccountPost, getProducts } from '../../DataProvider/DataProvider';
import { Notifications } from '../../Notifications/Notifications';
import { SuccessModalWindow } from './SuccessModalWindow';
import InputTypePhone from '../../InputUtils/InputTypePhone';
import checkMarkImage from '../../../Assets/Icons/MenuIcons/check-circle.svg';
import { Loader } from '../../Loader/Loader';
import { UserDetailsService } from '../../../services/UserDetailsService'



let stringMessage;

export class ChargingElectricity extends Component {

    /**
     * TODO to add inner onChanges which will work with state
     * and to make a validation process
     *
     */
    constructor(props) {
        super(props)

        this.state = {
            modal: false,
            successMessage: '',
            billsPaymentObj: undefined,
            transactionId: 0
        }
        this.confirm = this.confirm.bind(this);
        this.total = this.total.bind(this);

    }
    toggle = () => {
        this.setState({
            modal: !this.state.modal
        })
    }

    componentDidMount() {
        getProducts(this.props.supplierId).then(
            (res) => {

                this.setState({ billsPaymentObj: res });
            }
        ).catch(
            (err) => {
                Notifications.show(err, 'danger');
            }
        )
    }

    confirm() {
        let sum = this.state.billsPaymentObj.bills_payment[0].max_payment + " " + LM.getString("NIS")
        if ( this.moneyToCharge.getValue().value > this.state.billsPaymentObj.bills_payment[0].max_payment) {
            Notifications.show(LM.getString('thisDealIsLimitedTo') + sum, 'danger')
        }
        else if (this.contractNumberRef.getValue().value != '' && this.moneyToCharge.getValue().value != '' && this.TotalPayment.getValue().value != '' && this.TelnumTitle.getValue().value != ''
        ) { Loader.show();
            let sentIce = {

                contractNumber: this.contractNumberRef.getValue().value,
                price: this.moneyToCharge.getValue().value,
                itemId: this.state.billsPaymentObj.bills_payment[0].id
            }
            

            ChargingElectricityBillAccountPost(sentIce).then((response) => {
                Loader.hide();
                UserDetailsService.getUserDetails()

                if (response && response.success) {

                    stringMessage = response.success.replace(/ /g, '-');
                    stringMessage = stringMessage.replace(/\*/g, '');
                    stringMessage = stringMessage.replace(/\#/g, '');
                    stringMessage = `cod:*${stringMessage}#`;

                    this.setState({
                        successMessage: stringMessage,
                        transactionId: response.currentTransaction
                    }, () => (

                        this.toggle()
                    ))
                }
                else {
                }
            }
            ).catch(
                function (error) {
                    Loader.hide();
                    if (error && error.response && error.response.data && error.response.data.error) {
                        if (error.response.data.error === 'ארעה שגיאה. לפרטים ,אנא פנה למנהל המערכת ') {
                            Notifications.show(LM.getString("errorMsg"), 'danger');
                        }
                        else {
                            Notifications.show(error.response.data.error, 'danger');
                        }
                    }
                    else {

                        Notifications.show(LM.getString("unrecognizedError"), 'danger')
                    }
                }
            );
        } else {
            Notifications.show(LM.getString("enterAllFields"), 'danger')
        }
    }
    total() {
        this.TotalPayment.setValue(this.moneyToCharge.getValue().value)
    }
    render() {
       
        return (
            <Container className='border'>
                <Row >
                    <Col className="logo">
                        <img src={IEClogo} alt='' />
                    </Col>
                </Row>
                <Row className='my-3 m-2' >
                    <Col sm='12' className='headerCol'>
                        {LM.getString("chargeElectricityHeader")}
                    </Col>
                </Row>

                <Row >
                    <Col sm='3'>
                    </Col>
                    <Col>
                        <InputTypeNumber
                            id='contractNumberInput'
                            title={LM.getString("contractNumber")}
                            required='true'
                            ref={(componentObj) => { this.contractNumberRef = componentObj }} />

                        <InputTypeNumber
                            id='moneyToCharge'
                            title={LM.getString("moneyToCharge")}
                            required='true'
                            onChange={this.total}
                            ref={(componentObj) => { this.moneyToCharge = componentObj }} />

                        <InputTypeNumber
                            id='TotalPayment'
                            title={LM.getString("TotalamountPayment")}
                            disabled='true'
                            required='true'
                            ref={(componentObj) => { this.TotalPayment = componentObj }} />

                        {<p className='fs-22 font-weight-bold' >{LM.getString("Commission0")}</p>}

                        <InputTypePhone
                            id='supplierTelnumTitle'
                            title={LM.getString("supplierTelnumTitle")}
                            required='true'
                            ref={(componentObj) => { this.TelnumTitle = componentObj }} />
                        <Button className='mx-0 p-1' size='lg' id='electricityChargeButton' onClick={this.confirm}>
                            {LM.getString("confirm")}
                        </Button>
                    </Col>
                    <Col sm='3'>
                    </Col>
                </Row>
                <Row >
                    <Col sm='12' className='BottomCol border-radius-3 p-3 payment-note mt-4 mb-0 fs-26'>
                        {LM.getString("electricBottom")}
                    </Col>
                </Row>
                <SuccessModalWindow
                    printTransactionId={this.state.transactionId}
                    isOpen={this.state.modal}
                    className={this.props.className}
                    toggle={this.toggle}
                    headerToggle={this.toggle}
                    productImage={IEClogo}
                    validationImage={checkMarkImage}
                    cancelClick={this.toggle}
                    printClick={this.toggle}
                    modalMessage={this.state.successMessage}
                    messageDivClassName='fs-17'
                />
            </Container>)
    }
}