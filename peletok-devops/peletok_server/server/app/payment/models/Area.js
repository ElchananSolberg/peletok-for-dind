const BaseModel = require("../../BaseModel");
const DataTypes = require("sequelize").DataTypes;

/**
 * Area model
 */
class Area extends BaseModel {


    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            name_HE: {
                type: DataTypes.STRING(256),
                allowNull: true
            },
            name_EN: {
                type: DataTypes.STRING(256),
                allowNull: true
            },
            name_AR: {
                type: DataTypes.STRING(256),
                allowNull: true
            },
        };
        this.prefix = "PAY";
        this.author = true;
    }

    /**
     * creates associate for area model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.hasMany(models.city.model, { as: "City"});
    }
}

module.exports = new Area();