import React, {Component} from 'react';
import { PeletalkRouter } from './Components/PeletalkRouter/PeletalkRouter'
import './App.css';
import { withCookies } from 'react-cookie';
import { Loader } from './Components/Loader/Loader'
import { SelectedSeller } from './Components/SelectedSeller/SelectedSeller'
import { Notifications } from './Components/Notifications/Notifications'
import { Confirm } from './Components/Confirm/Confirm'

class App extends Component {
    render() {
        return (
            <div className={"body"}>
                <Notifications />
                <Confirm />
                <Loader />
                <SelectedSeller />
                <PeletalkRouter cookies={this.props.cookies}/>
            </div>
        )
    }
}

export default withCookies(App);
