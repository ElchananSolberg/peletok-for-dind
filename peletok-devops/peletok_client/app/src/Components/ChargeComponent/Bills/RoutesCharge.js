import React, { Component } from 'react'
import { Button, Container, Row, Col } from 'reactstrap';
import './RoutesCharge.css'
import { LanguageManager as LM } from '../../LanguageManager/Language';
import InputTypePhone from '../../InputUtils/InputTypePhone';
import InputTypeNumber from '../../InputUtils/InputTypeNumber';
import { SuccessModalWindow } from './SuccessModalWindow';
import { routesChargePost, getProducts } from '../../DataProvider/DataProvider';
import { PrepaidProduct } from '../Prepaid/PrepaidProduct';
import { Notifications } from '../../Notifications/Notifications';
import { Loader } from '../../Loader/Loader'
import { UserDetailsService } from '../../../services/UserDetailsService';


const ROUTE_SIX_ID = '187';

export class RoutesCharge extends Component {

    /**
     *TODO make inner validation for id input    
     *
     */

    constructor(props) {
        super(props)

        this.state = {
            modal: false,
            modalMessage: LM.getString("successModalMessage"),
            modalWindowImage: undefined,
            modalMessageClassName: false,
            billNumber: undefined,
            amount: undefined,
            bills: [],
            billsPaymentObj: undefined,
            finalCommission: '',
            transactionId: 0
        }
    }

    toggle = () => {
        this.setState({

            modal: !this.state.modal
        })
    }

    componentDidMount() {
        getProducts(this.props.supplierId).then(
            (res) => {

                this.setState({ billsPaymentObj: res, finalCommission: res.bills_payment[0].final_commission });
            }
        ).catch(
            (err) => {
                Notifications.show(err, 'danger');
            }
        )
    }

    idValidationFunction(value) {

        let IDnum = String(value);

        if ((IDnum.length > 9) || (IDnum.length < 5)) {
            return false;
        }
        else if (isNaN(IDnum)) {
            return false;
        }
        let counter = 0, incNum;
        for (var i = 0; i < 9; i++) {
            incNum = Number(IDnum.charAt(i));
            incNum *= (i % 2) + 1;
            if (incNum > 9)
                incNum -= 9;
            counter += incNum;
        }
        if (counter % 10 === 0) {

            return true;
        }
        return false;
    }
    /**
     * TODO after server merge change "amount" to "price"
     */
    sendPostHandler = () => {
        let validation = this.IDinput.getValue().valid && this.invoiceInput.getValue().valid && this.amountInput.getValue().valid && this.totalAmountInput.getValue().valid && this.phoneNumberInput.getValue().valid;
        this.setState({
            chargeButtonDisabled: !validation,
            billNumber: this.invoiceInput.getValue().value,
            amount: this.totalAmountInput.getValue().value,
        })
        let data = {
            itemId: this.state.billsPaymentObj.bills_payment[0].id,
            customerId: this.IDinput.getValue().value,
            billNumber: this.invoiceInput.getValue().value,
            price: this.totalAmountInput.getValue().value,
            phoneNumber: this.phoneNumberInput.getValue().value
        }
        let supplierId = this.props.supplierId;
        let sum = this.state.billsPaymentObj.bills_payment[0].max_payment + " " + LM.getString("NIS")
        if (Number(this.state.billsPaymentObj.bills_payment[0].max_payment < this.totalAmountInput.getValue().value)) {
            Notifications.show(LM.getString('thisDealIsLimitedTo') + sum, 'danger')
        }
        else if (validation) {
            Loader.show()
            routesChargePost(supplierId, data).then(

                (res) => {
                    if (Object.keys(res)[0] == 'success') {
                        Loader.hide()
                        UserDetailsService.getUserDetails()
                        Notifications.show(LM.getString("successModalMessage") + ' ' + res.success, 'success')
                        if (Object.keys(res)[1] == 'bills') {

                            this.setState({ bills: res.bills })
                            this.toggle()
                        }
                    }
                }).catch((error) => {
                    Loader.hide()
                    if (error && error.response && error.response.data && error.response.data.error) {
                        Notifications.show(error.response.data.error, 'danger')
                    }
                    else {
                        Notifications.show(LM.getString("connectionErrorModalMessage"), 'danger')
                    }
                })
        }
    }

    createInvoiceButtons = () => {

        let buttons = this.state.bills.map((button) => {
            return (
                <Container>
                    <Row className='invoiceRow var(--grayRow) m-2'>
                        <Col sm='4' className='mt-3 fs-17'> {button.invoiceNum}</Col>
                        <Col sm='4' className='mt-3 fs-17'>{button.includeVat}</Col>
                        <Col sm='4'>
                            <Button onClick={() => { this.newInvoicePayment(button.includeVat, button.invoiceNum) }}
                                className='invoiceBtn regularButton m-2' size='sm'>{LM.getString("payment")}
                            </Button></Col>
                    </Row>
                </Container>)
        })

        return buttons;
    }

    newInvoicePayment = (amount, invoice) => {
        let data = {
            itemId: this.state.billsPaymentObj.bills_payment[0].id,
            customerId: this.IDinput.getValue().value,
            billNumber: invoice,
            price: Number(amount + this.state.finalCommission),
            phoneNumber: this.phoneNumberInput.getValue().value
        }
        this.setState({ amount: amount, billNumber: invoice })
        let supplierId = this.props.supplierId;
        if (data.price < Number(this.state.billsPaymentObj.bills_payment[0].max_payment)) {
            routesChargePost(supplierId, data).then(

                (res) => {
                    this.setState({
                        bills: res.bills,
                        transactionId: res.currentTransaction
                    })
                    this.createInvoiceButtons(...this.state.bills)
                }).catch(

                    (err) => {
                        Notifications.show(err.response.data.err, 'danger')
                    }
                )
        }
        else {
            let sum = this.state.billsPaymentObj.bills_payment[0].max_payment + " " + LM.getString("NIS")
            Notifications.show(LM.getString('thisDealIsLimitedTo') + sum, 'danger')
        }
    }
    amountValidation = (value) => { return (value <= 0 ? false : true) }

    totalAmountHandler = (e) => { return e.target.value > 0 ? this.totalAmountInput.setValue(Number(e.target.value) + this.state.finalCommission) : this.totalAmountInput.setValue(Number(0)) }

    render() {

        return (
            <Container className='routesChargeContainer py-2'>
                <Row>
                    <Col id='routesHeader'>
                        {this.props.header}
                    </Col>
                    <Col className='imageCol'>
                        <img className='routesChargeImage' src={this.props.image} alt='' />
                    </Col>
                </Row>
                <Row >
                    <Col sm='4'>
                        <InputTypeNumber id='IdInput'
                            title={LM.getString("idOrCompanyNumber")}
                            required='true'
                            ref={(objReference) => { this.IDinput = objReference }}
                            validationFunction={this.idValidationFunction}
                            notValidMessage={LM.getString("invalidIdNumber")}
                        />
                    </Col>
                    <Col className="InvoiceInputMargin" sm='4'>
                        <InputTypeNumber id='invoiceInput'
                            title={LM.getString("invoiceNumber")}
                            required='true'
                            maxLength="6"
                            minLength="6"
                            ref={(objReference) => { this.invoiceInput = objReference }}
                        />
                    </Col>
                </Row>
                <Row >
                    <Col sm='4'>
                        <InputTypeNumber id='RecieptAmountInput'
                            title={LM.getString("receiptAmount")}
                            required='true'
                            onChange={this.totalAmountHandler}
                            validationFunction={this.amountValidation}
                            notValidMessage={LM.getString("incorrectAmount")}
                            ref={(objReference) => { this.amountInput = objReference }}
                        />
                    </Col>
                    <Col sm='4'>
                        <InputTypeNumber id='TotalAmountInput'
                            title={LM.getString("totalPayment")}
                            disabled='true'
                            ref={(objReference) => { this.totalAmountInput = objReference }}
                        />
                    </Col>
                </Row>
                <Row className='mx-1 my-2 commissionNotesRow'>
                    <Col>
                        {LM.getString('commissionPay')} {this.state.finalCommission} {LM.getString("shekel")}
                    </Col>
                </Row>
                <Row>
                    <Col sm='4'>
                        <InputTypePhone id='PhoneNumberInput'
                            title={LM.getString("cellularNumber")}
                            maxLength="10"
                            minLength="10"
                            required='true'
                            ref={(objReference) => { this.phoneNumberInput = objReference }}
                        />
                    </Col>
                </Row>
                <Row className="mt-3 mx-1">
                    <Button id={`${this.props.header}Button`} color='primary' onClick={this.sendPostHandler}>{LM.getString("charge")}</Button>
                    <SuccessModalWindow
                        printTransactionId={this.state.transactionId}
                        isOpen={this.state.modal}
                        className={this.props.className}
                        toggle={this.toggle}
                        modalMessage={this.state.modalMessage}
                        bottomBorderForRoutesCharge='bottomBorderForRoutesCharge'
                        messageDivClassName={this.state.modalMessageClassName ? 'messageGreenColor' : 'messageRedColor'}
                        invoiceMessages={true}
                        paidCard={
                            <div className={'boxShadow border-r-5 mx-1 '} style={{ backgroundColor: '#00BBB8' }}>
                                <PrepaidProduct
                                    product={{
                                        name: this.props.supplierId == ROUTE_SIX_ID ? LM.getString("routeSixFee") : LM.getString("carmelTunnelFee"),
                                        price: this.state.amount,
                                        description: this.state.successMessage
                                    }}
                                    showMoreInfo={false}
                                />
                            </div>}
                        additionalInvoices={this.state.bills.length > 0 ? LM.getString("additionalInvoices") : null}
                        invoiceButtons={this.createInvoiceButtons()}
                        cancelClick={this.toggle}
                        printClick={this.toggle}
                        validationImage={this.state.modalWindowImage}
                    />
                </Row>
            </Container>)
    }
} 
