import React, { Component } from 'react';
import './InputUtils.css';
import { FormFeedback, Input, Row, Col } from 'reactstrap';
import PlacesAutocomplete from 'react-places-autocomplete';

import { Tooltip } from './Tooltip';

import { LanguageManager as LM } from '../LanguageManager/Language'


class InputTypeAddress extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            validationFuncs: [
                {
                    /**  Validates the value is valid to the function sent in  this.props.validationFunction */
                    f: this.props.validationFunction || (() => (true)),
                    msg: this.props.notValidMessage
                },
                {
                    f: this.innerValidationFunction,
                    msg: LM.getString("textInnerValidationFunctionMsg") + this.props.minLength,
                },
                {
                    f: this.isRequiredValidation,
                    msg: LM.getString("isRequiredValidationFunctionMsg"),
                }
            ],
            errorMessage: '',
        };
    }

    handleChange = value => {
        this.setState({ value });
    };

    handleSelect = value => {
        this.setState({value})
    };

    runValidation = () => {
        let summarizingValidationResult = (!this.props.required && !this.state.value) || this.state.validationFuncs.every((validation) => validation.f(this.state.value));
        if (!summarizingValidationResult) {
            let messages = [];
            this.state.validationFuncs.forEach((validation) => {
                if (!validation.f(this.state.value)) {
                    messages.push(validation.msg);
                }
            });
            this.setState(({ errorMessage: messages.join(`\n`) }));
        } else {
            this.setState(({ errorMessage: '' }));
        }
        return summarizingValidationResult;
    };

    /** If this.props.required is true validates the value was entered. */
    isRequiredValidation = (value) => {
        if (!value && this.props.required) {
            return false;
        }
        return true;
    };

    /** Validates the value's length is not less than this.props.minLength */
    innerValidationFunction = (value) => {
        let minLength = this.props.minLength;
        if (value.length < minLength) {
            return false;
        }
        return true;
    };

    getValue = () => {
        let summarizingValidationResult = this.runValidation();
        return { valid: summarizingValidationResult, value: this.state.value };
    };

    setValue(newVal) {
        this.setState({
            value: newVal
        })
    }


    render() {
        let disabled = this.props.disabled;
        let maxLength = this.props.maxLength;
        let placeholder = this.props.placeholder;
        let id = this.props.id.replace(/\s+/g, '');
        let title = this.props.title;
        let tooltipText = this.props.tooltip;

        return (
            <PlacesAutocomplete
                value={this.state.value}
                onChange={this.handleChange}
                onSelect={this.handleSelect}
            >
                {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                        <>
                            <Row>
                                <Col
                                    className={"d-flex justify-content-between line-height-1 " + (title ? 'py-1' : '')}>
                                    {title ?
                                        <div className="fs-17 white-space">{title}</div>
                                        : <></>
                                    }
                                    {tooltipText ?
                                        <Tooltip tooltipText={tooltipText} id={id}/>
                                        : <></>
                                    }
                                </Col>
                            </Row>
                            <Row className={'mt-auto position-relative'}>
                                <Col>
                                    {
                                        /** if there are any validation issues the input will get a prop "invalid" and will change a border color to red */
                                        < Input type="text" id={`InputTypeAddress${id}`}
                                                placeholder={placeholder}
                                                maxLength={maxLength}
                                                disabled={disabled} invalid={this.state.errorMessage.length !== 0}
                                                className={(LM.getDirection() === "rtl" ? "rtl" : "")}
                                                style={this.props.outerStyle}
                                                {...getInputProps({onBlur: this.runValidation})}

                                        />
                                    }
                                    {/** if there are any validation issues bellow the input will be the error message */}
                                    <FormFeedback className='ws-pre'>{this.state.errorMessage}</FormFeedback>
                                </Col>
                                {this.state.value.length > 0 && <div className="autocomplete-dropdown-container w-100">
                                    <div>
                                        {loading && <div className="loading">Loading...</div>}
                                        {suggestions.map(suggestion => {
                                            const className = suggestion.active
                                                ? 'suggestion-item--active'
                                                : 'suggestion-item';
                                            // inline style for demonstration purpose
                                            const style = suggestion.active
                                                ? {backgroundColor: '#fafafa', cursor: 'pointer'}
                                                : {backgroundColor: '#ffffff', cursor: 'pointer'};
                                            return (
                                                <div
                                                    {...getSuggestionItemProps(suggestion, {
                                                        className,
                                                        style,
                                                    })}
                                                >
                                                    <div>{suggestion.description}</div>
                                                </div>
                                            );
                                        })}
                                    </div>
                                </div>
                                }
                            </Row>
                        </>

                )}
            </PlacesAutocomplete>
        );
    }
}
export default InputTypeAddress;