var BaseModel = require('../../BaseModel.js');
const DataTypes = require("sequelize").DataTypes;


/**
 * Language model
 */
class Language extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            code: {
                type: DataTypes.STRING(5),
                primaryKey: true,
                allowNull: true
            },
            code_parent: {
                type: DataTypes.STRING(5),
                allowNull: true
            },
            name_english: {
                type: DataTypes.STRING(32),
                allowNull: true
            },
            name_native: {
                type: DataTypes.STRING(32),
                allowNull: true
            }


        };
        this.prefix = 'USR';
    }
    /**
     * creates associate for languages model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.hasMany(models.user.model, {as: 'User' });
        this.model.hasMany(models.userStatusMessage.model, {as: 'UserStatusMessage' });
        this.model.hasMany(models.productDetails.model, {as: 'productDetails' });
        this.model.hasMany(models.categoryName.model, {as: 'CategoryName' });
        this.model.hasMany(models.bannerLanguage.model, {as: 'BannerLanguage' });

    }


}
//exporting instance
module.exports = new Language();