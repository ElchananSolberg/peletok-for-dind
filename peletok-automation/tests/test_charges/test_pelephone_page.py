from tests.base_project_test_class import *


class TestPelephone(BaseProjectTestClass):

    def login_and_get_pelephone(self):
        self.login()
        self.driver.tools.wait_and_click(HomePage.pelephone_icon)

    def test_012mobile_click_on_back_button(self):
        self.login_and_get_pelephone()
        self.click_on_back_botton()
        self.after_the_click_back_button()
        self.remove_logs_dir = True

    def login_and_click_pelephone_icone_and_click_to_pelephone_virtual_card(self):
        self.login_and_get_pelephone()
        self.driver.wait.wait_for_element_to_be_present(VerificationPage.img_logo_pelephone)
        self.driver.tools.wait_and_click(CompanySelectTypeCardsPage.virtual_card)
        self.driver.wait.wait_for_element_to_be_present(VerificationPage.price_pelephone_easy_99)
        self.remove_logs_dir = True

    def test_easy_75(self):
        self.login_and_click_pelephone_icone_and_click_to_pelephone_virtual_card()
        self.pay_for_calling_card_vc(75, "95/virtual/12")
        self.remove_logs_dir = True

    def test_easy_99(self):
        self.login_and_click_pelephone_icone_and_click_to_pelephone_virtual_card()
        self.pay_for_calling_card_vc(99, "95/virtual/13")
        self.remove_logs_dir = True

    def test_check_alert_favorite_tag_and_wait_btn_annul_pelephone(self):
        self.login_and_click_pelephone_icone_and_click_to_pelephone_virtual_card()
        self.click_favorite_tag_and_wait_btn_annul()
        self.remove_logs_dir = True

    def test_check_alert_favorite_tag_and_wait_btn_confirm_pelephone(self):
        self.login_and_click_pelephone_icone_and_click_to_pelephone_virtual_card()
        self.click_favorite_tag_and_wait_btn_confirm()
        self.remove_logs_dir = True

    def test_check_if_add_favorite_cards_pelephone(self):
        self.login_and_click_pelephone_icone_and_click_to_pelephone_virtual_card()
        self.click_favorite_tag_and_check_if_add_moadafim()
        self.remove_logs_dir = True