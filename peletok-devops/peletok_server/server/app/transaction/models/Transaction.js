const BaseModel = require('../../BaseModel');
const DataTypes = require("sequelize").DataTypes;

/**
 * Transaction model
 */
class Transaction extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            transaction_start_timestamp: {
                type: DataTypes.DATE,
                allowNull: true
            },
            transaction_end_timestamp: {
                type: DataTypes.DATE,
                allowNull: true
            },

            // consumer price
            price: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            price_not_including_vat: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },

            // business price (consumer price minus business profits)
            business_price: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            business_price_not_including_vat: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            business_profit: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            business_profit_not_including_vat: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            business_profit_vat: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            business_balance: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
    
       // distributor price (consumer price minus business profits)
            distributor_price: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            distributor_price_not_including_vat: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            distributor_profit: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            distributor_profit_not_including_vat: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            distributor_profit_vat: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            distributor_balance: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },


   
            payment: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            phone_number: {
                type: DataTypes.STRING,
                allowNull: true
            },
            action: {
                type: DataTypes.STRING,
                allowNull: true
            },
            failure_status: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            invoice_number: {
                type: DataTypes.STRING,
                allowNull: true
            },
            transaction_type: {
                    type: DataTypes.INTEGER
                },
            appeal_id: {
                type: DataTypes.STRING,
                allowNull: true
            },
            canceled_by_api: {
                type: DataTypes.BOOLEAN,
                allowNull: true
            },
            is_canceled:{
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false
            },
            is_cancelable:{
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: true
            },
            supplier_balance:{
                type: DataTypes.DOUBLE,
                allowNull: true,
            },
            cancel_process_status: {
                // Refers to the transaction that you want to cancel
                // (not necessarily the current transaction)
                type: DataTypes.ENUM(["NO", "AWAITING", "DONE", "REJECT"]),
                allowNull: false,
                defaultValue: "NO"
            }
        };
        this.prefix = "TRNSC";
        this.author = true;
        this.statusAuthor = true;
        this.statusField = true;


    }
    /**
     * creates associate for Transaction model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.user.model, { as: 'User' })
        this.model.belongsTo(models.terminal.model, { as: 'Terminal' })
        this.model.belongsTo(models.product.model, { as: 'Product' })
        this.model.hasMany(models.transactionLog.model, {as: 'TransactionLog' });
        this.model.hasMany(models.transaction.model, {as: 'Child', foreignKey: 'parent_transaction_id' });
        this.model.hasMany(models.manualCard.model, {as: 'ManualCard' });
        this.model.belongsTo(models.transaction.model, { as: 'Parent', foreignKey: 'parent_transaction_id' });
        this.model.belongsTo(models.supplier.model, { as: 'Supplier' });
        this.model.belongsTo(models.hashavshevetExport.model, { as: 'HashavshevetExport' });
        this.model.belongsTo(models.business.model, { as: 'Business' });
        this.model.hasMany(models.transactionData.model, {as: 'transactionData' });


    }
}

module.exports = new Transaction();