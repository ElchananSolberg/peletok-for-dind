from selenium.webdriver.common.by import By


class SuppliersProfilePage:
    add_new_profile_btn = (By.ID, 'suppliersProfileAddNewProfileButton')
    suppliers_profile_input = (By.ID, 'InputTypeSearchListSuppliersProfile_SelectSuppliersProfile')
    job_permission_input = (By.ID, 'InputTypeSearchListallowedToRole')
    clear_input_choice = (By.CLASS_NAME, 'css-19bqh2r')
    choice_in_input = (By.CLASS_NAME, 'css-12jo7m5')
    clear_btn_of_table = (By.XPATH, '//div[@class="mr-auto"]//button[@class="CommissionProfileDarkButton'
                                    ' CommissionProfileSmallButton btn btn-secondary"]')
    checkall_btn_of_table = (By.XPATH, '//div[@class="mr-auto"]//button[@class="commissionProfileButton'
                                       ' CommissionProfileSmallButton btn btn-secondary"]')
    all_check_box_in_table = (By.XPATH, '//*[@type="checkbox"]')
    check_one_checkbox = (By.ID, 'TableRowID{}CheckBox_checkbox')
    save_btn = (By.ID, 'suppliersProfileSaveButton')
    new_profile_input = (By.ID, 'InputTypeTextSuppliersProfile_ProfileName')
    cancel_btn = (By.ID, 'suppliersProfileCancelButton')
    marketers_link = (By.ID, 'resellersLink')
    distributor_input = (By.ID, 'InputTypeSearchListSearchBusinessSelectDistributor')
    marketers_input = (By.ID, 'InputTypeSearchListSearchBusinessSelectSeller')
    suppliers_profile_input_in_marketers = (By.ID, 'InputTypeSearchListBusinessSuppliersProfile')
    profit_profile_input_in_marketers = (By.ID, 'InputTypeSearchListBusinessProfitPercentageProfile')
    commission_profile_input_in_marketers = (By.ID, 'InputTypeSearchListBusinessCommissionsProfile')
    save_btn_on_marketers = (By.ID, 'businessButtonProceed')
    x_btn_on_commercial = (By.XPATH, '//h5[@class="myX" and text()="x"]')
    all_companies_on_main = (By.CSS_SELECTOR, 'div.px-2 h1.provider-card-text')
