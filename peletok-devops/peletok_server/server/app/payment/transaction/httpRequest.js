var https = require('https');
var logs = require('../../logger')
const {MongoLogger} = require('../../mongoLogger')



/***
 * Returns html page or session (Depending on the getSession)
 * @param options request options
 * @param getSession if true returns the session only else returns the body
 * @param postData Data to post
 * @returns {Promise}
 */
function httpRequest(options, getSession, postData = null) {
    return new Promise(
        function (resolve, reject) {
            const req = https.request(options,
                function (res) {
                    
                    if (res.statusCode < 200 || res.statusCode >= 400) {
                        return reject(new Error('statusCode=' + res.statusCode));
                    }

                    var chunks = [];

                    res.on("data", function (chunk) {
                        chunks.push(chunk);
                    });

                    res.on("end", function (chunk) {
                        if (getSession) {
                            resolve(res.headers['set-cookie'])
                        } else {
                            body = Buffer.concat(chunks).toString();
                            MongoLogger.logHttpRequestError({
                                request: options,
                                response: body ,
                            })
                            resolve(body);
                        }
                    });

                    res.on("error", function (error) {
                        MongoLogger.logHttpRequestError({
                            request: options,
                            response: error ,
                        })
                        logs.error(error);
                    });
                });
            if (postData) {
                req.write(postData);
            }
            req.end();
        })
}
module.exports = httpRequest