FROM python:3.6

RUN apt-get update && apt-get install -y \
    software-properties-common \
    unzip \
    curl \
    xvfb

# Chrome browser to run the tests
RUN curl https://dl-ssl.google.com/linux/linux_signing_key.pub -o /tmp/google.pub \
    && cat /tmp/google.pub | apt-key add -; rm /tmp/google.pub \
    && echo 'deb http://dl.google.com/linux/chrome/deb/ stable main' > /etc/apt/sources.list.d/google.list \
    && mkdir -p /usr/share/desktop-directories \
    && apt-get -y update && apt-get install -y google-chrome-stable
# Disable the SUID sandbox so that chrome can launch without being in a privileged container
RUN dpkg-divert --add --rename --divert /opt/google/chrome/google-chrome.real /opt/google/chrome/google-chrome \
    && echo "#!/bin/bash\nexec /opt/google/chrome/google-chrome.real --no-sandbox --disable-setuid-sandbox \"\$@\"" > /opt/google/chrome/google-chrome \
    && chmod 755 /opt/google/chrome/google-chrome

# Chrome Driver
RUN mkdir -p /opt/selenium \
    && curl http://chromedriver.storage.googleapis.com/$(curl https://chromedriver.storage.googleapis.com/LATEST_RELEASE)/chromedriver_linux64.zip -o /opt/selenium/chromedriver_linux64.zip \
    && cd /opt/selenium; unzip /opt/selenium/chromedriver_linux64.zip; rm -rf chromedriver_linux64.zip; ln -fs /opt/selenium/chromedriver /usr/local/bin/chromedriver;


# set xvfb display size
ENV DISPLAY_WIDTH 1080
ENV DISPLAY_HEIGHT 1920
#RUN export DISPLAY=:20
#RUN Xvfb :20 -screen 0 "$DISPLAY_WIDTH"x"$DISPLAY_HEIGHT"x16 &

ENV PYTHONUNBUFFERED 1
ENV RUNNING_ON_DOCKER True

ARG debug
ENV DEBUG=$debug

ARG base_url
ENV BASE_URL=$base_url

ARG headless=True
ENV HEADLESS=$headless

ARG test_option='test/'
ENV TEST_OPTION=$test_option

RUN mkdir /junit
RUN mkdir /selenium
RUN mkdir /selenium/logs-dir

COPY requirments.txt /selenium/requirments.txt
WORKDIR /selenium
RUN pip install -r requirments.txt
COPY import_pages.py /selenium/import_pages.py
COPY appium_selenium_driver /selenium/appium_selenium_driver
COPY pages /selenium/pages
COPY tests /selenium/tests

ENV DISABLE_CAPTURE=true

CMD bash -c "pytest -sv ${TEST_OPTIONS} --junitxml=/junit/result.xml --html=/html-reports/tests-report.html"