import React, { Component } from 'react';
import { Route, Link } from 'react-router-dom';
import { ElectricityChargeCancelation } from './ElectricityChargeCancelation';
import { ElectricityChargeCancelationConfirmation } from './ElectricityChargeCancelationConfirmation';
import './ElectricityChargeCancelation.css';
import { ElectricityCharge } from './ElectricityCharge';
import { Container, Row, Col, Button } from 'reactstrap';
import { LanguageManager as LM } from '../../../LanguageManager/Language';
import { SERVER_BASE } from '../../../DataProvider/DataProvider';
import { backToPreviousScreenHandler } from '../../../../utils/BackToPreviousScreenHandler'

export class RetrieveDetailsRenderer extends Component {

    retrieveDetails = () => {
        return (
            <Container>
                <Row className='fs-26 m-1'>{LM.getString("electricityCompany")}</Row>
                <div className='border'>
                    <div className='m-3 fs-17'>{LM.getString("retrieveDetails")}</div>
                    <Row className='my-4'>
                        <Col sm='3'>
                            <Link to={`${this.props.match.url}/RetrieveDetails`}>
                                <Button size='lg' className='regularButton fs-17 px-4 m-2'>{LM.getString("electricCompanyCharge")}</Button></Link>
                        </Col>
                        <Col sm='3'>
                            <Link to={`${this.props.match.url}/ElectricityChargeCancelation`}>
                                <Button size='lg' className='regularButton fs-17 px-5 my-2'>{LM.getString("electricityCharge")}</Button></Link>
                        </Col>
                        <Col className='imageCol imgPosition'>
                            <img className='ecImage' src={`${SERVER_BASE}/suppliersLogos/israel-electric.svg`} />
                        </Col>
                    </Row>
                </div>
            </Container>
        )
    }
    render() {
        return (
            <div>
                {backToPreviousScreenHandler(this.props.history)}
                <Route exact path={this.props.match.path} render={() => this.retrieveDetails()}></Route>
                <Route exact path={`${this.props.match.path}/RetrieveDetails`} render={(routeProps) => { return <ElectricityChargeCancelation {...routeProps} image={`${SERVER_BASE}/suppliersLogos/israel-electric.svg`} /> }}></Route>
        <Route exact path={`${this.props.match.path}/ElectricityChargeCancelation`} render={(routeProps) =>{return <ElectricityCharge {...routeProps} image={`${SERVER_BASE}/suppliersLogos/israel-electric.svg`} />}}></Route>
                <Route exact path={`${this.props.match.path}/RetrieveDetails/Confirm`} render={(routeProps) => {
                     return <ElectricityChargeCancelationConfirmation {...routeProps} image={`${SERVER_BASE}/suppliersLogos/israel-electric.svg`} /> }}>
                </Route>
            </div>
        );
    }
}
