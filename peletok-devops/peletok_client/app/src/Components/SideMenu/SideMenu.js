import React, { Component } from 'react';
import { MainMenu } from './MenuComponents/MainMenu';
import { CancelMenu } from './MenuComponents/CancelMenu';
import { SuppliersMenu } from './MenuComponents/SuppliersMenu';
import { ResellersMenu } from './MenuComponents/ResellersMenu';
import { ConfigMenu } from "./MenuComponents/ConfigMenu";
import { ReportMenu } from "./MenuComponents/ReportsMenu";
import { Route } from "react-router-dom";
import { BalanceDisplay } from "../BalanceDisplay/BalanceDisplay";
import Banner from '../AdBanner/Banner';
import { UserDetailsService } from '../../services/UserDetailsService'


/**
 * creates a Menu for the user display
 * TODO - check if can be generalized for all types of menus needed
 */
export class SideMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            balanceData: []
        }
    }

    /**
     * loads data from server whenever site loads the main screen
     */
    componentDidMount() {
        UserDetailsService.balance.subscribe(balance => {
          this.setState({ balanceData: balance})
        });
        UserDetailsService.getUserDetails()
    }

    render() {
        /**
         * renders the user Menu - with loading reports from server
         * TODO - language changes with user (might be implemented via route - e.g. address.com/en/Charge
         */
        let selectedMenu = <div style={{ textAlign: "center", color: "#FFFFFF" }}> NO MENU WAS FOUND </div>;
        switch (this.props.match.params.tab) {
            case 'main':
                selectedMenu = <MainMenu {...this.props} />;
                break;
            case 'cancel':
                selectedMenu = <CancelMenu {...this.props} />;
                break;
            case 'suppliers':
                selectedMenu = <SuppliersMenu {...this.props} />;
                break;
            case 'resellers':
                selectedMenu = <ResellersMenu {...this.props} />;
                break;
            case 'config':
                selectedMenu = <ConfigMenu {...this.props} />;
                break;
            case 'reports':
                selectedMenu = <ReportMenu {...this.props} />;
                break;
            default:
                selectedMenu = <div> Route Incorrect </div>;
        }
        return (
            <div className="sideMenu">
                {selectedMenu}
                <Route path={`${this.props.match.path}`}
                    render={((routeProps) => (<BalanceDisplay {...routeProps}
                        values={this.state.balanceData} />))} />
                <Banner height="8rem" />
            </div>
        )
    }
}