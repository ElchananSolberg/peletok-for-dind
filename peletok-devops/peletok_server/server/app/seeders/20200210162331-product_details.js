'use strict';
const moment = require('moment');

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    let dateNow = moment().toDate();
    let productDetails = [{
      id: 1,
      product_name: "Confidential Call ID 29.90",
      product_id: 1,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 2,
      product_name: "זיהוי שיחה חסויה 29.90",
      product_id: 1,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 3,
      product_name: "معرف مكالمة سرية 29.90",
      product_id: 1,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 4,
      product_name: "זיהוי שיחה חסויה 164.45",
      product_id: 3,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 5,
      product_name: "Confidential Call ID 164.45",
      product_id: 3,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 6,
      product_name: "معرف مكالمة سرية 164.45",
      product_id: 3,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 7,
      product_name: "easy 59",
      product_id: 11,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 8,
      product_name: "איזי 59",
      product_id: 11,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 9,
      product_name: "سهل 59",
      product_id: 11,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 10,
      product_name: "easy 75",
      product_id: 12,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 11,
      product_name: "איזי75",
      product_id: 12,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 12,
      product_name: "75",
      product_id: 12,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 13,
      product_name: "איזי 99",
      product_id: 13,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 14,
      product_name: "easy 99",
      product_id: 13,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 15,
      product_name: "99",
      product_id: 13,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 16,
      product_name: "ביגטוק טעינה 53 ש''ח",
      product_id: 994,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 17,
      product_name: "Big talk charge 53 nis",
      product_id: 994,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 18,
      product_name: "تهمة الحديث الكبير 53 شيكل",
      product_id: 994,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 19,
      product_name: "BigTock 29  Unlimited Unlimited up to 1000 minutes and 1000 SMS messages in all networks in Israel + 500MB surfing",
      product_id: 995,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 20,
      product_name: "ביגטוק 29 ללא הגבלה מקנה עד 1000דקות ו1000 הודעות SMSבכל הרשתות בארץ + 500MB גלישה",
      product_id: 995,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 21,
      product_name: "BigTock 29  غير محدود غير محدود ما يصل إلى 1000 دقيقة و 1000 رسالة نصية قصيرة في جميع الشبكات في إسرائيل + تصفح 500 ميجابايت",
      product_id: 995,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 22,
      product_name: "ביגטוק 50 ללא הגבלה מקנה עד 1500 דקות ו 1500 הודעות SMS בכל הרשתות בארץ + 1GB גלישה",
      product_id: 996,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 23,
      product_name: "BigTock 50  unlimited to 1500 minutes and 1500 SMS in all networks in Israel + 1GB surfing",
      product_id: 996,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 24,
      product_name: "BigTock 50  غير محدود إلى 1500 دقيقة و 1500 SMS في جميع الشبكات في إسرائيل + 1GB تصفح",
      product_id: 996,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 25,
      product_name: "BigTock 29  unlimited to 1000 minutes and 1000 SMS in all networks in Israel + 1GB surfing",
      product_id: 1024,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 26,
      product_name: "BigTock 29  غير محدود إلى 1000 دقيقة و 1500 SMS في جميع الشبكات في إسرائيل + 1GB تصفح",
      product_id: 1024,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 27,
      product_name: "ביגטוק 29 ללא הגבלה מקנה עד 1000 דקות ו 1000 הודעות SMS בכל הרשתות בארץ + 1GB גלישה",
      product_id: 1024,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 28,
      product_name: "ביגטוק 50 ללא הגבלה מקנה עד 1500 דקות ו 1500 הודעות SMS בכל הרשתות בארץ + 4 גלישה",
      product_id: 1025,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 29,
      product_name: "BigTock 50  غير محدود إلى 1500 دقيقة و 1500 SMS في جميع الشبكات في إسرائيل + 4 تصفح",
      product_id: 1025,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 30,
      product_name: "BigTock 50  unlimited to 1500 minutes and 1500 SMS in all networks in Israel + 4GB surfing",
      product_id: 1025,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 31,
      product_name: "ביגטוק 59 ש''ח ללא הגבלה בארץ (5000 דקות והודעות) + 15 גיגה גלישה",
      product_id: 1026,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 36,
      product_name: "10-minute calling to the whole world is valid for only one week from the day of loading",
      product_id: 103, language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 32,
      product_name: "BigTock 59 NIS Unlimited in Israel (5000 minutes and messages) + 15 GB surfing",
      product_id: 1026,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 34,
      product_name: "כרטיס 10 דק חיוג לכל העולם בתוקף שבוע  בלבד מיום הטעינה",
      product_id: 103,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 37,
      product_name: "כרטיס חיוג בינלאומי Z BESEDER",
      product_id: 101,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 47,
      product_name: "Talkmen 49 nis",
      product_id: 1031,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 50,
      product_name: "Talkmen 59 nis",
      product_id: 1032,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 53,
      product_name: "freePhone 25 NIS Calls Prisons Jobs Calls from أي هاتف عمومي (غير متوفر)توكمان هو 25 شيكل",
      product_id: 25,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 55, product_name: "פריפון 50 שח שיחות משרות בתי הסוהר שיחות מכל טלפון ציבורי (לא ניתן לביטול)", product_id: 50, language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 58, product_name: "פריפון 100 שח שיחות משרות בתי הסוהר שיחות מכל טלפון ציבורי (לא ניתן לביטול)", product_id: 100, language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 62, product_name: "جوال شحن 10 ش ج לא ניתן לביטול לאחר הטעינה!!!", product_id: 10, language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 64, product_name: "جوال شحن 20 ش ج לא ניתן לביטול לאחר הטעינה!!!", product_id: 20, language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 72, product_name: "וואטניה טעינה 10 שח  לשים לב לא ניתן לבטל את הטעינה", product_id: 1020, language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 75, product_name: "وطنية شحن 15 ش ج לא ניתן לביטול לאחר הטעינה!!!", product_id: 1021, language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 90, product_name: "دفع فاتورة الكهرباء", product_id: 1001, language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 97, product_name: "תשלום חשבון כביש 6", product_id: 1004, language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 101, product_name: "Carmel tunnels payment", product_id: 1005, language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 33,
      product_name: "BigTock 59 شيكل غير محدود في إسرائيل (5000 دقيقة ورسائل) + تصفح 15 جيجابايت",
      product_id: 1026,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 41,
      product_name: "Calling card for France up to 155 minutes for mobile or 355 minutes for landline",
      product_id: 102,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 43,
      product_name: "טוקמן 25 ש''ח",
      product_id: 1030,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 49,
      product_name: "טוקמן 59 ש''ח",
      product_id: 1032,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 57,
      product_name: "freePhone 50 NIS Calls Prisons Jobs Calls from أي هاتف عمومي (غير متوفر)توكمان هو 50 شيكل",
      product_id: 50,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 59,
      product_name: "Freerfhone 100 NIS Calls Prisons Jobs Calls From Any Public Phone (uncancelable)",
      product_id: 100,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 61,
      product_name: "جوال شحن 10 ش ج לא ניתן לביטול לאחר הטעינה!!!",
      product_id: 10,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 67,
      product_name: "جوال شحن 30 ش ج לא ניתן לביטול לאחר הטעינה!!!",
      product_id: 30,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 77,
      product_name: "וואטניה טעינה 21 שח  לשים לב לא ניתן לבטל את הטעינה",
      product_id: 1022,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 81,
      product_name: "Home card virtual 4you - XL",
      product_id: 2026,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 83,
      product_name: "International calling card Home Card 013 Worldwide for 20 NIS",
      product_id: 2002,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 87,
      product_name: "بطاقة الاتصال الدولية بطاقة Home 013 Worldwide مقابل 30 شيكل جديد ",
      product_id: 2003,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 88,
      product_name: "electricity bill payment",
      product_id: 1001,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 93,
      product_name: "دفع فاتورة الكهرباء",
      product_id: 1002,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 105,
      product_name: "bezeq الدفع عبر الإنترنت",
      product_id: 1006,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 109,
      product_name: "5 גיגה לגלישה בלבד למשך 14 יום",
      product_id: 1506,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 35,
      product_name: "الاتصال لمدة 10 دقائق للعالم كله صالح لمدة أسبوع واحد فقط من يوم التحميل",
      product_id: 103,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 40,
      product_name: "بطاقة الاتصال لفرنسا تصل إلى 155 دقيقة للجوال أو 355 دقيقة للهاتف الثابت",
      product_id: 102,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 44,
      product_name: "Talkmen 25 nis",
      product_id: 1030,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 46,
      product_name: "טוקמן 49 ש''ח",
      product_id: 1031,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow

    },
    {
      id: 65,
      product_name: "جوال شحن 20 ش ج לא ניתן לביטול לאחר הטעינה!!!",
      product_id: 20,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 69,
      product_name: "جوال شحن 30 ش ج לא ניתן לביטול לאחר הטעינה!!!",
      product_id: 30,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 70,
      product_name: "וואטניה טעינה 10 שח  לשים לב לא ניתן לבטל את הטעינה",
      product_id: 1020,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 84, product_name: "כרטיס לחיוג בנלאומי הום כארד 013 לכל העולם ב 20 ש''ח ", product_id: 2002,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow

    },
    {
      id: 85, product_name: "כרטיס לחיוג בנלאומי הום כארד 013 לכל העולם ב 30 ש''ח ", product_id: 2003,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow

    },
    {
      id: 92, product_name: "electricity bill payment", product_id: 1002
      ,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 95, product_name: "بيع الطاقة", product_id: 1003
      ,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 98, product_name: "Road six bill payment", product_id: 1004
      ,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 102, product_name: "دفع أنفاق الكرمل", product_id: 1005
      ,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 106, product_name: "תשלום לרשויות", product_id: 1007
      ,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 110, product_name: "5 gigabytes internet only for 14 days", product_id: 1506
      ,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 38, product_name: "International calling card Z BESEDER", product_id: 101
      ,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 42, product_name: "כרטיס חיוג לצרפת עד 155 דקות לנייד או 355 דקות לנייח", product_id: 102
      ,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 45, product_name: "توكمان هو 25 شيكل", product_id: 1030
      ,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 48, product_name: "توكمان هو 49 شيكل", product_id: 1031
      ,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 51, product_name: "توكمان هو 59 شيكل", product_id: 1032
      ,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 54, product_name: "פריפון 25 שח שיחות משרות בתי הסוהר שיחות מכל טלפון ציבורי (לא ניתן לביטול)", product_id: 25
      ,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 56, product_name: "Freerfhone 50 NIS Calls Prisons Jobs Calls From Any Public Phone (uncancelable)", product_id: 50
      ,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 73, product_name: "وطنية شحن 15 ش ج לא ניתן לביטול לאחר הטעינה!!!", product_id: 1021
      ,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 78, product_name: "וואטניה טעינה 21 שח  לשים לב לא ניתן לבטל את הטעינה", product_id: 1022
      ,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 79, product_name: "Home card virtual 4you - XL", product_id: 2026
      ,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 91, product_name: " תשלום חשבון חשמל באשראי", product_id: 1002
      ,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 96, product_name: "electricity charge", product_id: 1003
      ,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 100, product_name: "תשלום מנהרות הכרמל", product_id: 1005
      ,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 104, product_name: "Bezeq online payment", product_id: 1006
      ,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 108, product_name: "الدفع إلى السلطات", product_id: 1007
      ,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 39, product_name: "بطاقة الاتصال الدولية Z BESEDER", product_id: 101
      ,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 52, product_name: "Freerfhone 25 NIS Calls Prisons Jobs Calls From Any Public Phone (uncancelable)", product_id: 25
      ,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 60, product_name: "freePhone 100 NIS Calls Prisons Jobs Calls from أي هاتف عمومي (غير متوفر)توكمان هو 100 شيكل", product_id: 100
      ,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 63, product_name: "جوال شحن 10 ش ج לא ניתן לביטול לאחר הטעינה!!!", product_id: 10
      ,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 66, product_name: "جوال شحن 20 ش ج לא ניתן לביטול לאחר הטעינה!!!", product_id: 20
      ,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 68, product_name: "جوال شحن 30 ش ج לא ניתן לביטול לאחר הטעינה!!!", product_id: 30
      ,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 71, product_name: "וואטניה טעינה 10 שח  לשים לב לא ניתן לבטל את הטעינה", product_id: 1020
      ,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 74, product_name: "وطنية شحن 15 ش ج לא ניתן לביטול לאחר הטעינה!!!",
      product_id: 1021,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    }
      ,
    {
      id: 76, product_name: "וואטניה טעינה 21 שח  לשים לב לא ניתן לבטל את הטעינה",
      product_id: 1022,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    }
      ,
    {
      id: 80, product_name: "Home card virtual 4you - XL",
      product_id: 2026,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    }
      ,
    {
      id: 82, product_name: "بطاقة الاتصال الدولية بطاقة Home 013 Worldwide مقابل 20 شيكل جديد ",
      product_id: 2002,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    }
      ,
    {
      id: 86, product_name: "International calling card Home Card 013 Worldwide for 30 NIS",
      product_id: 2003,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    }
      ,
    {
      id: 89, product_name: "תשלום חשבון חשמל במזומן",
      product_id: 1001,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    }
      ,
    {
      id: 94, product_name: "מכירת חשמל",
      product_id: 1003,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    }
      ,
    {
      id: 99, product_name: "دفع حساب الطريق السادس",
      product_id: 1004,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    }
      ,
    {
      id: 103, product_name: "תשלום בזק און ליין",
      product_id: 1006,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    }
      ,
    {
      id: 107, product_name: "Authorities payment",
      product_id: 1007,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    }
      ,
    {
      id: 111, product_name: "5 غيغابايت فقط لمدة 14 يومًا",
      product_id: 1506,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    }
      ,
    {
      id: 112, product_name: "שיחות והודעות בישראל ללא הגבלה (עד 3,000 דקות + 3,000 הודעות) + 1 גיגה גלישה למשך 30 יום",
      product_id: 1500,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    }
      ,
    {
      id: 113, product_name: "Calls & Messages in Israel Unlimited (up to 3,000 minutes + 3,000 messages) + 1 Gigabyte for 30 days",
      product_id: 1500,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 114, product_name: "مكالمات ورسائل غير محدودة في إسرائيل (حتى 3000 دقيقة + 3000 رسالة) + جيجا لمدة 30 يومًا",
      product_id: 1500,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 115, product_name: "שיחות והודעות בישראל ללא הגבלה (עד 3,000 דקות + 3,000 הו) + 20 גיגה לגלישה פלוס 4 גיגה בונוס למשך 30 יום",
      product_id: 1501,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 116, product_name: "Calls & Messages in Israel Unlimited (up to 3,000 minutes + 3,000 ho) + 20 gigabytes plus 4 gigabytes bonus for 30 days",
      product_id: 1501,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 117, product_name: "المكالمات والرسائل في إسرائيل بلا حدود (حتى 3000 دقيقة + 3000 حو) + 20 غيغابايت بالإضافة إلى 4 غيغابايت منحة لمدة 30 يومًا",
      product_id: 1501,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 118, product_name: "שיחות והודעות בישראל ללא הגבלה (עד 3,000 דקות + 3,000 הו) + 20 גיגה לגלישה פלוס 4 גיגה בונוס למשך 30 יום",
      product_id: 1600,
      language_code: "HE",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 119, product_name: "Calls & Messages in Israel Unlimited (up to 3,000 minutes + 3,000 ho) + 20 gigabytes plus 4 gigabytes bonus for 30 days",
      product_id: 1600,
      language_code: "EN",
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 120, product_name: "المكالمات والرسائل في إسرائيل بلا حدود (حتى 3000 دقيقة + 3000 حو) + 20 غيغابايت بالإضافة إلى 4 غيغابايت منحة لمدة 30 يومًا",
      product_id: 1600,
      language_code: "AR",
      created_at: dateNow,
      updated_at: dateNow
    },
    ]
    return queryInterface.bulkInsert('SUPL_ProductDetails', productDetails, {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.
 
      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.bulkDelete('SUPL_ProductDetails', null, {});
  }
};
