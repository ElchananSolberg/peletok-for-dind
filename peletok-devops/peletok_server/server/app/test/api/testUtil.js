const sequelize = require('../../setupSequelize')

module.exports = {
    truncateModel(model){
       return sequelize.getQueryInterface().bulkDelete(model.getTableName(),{},{
           truncate: true,
           cascade: true,
       }, model)
   },
   fileNameFormat(folder, name){
       return new RegExp(`^\/${folder}\/${name}-wataniya-10-[0-9]{1,4}-[0-9]{1,2}-[0-9]{1,2}T[0-9]{1,2}-[0-9]{1,2}-[0-9]{1,2}\.png$`)
   }
}