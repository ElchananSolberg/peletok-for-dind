
const BACKEND_URL = process.env.REACT_APP_BACKEND_URL || 'http://localhost:8080';
const BACKEND_BASE_URL = "/api/v1";

export const config = {
    // TODO change baseUrl
    baseUrl: BACKEND_URL + BACKEND_BASE_URL,
    localStorageKey: 'pelehTalk'
};