var BaseModel = require('../../BaseModel.js');
const DataTypes = require("sequelize").DataTypes;

/**
 * UserContact model
 */
class UserContact extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            type: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            value: {
                type: DataTypes.STRING(128),
                allowNull: true
            },
            label: {
                type: DataTypes.STRING(64),
                allowNull: true
            }
        };
        this.options["indexes"] = [
            {
                unique: false,
                fields: ['user_id']
            }
        ];

        this.author = true;
        this.statusTimestampField = true;
        this.statusField = true;
        this.prefix = 'USR';

    }
    /**
     * creates associate for userContact model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.user.model, { as: 'User' })
    }
}
// exporting instance
module.exports = new UserContact();