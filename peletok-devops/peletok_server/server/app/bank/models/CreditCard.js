const DataTypes = require('sequelize').DataTypes;
const BaseModel = require('../../BaseModel');
/**
 * CreditCard model
 */

class CreditCard extends BaseModel {
    config() {
        /**
         * configs fields and options
         */
        this.attributes = {
            cc_company_name: {
                type: DataTypes.STRING,
                allowNull: true
            },
            cc_type: {
                type: DataTypes.STRING,
                allowNull: true
            },
         
            cc_number: {
                type: DataTypes.STRING,
                allowNull: true
            },
            cc_holder_name: {
                type: DataTypes.STRING,
                allowNull: true
            },
            cc_holder_id: {
                type: DataTypes.STRING,
                allowNull: true
            },
            cc_cvv_number: {
                type: DataTypes.STRING,
                allowNull: true
            },
            cc_validity: {
                type: DataTypes.DATE,
                allowNull: true
            },
            description: {
                type: DataTypes.STRING,
                allowNull: true
            }
            
        }


        this.author = true;
        this.statusAuthor = true;
        this.statusField = true;
        this.statusTimestampField = true;
        this.prefix = 'BNK';

    }
     /**
     * creates associate for CreditCard model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.hasMany(models.supplier.model, {as: 'Supplier' });
    }
}

module.exports = new CreditCard();
