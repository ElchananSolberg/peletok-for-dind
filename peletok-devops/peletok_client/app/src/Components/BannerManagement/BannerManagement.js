import React from 'react';
import { Container, Row, Col, Button, Collapse } from 'reactstrap';
import Tag from '../InputUtils/Tag';

import { LanguageManager as LM } from "../LanguageManager/Language";
import ImageUpload from '../InputUtils/ImageUpload'
import "./BannerManagement.css"
import InputTypeCheckBox from '../InputUtils/InputTypeCheckBox';
import InputTypeText from '../InputUtils/InputTypeText';
import InputTypeSearchList from '../InputUtils/InputTypeSearchList';
import InputTypeNumber from '../InputUtils/InputTypeNumber';
import InputTypeRadio from '../InputUtils/InputTypeRadio';
import InputTypeDate from '../InputUtils/InputTypeDate';
import InputTypeTime from '../InputUtils/InputTypeTime';
import { getAllBanners, getDistributors, getTags } from '../DataProvider/Functions/DataGeter';
import BannerTable from './BannerTable';
import { postNewBanner } from '../DataProvider/Functions/DataPoster';

import { SERVER_BASE } from '../DataProvider/DataProvider';

/**
 * A component that displays banners information 
 */
export class BannerManagement extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            creatingNewBaner: false,
            isEdit: false,
            segmentationList: [],


            lang: { hebrew: false, english: false, arabic: false },


            bannersList: {
                title: LM.getString("bannersList") + ":",
                id: "BannersList",
                options: [],
                default: ''
            },
            selectDistributors: {
                title: LM.getString("SelectDistributors"),
                id: "selectDistributors",
                options: [],
            },
            segmentation: {
                title: LM.getString("tags"),
                id: "segmentation",
                options: [],
            },
            bannersName: {
                maxLength: 40,
                minLength: 1,
                title: LM.getString("bannersName"),
                id: "BannersName",
                required: true,
            },
            linkToPage: {
                maxLength: 15,
                minLength: 1,
                title: LM.getString("linkToPage"),
                id: "LinkToPage",
                required: true,
            },
            BannerDurationInSeconds: {
                minLength: 1,
                maxLength: 4,
                title: LM.getString("BannerDurationInSeconds"),
                id: "BannerDurationInSeconds",
                required: true,
            },
            order: {
                minLength: 1,
                maxLength: 4,
                title: LM.getString("order"),
                id: "Order",
                required: true,
            },
            activeLang: {
                title: LM.getString("lang") + ":",
                id: "activeLang",
                required: true,
                options: [LM.getString("any"), LM.getString("hebrew"), LM.getString("english"), LM.getString("arabic")],
                default: LM.getString("any")
            },

            activeLocation: {
                title: LM.getString("location"),
                id: "ActiveLocation",
                options: ['up', 'down'],//TODO real option
                required: true,
            },
            howMuchWeCanBounce: {
                minLength: 1,
                maxLength: 4,
                title: LM.getString("howMuchWeCanBounce"),
                id: "HowMuchWeCanBounce",
                required: true,
            },
            bannerStatus: {
                options: LM.getString("banersStatusList"),
                title: LM.getString("bannerStatus"),
                id: "BannerStatus-",
                required: true,
            },

            /** data for "Favorites" field (InputTypeDate InputUtils component) */
            favoritesProps: {
                buttonText: LM.getString("favorites"),
                title: LM.getString("tags"),
                id: "BusinessFavorites",
            },
            /** data for "Recommended" field (Tag InputUtils component) */
            recommendedProps: {
                buttonText: LM.getString("recommended"),
                title: "",
                id: "BusinessRecommended",
            },
            /** data for "For Internet" field (Tag InputUtils component) */
            forInternetProps: {
                buttonText: LM.getString("forInternet"),
                title: "",
                id: "BusinessForInternet",
            },
            distributors: {
                lableText: LM.getString("distributors"),
                id: "Distributors",
            },
            sellers: {
                lableText: LM.getString("sellers"),
                id: "Sellers",
            },
            all: {
                lableText: LM.getString("all"),
                id: "All",
            },
            hebrew: {
                lableText: LM.getString("hebrew"),
                id: "LangHebrew",
            },
            english: {
                lableText: LM.getString("english"),
                title: "",
                id: "LangEnglish",
            },
            arabic: {
                lableText: LM.getString("arabic"),
                id: "LangArabic",
            },
            selectAll: {
                lableText: LM.getString("selectAll"),
                id: "LangSelectAll",
            },
            bannersFile: {
                title: LM.getString("bannersFile"),
                id: "BannersFile",
                required: true,
            },
            distributorsNamesList: [],
            receivedBanners: [],
            tagsList: [],
            curentBanner: {},
        }
        this.tags = []
        this.tagsObj = {}
    }

    componentDidMount() {
        getTags().then(
            (res) => {
                this.setState({ tagsList: res },
                    () => {
                        for (let key in this.state.tagsList) {
                            this.tags.push(
                                <Col sm='auto'>
                                    <Tag
                                        buttonText={this.state.tagsList[key].name}
                                        title={key == 0 ? LM.getString("tags") : ''}
                                        id={`${this.state.tagsList[key].name}${this.state.tagsList[key].id}`}
                                        ref={(componentObj) => { this.tagsObj[this.state.tagsList[key].name] = componentObj }}
                                    />
                                </Col>
                            )
                        }
                    })
            }
        ).catch(
            (err) => {
                alert(err);
            }
        )
        this.getTheBanners();
        getDistributors().then(
            (res) => {
                this.setState({ allDistributors: res }, () => {

                    var distributorsNamesList = [];
                    for (let key in this.state.allDistributors) {
                        if (this.state.allDistributors[key].first_name != null || this.state.allDistributors[key].last_name != null) {

                            distributorsNamesList.push({ id: this.state.allDistributors[key].id, name: this.state.allDistributors[key].first_name + ' ' + this.state.allDistributors[key].last_name });
                            this.setState({ distributorsNamesList }, () => {
                                let newDistributorsList = [];
                                for (let key in this.state.distributorsNamesList) {
                                    newDistributorsList.push(distributorsNamesList[key].name)
                                }
                                let selectDistributors = Object.assign({}, this.state.selectDistributors);
                                selectDistributors.options = newDistributorsList;
                                this.setState({ selectDistributors })
                            })
                        }
                        else {
                        }
                    }
                })
            }
        ).catch(
            (err) => {
                alert(err);
            }
        )
    }

    getTheBanners = () => {
        getAllBanners().then(
            (res) => {
                this.setState({ receivedBanners: [] })
                this.setState({ receivedBanners: res },
                    () => {
                        var namesList = [];
                        for (let key in this.state.receivedBanners) {
                            namesList.push(this.state.receivedBanners[key].name);
                        }
                        let nameBannersList = Object.assign({}, this.state.bannersList);
                        nameBannersList.options = namesList;
                        this.banersListFun.setValue(namesList[0])
                        this.setState({ bannersList: nameBannersList })
                    })
            }
        ).catch(
            (err) => {
                alert(err);
            }
        )
    }

    save = (e) => {
        if (this.banersNameFun.getValue().value != ''
            && this.linkToPage.getValue().value != ''
            && this.banersFile.getValue().value != ''
            && this.BannerDurationInSecondsFun.getValue().value != ''
            && this.location.getValue().value != undefined
            && this.howMuchWeCanBounceFun.getValue().value != ''
            && this.order.getValue().value != ''
            && this.bannerStatus.getValue().value != undefined
            && (this.english.getValue().value != false
                || this.arabic.getValue().value != false
                || this.hebrew.getValue().value != false)

            && this.startPosting.getValue().value != ''
            && this.endPosting.getValue().value != ''
            // && this.selectDistributors.getValue().value != ''
        ) {
            var newBaner = {};
            newBaner.name = this.banersNameFun.getValue().value;
            newBaner.banner_url = this.linkToPage.getValue().value;
            newBaner.duration = this.BannerDurationInSecondsFun.getValue().value;
            newBaner.location = this.location.getValue().value;
            newBaner.num_bounce = this.howMuchWeCanBounceFun.getValue().value;
            newBaner.order = this.order.getValue().value;
            newBaner.status = this.bannerStatus.getValue().value == LM.getString("banersStatusList")[0] ? 1 : 2;
            newBaner.language = [];
            newBaner.language['EN'] = this.english.getValue().value;
            newBaner.language['AR'] = this.arabic.getValue().value;
            newBaner.language['HE'] = this.hebrew.getValue().value;
            newBaner.start_date = this.startPosting.getValue().value;
            newBaner.end_date = this.endPosting.getValue().value;
            newBaner.time = this.time.getValue().value;
            // newBaner.distributor_id = this.selectDistributors.getValue().value;//  need to add functionality to get distributor_id

            // newBaner.tag = []
            // Object.keys(this.tagsObj).forEach((key) => {
            //     this.state.tagsList.forEach(element => {
            //         if (element.name == key && this.tagsObj[key].getValue().value == true) {
            //             newBaner.tag.push(""+element.id)
            //         }
            //     });
            // })

            if (!this.state.isEdit) {
                postNewBanner(newBaner).then(
                    (res) => {
                        alert(LM.getString("bannerAddedSuccessfully"))
                        this.cleanPage()
                        this.getTheBanners()
                    }
                ).catch(
                    (error) => {
                        this.cleanPage()
                        if (error && error.response && error.response.data && error.response.data[0].message) {
                            alert(error.response.data[0].message);
                        }
                        else (alert(LM.getString("unrecognizedError")))
                    }
                )
            }
            else {
                // this.BanerTableRef
            }
        }
        else { alert(LM.getString("enterAllFields")) }
    }
    isEdit = (e) => {
        this.setState({ isEdit: !this.state.isEdit }, () => {

            this.setState({ creatingNewBaner: this.state.isEdit ? true : false });
            window.scrollTo({
                top: 0,
                behavior: "smooth"
            });
        })
    }
    hebrewChange = (e) => {
        var lang = Object.assign({}, this.state.lang);
        lang.hebrew = this.hebrew.getValue().value;
        this.setState({ lang })
    }
    englishChange = (e) => {
        var lang = Object.assign({}, this.state.lang);
        lang.english = this.english.getValue().value;
        this.setState({ lang })
    }
    arabicChange = (e) => {
        var lang = Object.assign({}, this.state.lang);
        lang.arabic = this.arabic.getValue().value;
        this.setState({ lang })
    }
    allChange = (e) => {
        var lang = Object.assign({}, this.state.lang);
        if (!lang.hebrew || !lang.english || !lang.arabic) {

            this.hebrew.setValue(true);
            this.english.setValue(true);
            this.arabic.setValue(true);
            lang.hebrew = true;
            lang.english = true;
            lang.arabic = true;
        }
        else {
            this.hebrew.setValue(false);
            this.english.setValue(false);
            this.arabic.setValue(false);
            lang.hebrew = false;
            lang.english = false;
            lang.arabic = false;
        }
        this.setState({ lang })
    }
    delete = (e) => {
        this.cleanPage()
        this.setState({ creatingNewBaner: false });
    }
    selectDistributorsFun = (e) => {
        alert(this.selectDistributors.getValue().value);
    }
    new = (e) => {
        this.BanerTableRef.changeIsEdit()
        this.cleanPage();
        this.setState({ creatingNewBaner: this.state.isEdit || !this.state.creatingNewBaner ? true : false });
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        });
        this.setState({ isEdit: false });
    }

    ShowNow = (e) => {
        this.bannerStatus.setValue(LM.getString("banersStatusList")[1])
        alert(this.bannerStatus.setValue(LM.getString("banersStatusList")[1]))
    }
    onChangeBannerStatus = (e) => {

    }
    cleanPage = () => {
        document.getElementById("InputUtilsRadioBannerStatus-0").checked = false;
        document.getElementById("InputUtilsRadioBannerStatus-1").checked = false;
        this.banersListFun.setValue('');
        this.banersNameFun.setValue('');
        this.linkToPage.setValue('');
        this.banersFile.setValue('');
        this.BannerDurationInSecondsFun.setValue('');
        this.location.setValue('');
        this.howMuchWeCanBounceFun.setValue('');
        this.order.setValue('');
        this.english.setValue(false);
        this.arabic.setValue(false);
        this.hebrew.setValue(false);
        this.startPosting.setValue();
        this.endPosting.setValue();
        let d = new Date();
        let h = d.getHours();
        let m = d.getMinutes();
        this.time.setValue(`${h}:${m}`);
        this.selectDistributors.setValue('')
        this.bannerStatus.setValue(false)
        this.selectAll.setValue(false);
        Object.keys(this.tagsObj).forEach((key) => {
            this.tagsObj[key].setValue(false)
        })
    }
    fillThePage = (e) => {
        this.banersNameFun.setValue(this.state.curentBanner.name);
        this.banersFile.setValue(`${SERVER_BASE}/${this.state.curentBanner.banner_url}`);//fake url 
        this.linkToPage.setValue(this.state.curentBanner.link_url);
        this.BannerDurationInSecondsFun.setValue(this.state.curentBanner.duration);

        this.location.setValue(this.state.curentBanner.location);
        this.howMuchWeCanBounceFun.setValue(this.state.curentBanner.num_bounce);
        this.order.setValue(this.state.curentBanner.order);

        if (this.state.curentBanner.status == 1) {

            document.getElementById("InputUtilsRadioBannerStatus-0").checked = true;
            this.bannerStatus.setValue(LM.getString("banersStatusList")[0]);

        }
        else {
            this.bannerStatus.setValue(LM.getString("banersStatusList")[1]);
            document.getElementById("InputUtilsRadioBannerStatus-1").checked = true;
        }
        for (var lang in this.state.curentBanner.language) {
            if (this.state.curentBanner.language[lang] == 'EN') {
                this.english.setValue(true);
            }
            if (this.state.curentBanner.language[lang] == 'AR') {
                this.arabic.setValue(true);
            }
            if (this.state.curentBanner.language[lang] == 'HE') {
                this.hebrew.setValue(true);
            }
        }
        if (this.state.curentBanner.start_date) {
            this.startPosting.setValue(this.state.curentBanner.start_date.split('T')[0]);
            this.endPosting.setValue(this.state.curentBanner.end_date.split('T')[0]);
            this.time.setValue(this.state.curentBanner.end_date.split('T')[1].split('Z')[0].split('.')[0]);

        }
    }
    onSelect = (e) => {
        var selected = this.banersListFun.getValue().value;
        this.cleanPage();
        for (var key in this.state.receivedBanners) {
            if (this.state.receivedBanners[key].name === selected) {
                this.setState({ curentBanner: this.state.receivedBanners[key] }, () => { this.fillThePage() })
            }

        }
    }
    editRowFromTable = (idFromRow) => {
        this.cleanPage();
        for (var key in this.state.receivedBanners) {
            if (this.state.receivedBanners[key].id === idFromRow) {
                this.setState({ curentBanner: this.state.receivedBanners[key] }, () => { this.fillThePage() })
            }
        }
    }

    render() {
        /** Screen Header text */
        const header = LM.getString('bannerManagement');

        return (
            <Container className="bannerManagementContainer">
                <Row >
                    <Col className="bannerManagementHeader">
                        {header}
                    </Col>
                </Row>
                <Container className="bannerManagementSettings">
                    <Row >
                        <Col lg='4' sm='6'>
                            <Collapse isOpen={!this.state.creatingNewBaner}>
                                <InputTypeSearchList ref={(componentObj) => { this.banersListFun = componentObj }}{...this.state.bannersList} onChange={this.onSelect} />
                            </Collapse>
                        </Col>

                        <Col lg='4' sm='6'>
                            <InputTypeText
                                ref={(componentObj) => { this.banersNameFun = componentObj }}{...this.state.bannersName}
                            />
                        </Col>
                    </Row>
                    <Row >
                        <Col lg='8' sm='12'>
                            <ImageUpload {...this.state.bannersFile} ref={(componentObj) => { this.banersFile = componentObj }} />
                        </Col>
                        <Col sm='2'></Col>
                    </Row>
                    <Row >
                        <Col lg='4' sm='6'>
                            <InputTypeText ref={(componentObj) => { this.linkToPage = componentObj }}{...this.state.linkToPage}
                            />
                        </Col>
                        <Col lg='4' sm='6'>
                            <InputTypeNumber {...this.state.BannerDurationInSeconds} ref={(componentObj) => { this.BannerDurationInSecondsFun = componentObj }}
                            />
                        </Col>
                    </Row>
                    <Row >
                        <Col lg='4' sm='6'>
                            <InputTypeSearchList
                                ref={(componentObj) => { this.location = componentObj }}
                                title={LM.getString("location")}
                                id="Location"
                                options={LM.getString("banersLocation")}
                            />
                        </Col>
                        <Col lg='4' sm='6'>
                            <div className='fs-17'>{this.state.howMuchWeCanBounce.title}</div>
                            <InputTypeNumber {...this.state.howMuchWeCanBounce} ref={(componentObj) => { this.howMuchWeCanBounceFun = componentObj }} title='' />
                        </Col>
                        <Col sm='auto'>
                            <Button className='mt-4 btn-lg px-3 border-radius-3 font-weight-bold regularButton fs-17 btn-sm-block' onClick={this.ShowNow} id='showNowButton'>
                                {LM.getString("showNow")}
                            </Button>
                        </Col>
                    </Row>
                    <Row >
                        <Col lg='4' sm='6'>
                            <InputTypeNumber {...this.state.order} ref={(componentObj) => { this.order = componentObj }}
                            />
                        </Col>
                        <Col lg='4' sm='6'>
                            <InputTypeRadio {...this.state.bannerStatus} onChange={this.onChangeBannerStatus} ref={(componentObj) => { this.bannerStatus = componentObj }}
                            />
                        </Col>
                    </Row>

                    {LM.getString("lang")}
                    <div className="d-flex">

                        <InputTypeCheckBox checked={this.state.lang.hebrew} {...this.state.hebrew} ref={(componentObj) => { this.hebrew = componentObj }}
                            onChange={this.hebrewChange} />


                        <InputTypeCheckBox checked={this.state.lang.arabic} {...this.state.arabic} ref={(componentObj) => { this.arabic = componentObj }}
                            onChange={this.arabicChange} />



                        <InputTypeCheckBox checked={this.state.lang.english} {...this.state.english} ref={(componentObj) => { this.english = componentObj }}
                            onChange={this.englishChange} />


                        <InputTypeCheckBox {...this.state.selectAll} ref={(componentObj) => { this.selectAll = componentObj }}
                            onChange={this.allChange} />

                    </div>

                    <Row className="align-items-end pt-3">
                        {this.tags}
                    </Row>

                    <Row >
                        <Col lg='4' sm='6'>
                            <InputTypeSearchList
                                ref={(componentObj) => { this.selectDistributors = componentObj }}
                                {...this.state.selectDistributors}
                                onChange={this.selectDistributorsFun}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col lg='3' sm='6'>
                            <InputTypeDate
                                title={LM.getString("startDatePosting")}
                                id="StartDatePosting"
                                ref={(componentObj) => { this.startPosting = componentObj }}
                                onChange={this.startPostingFun
                                }
                            />
                        </Col>
                        <Col lg='3' sm='6'>
                            <InputTypeDate
                                title={LM.getString("endDatePosting")}
                                id="endDatePosting"
                                ref={(componentObj) => { this.endPosting = componentObj }}
                                onChange={this.endPostingFun
                                }
                            />
                        </Col>
                        <Col lg='5' sm='6'>
                            <InputTypeTime
                                title={LM.getString("time")}
                                id="Time"
                                ref={(componentObj) => { this.time = componentObj }}
                                onChange={this.timeFun}
                            />
                        </Col>
                    </Row>
                    <div className='mt-4'>
                        <Button
                            onClick={this.save}
                            id='supplierSave'
                            className='bannerManagementButton px-4 fs-17' /*infoButton*/
                        >
                            {!this.state.isEdit ? LM.getString("save") : LM.getString("saveChanges")}
                        </Button>
                        <Button
                            color="secondary"
                            size="sm"
                            onClick={this.delete}
                            id='delete'
                            className='btn-lg mx-4 border-radius-3 font-weight-bold px-4 fs-17'

                        >
                            {LM.getString("delete")}
                        </Button>
                        <Button
                            color="secondary"
                            size="sm"
                            onClick={this.new}
                            id='new'
                            className='font-weight-bold border-radius-3 btn-lg regularButton px-4 fs-17'
                            active >
                            {LM.getString("new")}
                        </Button>
                    </div>
                </Container>

                <Container className='bannerManagementSettings' >
                    <p style={{ fontWeight: "bold" }}>{LM.getString("activeBanners")}</p>
                    <Row >
                        <Col sm='4'>
                            <InputTypeSearchList
                                {...this.state.activeLang}
                                onChange={this.activeLangChanged}
                            ref={(componentObj) => { this.activeLang = componentObj }}

                            />
                        </Col>
                        <Col sm='4'>
                            <InputTypeNumber
                                {...this.state.activeLocation}
                                onChange={this.activeLocationChanged}
                                ref={(componentObj) => { this.activeLocation = componentObj }}
                            />
                        </Col>
                        <Col sm='4'>
                            <InputTypeSearchList
                                {...this.state.segmentation}
                                onChange={this.activeSegmentationChanged}
                                ref={(componentObj) => { this.activeSegmentation = componentObj }}                                
                            />
                        </Col>
                    </Row>
                    <BannerTable
                        cleanPage={this.cleanPage}
                        isEdit={this.isEdit}
                        isEditState={this.state.isEdit}
                        editRowFromTable={this.editRowFromTable}
                        tableRows={this.state.receivedBanners}
                        ref={(componentObj) => { this.BanerTableRef = componentObj }}
                    />
                </Container>
            </Container>
        );
    }
}