const moment = require('moment');
var Sequelize = require('sequelize');
var Permission = require('../permission/models/Permission').model;
var Op = Sequelize.Op;
const sequelizeInstance = require('../setupSequelize')

// let transaction = sequelizeInstance.transaction()

const addPermission = (queryInterface, permissionName, permissionType, path, permissionGroup) => {
	return new Promise(async (resolve, reject) => {
		let p =[{ 
	            name: permissionName,
	            type: permissionType,
	            path: path,
	            permission_group_id: permissionGroup,
	            created_at: moment().toDate(),
	            updated_at: moment().toDate(),
	        }
	   ,]
	   await queryInterface.bulkInsert('PRM_Permission', p, {})
	   const permissions = await queryInterface.select(Permission, 'PRM_Permission', {
	      attributes: ['id', 'path'],
	      where: {
	        path: {[Op.in]: [path]},
	      },
	    });
	    await queryInterface.bulkInsert('PRM_PermissionRole',  permissions.map((p) => {
	      return {
	          action: 'edit',
	          role_id: 1,
	          permission_id: p.id,
	          created_at: moment().toDate(),
	          updated_at: moment().toDate(),
	        }
	      }), {});
	    resolve() 

	})
}
// columns should be {name: string, typeOptions: {} }
async function addColumnsToTable(queryInterface, tableName, columns){
	let table = await queryInterface.describeTable(tableName)
	return  Promise.all(columns.map(c=>!table[c.name] ?  queryInterface.addColumn(tableName, c.name, c.typeOptions) : Promise.resolve()))
	
}


async function renameColumnsIfExist(queryInterface, tableName, columns){
	let table = await queryInterface.describeTable(tableName)
	return  Promise.all(columns.map(c=>table[c[0]] ?  queryInterface.renameColumn(tableName, c[0], c[1]) : Promise.resolve()))
	
}

async function removeColumnsIfExist(queryInterface, tableName, columns){
	let table = await queryInterface.describeTable(tableName)
	return  Promise.all(columns.map(c=>table[c] ?  queryInterface.removeColumn(tableName, c) : Promise.resolve()))
	
}

async function renamePermission(queryInterface, oldPath, newPath){
	const permission = (await queryInterface.select(Permission, 'PRM_Permission', {
      where: {
        path: oldPath,
      },
    }))[0];
    await permission.update({path: newPath})
    return

}

module.exports.addPermission = addPermission; 
module.exports.addColumnsToTable = addColumnsToTable; 
module.exports.renamePermission = renamePermission; 
module.exports.renameColumnsIfExist = renameColumnsIfExist; 
module.exports.removeColumnsIfExist = removeColumnsIfExist; 
