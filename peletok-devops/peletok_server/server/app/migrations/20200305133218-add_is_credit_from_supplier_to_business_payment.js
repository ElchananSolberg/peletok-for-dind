'use strict';
const {addColumnsToTable} = require('../dbUtils/addPermission')
module.exports = {
  async up(queryInterface, Sequelize) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    
    await  addColumnsToTable(queryInterface, 'BNK_BusinessPayment', [{
        name: 'is_credit_from_supplier', typeOptions: {
          type: Sequelize.BOOLEAN,
          defaultValue: false
        }
      }])
    
  },
  async down(queryInterface, Sequelize) {
    
  }
};
