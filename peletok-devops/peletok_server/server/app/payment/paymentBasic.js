/***
 Payment basic class 
Each inherited class should implement those functions. 

 */
const Sequelize = require('sequelize');
const Op = Sequelize.Op
const models = require('../index');
const moment = require('moment')
const logs = require('../logger')

 class PaymentBasic{

 	constractor(){
	

	
 	}
    
/** *Login to supplier api */
login(){

}

/** *TO FILL detailes for  supplier api */
fill_detailes(){

}
/** *Pay to supplier api */
pay(){

}

async getPrice() {

    const product = await models.productPrice.model.findOne(
        {
            where: {
                product_id: this.productId,
                timestamp_end: {[Op.gte]: moment()}
            }
        })

    if (!product) {
        this.info.setErrMessage(" ProductId  is  null ")
        this.info.err_in_productId = true
        return this.info
    }


    this.info.price = product["fixed_price"]
    logs.info("price  of  card  is ", product["fixed_price"], " productId is ", this.productId)
    return this.info
}
/** *Cancle payment  to supplier api */
cancle_payment(){

}

async isDemoTransactionUser(){
	
	return (await models.user.model.count({
	        include: [{
	            model: models.role.model,
	            attributes: [],
	            include: [{
	                model: models.permission.model,
	                where: {
	                    path: '/demo_transaction',
	                },
	                attributes: [],
	                required: true,
	                through: {
	                    attributes: [],
	
	                }
	            }],
	            required: true
	        }],
	        where: { id: this.req.user.id }
	    })) > 0
}

demoRespons(){
	this.info.confirmation = "000000001"
	this.info.send = {"success":"test0000001" }
	this.info.isPaid = true
	this.info.externalId="11111"
	return this.info
	
}


}
PaymentBasic.prototype.timeout = 20000; // in milliseconds
module.exports =  PaymentBasic;