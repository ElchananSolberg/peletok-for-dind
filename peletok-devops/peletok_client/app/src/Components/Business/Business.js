import React, { Component } from 'react';
import './Business.css';

import { Button, Collapse, Container, Row, Col } from 'reactstrap';

import { LanguageManager as LM } from '../LanguageManager/Language';
import { Notifications } from '../Notifications/Notifications'
import { Loader } from '../Loader/Loader'
import { Confirm } from '../Confirm/Confirm'
import TagsField from '../TagsField/TagsField';

import {
    getDistributors,
    postDistributor,
    putDistributor,
    deleteDistributor,
    deleteSeller,
    putSeller,
    postSeller,
    getBusinessProfiles,
    putApproveSeller,
    getSellersByDistributorId,
    getDoseModeFieldValueExsist,
    getBusinessTags
} from '../DataProvider/DataProvider'

import { config } from '../../utils/Config'
import { BusinessIdsEnum } from '../../enums'

import InputTypeSearchList from '../InputUtils/InputTypeSearchList';
import InputTypeText from '../InputUtils/InputTypeText';
import InputTypeNumber from '../InputUtils/InputTypeNumber';
import InputTypeSimpleSelect from '../InputUtils/InputTypeSimpleSelect';
import InputTypeCheckBox from '../InputUtils/InputTypeCheckBox';
import InputTypePhone from '../InputUtils/InputTypePhone';
import InputTypeEmail from '../InputUtils/InputTypeEmail';
import InputTypeDate from '../InputUtils/InputTypeDate';
import InputTypeAddress from '../InputUtils/InputTypeAddress';
import Tag from '../InputUtils/Tag';
import ImageUpload from '../InputUtils/ImageUpload'


import whiteBlock from '../InputUtils/white_block.png';
import { getAgentsResponsible } from "../DataProvider/Functions/DataGeter";
import { AuthService } from '../../services/authService'
import { AddNewUser } from '../selectUser/addNewUser';
import { BusinessStatusEnum } from '../../enums/'
import { SelectedSeller } from '../SelectedSeller/SelectedSeller';

/**
 * Main component of Business screen.  Business.js is working with separate .css file: Business.css.
 */

class Business extends Component {

    constructor(props) {
        super(props);

        this.state = {
            /** working variable for a "Search" button click handler  */
            searching: false,
            /** working variable for a "Save"(as distributor) button click handler  */
            saved: false,
            /** working variable that will contain an array of distributors from the server */
            distributors: [],
            /** working variable that will contain a selected distributor object */
            selectedDistributor: {},
            /** working variable that will contain a selected distributor id */
            distributorID: -1,
            /** working variable that will contain an array (of objects) of all the selected distributor sellers */
            sellers: [],
            /** working variable that will contain a selected seller object */
            selectedSeller: {},
            /** working variable that indicates if a user is on the stage of creating a new seller */
            creatingNew: false,

            renderNumbersFields: true,
            profiles: {},
            agentsResponsible: [],
            renderFields: true,
            businessNameValidationResult: true,
        };


        /** working variable that will contain an object of all the input fields (on the Business screen), which are getting data from the selectedSeller object (from the server ), and sending data to the server */
        this.fields = {};
        this.distributorFields = {};
        this.addUser = {}
    }

    businessNameChanged = async () => {
        let newBusinessName = this.fields.name.getValue().value;
        if (  this.state.selectedSeller.name !== newBusinessName ) {
            await this.validateBusinessName();
            this.fields.name.runValidation();
        }
    }
    validateBusinessName = async () => {
        let businessName = this.fields.name.getValue().value;
        await getDoseModeFieldValueExsist('business', 'name', businessName).then(
            (res) => {
                if (!res) {
                    this.setState({ businessNameValidationResult: true });
                }
                else {
                    this.setState({ businessNameValidationResult: false });
                }
            }
        ).catch(
            (err) => {
                Notifications.show(err.response.data.error, 'danger');
            }
        )
    }
    businessNameOuterValidationFunction = (value) => {
        if (this.state.businessNameValidationResult) {
            return true
        }
        return false
    }
 

    /** TODO  change functionality */
    /** "Proceed" button click handler*/
    proceedClick = async (e) => {
        Loader.show();
        this.fields["name"].runValidation();
        let dataSeller = {};
        let requestValid = true
        if (this.addUser && this.addUser.validate) {
            let [tempRequestValid, is_valid_name_id] = await this.addUser.validate();
            requestValid = tempRequestValid && is_valid_name_id;

        }
        Object.keys(this.fields).forEach((key) => {
            let value = this.fields[key].getValue();
            if (!value.valid) requestValid = false;
            else {
                if (!this.state.creatingNew && value.value === "") {
                    dataSeller[key] = null;
                } else dataSeller[key] = value.value;
            }
        });

        if (!requestValid) {
            Notifications.show(LM.getString("notValidField"), 'danger');
            Loader.hide();
            return;
        }
        let dataDistributor = {};
        /** if a seller was selected to be a distributor by a user */
        if (this.state.saved) {
            let distributorFieldValid = true;
            Object.keys(this.distributorFields).forEach((key) => {
                let value = this.distributorFields[key].getValue();
                if (!value.valid) distributorFieldValid = false;
                else {
                    if (!this.state.creatingNew && value.value === "") dataDistributor[key] = null;
                    else dataDistributor[key] = value.value;
                }
            });

            if (!distributorFieldValid || !this.fields["name"].getValue().valid) {
                Notifications.show(LM.getString("notValidField"), 'danger');
                return;
            }
            if (!this.state.creatingNew && this.fields["name"].getValue().value === "") dataDistributor.business_name = null;
            else dataDistributor.business_name = this.fields["name"].getValue().value;
        }

        dataSeller.distributor_id = this.state.distributorID;
        dataSeller.distributor_business_id = this.state.selectedDistributor.id;
        let mergedData = Object.assign({}, { ...dataSeller, ...dataDistributor });



        /** when CREATING new */
        if (this.state.creatingNew) {
            
                
                if(this.state.saved){
                    mergedData.is_distributor = true
                }
                Loader.show();
                postSeller(this.state.saved ? mergedData : dataSeller).then(async res => {
                    let userResponse = await this.addUser.onAdd(res.id)
                    Loader.hide();

                    if (userResponse) {
                        Notifications.show(LM.getString("successSellerAdding") + dataSeller["name"], 'success');
                    } else {
                        Notifications.show(LM.getString("successSellerAddingButUserFail") + dataSeller["name"], 'danger');
                    }
                    SelectedSeller.resetSeller.next();
                    this.setState({ creatingNew: false, sellers: [], selectedSeller: {} });

                    SelectedSeller.createSellerClicked.next();

                    let newSellerId = res.id;
                    SelectedSeller.refreshSellers.next(newSellerId);

                }).catch(
                    err => {
                        Loader.hide();
                        Notifications.show(err.response.data.error, 'danger');
                    }
                );
            }

        /** when UPDATING */
        else {
            if (this.state.distributorID === undefined || this.state.distributorID === null || this.state.distributorID === -1) {
                Notifications.show(LM.getString('distributorIdError'), 'danger');
                return;
            }
            
                if (SelectedSeller.currentSeller.getValue().id) {
                    Loader.show();
                    putSeller(this.state.selectedSeller.id, this.state.saved ? mergedData : dataSeller).then(
                        res => {
                            Loader.hide();
                            // this.context.refreshDistributorsList();
                            SelectedSeller.refreshSellers.next()
                            Notifications.show(LM.getString("successSellerUpdating") + dataSeller["name"], 'success');
                        }
                    ).catch(
                        err => {
                            Loader.hide();
                            Notifications.show(err.response.data.error, 'danger');
                        }
                    );
                } else Notifications.show(LM.getString("sellerNotSelectedError"), 'warning');
        }
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        });
    }

    /** Button "New" onClick handler.
     * Changes the value of creatingNew variable to true, that will indicate, that a user is on the stage of creating a new seller.
     * At the end it clears a value of the 'Select Seller' field and calls the clearFields() function. */
    newClick = (e) => {
        this.setState({ creatingNew: true, saved: false });
        SelectedSeller.createSellerClicked.next()
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        });
        SelectedSeller.resetSeller.next()
        this.clearFields();
    }
    /** Button "Cancel" onClick handler.
     * Changes the value of creatingNew variable to false, that will indicate, that a user is on the stage of editing an existing seller. */
    cancelClick = (e) => {
        this.setState({ creatingNew: false });
        SelectedSeller.createSellerClicked.next()
        /**  if a seller was selected to be a distributor by a user */
        if (this.state.saved) {
            SelectedSeller.createDistributorClicked.next()
         }
        this.clearFields();
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        });
    }
    /** "Save" button click handler
     * After a click on "Save" button IF "Define Seller as a Distributor" checkbox is checked 
     * a collapse with specific for a Distributor input fields should be opened up
     * and a collapse with a row that includes a "Define Seller as a Distributor" checkbox should be closed up */
    saveClick = (e) => {
        if (this.defineSellerDistributor.getValue().value) this.setState({ saved: true });
        /**  if a user is on a stage of creating a new (seller-distributor)  */
        if (this.state.creatingNew) SelectedSeller.createDistributorClicked.next();
    }
    deleteClickHandler = (e) => {
        Confirm.confirm(LM.getString("areYouSureDelete")).then(()=>{
            Loader.show()
            deleteSeller(this.state.selectedSeller.id).then(res => {
                Notifications.show(LM.getString("successSellerDeleting"), 'success');
                Loader.hide()
                SelectedSeller.createDistributorClicked.next()
                SelectedSeller.deleteSeller.next()
            }).catch((err) => {
                Notifications.show(LM.getString("businessDeletingFailed") + err, 'danger');
                Loader.hide()
            })
        })

    }

    approveClickedHandler = (e) => {
        Loader.show()
        putApproveSeller(this.state.selectedSeller.id, { business: this.state.selectedSeller }).then(res => {
            Notifications.show(LM.getString("businessActivatedSuccessfully"), 'success');
            this.setState({
                selectedSeller: Object.assign(this.state.selectedSeller, {status: 1})
            })
            Loader.hide()
        }).catch((err) => {
            Notifications.show(LM.getString("businessActivationFailed") + err, 'danger');
            Loader.hide()
        })



    }

    /** Function that loads a data from the selected seller object into the this.fields and this.distributorFields. 
     * If a value of a selected seller or distributor object field is null or undefined fieldsLoad() will clean the 
     * field on a screen from the previous inputs.
     */
    fieldsLoad = () => {
        this.setState({
            renderNumbersFields: false,
        }, () => {
            this.setState({
                renderNumbersFields: true,
            }, () => {

                Object.keys(this.fields).forEach((key) => {
                    if (this.state.selectedSeller[key] === null || this.state.selectedSeller[key] === undefined) {
                        if (this.fields[key].getValue().value === true) this.fields[key].setValue(false);
                        this.fields[key].setValue("");
                    } else {
                        this.fields[key].setValue(this.state.selectedSeller[key]);
                    }
                });
                Object.keys(this.distributorFields).forEach((key) => {
                    if (key === "logo" || key === "favicon") {
                        if (this.state.selectedDistributor[key] === null || this.state.selectedDistributor[key] === undefined) {
                            this.distributorFields[key].setValue(whiteBlock);
                        } else {
                            this.distributorFields[key].setValue(config.baseUrl + this.state.selectedDistributor[key]);
                        }
                    } else {
                        if (this.state.selectedDistributor[key] === null || this.state.selectedDistributor[key] === undefined) {
                            if (this.distributorFields[key].getValue().value === true) this.distributorFields[key].setValue(false);
                            this.distributorFields[key].setValue("");
                        } else {
                            this.distributorFields[key].setValue(this.state.selectedDistributor[key]);
                        }
                    }
                });
            })
        })

        /** if a seller was selected to be a distributor by a user */
        // if (this.state.saved) {
        // }
    }

    /** Function that clears a data from the selected seller object and from the this.fields object. */
    clearFields = () => {
        this.setState({
            renderFields: false,
        }, () => {
            this.setState({
                renderFields: true,
            }, () => {
                this.setState({ selectedSeller: {} });


                SelectedSeller.clearFields.next()
            })
        })
        this.defineSellerDistributor && this.defineSellerDistributor.setValue(false);
        this.setState({ saved: false });
    }

    /** Function that opens the GET html request (getDistributors from DataProvider).
     *  Saves an array of distributors from the server into distributors variable. */
    loadDistributorsList = (forceUpdate = false) => {
        getDistributors(forceUpdate).then(
            (res) => {
                this.setState({ distributors: res });
            }
        ).catch(
            (err) => {
                Notifications.show(LM.getString("loadDistributorsListFailed") + err, 'danger');
            }
        )
    }


    loadAgentsResponsibleList = (forceUpdate = false) => {
        getAgentsResponsible(forceUpdate).then(
            (res) => {
                this.setState({ agentsResponsible: res })
            }
        ).catch(
            (err) => {
                Notifications.show("agentsResponsible failed: " + err)

            }
        )
    }

    componentDidMount() {
        getBusinessProfiles().then(res => {
            this.setState({
                profiles: res
            })
        })
        getBusinessTags().then(res => {
            this.setState({
                tags: res
            })
        })
        this.loadDistributorsList();
        this.loadAgentsResponsibleList();
        SelectedSeller.setHeader.next(LM.getString("business"));
        if (SelectedSeller.currentSeller.getValue().id && SelectedSeller.currentDistributor.getValue().id) this.setState({
            selectedSeller: SelectedSeller.currentSeller.getValue(),
            selectedDistributor: SelectedSeller.currentDistributor.getValue(),
            distributorID: SelectedSeller.currentDistributor.getValue().distributor_id,
            saved: SelectedSeller.currentDistributor.getValue().is_distributor
        }, () => this.fieldsLoad());
        SelectedSeller.currentSeller.subscribe(x => {
            if(x.id){
                this.setState({ selectedSeller: x, saved: x.is_distributor }, () => this.fieldsLoad())
            }
        });
        SelectedSeller.currentDistributor.subscribe(x => {
            if(x.id){
                this.setState({ selectedDistributor: x, distributorID: x.distributor_id }, () => this.clearFields())
            }
        });
    }

    render() {
        
        /** Distributor Settings Header  */
        const distributorSettingsHeader = LM.getString("distributorSettings");

        /** data for "Agent Responsible" field (InputTypeSearchList InputUtils component) */
        const agentResponsibleProps = {
            options: [],
            title: LM.getString("agentResponsible"),
            id: "BusinessAgentResponsible",
            required: true
        };
        /** data for "Area" field (InputTypeSearchList InputUtils component) */
        const areaProps = {
            options: [],
            title: LM.getString("area"),
            id: "BusinessArea",
        };
        /** data for "Suppliers Profile" field (InputTypeSearchList InputUtils component) */
        const suppliersProfileProps = {
            title: LM.getString("suppliersProfile") + ":",
            id: "BusinessSuppliersProfile",
            required: this.state.creatingNew
        };
        /** data for "Profit Percentage Profile" field (InputTypeSearchList InputUtils component) */
        const profitPercentageProfileProps = {
            title: LM.getString("profitPercentageProfile") + ":",
            id: "BusinessProfitPercentageProfile",
            required: this.state.creatingNew
        };
        /** data for "Commissions Profile" field (InputTypeSearchList InputUtils component) */
        const commissionProfileProps = {
            title: LM.getString("commissionProfile") + ":",
            id: "BusinessCommissionsProfile",
            required: this.state.creatingNew
        };
        /** data for "System Colors Profile" field (InputTypeSearchList InputUtils component) */
        const systemColorsProfileProps = {
            options: LM.getString("systemColorsProfileOptions"),
            title: LM.getString("systemColorsProfile"),
            id: "BusinessSystemColorsProfile",
        };

        /** data for "Authorized Dealer" field (InputTypeNumber InputUtils component) */
        const authorizedDealerProps = {
            title: LM.getString("authorizedDealer"),
            id: "BusinessAuthorizedDealer",
            required: true

        };

        /** data for "Business Phone" field (InputTypePhone InputUtils component) */
        const businessPhoneProps = {
            prefixTwoDigits: true,
            title: LM.getString("businessPhone"),
            id: "BusinessBusinessPhone",
            required: true
        };
        /** data for "Additional Phone" field (InputTypePhone InputUtils component) */
        const additionalPhoneProps = {
            title: LM.getString("additionalPhone"),
            id: "BusinessAdditionalPhone",
        };
        /** data for "Support Phone Number" field (InputTypePhone InputUtils component) */
        const supportPhoneProps = {
            title: LM.getString("supportPhone"),
            id: "BusinessSupportPhone",
            required: true
        };

        /** data for "Business Name" field (InputTypeText InputUtils component) */
        const businessNameProps = {
            title: LM.getString("business_name"),
            id: "BusinessBusinessName",
            required: true,
            validationFunction: this.businessNameOuterValidationFunction,
            notValidMessage: LM.getString("businessName") + " " + LM.getString("isAlreadyExist"),
        };
        /** data for "City" field (InputTypeText InputUtils component) */
        const cityProps = {
            title: LM.getString("city"),
            id: "BusinessCity",
            required: true
        };
        /** data for "Address" field (InputTypeText InputUtils component) */
        const addressProps = {
            title: LM.getString("address"),
            id: "BusinessAddress",
            required: true
        };
        /** data for "Business Hours" field (InputTypeText InputUtils component) */
        const businessHoursProps = {
            title: LM.getString("businessHours"),
            id: "BusinessBusinessHours",
        };
        /** data for "Sub Domain" field (InputTypeText InputUtils component) */
        const subDomainProps = {
            title: LM.getString("subDomain"),
            id: "BusinessSubDomain",
        };
        /** data for "Free Text for Support" field (InputTypeText InputUtils component) */
        const freeTextSupportProps = {
            title: LM.getString("freeTextSupport"),
            id: "BusinessFreeTextSupport",
        };

        /** data for "Email" field (InputTypeEmail InputUtils component) */
        const emailProps = {
            placeholder: "email@example.com",
            title: LM.getString("email"),
            id: "BusinessEmail",
        };
        /** data for "Email Invoice" field (InputTypeEmail InputUtils component) */
        const emailInvoiceProps = {
            placeholder: "email@example.com",
            title: LM.getString("emailInvoice"),
            id: "BusinessEmailInvoice",
        };

        /** data for "Created Date" field (InputTypeDate InputUtils component) */
        const createdDateProps = {
            disabled: true,
            /** format: YY-MM-DD */
            min: "1900-01-01",
            title: LM.getString("createdDate"),
            id: "BusinessCreatedDate",
        };

        /** data for "Favorites" field (Tag InputUtils component) */
        const favoritesProps = {
            buttonText: LM.getString("favorites"),
            title: LM.getString("tags"),
            id: "BusinessFavorites",
        };
        /** data for "Recommended" field (Tag InputUtils component) */
        const recommendedProps = {
            buttonText: LM.getString("recommended"),
            title: "",
            id: "BusinessRecommended",
        };
        /** data for "For Internet" field (Tag InputUtils component) */
        const forInternetProps = {
            buttonText: LM.getString("forInternet"),
            title: "",
            id: "BusinessForInternet",
        };

        /** data for "Paragraph 20" field (InputTypeCheckBox InputUtils component) */
        const paragraphProps = {
            lableText: LM.getString("paragraph20"),
            id: "BusinessParagraph",
        };
        /** data for "Define a Seller as a Distributor" field (InputTypeCheckBox InputUtils component) */
        const defineSellerDistributorProps = {
            lableText: LM.getString("defineSellerDistributor"),
            id: "BusinessDefineSellerDistributor",
        };
        /** data for "User Security Management" field (InputTypeCheckBox InputUtils component) */
        const userSecurityManagementProps = {
            lableText: LM.getString("userSecurityManagement"),
            id: "BusinessUserSecurityManagement",
        };
        /** data for "Administrator" field (InputTypeCheckBox InputUtils component) */
        const administratorProps = {
            lableText: LM.getString("administrator"),
            id: "BusinessAdministrator",
        };
        /** data for "Security Setting Management for Users" field (InputTypeCheckBox InputUtils component) */
        const securitySettingManagementForUsersProps = {
            lableText: LM.getString("securitySettingManagementForUsers"),
            id: "BusinessSecuritySettingManagementForUsers",
        };

        /** data for "Logo" field (ImageUpload InputUtils component) */
        const logoProps = {
            title: LM.getString("logo"),
            id: "BusinessLogo",
        };
        /** data for "Favicon" field (ImageUpload InputUtils component) */
        const faviconProps = {
            title: LM.getString("favicon"),
            id: "BusinessFavicon",
        };

        /** button "Proceed" data  */
        const buttonProceed = LM.getString("save");
        /** button "Delete"  data  */
        const buttonDelete = LM.getString("delete");
        /** button "New"  data  */
        const buttonNew = LM.getString("new");
        /** button "Cancel"  data  */
        const buttonCancel = LM.getString("cancel");
        /** button "Save" data  */
        const buttonSave = LM.getString("save");
        const aproveSellerbtnName = LM.getString("activate");
        const deleteSellerBtnName = LM.getString("delete1");

        /** text note "Attention!" data  */
        const attention = LM.getString("attentionAfterSaving");

        return (
            this.state.renderFields ? <Container className="businessContainer">

                    <Container className="businessSettings">
                        {SelectedSeller.currentSeller.getValue().id || SelectedSeller.newSellerState ?

                            <React.Fragment>
                                <Row>
                                    <Col sm='4'>
                                        <InputTypeText
                                            {...businessNameProps}
                                            onChange={this.businessNameChanged}
                                            ref={(componentObj) => {
                                                this.fields["name"] = componentObj
                                            }}
                                        />
                                    </Col>
                                    <Col sm='4'>
                                        <InputTypeNumber
                                            {...authorizedDealerProps}
                                            ref={(componentObj) => {
                                                this.fields["business_license_number"] = componentObj
                                            }}
                                        />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm='4'>
                                        <InputTypeSimpleSelect
                                            {...agentResponsibleProps}
                                            options={this.state.agentsResponsible.map(agentResponsible => {
                                                return {label: agentResponsible.name, value: agentResponsible.id}
                                            })}
                                            ref={(componentObj) => {
                                                this.fields["agent_id"] = componentObj
                                            }}
                                        />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm='4'>
                                        {this.state.renderNumbersFields && <InputTypePhone
                                            {...businessPhoneProps}
                                            ref={(componentObj) => {
                                                this.fields["phone_number"] = componentObj
                                            }}
                                        />}
                                    </Col>
                                    <Col sm='4'>
                                        <InputTypePhone
                                            {...additionalPhoneProps}
                                            ref={(componentObj) => {
                                                this.fields["second_phone_number"] = componentObj
                                            }}
                                        />
                                    </Col>
                                </Row>
                                <Row>
                                    {false && <Col sm='4'>
                                        <InputTypeSearchList
                                            {...areaProps}
                                            ref={(componentObj) => {
                                                this.area = componentObj
                                            }}
                                        />
                                    </Col>}
                                    {/* <Col sm='4'>
                                        <InputTypeText
                                            {...cityProps}
                                            ref={(componentObj) => {
                                                this.fields["city"] = componentObj
                                            }}
                                        />
                                    </Col> */}
                                </Row>
                                <Row>
                                    <Col sm='8'>
                                        <InputTypeAddress
                                            {...addressProps}
                                            ref={(componentObj) => {
                                                this.fields["street"] = componentObj
                                            }}
                                        />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm='8'>
                                        <InputTypeText
                                            {...businessHoursProps}
                                            ref={(componentObj) => {
                                                this.fields["opening_hours"] = componentObj
                                            }}
                                        />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm='4'>
                                        <InputTypeEmail
                                            {...emailProps}
                                            ref={(componentObj) => {
                                                this.fields["contact_mail"] = componentObj
                                            }}
                                        />
                                    </Col>
                                    <Col sm='4'>
                                        <InputTypeEmail
                                            {...emailInvoiceProps}
                                            ref={(componentObj) => {
                                                this.fields["contact_account_mail"] = componentObj
                                            }}
                                        />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm='4'>
                                        <InputTypeDate
                                            {...createdDateProps}
                                            ref={(componentObj) => {
                                                this.createdDate = componentObj
                                            }}
                                        />
                                    </Col>
                                </Row>
                                {false && <Row className="align-items-end">
                                    <Col sm='auto'>
                                        <Tag
                                            {...favoritesProps}
                                            ref={(componentObj) => {
                                                this.favorites = componentObj
                                            }}
                                        />
                                    </Col>
                                    <Col sm='auto'>
                                        <Tag
                                            {...recommendedProps}
                                            ref={(componentObj) => {
                                                this.recommended = componentObj
                                            }}
                                        />
                                    </Col>
                                    <Col sm='auto'>
                                        <Tag
                                            {...forInternetProps}
                                            ref={(componentObj) => {
                                                this.forInternet = componentObj
                                            }}
                                        />
                                    </Col>
                                </Row>}
                                <Row>
                                    <Col sm='4'>
                                        <InputTypeSimpleSelect
                                            {...suppliersProfileProps}
                                            options={(this.state.profiles.supplier || []).map(v => {
                                                return {label: v.name, value: v.id}
                                            })}
                                            ref={(componentObj) => {
                                                this.fields['suppliers_profile_id'] = componentObj
                                            }}
                                        />
                                    </Col>
                                    <Col sm='4'>
                                        <InputTypeSimpleSelect
                                            {...profitPercentageProfileProps}
                                            options={(this.state.profiles.profit || []).map(v => {
                                                return {label: v.name, value: v.id}
                                            })}
                                            ref={(componentObj) => {
                                                this.fields['profitPercentagesProfile_id'] = componentObj
                                            }}
                                        />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm='4'>
                                        <InputTypeSimpleSelect
                                            {...commissionProfileProps}
                                            options={(this.state.profiles.commission || []).map(v => {
                                                return {label: v.name, value: v.id}
                                            })}
                                            ref={(componentObj) => {
                                                this.fields['commissionsProfile_id'] = componentObj
                                            }}
                                        />
                                    </Col>
                                </Row>
                                <Row>
                                    <Collapse isOpen={!this.state.selectedDistributor.section_20}>
                                        <Col sm='auto'>
                                            <InputTypeCheckBox
                                                {...paragraphProps}
                                                ref={(componentObj) => {
                                                    this.fields['section_20'] = componentObj
                                                }}
                                            />
                                        </Col>
                                    </Collapse>
                                </Row>
                                <Container className="createProductInputBlock businessTagBkgColor" onClick={this.highlightEditBtn}>
                                <Row><Col
                                    className="subHeader createProductBottom">{LM.getString('selectTags') + ':'}</Col></Row>
                                <TagsField
                                    className='businessTagBkgColor'
                                    disabled={this.state.disabled}
                                    tags={this.state.tags || []}
                                    ref={(componentObj) => {
                                        this.fields["tags"] = componentObj
                                    }}
                                />
                            </Container>
                                <Collapse
                                
                                    isOpen={this.state.creatingNew && !this.state.saved && SelectedSeller.currentDistributor.getValue().id && this.state.selectedDistributor.id == BusinessIdsEnum.PELETOK}>
                                    <Col>
                                        <Row className="businessBackground align-items-center justify-content-start">
                                            <Col sm='auto'>
                                                <InputTypeCheckBox
                                                    {...defineSellerDistributorProps}
                                                    ref={(componentObj) => {
                                                        this.defineSellerDistributor = componentObj
                                                    }}
                                                />
                                            </Col>
                                            <Col sm='auto'>
                                                <Button
                                                    className="businessButton businessButtonSmall businessButtonSave"
                                                    id="businessButtonSave"
                                                    onClick={this.saveClick}
                                                >
                                                    {buttonSave}
                                                </Button>
                                            </Col>
                                            <Col sm='auto'>
                                        <span className="businessNote">
                                            {attention}
                                        </span>
                                            </Col>
                                        </Row>
                                    </Col>
                                    {/*</Collapse>*/}

                                    {/* TODO: Uncomment this Distributor Fields block when all needed fields of distributor on server will be ready
                        <Collapse isOpen={this.state.saved} >
                            <Row >
                                <Col >
                                    <hr className="businessLine" />
                                </Col>
                            </Row>
                            <Row >
                                <Col className="subHeader businessMarginBottom">
                                    {distributorSettingsHeader}
                                </Col>
                            </Row>
                            <Row>
                                <Col sm='4'>
                                    <ImageUpload
                                        {...logoProps}
                                        ref={(componentObj) => { this.distributorFields["logo"] = componentObj }}
                                    />
                                </Col>
                                <Col sm='4'>
                                    <ImageUpload
                                        {...faviconProps}
                                        ref={(componentObj) => { this.distributorFields["favicon"] = componentObj }}
                                    />
                                </Col>
                            </Row>
                            <Row>
                                <Col sm='4'>
                                    <InputTypeText
                                        {...subDomainProps}
                                        ref={(componentObj) => { this.distributorFields["sub_domain"] = componentObj }}
                                    />
                                </Col>
                                <Col sm='4'>
                                    <InputTypeSearchList
                                        {...systemColorsProfileProps}
                                        ref={(componentObj) => { this.systemColorsProfile = componentObj }}
                                    />
                                </Col>
                            </Row>
                            <Row >
                                <Col sm='auto'>
                                    <InputTypeCheckBox
                                        {...userSecurityManagementProps}
                                        ref={(componentObj) => { this.userSecurityManagement = componentObj }}
                                    />
                                </Col>
                            </Row>
                            <Row>
                                <Col sm='4'>
                                    {this.state.renderNumbersFields && <InputTypePhone
                                        {...supportPhoneProps}
                                        ref={(componentObj) => { this.distributorFields["support_phone"] = componentObj }}
                                    />}
                                </Col>
                            </Row>
                            <Row >
                                <Col sm='auto'>
                                    <InputTypeCheckBox
                                        {...administratorProps}
                                        ref={(componentObj) => { this.distributorFields["is_admin"] = componentObj }}
                                    />
                                </Col>
                                <Col sm='auto'>
                                    <InputTypeCheckBox
                                        {...securitySettingManagementForUsersProps}
                                        ref={(componentObj) => { this.securitySettingManagementForUsers = componentObj }}
                                    />
                                </Col>
                            </Row>
                            <Row>
                                <Col sm='8'>
                                    <InputTypeText
                                        {...freeTextSupportProps}
                                        ref={(componentObj) => { this.distributorFields["free_text"] = componentObj }}
                                    />
                                </Col>
                            </Row>
                        </Collapse> */}
                                </Collapse>
                                 {this.state.creatingNew && <AddNewUser
                                     ref={obj=>this.addUser = obj}
                                     updateUsers={()=>{}}
                                     addUserClick={()=>{}}
                                     currentDistributor={this.state.selectedSeller.id}
                                     creatBusiness={true}
                                 />}
                                <Row>
                                    <Col sm='auto'>
                                        <Button
                                            className="businessButton businessButtonBig businessButtonBlue"
                                            id="businessButtonProceed"
                                            onClick={this.proceedClick}
                                        >
                                            {buttonProceed}
                                        </Button>
                                    </Col>
                                    <Col sm='auto'>
                                        <Button
                                            className="businessButton businessButtonBig businessButtonDark"
                                            id="businessButtonNew"
                                            onClick={this.newClick}
                                            hidden={this.state.creatingNew}
                                        >
                                            {buttonNew}
                                        </Button>
                                    </Col>
                                    {this.state.creatingNew &&
                                    <Col sm='auto'>
                                        <Button
                                            className="businessButton businessButtonBig businessButtonDark"
                                            id="businessButtonCancel"
                                            onClick={this.cancelClick}
                                        >
                                            {buttonCancel}
                                        </Button>
                                    </Col>
                                }
                                {AuthService.allowed('/activate_business') && !this.state.creatingNew &&
                                    this.state.selectedSeller && this.state.selectedSeller.status == BusinessStatusEnum.PENDING &&
                                    <Col sm='auto'>
                                        <Button
                                            color={"success"}
                                            className="businessButton businessButtonBig"
                                            id="businessButtonConfirm"
                                            onClick={this.approveClickedHandler}
                                        >
                                            {aproveSellerbtnName}
                                        </Button>
                                    </Col>}
                                {!this.state.selectedSeller.transactions && !this.state.selectedSeller.is_distributor && !this.state.creatingNew &&
                                    <Col sm='auto'>
                                        <Button
                                            color={"danger"}
                                            className="businessButton businessButtonBig"
                                            id="businessButtonDelete"
                                            onClick={this.deleteClickHandler}
                                        >
                                            {deleteSellerBtnName}
                                        </Button>
                                    </Col>}
                            </Row>

                        </React.Fragment>
                        : <></>
                    }

                    {!SelectedSeller.currentSeller.getValue().id ?

                            <Row>
                                <Col sm='auto'>
                                    <Button
                                        className="businessButton businessButtonBig businessButtonDark"
                                        id="businessButtonNew"
                                        onClick={this.newClick}
                                        hidden={this.state.creatingNew}
                                    >
                                        {buttonNew}
                                    </Button>
                                </Col>
                            </Row>
                            : <></>
                        }
                    </Container>

            </Container> : <> </>
        )
    }
}
export default Business;