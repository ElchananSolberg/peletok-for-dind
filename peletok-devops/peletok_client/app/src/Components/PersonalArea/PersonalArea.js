import React, { Component } from 'react';
import './PersonalArea.css';

import { Button, Container, Row, Col } from 'reactstrap';

import { LanguageManager as LM } from '../LanguageManager/Language';
import { Notifications } from '../Notifications/Notifications';


import { getUserForPersonalArea } from '../DataProvider/DataProvider'

import InputTypeText from '../InputUtils/InputTypeText';
import InputTypeDate from '../InputUtils/InputTypeDate';
import InputTypePhone from '../InputUtils/InputTypePhone';

import { ChangePassword } from '../selectUser/changePass';

/**
 * The main component of Personal Area screen. 
 * It works with separate css file - PersonalArea.css
 */

export class PersonalArea extends Component {
    constructor(props) {
        super(props);


        this.state = {
            /** working variable that will contain an object of a current user data */
            currentUser: {},
            /** working variable that indicates if a user is on the stage of changing a password */
            isChangingPassword: false,
        }
    }

    /** "Change Password" button click handler */
    changePasswordButtonClicked = (_e) => {
        this.toggleModal();
    }

    /** toggles modal window (inside <ChangePassword>) */
    toggleModal = () => {
        this.setState({ isChangingPassword: !this.state.isChangingPassword })
    }

    /** Function that loads a data from currentUser object into the input fields on the screen. */
    fieldsLoad = () => {
        this.name.setValue(this.state.currentUser.first_name + " " + this.state.currentUser.last_name);
        /** because phone_number can be null, we need to check that it is not so */
        this.state.currentUser.phone_number && this.phoneNumber.setValue(this.state.currentUser.phone_number);
        this.username.setValue(this.state.currentUser.login_name);
        let token = window.localStorage.getItem('token')

        this.token.setValue(token ? `*******${token.substr(token.length - 4)}` : '');
        /** because last_login can be null, we need to check  that it is not so */
        this.state.currentUser.last_login && this.lastLoginDate.setValue(this.state.currentUser.last_login);
    }

    loadUserData = () => {
        getUserForPersonalArea(true).then(
            res => {
                this.setState({ currentUser: res }, () => { this.fieldsLoad() });
            }
        ).catch(
            function (error) {
                Notifications.show(error, 'danger');
            }
        );
    }

    componentDidMount() {
        this.loadUserData()
    }

    render() {
        /** Screen Header  */
        const header = LM.getString('personalArea');

        /** data for "First Name" field (InputTypeText InputUtils component) */
        const nameProps = {
            disabled: true,
            title: LM.getString("name") + ':',
            id: "PersonalArea_Name",
        };
        /** data for "Username" field (InputTypeText InputUtils component) */
        const usernameProps = {
            disabled: true,
            title: LM.getString("username") + ':',
            id: "PersonalArea_Username",
        };

        const tokenProps = {
            disabled: true,
            title: LM.getString("token") + ':',
            id: "PersonalArea_Token",
        };

        /** data for "Phone Number" field (InputTypePhone InputUtils component) */
        const phoneNumberProps = {
            disabled: true,
            title: LM.getString("phoneNumber"),
            /** A final id of a prefix input (first one with 3 numbers) will look so:
            * "InputTypeNumberInputTypePhonePrefix"+id
            * A final id of a number input (second one with 7 numbers) will look so:
            * "InputTypeNumberInputTypePhoneNumber"+id
            */
            id: "PersonalAreaPhoneNumber",
        };

        /** data for "Last Login" field (InputTypeDate InputUtils component) */
        const lastLoginDateProps = {
            disabled: true,
            /** format: YY-MM-DD */
            min: "1900-01-01",
            title: LM.getString("lastLogin"),
            id: "PersonalAreaLastLoginDate",
        };

        /** data for "Change Password" button */
        const changePasswordButtonText = LM.getString("changePassword");

        return (
            <Container className="personalAreaContainer">
                <Row >
                    <Col className="personalAreaHeader">
                        {header}
                    </Col>
                </Row>
                <Container className="personalAreaSettings">
                    <Row>
                        <Col sm='4'>
                            <InputTypeText
                                {...nameProps}
                                ref={(componentObj) => { this.name = componentObj }}
                            />
                        </Col>
                        <Col sm='4'>
                            <InputTypePhone
                                {...phoneNumberProps}
                                ref={(componentObj) => { this.phoneNumber = componentObj }}
                            />
                        </Col>
                        <Col sm='4'>
                            <InputTypeText
                                {...tokenProps}
                                ref={(componentObj) => { this.token = componentObj }}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm='4'>
                            <InputTypeText
                                {...usernameProps}
                                ref={(componentObj) => { this.username = componentObj }}
                            />
                        </Col>
                        <Col sm='4'>
                            <InputTypeDate
                                {...lastLoginDateProps}
                                ref={(componentObj) => { this.lastLoginDate = componentObj }}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm='auto'>
                            <Button
                                className="personalAreaButton"
                                id="personalAreaChangePasswordButton"
                                onClick={this.changePasswordButtonClicked}
                            >
                                {changePasswordButtonText}
                            </Button>
                        </Col>
                    </Row>
                    <ChangePassword
                        userId={this.state.currentUser.id}
                        theUser={{ 'first_name': this.state.currentUser.first_name, 'last_name': this.state.currentUser.last_name }}
                        isOpen={this.state.isChangingPassword}
                        toggle={this.toggleModal}
                    />
                </Container>
            </Container>
        )
    }
}