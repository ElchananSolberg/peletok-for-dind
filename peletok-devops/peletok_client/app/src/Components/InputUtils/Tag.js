import React, { Component } from 'react';
import './InputUtils.css';
import { Row, Col, Button, Badge } from 'reactstrap';

import { Tooltip } from './Tooltip';

/**
 * Tag component lets to show an outline button that will change to standart after the click on it.
 * After the click on it an 'X' button (<Badge/>) inside of it will be shown. 
 * After the click on the 'X' button, outside standart button  will change to outline button and 'X' button inside of it will be hidden. 
 * 
 * it can render the title above the button.
 * it can render the tooltip to the side of the title.
 * It validates the value is valid to the function sent in  this.props.validationFunction;
 * if there are any validation issues the tag will change a border color to red;
 * if there are any validation issues bellow the tag will be the error message
 */
class Tag extends Component {
    constructor(props) {
        super(props);

        this.state = {
            value: this.props.default,
            validationFuncs: [
                {
                    /**  Validates the value is valid to the function sent in  this.props.validationFunction */
                    f: this.props.validationFunction || (() => (true)),
                    msg: this.props.notValidMessage,
                },
            ],
            errorMessage: '',
        }
    }

    badgeOnClick = (e) => {
        if(this.props.onBatgeClick){
            this.props.onBatgeClick(e)
        }
        this.setState({ value: false });
        e.stopPropagation();  // we don't want buttonOnClick to work when user clicked on badge
    }
    buttonOnClick = (e) => {
        if(this.props.onClick){
            this.props.onClick(e)
        }
        this.setState({ value: true });
    }

    /** summarizing validation function
    * is running 2 times: when input is loosing focus and when getValue function is running (by programmer-user)
    * */
    runValidation = () => {
        let summarizingValidationResult = (!this.props.required && !this.state.value) || this.state.validationFuncs.every((validation) => validation.f(this.state.value));
        if (!summarizingValidationResult) {
            let messages = [];
            this.state.validationFuncs.forEach((validation) => {
                if (!validation.f(this.state.value)) {
                    messages.push(validation.msg);
                }
            })
            this.setState(({ errorMessage: messages.join(`\n`) }));
        } else {
            this.setState(({ errorMessage: '' }));
        }
        return summarizingValidationResult;
    }

    getValue = () => {
        let summarizingValidationResult = this.runValidation();
        return { valid: summarizingValidationResult, value: this.state.value };
    }

    setValue = (newValue) => {       
        if (typeof newValue === 'boolean'){
            this.setState({ value: newValue });
            return true;
        }
        return false;
    }

    render() {
        let disabled = this.props.disabled;
        let id = this.props.id.replace(/\s+/g, '');
        let title = this.props.title;
        let tooltipText = this.props.tooltip;

        return (
            <>
                <Row>
                    <Col className={"d-flex justify-content-between line-height-1 " + (title ? 'py-1' : '') }>
                        {title ?
                            <div className="fs-17 white-space">{title}</div>
                            : <></>
                        }
                        {tooltipText ?
                            <Tooltip tooltipText={tooltipText} id={id} />
                            : <></>
                        }
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <div className={(this.state.errorMessage ? "invalidDiv" : "")} onBlur={this.runValidation}>
                            <Button disabled={disabled} className={"py-0 " + (this.state.value ? "utilsTagSelected" : "utilsTagNotSelected") + (this.state.errorMessage ? " invalidDiv" : "")} id={`Tag${id}`} 
                            onClick={this.buttonOnClick} onBlur={this.runValidation}>
                                {this.state.value ? <Badge className="mx-1 utilsBadge" onClick={disabled ? null : this.badgeOnClick} >X</Badge>
                                    : <React.Fragment/>
                                }
                                <span className="mx-1">{this.props.buttonText}</span>
                            </Button>
                        </div>
                        <div className='ws-pre errorMessageDiv'>{this.state.errorMessage}</div>
                    </Col>
                </Row>
            </>
        )
    }
}
export default Tag;