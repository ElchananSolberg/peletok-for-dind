import React, { Component } from 'react';
import {
    Button, Input, FormFeedback,
    Label, FormGroup, Form, Col
} from 'reactstrap';
import { LanguageManager as LM } from "../LanguageManager/Language"


export class ForgotPasswordResetPass extends Component {
    render() {
        return (
            <Form autoComplete='new-password'>
                <FormGroup>

                    <Label size='sm'>{LM.getString('enterNwePass')}</Label>
                    <Input invalid={this.props.errMsg.length !== 0} size='sm' type="password" name="passOne" id="passOne"
                        onChange={this.props.onChangeOne} />
                    <FormFeedback id='enterNwePassErrMsg' size='sm'>{this.props.errMsg}</FormFeedback>
                    <Label size='sm'>{LM.getString('enterNewPassTwo')}</Label>
                    <Input invalid={this.props.errMsg.length !== 0} size='sm' type="password" name="passTwo" id="passTwo"
                        onChange={this.props.onChangeTwo} />
                    <FormFeedback id="enterNewPassTwoErrMsg" size='sm'>{this.props.errMsg}</FormFeedback>
                    <Col md="5" sm="12" className='px-0 mt-2 mt-md-4'>
                        <Button block size='lg' className={'fs-17 border-radius-3 bc-blue-1 border-unset'} id="resetPassword" onClick={this.props.onSubmit}>
                            {LM.getString("resetPassword")}
                        </Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}
