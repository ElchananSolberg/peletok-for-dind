'use strict';
const {addPermission} = require('../dbUtils/addPermission');

module.exports = {
  up: (queryInterface, Sequelize) => {
   return addPermission(queryInterface, 'אישור להקמת עסק', 'ui', '/activate_business', 4)
  },

  down: (queryInterface, Sequelize) => {
   return Promise.resolve()
  }
};
