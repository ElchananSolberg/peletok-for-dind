from selenium.webdriver.common.by import By


class TehinatChashmalPage:
    tehinatChashmal_contract_number = (By.ID, "InputTypeNumbercontractNumberInput")
    tehinatChashmal_amount_money_to_charge = (By.ID, "InputTypeNumbermoneyToCharge")
    tehinatChashmal_total_payment = (By.ID, "InputTypeNumberTotalPayment")
    tehinatChashmal_phone_pre_number = (By.ID, "InputTypeNumberInputTypePhonePrefixsupplierTelnumTitle")
    tehinatChashmal_phone_senven_number = (By.ID, "InputTypeNumberInputTypePhonePrefixsupplierTelnumTitle")
    tehinatChashmal_charge_button = (By.ID, "electricityChargeButton")
    tehinatChashmal_success_msg = (By.CLASS_NAME, "detailBorder")
    tehinatChashmal_cancelButton = (By.ID, "cancelButton")
    commission_cost = (By.CSS_SELECTOR, "p.fs-22")