import React, { Component } from 'react';
import './Banner.css';
import img from './sample_image.png';

import { LanguageManager as LM } from '../LanguageManager/Language'

import { getAllBanners } from '../DataProvider/DataProvider'

import { config } from '../../utils/Config'

/** Ad Banner component to be placed in any place you need (like footer or menu). It spreads on all given width. 
You can change its Height by sending the proper bannerType in the props (like bannerType='menu').
Attention:  Ad Banner component uses his own .css file - Banner.css
*/
class Banner extends Component {
    constructor(props){
        super(props);
        this.state = { /** TODO  every value here can be changed to this.props.someAttribute   or maybe to may set to the data from server */
            bannerType : this.props.bannerType,  //  bannerType should be:  'footer' or 'menu'
            menuHeight: '100px',                // if bannerType is 'menu' banners height will be this.state.menuHeight 
            footerHeight: '200px',              // if bannerType is 'footer' banners height will be this.state.footerHeight
            adUrl: "http://peletop.co.il/",

            banners: [],
            banner: undefined,
            bannerDuration: 0,
        }
    }

    /** <img/> click handler */
    bannerClicked = () => { 
        window.open( this.state.banner !== undefined ? this.state.banner.link_url : this.state.adUrl);        
    };

    loadBannersList = (forceUpdate = false) => {
        getAllBanners(forceUpdate)
        .then(
            (res) => {               
                if(this.state.bannerType === 'footer') {
                    /** TODO: change a string "לייטבוקס" to a real value (i think it should be with a use of a LM) when a server will be ready */
                    this.setState({ banners : res.filter(banner=>banner.location === "לייטבוקס") }, ()=> {
                        let outerIndex=0; 
                        if (this.state.banners[outerIndex]) this.repeatSetBanner(outerIndex);
                    });
                } else {
                    /** TODO: change a string "לייטבוקס" to a real value (i think it should be with a use of a LM) when a server will be ready */
                    this.setState({ banners : res.filter(banner=>banner.location !== "לייטבוקס") }, ()=> {                        
                        let outerIndex=0;
                        if (this.state.banners[outerIndex]) this.repeatSetBanner(outerIndex);
                    });
                }
            }
        )
        .catch(
            (err) => {               
                alert(LM.getString("loadBannersListFailed") + err );
            }
        )
    }

    repeatSetBanner = (index)=>{
        let banner = this.state.banners[index];         
        this.setBanner(index);
        setTimeout(() => {            
            if(index == this.state.banners.length - 1){
                index = 0;
            } else {
                index++;
            }            
            this.repeatSetBanner(index);
        },banner.duration*1000);
    }

    setBanner = (index) => {
        this.setState({ banner : this.state.banners[index] });
      
    }
   
    componentDidMount() {
        this.loadBannersList();
    }

    render() {  /** renders <div/> AND <img/> inside. 
        You CAN NOT throw away the <div/> because it always saves its own height and width - 
        NOT LIKE the <img/> that collapses to a small inline element if there are problems with its src.
        This is the reason why only the divs css-class ("banner") has background-color attribute.
        */
       let myHeight = this.props.height || "200px";
       if (this.props.bannerType === 'footer'){
            myHeight = this.state.footerHeight;
        }
        else if (this.props.bannerType === 'menu'){
            myHeight = this.state.menuHeight;
        }
        return (
            <div className="adBannerBanner" style={{height:myHeight}}>
                <img className="adBannerImage" onClick={this.bannerClicked} src={this.state.banner!== undefined ? (config.baseUrl + this.state.banner.banner_url) : img} alt={"AD"}/>
            </div>
        );
    }
}

export default Banner;