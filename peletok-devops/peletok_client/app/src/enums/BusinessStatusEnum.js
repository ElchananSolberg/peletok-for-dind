
export  class BusinessStatusEnum {
    static ACTIVE   = 1    
    static LOCKED   = 3    
    static FROZEN   = 2    
    static PENDING  = 4    
}