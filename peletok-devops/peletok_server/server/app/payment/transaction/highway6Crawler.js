peletalkCrawler = require('./peletalkCrawler') ;
/***
 * Payment manager Road6 
 * @returns link   for  payment
 */


class Highway6 extends peletalkCrawler {
    getHostname() { return "mybill.kvish6.co.il"; }
}

module.exports = Highway6;