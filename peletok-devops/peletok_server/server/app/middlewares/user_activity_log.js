const models = require('../index.js');
const whiteList = require('./path_white_list')

const Role = models.role.model;
const Permission = models.permission.model
const { getNumStatus } = require("../utils");


const UserActivityLogMiddleware = async (req, res, next) => {
    try {

        let uri = req.url.split('/api/v1')[1]
        if(!uri) {uri = req.url}
        // remove query params from uri
        uri = uri.split('?')[0]
        
        // filter last uri element if uri has params example "/user/:userId"
        uriArr = uri.split('/');
        uri = uriArr.filter(u=>!/^\d+$/.test(u)).join('/') 
        let path = req.method + " " + (uri[uri.length - 1] == '/' ? uri.slice(0, -1) : uri)
        let activity = await models.userActivityLog.model.create({
            path,
            full_url: req.url,
            query: req.query ? JSON.stringify(req.query) : '',
            params: req.params ? JSON.stringify(req.params) : '',
            body: req.body ? JSON.stringify(req.body) : '',
            user: req.user ? JSON.stringify(req.user) : '',
            ip: JSON.stringify((req.headers['x-forwarded-for'] || req.connection.remoteAddress).split(":")),

        })
        req.activity_id = activity.id;
    } catch (e){
        console.error(e)
    }
    return next()
        
    
}

module.exports = UserActivityLogMiddleware
