const express = require('express');
const models = require('../index')
const Supplier = models.supplier.model;
const Sequelize = require('sequelize');
const Op = Sequelize.Op
const supplierRouter = express.Router();
// const moment = require('moment')
// const {onlyIfContainOne, mapByType} = require('./apiUtils')
const {cleanRawKeys, getTypeNum, uploadFile, getNumStatus} = require('../utils')
const path = require("path");
const suppliersLogosDir = path.join(__dirname, '../rest/restData/public/suppliersLogos')
const {filterIfAllObjectsFieldsMissing, mapByType} = require('./apiUtils')
const manualCardsSupplierId = 1
const activeSupplierStatus = 1

supplierRouter.post("/",async (req, res) => {
    
    let data = await uploadFile(req, suppliersLogosDir)
    const supplier = await Supplier.create({
        name: data.name, 
        type: data.type, 
        credit_card_id: data.credit_card_id, 
        order: data.order || null, 
        status: data.status == 'true' ? getNumStatus('active', 'Supplier') : getNumStatus('inactive', 'Supplier'), 
        max_percentage_profit: data.max_percentage_profit, 
        min_percentage_profit: data.min_percentage_profit, 
        max_payment: data.max_payment,
        Address: {
            country: data.country,
            city: data.city,
            street: data.street,
            number: data.number,
            zip_code: data.zip_code,
        },
        SupplierContact: {
            name: data.name,
            phone: data.contact_phone_number,
        },
        Document: filterIfAllObjectsFieldsMissing([
            {url: data.fileName, type: getTypeNum('logo', 'Document')}
        ],'url'),  
    },{
        include:[
            {model: models.address.model, as: 'Address'},
            {model: models.supplierContact.model, as: 'SupplierContact'},
            {model: models.document.model, as: 'Document'}
        ]
    })

    res.send(supplier)
} );

supplierRouter.get("/",async (req, res) => {
    let whereObj =  {'$Business.id$': {[Op.ne]: null},id:{[Op.not]:manualCardsSupplierId}}
    if (req.query.filter) {
        whereObj.status = activeSupplierStatus
    }
    let suppliers = await models.supplier.model.findAll({
        attributes: [
            Sequelize.literal(`
                DISTINCT ON("Supplier"."id") "Supplier"."id" AS "Supplier.id",
                "SupplierColor"."logo_background" AS "logo_background_color",
                "Document"."url" AS "image"
                `),
            'name',
            'order',
            'type'
           ],
        include: [
            {
                model: models.business.model,
                attributes: ['id'],
                through: {
                    attributes: [['supplier_id', 'id']],                   
                    where: {is_authorized: true, business_id: req.user.business_id}
                },
                as: 'Business',
                require: true
            },
            {
                model: models.document.model,
                as: 'Document',
                where: {type: getTypeNum('image', 'Document')},
                through: {
                    attributes: [['supplier_id', 'id']],
                },
                require: false,
            },
            {
                model: models.supplierColor.model,
                as: 'SupplierColor',
                require: false,
            },
        ],
        raw: true,
        where: whereObj,
        // order: [[Sequelize.literal('order'), 'ASC']],
        includeIgnoreAttributes: false
    }).catch(e=>console.error(e));
    res.send(mapByType('supplier',suppliers.map(s=>cleanRawKeys(s, false))))

})

supplierRouter.get("/prepaid/:supplier_id",async (req, res) => {
    let [supplier, images] = await Promise.all([models.supplier.model.findOne({
            attributes: [
            'id',
            'name',
            'order',
            'type',
             Sequelize.literal(`
                "SupplierColor"."favorite_star_color" AS "favorite_star_color",
                "SupplierColor"."product_background" AS "product_card_background_color",
                "SupplierColor"."chosen_background" AS "chosen_card_background_color",
                "SupplierColor"."logo_background" AS "logo_background_color",
                "SupplierColor"."font" AS "font_color"
             `),
           ],
            include: [
                {
                    model: models.supplierColor.model,
                    as: 'SupplierColor',
                },
                {
                    model: models.business.model,
                    attributes: ['id'],
                    through: {
                        attributes: [['supplier_id', 'id']],                   
                        where: {is_authorized: true}
                    },
                    as: 'Business',
                    require: true
                }
            ],
            raw: true,
            where: {
                id: req.params.supplier_id,
                '$Business.id$': {[Op.ne]: null}
            },
            includeIgnoreAttributes: false
        }),
        models.document.model.findAll({
            where:  {type: { [Op.or]: [getTypeNum('backgroundImage', 'Document'), getTypeNum('image', 'Document')]}},
            include: [
                {
                    model:models.supplier.model,
                    as: 'Supplier',
                    where: {id: req.params.supplier_id}
                }
            ]
        })
    ]).catch(e=>console.error(e));
    images.map((image=>{
        if (supplier) {
            if (image.type ==getTypeNum('backgroundImage', 'Document') ) {
                supplier =Object.assign(supplier, {background_image: (image || {}).url}) 
            }
            if (image.type ==getTypeNum('image', 'Document')) {
                supplier =Object.assign(supplier, {image: (image || {}).url}) 
            }
        } 
    }))
    res.send(supplier)
})

supplierRouter.get("/:supplier_id",async (req, res) => {

    let [supplier, images] = await Promise.all([models.supplier.model.findOne({
            attributes: [
            'id',
            'name',
            'order',
            'type',
            'credit_card_id',
            'max_payment',
            'max_percentage_profit',
            'min_percentage_profit',
            'status',
            'default_creation',
            'cancel_option',
             Sequelize.literal(`
                "SupplierContact"."name" AS "contact_name",
                "SupplierContact"."phone" AS "contact_phone_number",
                "SupplierColor"."logo_background" AS "logo_background_color",
                "SupplierColor"."font" AS "font_color",
                "SupplierColor"."product_background" AS "product_card_background_color",
                "SupplierColor"."chosen_background" AS "chosen_card_background_color",
                "Address"."street" AS "address"
             `),
           ],
            include: [
                {model: models.supplierContact.model, as: 'SupplierContact'},
                {model: models.address.model, as: 'Address'},
                {
                    model: models.supplierColor.model,
                    as: 'SupplierColor',
                },
                {
                    model: models.business.model,
                    attributes: ['id'],
                    through: {
                        attributes: [['supplier_id', 'id']],                   
                        where: {is_authorized: true}
                    },
                    as: 'Business',
                    require: true
                }
            ],
            raw: true,
            where: {
                id: req.params.supplier_id,
                '$Business.id$': {[Op.ne]: null}
            },
            includeIgnoreAttributes: false
        }),
        models.document.model.findAll({
            where: {type: getTypeNum('image', 'Document')},
            include: [
                {
                    model:models.supplier.model,
                    as: 'Supplier',
                    where: {id: req.params.supplier_id}
                }
            ]
        })
    ]).catch(e=>console.error(e));
    supplier = supplier ? Object.assign(supplier, {image: images[0] ? images[0].url : ''}) : ''
    res.send(supplier)
})


supplierRouter.put("/:supplier_id",async (req, res) => {

    const CurrentBusinessSupplier = models.supplier.model.scope({ method: ['BusinessSupplier', req.user.business_id]});
    const supplier = await CurrentBusinessSupplier.findOne({
        where: {id: req.params.supplier_id},
        include: [
            {model: models.address.model, as: 'Address', required: false},
            {model: models.document.model, as: 'Document', required: false},
            {model: models.supplierContact.model, as: 'SupplierContact', required: false},
            {model: models.supplierColor.model, as: 'SupplierColor', required: false},
        ]
    }).catch(e=>console.error(e));
    let logo = supplier.Document.find(d=>d.type==getTypeNum('image', 'Document'))
    if(!supplier){
        res.send(401, 'Unauthorized')
    } else {
        let data = await uploadFile(req, suppliersLogosDir)
        let address;
        if(!supplier.Address){
            address = await models.address.model.create({street: data.address})
        }
        await Promise.all([supplier.update({
                    name: data.name,
                    type: data.type,
                    credit_card_id: data.credit_card_id,
                    order: data.order || null, 
                    status: data.status == 'true' ? getNumStatus('active', 'Supplier') : getNumStatus('inactive', 'Supplier'),
                    max_percentage_profit: data.max_percentage_profit,
                    min_percentage_profit: data.min_percentage_profit,
                    max_payment: data.max_payment,
                    default_creation: data.default_creation,
                    cancel_option: data.cancel_option,
                    address_id: address ? address.id : supplier.Address.id
                }),
                supplier.SupplierContact ? supplier.SupplierContact.update({
                    phone: data.contact_phone_number,
                    name: data.contact_name, 
                }) : models.supplierContact.model.create({
                    phone: data.contact_phone_number,
                    name: data.contact_name,
                    supplier_contact_id: supplier.id 
                }),
                data.validPath ? (logo ? logo.update({
                    url: data.validPath
                }) : supplier.addDocument({
                    url: data.validPath,
                    type:getTypeNum('image', 'Document'),

                },{through:{}})) : Promise.resolve(),

                supplier.Address ? supplier.Address.update({
                    street: data.address,
                }) : Promise.resolve(),

                supplier.SupplierColor ?  supplier.SupplierColor[0].update({
                    logo_background: data.logo_background_color,
                    font: data.font_color,
                    product_background: data.product_card_background_color,
                    chosen_background: data.chosen_card_background_color,
                }) : Promise.resolve()

        ]).catch(e=>console.error(e));

        res.send('success')
    }

})




module.exports.supplierRouter = supplierRouter;