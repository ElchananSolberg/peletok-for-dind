'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('RoleAllowedRole',
      {
        role_id: {
          type: Sequelize.INTEGER,
        },
        allowed_role_id: {
          type: Sequelize.INTEGER,
        },
        created_at: Sequelize.DATE,
        updated_at: Sequelize.DATE,
        
      }
  )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('RoleRole')
  }
};
