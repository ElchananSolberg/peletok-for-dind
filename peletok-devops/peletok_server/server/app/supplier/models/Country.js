const BaseModel = require('../../BaseModel');
const {DataTypes} = require("sequelize");

/**
 * Country model
 */
class Country extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            name: {
                type: DataTypes.STRING(256),
                allowNull: true
            },
        };
        this.prefix = "SUPL";
        this.statusField = true;
        this.statusTimestampField = true;
        this.statusAuthor = true;
    }

    /**
     * creates associate for Country model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.hasMany(models.cost.model, { as: "Cost"})

    }
}
module.exports = new Country();
