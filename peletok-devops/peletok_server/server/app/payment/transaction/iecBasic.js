const soap = require('soap');
const logs = require('../../logger')
const path = require('path');
const ResInfo = require('../../resInfo')
const paymentBasic = require('../paymentBasic')
const LM = require('../../config/LanguageManager').LM
//TODO   to   change  url  to  "./wsdl/iecPayment.wsdl"
// Move the user name and password and others Means of identification to a config file and read them from there
const url = path.join(__dirname, "./wsdl/iecTest.wsdl")
const reportView = require("../../transaction/view/ReportView")
const user = "p22?p"
const pass = "ad22?p"
const kodSapak = 5
const kod_emda = 2
const terminal = 2
const teur_emda = 2
const asmachtaYomi = 155
const asmachIshur = 1555
const taarichAsmachtaIshur = new Date().getTime()
const misparIska = 1555
const tashlum = 1
const models = require('../../index')


/***
    * Payment  BasicIEC to   pay IEC  Matam and IEC  Bill 
    * "$value"- it's  "key" of  IEC 
       */
class BasicIEC extends paymentBasic {

    constructor(contract, price, idAppeal, businessId,req) {
        super()
        this.contract = contract
        this.price = price
        this.idAppeal = idAppeal
        this.businessId = businessId
        this.info = new ResInfo(req)
        this.cancel = null
        this.details = null
        this.prePay = null
        this.approval = null
        this.revert = null
        this.req=req
    }

    /***
   * check  what price of  bil/product for  transactions
   * 
   */
    getPrice() {
        this.info.price = this.price
        return this.info
    }


    /***
         * step 1 to  payment for  IecMatam (Sales question)
         * 
         */
    async prePaidDet() {
        return await this.request(this.details, "")

    }

    /***
      * step 2 to payment for  Iec (Request payment execution)
      * 
      */
    async prePaidPay() {
        return await this.request(this.prePay, "")
    }
    /***
      *revert data of payment
      */
    async revertPayment(transationId, businessId) {
        let transactionOld = await models.transaction.model.findOne(
            {
                where: {

                    id: transationId,
                    business_id: businessId,
                    status: 4,
                }, include: [
                    {
                        model: models.transactionData.model,
                        as: 'transactionData',
                        where: { contract_number: this.contract }
                    }],

                raw: true
            })
        if (!transactionOld) {
            this.info.setErrMessage(LM.getErrorBasic(), "transaction_id is unsuitable to contract ")
            return this.info
        }



        const revert = await this.request(this.revert, "")


        if (this.info.error.message) { return this.info }

        transactionOld["appeal_id"] = revert["o_zihuy_pniya"]["$value"]
        await models.transaction.model.update({ appeal_id: revert["o_zihuy_pniya"]["$value"] }, { where: { id: transactionOld.id } })



        this.info.send = revert["o_zihuy_pniya"]["$value"]
        return this.info
    }

    /***
       *Canceled payment
       * 
       */
    async cancelPayment() {
        let idAppealOld = await reportView.model.findOne(
            {
                where: {
                    appeal_id: this.idAppeal,
                    business_id: this.businessId,
                    contract_number: this.contract
                }

            })
        if (!idAppealOld) {
            this.info.setErrMessage(LM.getErrorBasic(), "idAppeal no proper ")
            return this.info
        }
        return await this.request(this.cancel, this.idAppeal)
    }


    /***
       *approval payment
       * 
       */
    async approvall() {

        let idAppealOld = await reportView.model.findOne(
            {
                where: {
                    appeal_id: this.idAppeal,
                    business_id: this.businessId,
                    status: 4,
                    contract_number: this.contract
                }

            })
        if (!idAppealOld) {
            this.info.setErrMessage(LM.getErrorBasic(), "idAppeal no proper ")
            return this.info
        }
        this.info.price = idAppealOld["price"]
        return await this.ReqApproval(this.idAppeal)

    }
    /***
  *step3 payment for Iec ( and  for approval payment ) 
  * 
  */
    async ReqApproval(idAppeal) {
        const resApproval = await this.request(this.approval, idAppeal)
        if (this.info.error.message) { return this.info }
        this.info.isPaid = true
        logs.info("approval is ", resApproval["o_ishur_bitzuaa"]["$value"])
        this.info.confirmation = resApproval["o_ishur_bitzuaa"]["$value"] + resApproval["o_mispar_zhiuy"]["$value"]
        this.info.send = { "success": { "kod": this.info.kod, "isPaid": resApproval["o_ishur_bitzuaa"]["$value"] + idAppeal } }
        return this.info
    }

    /***
      * Pay  to IEC  
       @returns number  of   confirmation
            */
    async pay() {
        if ((await this.isDemoTransactionUser())) {
            return this.demoRespons()
        }
        else {
            const details = await this.prePaidDet()
            if (details.error) {
                return details
            }
            const prePaid = await this.prePaidPay()
            if (prePaid.error) {
                return prePaid
            }


            const idAppeal = prePaid["o_zihuy_pniya"]["$value"]
            this.info.billNumber = prePaid["o_invoice"]["$value"]
            this.info.kod = prePaid["o_kod"]["$value"]
            this.info.externalId = idAppeal
            logs.info(" idAppeal is ", idAppeal)
            return await this.ReqApproval(idAppeal)


        }
    }

    /***
     basic request  to  IEC
             */
    async request(kodProcess, idAppeal) {
        const args = {
            i_user: user,
            i_password: pass,
            i_kod_sapak: kodSapak,
            i_kod_emda: kod_emda,
            i_teur_emda: teur_emda,
            i_kod_process: kodProcess,
            i_contract: this.contract,
            i_schom: this.price,
            i_mispar_kupa: "",
            i_terminal: terminal,
            i_zihui_pniya: idAppeal,
            i_taarich_asmachta_ishur: taarichAsmachtaIshur,
            i_mispar_iska: misparIska,
            i_mispar_asmachta_ishur: asmachIshur,
            i_emtzai_tashlum: tashlum,
            i_mispar_asmachta_yomi: asmachtaYomi,
            i_barcode: ""
        };
        const res = await this.createClient(args);

        console.log("res", res);

        logs.info("response code", res["o_rc"]["$value"]);

        if (res.error) {
            return res
        }

        if (res["o_rc"]["$value"] == 401 || res["o_rc"]["$value"] == 111 || res["o_rc"]["$value"] == 312) {
            this.info.setErrMessage(LM.getString("noContract"), res["o_error_description"]["$value"])
            return this.info
        }
        if (res["o_rc"]["$value"] == 114) {
            this.info.setErrMessage(LM.getString("partial debt"), res["o_error_description"]["$value"])
            return this.info
        }
        if (res["o_rc"]["$value"] == 107) {
            this.info.setErrMessage(LM.getString("noRevertPayment"), res["o_error_description"]["$value"])
            return this.info
        }
        else if (res["o_rc"]["$value"] == 102) {
            this.info.setErrMessage(LM.getString("noBill"), res["o_error_description"]["$value"])
            return this.info
        }
        else if (res["o_rc"]["$value"] == 3) {
            this.info.setErrMessage(LM.getString("debts"), res["o_error_description"]["$value"])
            return this.info
        }
        else if (res["o_rc"]["$value"] == 106) {
            this.info.setErrMessage(LM.getString("retrieve details"), res["o_error_description"]["$value"])
            return this.info
        }
        else if (res["o_rc"]["$value"] == 4) {
            this.info.setErrMessage(LM.getString("insufficient"), res["o_error_description"]["$value"])
            return this.info
        }

        if (res["o_rc"]["$value"] != 0) {
            this.info.setErrMessage(LM.getErrorBasic(), res["o_error_description"]["$value"])
            return this.info
        }
        return res
    }

    /***
     * Get response by soap
     @return (resolve) number of confirmation
     */
    async createClient(args) {

        let client = await soap.createClientAsync(url);

        return await client.kod_matamAsync(args, { timeout: this.timeout }).then(result => {
            return result[0];
        }).catch(err => {
            this.info.error.err_connection_site = true
            this.info.setErrMessage(LM.getErrorBasic(), "Error in connection " + err)
            return this.info;
        });
    }

}
module.exports = BasicIEC;
