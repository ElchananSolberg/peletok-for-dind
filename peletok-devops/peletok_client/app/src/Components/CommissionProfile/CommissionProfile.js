import React, { Component } from 'react';
import './CommissionProfile.css';

import { Button, Container, Row, Col } from 'reactstrap';

import { LanguageManager as LM } from "../LanguageManager/Language"

import CommissionProfileTable from './CommissionProfileTable';

import InputTypeSearchList from '../InputUtils/InputTypeSearchList';
import InputTypeRadio from '../InputUtils/InputTypeRadio';
import InputTypeNumber from '../InputUtils/InputTypeNumber';



/**
 * CommissionProfile is a main component of CommissionProfile Page.  CommissionProfile and its children are working with separate .css file: CommissionProfile.css.
 */
class CommissionProfile extends Component {
    constructor(props) {
        super(props);

        this.state = {
            commissionProfileHeader: LM.getString('commissionProfile'),


            /** selectCommissionProfileProps  selectSupplierProps selectProductProps ] - data sets for of <InputTypeSearchList/> component */
            selectCommissionProfileProps: {
                options: LM.getString('commissionsOptions'),
                title: LM.getString('selectCommissionsProfile'),
                id: "SelectCommissionProfile",
                required: true,
            },
            selectSupplierProps: {
                options: LM.getString('suppliersOptions'),
                default: LM.getString("suppliersOptions")[0],
                title: LM.getString('selectSupplier'),
                id: "SelectSupplier",
                required: true,
            },
            selectProductProps: {
                notValidMessage: '',
                options: LM.getString('productsOptions'),
                title: LM.getString('selectProductFromList'),
                id: "SelectProduct",
                required: true,
            },

            /** serviseTypeProps - an Object data for <InputTypeRadio/> component */
            serviseTypeProps: {
                options: LM.getString('serviceTypeOptions'),
                default: LM.getString('serviceTypeOptions')[0],
                id: "ServiseType",
                required: true,
            },

            /** commissionProps commissionFinalProps - different data for InputTypeNumber */
            commissionProps: {
                /** TODO: If there should be a minimum or/and maximum amount of digits, an empty string should be changed to number inside "" */
                minLength: "",
                maxLength: "",
                title: LM.getString('commission'),
                id: "Commission",
            },
            commissionFinalProps: {
                /** TODO: If there should be a minimum or/and maximum amount of digits, an empty string should be changed to number inside "" */
                minLength: "",
                maxLength: "",
                title: LM.getString('finalCommission') + ":",
                id: "CommissionFinal",
            },

            /** data for Button Spread Comission on all the Products  */
            buttonSpreadComission: LM.getString('spreadCommissionOnAll'),
            /** data for Button Spread Final Comission on all the Products  */
            buttonSpreadFinalComission: LM.getString('spreadFinalCommissionOnAll'),
            /** data for Button Save  */
            buttonSave: LM.getString('save'),
            /** data for Button Add Profile  */
            buttonAddProfile: LM.getString('addProfile'),

            /** tableHeadersObj - an object for <CommissionProfileTable/> component. The values of the object are the HEADERS of a table */
            tableHeadersObj:
            {
                product: LM.getString('product'),
                cardType: LM.getString('cardType'),
                resellerComission: LM.getString('resellerCommission'),
                resellerComissionP20: LM.getString('resellerCommissionP20'),
                finalCommision: LM.getString('finalCommission'),
                authorizedUser: LM.getString('authorizedUser'),
            },
            /** tableRows - an array of objects for <CommissionProfileTable/> component. The values of the objects are the data of the rows of a table */
            tableRows: [
                {
                    product: 'Pay bill Bezeq ',
                    cardType: 'Pay bill',
                    resellerComission: '2.245',
                    resellerComissionP20: '2.245',
                    finalCommision: '0',
                    checkbox: true,
                },
                {
                    product: 'Pay bill Highway 6',
                    cardType: 'Pay bill',
                    resellerComission: '2.138',
                    resellerComissionP20: '2.138',
                    finalCommision: '2',
                    checkbox: true,
                },
                {
                    product: 'Pay bill Authorities',
                    cardType: 'Pay bill',
                    resellerComission: '2.245',
                    resellerComissionP20: '2.245',
                    finalCommision: '1',
                    checkbox: false,
                },
                {
                    product: 'Pay bill Bezeq ',
                    cardType: 'Pay bill',
                    resellerComission: '2.245',
                    resellerComissionP20: '2.245',
                    finalCommision: '0',
                    checkbox: true,
                },
                {
                    product: 'Pay bill Highway 6',
                    cardType: 'Pay bill',
                    resellerComission: '2.138',
                    resellerComissionP20: '2.138',
                    finalCommision: '2',
                    checkbox: true,
                },
                {
                    product: 'Pay bill Authorities',
                    cardType: 'Pay bill',
                    resellerComission: '2.245',
                    resellerComissionP20: '2.245',
                    finalCommision: '1',
                    checkbox: false,
                },
                {
                    product: 'Pay bill Bezeq ',
                    cardType: 'Pay bill',
                    resellerComission: '2.245',
                    resellerComissionP20: '2.245',
                    finalCommision: '0',
                    checkbox: true,
                },
                {
                    product: 'Pay bill Highway 6',
                    cardType: 'Pay bill',
                    resellerComission: '2.138',
                    resellerComissionP20: '2.138',
                    finalCommision: '2',
                    checkbox: true,
                },
                {
                    product: 'Pay bill Authorities',
                    cardType: 'Pay bill',
                    resellerComission: '2.245',
                    resellerComissionP20: '2.245',
                    finalCommision: '1',
                    checkbox: false,
                },
                {
                    product: 'Pay bill Bezeq ',
                    cardType: 'Pay bill',
                    resellerComission: '2.245',
                    resellerComissionP20: '2.245',
                    finalCommision: '0',
                    checkbox: true,
                },
                {
                    product: 'Pay bill Highway 6',
                    cardType: 'Pay bill',
                    resellerComission: '2.138',
                    resellerComissionP20: '2.138',
                    finalCommision: '2',
                    checkbox: true,
                },
                {
                    product: 'Pay bill Authorities',
                    cardType: 'Pay bill',
                    resellerComission: '2.245',
                    resellerComissionP20: '2.245',
                    finalCommision: '1',
                    checkbox: false,
                },
            ],
        }
    }

    /** TODO: add funtionality */
    buttonSpreadComissionClick = (e) => {
    }

    /** TODO: add funtionality */
    buttonSpreadFinalComissionClick = (e) => {
    }

    /** TODO: add funtionality */
    buttonSaveClick = (e) => {
    }

    /** TODO: add funtionality */
    buttonAddProfileClick = (e) => {
    }

    render() {
        return (
            <Container className="commissionProfileContainer">
                <Row >
                    <Col className="commissionProfileHeader">
                        {this.state.commissionProfileHeader}
                    </Col>
                </Row>

                <Container className="commissionProfileSettings">
                    <Row >
                        <Col lg="4" className='d-flex flex-column'>
                            <InputTypeSearchList {...this.state.selectCommissionProfileProps} />
                        </Col>
                    </Row>
                    <Row >
                        <Col >
                            <hr></hr>
                        </Col>
                    </Row>
                    <Row >
                        <Col lg="8">
                            <InputTypeRadio {...this.state.serviseTypeProps} />
                        </Col>
                    </Row>

                    <Row >
                        <Col lg="4" className='d-flex flex-column'>
                            <InputTypeSearchList {...this.state.selectSupplierProps} />
                        </Col>

                        <Col lg="4" className='d-flex flex-column'>
                            <InputTypeSearchList {...this.state.selectProductProps} />
                        </Col>
                    </Row>
                    <Row >
                        <Col lg="4" className='d-flex flex-column'>
                            <InputTypeNumber {...this.state.commissionProps} />
                        </Col>

                        <Col lg="auto" className='d-flex flex-column'>
                            <Row>
                                <Col style={{ marginTop: '25px', }}>
                                    <Button style={{ width: '100%', overflow: 'hidden' }} className={'commissionProfileButton commissionProfileBigButton commissionProfileMinWidth'} onClick={this.buttonSpreadComissionClick}> {this.state.buttonSpreadComission} </Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row >
                        <Col lg="4" className='d-flex flex-column'>
                            <InputTypeNumber {...this.state.commissionFinalProps} />
                        </Col>
                        <Col lg="auto" className='d-flex flex-column'>
                            <Row>
                                <Col style={{ marginTop: '25px', }}>
                                    <Button style={{ width: '100%', overflow: 'hidden' }} className={'commissionProfileButton commissionProfileBigButton commissionProfileMinWidth'} onClick={this.buttonSpreadFinalComissionClick}> {this.state.buttonSpreadFinalComission} </Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row style={{ marginTop: '30px' }}>
                        <Col >
                            <CommissionProfileTable tableRows={this.state.tableRows} tableHeadersObj={this.state.tableHeadersObj} tableFootersObj={this.state.tableFootersObj} />
                        </Col>
                    </Row>
                    <Row>
                        <Col lg="2" style={{ marginTop: '15px', }}>
                            <Button style={{ width: '100%', overflow: 'hidden' }} className="commissionProfileButton commissionProfileBigButton commissionProfileButtonSave" onClick={this.buttonSaveClick}> {this.state.buttonSave} </Button>
                        </Col>
                    </Row>
                    <Row >
                        <Col >
                            <hr></hr>
                        </Col>
                    </Row>
                    <Row>
                        <Col lg="3" style={{ marginTop: '15px', }}>
                            <Button style={{ width: '100%', overflow: 'hidden' }} className="commissionProfileButton commissionProfileBigButton" onClick={this.buttonAddProfileClick}> {this.state.buttonAddProfile} </Button>
                        </Col>
                    </Row>
                </Container>
            </Container>
        );
    }
}
export default CommissionProfile;