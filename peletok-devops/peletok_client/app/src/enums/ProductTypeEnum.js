import { LanguageManager as LM } from "../../src/Components/LanguageManager/Language";

export default class ProductTypeEnum {
    static 1 = LM.getString('virtual');
    static 2 = LM.getString('manual');
    static 3 = LM.getString('billsPayment');
}