import React, { Component } from 'react';
import './TagsManagement.css';

import { Row, Col } from 'reactstrap';

import { LanguageManager as LM } from '../LanguageManager/Language'

import { getTags } from '../DataProvider/DataProvider'

import TagsTableRow from './TagsTableRow.js';

/** TagsTable component is a part of a Tags Management page. 
 * It creates a tags table. It renders the a table headers and needed amount of table rows with dynamic data from this.state.tableRows array.
*/
class TagsTable extends Component {
    constructor(props) {
        super(props);

        this.state = {
            tags: [],
        }
    }

    getTags = () => {
        return this.state.tags;
    }

    /** Function that opens the GET html request (getTags from DataProvider).
     *  Saves an array of tags from the server into tags variable. */
    loadTagsList = (forceUpdate = false) => {
        getTags(forceUpdate)
            .then(
                (res) => {
                    this.setState({ tags: res });
                }
            ).catch(
                (err) => {
                    alert(LM.getString("loadTagsListFailed") + err)

                }
            )
    }
    componentDidMount() {
        this.loadTagsList();
    }

    render() {
        let order = LM.getString("orderWithoutColon");
        let tagName = LM.getString("tagName");
        let tagNameForSearch = this.props.tagNameForSearch;
        let tableRows = this.state.tags;
        let orders = tableRows.map(current => {return current.order})
        let tableRowsRendered;
        if (tagNameForSearch !== "") tableRows = this.state.tags.filter(tag => tag.name === tagNameForSearch);                
        tableRowsRendered = tableRows.map(current => {
            return <TagsTableRow {...current} loadTagsList={this.loadTagsList} orders={orders} />
        });

        return (
            <>
                <div className="d-flex tagsManagementTableHeaders">
                    <Col xs='1'>
                        {order}
                    </Col>
                    <Col xs='3'>
                        {tagName}
                    </Col>
                </div>
                {tableRowsRendered}
            </>
        )
    }
}
export default TagsTable;