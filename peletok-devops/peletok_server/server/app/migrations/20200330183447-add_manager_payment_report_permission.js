'use strict';
const {addPermission, renamePermission} = require('../dbUtils/addPermission');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      renamePermission(queryInterface, '/reports/reports/obligo', '/reports/reports/16'),
      renamePermission(queryInterface, '/reports/reports/manualCards', '/reports/reports/17'),
      renamePermission(queryInterface, '/reports/reports/cardsAssemblage', '/reports/reports/18'),
      ])
  },

  down: (queryInterface, Sequelize) => {
   return Promise.resolve()
  }
};
