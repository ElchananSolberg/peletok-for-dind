const sequelize = require('sequelize');
const {dbConfiguration} = require('./config/config');


//Sequelize configuration
module.exports = new sequelize(
    dbConfiguration.database,
    dbConfiguration.username,
    dbConfiguration.password,
    dbConfiguration
);
