import React from 'react';
import { Table } from 'reactstrap';
import { Scrollbars } from 'react-custom-scrollbars';
import { LanguageManager as LM } from "../LanguageManager/Language";
import  PtTable  from '../PtTable/PtTable'
import  TableHead  from '../PtTable/TableHead'
import  TableBody  from '../PtTable/TableBody'
import  TableRow  from '../PtTable/TableRow'
import  InputTypeCheckBox  from '../InputUtils/InputTypeCheckBox'
import  Asterisk  from '../Ui/Asterisk'
import {postBarcodeTesting} from "../DataProvider/Functions/DataPoster";
/**
 * CatalogNumberTable - component that builds the table from the data (this.props.tableRows and this.props.tableHeadersObj) sended by the father component.
 * It use TableHead and TableRow components.
 */
export default class CatalogNumberTable extends React.Component {

    constructor(props) {
        super(props);
        this.tableHeaders = [
            'name',
            'required',
            'barcode',
        ]

        this.state = {
            render: true,
            barcodes: [],
            notValid: [],
            errors: {}
        }
       
    }

    setValue = (value) => {
        this.setState({
            render: false,
            barcodes: value || []
        },()=>{
            this.setState({
                render: true
            })
        })
    }

    runValidation = () => {
        const requireds = this.props.barcodeTypes.filter(bt=>bt.required).map(bt=>bt.id)
        const notValid = requireds.filter(id=>!(this.state.barcodes.find(b=>b.barcode_type_id) || {}).barcode)
        this.setState({
            notValid
        })
        if(notValid.length > 0){
            return false
        }else{
            return true
        }
    }

    getValue = () => {
        return { valid: true, value: JSON.stringify(this.state.barcodes) };
    }

    getValidator = (index) => {
        return (e) => {
            let barcode = e.target.value;
            postBarcodeTesting(barcode).then(
                res =>{
                        this.setState({errors: Object.assign(this.state.errors, {[index]: !res})})
                }
            )
        }
    }

    getInputHendler = (barcode_type_id) => {
        return (e) => {
            const index = this.state.barcodes.findIndex((b) => b.barcode_type_id == barcode_type_id);
            
            if (index === -1) {
                this.setState({
                    barcodes: [...this.state.barcodes, {barcode: e.target.value, barcode_type_id: barcode_type_id}]
                })
            } else {
                this.setState({
                    barcodes: this.state.barcodes.map(b=>b.barcode_type_id == barcode_type_id ? {barcode: e.target.value, barcode_type_id: barcode_type_id} : b)
                })
            }
            
        }
    }


    render() {
        let hasValidationError = Object.values(this.state.errors).filter(e=>e).length > 0
        return (
            <div style={{float: 'none', width: '90%', margin: '10px auto' }}>
                <PtTable height="200px">
                    <TableHead>
                        {this.tableHeaders.map((h,i) =><th style={{whiteSpace: 'nowrap'}} key={i}>{LM.getString(h)}</th>)}
                    </TableHead>
                    <TableBody>
                        { this.state.render && this.props.barcodeTypes.map((bt,  i) => {
                            return <TableRow key={i} className="trx-row">
                                    <td>
                                        {bt.name}
                                        <Asterisk show={bt.required} />
                                    </td>
                                    <td>
                                        <InputTypeCheckBox
                                            disabled={true}
                                            id={`is-required-${i}`}
                                            checked={bt.required}
                                         />
                                    </td>
                                    <td>
                                        <input
                                            onKeyUp={this.getValidator(i)}
                                            className={"input-barcode " + (this.state.errors[i] ? "barcode-error": "")}
                                            disabled={this.props.disabled}
                                            type="text"
                                            style={Object.assign({width: '97%'}, this.state.notValid.indexOf(bt.id) > -1 ? {border: '1px solid #dc3545'} : {})}
                                            defaultValue={(this.state.barcodes.find(b=>b.barcode_type_id == bt.id) || {}).barcode}
                                            onChange={this.getInputHendler(bt.id)}
                                        />
                                    </td>
                                </TableRow>
                        } ) }
                    </TableBody>
               </PtTable>
                {hasValidationError && <div className={'error-message-barcode'}>{LM.getString('errorBarcode')}</div>}
           </div>
        )
    }
}