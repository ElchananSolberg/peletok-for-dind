import XLSX from 'xlsx'

export class ExcelUtils{
  static parseExcel(file){
    return new Promise((resolve, reject)=>{
      let reader = new FileReader();
      reader.onload = function(e) {
        let data = e.target.result;
        let workbook = XLSX.read(data, {
          type: 'binary'
        });

        workbook.SheetNames.forEach(function(sheetName) {
          // Here is your object
          let XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
          // let json_object = JSON.stringify(XL_row_object);
          resolve(XL_row_object)

        })

    };

    reader.onerror = (error) => {
      reject(error)
    };
    

    reader.readAsBinaryString(file);

    })
    
  }
}


