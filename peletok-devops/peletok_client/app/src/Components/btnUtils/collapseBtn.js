import React from 'react';
import './btnUtils.css'

export class CollapseBtn extends React.Component {
    arrowsDirection = {
        open:'arrow-up',
        close:'arrow-down',
    }
    constructor(props){
        super(props);
        this.state = {
            rotateClass: '',
            open: this.props.open,
        }
        
    }

    toggle(){
        this.setState({
            rotateClass: !this.props.open ? this.arrowsDirection.open : this.arrowsDirection.close,
            open: !this.state.open,
        })
    }

    render(){
        return (
            <svg className="collapse-btn"
                onClick={()=>{this.props.onClick();this.toggle()}} 
                height="32" 
                viewBox="0 0 25 50"
                style={{fillRule:'evenodd',clipRule:'evenodd',strokeLinecap:'round',strokeLinejoin:'round',strokeMiterlimit:'1.5'}}
                >
                <circle cx="12.5" cy="25.5" r="12" style={{fill:'#fff',stroke:'#2bc6c3',strokeWidth:'1px'}}/>
                <path className={'arrow-icon ' + (this.state.rotateClass)} 
                 d="M12.355,32.363l8.618,-10.224l-17.236,0l8.618,10.224Z"  />
                {this.state.open  && <path 
                 d="M12.5,38l0.015,13" 
                 style={{fill:'none',stroke:'#808080',strokeWidth:'0.5px'}}
                 />}
            </svg>
        )
    }
}

