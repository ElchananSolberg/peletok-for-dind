import React, { Component } from 'react';
import { UsersScreen } from './selectUser';
import { LanguageManager as LM } from '../LanguageManager/Language';
import './selectUser.css';
import {  Row, Col } from 'reactstrap';
import { getSellerUsersData } from '../DataProvider/Functions/DataGeter'



/**
 * A component that encapsulates the Componante that displays a list of people, so that the user can transfer specific information in Propes
 */

export class PersonalArea extends Component {
    constructor(props) {
        super(props);


        this.state = {
            UsersData: null
        }
        this.updateUsers = this.updateUsers.bind(this)
    }

    /**
     * loads data from server whenever site loads the main screen
     */
    componentDidMount() {
        this.updateUsers()

    }
    updateUsers() {
        // TODO - remove this true after implementation on event system
        getSellerUsersData(true).then(
            res => {
                this.setState({ UsersData: [] }, () => {
                    this.setState({UsersData:res})
                })

            }
        ).catch(/* TODO to use general error handler */
            function (error) {
                alert(error)
            }
        );
    }
    render() {
        return (
            <React.Fragment>

                <div className={'d-flex justify-content-between'}>
                    <h3 className="color-page-header font-weight-light fs-26">{LM.getString("selectUserBack")}</h3>
                    <h3 className="color-page-header font-weight-light fs-26">{LM.getString("personalArea")}</h3>
                </div>
                {
                    this.state.UsersData ?
                        <UsersScreen ourUsers={this.state.UsersData} updateUsers={this.updateUsers} /> : <React.Fragment />
                }
            </React.Fragment>

        )
    }
}