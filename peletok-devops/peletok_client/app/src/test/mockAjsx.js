let transactions = [
            {
                id: 1,
                date: '22/12/79',
                time: '',
                businessName: '',
                customerNumber: '',
                agentName: '',
                agentNumber: '',
                customerPrice: '',
                businessPirce: '',
                supplier: '',
                product: '',
                action: '',
                phone: '',
                requestDate: '',
                requestTime: '',
                requesterName: '',
                parentTransactionId: 97

            },{
                id: 2,
                date: '22/12/79',
                time: '',
                businessName: '',
                customerNumber: '',
                agentName: '',
                agentNumber: '',
                customerPrice: '',
                businessPirce: '',
                supplier: '',
                product: '',
                action: '',
                phone: '',
                requestDate: '',
                requestTime: '',
                requesterName: '',
                parentTransactionId: 98

            },
            {
                id: 3,
                date: '22/12/79',
                time: '',
                businessName: '',
                customerNumber: '',
                agentName: '',
                agentNumber: '',
                customerPrice: '',
                businessPirce: '',
                supplier: '',
                product: '',
                action: '',
                phone: '',
                requestDate: '',
                requestTime: '',
                requesterName: '',
                parentTransactionId: 99

            },
          
        ]

const suppliers = {
    "prepaid":[
           {"name":"פלאפון","id":95}
          ,{"name":"סלקום","id":94}
          ,{"name":"פרטנר","id":127}
          ,{"name":"שיחה חסויה","id":196}
          ,{"name":"בינלאומי","id":124}
          ,{"name":"גלובל סים","id":203}
          ,{"name":"בזק בינלאומי 014","id":205}
          ,{"name":"012 מובייל","id":201}
          ,{"name":"פריפון","id":202}
      ],
      "bills":[
           {"name":"תשלום חשבון חשמל","id":185}
          ,{"name":"טעינת חשמל","id":186}
          ,{"name":"כביש 6","id":187}
          ,{"name":"בזק אונליין","id":194}
          ,{"name":"רב קו","id":999}
          ,{"name":"מנהרות הכרמל","id":192}
          ,{"name":"תשלום לרשויות","id":193}
      ]}

const withOutThree = transactions.filter(t=>t.id!=3)
const withOutTwo = transactions.filter(t=>t.id!=2)
export default {
 get(url){

    switch (url) {
      case 'http://localhost:8080/api/v1/transaction/cancelation_request/allSuppliers':
        return Promise.resolve({data: transactions})
      case 'http://localhost:8080/api/v1/supplier/supplier':
        return Promise.resolve({data: suppliers})
      case 'http://localhost:8080/api/v1/transaction/cancelation_request/127':
        return Promise.resolve({data: withOutThree})
      case 'http://localhost:8080/api/v1/transaction/cancelation_request/94':
        return Promise.resolve({data: []}) 
      default:
        return Promise.reject(new Error('not found'))
    }
  },
  post(url, data){
    switch (url) {
      case 'http://localhost:8080/api/v1/payment/cancel':
        if(data.transactionId == 98){
            return Promise.resolve({data: 'success'})
        }else{
            throw new Error('ajax error')
            // return Promise.reject({status: 403})

        }
      default:
        return Promise.reject(new Error('not found'))
    }
  }
}

         

     
    