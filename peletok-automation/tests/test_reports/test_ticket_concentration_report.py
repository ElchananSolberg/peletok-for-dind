from tests.test_reports.base_reports_test_class import *


class TestTicketConcentrationReport(BaseReportsTestClass):

    def test_get_into_concentration_report(self):
        self.login_and_get_in_report("4", 'ריכוז כרטיסים')
        self.remove_logs_dir = True

    def test_suppliers_select(self):
        self.login_and_get_in_report("4", 'ריכוז כרטיסים')
        self.check_if_got_options_in_which_select(GeneralReportsPage.suppliers_input)
        self.remove_logs_dir = True

    def test_concentration_report_radio_buttons(self):
        self.login_and_get_in_report("4", 'ריכוז כרטיסים')
        self.check_radio_buttons(GeneralReportsPage.radio_button)
        self.remove_logs_dir = True

    def test_concentration_report_check_inputs_date(self):
        self.login_and_get_in_report("4", 'ריכוז כרטיסים')
        self.check_if_current_date_in_input(GeneralReportsPage.start_date_input)
        self.check_if_current_date_in_input(GeneralReportsPage.end_date_input)
        self.remove_logs_dir = True

    def test_concentration_report_change_start_end_date_input(self):
        self.login_and_get_in_report("4", 'ריכוז כרטיסים')
        self.insert_different_date_to_input_and_check(GeneralReportsPage.start_date_input)
        self.insert_different_date_to_input_and_check(GeneralReportsPage.end_date_input)
        self.remove_logs_dir = True

    def test_concentration_report_hour_minute_select(self):
        self.login_and_get_in_report("4", 'ריכוז כרטיסים')
        self.insert_hour_and_check(GeneralReportsPage.start_hours_select)
        self.insert_hour_and_check(GeneralReportsPage.end_hours_select)
        self.insert_minute_and_check(GeneralReportsPage.start_minutes_select)
        self.insert_minute_and_check(GeneralReportsPage.end_minutes_select)
        self.remove_logs_dir = True

    def test_concentration_report_show_reports_btn(self):
        self.login_and_get_in_report("4", 'ריכוז כרטיסים')
        self.click_on_show_reports_btn_and_check()
        self.remove_logs_dir = True

    def test_concentration_report_alert_no_report(self):
        self.login_and_get_in_report("4", 'ריכוז כרטיסים')
        self.insert_current_time_to_select_and_check(GeneralReportsPage.start_hours_select,
                                                     GeneralReportsPage.start_minutes_select)
        self.driver.tools.wait_and_click(GeneralReportsPage.show_report_btn)
        self.driver.wait.wait_for_element_to_be_present(GeneralReportsPage.no_report_alert)
        self.remove_logs_dir = True

    def test_concentration_report_show_reports_btn_details(self):
        self.login_and_get_in_report("4", 'ריכוז כרטיסים')
        self.insert_different_date_to_input_and_check(GeneralReportsPage.start_date_input)
        self.click_on_show_reports_btn_and_check()
        self.check_dates_report_span()
        self.check_total_sales_span('דוחריכוזכרטיסים')
        self.remove_logs_dir = True
