'use strict';
const {addPermission, renamePermission} = require('../dbUtils/addPermission');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      renamePermission(queryInterface, '/reports/hashavshevet/dueToDate_of_ExportationToHashavshevet_Report', '/reports/hashavshevet/export/orderByDate'),
    ])
  },

  down: (queryInterface, Sequelize) => {
   return Promise.resolve()
  }
};
