const BaseModel = require("../../BaseModel");
const DataTypes = require("sequelize").DataTypes;

/**
 * City model
 */
class City extends BaseModel {


    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            name_HE: {
                type: DataTypes.STRING(256),
                allowNull: true
            },
            name_EN: {
                type: DataTypes.STRING(256),
                allowNull: true
            },
            name_AR: {
                type: DataTypes.STRING(256),
                allowNull: true
            },
        };
        this.prefix = "PAY";
        this.author = true;
    }

    /**
     * creates associate for city model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.hasMany(models.cityPaymentTypeSite.model, { as:"CityPaymentTypeSite"});
        this.model.belongsTo(models.area.model, {as: "Area"});
    }
}

module.exports = new City();