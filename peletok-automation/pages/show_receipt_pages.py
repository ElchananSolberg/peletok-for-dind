from selenium.webdriver.common.by import By


class ShowReceipt:

    choose_year_input = (By.ID, 'InputUtilsSelectInvoiceSelectYear')
    choose_month_input = (By.ID, 'InputUtilsSelectInvoiceSelectMonth')
    show_receipt_btn = (By.ID, 'invoiceShowInvoice')
    no_receipt_element = (By.XPATH, "//div[@class='d-flex tagsManagementTableHeaders' and"
                                    " contains(text(), 'לא נמצאו חשבוניות')]")
    checkbox_id = (By.ID, 'InputTypeCheckBox1')
    checkbox_of_receipt = (By.XPATH, '//label[@for="InputTypeCheckBox5"]')
    send_receipt_to_mail_btn = (By.ID, 'invoiceSendInvoice')
    email_input = (By.CSS_SELECTOR, 'div.modal-body input')
    send_email_btn = (By.CSS_SELECTOR, 'div.modal button.regularButton')

    download_adobe_reader_btn = (By.ID, 'invoiceDownloadPdf')
    element_in_adobe_reader_page = (By.ID, 'AUTO_ID_singlepage_span_home')
