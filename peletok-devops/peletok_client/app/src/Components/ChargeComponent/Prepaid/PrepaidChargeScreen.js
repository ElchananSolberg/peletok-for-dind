import React from 'react';
import { Button, Container, Row, Col } from 'reactstrap';
import { LanguageManager as LM } from '../../LanguageManager/Language';
import { Link } from 'react-router-dom';
import { SERVER_BASE } from '../../DataProvider/DataProvider'
import './PrepaidChargeScreen.css'

export const PrepaidChargeScreen = (props) => {
    // TODO - create unified breadcrumb components and implement it everywhere
    // TODO - re arange style (background texture + image in the right position)
    return (
        // FIX IMAGE LAYUOUT
        <Container style={{ backgroundImage: `url("${SERVER_BASE + '/' + props.background_image}")` }}
            className='prepaidProductContainer p-4' fluid>
            <div style={props.chargeCancelBackgroundStyle} className='d-sm-flex'>
                <div className="">
                    <p className='prepaidHeader'>{props.operationTypeName} - {props.name}</p>
                    <div className='d-flex mb-1'>
                        {props.virtual ?
                            <Link to={props.match.url + `/virtual`}>
                                <Button style={{ color: props.font_color }}
                                    className={'border-radius-3 btn-lg font-weight-bold fs-17 prepaidBtn px-3 '
                                    + (LM.getDirection() === "rtl" ? "ml-2" : "mr-2")}
                                    id='virtual'>{LM.getString("virtualCard")}</Button>
                            </Link>

                            : <React.Fragment />}
                        {props.manual ?
                            <div>
                                <Link to={props.match.url + `/manual`}>
                                    <Button style={{ color: props.font_color }} className='border-radius-3 btn-lg font-weight-bold fs-17 prepaidBtn px-3'
                                        id='manual'>{LM.getString("manualCard")}</Button>
                                </Link>
                            </div>
                            : <React.Fragment />}
                    </div>
                </div>
                {/**TODO - FIX LOGO SIZE */}
                {/**TODO - FIX LOGO POSITION WHILE SCREEN IS SMALL */}
                <img className={'d-flex mt-5 mt-sm-0 ' + (LM.getDirection() === "rtl" ? "mr-auto" : "ml-auto")}
                    src={`${SERVER_BASE}/${props.image}`} alt='' />
            </div>
        </Container>
    )
}





