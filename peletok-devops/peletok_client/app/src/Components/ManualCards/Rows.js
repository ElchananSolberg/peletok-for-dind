import React from 'react';
import { Row, Col, Button } from 'reactstrap';
import { LanguageManager as LM } from '../LanguageManager/Language';
import './ManualCards.css';
import { Table } from './Table';
import { postNewManualCard } from '../DataProvider/Functions/DataPoster';
import { deleteManualCard } from '../DataProvider/Functions/DataDeleter';
import { putManualCard } from '../DataProvider/Functions/DataPuter';
import { Notifications } from '../Notifications/Notifications';



export class Rows extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            open: this.props.open
        }
    }

    edit = () => {
        this.setState({ open: !this.state.open })
    }
    delete = () => {
        deleteManualCard(this.props.cardID)
        this.props.getCards()
    }
    editCancel = () => {
        this.setState({ open: !this.state.open })
    }

    save = () => {
        var newManualCard = {};
        this.setState({ open: !this.state.open })
        var newCard = this.table.getValue();
        newManualCard.card_number = newCard.card_number;
        newManualCard.code = newCard.code;
        newManualCard.expiry_date = newCard.expiry_date;
        newManualCard.status = newCard.status;
        newManualCard.product_id = this.props.ProductId;
        if (this.props.id != 'new') {
            putManualCard(this.props.id, newManualCard).then((res) => {
                this.props.getCards()


            }).catch(
                (err) => {
                    Notifications.show(err, 'danger');
                }
            )
        } else {

            postNewManualCard(newManualCard).then((res) => {
                this.props.getCards()

            }).catch(
                (err) => {
                    Notifications.show(err, 'danger');
                }
            )
        }

        if (this.props.closefun) { this.props.closefun() }
    }

    render() {

        var display = !this.state.open ?
            <Row className="cardRow justify-content-end mx-0" >
                <Col > {this.props.card_number}</Col>
                <Col > {this.props.status == 2 ? LM.getString("yes") : LM.getString("no")}</Col>
                <Col > {this.props.insert_date != undefined ? this.props.insert_date + '  ' + this.props.Time_of_income : <React.Fragment />}</Col>
                <Col>  {this.props.expiry_date}</Col>
                <Col > {this.props.code}</Col>
                <Col style={{ display: 'flex', padding: '0.5%' }}>
                    <Button
                        className='manualCardsTableBtn manualCardsTableBtnEdit mx-1'
                        onClick={this.edit}
                        id={`manualCardsEdit` + this.props.id}>{LM.getString("edit")}
                    </Button>
                    <Button
                        className='manualCardsTableBtn manualCardsTableBtnDelete'
                        onClick={this.delete.bind(this)}
                        id={`manualCardsDelete` + this.props.id}
                    >
                        {LM.getString("manualCardsDelete")}
                    </Button>
                </Col>
            </Row>
            :
            <Table {...this.props} ref={(componentObj) => { this.table = componentObj }}
                row={
                    <Row className="tabl justify-content-end" >

                        <Col sm="3" style={{ display: 'flex' }} >
                            <Button
                                className='manualCardsTableBtn manualCardsTableBtnSave mx-1'
                                onClick={this.save.bind(this)}
                                id={`manualCardsSave` + this.props.id}
                            >
                                {LM.getString("manuelCardSAve")}
                            </Button>
                            <Button
                                className='manualCardsTableBtn manualCardsTableBtnDelete'
                                onClick={this.editCancel}
                                id={`manualCardeditCancel` + this.props.id}>
                                {LM.getString("cancel")}
                            </Button>
                        </Col>
                    </Row>
                }
            />

        return (

            <React.Fragment >
                {display}
            </React.Fragment>
        );
    }
}