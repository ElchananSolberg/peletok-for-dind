import React, { Component } from 'react';
import './InputUtils.css';
import { Input, Row, Col } from 'reactstrap';

import { Tooltip } from './Tooltip';

import { LanguageManager as LM } from '../LanguageManager/Language'

/**
 * Slider component lets to input the value with the use of a slider. It checks if it is valid.
 * it can render the title above the slider (if this.props.title is not an empty string).
 * it can render the tooltip to the side of the title (if this.props.tooltip is not an empty string).
 * It validates the value is valid to the function sent in  this.props.validationFunction;
 * if there are any validation issues the input will get a prop "invalid" and will change a border color to red;
 * if there are any validation issues bellow the input will be the error message 
 * It can add outer onChange function to checkbox onChange.
 * It can change a view to disabled view due to this.props.disabled.
 */
class Slider extends Component {

    constructor(props) {
        super(props);

        this.state = {
            value: this.props.defaultValue || (this.props.min + this.props.max) / 2,
            validationFuncs: [
                {
                    /**  Validates the value is valid to the function sent in  this.props.validationFunction */
                    f: this.props.validationFunction || (() => (true)),
                    msg: this.props.notValidMessage
                },
                {
                    f: this.innerValidationFunction,
                    msg: LM.getString("sliderInnerValidationFunctionMsg"),
                },
            ],
            errorMessage: '',
        }
    }

    /** can add outer onChange function to checkbox onChange. */
    sliderChanged = (e) => {
        e.persist();
        this.setState({ value: e.target.value },
            () => {
                if (this.props.onChange) {
                    this.props.onChange(e);
                }
            }
        );
    }

    /** summarizing validation function
    * is running 2 times: when input is loosing focus and when getValue function is running (by programmer-user) 
    * */
    runValidation = () => {
        let summarizingValidationResult = (!this.props.required && !this.state.value) || this.state.validationFuncs.every((validation) => validation.f(this.state.value));
        if (!summarizingValidationResult) {
            let messages = [];
            this.state.validationFuncs.forEach((validation) => {
                if (!validation.f(this.state.value)) {
                    messages.push(validation.msg);
                }
            })
            this.setState(({ errorMessage: messages.join(`\n`) }));
        } else {
            this.setState(({ errorMessage: '' }));
        }
        return summarizingValidationResult;
    }

    getValue = () => {
        let summarizingValidationResult = this.runValidation();
        return { valid: summarizingValidationResult, value: this.state.value };
    }

    setValue = (newValue) => {
        let min = parseInt(this.props.min);
        let max = parseInt(this.props.max);
        
        if (newValue >= min && newValue <= max){
            this.setState({ value : newValue });
            return true;
        }
        return false;
    }

    /** validats the STEP is NOT BIGGER THAN this.props.max-this.props.min */
    innerValidationFunction = () => {
        let step = this.props.step;
        let min = this.props.min;
        let max = this.props.max;
        if (step > max - min) {
            return false;
        }
        return true;
    }

    render() {
        let disabled = this.props.disabled;
        let min = this.props.min;
        let max = this.props.max;
        let step = this.props.step;
        let id = this.props.id.replace(/\s+/g, '');
        let title = this.props.title;
        let tooltipText = this.props.tooltip;

        return (
            <>
                <Row>
                    <Col className={"d-flex justify-content-between line-height-1 " + (title ? 'py-1' : '') }>
                        {title ?
                            <div className="fs-17 white-space">{title}</div>
                            : <></>
                        }
                        {tooltipText ?
                            <Tooltip tooltipText={tooltipText} id={id} />
                            : <></>
                        }
                    </Col>
                </Row>

                <Row>
                    <Col style={{ textAlign: 'start' }}>
                        <div className="sliderInfo" >
                            <b>{min}</b>
                        </div>
                    </Col>
                    <Col style={{ textAlign: 'center' }}>
                        <div className="sliderInfo" >
                            <b>{this.state.value}</b>
                        </div>
                    </Col>
                    <Col style={{ textAlign: 'end' }}>
                        <div className="sliderInfo" >
                            <b>{max}</b>
                        </div>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <div className={(this.state.errorMessage ? "invalidDiv" : "")} onBlur={this.runValidation}>
                            <Input id={`Slider${id}`} type="range"
                                min={min} max={max}
                                step={step || 1}
                                value={this.state.value}
                                disabled={disabled}
                                onChange={this.sliderChanged}
                            />
                        </div>
                        <div className='ws-pre errorMessageDiv'>{this.state.errorMessage}</div>
                    </Col>
                </Row>
            </>
        )
    }
}
export default Slider;