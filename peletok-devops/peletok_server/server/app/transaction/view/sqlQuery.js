const {awaitingForCancellation} = require("../../payment/transaction/createCancelationTransaction");
const { getTypeNum } = require("../../utils.js");
module.exports.getDropReportCommand = function (tableName) {
    return `DROP VIEW IF EXISTS "${tableName}";`;
};
// TODO- left join "USR_User" USR on BSN.agent_id = USR.id
// for agent name
module.exports.getSyncReportCommand = function (tableName) {
    return `CREATE VIEW "${tableName}" as
         select "TRNSC_Transaction".*,

       SPD.product_name,
       SUP.name as  supplier_name,
       SUP.type as supplier_type,
       BR.business_id as branch_business_id,
       SP.type as product_type,
       TRD.comment as comment,
       TRD.bezek_phone_number as bezek_phone_number,
       TRD.bezek_client_id as bezek_client_id,
       TRD.external_transaction_id as external_transaction_id,
       TRD.phone_number as td_phone_number,cities_item_id, bill_number, client_id, contract_number, item_id, customer_id,card_number,card_code,client_name,client_address,contact_identification,confirmation,reference_number,city_pay_city,city_pay_type,
       TRD.ip,
       BSN.distributor_id as distributor_id,
       BSN.name as business_name,transaction_id,BSN.business_identifier as business_identifier  ,business_credit,
       BSN.status as business_status,
       BSN.use_article_20 as use_article_20,
       BSN.agent_id,
       BPL.sum as business_points,
       DIST.name as distributor_name,
       ADD.street as address,ADD.city,
       BST.tag_id as business_tags_id,
       PP.buying_price as peletok_price,
       PB.barcode as barcode

       from "TRNSC_Transaction"
       left join "BSN_Terminal" BT
                 on "TRNSC_Transaction".terminal_id = BT.id
       left join "BSN_Branch" BR
                 on BT.branch_id = BR.id
       left join "BSN_BusinessAddress" BBA on BBA.business_id = "TRNSC_Transaction".business_id
       left join "USR_Address" UA on UA.id = BBA.address_id
       left join "SUPL_Product" SP
                 on "TRNSC_Transaction".product_id = SP.id
       left join "SUPL_Supplier" SUP
                 on "TRNSC_Transaction".supplier_id = SUP.id or ("TRNSC_Transaction".supplier_id is null and SP.supplier_id = SUP.id)
       left join "TRNSC_TransactionData" TRD
                 on "TRNSC_Transaction".id = TRD.transaction_id 
       left join "BSN_Business" BSN
                 on "TRNSC_Transaction".business_id = BSN.ID
       left join "BSN_Business" DIST
                 on BSN.distributor_id = DIST.distributor_id and DIST.is_distributor = true
       left join "SUPL_ProductPrice" PP
                 on "TRNSC_Transaction".product_id = PP.product_id and "TRNSC_Transaction".created_at between PP.timestamp_start and PP.timestamp_end
       left join "GFT_BusinessPointLog" BPL
                 on BSN.id = BPL.business_id and "TRNSC_Transaction".created_at between BPL.timestamp_start and BPL.timestamp_end
       left join "TAG_BusinessTag" BST
                 on BSN.id = BST.business_id 
       left join "USR_Address" ADD
                 on BBA.address_id = ADD.id
       left join "SUPL_ProductDetails" SPD
                 on SP.id = SPD.product_id and language_code = 'HE'
       left join "SUPL_ProductBarcode" PB
                 on "TRNSC_Transaction"."product_id" = PB.product_id and PB.barcode_type_id = ${getTypeNum('peletok', 'Barcode')} 
       where "TRNSC_Transaction".transaction_type!=${awaitingForCancellation} or "TRNSC_Transaction".transaction_type is null;`;
};

