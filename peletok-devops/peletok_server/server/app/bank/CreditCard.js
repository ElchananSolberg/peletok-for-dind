let logger =require("../../app/logger")
const {Info } = require("../mapping/crudMiddleware");
const models=require("../../app/index")
/***
     * delete credit card
     */
    async function deleteCreditCard(req, res){
        let info = new Info(res)
        
       await models.creditCard.model.destroy({where: {id: req.params.id}}).catch(e=>{
                info.errors.push(e)
                logger.error(e);
            }).then(()=>{
                res.customSend(info)
            })
    }

module.exports.deleteCreditCard=deleteCreditCard
