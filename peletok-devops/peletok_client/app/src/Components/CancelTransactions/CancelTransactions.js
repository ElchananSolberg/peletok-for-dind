import React, { Component } from 'react';
import { Row, Col, Button } from 'reactstrap'
import { LanguageManager as LM } from '../LanguageManager/Language';
import InputTypeSearchList from '../InputUtils/InputTypeSearchList';
import PtTable from '../PtTable/PtTable'
import TableHead from '../PtTable/TableHead'
import TableBody from '../PtTable/TableBody'
import TableRow from '../PtTable/TableRow'
// import UnpaidPaymentsTable from './UnpaidPaymentsTable'

import { getSuppliers, getTansaction } from '../DataProvider/Functions/DataGeter'
import { postCompleetCancelTransaction, postRejectCancelTransaction } from '../DataProvider/Functions/DataPoster'
import { Notifications } from '../Notifications/Notifications'
import { Confirm } from '../Confirm/Confirm';
import { Loader } from '../Loader/Loader';

/**
 * 
 */
export class CancelTransactions extends Component {
    constructor(props) {
        super(props);
        this.tableHeaders = [
            'transactionDate',
            'transactionTime',
            'businessName',
            'customerNumber',
            //'agentName',
            //'agentNumber',
            'customerPrice',
            'businessPrice',
            'supplierName',
            'productName',
            'action',
            'phoneNumber',
            'requestDate',
            'requestTime',
            'requesterName',
            'actions',
        ]

        this.state = {
            transactions: [],
            listSuppliersNamesAndIDs: [],
            supplierSelectListProps: {
                disabled: false,
                options: [],
                default: LM.getString("AllSuppliers"),
                title: LM.getString("supplierSelect"),
                id: "supplierSelect",
            },


        };
    }

    componentDidMount = async () => {
        try {
            const supplierResponse = await getSuppliers()
            var listSuppliersNames = [LM.getString("AllSuppliers")];
            var listSuppliersNamesAndIDs = [];
            var listSuppliers = supplierResponse.prepaid;

            listSuppliers.forEach(element => {
                listSuppliersNamesAndIDs.push({ 'name': element.name, 'id': element.id })
                listSuppliersNames.push(element.name)
            });
            let props = { value: listSuppliersNamesAndIDs[0].name, options: listSuppliersNames }
            this.setState({
                listSuppliersNamesAndIDs,
                supplierSelectListProps: Object.assign(this.state.supplierSelectListProps, props)
            });


        } catch (e) {
            Notifications.show(e, 'danger')
        }
        let transactions;
        try {
            transactions = await getTansaction('allSuppliers')
            this.setState({ transactions })
        } catch (e) {
            Notifications.show(e, 'danger')
        }
    }

    filterRequests = async (supplierName) => {
        const supplier = this.state.listSuppliersNamesAndIDs.find(s => s.name === supplierName)
        const transactions = await getTansaction(supplierName != LM.getString("AllSuppliers") ? supplier.id : 'allSuppliers')
        this.setState({ transactions })
    }

    sendCancel = async (id, supplierName, action, productName) => {
        Confirm.confirm(LM.getString("areYouSureCancel") + "\n" + (action ? action : " ") + "\n" + (productName ? productName : '') + ' ' + LM.getString("fullRefundInnerCancelation")).then(async () => {
            try {
                const supplierId = (this.state.listSuppliersNamesAndIDs.find(s => s.name === supplierName)).id.toString()
                Loader.show()
                // changed from postCardCancel() postCancelTransactin()... no such endPoint
                const res = await postCompleetCancelTransaction(supplierId, { transactionId: id, internal: true })
                if (res) {
                    Loader.hide();
                    this.setState({
                        transactions: this.state.transactions.filter(t => t.parentTransactionId != id)
                    })
                }
            } catch (e) {
                Loader.hide()
                Notifications.show(LM.getString('errorMsg'))
            }
        }).catch((err) => { })
    }

    sendReject = async (id, supplierName) => {
        Confirm.confirm("The user will not be able to request a cancelation again!").then(
            async () => {
                try {
                    const supplierId = (this.state.listSuppliersNamesAndIDs.find(s => s.name === supplierName)).id.toString()
                    Loader.show()
                    let res = await postRejectCancelTransaction(supplierId, { transactionId: id });
                    if (res) {
                        Loader.hide();
                        this.setState({
                            transactions: this.state.transactions.filter(t => t.parentTransactionId != id)
                        })
                    }
                } catch (error) {
                    Loader.hide()
                    Notifications.show(LM.getString('errorMsg') + error)
                }
            }
        )
    }

    render() {
        return (<div >
            <h3 className="firstDivString">  {LM.getString("CancelTransactions")}</h3>

            <div >
                <Row >
                    <Col id="supplier-list-select" style={{ maxWidth: '36%' }} >
                        <InputTypeSearchList  {...this.state.supplierSelectListProps} onChange={this.filterRequests} />
                    </Col>

                </Row>
                <hr />
                <Row style={{ marginTop: '30px' }}>
                    <Col >
                        {this.state.transactions.length > 0 ?
                            <PtTable>
                                <TableHead>
                                    {this.tableHeaders.map((h, i) => <th style={{ whiteSpace: 'nowrap' }} key={i}>{LM.getString(h)}</th>)}
                                </TableHead>
                                <TableBody>
                                    {this.state.transactions.map((t, i) => {
                                        return <TableRow key={i} className="trx-row">
                                            {this.tableHeaders.filter(h => h != 'actions').map((k, index) => { return <td key={index}>{t[k]}</td> })}
                                            <td>
                                                <Button className="CommissionProfileSmallButton" style={{ marginLeft: '1px' }} onClick={() => { this.sendCancel(t.parentTransactionId, t.supplierName, t.action, t.productName) }} color="warning">{LM.getString('confirm')}</Button>
                                                <Button className="CommissionProfileSmallButton" style={{ marginLeft: '1px' }} onClick={() => { this.sendReject(t.parentTransactionId, t.supplierName) }} color="danger">{LM.getString('reject')}</Button>
                                            </td>
                                        </TableRow>

                                    })}
                                </TableBody>
                            </PtTable> :
                            <PtTable>
                                <tr><td colspan={this.tableHeaders.length} >{LM.getString('noTrxFoundToCancel')}</td></tr>
                            </PtTable>
                        }
                    </Col>
                </Row>


            </div>
        </div>


        );
    }
}
