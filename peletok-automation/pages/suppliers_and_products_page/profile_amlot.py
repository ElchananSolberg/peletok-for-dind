from selenium.webdriver.common.by import By


class ProfileAmlot:

    alert_success = (By.CSS_SELECTOR, 'div.alert-success')
    alert_warning = (By.CSS_SELECTOR, 'div.alert-warning ')
    supplier_product_link = (By.ID, "suppliersLink")
    profile_amlot_link = (By.XPATH, '//div[@class="temp_background main-body pt-5"] //a[@href="/suppliers/suppliersAndProducts/profitPercentageProfile/commission"]')

    profile_select = (By.ID, "InputTypeSearchListProfitPercentageProfile_SelectProfitPercentageProfile")
    option = (By.ID, "react-select-5-option-0")

    add_now_profile_btn = (By.ID, "profitPercentageProfile_AddNewProfileButton")
    popup = (By.CLASS_NAME, "modal-content")
    popup_insert_profile_name = (By.ID, "InputTypeTextProfitPercentageProfile_ProfileName")
    popup_x_btn = (By.CLASS_NAME, "close")
    popup_save_btn = (By.CSS_SELECTOR, "button.regularButton.m-2.btn.btn-secondary")
    popup_cancel_btn = (By.XPATH, "//button[@class='btn btn-secondary']")

    select_provider_list = (By.ID, "InputTypeSearchListProfitPercentageProfile_SelectSupplier")
    option_electricity_bill_payment = (By.XPATH, "//div[@class='css-fk865s-option' and contains(text(), 'טעינת חשמל')]")

    commission_input = (By.ID, "InputTypeNumberCommission")
    apply_commission_on_products_btn = (By.ID, "profitPercentageProfile_SpreadProfitPercentageButton")
    final_commission_input = (By.ID, "InputTypeNumberCommissionFinal")
    apply_final_commission_on_products_btn = (By.ID, "profitPercentageProfile_SpreadEarningPointsButton")
    confirm = (By.CLASS_NAME, "modal-content")
    confirm_approve_btn = (By.CSS_SELECTOR, "button.m-2.btn.btn-primary")
    confirm_cancel_btn = (By.CSS_SELECTOR, "div.modal-footer > button.btn.btn-secondary")

    checkbox_save_for_default_label = (By.XPATH, '//label[@for="InputTypeCheckBoxProfitPercentageProfile_SaveAsDefaultProfile"]')
    checkbox_save_for_default_id = (By.ID, 'InputTypeCheckBoxProfitPercentageProfile_SaveAsDefaultProfile')

    approve_for_input_select = (By.ID, "InputTypeSearchListallowedToRole")
    approve_for_input_select_x_btn = (By.CLASS_NAME, 'css-19bqh2r')

    mark_all_btn = (By.CSS_SELECTOR, "button.commissionProfileButton.CommissionProfileSmallButton.btn.btn-secondary")
    clean_btn = (By.CSS_SELECTOR, "button.CommissionProfileDarkButton.CommissionProfileSmallButton.btn.btn-secondary")

    table_product = (By.ID, "TableRowID0_product")
    table_commission = (By.ID, "InputTypeNumberTableRowID0EditableNumber_resellerCommission")
    table_commission_section_20 = (By.ID, "TableRowID0_resellerCommissionP20")
    table_final_commission = (By.ID, "InputTypeNumberTableRowID0EditableNumber_commissionForFinalClient")
    table_licensed_to_label = (By.XPATH, "//label[@for='TableRowID0CheckBox_checkbox']")
    table_licensed_to_css_selector = (By.ID, "TableRowID0CheckBox_checkbox")
    save_btn = (By.ID, "profitPercentageProfileSaveButton")
