import React, { Fragment } from 'react';
import { Input, Label, Button } from 'reactstrap';
import { LanguageManager as LM } from "../LanguageManager/Language";
import { postUnpaidBill } from '../DataProvider/Functions/DataPoster';


/**
 * UnpaidTableRow - component that builds and returns an array of <td> elements from the data sended by unpaid paymends 
 */
export default class UnpaidTableRow extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            checked: false,
            unpaidId : this.props.tableRowObj.id,
        }
    }


    rowCheckBoxChange = (e) => {
        this.setState({ checked: !this.state.checked }, () => {
            if (this.props.onChecked) {
                this.props.onChecked(this.state.checked, this.state.unpaidId )
            }
        })
    };
    paySingel = () => {
        postUnpaidBill({ transaction_id: this.state.unpaidId  }).then(
            (res) => {
            }
        ).catch(
            (err) => {
                alert(err);
            }
        )
    };
    checkMy = () => {
        if (!this.state.checked) {
            this.setState({ checked: true }, () => {
                if (this.props.onChecked) {
                    this.props.onChecked(this.state.checked, this.state.unpaidId )
                }
            })
        }
        else {
            return
        }
    };
    unCheckMy = () => {

        if (this.state.checked) {

            this.setState({ checked: false }, () => {
                if (this.props.onChecked) {
                    this.props.onChecked(false, this.state.unpaidId )
                }
            })
        }
        else {
            return
        }
    };

    render() {
        var tableRowObj = this.props.tableRowObj;
        var tdArray = [];
        var mapKey = 0;

        tdArray = Object.keys(tableRowObj).map((key, index) => {
            if (key != "time") {
                return <td key={mapKey++}>{tableRowObj[key]}</td>
            }
        });
        tdArray.push(<td key={mapKey++}>
            <div className={"custom-control custom-checkbox " + (LM.getDirection() === "rtl" ? "checkbox-rtl" : "")}>
                <Input className="custom-control-input" checked={this.state.checked} type="checkbox" onChange={this.rowCheckBoxChange} id={this.props.id} value={this.props.id} />
                <Label className="custom-control-label" for={this.props.id} />
            </div>
        </td>)
        tdArray.push(<td key={mapKey++}>
            <div className={"custom-control custom-checkbox " + (LM.getDirection() === "rtl" ? "checkbox-rtl" : "")}>
                <Button style={{ marginLeft: '1px' }} onClick={this.paySingel} className="commissionProfileButton CommissionProfileSmallButton">                 {LM.getString('payment')}</Button>
            </div>
        </td>)
        return (
            <Fragment>
                {tdArray}
            </Fragment>
        );
    }
}