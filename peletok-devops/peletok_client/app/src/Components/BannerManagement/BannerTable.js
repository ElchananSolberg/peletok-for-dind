import React from 'react';
import { Button, Table, Row, Col } from 'reactstrap';
import BannerTableHead from './BannerTableHead.js';
import { Scrollbars } from 'react-custom-scrollbars';
import InputTypeCheckBox from '../InputUtils/InputTypeCheckBox'
import { LanguageManager as LM } from "../LanguageManager/Language";
import BannerTableRow from './BannerTableRow';
/**
 * BannerTable - component that builds the table from the data (this.props.tableRows and this.props.tableHeadersObj) sended by the father component.
 * It use BannerTableHead and BannerTableRow components.
 */
export default class BannerTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            inputTypeCheckBoxIncludingPayments: {
                disabled: false,
                lableText: LM.getString("IncludingPayments"),
                id: "IncludingPayments",
            },
            tableHeadersObj: {
                name: LM.getString("bannerNameWithoutPoints"),
                order: LM.getString("orderWithoutPoints"),
                isActive: LM.getString("bannerStatusWithoutPoints"),
            },
            refsobj: [],
            SomeLineIsChangingNow: false,
        }

    }

    onClick = () => {
        this.changeSomeLineIsChangingNow()
        if (this.props.isEdit) { this.props.isEdit() }
    }
    changeSomeLineIsChangingNow = () => {
        this.setState({ SomeLineIsChangingNow: !this.state.SomeLineIsChangingNow })
    }
    changeIsEdit = () => {
        this.state.refsobj.forEach(element => {
           element.iAmNotEditFun()
            
        });
        
        this.setState({ SomeLineIsChangingNow: false})
    }

    render() {
        var key = 0;
        var tableRows = this.props.tableRows;
        var trId = 'TableRowID';
        var index = 0;
        var trArray = tableRows.map((current) => {
            return <tr key={key++}>
                <BannerTableRow
                    SomeLineIsChangingNow={this.state.SomeLineIsChangingNow}
                    isEditState={this.props.isEditState}
                    cleanPage={this.props.cleanPage}
                    editRowFromTable={this.props.editRowFromTable}
                    ref={(componentObj) => { this.state.refsobj[current.id] = componentObj }}
                    id={trId + index++}
                    onClick={this.onClick}
                    tableRowObj={current} />
            </tr>
        });

        return (
            <React.Fragment>
                <div className={'table-responsive table-css ' + (LM.getDirection() === "rtl" ? "table-rtl" : "")}>
                    <Scrollbars style={{ width: '100%', height: '490px' }}>
                        <Table bordered striped size="sm" >
                            <thead className='color-gray' >
                                <tr>
                                    <BannerTableHead tableHeadersObj={this.state.tableHeadersObj} />
                                </tr>
                            </thead>
                            <tbody className='color-gray' >
                                {trArray}
                            </tbody>
                            <tfoot >
                                <tr >
                                    <th colSpan="6">
                                        <div style={{ minHeight: '23.5px' }} />
                                    </th>
                                </tr>
                            </tfoot>
                        </Table>
                    </Scrollbars>
                </div>
            </React.Fragment >
        );
    }
}