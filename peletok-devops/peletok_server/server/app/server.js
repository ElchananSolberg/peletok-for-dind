var createError = require('http-errors');
var express = require('express');
const bodyParser = require('body-parser');
// var logger = require('morgan');
var session = require('express-session');
var passport = require('passport');
var debug = require('debug')('learn:server');
var http = require('http');
// var flash = require('flash');
const AuthMiddleware = require('./middlewares/auth_middleware') 
const UserActivityLogMiddleware = require('./middlewares/user_activity_log') 
const TempPermissionLoggerMiddleware = require('./middlewares/temp_permission_logger') 
const routing = require("./routing");
// const {PtMailer, peletokMail, peletokDevMail} = require('./ptMailer')
// csonst {branceName, userName} = require('./config/gitSettingReadOnly')
const logger = require('./logger')

const sequelize = require('./setupSequelize.js')
const MongoStore = require('connect-mongo')(session);
const {MongoLogger} = require('./mongoLogger')

var app = express();
// app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
// TODO: Change store for production
// TODO: reconsider values for resave and for saveUninitialized
app.use(session({
    secret: 'peletok_ravtech',
    resave: false,
    saveUninitialized: true, // TODO change to ture
    store: new MongoStore({ mongooseConnection: MongoLogger.connection }), 
    cookie: {maxAge: 1000 * 60 * 60 * 12 * 14} // check how long a session shuold be...
}));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(passport.initialize());
app.use(passport.session());
// app.use(flash());

// utility for filtering  non allowed object by white list of allowed ids and their key in the object
// should run once when system init (server.js) 
Array.prototype.filterByWhiteList =  function (field, authItemIds) {
    var filtered = [];
    for(let i = 0; i < this.length; i++) {
      if(authItemIds.indexOf(this[i][field]) >= 0){
          filtered.push(this[i]);
      }
    }
    return filtered;
};


//Access-Control-Allow-Origin
app.use(function (req, res, next) {
     
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', process.env["CLIENT_URL"] || 'http://node-app:8080'  || 'http://locallhost:3000' || 'http://locallhost:8000' || 'http://rmtkts.co/' || 'http://ravkavonline.co.il/' || 'https://test-ravkavonline.co/');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

app.use((req, res, next) => {
  let oldSend = res.send;

  res.send = function (body) {
    
    MongoLogger.logResponses({
      transactionId: req.transactionId,
      activityId: req.activity_id,
      userId: req.user ? req.user.id : null,
      businessId: req.user ? req.user.business_id : null,
      body,

    })
    
    return oldSend.apply(res, arguments);
  };

  
  next();
})



const versionNumber = "v1/";
const baseUrl = "/api/";
// app.use(AuthMiddleware);
app.use(UserActivityLogMiddleware);
app.use(TempPermissionLoggerMiddleware);

app.use(function (req, res, next) {
    let oldConsoleError = console.error;
    console.error = (error) => {
        
            MongoLogger.logConsoleError({
              transactionId: req.transactionId,
              env: process.env.NODE_ENV || 'development',
              activityId: req.activity_id,
              userId: req.user ? req.user.id : null,
              businessId: req.user ? req.user.business_id : null,
              error,
            })
        
        
      // if(!rejectedStrings.find(r=>err.indexOf(s) > -1)){
          // const env = process.env.NODE_ENV || 'development'
          // let text = `Env: ${env} User: ${req.user ? req.user.id : 'None'} Activity: ${req.user ? req.acivity_id : 'None'}\n\n${err}`
          // PtMailer.send(peletokMail, peletokDevMail,`Error notification. ENV: ${env}, [${err.message}]`, text, null, null )
      // }
      return oldConsoleError.apply(console, arguments);
    };
    next()
});


    


app.use(baseUrl + versionNumber, routing);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    console.log("originalUrl: ", req.originalUrl);
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;

    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    throw err;
    // res.send(err.toString());
});




/**
 * Get port from environment and store in Express.
 */

var port = normalizePort(process.env.PORT || '8080');
app.set('port', port);


/**
 * Create HTTP server.
 */
var server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

    // handle specific listen errors with friendly messages
    //TODO: Check this
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    debug('Listening on ' + bind);
}

module.exports = app;
