const express = require('express');
const models = require('../index')
const Product = models.product.model;
const Sequelize = require('sequelize');
const Op = Sequelize.Op
const productRouter = express.Router();
const moment = require('moment')
const {filterIfAllObjectsFieldsMissing, mapByType, isBoolean} = require('./apiUtils')
const {
    cleanRawKeys,
    getTypeNum,
    extractFormDataAndUploadFiles,
    getNumStatus
} = require('../utils')
const path = require("path");
const productsImagesDir = 'productsImages'
const initialEndDate = '2099-12-31'
const language_code_he = 'HE'
const language_code_en = 'EN'
const language_code_ar = 'AR'
const {snakeCase, mapKeys} = require('lodash');
const { Info } = require("../mapping/crudMiddleware");
const activeProductsStatus = 1

productRouter.post("/",async (req, res) => {
    
    let data = await extractFormDataAndUploadFiles(req, productsImagesDir)

    const product = await Product.create({
        upscale_product_id: data.upscale_product_id || null,
        more_details_link: data.more_details_link,
        video_link: data.video_link,
        call_terms: data.call_terms,
        supplier_identity_number:data.supplier_identity_number,
        order: data.order || undefined,
        more_info_description: data.more_info_description,
        more_info_youtube_url: data.more_info_youtube_url,
        more_info_site_url: data.more_info_site_url,
        supplier_id: data.supplier_id,
        id: data.id || undefined,
        status_author_id: req.user.id,
        status_timestamp: moment(),
        usage_time_limit: data.usage_time_limit || undefined,
        type: data.type,
        is_abstract: !!data.is_abstract,
        profit_model: data.profit_model || undefined,
        call_minute: data.call_minute || undefined,
        sms: data.sms || undefined,
        browsing_package: data.browsing_package || undefined,
        call_to_palestinian: !!data.call_to_palestinian,
        abroed_price: data.abroed_price || undefined,
        other1: data.other1 || undefined,
        other2: data.other2 || undefined,
        status: data.status ? getNumStatus('active', 'Product') : getNumStatus('inactive', 'Product'),
        default_is_allow: !!data.default_is_allow,
        cancelable: !!data.cancelable,
        ProductDetails: [ 
            {
                 product_description: data.description_he || '',
                 product_name: data.name_he || '',
                 language_code: language_code_he,
            },
            {
                 product_description: data.description_en || '',
                 product_name: data.name_en || '',
                 language_code: language_code_en,
            },
            {
                 product_description: data.description_ar || '',
                 product_name: data.name_ar || '',
                 language_code: language_code_ar,
            },
        ],
        Document: filterIfAllObjectsFieldsMissing([
            {url: data.image, type: getTypeNum('image', 'Document')},
            {url: data.more_info_image, type: getTypeNum('other_image', 'Document')}
        ],'url'), 
        ProductPrice: filterIfAllObjectsFieldsMissing([
            {
                fixed_price: data.price || null,
                buying_price: data.buying_price || null,
                distribution_fee: data.distribution_fee || null,
                timestamp_start: moment(),
                timestamp_end: moment(initialEndDate)
            }
        ],['fixed_price', 'buying_price', 'distribution_fee']), 
        ProductBarcode:  data.barcodes ? JSON.parse(data.barcodes) : [], // TODO server side validation
        TalkPrice:  data.talk_prices ? JSON.parse(data.talk_prices).map(t=>mapKeys(t,(v,k)=>snakeCase(k))) : [] // TODO server side validation
    },{
        include: [
            {
                model: models.productDetails.model, as: 'ProductDetails'
            },
            {
                model: models.document.model, as: 'Document'
            },
            {
                model: models.productPrice.model, as: 'ProductPrice'
            },
            {
                model: models.productBarcode.model, as: 'ProductBarcode'
            },
            {
                model: models.talkPrice.model, as: 'TalkPrice'
            }
        ]
    }).catch((e)=>console.log(e))
    let tags = await models.tag.model.findAll({
        where: {id: data.tags ? JSON.parse(data.tags) : []}
    })
    await product.addTags(tags.filter(t=>t.type==getTypeNum('product', 'Tag')))

    res.send('success')
} );


productRouter.get("/products/:supplier_id", async function (req, res) {
    
    let whereObj = { supplier_id: req.params.supplier_id}
    if (req.query.filter) {
        whereObj.status = activeProductsStatus
    }

    let products = await Product.findAll({
        attributes: [
             'id',
             'type',
             'supplier_id',
             'call_terms',
             'call_minute',
             'sms',
             'browsing_package',
             'call_to_palestinian',
             'abroed_price',
             'other1',
             'other2',
             'upscale_product_id',
         ],
        where: whereObj,
        include: [
            {
                model: models.productDetails.model,
                as: 'ProductDetails',
                required: false,
                where: {language_code: req.user.language_code},
                attributes: [['product_name', 'name'], ['product_description', 'description']]
            },
            {
                attributes: [['product_id', 'id'],'favorite','final_commission'],
                model: models.businessProduct.model,
                as: 'BusinessProduct',
                where: {
                    business_id: req.user.business_id,
                    is_authorized: true,
                },
            },
            {
                    model: models.productDetails.model,
                    where: {language_code: language_code_he},
                    as: 'ProductDetailsHe',
                    required: false,
                    attributes: [
                        ['product_name', 'name_he'],
                    ]
             },
            {
                attributes: [['url', 'image']],
                model: models.document.model,
                required: false,
                as: 'Document',
                where: {
                    type: getTypeNum('image', 'Document')
                }
            },
            {
                attributes: ['max_payment'],
                model: models.supplier.model,
                required: false,
                as: 'Supplier',
            },
            {
                attributes: [['fixed_price', 'price']],
                model: models.productPrice.model,
                as: 'ProductPrice',
                required: false,
                where: {
                    timestamp_end: {
                        [Op.gte]: moment()
                    }
                }
            }
            
        ],
        
        raw: true
    })

    let tags = await models.productTag.model.findAll({
        attributes: ['product_id', [Sequelize.fn('ARRAY_TO_STRING',Sequelize.fn('ARRAY_AGG', Sequelize.col('tag_id')), ','), 'tags']],
        where: {product_id: products.map(p=>p.id)},
        group:['product_id'],
        raw: true
    })
    tags = tags.reduce((n,t)=>Object.assign(n, {[t.product_id]: (t.tags || '').split(',').map(t=>parseInt(t))}),{})
    return res.send(mapByType('product', products.map(p=>Object.assign(cleanRawKeys(p, false),{tags: tags[p.id] || []}))))
})

productRouter.get("/tags", async function (req, res) {
    let tags = await models.tag.model.findAll({
        attributes: ['id',['tag_name', 'name']],
        where: {type: getTypeNum('product', 'Tag')},
        order: [['order', 'ASC']],
        raw: true
    })
    res.send(tags)
})

productRouter.get("/:product_id", async function (req, res) {
    const [product, barcodes, tags, talk_prices, documents] = await Promise.all([ Product.findOne({
            attributes: [
                'id',
                'type',
                'supplier_id',
                'order',
                'call_terms',
                'profit_model',
                'usage_time_limit',
                'status',
                'more_info_description',
                'more_info_youtube_url',
                'more_info_site_url',
                'call_minute',
                'sms',
                'supplier_identity_number',
                'browsing_package',
                'call_to_palestinian',
                'abroed_price',
                'other1',
                'other2',
                'status',
                'default_is_allow',
                'cancelable',
                'upscale_product_id'
            ],

           where: {
                id: req.params.product_id
            },
            include: [
                {
                    model: models.productDetails.model,
                    as: 'ProductDetails',
                    required: false,
                    where: {language_code: req.user.language_code},
                    attributes: [
                        ['product_name', 'name'],
                        ['product_description', 'description'],
                    ]
                },
                 {
                    model: models.productDetails.model,
                    where: {language_code: language_code_he},
                    as: 'ProductDetailsHe',
                    required: false,
                    attributes: [
                        ['product_name', 'name_he'],
                    ]
                },
                 {
                    model: models.productDetails.model,
                    where: {language_code: language_code_ar},
                    as: 'ProductDetailsAr',
                    required: false,
                    attributes: [
                        ['product_name', 'name_ar'],
                    ]
                },
                {
                    model: models.productDetails.model,
                    where: {language_code: language_code_en},
                    as: 'ProductDetailsEn',
                    required: false,
                    attributes: [
                        ['product_name', 'name_en'],
                    ]
                },
                {
                    attributes: [['product_id', 'id']],
                    model: models.businessProduct.model,
                    as: 'BusinessProduct',
                    required: true,
                    where: {
                        business_id: req.user.business_id,
                        is_authorized: true,
                    },
                },
                {
                    attributes: [['fixed_price', 'price'], 'buying_price', 'distribution_fee'],
                    model: models.productPrice.model,
                    as: 'ProductPrice',
                    required: false,
                    where: {
                        timestamp_end: {
                            [Op.gte]: moment()
                        }
                    }
                }
            ],
            raw: true
        }),
         models.productBarcode.model.findAll({
            attributes: ['barcode_type_id', 'barcode'],
            where: {product_id: req.params.product_id},
            raw: true
        }),
        models.tag.model.findAll({
            attributes: ['id'],
            include: [
                {
                    model: models.product.model,
                    as: 'Products',
                    where: {id: req.params.product_id}
                }
            ],
            includeIgnoreAttributes: false,
            raw: true
        }),
        models.talkPrice.model.findAll({
            attributes: [['country', 'country'], ['outcome_price', 'outcomePrice'], ['income_price', 'incomePrice'], ['sms_price', 'smsPrice'],],
            where: {product_id: req.params.product_id},
            raw: true
        }),
        models.document.model.findAll({
            attributes: ['url', 'type'],
            include: [
                {
                    model: models.product.model,
                    as: 'Products',
                    where: {id: req.params.product_id}
                }
            ],
            includeIgnoreAttributes: false,
            raw: true
        }).catch(e=>console.error(e)),
     ])
    if(product){
        let response = Object.assign(cleanRawKeys(product, false), 
            {
                barcodes,  tags:
                tags.map(t=>t.id),
                talk_prices,
                image: (documents.find(d=>d.type == getTypeNum('image', 'Document')) || {}).url,
                more_info_image: (documents.find(d=>d.type == getTypeNum('other_image', 'Document')) || {}).url
            })
        res.send(response)
    } else {
        res.status(401).send('Unauthorized')
    }
})

productRouter.put('/:product_id', async function (req, res) {
    const CurrentBusinessProduct = Product.scope({ method: ['BusinessProduct', req.user.business_id]});
    const product = await CurrentBusinessProduct.findOne({
         where: {id: req.params.product_id},
         include: [
            {model: models.document.model, as: 'Document', required: false},
            {model: models.productDetails.model, as: 'ProductDetails', required: false},
            {model: models.productBarcode.model, as: 'ProductBarcode', required: false},
            {model: models.tag.model, as: 'Tags', required: false},
            {model: models.talkPrice.model, as: 'TalkPrice', required: false},
            {
                model: models.productPrice.model,
                as: 'ProductPrice',
                where: {timestamp_end: moment(initialEndDate).toDate()},
                required: false
            },
        ]
    })
    if(!product){
        res.send(401, 'Unauthorized')
    } else {

        let data = await extractFormDataAndUploadFiles(req, productsImagesDir)
        let barcodes = data.barcodes ? JSON.parse(data.barcodes) : []
        let newBarcodes = barcodes.filter(b=>!product.ProductBarcode.find(pb=>b.barcode_type_id == pb.barcode_type_id))
        let tags = await models.tag.model.findAll({
            where: {id: data.tags ? JSON.parse(data.tags) : []}
        }).filter(t=>t.type==getTypeNum('product', 'Tag'))
        let image = product.Document.find(d=>d.type==getTypeNum('image', 'Document'))
        let more_info_image = product.Document.find(d=>d.type==getTypeNum('other_image', 'Document'))
        
        if(data.talk_prices){
            product.TalkPrice.forEach(tp=>tp.destroy())
        }
                
        await Promise.all([
                product.update({
                    more_details_link: data.more_details_link,
                    video_link: data.video_link,
                    call_terms: data.call_terms,
                    order: data.order || null,
                    supplier_id: data.supplier_id,
                    id: data.id,
                    upscale_product_id: data.upscale_product_id || null,
                    supplier_identity_number: data.supplier_identity_number,
                    status: data.status,
                    status_author_id: data.status ? req.user.id : undefined,
                    status_timestamp: data.status ? moment() : undefined,
                    usage_time_limit: data.usage_time_limit,
                    type: data.type,
                    more_info_description: data.more_info_description,
                    more_info_youtube_url: data.more_info_youtube_url,
                    more_info_site_url: data.more_info_site_url,
                    // is_abstract: data.is_abstract,
                    profit_model: data.profit_model || product.profit_model,
                    call_minute: data.call_minute,
                    sms: data.sms,
                    browsing_package: data.browsing_package,
                    call_to_palestinian: data.call_to_palestinian,
                    abroed_price: data.abroed_price,
                    other1: data.other1,
                    other2: data.other2,
                    status: data.status == 'true' ? getNumStatus('active', 'Product') : getNumStatus('inactive', 'Product'),
                    default_is_allow: data.default_is_allow,
                    cancelable: data.cancelable,   
                }),
                product.ProductDetails.find(p=>p.language_code == language_code_he).update({
                    product_name: data.name_he,
                    product_description: data.description_he
                }),
                product.ProductDetails.find(p=>p.language_code == language_code_en).update({
                    product_name: data.name_en,
                    product_description: data.description_en
                }),
                product.ProductDetails.find(p=>p.language_code == language_code_ar).update({
                    product_name: data.name_ar,
                    product_description: data.description_ar
                }),
                product.setTags(tags),
                data.talk_prices ? models.talkPrice.model.bulkCreate(JSON.parse(data.talk_prices).map(t=>Object.assign(mapKeys(t,(v,k)=>snakeCase(k)), {product_id: req.params.product_id}))) : Promise.resolve(),
                data.price || data.buying_price ? product.ProductPrice.filter(pp=>moment(pp.timestamp_end) > moment()).forEach(pp=>pp.update({timestamp_end: moment()})) : Promise.resolve(),
                ...product.ProductBarcode.map(pb=>barcodes.filter(b=>b.barcode_type_id == pb.barcode_type_id) == 0 ? pb.destroy() : pb.update({barcode: barcodes.find(b=>b.barcode_type_id == pb.barcode_type_id).barcode})),

        ])
        let newImage;
        image && data.image ? await image.update({
            url: data.image
        }) : data.image ? newImage = await models.document.model.create({
            url: data.image,
            type: getTypeNum('image', 'Document'),
            product_id: product.id
        }) : '';
        let newMoreInfoImage;
        more_info_image && data.more_info_image ? more_info_image.update({
            url: data.more_info_image
        }) : data.more_info_image ? newMoreInfoImage = await models.document.model.create({
            url: data.more_info_image,
            type: getTypeNum('other_image', 'Document'),
            product_id: product.id
        }) : '';
        let newDocuments = [newImage, newMoreInfoImage].filter(d=>d).map(d=>d.id)
        await product.addDocument(newDocuments)
        if(data.price || data.buying_price){
             await models.productPrice.model.create({
                fixed_price: data.price,
                buying_price: data.buying_price,
                distribution_fee: data.distribution_fee,
                product_id: req.params.product_id,
                timestamp_start: moment(),
                timestamp_end: moment(initialEndDate)
            }) 
        }
        
        await models.productBarcode.model.bulkCreate(newBarcodes.map(b=>Object.assign(b, {product_id: req.params.product_id}))) 
        res.send('success')
    }
} )

productRouter.post("/favorite/:product_id", async function (req, res) {
    let info = new Info()
    await models.businessProduct.model.update({ favorite: req.body.favorite }, { where: { product_id: req.params.product_id,business_id:req.user.business_id } }).catch(err => {
        info.errors.push(err)
    })
    res.customSend(info)
})

productRouter.get('/more_info/:product_id', async function (req, res) {
    const [product, talk_prices] = await Promise.all([ Product.findOne({
            attributes: [
                'id',
                'call_terms',
                'more_info_description',
                'more_info_youtube_url',
                'more_info_site_url',
                'call_minute',
                'sms',
                'browsing_package',
                'call_to_palestinian',
                'abroed_price',
                'other1',
                'other2',
                'supplier_color'
            ],

           where: {
                id: req.params.product_id
            },
            include: [
                {
                    model: models.productDetails.model,
                    as: 'ProductDetails',
                    required: false,
                    where: {language_code: req.user.language_code},
                    attributes: [
                        ['product_name', 'name'],
                        ['product_description', 'description'],
                    ]
                },
                 {
                    model: models.productDetails.model,
                    where: {language_code: language_code_he},
                    as: 'ProductDetailsHe',
                    required: false,
                    attributes: [
                        ['product_name', 'name_he'],
                    ]
                },
                 {
                    model: models.productDetails.model,
                    where: {language_code: language_code_ar},
                    as: 'ProductDetailsAr',
                    required: false,
                    attributes: [
                        ['product_name', 'name_ar'],
                    ]
                },
                {
                    model: models.productDetails.model,
                    where: {language_code: language_code_en},
                    as: 'ProductDetailsEn',
                    required: false,
                    attributes: [
                        ['product_name', 'name_en'],
                    ]
                },
                {
                    attributes: [['product_id', 'id']],
                    model: models.businessProduct.model,
                    as: 'BusinessProduct',
                    required: true,
                    where: {
                        business_id: req.user.business_id,
                        is_authorized: true,
                    },
                },
                {
                    attributes: [['fixed_price', 'price']],
                    model: models.productPrice.model,
                    as: 'ProductPrice',
                    required: false,
                    where: {
                        timestamp_end: {
                            [Op.gte]: moment()
                        }
                    }
                },
                {
                    attributes: [['url', 'more_info_image']],
                    model: models.document.model,
                    required: false,
                    as: 'Document',
                    where: {type: getTypeNum('other_image', 'Document')}
                }
            ],
            raw: true
        }),
         
        models.talkPrice.model.findAll({
            attributes: [['country', 'country'], ['outcome_price', 'outcomePrice'], ['income_price', 'incomePrice'], ['sms_price', 'smsPrice'],],
            where: {product_id: req.params.product_id},
            raw: true
        })
        
     ])
    if(product){
        let response = Object.assign(cleanRawKeys(product, false), {talk_prices})
        res.send(response)
    } else {
        res.status(401).send('Unauthorized')
    }
})

productRouter.get('/supplier_details/:product_id', async function (req, res) {
    const supplier = await models.supplier.model.findOne({
        include: [
            {
                model: models.product.model,
                as: 'Product',
                where: {id: req.params.product_id}
            }

        ]
    })
    res.send({
        supplierProfitPrecentage: supplier.max_percentage_profit,
        minFinalCommission: supplier.min_percentage_profit,
    })
})

productRouter.put('/update_authorization/:product_id', async function (req, res) {
    const supplier = await models.supplier.model.findOne({
        include: [
            {
                model: models.product.model,
                as: 'Product',
                where: {id: req.params.product_id}
            }

        ]
    })
    if(req.body.percentage_profit && req.body.percentage_profit > supplier.max_percentage_profit){
        return res.status(401).send('Unauthorized action')
    }
    let data = [
        'percentage_profit',
        'points',
        'business_commission',
        'final_commission',
        'is_authorized'
    ].reduce((n, k)=>Object.assign(n, req.body[k] != undefined ? {[k]:  req.body[k]} : {}),{})
    let options = {where: {id: {[Op.ne]: 1}}, attributes: ['id'], raw: true}
    if(req.body.businessType == 'distributor'){
        options.where = Object.assign(options.where, {is_distributor: true}) 
    }else if(req.body.businessType == 'seller') {
        options.where = Object.assign(options.where, {is_distributor: {[Op.ne]: true}})
    }
    const businesses = await models.business.model.findAll(options)
    await models.businessProduct.model.update(data,{
        where: {product_id: req.params.product_id, business_id: businesses.map(b=>b.id)}
    })

    res.send('success')

})

productRouter.post('/clone_porduct_auth', async function (req, res) {
    
    let sources = models.businessProduct.model.findAll({
        where:{
            product_id: req.body.sourceProductId
        },
        raw: true
    })
    await Promise.all([
        sources.map(bp=>models.businessProduct.model.update(['business_commission', 'percentage_profit', 'final_commission', 'points', 'is_authorized', 'favorite', 'distribution_fee'].reduce((n, k)=>Object.assign(n, {[k]: bp[k]})), {
            where: {product_id: req.body.targetProductId, business_id: bp.business_id}
        }))
    ])

    res.send('success')

})








module.exports.productRouter = productRouter;