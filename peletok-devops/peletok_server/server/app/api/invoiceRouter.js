const express = require('express');
const invoiceRouter = express.Router();
const models = require('../index.js');
const Sequelize = require('sequelize');
const Op = Sequelize.Op
const fs = require('fs')
const {PtMailer, peletokMail} = require('../ptMailer')
const LM = require('../config/LanguageManager').LM;
const {uploadInvoiceAndSendMAil} = require('../utils')
const moment = require('moment')
const path = require("path");


const { promisify } = require("util");
const readFilePromise = promisify(fs.readFile)

invoiceRouter.post('/', async (req, res)=>{
	 
	 let info = await uploadInvoiceAndSendMAil(req, res).catch(err=>{
	 	console.log(err)
	 })
	 
	 
	 
	 res.customSend(info);
})

invoiceRouter.get('/', async (req, res)=>{
	const currentBusiness = await models.business.model.findOne({
		where: {id: req.user.business_id}
	})
	let filter = {customer_number: currentBusiness.business_identifier}
	const {year, month} = req.query
	if(year && month){
		let startOfMonth = moment([year, month - 1])
		let endOfMonth   = moment(startOfMonth).endOf('month')
		filter.invoice_date = {[Op.between]: [startOfMonth.toDate(), endOfMonth.toDate()]}
		
	}else if (year){
		let startOfMonth = moment([year, 0])
		let endOfMonth   =moment([year, 11]).endOf('month')
		filter.invoice_date = {[Op.between]: [startOfMonth.toDate(), endOfMonth.toDate()]}
	}
	console.log(filter)
	let invoice = await models.invoice.model.findAll({
		attributes: ['id',[Sequelize.fn('to_char', Sequelize.col('invoice_date'), 'YYYY-MM-DD'), 'invoice_date'] ,'invoice_number' ,'customer_number', 'path'],
		where: filter,
		raw: true,
	})
	res.send(invoice)
})

invoiceRouter.post('/subscribe/', async (req, res)=>{
	models.business.model.update({invoice_email: req.body.address},{
		where: {id: req.user.business_id}
	})
	res.send('success')
})

invoiceRouter.post('/unsubscribe/', async (req, res)=>{
	models.business.model.update({invoice_email: null},{
		where: {id: req.body.business_id}
	})
	res.send('success')
})

invoiceRouter.post('/is_subscribe/', async (req, res)=>{
	const filter = ['id', 'business_identifier'].reduce((newObj, i)=>Object.assign(newObj,req.body[i] ? {[i]: req.body[i]}: {}),{invoice_email:{ [Op.ne]: null}})
	let business;
	if(Object.keys(filter).length > 0){
		business = await models.business.model.findOne({
			where: filter
		})
	}
	res.send({isSubscribe: !!business, seller_id: business ? business.id : null})
})

invoiceRouter.post('/send_email/', async (req, res)=>{
	// protect from injection
	const currentBusiness = await models.business.model.findOne({
		where: {id: req.user.business_id}
	})
	const invoices = await models.invoice.model.findAll({
		where: {id: req.body.invoices, customer_number: currentBusiness.business_identifier}
	})
	let attachments = invoices.map(i=>{return {fileName: i.path.split('.')[0], path: path.join(__dirname, '..'+i.path)}})
	
	await PtMailer.send(peletokMail, req.body.to, LM.getString('invoice_mail_subject'), LM.getString('invoice_mail_text'), null, attachments )
	res.send('success')
})

module.exports.invoiceRouter = invoiceRouter;



