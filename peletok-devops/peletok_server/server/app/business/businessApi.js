const models = require('../index.js')
const businessModel = models.business.model
const transactionModel = models.transaction.model
const businessContactModel = models.businessContact.model
const businessAddressModel = models.businessAddress.model
const addressModel = models.address.model
const sequelize = require('sequelize')
const Op = require('sequelize').Op
const { Info } = require("../mapping/crudMiddleware");
const moment = require('moment')
// When sequelize 'finAll',findOne', 'get' etc'.. function finds no values it returns empty array. when performing a query that uses that returned value it converts to NULL
// for example  IN([]) = IN (NULL), to avoid errors in such situations were pushing 'avoidNull' const to the empty array and the output is: IN(-1)
const avoidNull = -1
const agentRole = 2
const logger = require('../logger')
/**
 * delete business
 * Only new business, not distributor and it hasn't made a transaction yet)
 */
async function deleteBusiness(req, res) {
    await businessModel.findOne({
        where: {id: req.params.id, is_distributor: false},
        include: {
            model: transactionModel,
            as: "Transaction",
        }
    }).then( async business => {
        if (business && business.Transaction.length===0) {
           await business.update({business_identifier: ''});
          await  business.destroy().then(info => {
                res.send(info)
            })
        } else {
            res.status(400).send('failed');
        }
    })
}
/**
 * create and delete business suppliers according to req params
 * @param {*} req 
 * @param {*} res 
 */
async function updateBusinessSupplier (req, res) {
    let business_id = req.body.business_id
    req.body.suppliers_list.forEach((businessSupplierRow)=>{
            businessSupplierRow['timestamp_start'] = moment().toDate()
            businessSupplierRow['author_id'] = req.user.id
            businessSupplierRow['status_author_id'] = req.user.id
            businessSupplierRow['approved_by_id'] = req.user.id
    })
    let info = new Info()
    const currentBusinessSuppliersIds = (await models.businessSupplier.model.findAll({where:{business_id:req.user.business_id,is_authorized:true}})).map((businessSupplierRow)=>{return businessSupplierRow.supplier_id})

   await Promise.all([ models.businessSupplier.model.update({is_authorized: true},{where: {supplier_id: [...req.body.suppliers_list.filter(s=>s.is_authorized).filter(s=>currentBusinessSuppliersIds.indexOf(s.id) > -1 ).map(s=>s.id), avoidNull],business_id:business_id}}).catch(errors=>{
        info.errors.push(errors)
    }),
    models.businessSupplier.model.update({is_authorized: false},{where: {supplier_id: [...req.body.suppliers_list.filter(s=>!s.is_authorized).filter(s=>currentBusinessSuppliersIds.indexOf(s.id) > -1 ).map(s=>s.id), avoidNull],business_id:business_id}}).catch(errors=>{
        info.errors.push(errors)
    })])
    res.customSend(info)
}

/**
 * Returns all list of business with id and business_identifier
 * @param req: request
 * @param res: response
 * @returns {Promise<void>}
 */
async function getBusinessId(req, res) {
    let info = new Info();
    await businessModel.findAll({attributes: ["id", "business_identifier"]}).then(r => {
        info.result = r;
    }).catch(e => {
        info.errors.push(e);
    });
    res.customSend(info)
}

/**
 * Gets all users with agent role
 * @param {} req 
 * @param {*} res 
 */
async function getAgents(req,res){
   let agents = await models.user.model.findAll({
        include:[
            {
                model:models.role.model,
                where:{id:agentRole},
                attributes:[]
            },
            {
                model: models.userBusiness.model,
                as:'UserBusiness',
                where: {business_id: req.user.business_id},
                required: true
            }
        ],
        attributes:[[sequelize.fn("concat",
        sequelize.col("first_name"), ' ', sequelize.col("last_name")), "name"],'id']
    }).catch(err=>{
        logger.error.message(err)
    })
    res.send(agents)
}
/**
 * Gets all business profiles
 * @param {*} req 
 * @param {*} res 
 */
async function getBusinessProfiles(req, res){
    const UserRoles = models.role.model.scope({ method: ['userRoles', req.user.id]});
    const authorizedRoles = await UserRoles.findAll({attributes: ['id']}).map(r=>r.id)
    let [profit, commission, supplier] = await Promise.all([
            models.business.model.findAll({ where: { is_abstract: true, abstract_type: "profit profile" },attributes:['id','name'], include: {model: models.role.model}}),
            models.business.model.findAll({ where: { is_abstract: true, abstract_type: 'commission profile' },attributes:['id','name'], include: {model: models.role.model}}),
            models.business.model.findAll({ where: { is_abstract: true, abstract_type: 'supplier profile' },attributes:['id','name'], include: {model: models.role.model}})
         ])
    profiles = {
        profit: profit.filter(p=>p.Roles.filter(r=>authorizedRoles.indexOf(r.id) > -1).length > 0),
        commission: commission.filter(p=>p.Roles.filter(r=>authorizedRoles.indexOf(r.id) > -1).length > 0),
        supplier: supplier.filter(p=>p.Roles.filter(r=>authorizedRoles.indexOf(r.id) > -1).length > 0)
    }
    res.send(profiles)
}

async function isDistributorUseSection20(distributor_id){
  return (await models.business.model.count({where:{distributor_id:distributor_id,is_distributor:true,use_article_20:true}})) > 0
}
module.exports.deleteBusiness = deleteBusiness
module.exports.updateBusinessSupplier = updateBusinessSupplier
module.exports.getBusinessId = getBusinessId;
module.exports.getAgents = getAgents;
module.exports.getBusinessProfiles = getBusinessProfiles;
module.exports.isDistributorUseSection20 = isDistributorUseSection20;
