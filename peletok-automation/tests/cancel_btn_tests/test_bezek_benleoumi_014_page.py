from tests.base_project_test_class import *

@pytest.mark.skip
class TestBezekBenleoumi014CardsCancelBtns(BaseProjectTestClass):

    def login_and_get_to_bezek_benleoumi_014_page(self):
        self.login()
        self.driver.tools.wait_and_click(HomePage.bezek_benleoumi_014)
        self.driver.wait.wait_for_element_to_be_present(VerificationPage.price_bezek_beleoumi_014)

    def test_cancel_btn_card_leoumi_50(self):
        self.login_and_get_to_bezek_benleoumi_014_page()
        self.pay_for_calling_card_vc(50, "205/1600", cancel_payment=True)
        self.remove_logs_dir = True


