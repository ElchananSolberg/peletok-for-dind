const axios = require('axios').default;
const logs = require('../../../logger')
const Info = require('../../../resInfo')
const qs = require("querystring");
const PaymentBasic = require('../../paymentBasic');
const urlPrice = "https://city4u.co.il/WebApiCityPay/v1/PayMethods/CheckPayment/0/"
const urlPayment = "https://city4u.co.il/WebApiCityPay/v1/PayMethods/PayBillWithPayerDetails"
const LM = require('../../../config/LanguageManager').LM
var models = require('../../../index')
const error_num_car = 2009
const error_num_bill = 2015
axios.defaults.withCredentials = true
/***
 * class to  pay  bills  from  www.city4u.co.il
 */
class City4uCrawler extends PaymentBasic {
    constructor(url, billNum, carNum, price, req) {
        super()
        this.req=req
        this.price = price
        this.url = url
        this.billNum = billNum
        this.carNum = carNum
        this.info = new Info()
        logs.info("url is ", url + " billNum  is ", billNum, "carNum is ", carNum)
    }
    /***
     * check  what price of  bill/product for transaction 
     * 
     */
    getPrice() {
        this.info.price = this.price
        return this.info
    }
    /***
     * return price of  bill 
     */
    async getBillPrice() {
        if ((await this.isDemoTransactionUser())) {
            this.info.send = { "success": 150 }
            this.info.price="150"
            return  this.info
          
          }
          else {


        const urlGetPrice = urlPrice + this.url.slice(48, 55) + this.url.slice(63) + "/" + this.carNum + "/" + this.billNum + "/false"
        let res = await axios.get(urlGetPrice)
            .catch(e => {
                this.info.setErrMessage(LM.getErrorBasic(), "err_connection_site" + e.toString())
                this.info.error.err_connection_site_step1 = true
                return this.info
            })

        if (res.data["Sum"] == 0) {

            if (res.data["ActionCode"] == error_num_car) {
                this.info.setErrMessage(LM.getString("ErrorInNumCar"), res.data["Description"])
            };
            if (res.data["ActionCode"] == error_num_bill) {
                this.info.setErrMessage(LM.getString("ErrorInNumBill"), res.data["Description"])
            }
            else if (res.data["ActionCode"] == 2012) { this.info.setErrMessage(LM.getString("No_balance_payable"), res.data["Description"]) }
            else this.info.setErrMessage(LM.getErrorBasic(), res.data["Description"])
            return this.info
        }
        this.info.price = res.data["Sum"];
        this.info.send = { "success": this.info.price }
        return this.info
    }
}
    /***
     * to  pay   a bill 
     * return "this.info.confirmation"
     * TODO   change creditCardData
     */
    async pay(transactionId, creditCardId) {
        if ((await this.isDemoTransactionUser())) {
            return this.demoRespons()
        }
        else {



            let creditCard = await models.creditCard.model.findOne({
                where: { id: creditCardId }
            });
            const creditType = "1"
            const customerNumber = this.url.slice(48, 54)
            const serviceNumber = this.url.slice(63)
            const paymentsNumber = "1"
            const transactionId = "0"
            const callerId = "990"
            const isGCenter = "0"
            const fieldTitle1 = "MisparMovil"
            const fieldTitle2 = "MisparRechev"
            const body = {
                CCValidDate: "0623",
                CreditNumber: "4580110705790910",
                Cvv: 969,
                // CreditNumber: creditCard.creditCard.cc_number,
                // CCValidDate: creditCard.cc_validity_month + creditCard.cc_validity_year,
                // CVV: creditCard.cc_cvv_number,

                CreditType: creditType,
                CustomerNumber: customerNumber,
                Cvv: 969,
                Email: "gurvicheli@gmail.com",
                FieldTitle1: fieldTitle1,
                FieldTitle2: fieldTitle2,
                FieldValue1: this.carNum,
                FieldValue2: this.billNum,
                IdNumber: "012163234",
                PayerFullAddress: "bublik",
                PayerFullName: "gurvich",
                PayerPhoneNumber: "0548408908",
                PaymentsNumber: paymentsNumber,
                ServiceNumber: serviceNumber,
                Sum: this.price,
                TransactionId: transactionId,
                callerId: callerId,
                isGCenter: isGCenter
            }

            let res = await axios.put(urlPayment, qs.stringify(body), {
                withCredentials: true
            })
                .catch(e => {
                    this.info.setErrMessage(LM.getErrorBasic(), "err_connection_site" + e.toString())
                    this.info.send = { "success": LM.getString("errPayment") }
                    this.info.error.err_connection_site = true
                    return this.info
                })
            if (this.info.error.message) {
                return this.info
            }
            logs.info("res.data.payment=", res.data);
            this.info.isPaid = true
            if (res.data["PaymentAnswer"]["ActionCode"] == '1000') {

                this.info.confirmation = res.data["PaymentAnswer"]["IskaNumber"]
                logs.info("confirmation", this.info.confirmation);
                this.info.send = { "success": res.data["PaymentAnswer"]["IskaNumber"] }
                return this.info
            }
            else {
                this.info.error.err_in_payment = true
                this.info.send = { "success": LM.getString("errPayment") }
                return this.info
            }

        }
    }
}
module.exports = City4uCrawler;