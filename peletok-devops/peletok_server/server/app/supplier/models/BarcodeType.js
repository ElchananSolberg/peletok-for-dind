const DataTypes = require('sequelize').DataTypes;
const BaseModel = require('../../BaseModel');
const {getNumStatus} = require("../../utils");

// Each product has an ID, but there are products that have different types of identifiers
// (for example, a product that is identified in the Peletok is 1 and Delek 2 and sonol 3 etc.)
// This model represents the possible types of identification of products in the system
// (in the above example, Peletok and Delek and sonol, etc.)

/**
 * BarcodeType model
 */
class BarcodeType extends BaseModel {

    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            required: {
                type: DataTypes.BOOLEAN,
                allowNull: true
            }
        };
        this.prefix = 'SUPL';
        this.author = true;
        this.statusAuthor = true;
        this.statusField = true;
        this.defaultStatus = getNumStatus("פעיל", "BarcodeType");
        this.statusTimestampField = true;
    }

    /**
     * creates associate for BarcodeType  model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        // noinspection JSUnresolvedVariable
        this.model.hasMany(models.productBarcode.model, {as: 'ProductBarcode' });
    }
}

module.exports = new BarcodeType ();
