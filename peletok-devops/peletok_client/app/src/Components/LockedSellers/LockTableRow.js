import React, { Fragment } from 'react';
import { LanguageManager as LM } from "../LanguageManager/Language";
import InputTypeSelect from '../InputUtils/InputTypeSelect';
import { putSeller } from '../DataProvider/Functions/DataPuter'


/**
 * LockTableRow - component that builds and returns an array of <td> elements from the data sended by LockTable
 */
export default class LockTableRow extends React.Component {

    constructor(props) {
        super(props);

       
    }
    onChange = (e) => {
        let status;
        if (e.target.value == LM.getString("lock")) { status = 3 }
        else if (e.target.value == LM.getString("froze")) { status = 2 }
        else if (e.target.value == LM.getString("active")) { status = 1 }
        putSeller(this.props.tableRowObj.id, { status: status }).then(
            res => {
                alert(this.props.tableRowObj.name+"  " + e.target.value)
            }
        ).catch(
            err => {
                alert(err);
            }
        );

    }
    render() {
        var tableRowObj = this.props.tableRowObj;
        let select = <InputTypeSelect options={[LM.getString("lock"), LM.getString("froze"), LM.getString("active")]}
            default={tableRowObj["status"] == 3 ? LM.getString("lock") : LM.getString("froze")} onChange={(e) => this.onChange(e)} id='statusID' />
        var tdArray = [];
        var mapKey = 0;
        tdArray.push(<td className='font-weight-bold fs-13 pr-3' style={{ width: '40%' }} key={mapKey++}>{tableRowObj["name"]}</td>)
        tdArray.push(<td className='fs-13 pr-3' style={{ width: '10%' }} key={mapKey++}>{tableRowObj["id"]}</td>)
        tdArray.push(<td className='fs-13 pr-3' style={{ width: '20%' }} key={mapKey++}>{tableRowObj["agent_num"]}</td>)
        tdArray.push(<td className='fs-13 pr-3' key={mapKey++}>{select}</td>)
        return (
            <Fragment>
                {tdArray}
            </Fragment>
        );
    }
}