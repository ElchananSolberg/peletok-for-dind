
const models = require('../../index')
const sequelize = require('../../setupSequelize')
const request = require('supertest');
// const routing = require('../../routing')
const {productRouter} = require('../../api/productRouter')
const bodyParser = require('body-parser');
var express = require('express');
const {getStrType, getTypeNum, getNumStatus} = require('../../utils')
const {truncateModel, fileNameFormat} = require('./testUtil')
const {params, mockProducts} = require('./mockeProducts')
const path = require("path");
const moment = require('moment')
const initialEndDate = '2099-12-31'


describe('productRouterUpdate', () => {

    

    describe('PUT /product/:product_id', () => {

            
            let current_user;
            let app;
            let products;
            let business;

            beforeAll(async  (done) => {

                await sequelize.sync({
                         force: true,
                         logging: false
                });
                
                [current_user, supplier1, business] = await mockProducts();
                
                app = express();
                app.use(express.json());
                app.use(express.urlencoded({extended: false}));
                app.use(bodyParser.urlencoded({ extended: false })); 
                // mock req.user
                app.use((req, res, next)=>{      
                    req.user = {
                        id: current_user.id,
                        business_id: business.id,
                        language_code: 'HE',
                        business_id: 1
                    };
                    next();
                })
                // app.use('/', routing)
                app.use('/product/', productRouter );                
                
                done()
            } ) 

            afterAll(async (done) => {
                await sequelize.close()
            } )

            it('should\'nt return unautorized product', async () => {
                let productId = 2;
                await models.businessProduct.model.update({is_authorized: false}, {
                    where: {is_authorized: true}
                })
                        
                return request(app).put('/product/' + productId).then((response) => {
                    expect(response.statusCode).toBe(401);
                    expect(response.text).toBe('Unauthorized');
                } )
            } )

            // it('Should retun status 200', async() => {
            //     let productId = 2;
            //             await models.businessProduct.model.update({is_authorized: false}, {
            //                 where: {is_authorized: true}
            //             })
            //             await models.businessProduct.model.update({is_authorized: true}, {
            //                  where: {product_id: productId}
            //             })
                        
            //     return request(app).put('/product/' + productId).then((response) => {
            //         expect(response.statusCode).toBe(200);
            //     } )
            // } )

            test('product after apdate should be  ...', async () => {
                        let productId = 2;
                        await models.businessProduct.model.update({is_authorized: false}, {
                            where: {is_authorized: true}
                        })
                        await models.businessProduct.model.update({is_authorized: true}, {
                             where: {product_id: productId}
                        })
                       
                        const newParams = {
                            call_terms: 'new call_terms',
                            order: 77,
                            supplier_id: 2,
                            status: 1,
                            usage_time_limit: 77,
                            type: 44,
                            profit_model: 'commission',
                            more_info_description: 'new more_info_description',
                            more_info_youtube_url: 'new youtube_url',
                            more_info_site_url: 'new site_url',
                            call_minute: params.call_minute,
                            sms: 777,
                            browsing_package: '777',
                            call_to_palestinian: false,
                            abroed_price: 777,
                            other1: params.other1 + 'new',
                            other2: params.other2 + 'new',
                            status: false,
                            default_is_allow: false,
                            cancelable: false,
                            
                        }
                        
                        return request(app).put('/product/' + productId)
                        .send(newParams)
                        .then(async(response) => {
                            const product = await models.product.model.findOne({
                                where: {
                                    id: productId,
                                }
                            })
                            newParams.status = getNumStatus('inactive', 'Product');
                            expect(product).toMatchObject(newParams)
                        })

                    
            })

            test('product and associations after apdate should be  ...', async () => {
                        let productId = 2;
                        await models.businessProduct.model.update({is_authorized: false}, {
                            where: {is_authorized: true}
                        })
                        await models.businessProduct.model.update({is_authorized: true}, {
                             where: {product_id: productId}
                        })

                        await truncateModel(models.productBarcode.model)
                        await models.productBarcode.model.bulkCreate([
                            {
                                barcode: '12345',
                                barcode_type_id: 1,
                                product_id: productId
                            },
                             {
                                barcode: '123456',
                                barcode_type_id: 2,
                                product_id: productId

                            }
                        ])

                        const p = await models.product.model.findOne({where: {id: productId}})
                        p.addTags([2, 4, 5])
                        
                        const newParams = {
                           more_details_link: 'new more_details_link',
                            video_link: 'new video_link',
                            call_terms: 'new call_terms',
                            order: 77,
                            supplier_id: 2,
                            status: 1,
                            usage_time_limit: 77,
                            type: 44,
                            profit_model: 'commission',
                            name_he: 'new he name',
                            name_en: 'new en name',
                            name_ar: 'new ar name',
                            description_he: 'new he description',
                            description_en: 'new en description',
                            description_ar: 'new ar diescription',
                            more_info_description: 'new more_info_description',
                            more_info_youtube_url: 'new youtube_url',
                            more_info_site_url: 'new site_url',
                            price: 77,
                            buying_price: 777,
                            tags:'[1,2]',
                            barcodes: JSON.stringify([
                                
                                {barcode: '1235888', barcode_type_id: '2'},
                                {barcode: '1235777', barcode_type_id: '3'},
                            ]),
                            talk_prices: JSON.stringify([
                                {country: "ישראל", outcome_price: 7, income_price: 9, sms_price: 10},
                                {country: "ארהב", outcome_price: 7, income_price: 9, sms_price: 10},
                                {country: "אוקרינה", outcome_price: 7, income_price: 9, sms_price: 10},
                            ])
                        }
                        
                        return request(app).put('/product/' + productId)
                        .attach('image', path.join(__dirname,'wataniya-10.png')).field(newParams)
                        .attach('more_info_image', path.join(__dirname,'wataniya-10.png')).field(newParams)
                        .then(async(response) => {
                            const product = await models.product.model.findOne({
                                where: {
                                    id: productId,
                                },
                                include: [
                                    {model: models.productDetails.model, as: 'ProductDetails'},
                                    {model: models.productPrice.model, as: 'ProductPrice'},
                                    {model: models.productBarcode.model, as: 'ProductBarcode'},
                                    {model: models.talkPrice.model, as: 'TalkPrice'}
                                ]
                            })
                      
                        const [docs, tags] = await Promise.all(
                            [    
                                product.getDocument(),
                                product.getTags()
                            ]
                        )
                            expect(product).toMatchObject({
                                call_terms: newParams.call_terms,
                                order: newParams.order,
                                supplier_id: newParams.supplier_id,
                                status: newParams.status,
                                usage_time_limit: newParams.usage_time_limit,
                                type: newParams.type,
                                profit_model: newParams.profit_model,
                                more_info_description:newParams.more_info_description,
                                more_info_youtube_url:newParams.more_info_youtube_url,
                                more_info_site_url:newParams.more_info_site_url,
                            })

                            expect(product.ProductDetails.find(p=>p.language_code == 'HE')).toMatchObject({
                                product_name: newParams.name_he,
                                product_description: newParams.description_he
                            })
                            expect(product.ProductDetails.find(p=>p.language_code == 'EN')).toMatchObject({
                                product_name: newParams.name_en,
                                product_description: newParams.description_en
                            })
                            expect(product.ProductDetails.find(p=>p.language_code == 'AR')).toMatchObject({
                                product_name: newParams.name_ar,
                                product_description: newParams.description_ar
                            })
                            
                            

                            expect(moment(product.ProductPrice[0].timestamp_end).isBetween(moment().subtract(2, 'seconds'),moment() )).toBe(true)
                            expect(moment(product.ProductPrice[1].timestamp_end).isSame(moment(initialEndDate))).toBe(true)
                            expect(moment(product.ProductPrice[1].timestamp_start).isBetween(moment().subtract(2, 'seconds'),moment() )).toBe(true)

                            
                            
                             expect(docs.filter(d=>d.type == getTypeNum('image', 'Document')).length).toBe(1)
                             expect(docs.filter(d=>d.type == getTypeNum('other_image', 'Document')).length).toBe(1)


                            expect((docs.find(d=>d.type == getTypeNum('image', 'Document')) || {}).url).toEqual(
                                expect.stringMatching(fileNameFormat('productsImages', 'image'))
                                )
                            expect((docs.find(d=>d.type == getTypeNum('other_image', 'Document')) || {}).url).toEqual(
                                expect.stringMatching(fileNameFormat('productsImages', 'more_info_image'))
                                )

                            expect(product.ProductBarcode.find(pb=>pb.id == 7)).toBe(undefined)
                            expect(product.ProductBarcode.find(pb=>pb.id == 8).barcode).toBe('1235888')
                            expect(product.ProductBarcode.find(pb=>pb.id == 9)).not.toBe(undefined)
                            expect(product.ProductBarcode.find(pb=>pb.id == 9).barcode_type_id).toBe(3)
                            expect(product.ProductBarcode.find(pb=>pb.id == 9).barcode).toBe('1235777')
                            
                            expect(tags.length).toBe(1)
                            expect(tags[0].type).toBe(2)
                            
                            expect(product.TalkPrice.length).toBe(3)
                            expect(product.TalkPrice.map(t=>t.id)).toMatchObject([13,14,15])
                        

                        })



                        
                        

                        
                    
            })

            it('only more info params Should retun status 200', async() => {

             let moreInfoParams = {
                 more_info_description: 'new more_info_description',
                 more_info_youtube_url: 'new youtube_url',
                 more_info_site_url: 'new site_url',
                 talk_prices: JSON.stringify([
                    {country: "ישראל", outcome_price: 7, income_price: 9, sms_price: 10},
                    {country: "ארהב", outcome_price: 7, income_price: 9, sms_price: 10},
                    {country: "אוקרינה", outcome_price: 7, income_price: 9, sms_price: 10},
                ])

             }
                let productId = 2;
                await models.businessProduct.model.update({is_authorized: false}, {
                    where: {is_authorized: true}
                })
                await models.businessProduct.model.update({is_authorized: true}, {
                     where: {product_id: productId}
                })
                        
                return request(app).put('/product/' + productId)
                .attach('more_info_image', path.join(__dirname,'wataniya-10.png')).field(moreInfoParams)
                .then((response) => {
                    expect(response.statusCode).toBe(200);
                } )
            } )
           
    })
} )