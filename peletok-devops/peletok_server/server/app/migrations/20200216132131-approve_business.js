'use strict';
const {addPermission} = require('../dbUtils/addPermission');

module.exports = {
  up: (queryInterface, Sequelize) => {
   return addPermission(queryInterface, 'אישור הקמת עסקים', 'api', 'PUT /business/approve', 26)
  },

  down: (queryInterface, Sequelize) => {
   return Promise.resolve()
  }
};
