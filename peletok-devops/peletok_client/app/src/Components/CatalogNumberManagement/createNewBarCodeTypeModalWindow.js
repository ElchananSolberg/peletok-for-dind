import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Container, Row, Col } from 'reactstrap';
import InputTypeText from '../InputUtils/InputTypeText'
import InputTypeRadio from '../InputUtils/InputTypeRadio'
import { LanguageManager as LM } from '../LanguageManager/Language';


export class CreateNewBarCodeTypeModalWindow extends React.Component {
    constructor(props) {
        super(props);

    };
    render() {
        return (
            <Container>
                <Modal isOpen={this.props.isOpen} >
                    <ModalHeader toggle={this.props.toggle}>{LM.getString("createNewCatalogNumber")}</ModalHeader>
                    <ModalBody>
                        <Row>
                            <Col>
                                <InputTypeText id='1'
                                    ref={(objRef) => { this.newBarcodeNameRef = objRef }}
                                    required={true} />
                            </Col>
                        </Row>
                        <Row >
                            <Col>
                                <InputTypeRadio
                                    id='2'
                                    options={LM.getString("banersStatusList")}
                                    required={true}
                                    ref={(objRef) => { this.activeRadioBtnRef = objRef }} />
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <InputTypeRadio
                                    id='3'
                                    options={LM.getString("mandatory")}
                                    required={true}
                                    ref={(objRef) => { this.requiredRadioBtnRef = objRef }} />
                            </Col>
                        </Row>
                    </ModalBody>
                    <ModalFooter>
                        <Button className='regularButton m-2' onClick={this.props.saveHandler}>{LM.getString("save")}</Button>{' '}
                        <Button color="secondary" onClick={this.props.cancelClick}>{LM.getString("cancel")}</Button>
                    </ModalFooter>
                </Modal>
            </Container>
        );
    }
}
