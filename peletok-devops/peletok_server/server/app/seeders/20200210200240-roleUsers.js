'use strict';
const moment = require('moment');
module.exports = {
  up: (queryInterface, Sequelize) => {
    let dateNow = moment().toDate();
    let roleUsers = [
      // {
      //   user_id: 1, role_id: 2,
      //   created_at: dateNow,
      //   updated_at: dateNow
      // },
      // {
      //   user_id: 2, role_id: 2,
      //   created_at: dateNow,
      //   updated_at: dateNow
      // },
      // {
      //   user_id: 3, role_id: 2,
      //   created_at: dateNow,
      //   updated_at: dateNow
      // },
      {
        user_id: 4, role_id: 1,
        created_at: dateNow,
        updated_at: dateNow
      },
      // {
      //   user_id: 6, role_id: 2,
      //   created_at: dateNow,
      //   updated_at: dateNow
      // },
      // {
      //   user_id: 7, role_id: 2,
      //   created_at: dateNow,
      //   updated_at: dateNow
      // },
      // {
      //   user_id: 8, role_id: 2,
      //   created_at: dateNow,
      //   updated_at: dateNow
      // },
      // {
      //   user_id: 9, role_id: 2,
      //   created_at: dateNow,
      //   updated_at: dateNow
      // },
      // {
      //   user_id: 10, role_id: 2,
      //   created_at: dateNow,
      //   updated_at: dateNow
      // },
      // {
      //   user_id: 11, role_id: 2,
      //   created_at: dateNow,
      //   updated_at: dateNow
      // },
      // {
      //   user_id: 12, role_id: 2,
      //   created_at: dateNow,
      //   updated_at: dateNow
      // },
      // {
      //   user_id: 13, role_id: 2,
      //   created_at: dateNow,
      //   updated_at: dateNow
      // },
      // {
      //   user_id: 14, role_id: 2,
      //   created_at: dateNow,
      //   updated_at: dateNow
      // },
      // {
      //   user_id: 15, role_id: 2,
      //   created_at: dateNow,
      //   updated_at: dateNow
      // },
      // {
      //   user_id: 16, role_id: 2,
      //   created_at: dateNow,
      //   updated_at: dateNow
      // },
      // {
      //   user_id: 17, role_id: 2,
      //   created_at: dateNow,
      //   updated_at: dateNow
      // },
      // {
      //   user_id: 18, role_id: 2,
      //   created_at: dateNow,
      //   updated_at: dateNow
      // },
      // {
      //   user_id: 19, role_id: 2,
      //   created_at: dateNow,
      //   updated_at: dateNow
      // },
      // {
      //   user_id: 20, role_id: 2,
      //   created_at: dateNow,
      //   updated_at: dateNow
      // },
      // {
      //   user_id: 21, role_id: 2,
      //   created_at: dateNow,
      //   updated_at: dateNow
      // },
      // {
      //   user_id: 22, role_id: 2,
      //   created_at: dateNow,
      //   updated_at: dateNow
      // },
      // {
      //   user_id: 23, role_id: 2,
      //   created_at: dateNow,
      //   updated_at: dateNow
      // },
      // {
      //   user_id: 24, role_id: 2,
      //   created_at: dateNow,
      //   updated_at: dateNow
      // },
      // {
      //   user_id: 25, role_id: 2,
      //   created_at: dateNow,
      //   updated_at: dateNow
      // },
      // {
      //   user_id: 26, role_id: 2,
      //   created_at: dateNow,
      //   updated_at: dateNow
      // },
      // {
      //   user_id: 27, role_id: 2,
      //   created_at: dateNow,
      //   updated_at: dateNow
      // },
      // {
      //   user_id: 28, role_id: 2,
      //   created_at: dateNow,
      //   updated_at: dateNow
      // },
      // {
      //   user_id: 29, role_id: 2,
      //   created_at: dateNow,
      //   updated_at: dateNow
      // },
      // {
      //   user_id: 30, role_id: 2,
      //   created_at: dateNow,
      //   updated_at: dateNow
      // },
      // {
      //   user_id: 31, role_id: 2,
      //   created_at: dateNow,
      //   updated_at: dateNow
      // },
      // {
      //   user_id: 32, role_id: 2,
      //   created_at: dateNow,
      //   updated_at: dateNow
      // },
      // {
      //   user_id: 33, role_id: 2,
      //   created_at: dateNow,
      //   updated_at: dateNow
      // },
      // {
      //   user_id: 5, role_id: 2,
      //   created_at: dateNow,
      //   updated_at: dateNow
      // },
    ]

    return queryInterface.bulkInsert('RoleUser', roleUsers, {});

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('RoleUser', null, {});
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
