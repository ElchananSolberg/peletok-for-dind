// Reads project configurations
const env = process.env.NODE_ENV || 'development';
const fs = require('fs');
const path = require('path');

let config_dir = path.join(__dirname);
const configFile = require(path.join(config_dir, 'config.json'))
let config = configFile[env];
let localConfig = path.join(config_dir, 'localConfig.json');



/***
 * Returns the configurations of DB
 * @returns {*}
 */
function getDBConfig() {
    const dbConf = config["db"];
    if ((dbConf["use_local_config"]) && (fs.existsSync(localConfig))) {
        Object.assign(dbConf, require(localConfig)["db"]);
    }
    const seqLogging = process.env["SEQ_LOGGING"]||dbConf["SEQ_LOGGING"];
    if(seqLogging==="false")
        dbConf["logging"] = false;
    if (dbConf["use_env_variable"]) {
        dbConf["username"] = process.env["POSTGRES_USER"];
        dbConf["password"] = process.env["POSTGRES_PASSWORD"];
        dbConf["database"] = process.env["POSTGRES_DB"];
    }

    return dbConf
}


/***
 * Returns configuration (localConfig if exist and config if not)
 * @return {Object}
 */
function getConfig() {
    return (fs.existsSync(localConfig) && require(localConfig)) || config;
}

/***
 * Configs card for payment by local config
 */
function setCardConf(){
    if  (fs.existsSync(localConfig)){
        config["card"] = require(localConfig)["card"];
    }
}

/**
 * Get environment name that appeare in the log name
 */
function getLoggerName(){
    return config["env-name-for-log"]
}

/**
 * Get environment value or default of level logging
 */
function getLoggerLevel(){
    const defaultLevel = "debug";
    return process.env["LOGGING_LEVEL"] || getConfig()["LOGGING_LEVEL"] || defaultLevel;
}

/**
 * Get environment value or default of level console log
 */
function getConsoleLogLevel(){
    const defaultLevel = "debug";
    return process.env["CONSOLE_LOG_LEVEL"] || getConfig()["CONSOLE_LOG_LEVEL"] || defaultLevel;
}

function getChargeLimitTime(){
    return getConfig()["charge_time_limit"]
}

/***
 * Return configuration for start DB
 * @returns {*}
 */
function getStartDbConfig(){
    const logger = require("../logger");
    let startDbConfig = getConfig()["start_db"];
    if(startDbConfig["force_sync"]){
        if(!startDbConfig["store_map"]){
            logger.warn("data for mapping has been deleted and not restored")
        }
        if(!startDbConfig["demo_data"]){
            logger.warn("demo data has been deleted and not restored")
        }
    }
    if(!startDbConfig["force_sync"]){
        if(startDbConfig["store_map"]){
            logger.warn("data for mapping not deleted and restored (duplicated)")
        }
        if(startDbConfig["demo_data"]){
            logger.warn("demo data not deleted and restored (duplicated)")
        }
    }
    if(!startDbConfig["store_map"]){
        logger.warn("data for mapping not restored so that changes in map.csv file not effect")
    }
    if(!startDbConfig["demo_data"]){
        logger.warn("demo data not restored so that changes in demo data not effect")
    }
    if(!startDbConfig["force_sync_view"]){
        logger.warn("if view table exist will an error raised!!!");
    }
    return startDbConfig
}


module.exports.dbConfiguration = getDBConfig();
module.exports.config = config;
module.exports.getLoggerName = getLoggerName;
module.exports.getLoggerLevel = getLoggerLevel;
module.exports.getConsoleLogLevel = getConsoleLogLevel;
// For migration convention use
module.exports['development-vm'] = configFile['development-vm'].db
module.exports['prod-k8s'] = configFile['prod-k8s'].db



module.exports.getChargeLimitTime = getChargeLimitTime;
module.exports.getStartDbConfig = getStartDbConfig;


