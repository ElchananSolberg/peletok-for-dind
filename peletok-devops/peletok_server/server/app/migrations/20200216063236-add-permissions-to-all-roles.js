'use strict';
const moment = require('moment');
var Sequelize = require('sequelize');
var Permission = require('../permission/models/Permission').model;
var Op = Sequelize.Op;

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let permissionIds = [1, 17, 18, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 203, 216, 136, 159, 160, 93, 79, 110, 95, 100, 101, 105, 142, 139, 140, 141, 143, 144, 145, 146, 221, 222, 223, 224, 225, 226, 177];
    let permissionRoles = [2,3,4].reduce((n, r) => [...permissionIds.map(getCreatePermissionRole(r)), ...n],[])
    return queryInterface.bulkInsert('PRM_PermissionRole', permissionRoles, {});
  },

  down: (queryInterface, Sequelize) => {
    return Promise.resolve()
  }
}; 

const getCreatePermissionRole = (roleId) => {
  return (PermissionId) => {
      return {
        action: 'show',
        role_id: roleId,
        permission_id: PermissionId,
        created_at: moment().toDate(),
        updated_at: moment().toDate(),
      }
  }
}

