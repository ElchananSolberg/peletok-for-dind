NETWORK_PROFILES = {
    "cable": {
        "offline": "false",
        "latency": "28",
        "downloadThroughput": "5000000",
        "uploadThroughput": "1000000"
    },
    "dsl": {
        "offline": "false",
        "latency": 50,
        "downloadThroughput": 1500000,
        "uploadThroughput": 384000
    },
    "3G_slow": {
        "offline": "false",
        "latency": 400,
        "downloadThroughput": 400000,
        "uploadThroughput": 400000
    },
    "3G": {
        "offline": "false",
        "latency": 300,
        "downloadThroughput": 1600000,
        "uploadThroughput": 768000
    },
    "3G_fast": {
        "offline": "false",
        "latency": 170,
        "downloadThroughput": 1600000,
        "uploadThroughput": 768000
    },
    "4G": {
        "offline": "false",
        "latency": 150,
        "downloadThroughput": 9000000,
        "uploadThroughput": 9000000
    },
    "lte": {
        "offline": "false",
        "latency": 70,
        "downloadThroughput": 12000000,
        "uploadThroughput": 12000000
    },
    "edge": {
        "offline": "false",
        "latency": 840,
        "downloadThroughput": 240000,
        "uploadThroughput": 240000
    },
    "2G": {
        "offline": "false",
        "latency": 800,
        "downloadThroughput": 280000,
        "uploadThroughput": 256000
    }
}

