const models = require('../../index')
const sequelize = require('../../setupSequelize')
const request = require('supertest');
// const routing = require('../../routing')
const {supplierRouter} = require('../../api/supplierRouter')
const bodyParser = require('body-parser');
var express = require('express');
const {getStrType, getTypeNum} = require('../../utils')
const {truncateModel} = require('./testUtil')
const {params, mockSuppliers} = require('./mockSuppliers')




describe('supplierRouterGet', () => {

    

    describe('GET /supplier', () => {

            
            let current_user;
            let app;
            let suppliers;
            let business;

            beforeAll(async  (done) => {

                await sequelize.sync({
                         force: true,
                         logging: false
                });
                
                [suppliers, current_user, business] = await mockSuppliers();
                
                app = express();
                app.use(express.json());
                app.use(express.urlencoded({extended: false}));
                app.use(bodyParser.urlencoded({ extended: false })); 
                // mock req.user
                app.use((req, res, next)=>{      
                    req.user = {
                        id: current_user.id,
                        business_id: business.id,
                        language_code: 'HE',
                    };
                    next();
                })
                // app.use('/', routing)
                app.use('/supplier/', supplierRouter );                

                
                done()
            } ) 

            it('Should retun status 200', () => {
                return request(app).get('/supplier/').then((response) => {
                    expect(response.statusCode).toBe(200);
                } )
            } )

            it('Should retun 3 suppliers', async () => {
                await models.businessSupplier.model.update({is_authorized: true}, {
                    where: {is_authorized: false}
                })
                return request(app).get('/supplier/').then((response) => {
                    expect(response.body[getStrType(1, 'supplier')].length).toBe(3);
                } )
            } )

            // Test create supplier hooks . exist here because time reasons
            it('Should retun 6 businessSuppliers objects', (done) => {
                return request(app).get('/supplier/').then(async (response) => {
                    businessSuppliers = await models.businessSupplier.model.findAll()
                    expect(businessSuppliers.length).toBe(6);
                    done()
                } )
            } )

            it('Should retun only currentBussines suppliers', async () => {
                let supplierId = 2;
                await models.businessSupplier.model.update({is_authorized: true}, {
                     where: {is_authorized: false}
                }) 

                await models.businessSupplier.model.update({is_authorized: false}, {
                    where: {is_authorized: true, business_id: business.id}
                })
                await models.businessSupplier.model.update({is_authorized: true}, {
                     where: {supplier_id: supplierId, business_id: business.id}
                })
                

                return request(app).get('/supplier/').then((response) => {
                    console.log(response.body)
                    expect(response.body[getStrType(1, 'supplier')].length).toBe(1)
                    expect(response.body[getStrType(1, 'supplier')][0].id).toBe(supplierId)
                } )
            } )

            

            test('response should contain fields', async () => {
                let supplier = await models.supplier.model.findOne({
                    where: {id: 2}
                })
                let images = await supplier.getDocument({
                    where: {type: getTypeNum('image', 'Document')}
                })
                let color = await supplier.getSupplierColor()
                return request(app).get('/supplier/').then((response) => {
                    expect(response.body).toMatchObject({
                           [getStrType(1, 'supplier')]:[
                               {
                                  id: supplier.id,
                                  name: supplier.name,
                                  order: supplier.order,
                                  image: images[0].url,
                                  logo_background_color:color[0].logo_background
                              }
                            ]
                          
                    });
                } )
            } )

            it('should return 1 supplier even document doesnt exist', async () => {
                truncateModel(models.document.model)
                return request(app).get('/supplier/').then((response) => {
                    console.log(response.body)
                    expect(response.body[getStrType(1, 'supplier')].length).toBe(1);
                } )
            } )

            



      } )
} )