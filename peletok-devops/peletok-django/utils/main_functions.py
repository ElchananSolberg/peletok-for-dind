from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
import logging
from selenium.webdriver.common.keys import Keys
import os

TIMEOUT = 5


class MainFunctions:
    def __init__(self, url):
        self.url = url

        # chrome_options = Options()
        # chrome_options.add_argument('--headless')
        # chrome_options.add_argument('--no-sandbox')
        # chrome_options.add_argument('--disable-dev-shm-usage')..
        # remote_id = "{}/wd/hub".format(os.getenv("SELENIUM_URL"))
        remote_id = 'http://peletok-proxy.ravtech.co.il:3447/wd/hub'
        
        selenium_remote_docker = {
              'browserName': 'chrome',
              'version': '',
              'platform': 'ANY',
              'acceptSslCerts': True,
              'acceptInsecureCerts': True
        }
        self.driver = webdriver.Remote(command_executor=f'{remote_id}', desired_capabilities=selenium_remote_docker)
        self.driver.get(url)

    def wait_for_element(self, selector, timeout=TIMEOUT, raise_exception=True):
        try:
            element = WebDriverWait(self.driver, timeout=timeout).until(
                ec.presence_of_element_located(selector))
            if not element:
                raise NoSuchElementException
            return element
        except Exception as e:
            print("element {} not found exception: {}".format(selector, e))
            if raise_exception:
                raise e
            return False

    def wait_for_alert(self, timeout=TIMEOUT, raise_exception=True):
        try:
            element = WebDriverWait(self.driver, timeout=timeout).until(
                ec.alert_is_present())
            if not element:
                raise NoAlertPresentException
            return element
        except Exception as e:
            print("alert not found exception: {}", e)
            if raise_exception:
                raise e
            return False

    def wait_for_button(self, selector, timeout=TIMEOUT, raise_exception=True):
        try:
            element = WebDriverWait(driver=self.driver, timeout=timeout).until(
                ec.element_to_be_clickable(selector))
            if not element:
                raise NoSuchElementException
            return element
        except Exception as e:
            print("element {} not found exception: {}".format(selector, e))
            if raise_exception:
                raise e
            return False

    def wait_for_elements_to_be_present(self, selector, timeout=TIMEOUT, raise_exception=True):
        try:
            elements = WebDriverWait(driver=self.driver, timeout=timeout). \
                until(ec.visibility_of_all_elements_located(selector))
            if not elements:
                raise NoSuchElementException
            return elements
        except Exception as e:
            print("element {} not found exception: {}".format(selector, e))
            if raise_exception:
                raise e
            return False

    def wait_for_frame_available_and_switch(self, selector, timeout=TIMEOUT, raise_exception=True):
        try:
            elements = WebDriverWait(driver=self.driver, timeout=timeout). \
                until(ec.frame_to_be_available_and_switch_to_it(selector))
            if not elements:
                raise NoSuchElementException
            return elements
        except Exception as e:
            print("frame {} not found exception: {}".format(selector, e))
            if raise_exception:
                raise e
            return False

    def wait_tree_seconds_for_element(self, selector, timeout=3, raise_exception=True):
        try:
            element = WebDriverWait(self.driver, timeout=timeout).until(
                ec.presence_of_element_located(selector))
            if not element:
                raise NoSuchElementException
            return element
        except Exception as e:
            if raise_exception:
                raise e
            return False

    def wait_for_element_contain_text(self, selector, text_contain=None, timeout=TIMEOUT,
                                      raise_exception=True):
        by, selector_new = selector
        new_selector = (by, selector_new.format(text=text_contain))
        self.wait_for_elements_to_be_present(selector=new_selector, timeout=timeout,
                                             raise_exception=raise_exception)

    def wait_for_element_until_not_displayed(self, selector, timeout=TIMEOUT):
        WebDriverWait(self.driver, timeout=timeout).until_not(
            ec.presence_of_element_located(selector))

    def wait_for_element_until_invisibility(self, selector, timeout=TIMEOUT):
        WebDriverWait(self.driver, timeout=timeout).until(
            ec.invisibility_of_element_located(selector))

    def send_keys(self, locator, send_keys):
        self.wait_for_element(locator)
        logging.info(f' send keys "{send_keys}" in "{locator[1]}".'.encode("utf-8"))
        self.delete_text_from_element(locator)
        return self.driver.find_element(*locator).send_keys(send_keys)

    def delete_text_from_element(self, locator):
        self.wait_for_element(locator)
        text = self.get_value_from_element(locator)
        if text is None:
            text = self.get_text_from_element(locator)
        logging.info(f' delete text "{text}" from "{locator[1]}".'.encode("utf-8"))
        for i in range(len(text)):
            self.driver.find_element(*locator).send_keys(Keys.BACK_SPACE)

    def click_button(self, locator):
        self.wait_for_button(locator)
        logging.info(f' click "{locator[1]}" button.'.encode("utf-8"))
        return self.driver.find_element(*locator).click()

    def click_button_by_doc(self, locator):
        logging.info(f' click "{locator}" button.'.encode("utf-8"))
        return self.driver.execute_script(f"document.getElementById('{locator}').click()")

    def click_button_in_elements(self, locator, index):
        self.wait_for_element(locator)
        logging.info(f' click "{locator[1]}" button.'.encode("utf-8"))
        return self.driver.find_elements(*locator)[index].click()

    def get_text_from_element(self, locator):
        self.wait_for_element(locator)
        text = self.driver.find_element(*locator).text
        logging.info(f' get "{text}" from "{locator[1]}" element.'.encode('utf-16'))
        return text

    def get_value_from_element(self, locator):
        self.wait_for_element_contain_text(locator)
        value = self.driver.find_element(*locator).get_attribute('value')
        logging.info(f' get "{value}" from "{locator[1]}" element.'.encode('utf-8'))
        return value

    def get_text_in_elements(self, locator, index):
        self.wait_for_element(locator)
        logging.info(f' click "{locator[1]}" button.'.encode("utf-8"))
        return self.driver.find_elements(*locator)[index].text

    def check_if_element_is_displayed(self, locator):
        self.wait_tree_seconds_for_element(locator)
        logging.info(f' check if "{locator[0]}, {locator[1]}" displayed.')
        if self.driver.find_element(*locator).is_displayed():
            return [True, ""]
        else:
            return [False, f"The element: '{locator[1]}' not exits on the screen."]

    def check_if_element_exits(self, locator):
        logging.info(f' check if "{locator[0]}, {locator[1]}" displayed.')
        try:
            self.driver.find_element(*locator).is_displayed()
            return [True, ""]
        except NoSuchElementException:
            return [False, f"The element: '{locator[1]}' not exits on the screen."]

    def check_element_is_enabled(self, locator):
        self.wait_for_element(locator)
        logging.info(f' click "{locator[1]}" button.'.encode("utf-8"))
        return self.driver.find_element(*locator).is_enabled()

    # check if can delete
    def click_by_text(self, value):
        buttons = self.driver.find_elements_by_css_selector('div')
        for b in buttons:
            if b.text == value:
                logging.info(f' click text: "{value}".')
                return b.click()

    # check if can delete
    def select_from_selector(self, locator, value):
        self.click_button(locator)
        self.click_by_text(value)

    def select_by_text(self, elem, value):
        self.wait_for_element(elem)
        select = Select(self.driver.find_element(*elem))
        select.select_by_visible_text(value)

    def select_by_index(self, elem, index):
        self.wait_for_element(elem)
        select = Select(self.driver.find_element(*elem))
        select.select_by_index(index)

    def select_by_value(self, elem, value):
        select = Select(self.driver.find_element(*elem))
        select.select_by_value(value)

    def get_alert_text(self):
        self.wait_for_alert()
        alert = self.driver.switch_to.alert
        return alert.text

    def accept_alert(self):
        self.wait_for_alert()
        alert = self.driver.switch_to.alert
        alert.accept()

    def dismiss_alert(self):
        self.wait_for_alert()
        alert = self.driver.switch_to.alert
        alert.dismiss()
