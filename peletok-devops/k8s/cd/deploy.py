import argparse
import os
import re
import subprocess
from _decimal import Decimal


class Deploy:
    def __init__(self, namespace: str, registry: str, create_deployment='false', service_name='client'):
        self.service = service_name
        self.registry = registry
        self.create_deployment = create_deployment  # use to replace update to create.
        self.image = service_name + '-' + namespace
        self.namespace = namespace
        # self.namespace_dir = namespace.replace('-', '_')
        self.new_version = self.get_last_version().get('last_tag')

    def get_last_version(self) -> dict:
        """
        Get the last tag, from the repository of Azure.
        :return dict {last_tag: '', next tag: ''}:
        """
        print('Get the last tag')
        try:
            str_tags = subprocess.check_output(
                f"az acr repository show-tags -n {self.registry} --repository {self.image}",
                shell=True).decode('utf-8')
            print(str_tags)
            list_tags = re.findall(r'"(.*)"', str_tags)
            str_last_tag = max(list_tags)
            last_tag_flout = Decimal(re.findall("\d+\.\d+", str_last_tag)[0])
            str_next_tag = f"v{last_tag_flout + Decimal('0.01')}"
            return {"last_tag": str_last_tag, "next_tag": str_next_tag}
        except subprocess.CalledProcessError:
            # use for new image
            return {"last_tag": 'v0.00', "next_tag": 'v1.01'}

    def build_deploy(self):
        print('Build Deployment')
        # create new deployments
        if self.create_deployment == 'true':
            os.system(f"kubectl delete --all deployments --namespace={self.namespace}")
            os.system(f"kubectl delete --all pv --namespace={self.namespace}")
            os.system(
                f"kubectl create -f k8s/deployment/deploy/{self.service}_deployment.yml --namespace={self.namespace} "
                f"--validate=false --record")
        else:
            print("kubectl deploy command")
            os.environ["version"] = self.new_version
            os.environ["tag_name"] = self.image
            os.environ["registry_name"] = self.registry
            os.system(f'envsubst < k8s/deployment/deploy/{self.service}_deployment.yml | kubectl apply -f - --namespace={self.namespace} --validate=false --record')
            print("==FINISH==")


class Main:
    @staticmethod
    def get_args():
        """
        get args from command line.
        :return: args
        """
        print("\nget args\n")
        # initiate the parser
        parser = argparse.ArgumentParser()
        parser.add_argument("-n", "--namespace", help="namespace of k8s", default='tal-dev')
        parser.add_argument("-r", "--registry", help="registry of images", default='tal-dev')
        parser.add_argument("-rb", "--rebuild", help="'true' for delete pods and create new pods", default='false',
                            action="store_true")
        parser.add_argument("-s", "--service", help="service of deployment", default='web')
        # read arguments from the command line
        command_line_args = parser.parse_args()
        return command_line_args

    @staticmethod
    def main():
        args = Main.get_args()
        deploy = Deploy(namespace=args.namespace, registry=args.registry, service_name=args.service,
                        create_deployment=args.rebuild)
        deploy.build_deploy()

if __name__ == '__main__':
    Main.main()
