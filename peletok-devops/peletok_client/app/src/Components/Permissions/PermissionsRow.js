import React from 'react';
import { LanguageManager as LM } from "../LanguageManager/Language";
import InputTypeCheckBox from '../InputUtils/InputTypeCheckBox';
import {  PermissionsContext } from './permissionsContext';
import {CollapseChild} from '../btnUtils/collapseChild'
import {PermissionsEnum} from '../../enums'
import DatePicker from "react-datepicker";
import {dateToString} from '../../utilities/utilities'
import "react-datepicker/dist/react-datepicker.css";
import DateCheckbox from './DateCheckbox'
/**
 * PermissionsTableLastRow - component that builds and returns an array of <td> elements .
 */
export default class PermissionsTableLastRow extends React.Component {

    constructor(props) {
        super(props);
        
        
        
        this.state = {
            showDate: false,
            permissionType: this.props.permissionType,
            Watching: this.props.Watching,
            edit: this.props.edit,
            timesEdit: this.props.timesEdit,
            cancelation: this.props.cancelation,


        }
    }


    render() {
        let mapKey = 0;

        return (
           
                <PermissionsContext.Consumer>
                    {({ permissionsRolesForm,
                        updateForm,
                        permissionActions,
                        permissionLableCellWidth,
                        permissionTempEditCellWidth,
                    }) => (

                        <tr className='PermissionsTableLastRow'>
                            <td style={{width: permissionLableCellWidth}} className="collapse-td" key={mapKey++}>
                                <CollapseChild
                                    last={this.props.last}
                                />
                                <span>{ this.props.permission.name }</span>
                            
                            </td>
                            { Object.keys(permissionActions).map((key)=>{
                                let permissionRole = permissionsRolesForm[this.props.permission.id] ? 
                                permissionsRolesForm[this.props.permission.id].find(p=>PermissionsEnum.mapPermisionRoleToAction(p, key)) : null;
                                
                                return <td key={mapKey++} style={{width: parseInt(key) === PermissionsEnum.ACTION_TEMP_EDIT ? permissionTempEditCellWidth : ''}}>
                                            <div className={"custom-control custom-checkbox " + (LM.getDirection() === "rtl" ? "checkbox-rtl" : "")}>
                                                {parseInt(key) != PermissionsEnum.ACTION_TEMP_EDIT &&
                                                <InputTypeCheckBox 
                                                    checked={permissionRole} 
                                                    id={`permission_${key}_${this.props.permission.id}`} 
                                                    onChange={
                                                        updateForm(key, this.props.permission.id, this.props.groupName)  
                                                    } 
                                                  />
                                                    }
                                                    {parseInt(key) == PermissionsEnum.ACTION_TEMP_EDIT &&
                                                       <DateCheckbox
                                                           checked={permissionRole} 
                                                           id={`permission_${key}_${this.props.permission.id}`}
                                                           onChange={
                                                               (e)=>{
                                                                        updateForm(key, this.props.permission.id, this.props.groupName)(e)
                                                                }
                                                           } 
                                                       /> 
                                                    }
                                                    <span id="temp-date">
                                                        {permissionRole && permissionRole.date && dateToString(new Date(permissionRole.date))}
                                                    </span>
                                               
                                            </div>
                                           
                                            
                                        </td>
                            })}
                            
                        </tr>
                    )} 
                </PermissionsContext.Consumer>
           
        );
    }
}